# Draft differences

Differences between draft .pdfs,
with added text in blue and deleted text in red.

Run with instructions here: http://www.deanbodenham.com/learn/git-and-latexdiff.html