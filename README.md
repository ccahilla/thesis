# Craig Cahillane's Thesis Repository
Physics Graduate Student since August 2014
LIGO, California Institute of Technology

## Link to Latest Thesis Commit
[Thesis](https://gitlab.com/ccahilla/thesis/-/jobs/artifacts/master/file/Cahillane_Craig_Thesis.pdf?job=pdf "Thesis PDF Link")

## Caltech thesis library
[https://thesis.library.caltech.edu/14139/](https://thesis.library.caltech.edu/14139/)

## DOI
10.7907/76jj-mr73

Thesis on calibrating and understanding the noise at the LIGO Hanford gravitational wave detector. 
This thesis will cover the calibration uncertainty pipeline, LIGO Hanford O3 commissioning, and frequency noise for future detectors. 

TODO:
- [X] Introduction
- [X] Detector overview chapter

- [X] LHO O3 overview chapter

- [X] CARM control
- [X] CARM calibration
- [X] Frequency noise
- [X] Frequency TF

- [X] Intensity noise
- [X] Intensity TF

- [X] DARM plant 
- [X] Finesse modeling of SR3 and DARM spring

- [X] Calibration chapter
- [X] Uncertainty pipeline

- [X] CSD median averaging calculation
- [X] LHO Cross-correlation plot

- [X] Future work

- [X] Modulations appendix
- [X] Simple michelson example
- [X] Fabry Perot

- [X] ALS COMM measurement 
- [X] RF signal chain calibration

- [X] Uncertainty appendix
- [X] Covariance
- [X] PSD estimator variance for Gaussian noise
- [X] TF uncertainty

Places where things are:
- 3gfn-paper
- o3-commissioning
- darmplant
- IFO/IntensityNoise
- nds2utils

