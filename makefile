# LaTeX Makefile
DATE=date+"%m%d%Y"
PREFIX="" #/Library/TeX/texbin/
FILE=Cahillane_Craig_Thesis
all: $(FILE).pdf

.PHONY: clean

clean:
	rm -rf *.blg
	rm -rf *.out
	rm -rf *.bbl
	rm -rf *.log
	rm -rf *.ind
	rm -rf *.ilg
	rm -rf *.lot
	rm -rf *.lof
	rm -rf *.ind
	rm -rf *.idx
	rm -rf *.aux
	rm -rf *.toc
	rm -rf *.nlo
	rm -rf *.bcf
	rm -rf *.xml
	rm -rf *.fls
	rm -rf *.dvi
	rm -rf *.fdb_latexmk
	rm -rf *.gz
	rm -f ${FILE}.pdf


$(FILE).pdf: *.tex *.bib
	latexmk -pdf $(FILE).tex
