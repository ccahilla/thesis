'''
injection_psds.py

Uses the full model of the interferometer to estimate the arm power uncertainties.

Craig Cahillane
January 12, 2021
'''

#####   Imports   #####
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc

import scipy.signal as sig
from scipy.special import i0, i1 # modified bessel of first kind, zeroth order and first order

mpl.rcParams.update({   'figure.figsize': (12, 9),
                        'text.usetex': True,
                        'font.family': 'serif',
                        # 'font.serif': 'Georgia',
                        # 'mathtext.fontset': 'cm',
                        'lines.linewidth': 2.5,
                        'font.size': 22,
                        'xtick.labelsize': 'large',
                        'ytick.labelsize': 'large',
                        'legend.fancybox': True,
                        'legend.fontsize': 18,
                        'legend.framealpha': 0.9,
                        'legend.handletextpad': 0.5,
                        'legend.labelspacing': 0.2,
                        'legend.loc': 'best',
                        'legend.columnspacing': 2,
                        'savefig.dpi': 80,
                        'pdf.compression': 9})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])



#####   Functions   #####
def calc_all_csds(x, y, fs=1.0, window='hann', nperseg=None, noverlap=None, nfft=None,
        detrend='constant', return_onesided=True, scaling='density', axis=-1):
    '''
    calculates the cross spectral density from data arrays x and y.
    Copied docstring from scipy.signal._spectral_helper() function.

    The windows are not averaged over; 
    the result from each window is returned.

    Parameters
    ---------
    x : array_like
        Array or sequence containing the data to be analyzed.
    y : array_like
        Array or sequence containing the data to be analyzed. 
    fs : float, optional
        Sampling frequency of the time series. Defaults to 1.0.
    window : str or tuple or array_like, optional
        Desired window to use. If `window` is a string or tuple, it is
        passed to `get_window` to generate the window values, which are
        DFT-even by default. See `get_window` for a list of windows and
        required parameters. If `window` is array_like it will be used
        directly as the window and its length must be nperseg. Defaults
        to a Hann window.
    nperseg : int, optional
        Length of each segment. Defaults to None, but if window is str or
        tuple, is set to 256, and if window is array_like, is set to the
        length of the window.
    noverlap : int, optional
        Number of points to overlap between segments. If `None`,
        ``noverlap = nperseg // 2``. Defaults to `None`.
    nfft : int, optional
        Length of the FFT used, if a zero padded FFT is desired. If
        `None`, the FFT length is `nperseg`. Defaults to `None`.
    detrend : str or function or `False`, optional
        Specifies how to detrend each segment. If `detrend` is a
        string, it is passed as the `type` argument to the `detrend`
        function. If it is a function, it takes a segment and returns a
        detrended segment. If `detrend` is `False`, no detrending is
        done. Defaults to 'constant'.
    return_onesided : bool, optional
        If `True`, return a one-sided spectrum for real data. If
        `False` return a two-sided spectrum. Defaults to `True`, but for
        complex data, a two-sided spectrum is always returned.
    scaling : { 'density', 'spectrum' }, optional
        Selects between computing the cross spectral density ('density')
        where `Pxy` has units of V**2/Hz and computing the cross
        spectrum ('spectrum') where `Pxy` has units of V**2, if `x`
        and `y` are measured in V and `fs` is measured in Hz.
        Defaults to 'density'
    axis : int, optional
        Axis along which the FFTs are computed; the default is over the
        last axis (i.e. ``axis=-1``).

    Returns
    -------
    freqs : ndarray
        Array of sample frequencies.
    tt : ndarray
        Array of times corresponding to each data segment
    Sxys : ndarray
        2D array of cross spectral densities for each window


    '''
    if np.all(x == y):
        same_data = True
    else:
        same_data = False

    # Sanitize nperseg input
    if nperseg is not None:  # if specified by user
        nperseg = int(nperseg)
        if nperseg < 1:
            raise ValueError('nperseg must be a positive integer')
    else:
        nperseg = x.shape[-1]

    # Sanitize nfft input
    if nfft is None:
        nfft = nperseg
    elif nfft < nperseg:
        raise ValueError('nfft must be greater than or equal to nperseg.')
    else:
        nfft = int(nfft)

    # calculate the FFTs of each data stream, with flag mode='complex'
    # Fxx and Fyy are the short time Fourier transforms
    freqs, tt, Fxx = sig.spectrogram(x, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap,
                nfft=nfft, detrend=detrend, return_onesided=return_onesided,
                scaling=scaling, axis=axis, mode='complex')
    _, _, Fyy = sig.spectrogram(y, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap,
                nfft=nfft, detrend=detrend, return_onesided=return_onesided,
                scaling=scaling, axis=axis, mode='complex')

    Sxys = np.conjugate(Fxx) * Fyy
    if return_onesided:
        if nfft % 2:
            Sxys[..., 1:] *= 2 # double because we want a one-sided csd from the stfts
        else:
            # Last point is unpaired Nyquist freq point, don't double
            Sxys[..., 1:-1] *= 2

    if same_data: # return real PSD
        Sxys = np.abs(Sxys)

    return freqs, tt, Sxys

def rice_distribution(rr, mu2, sigma2):
    '''Returns a rice distribution given the line and noise parameters.
    Inputs:
    rr      = rice distribution random variable parameter
    mu2     = line power in V^2/Hz, equal to rms V^2 line power divided by equivalent noise bandwidth
    sigma2  = Gaussian noise power in V^2/Hz     (PSD with no line = 2*sigma2)
    '''
    rice = rr/sigma2 * np.exp( -(rr**2 + mu2) / (2 * sigma2) ) * i0(rr * np.sqrt(mu2) / sigma2)
    return rice

def call_rice_distribution(rr_max, line_power, noise_power_density, binwidth, num_points=1000):
    '''calls the rice_distribution() function which describes the amplitude spectral density of lines,
    collects more natural units of line_power and noise_power, 
    and tunes them for use on rice_distribution().
    Inputs:
    rr_max      = maximum we want rr to go up to
    line_power  = power in the calibration line in V^2 rms
    noise_power_density = power in the Gaussian noise in V^2/Hz
    binwidth    = frequency resolution of the PSD, frequency binwidth in Hz
    num_points  = number of points in the calling vector

    Outputs:
    rr   = sample vector (xaxis)
    rice = rice distribution on rr with mu2 and sigma2 parameters
    '''
    mu2 = line_power / binwidth
    sigma2 = noise_power_density / 2 # divide by two because Gaussian noise power = 2 sigma^2

    rr = np.linspace(0, rr_max, num_points)

    rice = rice_distribution(rr, mu2, sigma2)
    rice_mean = rice_distribution_mean(mu2, sigma2)

    rice_rms = rice_distribution_rms(mu2, sigma2)

    return rr, rice, rice_mean, rice_rms

def rice_distribution_mean(mu2, sigma2):
    '''Returns a rice distribution mean given the line and noise parameters.
    Inputs:
    mu2     = line power in V^2/Hz, equal to rms V^2 line power divided by equivalent noise bandwidth
    sigma2  = Gaussian noise power in V^2/Hz     (PSD with no line = 2*sigma2)
    '''
    rice_mean = np.sqrt(np.pi/2) / (2 * np.sqrt(sigma2)) * np.exp( -mu2 / (4 * sigma2) ) \
                * ( (mu2 + 2 * sigma2) * i0( mu2 / (4 * sigma2) ) + mu2 * i1( mu2 / (4 * sigma2) ) )
    return rice_mean

def rice_distribution_var(mu2, sigma2):
    '''Returns a rice distribution variance given the line and noise parameters.
    Inputs:
    mu2     = line power in V^2/Hz, equal to rms V^2 line power divided by equivalent noise bandwidth
    sigma2  = Gaussian noise power in V^2/Hz     (PSD with no line = 2*sigma2)
    '''
    rice_mean = rice_distribution_mean(mu2, sigma2)
    rice_var = mu2 + 2 * sigma2 - rice_mean**2
    return rice_var

def rice_distribution_rms(mu2, sigma2):
    '''Returns a rice distribution root mean square given the line and noise parameters.
    Inputs:
    mu2     = line power in V^2/Hz, equal to rms V^2 line power divided by equivalent noise bandwidth
    sigma2  = Gaussian noise power in V^2/Hz     (PSD with no line = 2*sigma2)
    '''
    rice_rms = np.sqrt( mu2 + 2 * sigma2 )
    return rice_rms

#####   Parameters   #####
avg = 100000

nperseg = 1e2                   # number of samples in a single fft segment

N = int( (avg + 1) * nperseg )         # number of samples
fs = 1e3                        # Hz, sampling frequency, samples/second
total_time = N / fs             # seconds, total time
times = np.linspace(0, total_time, N)


noverlap = 0 #nperseg // 2      # 50% overlap

binwidth = fs / nperseg
overlap = noverlap / nperseg

# Define Gaussian noise
noise_power_density = 1e-3      # power density in V**2/Hz.  Should show up in the power spectral density.
noise_power = noise_power_density * fs / 2              # total power in the noise spectrum in V**2.  
                                                        # Equal to the variance of the gaussian noise. 

a = np.random.normal(scale=np.sqrt(noise_power), size=N) # sqrt(noise_power) = sigma on guassian noise

# Define calibration lines
cal_line_power_1 = 1e0  # V^2, volts here are rms
cal_line_amp_1 = np.sqrt(2 * cal_line_power_1)  # V_pk, need the two for rms to peak conv
cal_line_freq_1 = 100    # Hz
cal_line_phase_1 = 0    # rads
cal_line_1 = cal_line_amp_1 * np.sin(2 * np.pi * cal_line_freq_1 * times + cal_line_phase_1)

cal_line_power_2 = 1e-1  # V^2 
cal_line_amp_2 = np.sqrt(2 * cal_line_power_2)  # V_pk, need the two for rms to peak conv
cal_line_freq_2 = 200       # Hz
cal_line_phase_2 = np.pi/3  # rads
cal_line_2 = cal_line_amp_2 * np.sin(2 * np.pi * cal_line_freq_2 * times + cal_line_phase_2)

cal_line_power_3 = 1e-2  # V^2 
cal_line_amp_3 = np.sqrt(2 * cal_line_power_3)  # V_pk, need the two for rms to peak conv
cal_line_freq_3 = 300           # Hz
cal_line_phase_3 = 2*np.pi/5    # rads
cal_line_3 = cal_line_amp_3 * np.sin(2 * np.pi * cal_line_freq_3 * times + cal_line_phase_3)

cal_line_power_4 = 1e-3  # V^2 
cal_line_amp_4 = np.sqrt(2 * cal_line_power_4)  # V_pk, need the two for rms to peak conv
cal_line_freq_4 = 400           # Hz
cal_line_phase_4 = 4*np.pi/5    # rads
cal_line_4 = cal_line_amp_4 * np.sin(2 * np.pi * cal_line_freq_4 * times + cal_line_phase_4)

cal_line_powers = np.array([
    cal_line_power_1,
    cal_line_power_2,
    cal_line_power_3,
    cal_line_power_4,
])

# Summed noise
x = a + cal_line_1 + cal_line_2 + cal_line_3 + cal_line_4

# Take spectral densities
ff, tt, Zxxs = calc_all_csds(x, x, fs, nperseg=nperseg, noverlap=noverlap, window='boxcar')
Zxx = Zxxs.mean(axis=-1)

ff, tt, Z11s = calc_all_csds(cal_line_1, cal_line_1, fs, nperseg=nperseg, noverlap=noverlap, window='boxcar')
Z11 = Z11s.mean(axis=-1)

ff, tt, Z22s = calc_all_csds(cal_line_2, cal_line_2, fs, nperseg=nperseg, noverlap=noverlap, window='boxcar')
Z22 = Z22s.mean(axis=-1)

ff, tt, Z33s = calc_all_csds(cal_line_3, cal_line_3, fs, nperseg=nperseg, noverlap=noverlap, window='boxcar')
Z33 = Z33s.mean(axis=-1)

ff, tt, Z44s = calc_all_csds(cal_line_4, cal_line_4, fs, nperseg=nperseg, noverlap=noverlap, window='boxcar')
Z44 = Z44s.mean(axis=-1)

# Find peak heights
idx1 = np.argwhere(ff >= cal_line_freq_1)[0,0]
idx2 = np.argwhere(ff >= cal_line_freq_2)[0,0]
idx3 = np.argwhere(ff >= cal_line_freq_3)[0,0]
idx4 = np.argwhere(ff >= cal_line_freq_4)[0,0]

idxs = np.array([
    idx1,
    idx2,
    idx3,
    idx4,
])

power_bias_1 = Zxx[idx1] / Z11[idx1]
power_bias_2 = Zxx[idx2] / Z22[idx2]
power_bias_3 = Zxx[idx3] / Z33[idx3]
power_bias_4 = Zxx[idx4] / Z44[idx4]

print()
print(f'ff[idx1] = {ff[idx1]}')
print(f'Zxx[idx1] = {Zxx[idx1]}')
print(f'Z11[idx1] = {Z11[idx1]}')
print(f'power_bias_1 = {power_bias_1}')
print()
print(f'ff[idx2] = {ff[idx2]}')
print(f'Zxx[idx2] = {Zxx[idx2]}')
print(f'Z22[idx2] = {Z22[idx2]}')
print(f'power_bias_2 = {power_bias_2}')
print()
print(f'ff[idx3] = {ff[idx3]}')
print(f'Zxx[idx3] = {Zxx[idx3]}')
print(f'Z33[idx3] = {Z33[idx3]}')
print(f'power_bias_3 = {power_bias_3}')
print()
print(f'ff[idx4] = {ff[idx4]}')
print(f'Zxx[idx4] = {Zxx[idx4]}')
print(f'Z44[idx4] = {Z44[idx4]}')
print(f'power_bias_4 = {power_bias_4}')

print()

# Get histogram data for fitting a rice distribution to the power spectral density results at the cal lines
zxxs = np.sqrt(Zxxs) # get ASDs from PSDs

zxx = np.sqrt(Zxx) # get ASD from PSD
z11 = np.sqrt(Z11)
z22 = np.sqrt(Z22)
z33 = np.sqrt(Z33)
z44 = np.sqrt(Z44)

labels = np.array([
    'cal line 1',
    'cal line 2',
    'cal line 3',
    'cal line 4',
])
colors = np.array([
    'C1',
    'C2',
    'C3',
    'C4',
])

data_dict = {}
for label, idx, cal_line_power, color in zip(labels, idxs, cal_line_powers, colors):
    ff_idx = ff[idx]
    asds = zxxs[idx, :]

    rr_max = np.max(asds)

    SNR = cal_line_power / (binwidth * noise_power_density)

    rr, rice, rice_mean, rice_rms = call_rice_distribution(rr_max, cal_line_power, noise_power_density, binwidth)

    mean = np.mean(asds)
    var = np.var(asds)
    rms = np.sqrt( np.mean(asds**2) )
    
    second_moment = np.sum((asds)**2) / avg # noncentral moments
    fourth_moment = np.sum((asds)**4) / avg # noncentral moments

    mu4 = 2*second_moment**2 - fourth_moment

    cal_line_power_estimate = binwidth * np.sqrt( mu4 )

    print()
    print(f'label = {label}')
    print(f'cal_line_power          = {cal_line_power} V^2')
    print(f'cal_line_power_estimate = {cal_line_power_estimate} V^2')

    data_dict[label] = {}
    data_dict[label]['ff_idx'] = ff_idx
    data_dict[label]['cal_line_power'] = cal_line_power
    data_dict[label]['noise_power_density'] = noise_power_density
    data_dict[label]['binwidth'] = binwidth
    data_dict[label]['SNR'] = SNR

    data_dict[label]['asds'] = asds
    data_dict[label]['rice'] = rice
    data_dict[label]['rr'] = rr
    data_dict[label]['rice_mean'] = rice_mean
    data_dict[label]['rice_rms'] = rice_rms

    data_dict[label]['color'] = color


# rice1 = rice_distribution


#####   Figures   #####
# PSD comparison
fig, (s1) = plt.subplots(1)

s1.semilogy(ff, np.abs(Zxx), alpha=0.8, label=r'PSD with calibration lines $\langle z, z \rangle_{cal}$')

s1.semilogy(ff, np.abs(Z11), alpha=0.8, label=r'cal line 1')
s1.semilogy(ff, np.abs(Z22), alpha=0.8, label=r'cal line 2')
s1.semilogy(ff, np.abs(Z33), alpha=0.8, label=r'cal line 3')
s1.semilogy(ff, np.abs(Z44), alpha=0.8, label=r'cal line 4')


# s1.set_title(f'Cross spectral density - Averages = {averages}')
s1.set_ylabel(r'PSD [$\mathrm{V}^2/\mathrm{Hz}$]')
s1.set_xlabel('Frequency [Hz]')

s1.set_xlim([ff[1], ff[-1]])
s1.set_ylim([1e-5, 5e-1])

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)

s1.legend()

plot_name = f'injection_psd_{int(avg)}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# ASD comparison
fig, (s1) = plt.subplots(1)

s1.semilogy(ff, np.abs(zxx), alpha=0.8, label=r'ASD with calibration lines $\sqrt{ \langle z, z \rangle_{cal} }$')

s1.semilogy(ff, np.abs(z11), alpha=0.8, label=r'cal line 1, $\mathrm{SNR}$ = ' + f'{data_dict[labels[0]]["SNR"]}')
s1.semilogy(ff, np.abs(z22), alpha=0.8, label=r'cal line 2, $\mathrm{SNR}$ = ' + f'{data_dict[labels[1]]["SNR"]}')
s1.semilogy(ff, np.abs(z33), alpha=0.8, label=r'cal line 3, $\mathrm{SNR}$ = ' + f'{data_dict[labels[2]]["SNR"]}')
s1.semilogy(ff, np.abs(z44), alpha=0.8, label=r'cal line 4, $\mathrm{SNR}$ = ' + f'{data_dict[labels[3]]["SNR"]}')


# s1.set_title(f'Cross spectral density - Averages = {averages}')
s1.set_ylabel(r'ASD [$\mathrm{V}/\sqrt{\mathrm{Hz}}$]')
s1.set_xlabel('Frequency [Hz]')

s1.set_xlim([ff[1], ff[-1]])
s1.set_ylim([1e-3, 1e0])

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)

s1.legend()

plot_name = f'injection_asd_{int(avg)}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Histograms at cal lines
for label in labels:

    ff_idx = data_dict[label]['ff_idx']
    SNR = data_dict[label]['SNR']
    asds = data_dict[label]['asds']
    rice = data_dict[label]['rice']
    rr = data_dict[label]['rr']
    rice_mean = data_dict[label]['rice_mean']
    color = data_dict[label]['color'] 

    fig, (s1) = plt.subplots(1)

    num_bins = 1000
    low_bin  = 0
    high_bin = np.max(asds)
    bins = np.linspace(low_bin, high_bin, num_bins+1)

    NN1, bins, patches = s1.hist(asds, bins=bins, histtype='step', color=color, alpha=0.5, lw=2, density=True, label=label)

    s1.plot(rr, rice, ls='--', color=color, label=f'rice dist, SNR = {SNR:.1f}')

    s1.plot([rice_mean, rice_mean], [0, np.max(rice)], ls=':', color=color, label=f'mean = {rice_mean:.2f}')

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.5)

    s1.legend()

    plot_name = f'line_asdsograms_fcal_{int(ff_idx)}_Hz_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()


# Histograms at cal lines single plot
fig, (s1) = plt.subplots(1)
for label in labels:

    ff_idx = data_dict[label]['ff_idx']
    SNR = data_dict[label]['SNR']
    asds = data_dict[label]['asds']
    rice = data_dict[label]['rice']
    rr = data_dict[label]['rr']
    rice_mean = data_dict[label]['rice_mean']
    rice_rms = data_dict[label]['rice_rms']
    color = data_dict[label]['color'] 

    num_bins = 1000
    low_bin  = 0
    high_bin = np.max(asds)
    bins = np.linspace(low_bin, high_bin, num_bins+1)

    NN1, bins, patches = s1.hist(asds, bins=bins, histtype='step', color=color, alpha=0.5, lw=2, density=True, label=label)

    s1.plot(rr, rice, ls='--', color=color, label=f'rice dist, SNR = {SNR:.1f}')

    s1.plot([rice_rms, rice_rms], [0, np.max(rice)], ls=':', color=color, label=f'rms = {rice_rms:.2f}')


s1.set_ylabel('Probability density')
s1.set_xlabel(r'ASD [$\mathrm{V}/\sqrt{\mathrm{Hz}}$]')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)

s1.legend()

plot_name = f'line_asdsograms_{int(avg)}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()


# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()
