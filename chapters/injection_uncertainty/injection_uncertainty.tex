\chapter{Line injection uncertainty}
\label{chap:injection_uncertainty}

Calibration lines are used in LIGO to monitor the DARM response over time,
and swept sine transfer functions employ a series of lines to measure the relation between two signals.
There are some circumstances where a line lies near or below the the spectral density we want to calibrate,
like driving the photon calibrator above DARM at high frequencies $f > 1~\mathrm{kHz}$.
The SoCal technique may require low lines, and we want to achieve record low uncertainty and bias with those lines.

This appendix will briefly derive and present the Rice distribution which describes a continuous sine wave injection in the presence of Gaussian noise.
Relevant results, including the intrinsic uncertainty and bias of an injection, are presented.
This appendix will rely on derivations from the distributions in Chapter~\ref{chap:probability_distributions}.
The results here are used for the line uncertainty and bias calculated for the SoCal calibration lines in Section~\ref{subsec:simultaneous_oscillator_calibration}.










\section{Basics}
\label{sec:injection_basics}

When injecting a calibration line into a spectral density, there are multiple important considerations that must be made for the accuracy and precision of the measurement.
The first is the injection power.
We want the line to be large enough that it quickly dominates the noise of the spectral density and achieves an acceptable SNR,
but not so large that the response to the line becomes nonlinear, the line starts exhibiting significant spectral leakage, or the actuation range runs out and the interferometer loses lock.
Therefore, some middle ground in the injection power must be found such that the uncertainty and bias levels required are achieved in a timely manner with as low an injection as possible.

First, an important quantity is the \textit{signal-to-noise ratio}, or SNR, of the line with power $P_{cal}$ in the power spectral density $S_P(f)$:
\begin{align}
    \label{eq:snr}
    \mathrm{SNR} = \dfrac{P_{signal}}{P_{noise}} = \dfrac{P_{cal}}{f_b S_P(f)}
\end{align}
where $f_b$ is the frequency binwidth, or frequency resolution. 
$f_b$ is the minimum measurable frequency with the data segment given, and for a single average, is equal to
\begin{align}
    \label{eq:frequency_binwidth}
    f_b = \dfrac{1}{T}
\end{align}
where $T$ is the total time of the signal.

The SNR in Eq.~\ref{eq:snr} can be increased in three ways:
\begin{enumerate}
    \item increase the line power $P_{cal}$,
    \item lower the noise $S_P(f)$,
    \item lower the frequency binwidth $f_b$ by increasing the measurement time $T$
\end{enumerate}
A power spectral density of Gaussian noise like $S_P(f)$ has units $\mathrm{V^2/Hz}$, and remains constant no matter the measurement time length.
A power spectrum $f_b S_P(f)$, on the other hand, has units $\mathrm{V^2}$, and the measurement noise decreases as the same noise power is divided amongst more bins.
The calibration line, however, has infinite frequency precision, or at least is assumed to in Eq.~\ref{eq:snr}.
So as the power spectrum $f_b S_P(f)$ of noise decreases with more time $T$, the calibration line power $P_{cal}$ at exactly the calibration line frequency $f_{cal}$ will remain constant.

For real spectral densities, a window must be used to enforce periodicity of our data.
The scale factor between spectrum and spectral density depends on this window, because the window throws away some of the power in the signal.
The real scale factor is the \textit{equivalent noise bandwidth}, which depends on the frequency binwidth $f_b$.
For the derivation in Section~\ref{sec:injection_distribution}, we assume a boxcar window function, which is no window, so the equivalent noise bandwidth $ = f_b$.
Heinzel Chapter 8 \cite{Heinzel2002} discusses windows and the equivalent noise bandwidth.










\section{Problem setup}
\label{sec:injection_problem_setup}

\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/injection_uncertainty/figures/injection_psds/injection_asd_100000.pdf}
    \caption[Calibration lines in a spectral density]{
        Four calibration lines $f_1, f_2, f_3, f_4$ summed into an amplitude spectral density $\sqrt{S_P(f)}$.
        The high SNR line $f_1$ shows in the sum spectral density with negligible noise.
        The $\mathrm{SNR} \sim 1$ line $f_3$ shows weakly in the sum spectrum, 
        with a significant bias between the line peak and sum peak and noise causing fluctuations on the peak height.
        To produce this plot, 100000 ASDs were taken using a boxcar window function and averaged together.
        The frequency binwidth $f_b = 10~\mathrm{Hz}$.
    }
    \label{fig:calibration_lines_in_spectral_density} 
\end{figure}

We would like to know the uncertainty and bias associated with calibration lines.

When we measured a calibration line in a spectral density, the Gaussian noise power of the density sums with the power in the line:
\begin{align}
    \label{eq:measured_power}
    P_{meas} = P_{cal} + f_b S_P(f_{cal}).
\end{align}
If the SNR is high, then $P_{meas} \approx P_{cal}$ and the effect of noise is negligible.
If the SNR is low, then noise contributes significantly to the calibration line, and produces a bias.
Figure~\ref{fig:calibration_lines_in_spectral_density} illustrates both the high and low SNR case.










\section{Bias}
\label{sec:injection_bias}

The bias between the measured peak height and the actual calibration line peak falls out of Eq.~\ref{eq:measured_power}:
\begin{align}
    \label{eq:measured_calibration_line_bias}
    \dfrac{P_{meas}}{P_{cal}} &= 1 + \dfrac{f_b S_P(f_{cal})}{P_{cal}} = 1 + \dfrac{1}{\mathrm{SNR}}\\
    \rightarrow b &= \dfrac{1}{\mathrm{SNR}}
\end{align}
So even with an SNR of 100, you will expect a 1\% bias in the calibration line.

The best way to accurately calculate the SNR and bias is to take a long spectral density measurement with no line to measure $S_P(f_{cal})$ very accurately (see Section~\ref{sec:sample_mean_for_psds} and \ref{sec:sample_median_for_psds}).
Then turn on the line, and measure for a time $T = 1/f_b$ seconds such that the minimum acceptable bias is achieved.
Note that if you take several averages $n$, you will have to measure the line for $n T$ seconds.










\section{Distribution}
\label{sec:injection_distribution}

\begin{figure}[hbt!]
    \centering
    \includegraphics[width=0.6\textwidth]{./chapters/injection_uncertainty/figures/injection_with_gaussian_uncertainty.pdf}
    \caption[Calibration line amplitude vs Gaussian noise]{
        Diagram of a moderate SNR calibration line $V_{cal}$ with Gaussian noise in the Fourier domain.
        The noise along the vector will have more of an effect than the noise orthogonal to the line.
        The Gaussian noise is the same as that derived in Section~\ref{sec:gaussian}.
    }
    \label{fig:injection_with_gaussian_uncertainty} 
\end{figure}

The uncertainty in the line height depends on the ratio of Gaussian noise relative to the line amplitude, i.e. the $\mathrm{SNR}$.
Figure~\ref{fig:injection_with_gaussian_uncertainty} shows the Fourier domain for the calibration line amplitude $V_{cal}$ with Gaussian noise.
We will start the derivation with Figure~\ref{fig:injection_with_gaussian_uncertainty}, and rotate our frame by $-\theta$ so the vector is entirely along with the real axis.

First, we recall our independent Gaussian random variables $\mathcal{A}, \mathcal{B}$ from Section~\ref{sec:gaussian} to describe the noise:
\begin{align}
    \label{eq:gaussian_random_variable_recall}
    \mathcal{A}, \mathcal{B} \sim \mathcal{N}(0, \sigma)
\end{align}
where $\sigma$ is the standard deviation.

Next, we write the distribution of $\mathcal{A} + V_{cal}$.
We assume that the line itself is noiseless, yielding
\begin{align}
    \label{eq:gaussian_plus_line}
    \mathcal{A} + V_{cal} \sim \mathcal{N}(\mu, \sigma)
\end{align}
where $\mu$ is the amplitude of $V_{cal}$.

We care about the measured power $P_{meas}$ calculated from these signals, so we define a random variable $\mathcal{Z}$ such that
\begin{align}
    \label{eq:measured_power_random_variable}
    \mathcal{Z} \sim (\mathcal{A} + V_{cal})^2 + \mathcal{B}^2
\end{align}

We recall the zero-mean Gaussian squared $\mathcal{B}^2$ is a general chi-squared distribution with one degree of freedom (Section~\ref{sec:chi_squared}):
\begin{align}
    \label{eq:chi_squared_scaled_recall}
    \mathcal{B}^2 \sim \chi^2_1 &= \dfrac{1}{\sqrt{2 \pi \sigma^2 b}} e^{-\frac{b}{2 \sigma^2}} \qquad b \in [0, \infty)
\end{align}

The nonzero-mean Gaussian squared follows a noncentral chi-squared distribution with one degree of freedom:
\begin{align}
    \label{eq:noncentral_chi_squared_scaled}
    (\mathcal{A} + V_{cal})^2 \sim \chi'^2_1 &= \dfrac{1}{\sqrt{2 \pi \sigma^2 a}} \cosh\left( \dfrac{\mu \sqrt{a}}{\sigma^2} \right) e^{-\frac{(a + \mu^2)}{2 \sigma^2}} \qquad a \in [0, \infty)
\end{align}



\subsection{Power spectral density with line}
\label{subsec:power_spectral_density_with_line}

Now we find the distribution for the power spectral density with a line $\inner{z}{z}_{cal}$, which follows $\mathcal{Z}$.
To find the distribution of $\mathcal{Z}$, we convolve Eqs.~\ref{eq:chi_squared_scaled_recall} and \ref{eq:noncentral_chi_squared_scaled}:
\begin{align}
    \inner{z}{z}_{cal} \sim p(z) &= \int_{-\infty}^{\infty} \chi'^2_1(x) \chi^2_1(z - x) dx \\
                        &= \dfrac{1}{2 \pi \sigma^2} e^{-\frac{(z + \mu^2)}{2 \sigma^2}} \int_{0}^{z} \dfrac{1}{\sqrt{x (z - x)}} \cosh\left( \dfrac{\mu \sqrt{x}}{\sigma^2} \right) dx \\
    \label{eq:power_line_distribution}
    p(z)  &= \dfrac{1}{2 \sigma^2} e^{-\frac{(z + \mu^2)}{2 \sigma^2}} I_0\left( \dfrac{\mu \sqrt{z}}{\sigma^2} \right)
\end{align}
where $I_0$ is the modified Bessel function of the first kind.
Eq.~\ref{eq:power_line_distribution} is the probability distribution for how a line with mean power $P_{cal} = \mu^2 f_b$ appears in a power spectral density bin with mean noise power density $2 \sigma^2$.
If we let $\mu \rightarrow 0$, we recover the usual exponential distribution of the power spectral density from Eq.~\ref{eq:scaled_exponential}.

Note that the units here are still $\mathrm{V^2/Hz}$.
We recall here that $\sigma^2 = \sigma_x^2 / f_s$ where $f_s$ is the sampling frequency and $\sigma_x^2$ is the measured power in $V^2$ of a white noise time-domain signal.
In Eq.~\ref{eq:power_line_distribution}, $\mu^2 = P_{mean}/f_b$ is the line power divided by the equivalent noise bandwidth, which in our case is just $f_b$.

The SNR from Eq.~\ref{eq:snr} can be expressed here as
\begin{align}
    \label{eq:snr_2}
    \mathrm{SNR} = \dfrac{\mu^2}{2 \sigma^2}.
\end{align}



\subsection{Amplitude spectral density with line}
\label{subsec:amplitude_spectral_density_with_line}

\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/injection_uncertainty/figures/injection_psds/line_asdsograms_100000.pdf}
    \caption[Rice distributions of ASDs for different line SNRs]{
        Histograms of amplitude spectral densities at calibration line frequencies vs estimated rice distributions.
        100000 ASDs of the calibration lines plus Gaussian noise from Figure~\ref{fig:calibration_lines_in_spectral_density} are plotted as the histograms.
        The Rice distributions are calculated via Eq.~\ref{eq:amplitude_line_rice_distribution}.
    }
    \label{fig:histogram_asds_vs_rice_distributions} 
\end{figure}


Finally, we also derive the distribution for the amplitude spectral density with a line $\sqrt{ \inner{z}{z}_{cal} }$, which follows $\sqrt{ \mathcal{Z} }$.
This can be done with change of variables $r = \sqrt{z}$, similar to Section~\ref{subsec:amplitude_spectral_densities}, and yields the distribution
\begin{align}
    \label{eq:amplitude_line_rice_distribution}
    \sqrt{ \inner{z}{z}_{cal} } \sim p(r) &= \dfrac{r}{\sigma^2} e^{-\frac{(r^2 + \mu^2)}{2 \sigma^2}} I_0\left( \dfrac{ \mu r }{\sigma^2} \right)
\end{align}
Eq.\ref{eq:amplitude_line_rice_distribution} is the Rice distribution.
The units here are $\mathrm{V/\sqrt{Hz}}$.

If we let $\mu \rightarrow 0$, we recover the usual Rayleigh distribution amplitude spectral density from Eq.~\ref{eq:asd_rayleigh}.
If we let $\mathrm{SNR} > 5$, the Rice distribution is well-approximated by a Gaussian distribution with mean $\mu$ and variance $\sigma^2$.
Figure~\ref{fig:histogram_asds_vs_rice_distributions} shows the Rice distributions of the calibration lines from Figure~\ref{fig:calibration_lines_in_spectral_density}.

The mean of the Rice distribution in Eq.\ref{eq:amplitude_line_rice_distribution} is
\begin{align}
    \label{eq:rice_distribution_mean}
    \lambda &= \sqrt{\dfrac{\pi}{2}} \dfrac{1}{2 \sigma} e^{-\frac{\mu^2}{4 \sigma^2}} \left[ (\mu^2 + 2 \sigma^2) I_0\left( \dfrac{ \mu^2 }{4 \sigma^2} \right) + \mu^2 I_1\left( \dfrac{ \mu^2 }{4 \sigma^2} \right) \right]
\end{align}

The variance is 
\begin{align}
    \label{eq:rice_distribution_variance}
    s^2 &= \mu^2 + 2 \sigma^2 - \lambda^2
\end{align}
where $\lambda$ is the mean from \ref{eq:rice_distribution_mean}.

The root mean square is 
\begin{align}
    \label{eq:rice_distribution_root_mean_square}
    \psi &= \sqrt{ \mu^2 + 2 \sigma^2 }
\end{align}

The noncentral fourth-order moment is
\begin{align}
    \label{eq:rice_distribution_noncentral_fourth_order_moment}
    \mu_4 &= \int_0^\infty r^4 p(r) dr = \mu^4 + 8 \mu^2 \sigma^2 + 8 \sigma^4 
\end{align}



\subsection{Line height estimation and uncertainty}
\label{subsec:line_height_estimation}

There are a couple of ways to estimate the true calibration line height $\sqrt{f_b} \mu$,
including the method of moments and numerical maximum likelihood estimation.
If the $\mathrm{SNR} > 5$, the Rice distribution approximates to a Gaussian, and the method of moments works well to quickly estimate the line height and its uncertainty from the data.

However, some of these techniques fall apart in the regime of low SNR, where the uncertainty would allow unphysical estimates like negative line amplitudes.
In those low SNR cases, it will be best to implement a model estimate which allows priors to be incorporated.



\subsubsection{Estimation via mean squared error}
\label{subsubsec:estimation_via_mean_squared_error}


The first and most straightforward way to estimate the line amplitude $\mu$ is to take the measured line height and subtract the measured noise.

Quantitatively, we find the sample noncentral second order moment $\hat{\psi}^2$, also known as the mean squared error, from the ASDs.
The root mean square $\hat{\psi}$ is the value naturally returned by Welch's method of ASD estimation.
Then combine $\hat{\psi}^2$ with a ``quiescent'' measurement taken with the line off to estimate the average noise $\hat{S}_P(f) = 2 \hat{\sigma}^2$:
\begin{align}
    \label{eq:first_line_height_estimate}
    \hat{\mu} = \sqrt{ \hat{\psi}^2 - 2 \hat{\sigma}^2 }.
\end{align}

To find the uncertainty in the estimate in Eq.~\ref{eq:first_line_height_estimate}, we combine the uncertainty in the mean squared error estimate with the uncertainty in the noise estimate.
The variance in the noncentral second order sample moment $\hat{\psi}^2$ estimate is %\cite{Cho2003}
\begin{align}
    \label{eq:second_order_moment_uncertainty}
    \mathrm{Var}\left[ \hat{\psi}^2 \right] &= \dfrac{1}{n} \left( \mu_4 - \psi^4 \right)
\end{align}
where $n$ is the number of ASDs taken, and
$\mu_4$ is the noncentral fourth-order sample moment.
Eq.~\ref{eq:second_order_moment_uncertainty} is similar to the expression for variance on the sample variance $\mathrm{Var}[\hat{s}^2]$ \cite{Cho2003}.

The variance in the noise comes from the pure noise measurement with the line off.
The variance in the \textit{power} spectral density estimate $2 \hat{\sigma}^2$ comes from Eq.~\ref{eq:psd_sample_mean_variance}:
\begin{align}
    \label{eq:power_spectral_density_variance}
    \mathrm{Var}\left[ \hat{S}_P(f) \right] = \mathrm{Var}\left[ 2 \hat{\sigma}^2 \right] = \dfrac{4 \sigma^4}{n}
\end{align}

Propagating these uncertainties forward to $\mu$ via the usual Taylor series expansion method:
\begin{align}
    \label{eq:mu_variance}
    \mathrm{Var}\left[ \hat{\mu} \right]    &= \dfrac{\sigma^2 \left(\mu^2 + 2 \sigma^2 \right)}{\mu^2 n} \\
    \label{eq:mu_variance_snr}
                                            &= \dfrac{\sigma^2}{n} \left( 1 + \dfrac{1}{\mathrm{SNR}} \right)
\end{align}
Expressing Eq.~\ref{eq:mu_variance_snr} in terms of relative uncertainty using Eqs.~\ref{eq:snr_2}:
\begin{align}
    \label{eq:mu_relative_variance_snr}
    \nonumber \dfrac{ \mathrm{Var}\left[ \hat{\mu} \right] }{ \hat{\mu}^2 }   &= \dfrac{ \dfrac{\sigma^2}{n} \left( 1 + \dfrac{1}{\mathrm{SNR}} \right) }{ 2 \sigma^2 \mathrm{SNR} } \\
                                                                    &= \dfrac{ 1 }{ 2 n \mathrm{SNR} } \left( 1 + \dfrac{1}{\mathrm{SNR}} \right)
\end{align}

The variance of our estimate of $\hat{\mu}$ decreases linearly with the number of averages $n$.
The absolute variance in Eq.~\ref{eq:mu_variance_snr} also flattens out starting at around $\mathrm{SNR} = 1$: absolute uncertainty does not improve much above $\mathrm{SNR} = 5$, but gets much worse for $\mathrm{SNR} < 1$.
This suggests that above an SNR of around 5, the experimenter may be better off using data to achieve more averages $n$ rather than increasing the SNR by integrating over more time to reduce the frequency binwidth $f_b$.
Figure~\ref{fig:line_height_variance_via_method_of_moments} plots the variance of the line estimate as a function of SNR.

\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/injection_uncertainty/figures/method_of_moments_variance/cal_line_height_vs_variance.pdf}
    \caption[Method of moments line height estimate variance]{
        Variance of the calibration line height estimate $\mathrm{Var}\left[ \hat{\mu} \right]$ via the method of moments in Eq~\ref{eq:mu_variance_snr}.
        The variance in the line height estimate flattens out at high SNR as more line power does not help resolve versus the Gaussian noise further.
        In this plot, $n = 100$ and $S_P(f) = 2 \sigma^2 = 1~\mathrm{V^2/Hz}$.
    }
    \label{fig:line_height_variance_via_method_of_moments} 
\end{figure}

Briefly, we overview the regime of unphysical uncertainty.
An unphysical result is when the uncertainty of the line height is greater than the line height itself, potentially yielding negative line heights.
We can always take more ASDs $n$ to reduce uncertainty to a physical regime, but increasing the SNR helps faster.
This can be described in terms of SNR using Eqs.~\ref{eq:snr_2} and \ref{eq:mu_variance}
\begin{align}
    \label{eq:unphysical_line_height}
    0 &< \hat{\mu} - \sqrt{ \mathrm{Var}\left[ \hat{\mu} \right] } \\
    0 &< \sqrt{ 2 \sigma^2 \mathrm{SNR} } - \sqrt{\dfrac{\sigma^2}{n} \left( 1 + \dfrac{1}{\mathrm{SNR}} \right) } \\
    \rightarrow n &> \dfrac{\mathrm{SNR} + 1}{2 \mathrm{SNR}^2}
\end{align}
If $\mathrm{SNR} \geq 1$, then $n > 1$ is sufficient for a physical $1\sigma$ uncertainty.
If $\mathrm{SNR} < 1$, then $n > 1/(2 \mathrm{SNR}^2)$ is required.

This method is simple, works well with high SNR lines, and the uncertainties easily calculated and propagated from the noncentral sample moments.
However, this method requires two measurements, one each for the line on and off.
The state of the interferometer cannot be changing much during this time.
Also, the uncertainties are not guaranteed to yield physical results in the very low SNR regime.
More samples may be taken to reduce uncertainty in the line height to be physical.
The sample moment estimates are not robust to glitches and non-Gaussian noise.



\subsubsection{Estimation via fourth order moment}
\label{subsubsec:estimation_via_fourth_order_moment}

If one does not want to perform a ``noise-only'' measurement with the calibration line off,
the noncentral fourth-order sample moment $\hat{\mu}_4$ may be used to estimate the line amplitude $\hat{\mu}$:
\begin{align}
    \label{eq:fourth_order_estimation_of_cal_line_amplitude}
    \hat{\mu} = \left( \hat{\mu}_4 - 2 \hat{\psi}^4 \right)^{\frac{1}{4}}
\end{align}

The uncertainties in Eq.~\ref{eq:fourth_order_estimation_of_cal_line_amplitude} can be propagated similarly to the previous section.
The variances associated with Eq.~\ref{eq:fourth_order_estimation_of_cal_line_amplitude} depend on the noncentral eighth-order sample moments. 
Analysis suggests the final variance in $\hat{\mu}$ from Eq.~\ref{eq:fourth_order_estimation_of_cal_line_amplitude} is $\sim100\times$ higher than Eq.~\ref{eq:mu_variance}.
Again, the uncertainties are not guaranteed to yield physical results in the very low SNR regime, and the sample moment estimates are still susceptible to glitches.



% $\mu_4$ is similar to Eq.~\ref{eq:rice_distribution_noncentral_fourth_order_moment} but with the mean subtracted from $r$, such that $r^4 \rightarrow (r - \lambda)^4$ under the integrand.
% The biased sample variance $\hat{s}^2$ and biased sample central fourth-order moment $\hat{\mu}_4$ can be estimated from the ASDs $a_i$:
% \begin{align}
%     \label{eq:biased_central_moments}
%     \hat{s}^2 = \dfrac{1}{n} \displaystyle \sum_{i=1}^n (a_i - \lambda)^2, \qquad \hat{\mu}_4 = \dfrac{1}{n} \displaystyle \sum_{i=1}^n (a_i - \lambda)^4.
% \end{align}
% These biased estimates of the moments are generally fine for our purposes above $n > 100$, so the variance on the variance will be $<1\%$.
% Unbiased central moments estimates of $\hat{s}^2$ and $\hat{\mu}_4$ are called h-statistics.

% The root mean square $\psi$ is most useful as it is the quadrature sum of the measured amplitude of the line plus the noise.
% This value can be directly related to the measured power $P_{meas}$ and true line power $P_{cal}$ using the bias from Eq.~\ref{eq:measured_calibration_line_bias}:
% \begin{align}
%     \label{eq:measured_power_via_rice_rms}
%     P_{meas} &= f_b \psi^2 \\ 
%     P_{cal} &= \dfrac{ f_b \psi^2 }{ 1 + \frac{1}{\mathrm{SNR}} }
% \end{align}

% Using the Rice distribution, we can generally estimate the parameter $\mu$, which corresponds to our line amplitude.

% \subsubsection{Estimation via Gaussian approximation}
% \label{subsubsec:estimation_via_gaussian_approximation}

% In the high SNR case $\mathrm{SNR} > 5$, the mean of the Rice distribution is our line amplitude $\mu$, with variance $\sigma^2$.
% The sample mean of the Gaussian is well known to have a variance $\sigma^2_\mu$ equal to
% \begin{align}
%     \label{eq:sample_mean_of_gaussian}
%     \sigma^2_\mu = \dfrac{ \sigma^2 }{ n }
% \end{align}
% where $n$ is the number of samples taken.
% In Figure~\ref{fig:histogram_asds_vs_rice_distributions} $\mu$ is calculated from $n = 100000$ averages.

% % Ultimately the quantity we care about is not $\mu$, but the actual line amplitude $\mu \sqrt{f_b}$.

% One usual method of probability distribution parameter estimation, maximum likelihood estimation, 
% is not simple to do analytically here because of the Bessel function in Eq.~\ref{eq:amplitude_line_rice_distribution} \cite{Sijbers2001}.
% Parameter estimation can still be done numerically, using simple MCMC methods is an easy way to incorporate priors into our estimates and rigorously extract the uncertainty in those parameters.






% \section{Results}
% \label{sec:injection_results}










\section{Future work}
\label{sec:future_work}

The above estimation of calibration line height and uncertainty in the presence of Gaussian noise is useful for quantifying the levels SNR and number of averages required for reasonable uncertainty.
For situations where high SNR lines may not be feasible, an MCMC which incorporates priors about the Rice distribution parameters may be necessary.

Future work in this area include examining the uncertainty statistics using medians rather than mean moments.
Another useful tool would be code to perform rigorous Bayesian model selection between Rayleigh and Rice distributions to decide whether a line is present or not given the data.