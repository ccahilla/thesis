\chapter{Power spectral density rejection for glitch gating}
\label{chap:psd_rejection}

Loud, transient glitches in Advanced LIGO data is a problem for spectral analysis.
The worst effect glitches can have is when they occur during gravitational wave events, spoiling astrophysical data, although the effects can be partially mitigated \cite{Pankow2018, Powell2018}.
Other issues glitches include causing locklosses, 
spoiling detector backgrounds \cite{Zackay2019, Godwin2020}, 
interfering with search pipelines and astrophysical parameter estimation \cite{Cornish2015, Nitz2018, Davis2020}, 
and spoiling spectral density estimates \cite{Littenberg2015}.
This chapter will focus on improving spectral density and transfer function estimates by throwing out glitchy data using the statistics of PSDs.

% In chapter~\ref{chap:probability_distributions}, we discussed in detail the pros and cons of median-averaging for PSD and CSD estimates.
% In chapter~\ref{chap:transfer_function_estimates}, we reviewed the estimation procedure and limits for several spectral quantities, including PSDs, CSDs, coherence, and transfer functions.



\section{Method}
\label{sec:psd_rejection_method}

Here, we will briefly examine ``PSD rejection'' which avoids the difficulties associated with median-averaging while still removing glitches from the PSD estimation process.
The procedure is
\begin{enumerate}
    \item Compute all PSDs as normal for Welch's method, say we have $n$ samples
    \item Choose a frequency bin $f_0$ that is sensitive to glitches (in LIGO this is the 20-40 Hz range),
    \item Calculate the median $\rho$ of all the PSDs in frequency bin $f_0$,
    \item Choose a rejection threshold $r$,
    \item Reject the PSDs with values higher than the threshold,
    \item Use mean-averaging on the rest.
\end{enumerate}

When we calculate the median, we completely characterize the Gaussian noise distribution we care about.
We know that a PSD follows an exponential distribution, with a median $\rho = \log(2) \lambda$.
By using the cumulative distribution function of the exponential
\begin{align}
    \label{eq:exponential_cdf2}
    F(x) = 1 - e^{-\frac{x}{\lambda}},
\end{align}
we can known exactly how many PSDs we might expect to reject given our rejection threshold $r$.

For instance, suppose we have $n = 1000$ PSDs. 
If we choose $r = 10 \rho$, then $F(r) = 0.9990$, so we might expect to reject $n (1 - F(r)) \approx 1$ PSD, if there were no glitchy PSDs in the sample.
If we reject much more than one, we can safely assume at least some of those PSDs were taken during a glitch.



\section{Discussion}
\label{sec:psd_rejection_discussion}

One might ask, ``if I've already characterized my Gaussian noise using the the median, why bother with the rejection?''
Indeed, if the PSD is the sole result one cares about, the analysis can stop there, simply correcting from the mean-to-median PSD bias.
The above process is most useful for CSD and TF rejection.
The individual signals comprising the CSDs may have their PSDs estimated first, 
and the CSDs associated with the glitchy PSDs can be removed from the analysis before computing the final mean-averaged CSD.

This method works very well for the short-duration, loud, and frequent glitches seen in LIGO data.
It avoids the difficulties of median-averaging for CSDs and TFs,
and avoids the difficulties of time-domain glitch gating, including data storage costs and spectral leakage due to gating.

This method requires the median to be a decent characterization of the PSD distribution, and so needs a large amount of data, 
and can miss some quiet glitches that do not exceed the rejection threshold in the bin we've chosen,
and can bias the mean-averaged result down if the rejection threshold is too low, cutting off the upper tail of the distribution.



\section{Example}
\label{sec:psd_rejection_example}

PSD rejection was used to simplify the analysis of correlated noise in the interferometer, as explored in Chapter~\ref{chap:correlated_noise}.
For the correlated noise measurement, very long stretches of glitchy data is required to resolve the correlated noise.
In the example from Chapter~\ref{chap:correlated_noise}, more than 9 hours of data was taken, yielding 67156 sample PSDs and CSDs.
At frequency $f_0 = 40~\mathrm{Hz}$, I chose a rejection threshold $r = 14 \rho$, which should have rejected $67156 (1 - F(r)) \approx 4$ CSDs.
In fact, 53 CSDs were rejected, meaning about 49 glitches were removed from the analysis.
