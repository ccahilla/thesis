\chapter{Introduction}
\label{chap:introduction}

In the early 20$^{\mathrm{th}}$ century, Albert Einstein revolutionized physics with the theories of special and general relativity.
Einstein cut ideas like absolute universal length, absolute universal time, and simultaneity in favor of a universal speed of light.
Relativity unified space and time into a singular ``spacetime.'' 
Travel through space and time is was no longer independent, and observers traveling through space relative to one another must experience different passage of time.

General relativity redefined gravity as \textit{curvature in spacetime} arising from the presence of mass and energy within that spacetime.
The ``force'' of gravity between two bodies was not a force at all, but two objects following a ``straight line'', or \textit{geodesic}, through a curved spacetime.
General relativity resolved issues with Newtonian gravity, including correctly predicting the precession of Mercury's orbit and gravitational lensing by the sun.
The development of general relativity revolutionized physics and astrophysics, provided a new framework for understanding the universe on a large scale, and kicked off the field of cosmology.

One key prediction of general relativity was the existence of waves in spacetime, known as gravitational waves.
Gravitational waves were a natural consequence of Einstein's equations describing spacetime curvature,
but Einstein predicted these waves were far too weak to ever be detected by humanity.

In 2015, Advanced LIGO made the first-ever detection of gravitational waves from a binary black hole merger \cite{GW150914}.
Since then, Advanced LIGO's sensitivity to gravitational waves has increased even further, resulting in 39 detections in the most recent observing run \cite{SecondCatalogPaper}.

This thesis will focus on the efforts to characterize and improve the sensitivity of the LIGO Hanford Observatory leading up to its third observing run (O3),
with topics in precision detector calibration, noise mitigation, and novel detector measurement techniques.



% \section{Motivation}
% \label{sec:motivation}

\section{What is a gravitational wave?}
\label{subsec:what_is_gravitational_wave}

Gravitational waves are the propagating wave manifestation of a fundamental force of nature.
The ``electric charge'' of gravity is mass, which can only be positive, not both positive and negative.
However, gravity is a much weaker force than electromagnetism,
and can only emit quadrupole radiation, as opposed to electromagnetism's dipole radiation.

A gravitational wave is often described as a ``ripple'' in spacetime.
As heavy objects move through the universe, they interact with spacetime, 
creating curvature such that the motions of nearby objects through spacetime appear distorted.

If two extremely heavy objects begin orbiting one another very quickly, 
spacetime curvature near this orbit becomes extremely strong and changes rapidly.
A significant amount of energy in the fluctuating spacetime propagates away to infinity in the form of oscillating spacetime.
% The amplitude of the wave decreases as it gets further from the source.

In general relativity, Einstein's field equations relate spacetime curvature to the energy and matter residing within that spacetime:
\begin{align}
    \label{eq:einstein_equations}
    G_{\mu\nu} - \Lambda g_{\mu\nu} = \kappa T_{\mu\nu}
\end{align}
where $G_{\mu\nu}$ is the Einstein tensor describing spacetime curvature,
$\Lambda$ is the cosmological constant,
$g_{\mu\nu}$ is the local spacetime metric,
$\kappa = 8 \pi G / c^4$ is the Einstein gravitational constant governing energy coupling to spacetime curvature, and
$T_{\mu\nu}$ is the stress-energy tensor describing the matter and energy within a spacetime.

% Accelerating masses in spacetime will produce changes in spacetime curvature.
% Some of these changes in curvature can propagate to infinity as gravitational waves. 
In the weak-field limit, where there is no matter or energy, the stress-energy tensor $T_{\mu\nu} = 0$.
Then the Einstein equations can be reduced to a wave equation and solved for small perturbations in spacetime.
These solutions to Einstein's equations are known as gravitational waves.

For a wave traveling transverse to the $z$ direction, the gravitational wave tensor $h_{\mu\nu}$ is 
\begin{align}
    \label{eq:gw_tensor}
    h_{\mu\nu} = \begin{pmatrix}
        0 & 0 & 0 & 0 \\
        0 & h_{+} & h_{\times} & 0 \\
        0 & h_{\times} & -h_{+} & 0 \\
        0 & 0 & 0 & 0 
    \end{pmatrix}.
\end{align}
The coefficients $h_{+}$ and $h_{\times}$ correspond to the two polarizations of gravitational waves,
and refer to the way they affect spacetime.

\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/introduction/figures/hplus_gravitational_radiation.pdf}
    \caption[Gravitational wave effect on test masses]{
        Effect of a passing linearly polarized $h_+$ gravitational wave traveling in the $z$ direction on two test masses on the $x$- and $y$-axis.
        In the lab frame, one axis is ``stretched'', the other is ``squeezed'', producing an effective differential length change.
    }
    \label{fig:hplus_gravitational_radiation} 
\end{figure}

The effect of a gravitational wave can be seen in the way it affects the distances of two objects resting in spacetime.
In the lab frame, a gravitational wave can be said to create a length change between any two points in space, 
with the sign of the change depending on the polarization and orientation relative to the wave.
For two points a distance $L$ apart, the length change $\Delta L$ is
\begin{align}
    \label{eq:gw_to_length_tf}
    \Delta L = h L
\end{align}
where $h$ is the gravitational wave \textit{strain}.
Figure~\ref{fig:hplus_gravitational_radiation} shows the differential length change effect of a gravitational wave with strain $h \sim 0.5$ on two tests masses.
This is the principle upon which gravitational wave detectors are based.

Gravitational waves are produced when any masses accelerate through spacetime, like in the orbit between two objects.
In reality, spacetime is a ``stiff'' medium, or gravity is a ``weak'' force: only the most massive objects in the universe can make an appreciable dent in spacetime,
and only the most massive, most energetic orbits can create significant gravitational waves.

Gravitational waves spread from their source over all space, losing amplitude inversely proportional to their distance from their source.
The strongest gravitational waves that reach Earth are all from extremely distant, rare, ultra-powerful astrophysical collisions.

For these reasons, the strongest gravitational waves reaching Earth have a strain on the order of $h \sim 10^{-21}$.
For reference, a human cell is $10^{-4}~\mathrm{m}$, the size of an atom $10^{-10}~\mathrm{m}$, the size of the nucleus of that atom is $10^{-15}~\mathrm{m}$.
Gravitational waves incident on our $L = 4~\mathrm{km}$ long detectors will be $h L \sim 10^{-18}$.
This is why the sensitivity of the Advanced LIGO detectors to gravitational waves are huge technological feat of engineering,
and the gravitational wave data a valuable new font of information on the depths of the universe.


\section{Sources of gravitational waves}
\label{subsec:gravitational_wave_sources}

% Gravitational waves carry information about their sources with them.
Gravitational waves provide a new unique source of information about the darkest, most massive objects, and most energetic events in the universe.
Events normally inaccessible through light, such as binary black hole mergers, supernovae core bounce, or the Big Bang, can be directly observed via GWs. 


Mergers of black hole and neutron star binaries are some of the most powerful events in the universe, 
but are completely invisible to observers on Earth except through the gravitational wave signature they produce \cite{GW150914, GW150914PEPaper, GW150915GRTests}.
The detections of binary black hole mergers are the first direct observational insights into the physics of massive binary systems.
The formation rates of stellar-mass black holes have been more accurately estimated than ever before,
as well as the spin parameters of both the inspiraling and final black holes . 

Binary neutron stars also offer insight into extreme events of spacetime, including the influence of matter on GW emission \cite{PhysRevLett.119.161101, GW170817neutronstarequationofstate, GW170817standardSiren2017}.
Tidal disruption breaks apart the neutron star pair prior to merger, 
causing irregularly in the inspiral and merger than can provide useful information on the type of matter that makes up a neutron star.
For binary neutron stars, multimessenger astronomy has already begun with the detection of prompt electromagnetic followup to a GW merger, 
which proved GWs travel very near to the speed of light.

Unequal-mass binaries consisting of a neutron star and black hole are also possible, and candidates have already been detected \cite{potentialNSBH}.
These are especially interesting because of the orbital precession physics possible, 
the higher-order multipoles of the GWs detected,
and the rates of formation of small black holes and large neutron stars near the so-called ``mass-gap''.

Supernovae are the explosive death of stars about $10\times$ more massive than the sun, and the birth of neutron stars or black holes,
but the mechanism that powers the explosion is not well-understood.
Light from the supernovae comes from the exploding surface of the star,
but the gravitational waves from the supernovae would come from the core, 
and with it valuable information about the formation of the core and the nature of the explosion.

The \textit{stochastic gravitational wave background} is the random noise of the universe.
The stochastic background is formed from the sum of all unresolved binary inspirals in the distant universe.
Cosmic gravitational wave backgrounds have the potential to provide information directly from the Big Bang, 
shortly after the era of inflation when the universe expanded exponentially.
The current limit to the direct observations from the early universe come from the cosmic microwave background, 
which occurred during a era called recombination when the universe cooled enough so the first atoms could form, around 370000~years after the Big Bang.

Some of these phenomena cannot be detected with light, or have questions that observation via light cannot answer.
Gravitational waves offer a new way of observing the universe, 
of listening to the universe by measuring spacetime reverberating with the echoes of unimaginably powerful events from billions of years ago.


\section{Detectors}
\label{subsec:gravitational_wave_detectors}


\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/introduction/figures/future_detector_noise_curves/future_detector_noise_curves.pdf}
    \caption[Noise curves of future gravitational wave detectors]{
        Noise curves of current and future gravitational wave detectors, 
        compared with the characteristic strain of the first GW detection, GW150914.
        The first GW signal measured is shown as the black curve,
        while each of the colored noise curves represent the measured noise of a current detector or projected noise of a future detector.
        The lower the noise, the more sensitive the detector is to GWs.
        By lowering the noise of the detector, a loud signal like GW150914 can be better resolved, and more precise information can be learned from the signal.
        Table~\ref{table:future_detectors_snr} calculates the signal-to-noise ratio (SNR) that a signal like GW150914 would have in each detector.
        Estimated future noise curves from this plot are produced by pygwinc as of January 2021 \cite{gwinc}.
    }
    \label{fig:future_detector_noise_curves}
\end{figure}

\begin{table}
    \caption[SNR of the first gravitational wave detection in future detectors]{
        Signal-to-noise ratio (SNR) the first gravitational wave detection, GW150914, would have had in current and future GW detectors.
        Figure~\ref{fig:future_detector_noise_curves} shows the current and future noise curves beside the characteristic strain of GW150914.
        Future detector noise curves are reported from gwinc \cite{gwinc}.
    }
    \begin{center}
        \begin{tabular}{ l r c } 
            Detector                        & SNR   & SNR ratio / Observing Run 1  \\ \hline
            Advanced LIGO Observing Run 1   & 23    & 1.0   \\
            Advanced LIGO Observing Run 3   & 34    & 1.5   \\
            Advanced LIGO design            & 53    & 2.4   \\
            Advanced LIGO A+ upgrade        & 101   & 4.5   \\
            LIGO Voyager                    & 250   & 11.1  \\
            Cosmic Explorer 1               & 1232  & 54.5  \\
            Cosmic Explorer 2               & 2273  & 100.5 
        \end{tabular}
        \label{table:future_detectors_snr}
    \end{center}
\end{table}


The future of gravitational wave astronomy and astrophysics relies on the continued improvement of the sensitivity of gravitational wave detectors.
Detector scientists, known as commissioners, at the Advanced LIGO detectors are working to achieve the maximum sensitivity possible with the current detectors.
Figure~\ref{fig:future_detector_noise_curves} illustrates the projected improvement of sensitivity for ground-based long-baseline interferometers in the United States.

As sensitivity of detectors is improved, both the rate of detections will increase, and the signal from current detections will be better resolved.
From the increased number of detections, 
we can learn about black hole and neutron star astronomy, including
binary formation rates,
and galaxy formation.

From the clearer signals, 
we can better resolve the physical parameters of the mergers like the masses, spins, distance, orbital plane inclination, and sky location.
Also, we can better test general relativity in the most extreme regions of spacetime where two black holes are merging into one, 
where extremely powerful spacetime curvature itself causes further curvature.
Precision measurements of the Hubble constant are also possible.

With a more sensitive detector, weaker sources of gravitational waves could be detected,
including gravitational waves from a single, rapidly spinning neutron stars,
gravitational waves from exploding supernovae,
or the random background of gravitational waves from binary black holes and neutron stars too far away to resolve individually.


This thesis will focus on efforts to achieve the sensitivity acquired by Advanced LIGO for observing run three (O3),
with topics in precision calibration of the Advanced LIGO detectors' response to gravitational waves, 
characterization of noise sources,
and new techniques for measurements of fundamental quantities of the interferometer.



% \section{History of gravitational waves}
% \label{sec:history}

% In the 19$^{\mathrm{th}}$ century, most physicists believed that light propagated through a medium known as a ``luminiferous aether.''

% In 1887, Michelson and Morley built a folded interferometer to measure the speed of light propagation through the theorized aether \cite{Michelson1887}.

% In 1905, Einstein published his ``theory of relativity'', which is now known as special relativity \cite{Einstein1905}.

% In 1916, Einstein predicted the existence of gravitational waves as a consequence of his theory of general relativity \cite{Einstein1916}.

% In 1919, Eddington measured the gravitational lensing of starlight passing near the sun during an eclipse \cite{Eddington1920}.

% In 1929, Hubble measured the increasing velocity of nebulae further away from Earth, demonstrating the expansion of the universe \cite{Hubble1929}.

% In 1960, Weber invented the field of gravitational wave astronomy with his ``resonant bar'' detectors, designed to ``ring'' when excited by a passing GW \cite{Weber1960}

% In 1975, Hulse and Taylor published their discovery of a binary pulsar \cite{Hulse1975}.
% In 1981, the period of the binary's orbit was found to be decaying at exactly the rate predicted by energy loss through gravitational waves \cite{Weisberg1981}.

% In 1972, the viability of interferometers for detecting gravitational waves was first explored by Rainer Weiss \cite{Weiss1972}.

% Advanced LIGO construction began in 2008, and the first operational data was collected in 2015 \cite{AdvLIGOPaper}.
% In 2015, Advanced LIGO made first direct detection of gravitational waves from a binary black hole merger \cite{GW150914}.


% \section{Sources of gravitational waves}
% \label{sec:sources}

% Gravitational waves are produced when masses accelerate through spacetime, 
% similar to the way light is produced when charged particles accelerate.
% However, gravitational waves are extremely weak, 
% and are only appreciably produced by the most massive objects in the universe accelerating at speeds approaching the speed of light.


% The primary source of gravitational waves detected by Advanced LIGO comes from massive binary systems merging together
% including binary black holes,
% binary neutron stars \cite{PhysRevLett.119.161101},
% and neutron star-black hole binaries \cite{potentialNSBH}.
% Catalogs of binary mergers detected by Advanced LIGO have been compiled \cite{FirstCatalogPaper, SecondCatalogPaper}.

% Supernovae

% Continuous waves \cite{O1CW2017a, O1CW2017b}

% Stochastic background \cite{O1Stoch2017a, O1Stoch2017b, Callister2020}.


