'''
Simple two mirror cavity simulation with higher order modes.

Craig Cahillane

'''
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc
import scipy.special as scp

import pykat

import nds2utils as nu

savefigs = True
__file__ = os.path.abspath(__file__)
fn_dir = __file__.rsplit('/', maxsplit=2)[0]

if savefigs:
    mpl.rcParams.update({'text.usetex': True,
                         'figure.figsize': (12, 9),
                         'font.family': 'serif',
                         # 'font.serif': 'Georgia',
                         # 'mathtext.fontset': 'cm',
                         'lines.linewidth': 2.5,
                         'font.size': 22,
                         'xtick.labelsize': 'large',
                         'ytick.labelsize': 'large',
                         'legend.fancybox': True,
                         'legend.fontsize': 12,
                         'legend.framealpha': 0.7,
                         'legend.handletextpad': 0.5,
                         'legend.labelspacing': 0.2,
                         'legend.loc': 'best',
                         'savefig.dpi': 80,
                         'pdf.compression': 9})

'''Function Definitions

I use the reflectivity convention of complex transmission (amp trans -> i*t)
This gives unphysical phase results, but it's the convention used by Finesse,
so to compare results this is the right choice.
'''

def goodTicks(axis):
    '''
    Returns the y ticks to always by factors of 10 on the y scale, given some matplotlib axis object.
    Use if matplotlib keeps making your y-axis plot ticks spacing greater than 10.

    Input: matplotlib axis object. Like axis = plt.gca(), or axis = fig.add_subplot(111)
    Output: Correct yTicks spaced by factors of 10.
    '''
    ymin, ymax = axis.get_ylim()
    yTicks = np.array([10**x for x in np.arange(np.ceil(np.log10(ymin)), np.ceil(np.log10(ymax)))])
    return yTicks

def beam_properties_from_q(qq, lambda0=1064e-9):
    """Compute the properties of a Gaussian beam from a q parameter

    Inputs:
      qq: the complex q paramter
      lambda0: wavelength [m] (Default: 1064e-9)

    Returns:
      w: beam radius on the optic [m]
      zR: Rayleigh range of the beam [m]
      z: distance from the beam waist to the optic [m]
        Negative values indicate that the optic is before the waist.
      w0: beam waist [m]
      R: radius of curvature of the phase front on the optic [m]
      psi: Gouy phase [deg]
    """
    z = np.real(qq)
    zR = np.imag(qq)
    w0 = np.sqrt(lambda0*zR/np.pi)
    w = w0 * np.sqrt(1 + (z/zR)**2)
    R = zR**2 / z + z 
    psi = (np.pi/2 - np.angle(qq)) * 180/np.pi
    return w, zR, z, w0, R, psi 

''' Parameters '''

L = 3994.5
Lp = 57.6557
mod_freq = 9100230
Pin = 10.0 # W
mod_depth = 0.2 # rads
ti = np.sqrt(0.014)
ri = np.sqrt(1 - ti**2 - 37.5e-6)
te = np.sqrt(5e-6)
re = np.sqrt(1 - te**2 - 37.5e-6)
tp = np.sqrt(0.03)
rp = np.sqrt(1-tp**2)
c = 299792458.
lam = 1064e-9
FSR = c/(2 * L)
Finesse = np.pi * np.sqrt(ri * re)/(1 - ri * re)

phi_m1 = 0 # radians of mistuning
phi_m2 = 0 # radians of mistuning

Rc_i = -1934. # m
Rc_e = 2245. # m

gi = 1 - L/np.abs(Rc_i) # switch sign because of Finesse nonsense
ge = 1 - L/Rc_e

gouy_accum = 2*np.arccos( np.sign(ge)*np.sqrt(gi*ge) )
TMS = gouy_accum * FSR / (2 * np.pi) # transverse mode spacing, should be around 32 kHz

def TMS_freq_spacing(FSR, TMS, n, m):
    ''' Finds HOM TEMnm frequency spacing closest to 00 resonant mode for concentric cavity'''
    return (n + m) * (FSR - TMS)

'''
### Finesse Simulation ###

Misalign and mismatch the laser to the cavity, see what happens to our HOM content.

                          |-----|                   |---------|                 |---------|
| laser0 |-n0 ------ n1 ->| mod |-> n2 ---------- n3| mirror1 |n4 ----------- n5| mirror2 |n6
                          |_____|                   |_________|                 |_________|

Notes for beginners like me:
1) xaxis works with number of STEPS, not number of POINTS.
    So it goes from phase1 to phase2 with 10000 steps, yielding 10001 points
2) Phases are all in DEGREES, and I usually work in radians so I'll have to be careful here.
    reflectivities and amplitudes are in POWER, not AMPLITUDE.
    a) Except for xbeta, ybeta, which are in radians :(
3) If you flip an amplitude detector using *, make sure to put the * AFTER the NODE, like this:
    ad cav 0 n4*
    NOT like this:
    ad* cav 0 n4
4) Mirror Tuning, from the finesse manual, Page 42:
    "The direction of the displacement is arbitrarily defined to bein the direction
    of the normal vector on the front surface, i.e. a positive tuning moves the
    mirror from node2 towards node1 (for a mirror given by ‘m ...node1 node2’)
5) Cavity does not lock automatically if considering HOMs.  Need to use `cav` command.
6) Make sure to include negative sign if you want a concave input mirror radius of curvature. -->|(----)|
7) The beam parameter command `bp` cannot "turn around" by using e.g. n0*.  
    To get the beam coming out of a laser on node n0, use `bp q_in x q n0`, 
    then invert the sign of the real part like `q_laser = -np.conj(out['q_in'][0])`
8) If the `cav` command is used, changing the mode matching will automatically alter the input laser to be 
    optimally matched to the new cavity.
    If you want to analyze the mode matching, perform the following steps:
    a)  Separate your base code defining your cavity from your `cav` command, i.e. define them as two separate strings.  
        Be sure to include a beam parameter detector `bp q_param` somewhere in your base setup.  
        Use `noxaxis` and `yaxis lin abs:deg` to get a single, complex value.
        Use `phase 2` so your results are physical.
    b)  Find the eigenvalue of the cavity by parsing and running them together:
        i.e. kat.parse(base_string + cav_string); out = kat.run()
    c)  Extract the beam parameter of the optimal cavity mode.  
        You may have to invert the real part if using the input laser (see 7). i.e. q_param = -np.conj( out['q_param'] )
    d)  Extract the minimum beam waist `w0` and distance to the minimum waist `z` like
        z = np.real(q_param);
        w0 = np.sqrt(lambda0 * np.imag(q_param)/np.pi)
    e)  Define a new string gauss_string which defines the gaussian beam parameter at your node
        `gauss gauss1 my_component my_node w0 z`
    f)  Parse the gauss definition string together with the base string.  
        Now your beam will remain the same as you move your mirror RoC.
'''

maxtem_order = 2

basecode = \
f'''
l laser0 {Pin} 0 n0       # 10 Watt, 0 Freq. Offset

s space0 1 1 n0 n1
mod modulator {mod_freq} {mod_depth} 1 pm 0 n1 n2
s space1 1 1 n2 n3

m mirror1 {ri**2} {ti**2} {phi_m1*180/np.pi} n3 n4
s space2 {L} 1 n4 n5
m mirror2 {re**2} {te**2} {phi_m2*180/np.pi} n5 n6

attr mirror1 Rc {Rc_i}
attr mirror2 Rc {Rc_e}
maxtem {maxtem_order}
phase 2

bp q_in x q n0
pd arm_power n5
pd trans_power n6

noxaxis
yaxis lin abs:deg
'''

# Use the `cav` command to find the cavity eigenmode
cav_command = \
'''
cav cav1 mirror1 n4 mirror2 n5
'''

kat_base = pykat.finesse.kat()
kat_base.parse(basecode + cav_command)
out_base = kat_base.run()

q_base = -np.conj(out_base['q_in'])
w_base, zR_base, z_base, w0_base, R_base, psi_base = beam_properties_from_q(q_base)


# Add in amplitude detectors for carrier HOMs
for ii in range(maxtem_order+1):
    for jj in range(maxtem_order+1):
        if ii + jj > maxtem_order:
            continue
        basecode += f'ad ad{ii}{jj}_in {ii} {jj} 0 n0' + '\n'
        basecode += f'ad ad{ii}{jj}_cav {ii} {jj} 0 n5' + '\n'
        basecode += f'ad ad{ii}{jj}_refl {ii} {jj} 0 n3' + '\n'
        basecode += f'ad ad{ii}{jj}_trans {ii} {jj} 0 n6' + '\n'

# Set the q-param of the laser in the basecode
basecode += f'gauss q_laser laser0 n0 {w0_base} {z_base}' + '\n'

# Misalign the end mirror
num_points = 1001
max_misalignment = 1e-6 # rads
xbeta = np.linspace(-max_misalignment, max_misalignment, num_points)
xbeta_log = np.logspace(-11, -8, num_points)

cavity_misalignment_code = \
f'''
xaxis mirror2 xbeta log {xbeta_log[0]} {xbeta_log[-1]} {num_points-1}
'''

kat = pykat.finesse.kat()
kat.parse(basecode + cavity_misalignment_code)
out = kat.run()

# Mode mismatch the end mirror
max_RoC_change = 100 # m
Rc = np.linspace(-max_RoC_change, max_RoC_change, num_points)
Rc_log = np.logspace(-4, 1, num_points)

cavity_mode_mismatch_code = \
f'''
# xaxis* mirror2 Rcx log {Rc_log[0]} {Rc_log[-1]} {num_points-1}

var dummy {Rc_log[0]}
xaxis dummy re log {Rc_log[0]} {Rc_log[-1]} {num_points-1}

func RoC = {Rc_e} + $x1
put mirror2 Rcx $RoC
put mirror2 Rcy $RoC
'''
kat2 = pykat.finesse.kat()
kat2.parse(basecode + cavity_mode_mismatch_code)
out2 = kat2.run()

# Frequency scan to find HOM resonances
ff_scan = np.linspace(-20000, 20000, num_points)
tem_ratio = 0.1

cavity_scan_code = \
f'''
# Use a dummy variable to sweep over
var dummy {ff_scan[0]}
xaxis dummy re lin {ff_scan[0]} {ff_scan[-1]} {num_points-1} # tune the real part of the dummy variable
yaxis lin abs:deg

# Functions for upper and lower sidebands
func CAR = $x1

# Put the result of the function calculations
put laser0 f $CAR
'''
for ii in range(maxtem_order+1):
    for jj in np.arange(maxtem_order+1):
        if ii + jj > maxtem_order:
            continue
        if ii + jj == 0:
            continue
        cavity_scan_code += f'tem laser0 {ii} {jj} {tem_ratio} 0.0' + '\n'
for ii in range(maxtem_order+1):
    for jj in np.arange(maxtem_order+1):
        if ii + jj > maxtem_order:
            continue
        cavity_scan_code += f'put ad{ii}{jj}_in f $CAR' + '\n'
        cavity_scan_code += f'put ad{ii}{jj}_cav f $CAR' + '\n'
        cavity_scan_code += f'put ad{ii}{jj}_refl f $CAR' + '\n'
        cavity_scan_code += f'put ad{ii}{jj}_trans f $CAR' + '\n'

kat_cavity_scan = pykat.finesse.kat()
kat_cavity_scan.parse(basecode + cavity_scan_code)
out_cavity_scan = kat_cavity_scan.run()

# Add laser frequency shaking and demodulated pd at REFL port
inj_freq = 100 # Hz

mismatch_fsig_code = \
f'''
pd1 refl_inphase {inj_freq} 0 n3
pd1 refl_quadphase {inj_freq} 90 n3

pd1 trans_inphase {inj_freq} 0 n6
pd1 trans_quadphase {inj_freq} 90 n6

fsig freq_noise laser0 {inj_freq} 0 

var dummy {Rc_log[0]}
xaxis dummy re log {Rc_log[0]} {Rc_log[-1]} {num_points-1}

func RoC = {Rc_e} + $x1
put mirror2 Rcx $RoC
put mirror2 Rcy $RoC
'''
kat_mismatch_fsig = pykat.finesse.kat()
kat_mismatch_fsig.parse(basecode + mismatch_fsig_code)
out_mismatch_fsig = kat_mismatch_fsig.run()


# Freq noise to trans TF with changing mode matching
fflog = np.logspace(0, np.log10(50000), num_points)

Rc_log2 = np.array([0, 1e-4, 1e-3, 1e-2, 1e-1, 1e0, 1e1])
delta_tem = 1e-3

out_laser_freq_TF_dict = {}
for rc2 in Rc_log2:

    laser_freq_TF_code = \
    f'''
    pd1 refl_inphase 1 0 n3
    pd1 refl_quadphase 1 90 n3

    pd1 trans_inphase 1 0 n6
    pd1 trans_quadphase 1 90 n6

    fsig freq_noise laser0 1 0 

    xaxis freq_noise f log {fflog[0]} {fflog[-1]} {num_points-1}

    put refl_inphase f1 $x1
    put refl_quadphase f1 $x1
    put trans_inphase f1 $x1
    put trans_quadphase f1 $x1

    func RoC = {Rc_e} + {rc2}
    put mirror2 Rcx $RoC
    put mirror2 Rcy $RoC
    '''
    for ii in range(maxtem_order+1):
        for jj in np.arange(maxtem_order+1):
            if ii + jj > maxtem_order:
                continue
            if ii + jj == 0:
                continue
            laser_freq_TF_code += f'tem laser0 {ii} {jj} {delta_tem} 0.0' + '\n'
    kat_laser_freq_TF = pykat.finesse.kat()
    kat_laser_freq_TF.parse(basecode + laser_freq_TF_code)
    out_laser_freq_TF = kat_laser_freq_TF.run()

    out_laser_freq_TF_dict[rc2] = out_laser_freq_TF

# Freq noise to trans TF with changing input HOM content
tem_log = np.array([0.0, 1e-4, 1e-3, 1e-2, 1e-1])
delta_RoC = 1e-4

out_laser_freq_TF_tem_dict = {}
for tem in tem_log:

    laser_freq_TF_code = \
    f'''
    pd1 refl_inphase 1 0 n3
    pd1 refl_quadphase 1 90 n3

    pd1 trans_inphase 10 n6
    pd1 trans_quadphase 1 90 n6

    fsig freq_noise laser0 1 0 

    xaxis freq_noise f log {fflog[0]} {fflog[-1]} {num_points-1}

    put refl_inphase f1 $x1
    put refl_quadphase f1 $x1
    put trans_inphase f1 $x1
    put trans_quadphase f1 $x1

    func RoC = {Rc_e} + {delta_RoC}
    put mirror2 Rcx $RoC
    put mirror2 Rcy $RoC
    '''
    for ii in range(maxtem_order+1):
        for jj in np.arange(maxtem_order+1):
            if ii + jj > maxtem_order:
                continue
            if ii + jj == 0:
                continue
            laser_freq_TF_code += f'tem laser0 {ii} {jj} {tem} 0.0' + '\n'
    
    kat_laser_freq_TF = pykat.finesse.kat()
    kat_laser_freq_TF.parse(basecode + laser_freq_TF_code)
    out_laser_freq_TF = kat_laser_freq_TF.run()

    out_laser_freq_TF_tem_dict[tem] = out_laser_freq_TF

# Print some results
max_arm_power = max(np.real(out['arm_power']))
print(f'Base')
print(f'Base cavity power = {np.real(out_base["arm_power"])}')
print(f'Base q-parameter = {q_base}')
print(f'Current Waist = {w_base}')
print(f'Rayleigh range = {zR_base}')
print(f'Distance from waist = {z_base}')
print(f'Beam waist = {w0_base}')
print(f'Beam RoC = {R_base}')
print(f'Gouy = {psi_base}')
print()
print(f'Max cavity power = {max_arm_power}')
print()



''' 
###########
# Figures # 
########### 
'''
plotted_figures = np.array([])

fig_dir = '{}/figures/simple_cavity_HOMs'.format(fn_dir) 
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    print()
    print('Making this directory')
    os.makedirs(fig_dir)

# Cavity HOM amplitudes with changing alignment
fig, ss = plt.subplots(2, 2, sharex='col')
s1 = ss[0,0]
s2 = ss[0,1]
s3 = ss[1,0]
s4 = ss[1,1]

plot_out = out

for ii in range(maxtem_order+1):
    for jj in np.arange(maxtem_order+1):
        if ii + jj > maxtem_order:
            continue
        if ii+jj == 0:
            ls = '-'
        elif ii+jj == 1:
            ls = '--'
        elif ii+jj == 2: 
            ls = ':'

        ad_name = f'ad{ii}{jj}_refl'
        s1.loglog(xbeta_log*1e6, np.abs(plot_out[ad_name]), ls=ls, alpha=0.7, label=f'TEM {ii}{jj}')
        s3.semilogx(xbeta_log*1e6, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, alpha=0.7, label=f'TEM {ii}{jj}')

        ad_name = f'ad{ii}{jj}_trans'
        s2.loglog(xbeta_log*1e6, np.abs(plot_out[ad_name]), ls=ls, alpha=0.7, label=f'TEM {ii}{jj}')
        s4.semilogx(xbeta_log*1e6, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, alpha=0.7, label=f'TEM {ii}{jj}')

fig.suptitle('Fabry-Perot HOMs in REFL (left) and TRANS (right) port with misalignment', y=1.02)

s1.set_ylabel(r'Mag $\left| E_{nm} \right|$')
s2.set_ylabel(r'Phase [degrees]')

s3.set_xlabel(r'End X-Misalignment [$\mu$rads]')
s4.set_xlabel(r'End X-Misalignment [$\mu$rads]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)
s2.set_xticks(x_ticks)
s3.set_xticks(x_ticks)
s4.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(nu.good_ticks(s2))
s3.set_yticks(-90*np.arange(-2,3))
s4.set_yticks(-90*np.arange(-2,3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)
s3.grid()
s3.grid(which='minor', ls='--', alpha=0.6)
s4.grid()
s4.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='lower right', fontsize=12)
s2.legend(loc='lower right', fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'two_mirror_scattering_HOM_end_mirror_misalignment.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plotted_figures = np.append(plotted_figures, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')


# Cavity HOM amplitudes with induced mode mismatch
fig, ss = plt.subplots(2, 2, sharex='col')
s1 = ss[0,0]
s2 = ss[0,1]
s3 = ss[1,0]
s4 = ss[1,1]

plot_out = out2

for ii in range(maxtem_order+1):
    for jj in np.arange(maxtem_order+1):
        if ii + jj > maxtem_order:
            continue
        if ii+jj == 0:
            ls = '-'
        elif ii+jj == 1:
            ls = '--'
        elif ii+jj == 2: 
            ls = ':'

        ad_name = f'ad{ii}{jj}_refl'
        s1.loglog(Rc_log, np.abs(plot_out[ad_name]), ls=ls, label=f'TEM {ii}{jj}')
        s3.semilogx(Rc_log, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, label=f'TEM {ii}{jj}')

        ad_name = f'ad{ii}{jj}_trans'
        s2.loglog(Rc_log, np.abs(plot_out[ad_name]), ls=ls, label=f'TEM {ii}{jj}')
        s4.semilogx(Rc_log, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, label=f'TEM {ii}{jj}')

fig.suptitle('Fabry-Perot HOMs in REFL (left) and TRANS (right) port with misalignment', y=1.02)

s1.set_ylabel(r'Mag $\left| E_{nm} \right|$')
s2.set_ylabel(r'Phase [degrees]')

s3.set_xlabel(r'End Mirror $\Delta$RoC [m]')
s4.set_xlabel(r'End Mirror $\Delta$RoC [m]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)
s2.set_xticks(x_ticks)
s3.set_xticks(x_ticks)
s4.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(nu.good_ticks(s2))
s3.set_yticks(-90*np.arange(-2,3))
s4.set_yticks(-90*np.arange(-2,3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)
s3.grid()
s3.grid(which='minor', ls='--', alpha=0.6)
s4.grid()
s4.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='lower right', fontsize=12)
s2.legend(loc='lower right', fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'two_mirror_scattering_HOM_end_mirror_mode_mismatch.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plotted_figures = np.append(plotted_figures, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')


# Cavity HOM resonances with frequency scan
fig, ss = plt.subplots(2, 2, sharex='col')
s1 = ss[0,0]
s2 = ss[0,1]
s3 = ss[1,0]
s4 = ss[1,1]

plot_out = out_cavity_scan

for ii in range(maxtem_order+1):
    for jj in np.arange(maxtem_order+1):
        if ii + jj > maxtem_order:
            continue
        if ii+jj == 0:
            ls = '-'
        elif ii+jj >= 10:
            ls = '--'
        # elif ii+jj == 2: 
        #     ls = ':'

        ad_name = f'ad{ii}{jj}_trans'
        index = np.argmax(np.abs(plot_out[ad_name]))
        peak_freq = plot_out.x[index]

        ad_name = f'ad{ii}{jj}_refl'
        s1.semilogy(ff_scan, np.abs(plot_out[ad_name]), ls=ls, alpha=0.7, 
                    label=f'TEM {ii}{jj}, Peak {peak_freq} Hz')
        s3.plot(ff_scan, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, alpha=0.7, 
                    label=f'TEM {ii}{jj}, Peak {peak_freq} Hz')

        ad_name = f'ad{ii}{jj}_trans'
        s2.semilogy(ff_scan, np.abs(plot_out[ad_name]), ls=ls, alpha=0.7, 
                    label=f'TEM {ii}{jj}, Peak {peak_freq} Hz')
        s4.plot(ff_scan, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, alpha=0.7,
                    label=f'TEM {ii}{jj}, Peak {peak_freq} Hz')

fig.suptitle('Fabry-Perot HOMs in REFL (left) and TRANS (right) port from cavity scan', y=1.02)

s1.set_ylabel(r'Mag $\left| E_{nm} \right|$')
s3.set_ylabel(r'Phase [degrees]')

s3.set_xlabel(r'Laser Frequency Offset [Hz]')
s4.set_xlabel(r'Laser Frequency Offset [Hz]')

# xmin, xmax = s1.get_xlim()
# x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
# s1.set_xticks(x_ticks)
# s2.set_xticks(x_ticks)
# s3.set_xticks(x_ticks)
# s4.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(nu.good_ticks(s2))
s3.set_yticks(-90*np.arange(-2,3))
s4.set_yticks(-90*np.arange(-2,3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)
s3.grid()
s3.grid(which='minor', ls='--', alpha=0.6)
s4.grid()
s4.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='lower right', fontsize=12)
s2.legend(loc='lower right', fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'two_mirror_cavity_scan.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plotted_figures = np.append(plotted_figures, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')



# Cavity trans power with induced mode mismatch
fig, (s1) = plt.subplots(1)

plot_out = out_mismatch_fsig

# I
p1, = s1.semilogx(Rc_log, np.abs(plot_out['trans_power']), alpha=0.5, label=f'Trans')

s1.set_title('Transmitted power with changing RoC')

s1.set_ylabel(r'$ P_\mathrm{trans} $ [W]')

s1.set_xlabel(r'End Mirror $\Delta$RoC [m]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s1.grid()

s1.legend(loc='lower right', fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'simple_cavity_trans_power_with_changing_RoC.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plotted_figures = np.append(plotted_figures, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')



# Cavity Frequency noise transmission TF with induced mode mismatch
fig, (s1, s2) = plt.subplots(2, sharex='col')

plot_out = out_mismatch_fsig
finesse_TF = plot_out['trans_inphase'] + 1j * plot_out['trans_quadphase']

# I
p1, = s1.loglog(Rc_log, np.abs(finesse_TF), alpha=0.5, label=f'Trans')
s2.semilogx(Rc_log, 180/np.pi*np.angle(finesse_TF), alpha=0.5, label=f'Trans')

s1.set_title('Frequency noise transmission TF with changing RoC')

s1.set_ylabel(r'Mag $ P_\mathrm{trans}/\delta \nu $ [W/Hz]')
s2.set_ylabel(r'Phase [deg]')

s2.set_xlabel(r'End Mirror $\Delta$RoC [m]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)
s2.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(-90*np.arange(-2,3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='lower right', fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'simple_cavity_frequency_noise_coupling_to_trans_with_changing_RoC.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plotted_figures = np.append(plotted_figures, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')


# Cavity frequency to trans power transfer function 
fig, (s1, s2) = plt.subplots(2, sharex='col')

for rc2 in Rc_log2:
    plot_out = out_laser_freq_TF_dict[rc2]
    label = r'$\Delta$RoC = %.1e'%(rc2)
    finesse_TF = plot_out['trans_inphase'] + 1j * plot_out['trans_quadphase']

    p1, = s1.loglog(fflog, np.abs(finesse_TF), alpha=0.5, label=label)
    s2.semilogx(fflog, 180/np.pi*np.angle(finesse_TF), alpha=0.5, label=label)

TEM01_freq_space = TMS_freq_spacing(FSR, TMS, 0, 1)
s1.axvline(x=TEM01_freq_space, ls='--', color='b', lw=1.5, alpha=0.3, label=f'TMS = {TEM01_freq_space:.0f} Hz')
s2.axvline(x=TEM01_freq_space, ls='--', color='b', lw=1.5, alpha=0.3, label=f'TMS = {TEM01_freq_space:.0f} Hz')

s1.axvline(x=FSR, ls='-', color='b', lw=1.5, alpha=0.6, label=f'FSR = {FSR:.0f} Hz')
s2.axvline(x=FSR, ls='-', color='b', lw=1.5, alpha=0.6, label=f'FSR = {FSR:.0f} Hz')

s1.set_title('Fabry Perot freq noise to TRANS power TF with RoC changed ' + r'$\Delta$tem = %.1e'%(delta_tem), y=1.02)

s1.set_ylabel(r'Mag $\left| P_{trans}/\delta\nu \right|$ [W/Hz]')
s2.set_ylabel(r'Phase [deg]')

s2.set_xlabel(r'Frequency [Hz]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)
s2.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(90*np.arange(-2, 3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'simple_cavity_frequency_noise_coupling_to_trans_power_TF_with_RoC_change.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plotted_figures = np.append(plotted_figures, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')


# Cavity frequency to trans power transfer function 
fig, (s1, s2) = plt.subplots(2, sharex='col')

for tem in tem_log:
    plot_out = out_laser_freq_TF_tem_dict[tem]
    label = r'tem = %.1e'%(tem)
    finesse_TF = plot_out['trans_inphase'] + 1j * plot_out['trans_quadphase']

    p1, = s1.loglog(fflog, np.abs(finesse_TF), alpha=0.5, label=label)
    s2.semilogx(fflog, 180/np.pi * np.angle(finesse_TF), alpha=0.5, label=label)

TEM01_freq_space = TMS_freq_spacing(FSR, TMS, 0, 1)
s1.axvline(x=TEM01_freq_space, ls='--', color='b', lw=1.5, alpha=0.3, label=f'TMS = {TEM01_freq_space:.0f} Hz')
s2.axvline(x=TEM01_freq_space, ls='--', color='b', lw=1.5, alpha=0.3, label=f'TMS = {TEM01_freq_space:.0f} Hz')

s1.axvline(x=FSR, ls='-', color='b', lw=1.5, alpha=0.6, label=f'FSR = {FSR:.0f} Hz')
s2.axvline(x=FSR, ls='-', color='b', lw=1.5, alpha=0.6, label=f'FSR = {FSR:.0f} Hz')

s1.set_title('Fabry Perot freq noise to TRANS power TF with HOMs input ' + r'$\Delta$RoC = %.1e'%(delta_RoC) , y=1.02)

s1.set_ylabel(r'Mag $\left| P_{trans}/\delta\nu \right|$ [W/Hz]')
s2.set_ylabel(r'Phase [deg]')

s2.set_xlabel(r'Frequency [Hz]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)
s2.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(90*np.arange(-2, 3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'simple_cavity_frequency_noise_coupling_to_trans_power_TF_with_HOMs_injected.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plotted_figures = np.append(plotted_figures, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')


# End
print()
print('Plotnames')
print('open', end=' ')
for pn in plotted_figures:
    print('{}'.format(pn), end=' ')
print()