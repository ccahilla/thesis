'''
simple_michelson_misalign_beam_profile.py

Code for a simple michelson in Finesse, 
with a large misalignment between the arms.
The beam profile at the antisymmetric port is plotted.

Craig Cahillane
March 30, 2020
'''
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc
import scipy.special as scp

import pykat



mpl.rcParams.update({'text.usetex': True,
                        'figure.figsize': (12, 9),
                        'font.family': 'serif',
                        # 'font.serif': 'Georgia',
                        # 'mathtext.fontset': 'cm',
                        'lines.linewidth': 2.5,
                        'font.size': 22,
                        'xtick.labelsize': 'large',
                        'ytick.labelsize': 'large',
                        'legend.fancybox': True,
                        'legend.fontsize': 12,
                        'legend.framealpha': 0.7,
                        'legend.handletextpad': 0.5,
                        'legend.labelspacing': 0.2,
                        'legend.loc': 'best',
                        'savefig.dpi': 80,
                        'pdf.compression': 9})



#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])



#####   Functions   #####
def beam_properties_from_q(qq, lambda0=1064e-9):
    """Compute the properties of a Gaussian beam from a q parameter

    Inputs:
      qq: the complex q paramter
      lambda0: wavelength [m] (Default: 1064e-9)

    Returns:
      w: beam radius on the optic [m]
      zR: Rayleigh range of the beam [m]
      z: distance from the beam waist to the optic [m]
        Negative values indicate that the optic is before the waist.
      w0: beam waist [m]
      R: radius of curvature of the phase front on the optic [m]
      psi: Gouy phase [deg]
    """
    z = np.real(qq)
    zR = np.imag(qq)
    w0 = np.sqrt(lambda0*zR/np.pi)
    w = w0 * np.sqrt(1 + (z/zR)**2)
    R = zR**2 / z + z 
    psi = (np.pi/2 - np.angle(qq)) * 180/np.pi
    return w, zR, z, w0, R, psi 

def beam_q_from_radius_and_RoC(ww, RoC, lambda0=1064e-9):
    inv_q = 1/RoC - 1j*lambda0/(np.pi * ww**2)
    return 1/inv_q


''' Michelson Parameters '''

L = 3994.5 # m, length of arms
Pin = 1.0 # W, input laser power

tix = np.sqrt(0.014) # mirror amplitude tranmittivity
rix = np.sqrt(1 - tix**2) # mirror amplitude reflectivity
tiy = np.sqrt(0.014)
riy = np.sqrt(1 - tiy**2)

c = 299792458. # speed of light
lam = 1064e-9 # laser wavelength

phi_mx = -90 # degrees of mistuning, -90 so we are on a dark fringe in Finesse convention
phi_my = 0 # degrees of mistuning

# Rc_i = -1934. # m
Rc_e = 2245. # m, radius of curvature of the end mirrors

# FSR = c/(2 * L)

maxtem_order = 2    # higher order modes to consider: TEM_nm, such that n + m <= maxtem_order

num_points = 300    # number of points we want to plot

'''
### Finesse Simulation ###

Notes for beginners like me:
1) xaxis works with number of STEPS, not number of POINTS.
    So it goes from phase1 to phase2 with 10000 steps, yielding 10001 points
2) Phases are all in DEGREES, and I usually work in radians so I'll have to be careful here.
    reflectivities and amplitudes are in POWER, not AMPLITUDE.
    a) Except for xbeta, ybeta, which are in radians :(
3) If you flip an amplitude detector using *, make sure to put the * AFTER the NODE, like this:
    ad cav 0 n4*
    NOT like this:
    ad* cav 0 n4
4) Mirror Tuning, from the finesse manual, Page 42:
    "The direction of the displacement is arbitrarily defined to be in the direction
    of the normal vector on the front surface, i.e. a positive tuning moves the
    mirror from node2 towards node1 (for a mirror given by ‘m ...node1 node2’)
5) Cavity does not lock automatically if considering HOMs.  Need to use `cav` command.
6) Make sure to include negative sign if you want a concave input mirror 
    radius of curvature. -->|(----)|
7) The beam parameter command `bp` cannot "turn around" by using e.g. n0*.  
    To get the beam coming out of a laser on node n0, use `bp q_in x q n0`, 
    then invert the sign of the real part like `q_laser = -np.conj(out['q_in'][0])`
8) If the `cav` command is used, changing the mode matching will automatically 
    alter the input laser to be optimally matched to the new cavity.
    If you want to analyze the mode matching, perform the following steps:
    a)  Separate your base code defining your cavity from your `cav` command, 
    i.e. define them as two separate strings.  
        Be sure to include a beam parameter detector `bp q_param` somewhere in your base setup.  
        Use `noxaxis` and `yaxis lin abs:deg` to get a single, complex value.
        Use `phase 2` so your results are physical.
    b)  Find the eigenvalue of the cavity by parsing and running them together:
        i.e. kat.parse(base_string + cav_string); out = kat.run()
    c)  Extract the beam parameter of the optimal cavity mode.  
        You may have to invert the real part if using the input laser (see 7). 
        i.e. q_param = -np.conj( out['q_param'] )
    d)  Extract the minimum beam waist `w0` and distance to the minimum waist `z` like
        z = np.real(q_param);
        w0 = np.sqrt(lambda0 * np.imag(q_param)/np.pi)
    e)  Define a new string gauss_string which defines the gaussian beam parameter at your node
        `gauss gauss1 my_component my_node w0 z`
    f)  Parse the gauss definition string together with the base string.  
        Now your beam will remain the same as you move your mirror RoC.


Misalign and mismatch the laser to the michelson, 
see what happens to our HOM content in each arm and at the AS port.
Also shake the laser frequency to check the common mode rejection of the simple michelson
1) For a well-balanced michelson
2) For large reflectivity difference
3) For significant mode mismatch between the arms
4) For high injected HOM content

                                                    n10
                                                  _______
                                                 |  my   |
                                                 |_______|
                                                  n7 |
                                                     |
                                                     |
                                                  n4 |
                          |-----|                    | /----/ n5         n8 |-----|
| laser0 |-n0 ------ n1 ->| mod |-> n2 ---------- n3 -/ bs /----------------| mx  | n11
                          |_____|                    /____/ |               |-----|
                                                            | n6
                                                            |
                                                            |
                                                            v n9   
'''

basecode = \
f'''
l laser0 {Pin} 0 n0

s space0 2 1 n0 n3

bs beamsplitter 0.5 0.5 0 45 n3 n4 n5 n6      # 63.64 45 n3 n4 n5 n6 

s sy {L} 1 n4 n7
m my {riy**2} {tiy**2} {phi_my} n7 n10

s sx {L} 1 n5 n8
m mx {rix**2} {tix**2} {phi_mx} n8 n11

s sas 1 1 n6 n9

attr mx Rc {Rc_e}
attr my Rc {Rc_e}
maxtem {maxtem_order}
phase 2

pd arm_power_y n7
pd arm_power_x n8
pd as_power n9
pd refl_power n3

noxaxis
yaxis lin abs:deg

beam beam_as 0 n9  # this line sets up the beam detector, basically a CCD.  beam_as is the name, 0 is the frequency, and n9 is the antisymmetric port

xaxis  beam_as x lin -10 10 {num_points-1}
x2axis beam_as y lin -10 10 {num_points-1}

'''

# Set the input beam parameter by setting the beam radius and RoC to match each end mirror
ww_mirror = 0.0623 # m, beam radius at the end mirror
q_mirror = beam_q_from_radius_and_RoC(ww_mirror, Rc_e)
q_base = q_mirror - L - 2 # propagation law
w_base, zR_base, z_base, w0_base, R_base, psi_base = beam_properties_from_q(q_base)

basecode += f'gauss q_laser laser0 n0 {w0_base} {z_base}' + '\n' # this line sets the q parameter of the input beam

# Beam detector (phase camera) for misalignment
phase_camera_delta_xbeta = 1e-8 # 'xbeta' refers to the misalignment of the mirror in the yaw degree of freedom.  'ybeta' is pitch DOF.

kat = pykat.finesse.kat()                       # creates the pykat file
kat.parse(basecode)                             # parses the string defined above into pykat
kat.mx.xbeta.value = phase_camera_delta_xbeta   # rads, this sets mirror 'mx' to be misaligned by whatever is phase_camera_delta_xbeta
out = kat.run()                                 # runs the Finesse code via pykat, and returns a dictionary of simulation results



#####   Figures   #####

# Michelson AS port phase camera pic with misalignment
fig, (s1, s2) = plt.subplots(2, sharex=True, figsize=(9, 12))

plot_out = out

cs1 = s1.contourf( plot_out.x, plot_out.y, np.abs(plot_out['beam_as']), 10, cmap=plt.cm.bwr )
cs2 = s2.contourf( plot_out.x, plot_out.y, 180/np.pi*np.angle(plot_out['beam_as']), 10, cmap=plt.cm.bwr )

cbar1 = fig.colorbar(cs1, ax=s1)
cbar2 = fig.colorbar(cs2, ax=s2, ticks=[-180, -90, 0, 90, 180])

s1.axis('equal')
s2.axis('equal')

s1.set_title('AS port phase detector (Mag top, phase bottom) ' + r'$\Delta\theta_x$' + f' = {phase_camera_delta_xbeta:.1e} rad')
s1.set_ylabel('Detector Y-position')
s2.set_ylabel('Detector Y-position')
s2.set_xlabel('Detector X-position')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

plt.tight_layout()

# Save the figure
plot_name = 'michelson_misalignment_AS_port_phase_camera.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()


# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()