'''arm_cavity_beamtrace.py
Plots the eigenmode of a LIGO arm cavity.

Example 2
Create a simple two mirror cavity with radius of curvature of 1.0 meter,
and distance between the mirrors of 0.5 meters
R1 = 1934 m        R2 = 2245 m
______              ______
|    /______________\    |
|   /    L=3995 m    \   |
|   \ ______________ /   |
|____\              /____|
'''
import os
import numpy as np
import matplotlib as mpl
mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 14,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 12,
                     'legend.title_fontsize':12,
                     'legend.framealpha': 0.95,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})
import matplotlib.pyplot as plt

import beamtrace
from beamtrace.tracer import BeamTrace
import scipy.constants as scc

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

def get_mirror_curve(top_coords, bottom_coords, radius_of_curvature, roc_sign=1, steps=100):
    """Calculates and returns tuple (xx, yy) corrdinates 
    to plot for the curve of the front of a mirror
    with a spherical radius_of_curvature.
    """
    x1, y1 = top_coords
    x2, y2 = bottom_coords
    roc = radius_of_curvature

    # Find the center of the mirror's circle (x0, y0), assuming y0 = 0
    y0 = 0
    x0 = x1 + roc_sign * np.sqrt(roc**2 - y1**2)
    # print(f'center x0 = {x0}')

    theta1 = np.arctan2(y1, x1 - x0)
    theta2 = np.arctan2(y2, x2 - x0)
    # print(f'theta1 = {theta1}')
    # print(f'theta2 = {theta2}')
    if theta2 < 0 and roc_sign == 1:
        theta2 += 2 * np.pi

    # theta1 = theta1 - 0.1
    # theta2 = theta2 + 0.1

    thetas = np.linspace(theta1, theta2, steps)
    
    xx = roc_sign * roc * np.cos(thetas) + x0
    yy = roc_sign * roc * np.sin(thetas) + y0

    return xx, yy


#####   Set up LIGO arm cavity   #####

labels = np.array(['planar', 'concentric', 'half symmetric', 'confocal', 'convex concave'])
R1s = np.array([3.0, 0.51,  1.25,   0.99, -0.75]) # m
R2s = np.array([3.0, 0.51,  np.inf, 0.99,   1.5]) # m
Ls  = np.array([1.0, 1.0,   1.0,    1.0,    1.0]) # m

cav_dict = {}
for label, R1, R2, L in zip(labels, R1s, R2s, Ls):
    print()
    print(f'cavity is {label}')
    cav = BeamTrace()

    cav.add_mirror(R1, name='R1')
    cav.add_space(L)
    cav.add_mirror(R2, name='R2')

    cav.scan_cavity(round_trip=False)

    cav_dict[label] = cav

#####   Figures   #####
# Plot the beam radius
fig, ss = plt.subplots(len(labels), sharex=True, figsize=(9, 12))
for ii in range(len(ss)):
    ax = ss[ii]
    label = labels[ii]
    cav = cav_dict[label]

    plot_ww = cav.ww * 1e3

    for comp_name, comp_dict in cav.comp_dict.items():
        if comp_dict['type'] == 'mirror':
            if comp_name == 'R1':
                top_coords = (cav.zz[0], plot_ww[0])
                bottom_coords = (cav.zz[0], -plot_ww[0])
                roc = comp_dict['radius_of_curvature']
                if label == 'convex concave':
                    roc_sign = -1
                else:
                    roc_sign = 1
                xx, yy = get_mirror_curve(top_coords, bottom_coords, roc*50, roc_sign)

                R1 = comp_dict['radius_of_curvature']
                g1 = 1 - L / R1
                line_label = r'$R_1$' + f' = {R1:.1f} m\n' + r'$g_1$' + f' = {g1:.2f}'

                plot_yy = yy
                ax.plot(xx, plot_yy, color='#88bbff', label=line_label)

            if comp_name == 'R2':
                top_coords = (cav.zz[-1], plot_ww[-1])
                bottom_coords = (cav.zz[-1], -plot_ww[-1])
                roc = comp_dict['radius_of_curvature']
                if label == 'convex concave':
                    roc_sign = 1
                else:
                    roc_sign = 1
                xx, yy = get_mirror_curve(top_coords, bottom_coords, roc*50, roc_sign)

                R2 = comp_dict['radius_of_curvature']
                g2 = 1 - L / R2
                

                if label == 'half symmetric':
                    line_label = r'$R_2 = \infty$' + f' m\n' + r'$g_2 = \infty$'
                    ax.plot(*zip(top_coords, bottom_coords), color='#1166ff', label=line_label)
                else:
                    line_label = r'$R_2$' + f' = {R2:.1f} m\n' + r'$g_2$' + f' = {g2:.2f}'
                    plot_xx = -1*(xx - 1) + 1.0
                    plot_yy = yy
                    ax.plot(plot_xx, plot_yy, color='#1166ff', label=line_label)


    g1g2 = g1 * g2
    ax.plot(cav.zz, plot_ww, color='C3', label=r'$g_1 g_2$' + f' = {g1g2:.2f}')
    ax.plot(cav.zz, -plot_ww, color='C3')

    ax.grid()
    ax.set_ylim([-1.5, 1.5])
    ax.set_ylabel('Beam radius $w$ [mm]')
    ax.set_title(f'{label} cavity')
    ax.legend(bbox_to_anchor=(1.01, 1))

# ss[-1].set_xlim([-0.2, 1.2])
ss[-1].set_xlabel('Cavity axis $z$ [m]')

plot_name = 'resonator_examples.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Plot the Gouy Phase
fig, ss = plt.subplots(len(labels), sharex=True, figsize=(9, 12))
for ii in range(len(ss)):
    ax = ss[ii]
    label = labels[ii]
    cav = cav_dict[label]

    round_trip_gouy_degs = 2 * cav.gouy[-1] * 180/np.pi

    plot_gouy = cav.gouy * 180/np.pi
    ax.plot(cav.zz, plot_gouy, color='C3', label=f"round-trip Gouy phase = {round_trip_gouy_degs:.0f} degs")

    ax.grid()
    ax.set_ylim([0, 180])
    ax.set_yticks([0, 45, 90, 135, 180])
    ax.set_ylabel(r'Gouy phase $\varphi$ [deg]')
    ax.set_title(f'{label} cavity')
    ax.legend()

# ss[-1].set_xlim([-0.2, 1.2])
ss[-1].set_xlabel('Cavity axis $z$ [m]')

plot_name = 'resonator_examples_gouy_phase.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Transverse mode spacing
FSR = scc.c / 2 * L
colors = [
    '#003f5c',
    '#58508d',
    '#bc5090',
    '#ff6361',
    '#ffa600',
]

fig, ss = plt.subplots(len(labels), sharex=True, figsize=(9, 12))
for ii in range(len(ss)):
    ax = ss[ii]
    label = labels[ii]
    cav = cav_dict[label]

    round_trip_gouy = 2 * cav.gouy[-1]
    transverse_mode_spacing = FSR * round_trip_gouy / (2 * np.pi)

    for qq in range(-5, 6):
        main_resonant_freq = qq * FSR
        for nn in range(5):
            resonant_freq = main_resonant_freq + nn * transverse_mode_spacing
            line_height = 1 - nn * 0.2
            color = colors[nn]
            if qq == 0:
                line_label = r"$\mathrm{TEM}_{%d0}$" % nn
            else:
                line_label = ""

            plot_resonant_freq = resonant_freq * 1e-6   # convert to MHz from Hz
            ax.plot([plot_resonant_freq, plot_resonant_freq], [0, line_height], color=color, label=line_label)

    ax.grid()
    ax.set_ylim([0, 1.1])
    ax.set_ylabel(r'Cav power $P_{nm}/P_{00}$')
    ax.set_title(f'{label} cavity - ' + r'$\nu_\mathrm{TMS}$' + f' = {transverse_mode_spacing * 1e-6:.0f} MHz')

ss[1].legend(title=r'$\nu_\mathrm{FSR}$' + f' = {FSR * 1e-6:.0f} MHz', fontsize=12)
xlim = 275
ss[-1].set_xlim([-xlim, xlim])
xtick = 50
ss[-1].set_xticks(xtick * np.arange(-xlim//xtick + 1, xlim//xtick + 1))
ss[-1].set_xlabel(r'Resonant mode frequencies $\nu_\mathrm{nm}$ [MHz]')

plot_name = 'resonator_examples_transverse_mode_spacing.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()
