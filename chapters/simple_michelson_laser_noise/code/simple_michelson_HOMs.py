'''
Simple michelson with higher order modes.

I use the reflectivity convention of complex transmission (amp trans -> i*t)
This gives unusual phase results, but it's the convention used by Finesse,
so to compare results this is the right choice.

Craig Cahillane

'''
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc
import scipy.special as scp

import pykat

import nds2utils as nu

mpl.rcParams.update({'text.usetex': True,
                        'figure.figsize': (12, 9),
                        'font.family': 'serif',
                        # 'font.serif': 'Georgia',
                        # 'mathtext.fontset': 'cm',
                        'lines.linewidth': 2.5,
                        'font.size': 22,
                        'xtick.labelsize': 'large',
                        'ytick.labelsize': 'large',
                        'legend.fancybox': True,
                        'legend.fontsize': 12,
                        'legend.framealpha': 0.7,
                        'legend.handletextpad': 0.5,
                        'legend.labelspacing': 0.2,
                        'legend.loc': 'best',
                        'savefig.dpi': 80,
                        'pdf.compression': 9})



#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])



#####   Functions   #####
def goodTicks(axis):
    '''
    Returns the y ticks to always by factors of 10 on the y scale, given some matplotlib axis object.
    Use if matplotlib keeps making your y-axis plot ticks spacing greater than 10.

    Input: matplotlib axis object. Like axis = plt.gca(), or axis = fig.add_subplot(111)
    Output: Correct yTicks spaced by factors of 10.
    '''
    ymin, ymax = axis.get_ylim()
    yTicks = np.array([10**x for x in np.arange(np.ceil(np.log10(ymin)), np.ceil(np.log10(ymax)))])
    return yTicks

def print_HOMs(out):
    '''
    Print the HOM content in each at all ports of the IFO
    '''
    det_list_suffix = np.array([
        'in',
        'x',
        'y',
        'refl',
        'as', 
        ])
    print()
    print('{:12}'.format('Detectors'), end = '')
    for ii in range(maxtem_order+1):
        for jj in range(maxtem_order+1):
            if ii + jj > maxtem_order:
                continue
            print(f'{ii:8}{jj}', end = ' ')
    print()
    for det in det_list_suffix:
        print(f'{det:12}', end = '')
        for ii in range(maxtem_order+1):
            for jj in range(maxtem_order+1):
                if ii + jj > maxtem_order:
                    continue
                full_det = f'ad{ii}{jj}_{det}'
                hom = np.abs(out[full_det])
                if hom < 1e-2:
                    print('', end='')
                elif hom < 1e-1:
                    print('\033[92m', end='')
                elif hom < 1e-0:
                    print('\033[93m', end='')
                else:
                    print('\033[91m', end='')

                print(f'{hom:9.2e}',end = ' ')
                print('\033[0m', end='')
        print()
    print()
    return

def beam_properties_from_q(qq, lambda0=1064e-9):
    """Compute the properties of a Gaussian beam from a q parameter

    Inputs:
      qq: the complex q paramter
      lambda0: wavelength [m] (Default: 1064e-9)

    Returns:
      w: beam radius on the optic [m]
      zR: Rayleigh range of the beam [m]
      z: distance from the beam waist to the optic [m]
        Negative values indicate that the optic is before the waist.
      w0: beam waist [m]
      R: radius of curvature of the phase front on the optic [m]
      psi: Gouy phase [deg]
    """
    z = np.real(qq)
    zR = np.imag(qq)
    w0 = np.sqrt(lambda0*zR/np.pi)
    w = w0 * np.sqrt(1 + (z/zR)**2)
    R = zR**2 / z + z 
    psi = (np.pi/2 - np.angle(qq)) * 180/np.pi
    return w, zR, z, w0, R, psi 

def beam_q_from_radius_and_RoC(ww, RoC, lambda0=1064e-9):
    inv_q = 1/RoC - 1j*lambda0/(np.pi * ww**2)
    return 1/inv_q

def michelson_freq_to_AS_power(ff, Pin, rx, ry, diff_offset, LL, lambda0=1064e-9):
    '''
    Takes the parameters of a simple michelson interferometer. 
    Returns the frequency to antisymmetric power transfer function constant.

    Inputs:
    ff              = frequency vector in Hz.
    Pin             = input power in watts.
    rx              = amplitude reflectivity of the x arm mirror
    ry              = amplitude reflectivity of the y arm mirror
    diff_offset     = microscopic differential phase offset from the antisymmetric dark port in radians
    LL              = length of michelson in meters.
    '''
    nu0 = scc.c / lambda0
    FSR = scc.c / (2 * LL)
    freq_to_AS_power_TF =   -np.exp(1j*np.pi/2) * Pin * rx * ry * np.sin(2 * diff_offset) * np.sin(ff * diff_offset / nu0) \
                            * np.exp(-1j * ff * 2 * np.pi / FSR) / ff
    return freq_to_AS_power_TF

def michelson_freq_to_AS_power_no_offset(ff, Pin, rx, ry, diff_offset, LL, lambda0=1064e-9):
    '''
    Takes the parameters of a simple michelson interferometer. 
    Returns the frequency to antisymmetric power transfer function constant.

    Inputs:
    ff              = frequency vector in Hz.
    Pin             = input power in watts.
    rx              = amplitude reflectivity of the x arm mirror
    ry              = amplitude reflectivity of the y arm mirror
    diff_offset     = microscopic differential phase offset from the antisymmetric dark port in radians
    LL              = length of michelson in meters.
    '''
    nu0 = scc.c / lambda0
    FSR = scc.c / (2 * LL)
    freq_to_AS_power_TF =   -Pin * rx * ry * np.sin(2 * diff_offset) * np.sin(ff * diff_offset / nu0) \
                            * np.exp(-1j * ff * np.pi / FSR) / ff
    return freq_to_AS_power_TF

def michelson_strain_to_AS_power(ff, Pin, rx, ry, diff_offset, LL, lambda0=1064e-9):
    '''
    Takes the parameters of a simple michelson interferometer. 
    Returns the frequency to antisymmetric power transfer function constant.
    NOTE: The minus sign in front is due to a difference in convention!
    transmission like i*t will give you a minus sign in front, 
    reflection like (+/-) on either side of a mirror will give you a plus sign in front

    Inputs:
    Pin             = input power in watts.
    rx              = amplitude reflectivity of the x arm mirror
    ry              = amplitude reflectivity of the y arm mirror
    diff_offset     = microscopic differential phase offset from the antisymmetric dark port in radians (phi_d = phi_x_rt - phi_y_rt)
    LL              = length of michelson in meters.
    '''
    FSR = scc.c / (2 * LL) # Hz
    k = 2 * np.pi / lambda0
    diff_to_AS_power_TF =   k * L * Pin * rx * ry * np.sin(2 * diff_offset) \
                            * np.exp(-1j * np.pi * ff / FSR) * np.sin(np.pi * ff / FSR) / (np.pi * ff / FSR) # W/rad
    return diff_to_AS_power_TF

def michelson_diff_space_to_AS_power(ff, Pin, rx, ry, diff_offset, LL, lambda0=1064e-9):
    '''
    Takes the parameters of a simple michelson interferometer. 
    Returns the frequency to antisymmetric power transfer function constant.
    NOTE: The minus sign in front is due to a difference in convention!
    transmission like i*t will give you a minus sign in front, 
    reflection like (+/-) on either side of a mirror will give you a plus sign in front

    Inputs:
    Pin             = input power in watts.
    rx              = amplitude reflectivity of the x arm mirror
    ry              = amplitude reflectivity of the y arm mirror
    diff_offset     = microscopic differential phase offset from the antisymmetric dark port in radians (phi_d = phi_x_rt - phi_y_rt)
    LL              = length of michelson in meters.
    '''
    FSR = scc.c / (2 * LL) # Hz
    phase_scaler_TF = 2 * np.pi * LL / lambda0 # m/m, phase of space modulation to phase of mirror modulation
    diff_to_AS_power_TF =   phase_scaler_TF * Pin * rx * ry * np.sin(2 * diff_offset) \
                            * np.exp(-1j * np.pi * ff / FSR) * np.sin(np.pi * ff / FSR) / (np.pi * ff / FSR) # W/rad
    return diff_to_AS_power_TF

def michelson_diff_displacement_to_AS_power(ff, Pin, rx, ry, diff_offset, LL, lambda0=1064e-9):
    '''
    Returns the differential displacement to AS power TF for a simple michelson.
    Takes the parameters of a simple michelson interferometer. 
    Returns the frequency to antisymmetric power transfer function constant.
    NOTE: The minus sign in front is due to a difference in convention!
    transmission like i*t will give you a minus sign in front, 
    reflection like (+/-) on either side of a mirror will give you a plus sign in front

    Inputs:
    Pin             = input power in watts.
    rx              = amplitude reflectivity of the x arm mirror
    ry              = amplitude reflectivity of the y arm mirror
    diff_offset     = microscopic differential phase offset from the antisymmetric dark port in radians (phi_d = phi_x_rt - phi_y_rt)
    LL              = length of michelson in meters.
    '''
    FSR = scc.c / (2 * LL) # Hz
    k = 2 * np.pi / lambda0
    diff_to_AS_power_TF = Pin * k * rx * ry * np.sin(2 * diff_offset) * np.exp(-1j * np.pi * ff / FSR) # W/m
    return diff_to_AS_power_TF

''' Parameters '''

L = 3994.5 # m 
Lp = 57.6557
mod_freq = 9100230
Pin = 10.0 # W
mod_depth = 0.2 # rads

# tix = np.sqrt(0.014)
# rix = np.sqrt(1 - tix**2) 
# tiy = np.sqrt(0.014)
# riy = np.sqrt(1 - tiy**2) 

rr = 0.95
dr = 0.025

rix = rr + dr
tix = np.sqrt(1 - rix**2) 
riy = rr - dr
tiy = np.sqrt(1 - riy**2) 

te = np.sqrt(5e-6)
re = np.sqrt(1 - te**2 - 37.5e-6)
tp = np.sqrt(0.03)
rp = np.sqrt(1-tp**2)
tbs = np.sqrt(0.5)
rbs = np.sqrt(1 - 0.5)

c = 299792458.
lam = 1064e-9
k = 2 * np.pi / lam

phi_mx = 90 # degrees of mistuning
phi_my = 0 # degrees of mistuning

Rc_i = -1934. # m
Rc_e = 2245. # m

FSR = c/(2 * L)

'''
### Finesse Simulation ###

Notes for beginners like me:
1) xaxis works with number of STEPS, not number of POINTS.
    So it goes from phase1 to phase2 with 10000 steps, yielding 10001 points
2) Phases are all in DEGREES, and I usually work in radians so I'll have to be careful here.
    reflectivities and amplitudes are in POWER, not AMPLITUDE.
    a) Except for xbeta, ybeta, which are in radians :(
3) If you flip an amplitude detector using *, make sure to put the * AFTER the NODE, like this:
    ad cav 0 n4*
    NOT like this:
    ad* cav 0 n4
4) Mirror Tuning, from the finesse manual, Page 42:
    "The direction of the displacement is arbitrarily defined to bein the direction
    of the normal vector on the front surface, i.e. a positive tuning moves the
    mirror from node2 towards node1 (for a mirror given by ‘m ...node1 node2’)
5) Cavity does not lock automatically if considering HOMs.  Need to use `cav` command.
6) Make sure to include negative sign if you want a concave input mirror 
    radius of curvature. -->|(----)|
7) The beam parameter command `bp` cannot "turn around" by using e.g. n0*.  
    To get the beam coming out of a laser on node n0, use `bp q_in x q n0`, 
    then invert the sign of the real part like `q_laser = -np.conj(out['q_in'][0])`
8) If the `cav` command is used, changing the mode matching will automatically 
    alter the input laser to be optimally matched to the new cavity.
    If you want to analyze the mode matching, perform the following steps:
    a)  Separate your base code defining your cavity from your `cav` command, 
    i.e. define them as two separate strings.  
        Be sure to include a beam parameter detector `bp q_param` somewhere in your base setup.  
        Use `noxaxis` and `yaxis lin abs:deg` to get a single, complex value.
        Use `phase 2` so your results are physical.
    b)  Find the eigenvalue of the cavity by parsing and running them together:
        i.e. kat.parse(base_string + cav_string); out = kat.run()
    c)  Extract the beam parameter of the optimal cavity mode.  
        You may have to invert the real part if using the input laser (see 7). 
        i.e. q_param = -np.conj( out['q_param'] )
    d)  Extract the minimum beam waist `w0` and distance to the minimum waist `z` like
        z = np.real(q_param);
        w0 = np.sqrt(lambda0 * np.imag(q_param)/np.pi)
    e)  Define a new string gauss_string which defines the gaussian beam parameter at your node
        `gauss gauss1 my_component my_node w0 z`
    f)  Parse the gauss definition string together with the base string.  
        Now your beam will remain the same as you move your mirror RoC.


Misalign and mismatch the laser to the michelson, 
see what happens to our HOM content in each arm and at the AS port.
Also shake the laser frequency to check the common mode rejection of the simple michelson
1) For a well-balanced michelson
2) For large reflectivity difference
3) For significant mode mismatch between the arms
4) For high injected HOM content

                                                    n10
                                                  _______
                                                 |  my   |
                                                 |_______|
                                                  n7 |
                                                     |
                                                     |
                                                  n4 |
                          |-----|                    | /----/ n5         n8 |-----|
| laser0 |-n0 ------ n1 ->| mod |-> n2 ---------- n3 -/ bs /----------------| mx  | n11
                          |_____|                    /____/ |               |-----|
                                                            | n6
                                                            |
                                                            |
                                                            v n9   
'''

maxtem_order = 4

inj_freq = 10
mod_depth = 0.1

basecode = \
f'''
l laser0 {Pin} 0 n0

s space0 2 1 n0 n3
# mod modulator {inj_freq} {mod_depth} 1 pm 0 n1 n2
# s space1 1 1 n2 n3

bs beamsplitter 0.5 0.5 0 45 n3 n4 n5 n6      # 63.64 45 n3 n4 n5 n6 

s sy {L} 1 n4 n7
m my {riy**2} {tiy**2} {phi_my} n7 n10

s sx {L} 1 n5 n8
m mx {rix**2} {tix**2} {phi_mx} n8 n11

s sas 1 1 n6 n9

attr mx Rc {Rc_e}
attr my Rc {Rc_e}
maxtem {maxtem_order}
phase 2

pd arm_power_y n7
pd arm_power_x n8
pd as_power n9
pd refl_power n3

noxaxis
yaxis lin abs:deg
'''

# Set the input beam parameter by setting the beam radius and RoC to match each end mirror
ww_mirror = 0.0623 # m, beam radius at the mirror
q_mirror = beam_q_from_radius_and_RoC(ww_mirror, Rc_e)
q_base = q_mirror - L - 2 # propagation law
w_base, zR_base, z_base, w0_base, R_base, psi_base = beam_properties_from_q(q_base)

def theta_hat(theta, zz, z0, w0):
    '''
    Takes in the misalignment of the end mirror, length from the beam waist, Rayleigh range, and beam waist. 
    Returns the higher-order scattering operator theta_hat, scaled according to the beam parameters and propogation.
    See LIGO DCC T990081 for scattering matrices and definitions.
    '''
    return theta * np.sqrt(z0**2 + zz**2) / w0

def alpha_hat(delta_RoC, z0):
    '''
    Takes in the radius of curvature of the mirror and length from the beam waist. 
    '''
    return delta_RoC / z0

basecode += f'gauss q_laser laser0 n0 {w0_base} {z_base}' + '\n'

# Add in amplitude detectors for carrier HOMs 
for ii in range(maxtem_order+1):
    for jj in range(maxtem_order+1):
        if ii + jj > maxtem_order:
            continue
        basecode += f'ad ad{ii}{jj}_in   {ii} {jj} 0 n0*' + '\n'
        basecode += f'ad ad{ii}{jj}_as   {ii} {jj} 0 n9' + '\n'
        basecode += f'ad ad{ii}{jj}_x    {ii} {jj} 0 n8' + '\n'
        basecode += f'ad ad{ii}{jj}_y    {ii} {jj} 0 n7' + '\n'
        basecode += f'ad ad{ii}{jj}_refl {ii} {jj} 0 n3' + '\n'

# Add in amplitude detectors for carrier HOMs 
for ii in range(maxtem_order+1):
    for jj in range(maxtem_order+1):
        if ii + jj > maxtem_order:
            continue
        basecode += f'ad ad{ii}{jj}_in_f1   {ii} {jj} {inj_freq} n0*' + '\n'
        basecode += f'ad ad{ii}{jj}_as_f1   {ii} {jj} {inj_freq} n9' + '\n'
        basecode += f'ad ad{ii}{jj}_x_f1    {ii} {jj} {inj_freq} n8' + '\n'
        basecode += f'ad ad{ii}{jj}_y_f1    {ii} {jj} {inj_freq} n7' + '\n'
        # mismatch_fsig_code += f'ad ad{ii}{jj}_refl_f1 {ii} {jj} {inj_freq} n3' + '\n'

        basecode += f'ad ad{ii}{jj}_in_mf1   {ii} {jj} {-1*inj_freq} n0*' + '\n'
        basecode += f'ad ad{ii}{jj}_as_mf1   {ii} {jj} {-1*inj_freq} n9' + '\n'
        basecode += f'ad ad{ii}{jj}_x_mf1    {ii} {jj} {-1*inj_freq} n8' + '\n'
        basecode += f'ad ad{ii}{jj}_y_mf1    {ii} {jj} {-1*inj_freq} n7' + '\n'

# Inject HOMs and see how much makes it to the dark port
tem = 1.0 # inject HOM ratio 
tem_code = ''
for ii in range(maxtem_order+1):
    for jj in np.arange(maxtem_order+1):
        if ii + jj > maxtem_order:
            continue
        if ii + jj == 0:
            continue
        tem_code += f'tem laser0 {ii} {jj} {tem} 0.0' + '\n'
kat_tem = pykat.finesse.kat()
kat_tem.parse(basecode + tem_code)
out_tem = kat_tem.run()

print_HOMs(out_tem)

# Tune the mirror over bright and dark fringes
num_points = 301
max_tune = 360 # degs
phis_mx = np.linspace(-max_tune, max_tune, num_points)
michelson_tune_code = \
f'''
xaxis mx phi lin {phis_mx[0]} {phis_mx[-1]} {num_points-1}
'''

kat_tune = pykat.finesse.kat()
kat_tune.parse(basecode + michelson_tune_code)
out_tune = kat_tune.run()

# Misalign the end mirror
num_points = 301
max_misalignment = 10e-6 # rads
xbeta = np.linspace(-max_misalignment, max_misalignment, num_points)
xbeta_log = np.logspace(-11, -8, num_points)

michelson_misalignment_code = \
f'''
xaxis mx xbeta log {xbeta_log[0]} {xbeta_log[-1]} {num_points-1}
'''

kat_misalign = pykat.finesse.kat()
kat_misalign.parse(basecode + michelson_misalignment_code)
out_misalign = kat_misalign.run()

# Mode mismatch the end mirror
max_RoC_change = 300 # m
Rc = np.linspace(-max_RoC_change, max_RoC_change, num_points)
Rc_log = np.logspace(-4, 1, num_points)

michelson_mode_mismatch_code = \
f'''
var dummy {Rc_log[0]}
xaxis dummy re log {Rc_log[0]} {Rc_log[-1]} {num_points-1}

func RoC = {Rc_e} + $x1
put mx Rcx $RoC
put mx Rcy $RoC

func RoC2 = {Rc_e} - $x1
put my Rcx $RoC2
put my Rcy $RoC2
'''
kat_mismatch = pykat.finesse.kat()
kat_mismatch.parse(basecode + michelson_mode_mismatch_code)
out_mismatch = kat_mismatch.run()


# Add laser frequency shaking and demodulated pd at AS port
# inj_freq = 10 # Hz

mismatch_fsig_code = \
f'''
pd1 as_power_f1_I {inj_freq} 0 n9
pd1 as_power_f1_Q {inj_freq} 90 n9

fsig freq_noise laser0 {inj_freq} 90

var dummy {Rc_log[0]}
xaxis dummy re log {Rc_log[0]} {Rc_log[-1]} {num_points-1}

func RoC = {Rc_e} + $x1
put mx Rcx $RoC
put mx Rcy $RoC

func RoC2 = {Rc_e} - $x1
put my Rcx $RoC2
put my Rcy $RoC2

mask as_power_f1_I 0 0 0
mask as_power_f1_Q 0 0 0

# mask as_power_f1_I 2 0 0
# mask as_power_f1_Q 2 0 0
# mask as_power_f1_I 0 2 0
# mask as_power_f1_Q 0 2 0

# mask as_power_f1_I 2 2 0
# mask as_power_f1_Q 2 2 0
# mask as_power_f1_I 4 0 0
# mask as_power_f1_Q 4 0 0
# mask as_power_f1_I 0 4 0
# mask as_power_f1_Q 0 4 0
'''

kat_mismatch_fsig = pykat.finesse.kat()
kat_mismatch_fsig.parse(basecode + mismatch_fsig_code)
kat_mismatch_fsig.mx.phi.value += -15
out_mismatch_fsig = kat_mismatch_fsig.run()



# print(f'ad00_as      = {np.abs(out_mismatch_fsig["ad00_as"][0])} mag * {np.angle(out_mismatch_fsig["ad00_as"][0], deg=True)} degs')
# print(f'ad00_as_f1   = {np.abs(out_mismatch_fsig["ad00_as_f1"][0])} mag * {np.angle(out_mismatch_fsig["ad00_as_f1"][0], deg=True)} degs')
# print(f'ad00_as_mf1  = {np.abs(out_mismatch_fsig["ad00_as_mf1"][0])} mag * {np.angle(out_mismatch_fsig["ad00_as_mf1"][0], deg=True)} degs')
# print()
# print(f'ad02_as      = {np.abs(out_mismatch_fsig["ad02_as"][0])} mag * {np.angle(out_mismatch_fsig["ad02_as"][0], deg=True)} degs')
# print(f'ad02_as_f1   = {np.abs(out_mismatch_fsig["ad02_as_f1"][0])} mag * {np.angle(out_mismatch_fsig["ad02_as_f1"][0], deg=True)} degs')
# print(f'ad02_as_mf1  = {np.abs(out_mismatch_fsig["ad02_as_mf1"][0])} mag * {np.angle(out_mismatch_fsig["ad02_as_mf1"][0], deg=True)} degs')


# Change the michelson offset by a few degrees, 
# calculate the estimated frequency noise coupling,
# shake the laser frequency

fflog = np.logspace(0, 5, num_points)
phis_my = np.array([-15, 3, 15, 45, 85]) # Define the detunings we want to operate at in degrees
analytic_freq_dict = {}
for phi_y in phis_my:
    phi_x = kat_tune.mx.phi.value
    phi_d = -1 * np.pi/180 * (2*phi_x - 2*phi_y)/2 # rads, diff phase = (phi_x_roundtrip - phi_y_roundtrip)/2,  minus bc positive tuning moves mirrors forward
    analytic_TF = michelson_freq_to_AS_power(fflog, Pin, rix, riy, phi_d, L)
    analytic_freq_dict[phi_y] = analytic_TF

out_freq_dict = {}
for phi_y in phis_my:
    offset_code = \
        f'''
        pd1 as_power_f1_I {inj_freq} 0 n9      # put a pd that we can demodulate at the as port at 0 phase
        pd1 as_power_f1_Q {inj_freq} 90 n9     # put a pd that we can demodulate at the as port at 0 phase

        fsig freqnoise laser0 freq {inj_freq} 90     # signal frequency on the laser
        xaxis freqnoise f log {fflog[0]} {fflog[-1]} {len(fflog)-1} # TF sweep of fsig frequency
        put as_power_f1_I f1 $x1        # set the demod freq to be what the xaxis is cookin'
        put as_power_f1_Q f1 $x1        # set the demod freq to be what the xaxis is cookin'
        '''
    kat = pykat.finesse.kat()
    kat.parse(basecode + offset_code)
    kat.my.phi.value = phi_y  # degs, set the tuning of the y-mirror to be slightly off the dark port
    out = kat.run()  
    out_freq_dict[phi_y] = out

# Shake the laser frequency at 100 Hz and demodulate the result in the antisymmetric port
tune_freq_code = \
f'''
pd1 as_power_f1_I {inj_freq} 0 n9     # put a pd that we can demodulate at the as port at 0 phase
pd1 as_power_f1_Q {inj_freq} 90 n9    # put a pd that we can demodulate at the as port at 0 phase

fsig freqnoise laser0 freq {inj_freq} 90    # signal frequency on the laser
xaxis mx phi lin {phis_mx[0]} {phis_mx[-1]} {len(phis_mx)-1} # Sweep of tuning while injecting frequency noise at 1000 Hz 
'''

kat_tune_freq = pykat.finesse.kat()
kat_tune_freq.parse(basecode + tune_freq_code)
out_tune_freq = kat_tune_freq.run()

phis_d = -1 * np.pi/180 * phis_mx
analytic_tune_freqs = michelson_freq_to_AS_power(inj_freq, Pin, rix, riy, phis_d, L, lambda0=1064e-9)



# Change the michelson offset by a few degrees, 
# calculate the estimated intensity noise coupling,
# shake the laser amplitude 

# fflog = np.logspace(0, 5, num_points)
# phis_my = np.array([-15, 3, 15, 45, 85]) # Define the detunings we want to operate at in degrees
# analytic_freq_dict = {}
# for phi_y in phis_my:
#     phi_x = kat_tune.mx.phi.value
#     phi_d = -1 * np.pi/180 * (2*phi_x - 2*phi_y)/2 # rads, diff phase = (phi_x_roundtrip - phi_y_roundtrip)/2,  minus bc positive tuning moves mirrors forward
#     analytic_TF = michelson_freq_to_AS_power(fflog, Pin, rix, riy, phi_d, L)
#     analytic_freq_dict[phi_y] = analytic_TF

out_int_dict = {}
for phi_y in phis_my:
    offset_code = \
        f'''
        pd1 as_power_f1_I {inj_freq} 0 n9      # put a pd that we can demodulate at the as port at 0 phase
        pd1 as_power_f1_Q {inj_freq} 90 n9     # put a pd that we can demodulate at the as port at 0 phase

        fsig intnoise laser0 amp {inj_freq} 0     # signal intensity on the laser
        xaxis intnoise f log {fflog[0]} {fflog[-1]} {len(fflog)-1} # TF sweep of fsig frequency
        put as_power_f1_I f1 $x1        # set the demod freq to be what the xaxis is cookin'
        put as_power_f1_Q f1 $x1        # set the demod freq to be what the xaxis is cookin'
        '''
    kat = pykat.finesse.kat()
    kat.parse(basecode + offset_code)
    kat.my.phi.value = phi_y  # degs, set the tuning of the y-mirror to be slightly off the dark port
    out = kat.run()  
    out_int_dict[phi_y] = out

# Shake the laser intensity at 100 Hz and demodulate the result in the antisymmetric port
tune_int_code = \
f'''
pd1 as_power_f1_I {inj_freq} 0 n9     # put a pd that we can demodulate at the as port at 0 phase
pd1 as_power_f1_Q {inj_freq} 90 n9    # put a pd that we can demodulate at the as port at 0 phase

fsig freqnoise laser0 amp {inj_freq} 0    # signal intensity noise on the laser
xaxis mx phi lin {phis_mx[0]} {phis_mx[-1]} {len(phis_mx)-1} # Sweep of tuning while injecting frequency noise at 1000 Hz 
'''

kat_tune_int = pykat.finesse.kat()
kat_tune_int.parse(basecode + tune_int_code)
out_tune_int = kat_tune_int.run()

phis_d = -1 * np.pi/180 * phis_mx
# analytic_tune_freqs = michelson_freq_to_AS_power(inj_freq, Pin, rix, riy, phis_d, L, lambda0=1064e-9)



# Shake the space in the arms differentially
analytic_diff_dict = {}
for phi in phis_my:
    phi_d = -1 * np.pi/180 * phi # differential phase = (phi_x_roundtrip - phi_y_roundtrip)/2.  phi_x_roundtrip = 0, phi_y_roundtrip = 2 * phi_y
    analytic_TF = michelson_diff_space_to_AS_power(fflog, Pin, rix, riy, phi_d, L)
    analytic_diff_dict[phi] = analytic_TF

out_diff_dict = {}
for phi_y in phis_my:
    diff_code = \
    f'''
    pd1 as_power_f1_I {inj_freq} 0 n9     # put a pd that we can demodulate at the as port at 0 phase
    pd1 as_power_f1_Q {inj_freq} 90 n9    # put a pd that we can demodulate at the as port at 0 phase

    fsig sig1 sx {inj_freq} 0 1.0      # inj_freq Hz init freq, 0 phase, 0.5 amplitude 
    fsig sig2 sy {inj_freq} 180 1.0    # inj_freq Hz init freq, 180 deg phase, 0.5 amplitude 

    xaxis sig1 f log {fflog[0]} {fflog[-1]} {len(fflog)-1} # tune the signal frequency 

    put as_power_f1_I f1 $x1    # set the new signal frequency as the demod frequency too
    put as_power_f1_Q f1 $x1
    '''
    kat_diff = pykat.finesse.kat()
    kat_diff.parse(basecode + diff_code)
    kat_diff.my.phi.value = phi_y
    out_diff = kat_diff.run()

    out_diff_dict[phi_y] = out_diff

# Shake the mirrors in the arms differentially
analytic_diff_disp_dict = {}
for phi in phis_my:
    phi_d = -1 * np.pi/180 * phi # differential phase = (phi_x_roundtrip - phi_y_roundtrip)/2.  phi_x_roundtrip = 0, phi_y_roundtrip = 2 * phi_y
    analytic_TF = michelson_diff_displacement_to_AS_power(fflog, Pin, rix, riy, phi_d, L)
    analytic_diff_disp_dict[phi] = analytic_TF

# From page 143, Eq. 6.11 of the Finesse 2 Manual
# the simulation below, with mx and my being shaken differentially,
# they are shaken in with phase sideband a_s.
# This phase sideband a_s represents a single-mirror single-pass phase tuning
# To get a single-mirror round trip, use 2 a_s
# To get a differential phase between arms, use 4 a_s
# We care about differential phase, so we will have to multiply the TF output of this simulation by 1/4
out_diff_disp_dict = {}
for phi_y in phis_my:
    diff_code = \
    f'''
    pd1 as_power_f1_I {inj_freq} 0 n9     # put a pd that we can demodulate at the as port at 0 phase
    pd1 as_power_f1_Q {inj_freq} 90 n9    # put a pd that we can demodulate at the as port at 0 phase

    fsig sig1 mx {inj_freq} 0 1.0      # inj_freq Hz init freq, 0 phase, 1.0 amplitude 
    fsig sig2 my {inj_freq} 180 1.0    # inj_freq Hz init freq, 180 deg phase, 1.0 amplitude 

    xaxis sig1 f log {fflog[0]} {fflog[-1]} {len(fflog)-1} # tune the signal frequency 

    put as_power_f1_I f1 $x1    # set the new signal frequency as the demod frequency too
    put as_power_f1_Q f1 $x1

    # scale meter
    '''
    kat_diff = pykat.finesse.kat()
    kat_diff.parse(basecode + diff_code)
    kat_diff.my.phi.value = phi_y
    out_diff = kat_diff.run()

    out_diff_disp_dict[phi_y] = out_diff

# Shake the space in the arms differentially at 100 Hz and demodulate the result in the antisymmetric port
tune_diff_code = \
f'''
pd1 as_power_f1_I {inj_freq} 0 n9     # put a pd that we can demodulate at the as port at 0 phase
pd1 as_power_f1_Q {inj_freq} 90 n9    # put a pd that we can demodulate at the as port at 0 phase

fsig sig1 sx {inj_freq} 0 1.0      # 10kHz init freq, 0 phase, 0.5 amplitude 
fsig sig2 sy {inj_freq} 180 1.0    # 10kHz init freq, 180 deg phase, 0.5 amplitude 

xaxis mx phi lin {phis_mx[0]} {phis_mx[-1]} {len(phis_mx)-1} # Sweep of tuning while injecting differential noise at 100 Hz
'''

kat_tune_diff = pykat.finesse.kat()
kat_tune_diff.parse(basecode + tune_diff_code)
out_tune_diff = kat_tune_diff.run()

phis_d = -1*np.pi/180*phis_mx
analytic_tune_diffs = michelson_diff_space_to_AS_power(inj_freq, Pin, rix, riy, phis_d, L)


# Misalign one ETM, and shake the laser frequency (no detuning)
xbeta_log2 = np.array([0, 1e-11, 1e-10, 1e-9, 1e-8, 1e-7])
out_xbeta2_dict = {}
for xb in xbeta_log2:
    misalign_code = \
        f'''
        pd1 as_power_f1_I {inj_freq} 0 n9      # put a pd that we can demodulate at the as port at 0 phase
        pd1 as_power_f1_Q {inj_freq} 90 n9     # put a pd that we can demodulate at the as port at 0 phase

        fsig freqnoise laser0 {inj_freq} 90     # signal frequency on the laser
        xaxis freqnoise f log {fflog[0]} {fflog[-1]} {len(fflog)-1} # TF sweep of fsig frequency
        put as_power_f1_I f1 $x1        # set the demod freq to be what the xaxis is cookin'
        put as_power_f1_Q f1 $x1        # set the demod freq to be what the xaxis is cookin'
        '''
    kat = pykat.finesse.kat()
    kat.parse(basecode + misalign_code)
    kat.mx.xbeta.value = xb  # rads
    
    out = kat.run()  
    out_xbeta2_dict[xb] = out


# Mode mismatch one ETM, and shake the laser frequency (no detuning)
Rc_log2 = np.array([0, 1e-2, 1e-1, 1e0, 1e1])
out_Rc2_dict = {}
for rc2 in Rc_log2:
    mismatch_code = \
    f'''
    pd1 as_power_f1_I {inj_freq} 0 n9      # put a pd that we can demodulate at the as port at 0 phase
    pd1 as_power_f1_Q {inj_freq} 90 n9     # put a pd that we can demodulate at the as port at 0 phase

    fsig freqnoise laser0 {inj_freq} 90     # signal frequency on the laser
    xaxis freqnoise f log {fflog[0]} {fflog[-1]} {len(fflog)-1} # TF sweep of fsig frequency
    put as_power_f1_I f1 $x1        # set the demod freq to be what the xaxis is cookin'
    put as_power_f1_Q f1 $x1        # set the demod freq to be what the xaxis is cookin'

    mask as_power_f1_I 0 0 0
    mask as_power_f1_Q 0 0 0
    '''

    kat = pykat.finesse.kat()
    kat.parse(basecode + mismatch_code)
    kat.mx.Rcx.value += rc2  # rads
    kat.mx.Rcy.value += rc2  # rads
    kat.my.Rcx.value -= rc2  # rads
    kat.my.Rcy.value -= rc2  # rads

    kat.mx.phi.value += -15

    out = kat.run()  
    out_Rc2_dict[rc2] = out


# Freq noise to trans TF with changing input HOM content
tem_log = np.array([0.0, 1e-4, 1e-3, 1e-2, 1e-1])
delta_RoC = 1e-4

out_laser_freq_TF_tem_dict = {}
for tem in tem_log:

    laser_freq_TF_code = \
    f'''
    pd1 as_power_f1_I {inj_freq} 0 n9      # put a pd that we can demodulate at the as port at 0 phase
    pd1 as_power_f1_Q {inj_freq} 90 n9     # put a pd that we can demodulate at the as port at 0 phase

    fsig freqnoise laser0 {inj_freq} 90     # signal frequency on the laser
    xaxis freqnoise f log {fflog[0]} {fflog[-1]} {num_points-1}
    put as_power_f1_I f1 $x1        # set the demod freq to be what the xaxis is cookin'
    put as_power_f1_Q f1 $x1        # set the demod freq to be what the xaxis is cookin'
    '''
    for ii in range(maxtem_order+1):
        for jj in np.arange(maxtem_order+1):
            if ii + jj > maxtem_order:
                continue

            laser_freq_TF_code += f'put ad{ii}{jj}_in_f1   f $x1' + '\n'
            laser_freq_TF_code += f'put ad{ii}{jj}_as_f1   f $x1' + '\n'
            laser_freq_TF_code += f'put ad{ii}{jj}_x_f1    f $x1' + '\n'
            laser_freq_TF_code += f'put ad{ii}{jj}_y_f1    f $x1' + '\n'

            laser_freq_TF_code += f'put ad{ii}{jj}_in_mf1  f $mx1' + '\n'
            laser_freq_TF_code += f'put ad{ii}{jj}_as_mf1  f $mx1' + '\n'
            laser_freq_TF_code += f'put ad{ii}{jj}_x_mf1   f $mx1' + '\n'
            laser_freq_TF_code += f'put ad{ii}{jj}_y_mf1   f $mx1' + '\n'

            if ii + jj == 0:
                continue
            laser_freq_TF_code += f'tem laser0 {ii} {jj} {tem} 0.0' + '\n'
    
    kat_laser_freq_TF = pykat.finesse.kat()
    kat_laser_freq_TF.parse(basecode + laser_freq_TF_code)
    out_laser_freq_TF = kat_laser_freq_TF.run()

    out_laser_freq_TF_tem_dict[tem] = out_laser_freq_TF

# Scan delta_T_ITM while monitoring frequency noise inj
# inj_freq = 10 # Hz, below cavity pole
T_ITMX_log = np.logspace(-5, 0, num_points)

out_T_mismatch_freq_inj_dict = {}
for phi_y in phis_my:
    T_mismatch_code = \
    f'''
    pd1 as_power_f1_I {inj_freq} 0 n9
    pd1 as_power_f1_Q {inj_freq} 90 n9

    fsig freqnoise laser0 {inj_freq} 90 

    var dummy {T_ITMX_log[0]}
    xaxis dummy re log {T_ITMX_log[0]} {T_ITMX_log[-1]} {num_points-1}

    func TT = $x1
    func RR = 1 + $mx1
    put mx T $TT
    put mx R $RR
    '''

    kat = pykat.finesse.kat()
    kat.parse(basecode + T_mismatch_code)
    kat.my.phi.value = phi_y
    out = kat.run()

    out_T_mismatch_freq_inj_dict[phi_y] = out

analytic_T_dict = {}
for phi_y in phis_my:
    phi_x = kat_tune.mx.phi.value
    phi_d = -1 * np.pi/180 * (2*phi_x - 2*phi_y)/2 # rads, diff phase = (phi_x_roundtrip - phi_y_roundtrip)/2,  minus bc positive tuning moves mirrors forward
    rixs = np.sqrt(1 - T_ITMX_log)
    analytic_TF = michelson_freq_to_AS_power(inj_freq, Pin, rixs, riy, phi_d, L)
    analytic_T_dict[phi_y] = analytic_TF

# Beam detector (phase camera) for misalignment
plot_phase_camera = False
if plot_phase_camera:
    phase_camera_delta_xbeta = 1e-8
    michelson_misalign_phase_camera_code = \
    f'''
    beam beam_as 0 n9

    xaxis  beam_as x lin -10 10 {num_points-1}
    x2axis beam_as y lin -10 10 {num_points-1}
    '''
    kat_misalign_phase_camera = pykat.finesse.kat()
    kat_misalign_phase_camera.parse(basecode + michelson_misalign_phase_camera_code)
    kat_misalign_phase_camera.mx.xbeta.value = phase_camera_delta_xbeta  # rads
    out_misalign_phase_camera = kat_misalign_phase_camera.run()

# Beam detector (phase camera) for mode mismatch
# phase_camera_delta_RoC = 1e1
# michelson_mode_mismatch_phase_camera_code = \
# f'''
# beam beam_as 0 n9

# xaxis  beam_as x lin -10 10 {num_points-1}
# x2axis beam_as y lin -10 10 {num_points-1}

# func RoC = {Rc_e} + {phase_camera_delta_RoC}
# put mx Rcx $RoC
# put mx Rcy $RoC
# '''
# kat_mismatch_phase_camera = pykat.finesse.kat()
# kat_mismatch_phase_camera.parse(basecode + michelson_mode_mismatch_phase_camera_code)
# out_mismatch_phase_camera = kat_mismatch_phase_camera.run()

if plot_phase_camera:
    out_mismatch_phase_camera_dict = {}
    for rc2 in Rc_log2:
        michelson_mode_mismatch_phase_camera_code = f'''
        beam beam_as 0 n9

        xaxis  beam_as x lin -10 10 {num_points-1}
        x2axis beam_as y lin -10 10 {num_points-1}

        func RoC = {Rc_e} + {rc2}
        put mx Rcx $RoC
        put mx Rcy $RoC
        '''
        kat_mismatch_phase_camera = pykat.finesse.kat()
        kat_mismatch_phase_camera.parse(basecode + michelson_mode_mismatch_phase_camera_code)
        out_mismatch_phase_camera = kat_mismatch_phase_camera.run()

        out_mismatch_phase_camera_dict[rc2] = out_mismatch_phase_camera

    phase_camera_delta_RoC = 1e1
    out_mismatch_phase_camera = out_mismatch_phase_camera_dict[phase_camera_delta_RoC]

# Print some results
max_refl_power = max(np.real(out_tune["refl_power"]))
print(f'Base')
print(f'FSR = {FSR} Hz')
print(f'Max reflected power = {max_refl_power}')
print(f'Base q-parameter = {q_base}')
print(f'Current Waist = {w_base}')
print(f'Rayleigh range = {zR_base}')
print(f'Distance from waist = {z_base}')
print(f'Beam waist = {w0_base}')
print(f'Beam RoC = {R_base}')
print(f'Gouy = {psi_base}')



##################################################################
#                            Figures                             # 
##################################################################

plot_names = np.array([])

# Tune perfect michelson to test power buildup
fig, (s1, s2) = plt.subplots(2, sharex=True)

s1.plot(phis_mx, np.abs(out_tune_freq['ad00_x'])**2, label='Arm Power X')
s1.plot(phis_mx, np.abs(out_tune_freq['ad00_y'])**2, label='Arm Power Y')
s1.plot(phis_mx, np.abs(out_tune_freq['ad00_refl'])**2, label='Reflected Power')
s1.plot(phis_mx, np.abs(out_tune_freq['ad00_as'])**2, label='Antisymmetric Power')

s2.plot(phis_mx, 180/np.pi * np.angle(out_tune_freq['ad00_x']), label='Arm Power X')
s2.plot(phis_mx, 180/np.pi * np.angle(out_tune_freq['ad00_y']), label='Arm Power Y')
s2.plot(phis_mx, 180/np.pi * np.angle(out_tune_freq['ad00_refl']), label='Reflected Power')
s2.plot(phis_mx, 180/np.pi * np.angle(out_tune_freq['ad00_as']), label='Antisymmetric Power')

s1.axvline(x=phi_mx, ls='--', color='k', label='Operating Point ' + r'$\phi_x = %.0f$ degs'%phi_mx)
s2.axvline(x=phi_mx, ls='--', color='k', label='Operating Point ' + r'$\phi_x = %.0f$ degs'%phi_mx)

s1.set_title(f'Michelson X mirror length tuning (Rx = {rix**2}, Ry = {riy**2})')
s1.set_ylabel(r'Power $|P_{00}|$ [$W$]')
s2.set_ylabel(r'Phase [degs]')
s2.set_xlabel(r'X-mirror tuning [degs]')

s1.set_xticks(-90*np.arange(phis_mx[0]/90, phis_mx[-1]/90 + 1))
s2.set_xticks(-90*np.arange(phis_mx[0]/90, phis_mx[-1]/90 + 1))

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=12)

# Save the figure
plotname = 'balanced_michelson_tune_x_mirror_length.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Frequency noise coupling for imperfect michelson with length offset
fig, (s1, s2) = plt.subplots(2, sharex='col')

for phi in phis_my:
    plot_out = out_freq_dict[phi]
    label = r'$\phi_D = %.0f$'%(phi_x - phi)
    finesse_TF = plot_out['as_power_f1_I'] + 1j * plot_out['as_power_f1_Q']

    p1, = s1.loglog(fflog, np.abs(finesse_TF), alpha=0.5, label='Finesse ' + label)
    s2.semilogx(fflog, 180/np.pi * np.angle(finesse_TF), alpha=0.5, label='Finesse ' + label)
    
    TF = analytic_freq_dict[phi]
    s1.loglog(fflog, np.abs(TF), ls='--', color=p1.get_color(), label='Analytic ' + label)
    s2.semilogx(fflog, 180/np.pi * np.angle(TF), ls='--', color=p1.get_color(), label='Analytic ' + label)

s1.axvline(x=FSR, ls='--', color='k', label='FSR')
s2.axvline(x=FSR, ls='--', color='k', label='FSR')

s1.set_title('Michelson frequency noise to AS power TF')

s1.set_ylabel(r'Mag $\left| P_{as}/\delta\nu \right|$ [W/Hz]')
s2.set_ylabel(r'Phase [deg]')

s2.set_xlabel(r'Frequency [Hz]')

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(90*np.arange(-2, 3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'simple_michelson_freq_to_AS_power_TFs.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Frequency noise coupling for michelson with length offset
analytic_tune_freqs_no_phase_offset = michelson_freq_to_AS_power_no_offset(inj_freq, Pin, rix, riy, -1*np.pi/180*phis_mx, L, lambda0=1064e-9)

fig, (s1, s2) = plt.subplots(2, sharex='col')

plot_out = out_tune_freq
finesse_TF = plot_out['as_power_f1_I'] + 1j * plot_out['as_power_f1_Q']

s1.plot(phis_mx, np.abs(finesse_TF), alpha=0.5, label='Finesse')
s2.plot(phis_mx, 180/np.pi * np.angle(finesse_TF), alpha=0.5, label='Finesse')

s1.plot(phis_mx, np.abs(analytic_tune_freqs), ls='--', label='Analytic')
s2.plot(phis_mx, 180/np.pi * np.angle(analytic_tune_freqs), ls='--', label='Analytic')

# s1.plot(phis_mx, np.abs(analytic_tune_freqs_no_phase_offset), ls='--', label='Analytic no offset')
# s2.plot(phis_mx, 180/np.pi * np.angle(analytic_tune_freqs_no_phase_offset), ls='--', label='Analytic no offset')

s1.axvline(x=phi_mx, ls='--', color='k', label='Operating Point ' + r'$\phi_x = %.0f$ degs'%phi_mx)
s2.axvline(x=phi_mx, ls='--', color='k', label='Operating Point ' + r'$\phi_x = %.0f$ degs'%phi_mx)

s1.set_title('Simple michelson frequency noise to AS power')
s1.set_ylabel(r'Mag $|P_{as}/\delta\nu|$ [W/Hz]')
s2.set_ylabel(r'Phase [deg]')
s2.set_xlabel(r'X length tuning $\phi_x$ [degrees]')

s1.set_xticks(-90*np.arange(phis_mx[0]/90, phis_mx[-1]/90 + 1))

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='lower right', fontsize=12)

# Save the figure
plotname = 'simple_michelson_length_offset_vs_freq_to_AS_power_coupling.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Intensity noise coupling for imperfect michelson with length offset
fig, (s1, s2) = plt.subplots(2, sharex='col')

for phi in phis_my:
    plot_out = out_int_dict[phi]
    label = r'$\phi_D = %.0f$'%(phi_x - phi)
    finesse_TF = plot_out['as_power_f1_I'] + 1j * plot_out['as_power_f1_Q']

    p1, = s1.loglog(fflog, np.abs(finesse_TF), alpha=0.5, label='Finesse ' + label)
    s2.semilogx(fflog, 180/np.pi * np.angle(finesse_TF), alpha=0.5, label='Finesse ' + label)
    
    # TF = analytic_freq_dict[phi]
    # s1.loglog(fflog, np.abs(TF), ls='--', color=p1.get_color(), label='Analytic ' + label)
    # s2.semilogx(fflog, 180/np.pi * np.angle(TF), ls='--', color=p1.get_color(), label='Analytic ' + label)

s1.axvline(x=FSR, ls='--', color='k', label='FSR')
s2.axvline(x=FSR, ls='--', color='k', label='FSR')

s1.set_title('Michelson intensity noise to AS power TF')

s1.set_ylabel(r'Mag $\left| P_{as}/(\delta P/P) \right|$ [W/RIN]')
s2.set_ylabel(r'Phase [deg]')

s2.set_xlabel(r'Frequency [Hz]')

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(90*np.arange(-2, 3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'simple_michelson_intensity_to_as_power_TFs.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Relative intensity noise coupling for michelson with length offset
# analytic_tune_freqs_no_phase_offset = michelson_freq_to_AS_power_no_offset(inj_freq, Pin, rix, riy, -1*np.pi/180*phis_mx, L, lambda0=1064e-9)

fig, (s1, s2) = plt.subplots(2, sharex='col')

plot_out = out_tune_int
finesse_TF = plot_out['as_power_f1_I'] + 1j * plot_out['as_power_f1_Q']

s1.plot(phis_mx, np.abs(finesse_TF), alpha=0.5, label='Finesse')
s2.plot(phis_mx, 180/np.pi * np.angle(finesse_TF), alpha=0.5, label='Finesse')

# s1.plot(phis_mx, np.abs(analytic_tune_freqs), ls='--', label='Analytic')
# s2.plot(phis_mx, 180/np.pi * np.angle(analytic_tune_freqs), ls='--', label='Analytic')

# s1.plot(phis_mx, np.abs(analytic_tune_freqs_no_phase_offset), ls='--', label='Analytic no offset')
# s2.plot(phis_mx, 180/np.pi * np.angle(analytic_tune_freqs_no_phase_offset), ls='--', label='Analytic no offset')

s1.axvline(x=phi_mx, ls='--', color='k', label='Operating Point ' + r'$\phi_x = %.0f$ degs'%phi_mx)
s2.axvline(x=phi_mx, ls='--', color='k', label='Operating Point ' + r'$\phi_x = %.0f$ degs'%phi_mx)

s1.set_title('Simple michelson relative intensity noise to AS power')
s1.set_ylabel(r'Mag $|P_{as}/(\delta P/P)|$ [W/RIN]')
s2.set_ylabel(r'Phase [deg]')
s2.set_xlabel(r'X length tuning $\phi_x$ [degrees]')

s1.set_xticks(-90*np.arange(phis_mx[0]/90, phis_mx[-1]/90 + 1))

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='lower right', fontsize=12)

# Save the figure
plotname = 'simple_michelson_length_offset_vs_intensity_to_as_power_coupling.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Differential phase TF for michelson with length offset
fig, (s1, s2) = plt.subplots(2, sharex='col')

for phi in phis_my:
    plot_out = out_diff_dict[phi] # different simulation results
    label = r'$\phi_D = %.0f$'%(phi_x - phi)
    finesse_TF = plot_out['as_power_f1_I'] + 1j * plot_out['as_power_f1_Q']

    p1, = s1.loglog(fflog, np.abs(finesse_TF), alpha=0.5, label=label)
    s2.semilogx(fflog, 180/np.pi*np.angle(finesse_TF), alpha=0.5, label=label)
    
    TF = analytic_diff_dict[phi] 
    s1.loglog(fflog, np.abs(TF), ls='--', color=p1.get_color(), label='Analytic ' + label)
    s2.semilogx(fflog, 180/np.pi*np.angle(TF), ls='--', color=p1.get_color(), label='Analytic ' + label)

s1.axvline(x=FSR, ls='--', color='k', label='FSR = $c/2L$')
s2.axvline(x=FSR, ls='--', color='k', label='FSR = $c/2L$')

# s1.set_title('Michelson differential phase to AS power TF')

s1.set_ylabel(r'Mag $\left| P_{as}/\delta\phi \right|$ [W/rad]')
s2.set_ylabel(r'Phase [deg]')

s2.set_xlabel(r'Frequency [Hz]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)
s2.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(90*np.arange(-2, 3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'simple_michelson_diff_phase_to_AS_power_TF.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Differential phase tuning for michelson with length offset
fig, (s1, s2) = plt.subplots(2, sharex='col')

plot_out = out_tune_diff
finesse_TF = plot_out['as_power_f1_I'] + 1j * plot_out['as_power_f1_Q']

s1.plot(phis_mx, np.abs(finesse_TF), alpha=0.5, label='Finesse')
s2.plot(phis_mx, 180/np.pi * np.angle(finesse_TF), alpha=0.5, label='Finesse')
s1.plot(phis_mx, np.abs(analytic_tune_diffs), ls='--', label='Analytic')
s2.plot(phis_mx, 180/np.pi * np.angle(analytic_tune_diffs), ls='--', label='Analytic')

s1.axvline(x=phi_mx, ls='--', color='k', label='Operating Point ' + r'$\phi_x = %.0f$ degs'%phi_mx)
s2.axvline(x=phi_mx, ls='--', color='k', label='Operating Point ' + r'$\phi_x = %.0f$ degs'%phi_mx)

s1.set_title('Simple michelson differential phase tuning to AS power')
s1.set_ylabel(r'Mag $\left| P_{as}/\delta\phi \right|$ [W/rad]')
s2.set_ylabel(r'Phase [deg]')
s2.set_xlabel(r'X length tuning $\phi_x$ [degrees]')

s1.set_xticks(-90*np.arange(phis_mx[0]/90, phis_mx[-1]/90 + 1))

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='lower right', fontsize=12)

# Save the figure
plotname = 'simple_michelson_diff_phase_offset_vs_diff_phase_to_AS_power_coupling.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Michelson HOM amplitudes in X and Y arms with changing alignment
fig, ss = plt.subplots(2, 2, sharex='col')
s1 = ss[0,0]
s2 = ss[0,1]
s3 = ss[1,0]
s4 = ss[1,1]

plot_out = out_misalign

ls = '-'
alpha = 0.5
# for jj in range(maxtem_order+1):
jj = 0
for ii in range(maxtem_order+1):
    if ii + jj > maxtem_order:
        continue

    ad_name = f'ad{ii}{jj}_x'
    s1.loglog(xbeta_log*1e6, np.abs(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')
    s3.semilogx(xbeta_log*1e6, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')

    ad_name = f'ad{ii}{jj}_y'
    s2.loglog(xbeta_log*1e6, np.abs(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')
    s4.semilogx(xbeta_log*1e6, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')

theta_x = theta_hat(xbeta_log, L + 2, zR_base, w0_base)
E00_to_10 = 1j * theta_x
E00_to_20 = -theta_x**2/np.sqrt(2)
E00_to_30 = -1j * theta_x**3/np.sqrt(6)
E00_to_40 = theta_x**4/np.sqrt(24)

E10 = -1j*np.sqrt(Pin)/np.sqrt(2) * E00_to_10 # -1j because of Finesse transmission through BS
E20 = -1j*np.sqrt(Pin)/np.sqrt(2) * E00_to_20
E30 = -1j*np.sqrt(Pin)/np.sqrt(2) * E00_to_30 # -1j because of Finesse transmission through BS
E40 = -1j*np.sqrt(Pin)/np.sqrt(2) * E00_to_40

s1.loglog(xbeta_log*1e6, np.abs(E10), ls='--', alpha=1.0, color='C3', label=f'Analytic TEM 10')
s3.semilogx(xbeta_log*1e6, 180/np.pi * np.angle(E10), ls='--', alpha=1.0, color='C3', label=f'Analytic TEM 10')

s1.loglog(xbeta_log*1e6, np.abs(E20), ls='--', alpha=1.0, color='C5', label=f'Analytic TEM 20')
s3.semilogx(xbeta_log*1e6, 180/np.pi * np.angle(E20), ls='--', alpha=1.0, color='C5', label=f'Analytic TEM 20')

s1.loglog(xbeta_log*1e6, np.abs(E30), ls=':', alpha=1.0, color='C2', label=f'Analytic TEM 30')
s3.semilogx(xbeta_log*1e6, 180/np.pi * np.angle(E30), ls=':', alpha=1.0, color='C2', label=f'Analytic TEM 30')

s1.loglog(xbeta_log*1e6, np.abs(E40), ls=':', alpha=1.0, color='k', label=f'Analytic TEM 40')
s3.semilogx(xbeta_log*1e6, 180/np.pi * np.angle(E40), ls=':', alpha=1.0, color='k', label=f'Analytic TEM 40')

fig.suptitle('Michelson HOMs in X arm (left) and Y arm (right) with misalignment', y=1.02)

s1.set_ylabel(r'Mag $\left| E_{nm} \right|$')
s3.set_ylabel(r'Phase [degrees]')
s3.set_xlabel(r'End X-Misalignment [$\mu$rads]')
s4.set_xlabel(r'End X-Misalignment [$\mu$rads]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)
s2.set_xticks(x_ticks)
s3.set_xticks(x_ticks)
s4.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(nu.good_ticks(s2))
s3.set_yticks(-90*np.arange(-2,3))
s4.set_yticks(-90*np.arange(-2,3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)
s3.grid()
s3.grid(which='minor', ls='--', alpha=0.6)
s4.grid()
s4.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='lower right', fontsize=12)
s2.legend(loc='lower right', fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'michelson_HOM_in_arms_with_end_mirror_misalignment.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Michelson HOM amplitudes at AS and REFL port with changing alignment
fig, ss = plt.subplots(2, 2, sharex='col')
s1 = ss[0,0]
s2 = ss[0,1]
s3 = ss[1,0]
s4 = ss[1,1]

plot_out = out_misalign

ls = '-'
alpha = 0.5
# for jj in range(maxtem_order+1):
jj = 0
for ii in range(maxtem_order+1):
    if ii + jj > maxtem_order:
        continue

    ad_name = f'ad{ii}{jj}_as'
    s1.loglog(xbeta_log*1e6, np.abs(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')
    s3.semilogx(xbeta_log*1e6, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')

    ad_name = f'ad{ii}{jj}_refl'
    s2.loglog(xbeta_log*1e6, np.abs(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')
    s4.semilogx(xbeta_log*1e6, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')

fig.suptitle('Michelson HOMs in AS (left) and REFL (right) port with misalignment', y=1.02)

s1.set_ylabel(r'Mag $\left| E_{nm} \right|$')
s3.set_ylabel(r'Phase [degrees]')
s3.set_xlabel(r'End X-Misalignment [$\mu$rads]')
s4.set_xlabel(r'End X-Misalignment [$\mu$rads]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)
s2.set_xticks(x_ticks)
s3.set_xticks(x_ticks)
s4.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(nu.good_ticks(s2))
s3.set_yticks(-90*np.arange(-2,3))
s4.set_yticks(-90*np.arange(-2,3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)
s3.grid()
s3.grid(which='minor', ls='--', alpha=0.6)
s4.grid()
s4.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='lower right', fontsize=12)
s2.legend(loc='lower right', fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'michelson_HOMs_at_AS_and_REFL_port_with_end_mirror_misalignment.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Frequency noise to AS port TF given different misalignments
fig, (s1, s2) = plt.subplots(2, sharex='col')

for xb in xbeta_log2:
    plot_out = out_xbeta2_dict[xb] # different simulation results
    label = r'AS $\Delta\theta_x$ = %.0e $\mathrm{\mu rad}$'%(xb*1e6)
    finesse_TF = plot_out['as_power_f1_I'] + 1j * plot_out['as_power_f1_Q']

    p1, = s1.loglog(fflog, np.abs(finesse_TF), alpha=0.5, label=label)
    s2.semilogx(fflog, 180/np.pi*np.angle(finesse_TF), alpha=0.5, label=label)

s1.axvline(x=FSR, ls='--', color='k', label='FSR')
s2.axvline(x=FSR, ls='--', color='k', label='FSR')

s1.set_title('Frequency noise to AS port TF given different misalignments', y=1.02)

s1.set_ylabel(r'Mag $\left| P_{as}/\delta\nu \right|$ [W/Hz]')
s2.set_ylabel(r'Phase $\left| P_{as}/\delta\nu \right|$ [deg]')

s2.set_xlabel(r'Frequency [Hz]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(90*np.arange(-2, 3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'frequency_noise_to_AS_port_TF_given_different_misalignments.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# AS port power with changing induced mode mismatch
fig, (s1) = plt.subplots(1)

plot_out = out_misalign
finesse_power = plot_out['as_power']

s1.loglog(xbeta_log*1e6, np.abs(finesse_power), label=f'AS power')

s1.set_title('Michelson AS power with misalignment')
s1.set_ylabel(r'Power in the AS port $ P_{as} $ [W]')
s1.set_xlabel(r'End X-Misalignment [$\mu$rads]')

s1.set_yticks(nu.good_ticks(s1))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'michelson_AS_power_with_arm_misalignment.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Michelson AS port phase camera pic with misalignment
if plot_phase_camera:
    fig, (s1, s2) = plt.subplots(2)

    plot_out = out_misalign_phase_camera

    cs1 = s1.contourf( plot_out.x, plot_out.y, np.abs(plot_out['beam_as']), 10, cmap=plt.cm.bwr )
    cs2 = s2.contourf( plot_out.x, plot_out.y, 180/np.pi*np.angle(plot_out['beam_as']), 10, cmap=plt.cm.bwr )

    cbar1 = fig.colorbar(cs1, ax=s1)
    cbar2 = fig.colorbar(cs2, ax=s2)

    s1.axis('equal')
    s2.axis('equal')

    s1.set_title('AS port phase detector (Mag top, phase bottom) ' + r'$\Delta\theta_x$' + f' = {phase_camera_delta_xbeta:.1e} rad')
    s1.set_ylabel('Detector Y-position')
    s2.set_ylabel('Detector Y-position')
    s2.set_xlabel('Detector X-position')

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.6)
    s2.grid()
    s2.grid(which='minor', ls='--', alpha=0.6)



    plt.tight_layout()

    # Save the figure
    plotname = 'michelson_misalignment_AS_port_phase_camera.pdf'
    full_plotname = '{}/{}'.format(fig_dir, plotname)
    plot_names = np.append(plot_names, full_plotname)
    print('Writing plot PDF to {}'.format(full_plotname))
    plt.savefig(full_plotname, bbox_inches='tight')
    plt.close()



# Michelson HOM amplitudes in X and Y arms with changing mode matching
fig, ss = plt.subplots(2, 2, sharex='col')
s1 = ss[0,0]
s2 = ss[0,1]
s3 = ss[1,0]
s4 = ss[1,1]

plot_out = out_mismatch

ls = '-'
alpha = 0.5
# for ii in range(maxtem_order+1):
ii = 0
for jj in range(maxtem_order+1):
    if ii + jj > maxtem_order:
        continue

    ad_name = f'ad{ii}{jj}_x'
    s1.loglog(Rc_log, np.abs(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')
    s3.semilogx(Rc_log, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')

    ad_name = f'ad{ii}{jj}_y'
    s2.loglog(Rc_log, np.abs(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')
    s4.semilogx(Rc_log, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')

alpha_RoC = alpha_hat(Rc_log, zR_base)

E00_to_02 = -1j*alpha_RoC/np.sqrt(2)
E00_to_04 = -np.sqrt(3.0/8.0) * alpha_RoC**2

E02 = -1j*np.sqrt(Pin)/np.sqrt(2) * E00_to_02 # -1j because of Finesse transmission through BS
E04 = -1j*np.sqrt(Pin)/np.sqrt(2) * E00_to_04 # -1j because of Finesse transmission through BS

s1.loglog(Rc_log, np.abs(E02), ls='--', alpha=1.0, color='k', label=f'Analytic TEM 02')
s3.semilogx(Rc_log, 180/np.pi * np.angle(E02), ls='--', alpha=1.0, color='k', label=f'Analytic TEM 02')

s1.loglog(Rc_log, np.abs(E04), ls=':', alpha=1.0, color='k', label=f'Analytic TEM 04')
s3.semilogx(Rc_log, 180/np.pi * np.angle(E04), ls=':', alpha=1.0, color='k', label=f'Analytic TEM 04')

fig.suptitle('Michelson HOMs in X arm (left) and Y arm (right) with mode mismatch', y=1.02)

s1.set_ylabel(r'Mag $\left| E_{nm} \right|$')
s3.set_ylabel(r'Phase [degrees]')
s3.set_xlabel(r'End Mirror $\Delta$RoC [m]')
s4.set_xlabel(r'End Mirror $\Delta$RoC [m]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)
s2.set_xticks(x_ticks)
s3.set_xticks(x_ticks)
s4.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(nu.good_ticks(s2))
s3.set_yticks(-90*np.arange(-2,3))
s4.set_yticks(-90*np.arange(-2,3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)
s3.grid()
s3.grid(which='minor', ls='--', alpha=0.6)
s4.grid()
s4.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='lower right', fontsize=12)
s2.legend(loc='lower right', fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'michelson_arms_HOM_end_mirror_mode_mismatch.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# AS and REFL HOM amplitudes with induced mode mismatch
fig, ss = plt.subplots(2, 2, sharex='col')
s1 = ss[0,0]
s2 = ss[0,1]
s3 = ss[1,0]
s4 = ss[1,1]

plot_out = out_mismatch

ls = '-'
alpha = 0.5
# for ii in range(maxtem_order+1):
ii = 0
for jj in range(maxtem_order+1):
    if ii + jj > maxtem_order:
        continue

    ad_name = f'ad{ii}{jj}_as'
    s1.loglog(Rc_log, np.abs(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')
    s3.semilogx(Rc_log, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')

    ad_name = f'ad{ii}{jj}_refl'
    s2.loglog(Rc_log, np.abs(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')
    s4.semilogx(Rc_log, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')

fig.suptitle('Michelson HOMs at AS (left) and REFL (right) port with mode mismatch', y=1.02)

s1.set_ylabel(r'Mag $\left| E_{nm} \right|$')
s3.set_ylabel(r'Phase [degrees]')
s3.set_xlabel(r'End Mirror $\Delta$RoC [m]')
s4.set_xlabel(r'End Mirror $\Delta$RoC [m]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)
s2.set_xticks(x_ticks)
s3.set_xticks(x_ticks)
s4.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(nu.good_ticks(s2))
s3.set_yticks(90*np.arange(-2, 3))
s4.set_yticks(90*np.arange(-2, 3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)
s3.grid()
s3.grid(which='minor', ls='--', alpha=0.6)
s4.grid()
s4.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'michelson_HOMs_at_AS_and_REFL_ports_end_mirror_mode_mismatch.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Frequency noise to AS port TF given different mode mismatches
fig, (s1, s2) = plt.subplots(2, sharex='col')

for rc2 in Rc_log2:
    plot_out = out_Rc2_dict[rc2] # different simulation results
    label = r'AS $\Delta$RoC = %.0e m'%(rc2)
    finesse_TF = plot_out['as_power_f1_I'] + 1j * plot_out['as_power_f1_Q']

    s1.loglog(fflog, np.abs(finesse_TF), alpha=0.5, label=label)
    s2.semilogx(fflog, 180/np.pi*np.angle(finesse_TF), alpha=0.5, label=label)

s1.axvline(x=FSR, ls='--', color='k', label='FSR')
s2.axvline(x=FSR, ls='--', color='k', label='FSR')

s1.set_title('Frequency noise to AS port TF given different mode mismatch', y=1.02)

s1.set_ylabel(r'Mag $\left| P_{as}/\delta\nu \right|$ [W/Hz]')
s3.set_ylabel(r'Phase [deg]')

s2.set_xlabel(r'Frequency [Hz]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)
s2.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(90*np.arange(-2, 3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'frequency_noise_to_AS_port_power_TF_given_different_mode_mismatch.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# AS port power with changing induced mode mismatch
fig, (s1) = plt.subplots(1)

plot_out = out_mismatch_fsig
finesse_power = plot_out['as_power']

s1.loglog(Rc_log, np.abs(finesse_power), label=f'AS power')

s1.set_title('Michelson AS power with changing arm mode matching')
s1.set_ylabel(r'Power in the AS port $ P_{as} $ [W]')
s1.set_xlabel(r'End Mirror $\Delta$RoC [m]')

s1.set_yticks(nu.good_ticks(s1))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'michelson_AS_power_with_arm_mode_mismatch.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Freq noise coupling to the AS port with changing induced mode mismatch
fig, (s1, s2) = plt.subplots(2, sharex=True)

plot_out = out_mismatch_fsig
finesse_TF = plot_out['as_power_f1_I'] + 1j * plot_out['as_power_f1_Q']

if maxtem_order >= 2:
    finesse_TF_ad0 = 2 * ( np.conj(plot_out['ad00_as']) * plot_out['ad00_as_f1'] + plot_out['ad00_as'] * np.conj(plot_out['ad00_as_mf1']) )
    finesse_TF_ad2 = 2 * ( np.conj(plot_out['ad02_as']) * plot_out['ad02_as_f1'] + plot_out['ad02_as'] * np.conj(plot_out['ad02_as_mf1']) \
                         + np.conj(plot_out['ad20_as']) * plot_out['ad20_as_f1'] + plot_out['ad20_as'] * np.conj(plot_out['ad20_as_mf1']) )
    finesse_TF_ad_total = finesse_TF_ad0 + finesse_TF_ad2

if maxtem_order >= 4:
    finesse_TF_ad4 = 4 * (np.conj(plot_out['ad00_as']) * plot_out['ad00_as_f1'] \
                        - np.conj(plot_out['ad02_as']) * plot_out['ad02_as_f1'] \
                        - np.conj(plot_out['ad20_as']) * plot_out['ad20_as_f1'] \
                        - np.conj(plot_out['ad04_as']) * plot_out['ad04_as_f1'] \
                        - np.conj(plot_out['ad40_as']) * plot_out['ad40_as_f1'] \
                        - np.conj(plot_out['ad22_as']) * plot_out['ad22_as_f1'])


s1.loglog(Rc_log, np.abs(finesse_TF), label=f'Finesse pd')
s2.semilogx(Rc_log, 180/np.pi*np.angle(finesse_TF), label=f'Finesse pd')

if maxtem_order >= 2:
    s1.loglog(Rc_log, np.abs(finesse_TF_ad0), ls='--', label=f'Finesse ad 0')
    s2.semilogx(Rc_log, 180/np.pi*np.angle(finesse_TF_ad0), ls='--', label=f'Finesse ad 0')
    s1.loglog(Rc_log, np.abs(finesse_TF_ad2), ls='--', label=f'Finesse ad 2')
    s2.semilogx(Rc_log, 180/np.pi*np.angle(finesse_TF_ad2), ls='--', label=f'Finesse ad 2')
    s1.loglog(Rc_log, np.abs(finesse_TF_ad_total), ls='--', label=f'Finesse ad total')
    s2.semilogx(Rc_log, 180/np.pi*np.angle(finesse_TF_ad_total), ls='--', label=f'Finesse ad total')

# if maxtem_order >= 4:
#     s1.loglog(Rc_log, np.abs(finesse_TF_ad4), ls=':', label=f'Finesse ad 4')
#     s2.semilogx(Rc_log, 180/np.pi*np.angle(finesse_TF_ad4), ls=':', label=f'Finesse ad 4')


s1.set_title('Michelson freq noise coupling with changing arm mode matching')
s1.set_ylabel(r'Mag $\left| P_{as}/\delta\nu \right|$ [W/Hz]')
s2.set_ylabel(r'Phase [deg]')
s2.set_xlabel(r'End Mirror $\Delta$RoC [m]')

# s1.set_ylim([1e-30, 1e-14])

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(90*np.arange(-2, 3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'michelson_freq_noise_to_AS_power_TF_with_arm_mode_mismatch.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Freq noise coupling to the AS port amplitudes with changing induced mode mismatch
fig, (s1, s2) = plt.subplots(2, sharex=True)

plot_out = out_mismatch_fsig

ls = '-'
alpha = 0.5
# for ii in range(maxtem_order+1):
ii = 0
for jj in range(maxtem_order+1):
    if ii + jj > maxtem_order:
        continue

    ad_name = f'ad{ii}{jj}_as_f1'
    s1.loglog(Rc_log, np.abs(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')
    s2.semilogx(Rc_log, 180/np.pi * np.angle(plot_out[ad_name]), ls=ls, alpha=alpha, label=f'TEM {ii}{jj}')

s1.set_title('Michelson freq noise coupling with changing arm mode matching')

s1.set_ylabel(r'Mag $\left| E_{nm,as}/\delta\nu \right|$ [$\sqrt{\mathrm{W}}$/Hz]')
s2.set_ylabel(r'Phase [deg]')

s2.set_xlabel(r'End Mirror $\Delta$RoC [m]')

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(90*np.arange(-2, 3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'michelson_freq_noise_to_AS_amplitudes_TF_with_arm_mode_mismatch.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Michelson AS port phase camera pic with mode mismatch
if plot_phase_camera:
    fig, (s1, s2) = plt.subplots(2)

    plot_out = out_mismatch_phase_camera

    cs1 = s1.contourf( plot_out.x, plot_out.y, np.abs(plot_out['beam_as']), 10, cmap=plt.cm.bwr )
    cs2 = s2.contourf( plot_out.x, plot_out.y, 180/np.pi*np.angle(plot_out['beam_as']), 10, cmap=plt.cm.bwr )

    cbar1 = fig.colorbar(cs1, ax=s1, cmap=plt.cm.bwr)
    cbar2 = fig.colorbar(cs2, ax=s2, cmap=plt.cm.bwr)

    s1.axis('equal')
    s2.axis('equal')

    s1.set_title('AS port phase detector (Mag top, phase bottom)' + r'$\Delta$RoC' + f' = {phase_camera_delta_RoC:.1e} m')
    s1.set_ylabel('Detector Y-position')
    s2.set_ylabel('Detector Y-position')
    s2.set_xlabel('Detector X-position')

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.6)
    s2.grid()
    s2.grid(which='minor', ls='--', alpha=0.6)

    plt.tight_layout()

    # Save the figure
    plotname = 'michelson_mode_mismatch_AS_port_phase_camera.pdf'
    full_plotname = '{}/{}'.format(fig_dir, plotname)
    plot_names = np.append(plot_names, full_plotname)
    print('Writing plot PDF to {}'.format(full_plotname))
    plt.savefig(full_plotname, bbox_inches='tight')
    plt.close()



# Many AS port phase camera pic with mode mismatch
if plot_phase_camera:
    fig, ss = plt.subplots(2, len(Rc_log2), sharex='col')

    for ii, rc2 in enumerate(Rc_log2):
        plot_out = out_mismatch_phase_camera_dict[rc2]
        s1 = ss[0,ii]
        s2 = ss[1,ii]

        levels = np.linspace(0, 0.5, 13)
        levels2 = np.linspace(-180, 180, 13)
        cs1 = s1.contourf( plot_out.x, plot_out.y, np.abs(plot_out['beam_as']), 13, cmap=plt.cm.bwr )
        cs2 = s2.contourf( plot_out.x, plot_out.y, 180/np.pi*np.angle(plot_out['beam_as']), levels2, cmap=plt.cm.bwr )

        # cbar1 = fig.colorbar(cs1, ax=s1, cmap=plt.cm.bwr)
        # cbar2 = fig.colorbar(cs2, ax=s2, cmap=plt.cm.bwr)

        s1.axis('equal')
        s2.axis('equal')

        s1.tick_params(axis='both', which='major', labelsize=10)
        s2.tick_params(axis='both', which='major', labelsize=10)

        s1.set_title(r'$\Delta RoC$' + f' = {rc2} m', fontsize=12)
        s2.set_xlabel('X-position')

        s1.grid()
        s1.grid(which='minor', ls='--', alpha=0.6)
        s2.grid()
        s2.grid(which='minor', ls='--', alpha=0.6)

    # s1.set_title('AS port phase detector (Mag top, phase bottom)' + r'$\Delta$RoC' + f' = {phase_camera_delta_RoC:.1e} m')
    ss[0,0].set_ylabel('Detector Y-position')
    ss[1,0].set_ylabel('Detector Y-position')

    cbar1 = fig.colorbar(cs1, ax=s1, cmap=plt.cm.bwr)
    cbar2 = fig.colorbar(cs2, ax=s2, cmap=plt.cm.bwr)

    # plt.tight_layout()

    # Save the figure
    plotname = 'many_michelson_mode_mismatch_AS_port_phase_camera.pdf'
    full_plotname = '{}/{}'.format(fig_dir, plotname)
    plot_names = np.append(plot_names, full_plotname)
    print('Writing plot PDF to {}'.format(full_plotname))
    plt.savefig(full_plotname, bbox_inches='tight')
    plt.close()



# Frequency noise to AS port TF given different TEM injection
fig, (s1, s2) = plt.subplots(2, sharex='col')

for tem in tem_log:
    plot_out = out_laser_freq_TF_tem_dict[tem] # different simulation results
    label = r'tem = %.0e'%(tem)
    finesse_TF = plot_out['as_power_f1_I'] + 1j * plot_out['as_power_f1_Q']

    p1, = s1.loglog(fflog, np.abs(finesse_TF), alpha=0.5, label=label)
    s2.semilogx(fflog, 180/np.pi*np.angle(finesse_TF), alpha=0.5, label=label)

s1.axvline(x=FSR, ls='--', color='k', label='FSR')
s2.axvline(x=FSR, ls='--', color='k', label='FSR')

s1.set_title('Frequency noise to AS port TF given different HOM injected')

s1.set_ylabel(r'Mag $\left| P_{as}/\delta\nu \right|$ [W/Hz]')
s2.set_ylabel(r'Phase [deg]')

s2.set_xlabel(r'Frequency [Hz]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)
s2.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(90*np.arange(-2, 3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'michelson_freq_noise_coupling_with_changing_HOMs_injected.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Frequency injection monitored with ITMX Transmission change
fig, (s1, s2) = plt.subplots(2, sharex='col')

for phi in phis_my:
    plot_out = out_T_mismatch_freq_inj_dict[phi]

    label = r'$\phi_y$' + f' = {phi:.0f}'
    finesse_TF = plot_out['as_power_f1_I'] + 1j * plot_out['as_power_f1_Q']

    p1, = s1.loglog(100*T_ITMX_log, np.abs(finesse_TF), alpha=0.5,  label=label)
    s2.semilogx(100*T_ITMX_log, 180/np.pi*np.angle(finesse_TF), alpha=0.5, label=label)

    TF = analytic_T_dict[phi] 
    s1.loglog(100*T_ITMX_log, np.abs(TF), ls='--', color=p1.get_color(), label='Analytic ' + label)
    s2.semilogx(100*T_ITMX_log, 180/np.pi*np.angle(TF), ls='--', color=p1.get_color(), label='Analytic ' + label)

label = 'Nominal ' + r'$T_\mathrm{IX} = %.1f$'%(100*tix**2) + r' [\%]'
s1.axvline(x=100*tix**2, ls='--', color='k', label=label)
s2.axvline(x=100*tix**2, ls='--', color='k', label=label)

s1.set_title('Freq to AS power TF - ' 
            + r'$\Delta T_\mathrm{IX}$' 
            + ' - ' 
            + r'$T_\mathrm{IX} = %.2f$'%(100*tix**2) + r' \%', 
            y=1.02)

s1.set_ylabel(r'Mag $ P_\mathrm{AS}/\delta \nu $ [W/Hz]')
s2.set_ylabel(r'Phase [deg]')

s2.set_xlabel(r'IX Transmission $T_\mathrm{IX}$ [\%]')

xmin, xmax = s1.get_xlim()
x_ticks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
s1.set_xticks(x_ticks)
s2.set_xticks(x_ticks)

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(-90*np.arange(-2,3))
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='lower right', fontsize=12)

plt.tight_layout()

# Save the figure
plotname = 'michelson_frequency_noise_coupling_to_AS_with_changing_ITMX_transmission.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# End
print()
print('Plotnames')
print('open', end=' ')
for pn in plot_names:
    print('{}'.format(pn), end=' ')
print()