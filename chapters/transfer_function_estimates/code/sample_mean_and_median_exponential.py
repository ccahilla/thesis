'''
sample_mean_and_median_exponential.py

Computes the sample mean and median PDFs 
for small numbers of samples of the exponential distribution.
Sample median argument follows FINDCHIRP
https://journals.aps.org/prd/pdf/10.1103/PhysRevD.85.122006
closely.

Craig Cahillane
September 9, 2020
'''

#####   Imports   #####
import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

from scipy.special import beta, factorial, gamma, poch

import mpmath

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 18, # 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 14,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Functions   #####
def sign_fixed(x, loc):
    '''Fixes point where x == loc, if it exists'''
    sign = np.sign(x - loc)
    try:
        idx = np.argwhere(sign == 0)[0][0]
    except IndexError:
        idx = None
    if idx is not None:
        sign[idx] = 1
    return sign

def exponential_pdf(x, scale):
    '''Defines an asymmetric laplace probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    scale   = scale parameter for broadening the PDF width
    Output:
    pdf     = asymmetric laplace probability distribution function on input x
    '''
    pdf = 1 / scale * np.exp(-x / scale)
    return pdf

def exponential_cdf(x, scale):
    '''Defines an asymmetric laplace cummulative distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    scale   = scale parameter for broadening the PDF width
    Output:
    cdf     = asymmetric laplace cummulative distribution function on input x
    '''
    cdf = 1 - np.exp(-x / scale)
    return cdf

def exponential_sample_median_pdf(x, scale, nn):
    '''Defines an asymmetric laplace sample median probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    scale   = scale parameter for broadening the PDF width
    nn      = number of samples. Odd numbers are best.
    Output:
    pdf     = asymmetric laplace sample median probability distribution function
    '''
    mm = (nn - 1) // 2
    pdf0 = exponential_pdf(x, scale)
    cdf0 = exponential_cdf(x, scale)

    bb = 1/beta(mm + 1, mm + 1)

    pdf = bb * cdf0**mm * (1 - cdf0)**mm * pdf0
    return pdf

def exponential_sample_median_mean(scale, nn):
    '''Calculates the mean of the sample median.
    Inputs:
    scale   = scale parameter for broadening the PDF width
    nn      = number of samples. Odd numbers only.
    Output:
    mean    = asymmetric laplace mean of the sample median for nn samples
    '''
    ll = np.arange(1, nn+1)
    num = (-1 * np.ones_like(ll))**(ll + 1)
    mean = scale * np.sum(num / ll) 
    return mean

def exponential_sample_median_variance(scale, nn):
    '''Calculates the variance of the sample median estimate,
    from Laplace's sample median proof.
    '''
    sample_median_mean = exponential_sample_median_mean(scale, nn)
    pdf_at_median = exponential_pdf(sample_median_mean, scale)

    sample_median_variance = 1/(4 * nn * pdf_at_median**2) # Laplace's sample median
    return sample_median_variance

def exponential_sample_median_pdf_approx(x, scale, nn):
    '''Normal approximation to the asymmetric Laplace sample median pdf
    '''
    mu = exponential_sample_median_mean(scale, nn)
    sigma2 = exponential_sample_median_variance(scale, nn)

    pdf = 1/np.sqrt(2 * np.pi * sigma2) * np.exp(-(x - mu)**2/(2 * sigma2))
    return pdf

def exponential_sample_mean_pdf(x, scale, nn):
    '''Defines an asymmetric laplace sample mean probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    scale   = scale parameter for broadening the PDF width
    nn      = number of samples.
    Output:
    pdf     = asymmetric laplace sample mean probability distribution function
    '''
    pdf = nn * (nn * x)**(nn - 1) * np.exp(- nn * x / scale) / (scale**nn * gamma(nn))
    return pdf


#####   Parameters   #####
lambda0 = 1.0
x0 = np.linspace(0, 2.5, 1001)

params0 = np.array([lambda0])

# nns = np.arange(1, 40, 4)
nns = np.array([3, 11, 33, 101, ])

#####   Figures Directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Figures   #####
# check pdf and cdf
fig, (s1) = plt.subplots(1)

s1.plot(x0, exponential_pdf(x0, *params0), label=f'PDF $\lambda$ = {lambda0}')
s1.plot(x0, exponential_cdf(x0, *params0), label=f'CDF')

mean = lambda0
median = lambda0 * np.log(2)
s1.plot([mean, mean],       [0, 1], color='C0', ls='-', alpha=0.6, label=f'mean = {mean:.3f}')
s1.plot([median, median],   [0, 1], color='C0', ls='--', alpha=0.6, label=f'median = {median:.3f}')

# s1.set_title('Asymmetric Laplace distribution functions')
s1.set_ylabel('Exponential PDF, CDF')
s1.set_xlabel('Sample space $x$')

s1.set_xlim([x0[0], x0[-1]])
s1.set_ylim([0, 1])

s1.grid()
s1.legend()

plot_name = f'exponential_pdf_and_cdf.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# sample mean
fig, (s1) = plt.subplots(1)

s1.plot(x0, exponential_pdf(x0, *params0), label=r'$f_{\hat{\mu}}(x|n = 1)$ ' + f'$\lambda$ = {lambda0}')

mean = lambda0
median = lambda0 * np.log(2)

max_pdf = 0
for nn in nns:
    temp_pdf = exponential_sample_mean_pdf(x0, *params0, nn)
    max_pdf = 1.01 * np.max(temp_pdf)

    p1, = s1.plot(x0, temp_pdf, label=r'$f_{\hat{\mu}}' + f'(x|n = {nn})$')

s1.plot([mean, mean],       [0, max_pdf], color='C0', ls='-', alpha=0.6, label=f'mean = {mean:.3f}')
s1.plot([median, median],   [0, max_pdf], color='C0', ls='--', alpha=0.6, label=f'median = {median:.3f}')

# s1.set_title('Asymmetric Laplace sample mean')
s1.set_ylabel('Exponential sample mean PDF ' + r'$f_{\hat{\mu}}(x)$')
s1.set_xlabel('Power spectral density sample space $x$ ' + r'[$\mathrm{V}^2/\mathrm{Hz}$]')

s1.set_xlim([x0[0], x0[-1]])
s1.set_ylim([0, max_pdf])

s1.grid()
s1.legend()

plot_name = f'exponential_sample_mean_pdfs.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# sample median
fig, (s1) = plt.subplots(1)

s1.plot(x0, exponential_pdf(x0, *params0), label=r'$f_{\hat{\rho}}(x|n = 1)$ ' + f'$\lambda$ = {lambda0}')

mean = lambda0
median = lambda0 * np.log(2)

temp_pdf = exponential_sample_median_pdf(x0, *params0, nns[-1])
max_pdf =  1.01 * np.max(temp_pdf)
for nn in nns:
    temp_pdf = exponential_sample_median_pdf(x0, *params0, nn)
    temp_mean = exponential_sample_median_mean(*params0, nn)
    temp_var = exponential_sample_median_variance(*params0, nn)
    approx_temp_pdf = exponential_sample_median_pdf_approx(x0, *params0, nn)

    p1, = s1.plot(x0, temp_pdf, label=r'$f_{\hat{\rho}}' + f'(x|n = {nn})$')
    s1.plot([temp_mean, temp_mean], [0, max_pdf], color=p1.get_color(), ls='-', alpha=0.6, label=r'$\langle \hat{\rho} \rangle$ = '+f'{temp_mean:.3f}')
    s1.plot(x0, approx_temp_pdf, ls='--', color=p1.get_color(), alpha=0.6, label=r'$\mathcal{N}('+ f'{temp_mean:.3f},{temp_var:.3f}'+')$')

s1.plot([mean, mean],       [0, max_pdf], color='C0', ls='-', alpha=0.6, label=f'mean = {mean:.3f}')
s1.plot([median, median],   [0, max_pdf], color='C0', ls='--', alpha=0.6, label=f'median = {median:.3f}')

# s1.set_title('Asymmetric Laplace sample median')
s1.set_ylabel('Exponential sample median PDF ' + r'$f_{\hat{\rho}}(x)$')
s1.set_xlabel('Power spectral density sample space $x$ ' + r'[$\mathrm{V}^2/\mathrm{Hz}$]')

s1.set_xlim([x0[0], x0[-1]])
s1.set_ylim([0, max_pdf])

s1.grid()
s1.legend()

plot_name = f'exponential_sample_median_pdfs.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()