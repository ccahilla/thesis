'''
transfer_function_histograms.py

Plot histograms of a transfer function estimate \hat{H}

Craig Cahillane
August 10, 2020
'''

import os
import sys
import time
import copy
import numpy as np
# import scipy.signal as sig
# import scipy.fft as fft

from scipy.stats import expon, rayleigh, norm, chi2, f
from scipy.special import kn, gamma, iv, betainc
from scipy.optimize import curve_fit

import matplotlib as mpl
import matplotlib.pyplot as plt

import mpmath

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 18, # 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})



#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])


#####   Functions   #####
def mid_bins(bins):
    '''Returns the middle of bins given from the native mpl.hist() output'''
    diff_bins = np.diff(bins)
    middle_bins = bins[:-1] + diff_bins
    return middle_bins

def sample_ordinary_coherence_pdf(xx, nn, true_coherence):
    '''Returns the probability density function of the ordinary coherence on xx
    given the number of samples nn and the true_coherence.
    From Equation (11) of https://apps.dtic.mil/dtic/tr/fulltext/u2/619993.pdf
    '''
    prefactor = (nn - 1) * (1 - true_coherence)**nn * (1 - xx)**(nn - 2)
    hyp = np.array([])
    for x in xx:
        temp_hyp = float( mpmath.hyp2f1(nn, nn, 1, x * true_coherence ) )
        hyp = np.append(hyp, temp_hyp)
    pdf = prefactor * hyp
    return pdf

def sample_b_over_a_pdf(xx, nn):
    '''Returns the probability density function of (n-1) \hat{B}/\hat{A},
    which is an F-distribution with degrees of freedom n1 = 2 and n2 = nn-2
    c.f. Equation 18 of https://apps.dtic.mil/dtic/tr/fulltext/u2/619993.pdf,
    with n -> 2n
    '''
    return f.pdf(xx, 2, nn - 2)

def sample_r_pdf(xx, nn, true_coherence):
    '''Returns the probability density function of r = sqrt(c^2 + q^2),
    the mean magnitude of the CSD complex number estimated from nn averages.
    From Equation 4.119 of Goodman's thesis: https://apps.dtic.mil/sti/pdfs/AD0134919.pdf
    Inputs:
    xx = sample space for the magnitude r
    nn = number of averages (i.e. degrees of freedom)
    true_coherence = the true coherence of the estimate
    Output:
    pdf = probability density function p_r(xx)
    '''
    delta = 1 - true_coherence
    amp_coh = np.sqrt(true_coherence)
    nn = int(nn)
    
    prefactor = delta**nn / (2**(nn-1) * gamma(nn)) 
    bessel_first = iv(0, amp_coh * xx)
    bessel_second = kn(nn - 1, xx)
    postfactor = xx**nn
    pdf = prefactor * bessel_first * bessel_second * postfactor
    return pdf

def sample_r_pdf_caller(xx, nn, true_coherence, pdf_scaler=1.0):
    '''Calls the function sample_r_pdf, with a pdf_scaler.
    The pdf_scaler scales the xaxis we call sample_r_pdf() with,
    and normalizes the resulting pdf: f_x(pdf_scaler * x) / pdf_scaler.
    Good for casting the pdf of r 
    into a pdf of R = r * sigma_x * sigma_y * (1 - coherence) / nn
    Inputs:
    xx = sample space for the magnitude R
    nn = number of averages (i.e. degrees of freedom)
    true_coherence  = the true coherence of the estimate
    pdf_scaler      = scales and normalizes the pdf for R
    Output:
    pdf = probability density function p_R(xx)    
    '''
    yy = xx / pdf_scaler
    pdf = sample_r_pdf(yy, nn, true_coherence) / pdf_scaler
    return pdf

def sample_gain_infinite_sum_func(kk, x0, nn, true_coherence):
    '''Function to input into mpmath.nsum() for the infinite sum calculation.
    Inputs:
    kk  = sum index as supplied by mpmath.nsum
    x0  = sample gain to calculate sum for 
    nn  = number of averages (i.e. degrees of freedom)
    true_coherence  = the true coherence of the estimate
    Output:
    sum_term    = evalution of the sum up to the tolerance in mpmath.nsum()
    '''
    kk = int(kk)
    amp_coh = np.sqrt(true_coherence)
    total = gamma(kk + 0.5) * gamma(nn - kk + 0.5) / (gamma(kk + 1) * gamma(nn - kk + 1)) \
            * ((1 - 2 * amp_coh * x0 + x0**2)/(1 + 2 * amp_coh * x0 + x0**2))**kk
    return total

def sample_gain_infinite_sum(xx, nn, true_coherence):
    '''Calculates the infinite sum for g = xx from Equation 4.97 
    of Goodman's thesis: https://apps.dtic.mil/sti/pdfs/AD0134919.pdf
    Inputs:
    xx  = sample gains to calculate sum for 
    nn  = number of averages (i.e. degrees of freedom)
    true_coherence  = the true coherence of the estimate
    Output:
    sums   = evalution of the sum up to the tolerance in mpmath.nsum()
    '''
    kk_interval = [0, mpmath.inf]
    sums = np.zeros_like(xx)
    for ii, x0 in enumerate(xx):
        def nsum_func(kk):
            return sample_gain_infinite_sum_func(kk, x0, nn, true_coherence)
        sums[ii] = mpmath.nsum(nsum_func, kk_interval)
    return sums

def sample_gain_pdf(xx, nn, true_coherence):
    '''Returns the probability density function of g = sqrt(c^2 + q^2)/a,
    the mean magnitude |H| of the transfer function H = CSDxy/PSDx estimated from nn averages.
    From Equation 4.97 of Goodman's thesis: https://apps.dtic.mil/sti/pdfs/AD0134919.pdf
    Inputs:
    xx = sample space for the magnitude r
    nn = number of averages (i.e. degrees of freedom)
    true_coherence = the true coherence of the estimate
    Output:
    pdf = probability density function p_g(xx)
    '''
    delta = 1 - true_coherence
    amp_coh = np.sqrt(true_coherence)

    sums = sample_gain_infinite_sum(xx, nn, true_coherence)
    prefactor = 2 * nn * delta**nn * xx / ( np.pi * (1 + 2 * amp_coh * xx + xx**2)**0.5 * (1 - 2 * amp_coh * xx + xx**2)**(nn+0.5) )
    pdf = prefactor * sums
    return pdf

def sample_gain_pdf_caller(xx, nn, true_coherence, pdf_scaler=1.0):
    '''Calls the function sample_gain_pdf, with a pdf_scaler.
    The pdf_scaler scales the xaxis we call sample_gain_pdf() with,
    and normalizes the resulting pdf: f_x(pdf_scaler * x) / pdf_scaler.
    Good for casting the pdf of g 
    into a pdf of G = g * sigma_y / sigma_x
    Inputs:
    xx = sample space for the magnitude transfer function G
    nn = number of averages (i.e. degrees of freedom)
    true_coherence  = the true coherence of the estimate
    pdf_scaler      = scales and normalizes the pdf for G
    Output:
    pdf = probability density function p_G(xx)    
    '''
    yy = xx / pdf_scaler
    pdf = sample_gain_pdf(yy, nn, true_coherence) / pdf_scaler
    return pdf

def sample_gain_pdf_approximation(xx, nn, true_coherence):
    '''Computes the usual normal approximation to 
    the transfer function gain distribution.
    mean(|H/H0|) = gamma
    variance(|H/H0|) = (1 - gamma^2)/(2 nn gamma^2)
    Inputs:
    xx = sample space for the magnitude transfer function G
    nn = number of averages (i.e. degrees of freedom)
    true_coherence  = the true coherence of the estimate
    Output:
    pdf = probability density function p_G(xx)    
    '''
    delta = 1 - true_coherence
    mean = np.sqrt(true_coherence)
    var = delta/(2 * nn) #(2 * nn * true_coherence)

    approx_pdf = 1/np.sqrt(2 * np.pi * var) * np.exp( -(xx - mean)**2/(2 * var) )
    return approx_pdf

def sample_gain_pdf_approximation_caller(xx, nn, true_coherence, pdf_scaler=1.0):
    '''Calls the function sample_gain_pdf, with a pdf_scaler.
    The pdf_scaler scales the xaxis we call sample_gain_pdf() with,
    and normalizes the resulting pdf: f_x(pdf_scaler * x) / pdf_scaler.
    Good for casting the pdf of g 
    into a pdf of G = g * sigma_y / sigma_x
    Inputs:
    xx = sample space for the magnitude transfer function G
    nn = number of averages (i.e. degrees of freedom)
    true_coherence  = the true coherence of the estimate
    pdf_scaler      = scales and normalizes the pdf for G
    Output:
    pdf = probability density function p_G(xx)    
    '''
    yy = xx / pdf_scaler
    pdf = sample_gain_pdf_approximation(yy, nn, true_coherence) / pdf_scaler
    return pdf

def sample_phase_pdf(phi, nn, true_coherence, phi0):
    '''Returns the probability density function of phi = arg((c + iq)/a),
    the mean phase arg(H) of the transfer function H = CSDxy/PSDx estimated from nn averages.
    From Equation 4.97 of Goodman's thesis: https://apps.dtic.mil/sti/pdfs/AD0134919.pdf
    Inputs:
    xx = sample space for the magnitude r
    nn = number of averages (i.e. degrees of freedom)
    true_coherence = the true coherence of the estimate
    Output:
    pdf = probability density function p_g(xx)
    '''
    delta = 1 - true_coherence
    amp_coh = np.sqrt(true_coherence)
    ss = -amp_coh * np.cos(phi - phi0)

    incomplete_beta = betainc(0.5, nn + 0.5, ss**2)

    pdf = delta**nn/(2 * np.pi) - nn * delta**nn * ss / (2 * np.pi * (1 - ss**2)**(nn + 0.5)) * gamma(0.5) * gamma(nn + 0.5) / gamma(nn + 1) * (1 + incomplete_beta)
    return pdf

def sample_phase_pdf_approximation(phi, nn, true_coherence, phi0):
    '''Computes the usual normal approximation to 
    the transfer function gain distribution.
    mean(|H/H0|) = gamma
    variance(|H/H0|) = (1 - gamma^2)/(2 nn gamma^2)
    Inputs:
    xx = sample space for the magnitude transfer function G
    nn = number of averages (i.e. degrees of freedom)
    true_coherence  = the true coherence of the estimate
    Output:
    pdf = probability density function p_G(xx)    
    '''
    mean = phi0

    delta = 1 - true_coherence
    var = delta/(2 * nn * true_coherence)

    approx_pdf = 1/np.sqrt(2 * np.pi * var) * np.exp( -(phi - mean)**2/(2 * var) )
    return approx_pdf


def sign_fixed(x, loc):
    '''Fixes point where x == loc, if it exists'''
    sign = np.sign(x - loc)
    try:
        idx = np.argwhere(sign == 0)[0][0]
    except IndexError:
        idx = None
    if idx is not None:
        sign[idx] = 1
    return sign

def asymmetric_laplace_pdf(x, loc, scale, kappa):
    '''Defines an asymmetric laplace probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplace
    scale   = scale parameter for broadening the PDF width, aka lambda
    kappa   = asymmetric parameter 
    Output:
    pdf     = asymmetric laplace probability distribution function on input x
    '''
    sign = sign_fixed(x, loc) # either +1 or -1 
    pdf = (1 / (scale * (kappa + 1/kappa))) * np.exp(-sign * (x - loc) * kappa**sign / scale)
    return pdf

def laplace_squared_pdf(x, scale):
    '''Defines the laplace squared probability density function on x.
    Inputs:
    x       = vector of inputs to the PDF 
    scale   = scale parameter of the original laplace distribution 
    Output:
    pdf     = laplace squared distribution on x in [0, infinity)
    '''
    pdf = 1/(2 * scale * np.sqrt(x)) * np.exp( -np.sqrt(x) / scale )
    return pdf

def asymmetric_laplace_magnitude_pdf(x, loc, scale, kappa):
    '''Defines the magnitude of the asymmetric laplace distribution for x on [0, infinity)
    Should be equivalant to |asymmetric_laplace_pdf(x, m, lambda, kappa)|
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplace
    scale   = scale parameter for broadening the PDF width, aka lambda
    kappa   = asymmetric parameter 
    Output:
    pdf     = asymmetric laplace probability distribution function on input x
    '''
    pdf = 1/(scale * (kappa + 1/kappa)) * ( np.exp(-(x - loc) / (kappa * scale)) + np.exp(-(x - loc) * kappa / scale) )
    return pdf

def asymmetric_laplace_squared_pdf(x, scale, kappa):
    '''Defines the laplace squared probability density function on x.
    Inputs:
    x       = vector of inputs to the PDF 
    scale   = scale parameter of the original laplace distribution 
    kappa   = asymmetric parameter 
    Output:
    pdf     = laplace squared distribution on x in [0, infinity)
    '''
    pdf = 1/(2 * scale * (kappa + 1/kappa) * np.sqrt(x)) * ( np.exp( -np.sqrt(x) / (scale * kappa) ) + np.exp( -kappa * np.sqrt(x) / scale ) )
    return pdf

#####   Random data for estimate   #####

# Plot real part of transfer function H = CSDab/PSDa = <x,y>/<x,x>
# where x = a + c, y = b + H c
# <x,y> = <a,b> + H <a,c> + <c,b> + H <c,c>
# <x,x> = <a,a> + <c,c>
# Re(H) = P/R
# Im(H) = Q/R

HH = 2.0 * np.exp(1j * np.pi/3) # true TF value we are estimating
HHr = np.real(HH)
HHi = np.imag(HH)
HH2 = np.abs(HH)**2

# Power levels
p_aa = 0.5 #sigma_aa**2
p_bb = 1.0 #sigma_bb**2
p_cc = 0.3 #sigma_cc**2

sigma_aa = np.sqrt(p_aa)
sigma_bb = np.sqrt(p_bb)
sigma_cc = np.sqrt(p_cc)

# Asymmetric laplace params
# Good only along the major axis, rotated by arg(HH)= np.pi/4
lambda1 = np.sqrt( p_aa * p_bb + np.abs(HH)**2 * p_aa * p_cc + p_bb * p_cc )
kappa1 = np.sqrt( 1 + 
                    (2 * np.abs(HH)**2 * p_cc**2 - 2 * p_cc * np.sqrt( np.abs(HH)**2 * (p_aa + p_cc) * (p_bb + np.abs(HH)**2 * p_cc) )) \
                    / (p_aa * p_bb + np.abs(HH)**2 * p_aa * p_cc + p_bb * p_cc) \
                )

power_ratio = (np.abs(HH)**2 * p_aa * p_cc + p_aa * p_bb + p_bb * p_cc) / (np.abs(HH)**2 * p_cc**2)
coherence = 1 / (1 + power_ratio)

tf_bias = 1/(1 + p_aa/p_cc)

true_csd_mean = np.sqrt(HH2) * 2 * p_cc

# Random data
sample_number = 25 # number of averages n

AAr = np.random.normal(scale=sigma_aa, size=sample_number)
BBr = np.random.normal(scale=sigma_bb, size=sample_number)
CCr = np.random.normal(scale=sigma_cc, size=sample_number)

AAi = np.random.normal(scale=sigma_aa, size=sample_number)
BBi = np.random.normal(scale=sigma_bb, size=sample_number)
CCi = np.random.normal(scale=sigma_cc, size=sample_number)


MMr = AAr*BBr + HH * AAr*CCr + CCr*BBr + HH * CCr**2
MMi = AAi*BBi + HH * AAi*CCi + CCi*BBi + HH * CCi**2
PPP = MMr + MMi

QQQ = AAr * BBi - AAi * BBr \
    + HH * (AAr * CCi - AAi * CCr) \
    + CCr * BBi - CCi * BBr

RRR = AAr**2 + CCr**2 + AAi**2 + CCi**2 + 2 * (AAr * CCr + AAi * CCi) # estimate of <x,x>
# RRR = AAr**2 + CCr**2 + AAi**2 + 2*AAi*CCi + CCi**2 + 2*AAr*CCr 

# TTT = BBr**2 + BBi**2 + HH2 * (CCr**2 + CCi**2) + 2 * ( np.real(HH) * (BBr * CCr + BBi * CCi) + np.imag(HH) * (BBr * CCi - BBi * CCr) )  # estimate of <y,y>
TTT = BBi**2 + BBr**2 + 2 * BBi * (CCr * HHi + CCi * HHr) + BBr * (-2 * CCi * HHi + 2 * CCr * HHr) + (CCi**2 + CCr**2) * (HHi**2 + HHr**2)

SSS = (AAi + 1j * AAr + CCi + 1j * CCr) *  (BBi - 1j * BBr + (1j * CCi + CCr) * (HHi - 1j * HHr)) # estimate of <x,y>

PPP2_mean = np.mean(np.abs(PPP)**2)
QQQ2_mean = np.mean(np.abs(QQQ)**2)
PPP_mean = np.mean(PPP)
QQQ_mean = np.mean(QQQ)
sample_psdx = np.mean(RRR)
sample_psdy = np.mean(TTT)
sample_csdxy = np.mean(SSS)

cohhat = np.abs( sample_csdxy )**2 / ( sample_psdx * sample_psdy)

print()
print(f'sample_number n  = {sample_number}')
print(f'lambda1 = {lambda1}')
print(f'kappa1 = {kappa1}')
print(f'coherence = {coherence}')
print(f'cohhat = {cohhat}')
print(f'mean CSDxz^2 = {np.abs(PPP_mean + 1j * QQQ_mean )**2}')
print(f'mean PSDx = {sample_psdx}')
print(f'mean PSDy = {sample_psdy}')
print(f'mean TF = |CSDxz|/PSDx = {np.abs(PPP_mean + 1j * QQQ_mean ) / sample_psdx}')
print(f'mean TF = sqrt(PSDz/PSDx) = {np.sqrt(sample_psdy / sample_psdx)}')
print()

###  Make for loop of TF and coherence estimates for histogram plot

num_estimates = 100000

data_dict = {}
sample_numbers = np.array([5, 25, 100])

for nn in sample_numbers:
    data_dict[nn]  = {}

    psdx = np.zeros(num_estimates)
    psdy = np.zeros(num_estimates)
    csdxy = np.zeros(num_estimates, dtype=complex)
    tfs = np.zeros(num_estimates, dtype=complex)
    cohs = np.zeros(num_estimates)
    ffs = np.zeros(num_estimates)

    co_spectra = np.zeros(num_estimates)     # not stritctly co and quad spectra as usually defined,
    quad_spectra = np.zeros(num_estimates)   # more like major and minor axes of the CSD.
    for est in np.arange(num_estimates):
        AAr = np.random.normal(scale=sigma_aa, size=nn)
        BBr = np.random.normal(scale=sigma_bb, size=nn)
        CCr = np.random.normal(scale=sigma_cc, size=nn)

        AAi = np.random.normal(scale=sigma_aa, size=nn)
        BBi = np.random.normal(scale=sigma_bb, size=nn)
        CCi = np.random.normal(scale=sigma_cc, size=nn)

        # estimate of <x,y> along correlated axis
        MMr = AAr*BBr + HH * AAr*CCr + CCr*BBr + HH * CCr**2
        MMi = AAi*BBi + HH * AAi*CCi + CCi*BBi + HH * CCi**2
        PPP = MMr + MMi

        # estimate of <x,y> along uncorrelated axis
        QQQ = AAr * BBi - AAi * BBr \
            + HH * (AAr * CCi - AAi * CCr) \
            + CCr * BBi - CCi * BBr

        # estimate of <x,x>
        RRR = AAr**2 + CCr**2 + AAi**2 + CCi**2 + 2 * (AAr * CCr + AAi * CCi) 
        # AAi**2 + AAr**2 + 2*AAi*CCi + CCi**2 + 2*AAr*CCr + CCr**2

        # estimate of <y,y>
        # TTT = BBr**2 + BBi**2 + HH2 * (CCr**2 + CCi**2) + 2 * ( np.real(HH) * (BBr * CCr + BBi * CCi) + np.imag(HH) * (BBr * CCi - BBi * CCr) )
        TTT = BBi**2 + BBr**2 + 2 * BBi * (CCr * HHi + CCi * HHr) + BBr * (-2 * CCi * HHi + 2 * CCr * HHr) + (CCi**2 + CCr**2) * (HHi**2 + HHr**2)

        SSS = (AAi + 1j * AAr + CCi + 1j * CCr) *  (BBi - 1j * BBr + (1j * CCi + CCr) * (HHi - 1j * HHr)) # estimate of <x,y>

        PPP2_mean = np.mean(np.abs(PPP)**2)
        QQQ2_mean = np.mean(np.abs(QQQ)**2)
        PPP_mean = np.mean(PPP)
        QQQ_mean = np.mean(QQQ)
        sample_psdx = np.mean(RRR) # \hat(G_xx)
        sample_psdy = np.mean(TTT) # \hat(G_yy)
        sample_csdxy = np.mean(SSS) # \hat(G_xy)

        sample_tf  = sample_csdxy / sample_psdx
        sample_coh = np.abs( sample_csdxy )**2 / ( sample_psdx * sample_psdy )

        sample_A = sample_psdy - np.abs(sample_csdxy)**2 / sample_psdx
        sample_B = np.abs(sample_tf - HH * tf_bias)**2 * sample_psdx
        sample_F = (sample_number - 1) * sample_B / sample_A

        psdx[est] = sample_psdx
        psdy[est] = sample_psdy
        csdxy[est] = sample_csdxy
        tfs[est] = sample_tf
        cohs[est] = sample_coh
        ffs[est] = sample_F

    data_dict[nn]['psdx']  = psdx
    data_dict[nn]['psdy']  = psdy
    data_dict[nn]['csdxy'] = csdxy
    data_dict[nn]['tfs']   = tfs
    data_dict[nn]['cohs']  = cohs
    data_dict[nn]['ffs']   = ffs

# Single large CSD sampling
HH0 = 2.0 # tf
HH02 = np.abs(HH0)**2
sample_number2 = 100000

lambda2 = np.sqrt( p_aa * p_bb + HH02 * p_aa * p_cc + p_bb * p_cc )
kappa2 = np.sqrt( 1 + 
                    (2 * HH02 * p_cc**2 - 2 * p_cc * np.sqrt( HH02 * (p_aa + p_cc) * (p_bb + HH02 * p_cc) )) \
                    / (p_aa * p_bb + HH02 * p_aa * p_cc + p_bb * p_cc) \
                )

power_ratio2 = (np.abs(HH0)**2 * p_aa * p_cc + p_aa * p_bb + p_bb * p_cc) / (np.abs(HH0)**2 * p_cc**2)
coherence2 = 1 / (1 + power_ratio2)

tf_bias2 = 1/(1 + p_aa/p_cc)

AAr = np.random.normal(scale=sigma_aa, size=sample_number2)
BBr = np.random.normal(scale=sigma_bb, size=sample_number2)
CCr = np.random.normal(scale=sigma_cc, size=sample_number2)

AAi = np.random.normal(scale=sigma_aa, size=sample_number2)
BBi = np.random.normal(scale=sigma_bb, size=sample_number2)
CCi = np.random.normal(scale=sigma_cc, size=sample_number2)

# estimate of <x,y> along correlated axis
MMr = AAr*BBr + HH0 * AAr*CCr + CCr*BBr + HH0 * CCr**2
MMi = AAi*BBi + HH0 * AAi*CCi + CCi*BBi + HH0 * CCi**2
PPP = MMr + MMi

# estimate of <x,y> along uncorrelated axis
QQQ = AAr * BBi - AAi * BBr \
    + HH0 * (AAr * CCi - AAi * CCr) \
    + CCr * BBi - CCi * BBr

co_spectra = PPP
quad_spectra = QQQ
    
print('Done sampling')


#####   Figures   #####

### Sample 2D histograms of transfer function
plot_tfs = data_dict[sample_number]['tfs']

fig, ss = plt.subplots(2, 2, sharex="col", sharey="row", figsize=(9, 9),
                        gridspec_kw=dict(   height_ratios=[1, 3],
                                            width_ratios=[3, 1])
                        )
s1 = ss[0, 0]   # top histogram
s2 = ss[0, 1]   # delete
s3 = ss[1, 0]   # 2d hist
s4 = ss[1, 1]   # right histogram

fig.delaxes(s2) # delete

num_bins = 200
low_bin = -0.5
high_bin = 3
bins = np.linspace(low_bin, high_bin, num_bins+1)
xx = np.linspace(low_bin, high_bin, num_bins+1)

reNN, rebins, repatches = s1.hist(np.real(plot_tfs), bins=bins, histtype='step', lw=2, zorder=3, density=True,
                                    label=r'$\Re(\hat{H})$'+' samples')  
imNN, imbins, impatches = s4.hist(np.imag(plot_tfs), bins=bins, histtype='step', lw=2, zorder=3, density=True, orientation='horizontal',
                                    label=r'$\Im(\hat{H})$'+' samples')  
h, xedges, yedges, image = s3.hist2d(np.real(plot_tfs), np.imag(plot_tfs), bins=(rebins, imbins), density=True, 
                                    cmap=mpl.cm.copper, norm=mpl.colors.LogNorm())   

s1.plot([np.mean(np.real(plot_tfs)), np.mean(np.real(plot_tfs))], [0, np.max(reNN)*1.1], color='C2', 
        label=r'$\Re(\hat{H})$' + f' mean = {np.mean(np.real(plot_tfs)):.2f}')
s4.plot([0, np.max(imNN)*1.1], [np.mean(np.imag(plot_tfs)), np.mean(np.imag(plot_tfs))], color='C3', 
        label=r'$\Im(\hat{H})$' + f' mean = {np.mean(np.imag(plot_tfs)):.2f}')
s3.plot([0, np.real(HH)], [0, np.imag(HH)], lw=3, label=r'True transfer function $H$')
s3.plot([0, np.mean(np.real(plot_tfs))], [0, np.mean(np.imag(plot_tfs))], lw=3, label=r'Mean transfer function $\hat{H}$')

s1.set_xlim([-0.5, 2.0])
s3.set_xlim([-0.5, 2.0])
s3.set_ylim([-0.5, 2.0])
s4.set_ylim([-0.5, 2.0])

s3.set_xlabel(r'$\Re(\hat{H})$ samples [V/V]')
s3.set_ylabel(r'$\Im(\hat{H})$ samples [V/V]')

s1.legend(fontsize=12)
s3.legend(fontsize=12, framealpha=1)
s4.legend(fontsize=12)

s1.grid()
s3.grid()
s4.grid()

plot_name = f'sample_transfer_function_2D_histogram.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()    



### Sample magnitude and phase of the transfer function
# Start fig
fig, (s1, s2) = plt.subplots(2)

# Define bins
num_bins = 200
low_bin  = 0
high_bin = 2.0
bins1 = np.linspace(low_bin, high_bin, num_bins)
xaxis1 = np.linspace(low_bin, high_bin, 200)
low_bin2 = -0.5
high_bin2 = 2.5
bins2 = np.linspace(low_bin2, high_bin2, num_bins)
xaxis2 = np.linspace(low_bin, high_bin2, 200)

sigma_xx = np.sqrt(p_aa + p_cc)
sigma_yy = np.sqrt(p_bb + np.abs(HH)**2 * p_cc)
gain_pdf_scaler = sigma_yy / sigma_xx 

for nn in sample_numbers:
    plot_tfs = data_dict[nn]['tfs']

    # Run pdf model
    gain_pdf = sample_gain_pdf_caller(xaxis1, nn, coherence, gain_pdf_scaler)
    approx_gain_pdf = sample_gain_pdf_approximation_caller(xaxis1, nn, coherence, gain_pdf_scaler)

    phase_pdf = sample_phase_pdf(xaxis2, nn, coherence, np.angle(HH))
    approx_phase_pdf = sample_phase_pdf_approximation(xaxis2, nn, coherence, np.angle(HH))

    # Plot histogram
    NNhhmag, bins, patches = s1.hist(np.abs(plot_tfs), bins=bins1, histtype='step', lw=2, density=True, label=r'$|\hat{H}|$ samples')
    color = patches[0].get_edgecolor()

    s1.plot(xaxis1, gain_pdf, ls='--', color=color, alpha=0.7, label=r'$f(|\hat{H}| \, | ' + r'\gamma^2 = ' + f'{coherence:.3f}' + r', n = ' + f'{nn}' + r')$')

    NNhharg, bins, patches = s2.hist(np.angle(plot_tfs), color=color, bins=bins2, histtype='step', lw=2, density=True, label=r'$\hat{\phi}_H$ samples')
    s2.plot(xaxis2, phase_pdf, ls='--', color=color, alpha=0.7, label=r'$f(\hat{\phi}_H) \, | ' + r'\gamma^2 = ' + f'{coherence:.3f}' + r', n = ' + f'{nn}' + r')$')

    # if nn == sample_numbers[-1]:
    #     s1.plot(xaxis1, approx_gain_pdf, ls='-.', color=color, alpha=0.7, label=r'$\mathcal{N}(|\hat{H}|, |\hat{H}|^2 (1 - \gamma^2)/(2 n \gamma^2))$')
    #     s2.plot(xaxis2, approx_phase_pdf, ls='-.', color=color, alpha=0.7, label=r'$\mathcal{N}(\phi_0, (1 - \gamma^2)/(2 n \gamma^2))$')

    s1.plot([np.mean(np.abs(plot_tfs)), np.mean(np.abs(plot_tfs))], [0, 9], color=color, 
            label=r'$|\hat{H}|$' + f' mean = {np.mean(np.abs(plot_tfs)):.2f}')
    s2.plot([np.mean(np.angle(plot_tfs)), np.mean(np.angle(plot_tfs))], [0, 9], color=color, 
            label=r'$\hat{\phi}_H$' + f' mean = {np.mean(np.angle(plot_tfs)):.2f} rads')

s1.plot([np.abs(HH) * tf_bias, np.abs(HH) * tf_bias], [0, 9], color='k', 
        label=r'$|H|$' + f' mean = {np.abs(HH) * tf_bias:.2f}')
s2.plot([np.angle(HH), np.angle(HH)], [0, 9], color='k', 
        label=r'$\phi_H$' + f' mean = {np.angle(HH):.2f} rads')

s1.set_ylim([0, 4])
s2.set_ylim([0, 3])

# Plot labels
# s1.set_title(r'Mag and Phase histograms')
s1.set_xlabel(r'Sample transfer function gain  $|\hat{H}|$')
s2.set_xlabel(r'Sample transfer function phase $\hat{\phi_H}$')
s1.set_ylabel(r'TF gain PDF  $f(|\hat{H}| \, | \gamma^2, n)$')
s2.set_ylabel(r'TF phase PDF $f(\hat{\phi}_H | \gamma^2, n)$')

# s2.set_xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi])
# s2.set_xticklabels([r'$-\pi$',r'$-\pi/2$',r'0',r'$\pi/2$',r'$\pi$',])

s1.legend(fontsize=14)
s1.grid()
s2.legend(fontsize=14)
s2.grid()

plt.tight_layout()
plot_name = f'sample_transfer_function_histograms_mag_and_phase.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



###  Sample coherences
# Start fig
fig, (s1) = plt.subplots(1)

# Define bins
num_bins = 200
low_bin  = 0.0
high_bin =  1.0
bins = np.linspace(low_bin, high_bin, num_bins)
xaxis = np.linspace(low_bin, high_bin, 1000)

for nn in sample_numbers:
    plot_cohs = data_dict[nn]['cohs']

    # Run sample coherence model
    sample_coherence_pdf = sample_ordinary_coherence_pdf(xaxis, nn, coherence)

    # Plot histogram
    NNcoh, bins, patches = s1.hist(plot_cohs, bins=bins, histtype='step', lw=2, density=True, label=r'$\hat{\gamma}^2$ samples' + f' $n = $ {nn}')
    color = patches[0].get_edgecolor()

    # Plot sample coh pdf
    s1.plot(xaxis, sample_coherence_pdf, color=color, ls='--', alpha=0.7, label=r'$f(\hat{\gamma}^2 | ' + f'n = {nn})$')

    # Plot hist mean
    s1.plot([np.mean(plot_cohs), np.mean(plot_cohs)], [0, 20], color=color, alpha=0.7, label=r'$\overline{ \hat{\gamma}^2 }$' + f' mean = {np.mean(plot_cohs):.3f}')

s1.plot([coherence, coherence], [0, 20], ls='-', color='k', label=f'True coherence ' + r'$\gamma^2$' + f' = {coherence:.3f}')

s1.set_ylim([0, 8])

# Plot labels
s1.set_xlabel(r'Sample coherence $\hat{\gamma}^2$')
s1.set_ylabel('Sample coherence PDFs' + r' $f(\hat{\gamma}^2 | n)$')

s1.legend()
s1.grid()

plot_name = f'sample_coherence_histograms.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



###  Sample TF variance distribution, part (n - 1) B / A
# Start fig
fig, (s1) = plt.subplots(1)

# Define bins
num_bins = 400
low_bin  = 0
high_bin = 30
bins = np.linspace(low_bin, high_bin, num_bins)
xaxis = np.linspace(low_bin, high_bin, 1000)

for nn in sample_numbers:
    plot_ffs = data_dict[nn]['ffs']

    # Run sample b/a model
    b_over_a_pdf = sample_b_over_a_pdf(xaxis, nn)

    # Plot histogram
    NNba, bins, patches = s1.hist(plot_ffs, bins=bins, histtype='step', lw=2, density=True, label=r'$(n - 1) \hat{B} / \hat{A}$ samples')
    color = patches[0].get_edgecolor()

    s1.plot(xaxis, b_over_a_pdf, ls='--', color=color, alpha=0.7, label=rf'$F(2, n - 2)$, $n = $ {nn}')

    # s1.plot([coherence, coherence], [0, np.max(NNba)*1.1], ls='--', color='C3', label=f'True coherence ' + r'$\gamma^2$' + f' = {coherence:.3f}')
    s1.plot([np.mean(plot_ffs), np.mean(plot_ffs)], [0, 4], color=color, alpha=0.7, label=r'$(n - 1) \overline{ \hat{B} }/ \overline{ \hat{A} }$' + f' mean = {np.mean(plot_ffs):.3f}')

s1.set_xlim([0, 10])
s1.set_ylim([0, 4])

# Plot labels
s1.set_xlabel(r'Sample values')
s1.set_ylabel('Normalized Occurances')

s1.legend()
s1.grid()

plot_name = f'sample_b_over_a_histogram.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



### mean CSD Magnitude squared |<x,y>| histogram
# Start fig
fig, (s1) = plt.subplots(1)

# Define bins
num_bins = 200
low_bin  = 0
high_bin = 100
bins = np.linspace(low_bin, high_bin, num_bins)
xaxis = np.linspace(low_bin, high_bin, 1000)

for nn in sample_numbers:
    plot_csdxy = data_dict[nn]['csdxy']

    # Run sample b/a model
    sigma_xx = np.sqrt(p_aa + p_cc)
    sigma_yy = np.sqrt(p_bb + np.abs(HH)**2 * p_cc)
    r_pdf_scaler = sigma_xx * sigma_yy * (1 - coherence) / nn
    r_pdf = sample_r_pdf_caller(xaxis, nn, coherence, r_pdf_scaler)

    # Plot histogram
    NNcsd, bins, patches = s1.hist(np.abs(plot_csdxy), bins=bins, histtype='step', lw=2, density=True, label=r'$\hat{R} = |\overline{ \langle x, y \rangle }| $ samples')
    color = patches[0].get_edgecolor()

    s1.plot(xaxis, r_pdf, ls='--', color=color, alpha=0.7, label=r'$f( R | ' + r'\gamma^2 = ' + f'{coherence:.3f}' + r', n = ' + f'{nn}' + r')$')

    # s1.plot([coherence, coherence], [0, np.max(NNba)*1.1], ls='--', color='C3', label=f'True coherence ' + r'$\gamma^2$' + f' = {coherence:.3f}')
    s1.plot([np.mean(np.abs(plot_csdxy)), np.mean(np.abs(plot_csdxy))], [0, 0.11], color=color, alpha=0.7, label=r'$\overline{ \hat{R} }$' + f' mean = {np.mean( np.abs(plot_csdxy) ):.3f}')

s1.plot([true_csd_mean, true_csd_mean], [0, 0.11], color='k', alpha=0.7, label=r'True CSD magnitude ' + f'$R = ' + f'{true_csd_mean}' + f'$')
s1.set_ylim([0, 0.11])

# Plot labels
s1.set_xlabel(r'Sample cross spectral density magnitude $\hat{R}$')
s1.set_ylabel(r'CSD mangitude PDF $f_( \hat{R} | \gamma^2, n)$')

s1.legend()
s1.grid()

plot_name = f'sample_mean_csd_magnitude_histogram.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# ### Quad-spectrum histogram
# # Start fig
# fig, (s1) = plt.subplots(1)

# # Define bins
# num_bins = 200
# low_bin  = -125
# high_bin = 125
# bins = np.linspace(low_bin, high_bin, num_bins)
# xaxis = np.linspace(low_bin, high_bin, 1000)

# # Run sample b/a model
# laplace_pdf = asymmetric_laplace_pdf(xaxis, 0, lambda2, 1.0)

# # Plot histogram
# NNcsd, bins, patches = s1.hist(quad_spectra, bins=bins, histtype='step', lw=2, density=True, 
#     label=r'$\langle x, y \rangle$ minor axis $\hat{Q}$ samples')

# s1.plot(xaxis, laplace_pdf, ls='--', label=r'$\frac{1}{2 \lambda} e^{-\frac{|x|}{\lambda}}$, $n = $ ' + f'{sample_number2}')

# # s1.plot([coherence, coherence], [0, np.max(NNba)*1.1], ls='--', color='C3', label=f'True coherence ' + r'$\gamma^2$' + f' = {coherence:.3f}')
# s1.plot([np.mean(quad_spectra), np.mean(quad_spectra)], [0, np.max(NNcsd)*1.1], color='C3', label=r'$\hat{Q}$' + f' mean = {np.mean(quad_spectra):.3f}')

# s1.set_ylim([0, np.max(NNcsd)*1.1])

# # Plot labels
# s1.set_xlabel(r'Sample values')
# s1.set_ylabel('Normalized Occurances')

# s1.legend()
# s1.grid()

# plot_name = f'sample_csd_minor_axis_histogram.pdf'
# full_plot_name = '{}/{}'.format(fig_dir, plot_name)
# plot_names = np.append(plot_names, full_plot_name)
# print('Writing plot PDF to {}'.format(full_plot_name))
# plt.savefig(full_plot_name, bbox_inches='tight')
# plt.close()



# ### Quad-spectrum squared histogram
# # Start fig
# fig, (s1) = plt.subplots(1)

# # Define bins
# num_bins = 2500
# low_bin  = 0
# high_bin = 25000
# bins = np.linspace(low_bin, high_bin, num_bins)
# xaxis = np.linspace(low_bin, high_bin, 10000)

# # Run sample b/a model
# q_squared_pdf = laplace_squared_pdf(xaxis, lambda2)

# # Plot histogram
# NNcsd, bins, patches = s1.hist(quad_spectra**2, bins=bins, histtype='step', lw=2, density=True, 
#     label=r'$\langle x, y \rangle$ minor axis squared $\hat{Q}^2$ samples')

# s1.plot(xaxis, q_squared_pdf, ls='--', label=r'$\frac{1}{2 \lambda \sqrt{x}} e^{-\frac{\sqrt{x}}{\lambda}}$, $n = $ ' + f'{sample_number2}')

# # s1.plot([coherence, coherence], [0, np.max(NNba)*1.1], ls='--', color='C3', label=f'True coherence ' + r'$\gamma^2$' + f' = {coherence:.3f}')
# s1.plot([np.mean(quad_spectra**2), np.mean(quad_spectra**2)], [0, np.max(NNcsd)*1.1], color='C3', label=r'$\hat{Q}^2$' + f' mean = {np.mean(quad_spectra**2):.3f}')

# s1.set_xlim([0, 1000])
# s1.set_ylim([0, np.max(NNcsd)*1.1])

# # Plot labels
# s1.set_xlabel(r'Sample values')
# s1.set_ylabel('Normalized Occurances')

# s1.legend()
# s1.grid()

# plot_name = f'sample_csd_minor_axis_squared_histogram.pdf'
# full_plot_name = '{}/{}'.format(fig_dir, plot_name)
# plot_names = np.append(plot_names, full_plot_name)
# print('Writing plot PDF to {}'.format(full_plot_name))
# plt.savefig(full_plot_name, bbox_inches='tight')
# plt.close()



# ### co-spectrum histogram
# # Start fig
# fig, (s1) = plt.subplots(1)

# # Define bins
# num_bins = 200
# low_bin  = -50
# high_bin = 500
# bins = np.linspace(low_bin, high_bin, num_bins)
# xaxis = np.linspace(low_bin, high_bin, 1000)

# # Run sample b/a model
# al_pdf = asymmetric_laplace_pdf(xaxis, 0, lambda2, kappa2)

# # Plot histogram
# NNcsd, bins, patches = s1.hist(co_spectra, bins=bins, histtype='step', lw=2, density=True, 
#     label=r'$\langle x, y \rangle$ major axis $\hat{C}$ samples')

# s1.plot(xaxis, al_pdf, ls='--', label=r'$\frac{1}{\lambda (\kappa + 1/\kappa)} e^{-\frac{|x - m| \kappa^s}{\lambda}}$, $n = $ ' + f'{sample_number2}')

# # s1.plot([coherence, coherence], [0, np.max(NNba)*1.1], ls='--', color='C3', label=f'True coherence ' + r'$\gamma^2$' + f' = {coherence:.3f}')
# s1.plot([np.mean(co_spectra), np.mean(co_spectra)], [0, np.max(NNcsd)*1.1], color='C3', label=r'$\hat{C}$' + f' mean = {np.mean(co_spectra):.3f}')

# s1.set_ylim([0, np.max(NNcsd)*1.1])

# # Plot labels
# s1.set_xlabel(r'Sample values')
# s1.set_ylabel('Normalized Occurances')

# s1.legend()
# s1.grid()

# plot_name = f'sample_csd_major_axis_histogram.pdf'
# full_plot_name = '{}/{}'.format(fig_dir, plot_name)
# plot_names = np.append(plot_names, full_plot_name)
# print('Writing plot PDF to {}'.format(full_plot_name))
# plt.savefig(full_plot_name, bbox_inches='tight')
# plt.close()



# ### magnitude of co-spectrum histogram
# # Start fig
# fig, (s1) = plt.subplots(1)

# # Define bins
# num_bins = 200
# low_bin  = 0
# high_bin = 500
# bins = np.linspace(low_bin, high_bin, num_bins)
# xaxis = np.linspace(low_bin, high_bin, 1000)

# # Run sample b/a model
# al_mag_pdf = asymmetric_laplace_magnitude_pdf(xaxis, 0, lambda2, kappa2)

# # Plot histogram
# NNcsd, bins, patches = s1.hist(np.abs(co_spectra), bins=bins, histtype='step', lw=2, density=True, 
#     label=r'$\langle x, y \rangle$ major axis $\hat{C}$ samples')

# s1.plot(xaxis, al_mag_pdf, ls='--', label=r'$\frac{1}{\lambda (\kappa + 1/\kappa)} e^{-\frac{|x - m| \kappa^s}{\lambda}}$, $n = $ ' + f'{sample_number2}')

# # s1.plot([coherence, coherence], [0, np.max(NNba)*1.1], ls='--', color='C3', label=f'True coherence ' + r'$\gamma^2$' + f' = {coherence:.3f}')
# s1.plot([np.mean(np.abs(co_spectra)), np.mean(np.abs(co_spectra))], [0, np.max(NNcsd)*1.1], color='C3', label=r'$\hat{C}$' + f' mean = {np.mean(np.abs(co_spectra)):.3f}')

# s1.set_ylim([0, np.max(NNcsd)*1.1])

# # Plot labels
# s1.set_xlabel(r'Sample values')
# s1.set_ylabel('Normalized Occurances')

# s1.legend()
# s1.grid()

# plot_name = f'sample_csd_major_axis_magnitude_histogram.pdf'
# full_plot_name = '{}/{}'.format(fig_dir, plot_name)
# plot_names = np.append(plot_names, full_plot_name)
# print('Writing plot PDF to {}'.format(full_plot_name))
# plt.savefig(full_plot_name, bbox_inches='tight')
# plt.close()



# ### co-spectrum squared histogram
# # Start fig
# fig, (s1) = plt.subplots(1)

# # Define bins
# num_bins = 15000
# low_bin  = 0
# high_bin = 150000
# bins = np.linspace(low_bin, high_bin, num_bins)
# xaxis = np.linspace(low_bin, high_bin, 50000)

# # Run sample b/a model
# c_squared_pdf = asymmetric_laplace_squared_pdf(xaxis, lambda2, kappa2)

# # Plot histogram
# NNcsd, bins, patches = s1.hist(co_spectra**2, bins=bins, histtype='step', lw=2, density=True, 
#     label=r'$\langle x, y \rangle$ major axis squared $\hat{C}^2$ samples')

# s1.plot(xaxis, c_squared_pdf, ls='--', label=r'$\frac{1}{\lambda (\kappa + 1/\kappa) \sqrt{x}} ( e^{-\frac{\sqrt{x}}{\kappa \lambda}} + e^{-\frac{\sqrt{x} \kappa}{\lambda}})$, $n = $ ' + f'{sample_number2}')

# # s1.plot([coherence, coherence], [0, np.max(NNba)*1.1], ls='--', color='C3', label=f'True coherence ' + r'$\gamma^2$' + f' = {coherence:.3f}')
# s1.plot([np.mean(co_spectra**2), np.mean(co_spectra**2)], [0, np.max(NNcsd)*1.1], color='C3', label=r'$\hat{C}^2$' + f' mean = {np.mean(co_spectra**2):.3f}')

# s1.set_xlim([0, 1000])
# s1.set_ylim([0, np.max(NNcsd)*1.1])

# # Plot labels
# s1.set_xlabel(r'Sample values')
# s1.set_ylabel('Normalized Occurances')

# s1.legend()
# s1.grid()

# plot_name = f'sample_csd_major_axis_squared_histogram.pdf'
# full_plot_name = '{}/{}'.format(fig_dir, plot_name)
# plot_names = np.append(plot_names, full_plot_name)
# print('Writing plot PDF to {}'.format(full_plot_name))
# plt.savefig(full_plot_name, bbox_inches='tight')
# plt.close()



# ### csd magnitude histogram
# # Start fig
# fig, (s1) = plt.subplots(1)

# # Define bins
# num_bins = 400
# low_bin  = 0
# high_bin = 500
# bins = np.linspace(low_bin, high_bin, num_bins)
# xaxis = np.linspace(low_bin, high_bin, 50000)

# # Run sample r model
# r_pdf_scaler_n1 = sigma_xx * sigma_yy * (1 - coherence) / 1
# r_pdf_n1 = sample_r_pdf_caller(xaxis, 1, coherence, pdf_scaler=r_pdf_scaler_n1)

# # Plot histogram
# r_spectra = np.sqrt( co_spectra**2 + quad_spectra**2 )
# NNcsd, bins, patches = s1.hist(r_spectra, bins=bins, histtype='step', lw=2, density=True, 
#     label=r'$|\langle x, y \rangle| = \hat{R}$ samples')

# s1.plot(xaxis, r_pdf_n1, ls='--', label=r'$f(R|n = 1)$')

# # s1.plot([coherence, coherence], [0, np.max(NNba)*1.1], ls='--', color='C3', label=f'True coherence ' + r'$\gamma^2$' + f' = {coherence:.3f}')
# s1.plot([np.mean(r_spectra), np.mean(r_spectra)], [0, np.max(NNcsd)*1.1], color='C3', label=r'$\hat{R}$' + f' mean = {np.mean(r_spectra):.1f}')
# s1.plot([np.median(r_spectra), np.median(r_spectra)], [0, np.max(NNcsd)*1.1], ls='--', color='C3', label=r'$\hat{R}$' + f' median = {np.median(r_spectra):.1f}')

# s1.set_xlim([0, 200])
# s1.set_ylim([0, np.max(NNcsd)*1.1])

# # Plot labels
# s1.set_xlabel(r'Sample values $R$')
# s1.set_ylabel('Cross spectral density magnitude PDF $f(R)$')

# s1.legend()
# s1.grid()

# plot_name = f'sample_csd_magnitude_histogram.pdf'
# full_plot_name = '{}/{}'.format(fig_dir, plot_name)
# plot_names = np.append(plot_names, full_plot_name)
# print('Writing plot PDF to {}'.format(full_plot_name))
# plt.savefig(full_plot_name, bbox_inches='tight')
# plt.close()



# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()