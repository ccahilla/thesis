'''
sample_mean_and_median_asymmetric_laplace.py

Computes the sample mean and median PDFs 
for small numbers of samples.
Sample median argument follows FINDCHIRP
https://journals.aps.org/prd/pdf/10.1103/PhysRevD.85.122006
closely.

Craig Cahillane
July 18, 2020
'''

#####   Imports   #####
import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

from scipy.special import beta, factorial, gamma, poch, kv

import mpmath

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 18, # 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 14,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Functions   #####
def sign_fixed(x, loc):
    '''Fixes point where x == loc, if it exists'''
    sign = np.sign(x - loc)
    try:
        idx = np.argwhere(sign == 0)[0][0]
    except IndexError:
        idx = None
    if idx is not None:
        sign[idx] = 1
    return sign

def asymmetric_laplace_pdf(x, loc, scale, kappa):
    '''Defines an asymmetric laplace probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplace
    scale   = scale parameter for broadening the PDF width
    kappa   = asymmetric parameter 
    Output:
    pdf     = asymmetric laplace probability distribution function on input x
    '''
    sign = sign_fixed(x, loc) # either +1 or -1 
    pdf = (1 / (scale * (kappa + 1/kappa))) * np.exp(-sign * (x - loc) * kappa**sign / scale)
    return pdf

def asymmetric_laplace_cdf(x, loc, scale, kappa):
    '''Defines an asymmetric laplace cummulative distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplace
    scale   = scale parameter for broadening the PDF width
    kappa   = asymmetric parameter 
    Output:
    cdf     = asymmetric laplace cummulative distribution function on input x
    '''
    sign = sign_fixed(x, loc) # either +1 or -1 
    zero_one = (sign + 1) // 2 # either +1 or 0
    one_zero = -(sign - 1) //2 # either 0 or +1
    cdf = 1 * zero_one - (-kappa**2)**one_zero / (1 + kappa**2) * np.exp(-sign * (x - loc) * kappa**sign / scale)
    return cdf

def asymmetric_laplace_mean(loc, scale, kappa):
    '''Calculates the mean of the asymmetric laplace distribution
    defined by the provided parameters'''
    mean = loc + scale * (1 - kappa**2)/(kappa)
    return mean

def asymmetric_laplace_median(loc, scale, kappa):
    '''Calculates the median of the asymmetric laplace distribution
    defined by the provided parameters'''
    if kappa < 1:
        median = loc - scale/kappa * np.log((1 + kappa**2)/2)
    else:
        median = loc + scale * kappa * np.log((1 + kappa**2)/(2 * kappa**2))
    return median

def asymmetric_laplace_sample_median_pdf(x, loc, scale, kappa, nn):
    '''Defines an asymmetric laplace sample median probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplace
    scale   = scale parameter for broadening the PDF width
    kappa   = asymmetric parameter 
    nn      = number of samples. Odd numbers are best.
    Output:
    pdf     = asymmetric laplace sample median probability distribution function
    '''
    mm = (nn - 1) // 2
    pdf0 = asymmetric_laplace_pdf(x, loc, scale, kappa)
    cdf0 = asymmetric_laplace_cdf(x, loc, scale, kappa)

    bb = 1/beta(mm + 1, mm + 1)

    pdf = bb * cdf0**mm * (1 - cdf0)**mm * pdf0
    return pdf

def asymmetric_laplace_sample_median_mean(loc, scale, kappa, nn):
    '''Calculates the mean of the sample median.
    Inputs:
    loc     = center peak location of the asymmetric laplace
    scale   = scale parameter for broadening the PDF width
    kappa   = asymmetric parameter 
    nn      = number of samples. Odd numbers only.
    Output:
    mean    = asymmetric laplace mean of the sample median for nn samples
    '''
    prefactor = (scale/kappa) * factorial(nn) / (1 + kappa**2)**((nn + 1)//2)
    hyp1 = float( mpmath.mp.hyper(  [(1 - nn)//2, (1 + nn)//2, (1 + nn)//2], 
                                    [(3 + nn)//2, (3 + nn)//2], 
                                    1/(1 + kappa**2)) )
    hyp2 = float( mpmath.mp.hyper(  [(1 - nn)//2, (1 + nn)//2, (1 + nn)//2], 
                                    [(3 + nn)//2, (3 + nn)//2], 
                                    kappa**2/(1 + kappa**2)) )
    regularization = gamma((3 + nn)//2)**2  # divide hypergeometric func by all gamma(b_i)

    mean = prefactor * ( hyp1 - kappa**(3 + nn) * hyp2 ) / regularization
    return mean

def asymmetric_laplace_sample_median_variance(loc, scale, kappa, nn):
    '''Calculates the variance of the sample median estimate,
    from Laplace's sample median proof.
    '''
    sample_median_mean = asymmetric_laplace_sample_median_mean(loc, scale, kappa, nn)
    pdf_at_median = asymmetric_laplace_pdf(sample_median_mean, loc, scale, kappa)

    sample_median_variance = 1/(4 * nn * pdf_at_median**2) # Laplace's sample median
    return sample_median_variance

def asymmetric_laplace_sample_median_pdf_approx(x, loc, scale, kappa, nn):
    '''Normal approximation to the asymmetric Laplace sample median pdf
    '''
    mu = asymmetric_laplace_sample_median_mean(loc, scale, kappa, nn)
    sigma2 = asymmetric_laplace_sample_median_variance(loc, scale, kappa, nn)

    pdf = 1/np.sqrt(2 * np.pi * sigma2) * np.exp(-(x - mu)**2/(2 * sigma2))
    return pdf

def asymmetric_laplace_sample_mean_pdf(x, loc, scale, kappa, nn):
    '''Defines an asymmetric laplace sample mean probability distribution on x.
    Equivalent to csd_1d_major_axis_pdf().
    Inputs:
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplace
    scale   = scale parameter for broadening the PDF width
    kappa   = asymmetric parameter 
    nn      = number of samples.
    Output:
    pdf     = asymmetric laplace sample mean probability distribution function
    '''
    idx = np.argwhere(x > loc)[0][0]
    xl = np.zeros_like(x)
    xr = np.zeros_like(x)
    xl[:idx] = x[:idx]
    xr[idx:] = x[idx:]

    nmo = nn - 1

    suml = 0
    sumr = 0
    for ii in np.arange(nn):
        prefactor = poch(nn, ii) / ( factorial(ii) * factorial(nmo - ii) )

        postfactorr = (nn * xr * (1 + kappa**2))**(nmo - ii) * (kappa * scale)**ii
        postfactorl = (nn * xl * (1 + kappa**2))**(nmo - ii) * (-kappa * scale)**ii

        suml += prefactor * postfactorl
        sumr += prefactor * postfactorr
    
    pdf0 = nn * kappa**nn / ( scale**nn * (1 + kappa**2)**(2*nn-1) )
    pdfl = (-1)**nmo * np.exp(nn * xl /(kappa * scale)) * suml * pdf0
    pdfr = np.exp(-nn * kappa * xr / scale) * sumr * pdf0

    pdf = np.zeros_like(x)
    pdf[:idx] = pdfl[:idx]
    pdf[idx:] = pdfr[idx:]

    return pdf

def csd_1d_major_axis_pdf(cc, lambda0, coherence, nn):
    '''Two dimentional cross spectral density probability distribution function
    p(c,d) = exp(cc/sigma_cc**2) K0(sqrt(cc**2 + dd**2)/(gamma * sigma_cc**2))/(2*pi*sigma_aa**2*sigma_cc**2)

    Input:
    cc  = real axis of csd
    dd  = imaginary axis of csd
    lambda0     = amplitude of uncorrelated noise
    coherence   = ratio of csd^2/(psd1 * psd2)
    phi0 = angle of csd
    nn  = number of samples
    '''

    amp_coh = np.sqrt(coherence)
    exp_coeff = nn * amp_coh / (lambda0 * np.sqrt(1 - coherence))
    bessel_coeff = nn / (lambda0 * np.sqrt(1 - coherence))

    pdf1 = np.abs(cc)**(nn - 0.5) * np.exp( cc * exp_coeff ) 
    pdf2 = kv(nn - 0.5, np.abs(cc) * bessel_coeff) 
    pdf3 = nn**(nn + 0.5) * (1 - coherence)**(0.5*(nn - 0.5)) / ( gamma(nn) * 2**(nn - 0.5) * np.sqrt(np.pi) * lambda0**(nn + 0.5) )
    pdf = pdf1 * pdf2 * pdf3
    return pdf

def kappa_to_coherence(kappa):
    '''Computes the asymmetric Laplace kappa parameter from the coherence'''
    coherence = ( (1 - kappa**2) / (1 + kappa**2) )**2
    return coherence

def coherence_to_kappa(coherence):
    '''Computes the coherence from the asymmetric Laplace kappa parameter'''
    kappa = np.sqrt( (1 - np.sqrt(coherence)) / (1 + np.sqrt(coherence)) )
    return kappa

#####   Parameters   #####
m0 = 0
lambda0 = 0.806
# kappa0 = 0.95

coherence0 = 0.198
kappa0 = coherence_to_kappa(coherence0)

print()
print(f'lambda0 = {lambda0}')
print(f'kappa0 = {kappa0}')
print(f'coherence0 = {coherence0}')
print()

x0 = np.linspace(-2, 3, 1000)

params0 = np.array([m0, lambda0, kappa0])

# nns = np.arange(1, 40, 4)
nns = np.array([3, 11, 33, 101, ])

#####   Figures Directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Figures   #####
# check pdf and cdf
fig, (s1) = plt.subplots(1)

s1.plot(x0, asymmetric_laplace_pdf(x0, *params0), label=f'PDF $m$ = {m0}, $\lambda$ = {lambda0}, $\kappa$ = {kappa0}')
s1.plot(x0, asymmetric_laplace_cdf(x0, *params0), label=f'CDF')

mean = asymmetric_laplace_mean(*params0)
median = asymmetric_laplace_median(*params0)
s1.plot([mean, mean],       [0, 1], color='C0', ls='-', alpha=0.6, label=f'mean = {mean:.3f}')
s1.plot([median, median],   [0, 1], color='C0', ls='--', alpha=0.6, label=f'median = {median:.3f}')

# s1.set_title('Asymmetric Laplace distribution functions')
s1.set_ylabel('CDF')
s1.set_xlabel('Sample space')

s1.set_xlim([x0[0], x0[-1]])
s1.set_ylim([0, 1])

s1.grid()
s1.legend()

plot_name = f'asymmetric_laplace_pdf_and_cdf.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# sample mean
fig, (s1) = plt.subplots(1)

s1.plot(x0, csd_1d_major_axis_pdf(x0, lambda0, coherence0, 1), label=r'$f_{\hat{\mu}}(u|n = 1)$ ' + f'$\lambda$ = {lambda0}, $\gamma^2$ = {coherence0:.4f}')

mean = asymmetric_laplace_mean(*params0)
median = asymmetric_laplace_median(*params0)
max_pdf = 3.0

s1.plot([mean, mean],       [0, max_pdf], color='C0', ls='-', alpha=0.6, label=r'$\mu$ = ' + f'{mean:.3f}')
s1.plot([median, median],   [0, max_pdf], color='C0', ls='--', alpha=0.6, label=r'$\rho$ = ' + f'{median:.3f}')

for nn in nns:
    # temp_pdf = asymmetric_laplace_sample_mean_pdf(x0, *params0, nn)
    temp_pdf = csd_1d_major_axis_pdf(x0, lambda0, coherence0, nn)

    p1, = s1.plot(x0, temp_pdf, label=r'$f_{\hat{\mu}}' + f'(u|n = {nn})$')
    # p1, = s1.plot(x0, temp_pdf2, ls='--', label=r'$f_{\hat{\mu}}' + f'(x|n = {nn})$')

# s1.set_title('Asymmetric Laplace sample mean')
s1.set_ylabel('CSD sample mean PDF ' + r'$f_{\hat{\mu}}(u)$')
s1.set_xlabel('CSD major axis ' + r'$u$')

s1.set_xlim([-0.4, 1.5])
s1.set_ylim([0, max_pdf])

s1.grid()
s1.legend()

plot_name = f'asymmetric_laplace_sample_mean_pdfs.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# sample median
fig, (s1) = plt.subplots(1)

s1.plot(x0, asymmetric_laplace_pdf(x0, *params0), label=r'$f_{\hat{\rho}}(u|n = 1)$ ' + f'$\lambda$ = {lambda0}, $\gamma^2$ = {coherence0:.4f}')

mean = asymmetric_laplace_mean(*params0)
median = asymmetric_laplace_median(*params0)
max_pdf =  3.2

s1.plot([mean, mean],       [0, max_pdf], color='C0', ls='-', alpha=0.6, label=r'$\mu$ = ' + f'{mean:.3f}')
s1.plot([median, median],   [0, max_pdf], color='C0', ls='--', alpha=0.6, label=r'$\rho$ = ' + f'{median:.3f}')

temp_pdf = asymmetric_laplace_sample_median_pdf(x0, *params0, nns[-1])

for nn in nns:
    temp_pdf = asymmetric_laplace_sample_median_pdf(x0, *params0, nn)
    temp_mean = asymmetric_laplace_sample_median_mean(*params0, nn)
    temp_var = asymmetric_laplace_sample_median_variance(*params0, nn)
    approx_temp_pdf = asymmetric_laplace_sample_median_pdf_approx(x0, *params0, nn)

    p1, = s1.plot(x0, temp_pdf, label=r'$f_{\hat{\rho}}' + f'(u|n = {nn})$')
    s1.plot([temp_mean, temp_mean], [0, max_pdf], color=p1.get_color(), ls='--', alpha=0.6, label=r'$\langle \hat{\rho} \rangle$ = '+f'{temp_mean:.3f}')
    # s1.plot(x0, approx_temp_pdf, ls='--', color=p1.get_color(), alpha=0.6, label=r'$\mathcal{N}('+ f'{temp_mean:.3f},{temp_var:.3f}'+')$')

# s1.set_title('Asymmetric Laplace sample median')
s1.set_ylabel('CSD sample median PDF ' + r'$f_{\hat{\rho}}(u)$')
s1.set_xlabel('CSD major axis ' + r'$u$')

s1.set_xlim([-0.4, 1.5])
s1.set_ylim([0, max_pdf])

s1.grid()
s1.legend()

plot_name = f'asymmetric_laplace_sample_median_pdfs.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()

# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()