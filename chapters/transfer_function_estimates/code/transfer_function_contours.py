'''
transfer_function_histograms.py

Plot histograms of a transfer function estimate \hat{H}

Craig Cahillane
August 10, 2020
'''

import os
import sys
import time
import copy
import numpy as np
# import scipy.signal as sig
# import scipy.fft as fft

from scipy.stats import expon, rayleigh, norm, chi2, f
from scipy.special import kn, gamma, iv
from scipy.optimize import curve_fit

import matplotlib as mpl
import matplotlib.pyplot as plt

import mpmath

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 14, # 22,
                     'xtick.labelsize': 'small',
                     'ytick.labelsize': 'small',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})



#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])


#####   Functions   #####
def gain_phi_pdf(gg, phi, nn, true_coherence, phi0):
    '''Returns two-dimensional joint probability density p(g,phi).
    From Equation 4.81 of Goodman's thesis: https://apps.dtic.mil/sti/pdfs/AD0134919.pdf
    '''
    delta = 1 - true_coherence
    gam = np.sqrt(true_coherence)
    prefactor = gg * nn * delta**nn / np.pi
    pdf = prefactor / (1 - 2 * gam * np.outer(gg, np.cos(phi - phi0)) + gg**2)**(nn + 1)
    return pdf

def real_imag_pdf(xx, yy, nn, true_coherence, alpha, beta):
    '''Returns two-dimensional joint probability density p(xx, yy).
    From Equation 4.91 of Goodman's thesis: https://apps.dtic.mil/sti/pdfs/AD0134919.pdf
    '''
    delta = 1 - true_coherence
    gam = np.sqrt(true_coherence)
    prefactor = nn * delta**nn / np.pi
    pdf = prefactor / ( delta + (xx - alpha)**2 + np.outer( (yy - beta)**2, np.ones_like(xx) ) )**(nn + 1)
    return pdf

#####   Main   #####

HH = 2.0 * np.exp(1j * np.pi/6)
alpha = np.real(HH) / np.abs(HH)
beta  = np.imag(HH) / np.abs(HH)
gam = HH / np.abs(HH)

xx = np.linspace(0, 1.5, 1000)
yy = np.linspace(-0.5, 1.5, 1000)

# gg = np.sqrt(xx**2 + yy**2)
# phi = np.arctan2(yy, xx)

nn = 5
true_coherence = 0.1
phi0 = np.angle(HH)

preim = real_imag_pdf(xx, yy, nn, true_coherence, alpha, beta)

xx = np.outer(np.ones_like(xx), xx)
yy = np.outer(yy, np.ones_like(yy))



#####   Figures   #####
###  Sample coherences
# Start fig
fig = plt.figure()
s1 = fig.add_subplot(111, projection='3d')
# s1.axis('equal')

# Plot contours
cset = s1.plot_surface(xx, yy, preim, alpha=0.5)
cset = s1.contour(xx, yy, preim, zdir='z', offset=np.min(preim))
cset = s1.contour(xx, yy, preim, zdir='x', offset=np.min(xx))
cset = s1.contour(xx, yy, preim, zdir='y', offset=np.max(yy))

# s1.clabel(CS, inline=1, fontsize=10)

# Plot normalized TF
s1.plot([0, alpha], [0, beta], label='$H/|H|$')

# Plot labels
s1.set_xlabel(r'$\Re(H)/H$')
s1.set_ylabel(r'$\Im(H)/H$')
s1.set_zlabel(r'$p(\Re(H)/H, \Im(H)/H)$')

s1.legend()
s1.grid()

plot_name = f'transfer_function_contours.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name)#, bbox_inches='tight')
plt.close()