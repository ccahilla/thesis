'''
sample_coherence.py

Plot how the estimates of the mean-averaged coherence, \hat{gamma^2},
converge to the actual coherence gamma^2,
as the number of samples increases.


Craig Cahillane
November 1, 2020
'''

import os
import sys
import time
import copy
import numpy as np
# import scipy.signal as sig
# import scipy.fft as fft

from scipy.special import kn, gamma, iv, betainc

import matplotlib as mpl
import matplotlib.pyplot as plt

import mpmath

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 18, # 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})



#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])



#####   Functions   #####
def sample_ordinary_coherence_pdf(xx, nn, true_coherence):
    '''Returns the probability density function of the ordinary coherence on xx
    given the number of samples nn and the true_coherence.
    From Equation (11) of https://apps.dtic.mil/dtic/tr/fulltext/u2/619993.pdf
    '''
    prefactor = (nn - 1) * (1 - true_coherence)**nn * (1 - xx)**(nn - 2)
    hyp = np.array([])
    for x in xx:
        temp_hyp = float( mpmath.hyp2f1(nn, nn, 1, x * true_coherence ) )
        hyp = np.append(hyp, temp_hyp)
    pdf = prefactor * hyp
    return pdf

def sample_ordinary_coherence_expected_value(nn, true_coherence):
    '''Returns the expected value of the ordinary coherence on xx
    given the number of samples nn and the true_coherence.
    From Equation (11) of https://apps.dtic.mil/dtic/tr/fulltext/u2/619993.pdf
    '''
    if true_coherence > 100.0 / nn:
        return true_coherence
    prefactor = (1 - true_coherence)**nn / nn
    hyp = float( mpmath.hyper([2, nn, nn], [1, nn + 1], true_coherence ) ) 
    ev = prefactor * hyp
    return ev

def sample_ordinary_coherence_variance(nn, true_coherence):
    '''Returns the expected value of the ordinary coherence on xx
    given the number of samples nn and the true_coherence.
    From Equation (11) of https://apps.dtic.mil/dtic/tr/fulltext/u2/619993.pdf
    '''
    if true_coherence > 100.0 / nn:
        return 2 * coherence * (1 - coherence)**2 / nn
    prefactor = 2 * (1 - true_coherence)**nn 
    hyp1 = float( mpmath.hyper([3, nn, nn], [1, nn + 2], true_coherence ) ) / ( nn * (nn + 1) )
    hyp2 = -0.5 * (1 - true_coherence)**nn * float( mpmath.hyper([2, nn, nn], [1, nn + 1], true_coherence ) )**2 / nn**2
    var = prefactor * (hyp1 + hyp2)
    return var



#####   Parameters   #####
nns = np.arange(1, 1e5 + 1, dtype=int)
coherences = np.array([0.5, 0.1, 1e-2, 1e-3, 1e-4])



#####   Figures   #####
# expected value
fig, (s1) = plt.subplots(1)

s1.loglog(nns, 1/nns, color='k', ls='--', label=r'$1 / n$')

for coherence in coherences:
    print(f'coherence = {coherence}')
    evs = np.array([])
    for nn in nns:
        nn = int(nn)
        ev = sample_ordinary_coherence_expected_value(nn, coherence)
        evs = np.append(evs, ev)

    p1, = s1.loglog(nns, evs, label=r'$\langle \hat{\gamma}^2 \rangle$')
    s1.loglog(nns, coherence * np.ones_like(nns), ls='--', color=p1.get_color(), alpha=0.6, label=r'$\gamma^2$' + f' = {coherence:.0e}')

s1.set_xlim([1, 1e5])

s1.set_ylabel(r'Sample coherence expected value $\langle \hat{\gamma}^2 \rangle$')
s1.set_xlabel('Number of samples ' + r'$n$')

s1.legend()
s1.grid()

plot_name = f'sample_coherence_expected_values.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# variance
fig, (s1) = plt.subplots(1)

s1.loglog(nns, 1/nns**2, color='k', ls='--', label=r'$1 / n^2$')

for coherence in coherences:
    print(f'coherence = {coherence}')
    varss = np.array([])
    for nn in nns[1:]:
        nn = int(nn)
        var = sample_ordinary_coherence_variance(nn, coherence)
        varss = np.append(varss, var)

    p1, = s1.loglog(nns[1:], varss, label=r'$\sigma^2_{ \hat{\gamma}^2 } $, ' + r'$\gamma^2$' + f' = {coherence:.0e}')
    s1.loglog(nns[1:], 2 * coherence * (1 - coherence)**2 / nns[1:], ls='--', color=p1.get_color(), alpha=0.6, label=r'$2 \gamma^2 (1 - \gamma^2)^2 / n$')

s1.set_xlim([1, 1e5])
s1.set_ylim([1e-9, 1e-1])

s1.set_ylabel(r'Sample coherence variance $\sigma^2_{ \hat{\gamma}^2 } $')
s1.set_xlabel('Number of samples ' + r'$n$')

s1.legend()
s1.grid()

plot_name = f'sample_coherence_variance.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()