'''
bayes_factor.py

Computes the bayes factor of the asymmetric Laplace distribution
over the symmetric Laplace.
Null hypothesis is the distribution follows a symmetric Laplace.
Non-null hypothesis is the dist follows an asymmetric Laplace.
Likelihood ratio is LR = Null hypothesis/non-null hypothesis.

We assume that m = 0 for both distributions in this script,
and try to find the number of samples n that will resolve 
different uncorrelated/correlated power ratios.

Craig Cahillane
July 23, 2020
'''

import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

from scipy.linalg import eig
from scipy.special import gamma, poch, loggamma

import mpmath

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 18, # 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Functions   #####
def negative_samples(xxs):
    '''Given all samples xxs, 
    returns the xxs that are less than zero.
    '''
    idxs = np.argwhere(xxs < 0)[:,0]
    neg_xxs = xxs[idxs]
    return neg_xxs

def positive_samples(xxs):
    '''Given all samples xxs, 
    returns the xxs that are greater than zero.
    '''
    idxs = np.argwhere(xxs > 0)[:,0]
    pos_xxs = xxs[idxs]
    return pos_xxs

def negative_sum(xxs):
    '''Given all samples xxs,
    return the average of only the negative samples
    '''
    nn = len(xxs)
    neg_xxs = negative_samples(xxs)
    neg_sum = np.sum(np.abs(neg_xxs))/nn
    return neg_sum

def positive_sum(xxs):
    '''Given all samples xxs,
    return the average of only the positive samples
    '''
    nn = len(xxs)
    pos_xxs = positive_samples(xxs)
    pos_sum = np.sum(pos_xxs)/nn
    return pos_sum

def mle_kappa(xxs):
    '''Maximum likelihood estimator of kappa, kappa_hat,
    from only the samples from an asymmetric Laplace distribution.
    From Equation 17 of 
    https://www.researchgate.net/publication/259472079_A_Class_Of_Asymmetric_Distributions
    '''
    neg_sum = negative_sum(xxs)
    pos_sum = positive_sum(xxs)

    kappa_hat = (neg_sum / pos_sum)**(1/4)
    return kappa_hat

def mle_lambda(xxs):
    '''Maximum likelihood estimator of lambda, lambda_hat,
    from only the samples from an asymmetric Laplace distribution.
    From Equation 17 of 
    https://www.researchgate.net/publication/259472079_A_Class_Of_Asymmetric_Distributions
    in this paper this parameter is sigma_hat.
    '''
    neg_sum = negative_sum(xxs)
    pos_sum = positive_sum(xxs)

    lambda_hat = (neg_sum * pos_sum)**(1/4) * ( np.sqrt(neg_sum) + np.sqrt(pos_sum) )
    return lambda_hat

def bayes_factor(nn, mean_ratio):
    '''Calculates the bayes factor model comparison of 
    bf = asymmetric laplace/symmetric laplace
    Analytic solution from max_likelihood_estimator_asymmetric_laplace.nb

    Input:
    nn  = number of samples
    mean_ratio = negative sample mean / positive sample mean = beta / alpha 
    Output:
    bf = bayes factor of model comparison between asymmetric/symmetric Laplace
    '''

    prefactor = 2**(nn - 1) * (1 + mean_ratio)**(nn - 1) * gamma(nn - 1) / poch(nn, nn - 1)
    hyp =  float( mpmath.hyp2f1(  nn - 1, nn - 1, 
                                    2*nn - 1, 
                                    1 - mean_ratio))
    # regularization = gamma(2*nn - 1)  # divide hypergeometric func by all gamma(b_i)

    bf = prefactor * hyp
    return bf

def log_bayes_factor(nn, mean_ratio):
    '''Calculates log version of bayes_factor()
    logbf = asymmetric laplace/symmetric laplace
    Analytic solution from max_likelihood_estimator_asymmetric_laplace.nb

    Input:
    nn  = number of samples
    mean_ratio = negative sample mean / positive sample mean = beta / alpha 
    Output:
    logbf = bayes factor of model comparison between asymmetric/symmetric Laplace
    '''
    prefactor1 = (nn - 1) * np.log(2)
    prefactor2 = (nn - 1) * np.log(1 + mean_ratio)
    prefactor3 = loggamma(nn - 1)
    prefactor4 = loggamma(nn)
    prefactor = prefactor1 + prefactor2 + prefactor3 + prefactor4
    regularization = loggamma(2*nn - 1)

    hyp =  float( mpmath.hyp2f1(  nn - 1, nn - 1, 
                                    2*nn - 1, 
                                    1 - mean_ratio))
    loghyp = np.log(hyp)

    logbf = prefactor + loghyp - regularization
    return logbf

def log_bayes_factor_null_approx(nn):
    '''Calculates log version of bayes_factor() when there is no support for 
    the asymmetric laplace hypothesis, i.e. mean_ratio = 1 exactly.
    Analytic solution from max_likelihood_estimator_asymmetric_laplace.nb

    Input:
    nn  = number of samples
    Output:
    logbf = bayes factor of model comparison between asymmetric/symmetric Laplace
    '''
    factor1 = np.log(np.sqrt(np.pi))
    factor2 = -0.5 * np.log(nn)
    logbf = factor1 + factor2 
    return logbf

def required_samples(power_ratios):
    '''Required samples to achieve a decisive log Bayes factor = log(100).
    '''
    return 8.0 * power_ratios

def required_samples_coh(coherences):
    '''Required samples to achieve a decisve log Bayes factor = log(100),
    given the power coherence gamma^2'''
    return 8.0 * (1/coherences - 1)

#####   Parameters   #####
nns = np.arange(2, 10001)
power_ratios = np.array([3.0, 10.0, 30.0, 100.0, 300.0, 1000.0]) - np.ones(6)
decisive_bound = np.log(100)

bfs_dict = {}
coherence_dict = {}
kappa_dict = {}
ratio_dict = {}
decisive_nn_dict = {}

print()
print(f'power_ratio   coherence   kappa1   mean_ratio   nn')

for power_ratio in power_ratios:
    coherence = 1/(1 + power_ratio)

    p_cc = 1.0
    p_aa = -p_cc + p_cc * np.sqrt(1 + power_ratio)
    p_bb = p_aa

    # Asymmetric laplace params
    lambda1 = np.sqrt( p_aa * p_bb + p_aa * p_cc + p_bb * p_cc )
    kappa1 = np.sqrt( 1 + (2 * p_cc**2 - 2 * p_cc * np.sqrt( (p_aa + p_cc) * (p_bb + p_cc) )) \
                            / (p_aa * p_bb + p_aa * p_cc + p_bb * p_cc) )
    
    mean_ratio = kappa1**4 # beta/alpha

    bfs = 21.0 * np.ones_like(nns)
    for ii, nn in enumerate(nns):
        temp_bf = log_bayes_factor(nn, mean_ratio)
        bfs[ii] = temp_bf

        if temp_bf > 21.0:
            break

    try:
        idx = np.argwhere(bfs > decisive_bound)[0][0]
        decisive_nn_dict[power_ratio] = nns[idx]
    except:
        decisive_nn_dict[power_ratio] = None 

    bfs_dict[power_ratio] = bfs
    coherence_dict[power_ratio] = coherence
    kappa_dict[power_ratio] = kappa1
    ratio_dict[power_ratio] = mean_ratio


    print(f'{power_ratio:.0f}  &  {coherence:.3f} &  {kappa1:.3f}  &  {mean_ratio:.3f}  &  {decisive_nn_dict[power_ratio]}')


#####   Figures   #####
fig, (s1) = plt.subplots(1)

s1.plot([nns[0], nns[-1]], [np.log(100), np.log(100)], ls='-', alpha=0.6, label=f'Decisive $\log(100)$')

for power_ratio in power_ratios:
    temp_bfs = bfs_dict[power_ratio]
    temp_kappa = kappa_dict[power_ratio]
    temp_coherence = coherence_dict[power_ratio]
    temp_nn = decisive_nn_dict[power_ratio]

    p1, = s1.semilogx(nns, temp_bfs, label=f'$\gamma^2$ = {temp_coherence:.3f}')
    s1.semilogx([temp_nn, temp_nn], [0, np.log(100)], ls='--', color=p1.get_color(), label=f'$n$ = {temp_nn}')

s1.semilogx(nns, log_bayes_factor_null_approx(nns), ls='--', label='$\log(\sqrt{\pi} n^{-1/2})$')

# s1.set_title('Asymmetric Laplace sample mean')
s1.set_ylabel('Log Bayes Factor $\log(BF)$')
s1.set_xlabel('Number of samples $n$')

s1.set_xlim([nns[0], nns[-1]])
s1.set_ylim(top=20)

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s1.legend()

plot_name = f'log_bayes_factor_vs_number_of_samples.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# power ratio vs samples
fig, (s1) = plt.subplots(1)

dnd = np.array(list(decisive_nn_dict.values())) 
s1.loglog(power_ratios, dnd, 'o')
s1.loglog(power_ratios, required_samples(power_ratios), ls='--', label='Approx')

# s1.set_title('Asymmetric Laplace sample mean')
s1.set_ylabel('Number of samples $n$')
s1.set_xlabel('Power ratio $\epsilon$')

# s1.set_xlim([-0.4, 0.6])
# s1.set_ylim(top=20)

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
# s1.legend()

plot_name = f'number_of_samples_vs_power_ratio.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# coherence vs samples
fig, (s1) = plt.subplots(1)

dnd = np.array(list(decisive_nn_dict.values())) 
cohs = np.array(list(coherence_dict.values())) 
s1.loglog(cohs, dnd, 's-', color='C2', label='Minimum required samples')
s1.loglog(cohs, required_samples_coh(cohs), ls='--', color='C3', label=r'$n \approx 8 \left( \frac{1}{\overline{\gamma^2}} - 1 \right)$')
s1.loglog(cohs, 1/cohs, ls=':', color='C0', label=r'$n = \frac{1}{\overline{\gamma^2}}$') # From Tukey 
# s1.loglog(cohs, np.log(0.05)/(np.log(1 - cohs)), ls='-.', color='C1', label=r'$n = 1 - 0.05^{ 1/\overline{\gamma^2}}$') # From https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20110014532.pdf

# s1.set_title('Asymmetric Laplace sample mean')
s1.set_ylabel('Number of samples $n$')
s1.set_xlabel('Mean-averaged sample coherence $\hat{\overline{\gamma^2}}$')

# s1.set_xlim([-0.4, 0.6])
# s1.set_ylim(top=20)

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s1.legend()

plot_name = f'number_of_samples_vs_coherences.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()