'''
mle_asymmetric_laplace.py

Computes the maximum likelihood estimates of the 
asymmetric laplace parameters for low numbers of samples.
We assume that the peak location parameter m = 0 in this script.

Craig Cahillane
July 20, 2020
'''

import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

from scipy.linalg import eig

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 18, # 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])


#####   Functions   #####
def asymmetric_laplace_pdf(x, loc, scale, kappa):
    '''Defines an asymmetric laplace probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplace
    scale   = scale parameter for broadening the PDF width
    kappa   = asymmetric parameter 
    Output:
    pdf     = asymmetric laplace probability distribution function on input x
    '''
    sign = sign_fixed(x, loc) # either +1 or -1 
    pdf = (1 / (scale * (kappa + 1/kappa))) * np.exp(-sign * (x - loc) * kappa**sign / scale)
    return pdf

def negative_samples(xxs):
    '''Given all samples xxs, 
    returns the normalized xxs that are less than zero.
    '''
    idxs = np.argwhere(xxs < 0)[:,0]
    neg_xxs = xxs[idxs]
    return neg_xxs

def positive_samples(xxs):
    '''Given all samples xxs, 
    returns the normalized xxs that are less than zero.
    '''
    idxs = np.argwhere(xxs > 0)[:,0]
    pos_xxs = xxs[idxs]
    return pos_xxs

def mle_lambda(xxs):
    '''Maximum likelihood estimator of lambda, lambda_hat,
    from only the samples from an asymmetric Laplace distribution.
    From Equation 17 of 
    https://www.researchgate.net/publication/259472079_A_Class_Of_Asymmetric_Distributions
    in this paper this parameter is sigma_hat.
    '''
    nn = len(xxs)
    neg_xxs = negative_samples(xxs)
    pos_xxs = positive_samples(xxs)

    neg_sum = np.sum(np.abs(neg_xxs))/nn
    pos_sum = np.sum(pos_xxs)/nn

    lambda_hat = (neg_sum * pos_sum)**(1/4) * ( np.sqrt(neg_sum) + np.sqrt(pos_sum) )
    return lambda_hat

def mle_kappa(xxs):
    '''Maximum likelihood estimator of kappa, kappa_hat,
    from only the samples from an asymmetric Laplace distribution.
    From Equation 17 of 
    https://www.researchgate.net/publication/259472079_A_Class_Of_Asymmetric_Distributions
    '''
    nn = len(xxs)
    neg_xxs = negative_samples(xxs)
    pos_xxs = positive_samples(xxs)

    neg_sum = np.sum(np.abs(neg_xxs))/nn
    pos_sum = np.sum(pos_xxs)/nn

    kappa_hat = (neg_sum / pos_sum)**(1/4)
    return kappa_hat

def mle_covariance_matrix(xxs):
    '''Covariance matrix of maximum likelihood estimates of 
    theta_hat = [lambda_hat, kappa_hat]
    '''
    nn = len(xxs)
    scale = mle_lambda(xxs)
    kappa = mle_kappa(xxs)

    ev11 = ((1 + 6 * kappa**2 + kappa**4) * scale**2)/(8 * kappa**2)
    ev12 = (scale - kappa**4 * scale)/(8 * kappa)
    ev22 = 1/8 * (1 + kappa**2)**2
    covmat = 1/np.sqrt(nn) * \
                np.array([
                    [ev11, ev12], 
                    [ev12, ev22]
                ])
    return covmat

#####   Parameters   #####
# m0 = 0
# lambda0 = 0.5
# kappa0 = 0.5
# x0 = np.linspace(-7, 7, 1001)

# params0 = np.array([m0, lambda0, kappa0])


#####   Make random data   #####
sample_numbers = np.array([10, 100, 1000, 10000, 100000, 1000000])
xxs_dict = {}
true_params_dict = {}
mle_params_dict = {}
cov_ellipse_x_dict = {}
cov_ellipse_y_dict = {}
for sample_number in sample_numbers:

    sigma_aa = 4
    sigma_bb = 5
    sigma_cc = 0.6

    Ar = np.random.normal(scale=sigma_aa, size=sample_number)
    Br = np.random.normal(scale=sigma_bb, size=sample_number)
    Cr = np.random.normal(scale=sigma_cc, size=sample_number)

    Ai = np.random.normal(scale=sigma_aa, size=sample_number)
    Bi = np.random.normal(scale=sigma_bb, size=sample_number)
    Ci = np.random.normal(scale=sigma_cc, size=sample_number)

    Mr = Ar*Br + Ar*Cr + Cr*Br + Cr**2
    Mi = Ai*Bi + Ai*Ci + Ci*Bi + Ci**2

    xxs = Mr + Mi

    # Asymmetric laplace params
    p_aa = sigma_aa**2
    p_bb = sigma_bb**2
    p_cc = sigma_cc**2

    lambda1 = np.sqrt( p_aa * p_bb + p_aa * p_cc + p_bb * p_cc )
    kappa1 = np.sqrt( 1 + (2 * p_cc**2 - 2 * p_cc * np.sqrt( (p_aa + p_cc) * (p_bb + p_cc) )) \
                            / (p_aa * p_bb + p_aa * p_cc + p_bb * p_cc) )

    power_ratio = lambda1**2/p_cc**2

    params1 = np.array([0, lambda1, kappa1])

    # Parameter estimates
    lambda_hat = mle_lambda(xxs)
    kappa_hat = mle_kappa(xxs)
    theta_hat = np.array([lambda_hat, kappa_hat])

    covmat = mle_covariance_matrix(xxs)
    print(f'n           = {sample_number}')
    print(f'theta_hat   = {theta_hat}')
    print(f'covmat      = {covmat}')

    ss, VV = eig(covmat)
    ss = np.real(ss)

    # Covariance ellipse
    tt = np.linspace(0, 2*np.pi, 200) # parametric variable
    ell = np.array([np.sqrt(ss[0]) * np.cos(tt), 
                    np.sqrt(ss[1]) * np.sin(tt)]) # circle scaled into ellipse by eigenvalues

    ell_rot = np.zeros_like(ell)
    for i in np.arange(200):
        ell_rot[:,i] = np.dot(VV, ell[:,i])

    # Cov ellipse about mean
    ell_rot_x_center = lambda_hat + ell_rot[0,:]
    ell_rot_y_center = kappa_hat + ell_rot[1,:]

    xxs_dict[sample_number] = xxs
    true_params_dict[sample_number] = params1
    mle_params_dict[sample_number] = theta_hat
    cov_ellipse_x_dict[sample_number] = ell_rot_x_center
    cov_ellipse_y_dict[sample_number] = ell_rot_y_center



#####   Figures   #####
# covariance ellipse of asymmetric laplace estimates lambda and kappa
fig, (s1) = plt.subplots(1)

s1.plot(lambda1, kappa1, '*', ms=15,
    label='True parameters\n' + r'$\lambda = $' + f'{lambda1:.3f} ' + r'$\kappa = $' + f'{kappa1:.3f}')

s1.plot(lambda_hat, kappa_hat, 'o', ms=15,
    label='Estimated parameters\n' + r'$\hat{\lambda} = $' + f'{lambda_hat:.3f} ' + r'$\hat{\kappa} = $' + f'{kappa_hat:.3f}')

s1.plot(ell_rot_x_center, ell_rot_y_center, 'C1-', label='1$\sigma$ covariance ellipse')

s1.set_title('Asymmetric Laplace parameter estimates from ' + r'$n =$ ' + f'{sample_number}')
s1.set_xlabel('$\lambda$')
s1.set_ylabel('$\kappa$')

s1.legend(loc='upper left')
s1.grid()

plot_name = f'asymmetric_laplace_param_est_with_cov_sample_number_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()


# covariance ellipses with different numbers of samples
fig, (s1) = plt.subplots(1)

s1.plot(lambda1, kappa1, '*', ms=20,
    label='True parameters\n' + r'$\lambda=$' + f'{lambda1:.3f} ' + r'$\kappa=$' + f'{kappa1:.3f}')

markers = np.array(['s', 'D', 'P', '8', 'p', 'v'])
for sample_number, marker in zip(sample_numbers, markers):
    theta_hat = mle_params_dict[sample_number] 
    ell_rot_x_center = cov_ellipse_x_dict[sample_number] 
    ell_rot_y_center = cov_ellipse_y_dict[sample_number]

    lambda_hat, kappa_hat = theta_hat

    p1, = s1.plot(lambda_hat, kappa_hat, marker, ms=10,
        label=r'$\hat{\lambda}=$' + f'{lambda_hat:.3f} ' + r'$\hat{\kappa}=$' + f'{kappa_hat:.3f} for ' r'$n =$ ' + f'{sample_number}')

    s1.plot(ell_rot_x_center, ell_rot_y_center, color=p1.get_color(), label='1$\sigma$ covariance ellipse')

s1.set_title('Asymmetric Laplace parameter estimates')
s1.set_xlabel('$\lambda$')
s1.set_ylabel('$\kappa$')

s1.legend()
s1.grid()

plot_name = f'asymmetric_laplace_param_est_with_covarainces_all_sample_numbers.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()