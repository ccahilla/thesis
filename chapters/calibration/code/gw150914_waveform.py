'''
gw150914_waveform.py

Simple GW150914 waveform plot.

Craig Cahillane
Jan 5, 2021
'''
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc

from pycbc import waveform

mpl.rcParams.update({'figure.figsize':(12,6),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})


#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Functions   #####

#
# PhenomD waveforms:
# https://journals.aps.org/prd/pdf/10.1103/PhysRevD.93.044007
# Appendix B
#

# Definitions
L = 3994.5 # m
c = scc.c
M_sun = 1.989e30 # kg
Mpc = 3.086e22 # m

# GW150914 params
m1_0 = 36
m2_0 = 29
M_chirp_0 = 30.7 * M_sun # kg
nu_0 = 0.247
t_c = 2.50e-3 # s, coalescence time
phi_c = 0.600 # rad, coalescence phase
dist_eff_0 = 756 * Mpc # m, depends on the actual system distance and the orientation of the source 

# Get a time domain waveform
hp, hc = waveform.get_td_waveform(
            approximant="IMRPhenomD", 
            mass1=m1_0, 
            mass2=m2_0, 
            delta_t=1.0/4096, 
            f_lower=20,
            inclination=0,
            distance=690
            )
tt_GW150914 = hp.sample_times.data
td_data_GW150914 = hp.data



#####   Figures   #####

fig, (s1) = plt.subplots(1)

s1.plot(tt_GW150914, td_data_GW150914, label='GW150914')

s1.set_xlim([-0.2, 0.03])

s1.set_xlabel('Time [s]')
s1.set_ylabel('Strain')

s1.grid()
s1.grid(which='minor', ls='--')
s1.legend()

plot_name = 'time_domain_GW150914_waveform.pdf'
full_plot_name = f'{fig_dir}/{plot_name}'.format(plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()