'''
darm_carm_als_noise_comparisons.py

Compare DARM, CARM, and ALS COMM sensitivities

Craig Cahillane
Jan 8, 2021
'''
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc
import scipy.special as scp

import nds2utils as nu

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})


#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Functions   #####
def load_txt(filename):
    '''Reads and loads the data in filename.  
    Expects two columns of data, 
    1) Frequency vector in Hz
    2) Spectral density in units/rtHz
    Input:
    filename = full path to file to read
    Output: tuple(fff, psd)
    fff = frequency vector in Hz
    psd = spectral density
    '''
    data = np.loadtxt(filename)
    fff = data[:,0]
    psd = data[:,1]
    return fff, psd

def rms(xx, yy): 
    '''Calculate the cumulative root mean squared value of yy from high to low xx
    Returns an array of the cumlative root mean of yy from xx[-1] to xx[ii]
    '''
    diff = np.diff(np.squeeze(xx)) 
    dx = np.concatenate(([diff[0]], diff), axis=0) 
 
    # Return rms intergrated from high to low 
    rms = np.flipud(np.sqrt(np.cumsum(np.flipud(np.squeeze(yy)**2 * dx)))) 
    return rms 

def arm_pole(   L,
                T1,
                T2):
    '''Computes the cavity pole of a two mirror cavity in Hz.

    Inputs:
    L = length of the cavity in meters
    T1 = power transmission of the input mirror
    T2 = power transmission of the end mirror

    Outputs:
    pole = cavity pole in Hz
    '''
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    FSR = scc.c/(2 * L)
    pole = FSR * np.log(1 / (r1 * r2)) / (2 * np.pi)
    return pole

def arm_zero(   L,
                T1,
                T2):
    '''Computes the cavity zero of a two mirror cavity in Hz.
    This refers to the scale factor on the zero at 0 Hz describing the cavity reflectivity:
    -r(0) + r^*(f) = if/f_zero / (1 + if/f_pole)
    c.f. Hall thesis Eq. C.24 
    and my phd_response_algebra.nb in my thesis repo.

    Inputs:
    L = length of the cavity in meters
    T1 = power transmission of the input mirror
    T2 = power transmission of the end mirror

    Outputs:
    pole = cavity pole in Hz
    '''
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    f_zero = (-scc.c * (1 - r1 * r2)**2)/(4 * L * np.pi * (-1 + r1**2) * r2)
    return f_zero

def arm_gain(phi, T1, T2, Loss_rt):
    '''
    Returns the amplitude gain of an arm cavity.
    phi     = round trip phase
    T1      = power transmittance of input mirror
    T2      = power transmittance of output mirror
    Loss_rt = roundtrip power loss
    '''
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    invloss = np.sqrt(1 - Loss_rt)
    return t1/(1 - r1 * r2 * invloss * np.exp(-1j * phi))

def arm_trans(phi, T1, T2, Loss_rt):
    '''
    Returns the amplitude gain of an arm cavity.
    phi     = round trip phase
    T1      = power transmittance of input mirror
    T2      = power transmittance of output mirror
    Loss_rt = roundtrip power loss
    '''
    t1 = np.sqrt(T1)
    t2 = np.sqrt(T2)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    invloss = np.sqrt(1 - Loss_rt)
    return t1 * t2 * np.exp(-1j * phi/2) / (1 - r1 * r2 * invloss * np.exp(-1j * phi))

def arm_refl(   phi, 
                T1, 
                T2, 
                Loss_rt):
    '''
    Returns the reflectivity of an arm cavity.
    phi     = round trip phase
    T1      = power transmittance of input mirror
    T2      = power transmittance of output mirror
    Loss_rt = roundtrip power loss
    '''
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    invloss = 1 - Loss_rt
    ra = (-r1 + invloss * r2 * np.exp(-1j * phi)) / (1 - r1 * r2 * np.exp(-1j * phi))
    return ra

def arm_refl_approx(phi, 
                    L,
                    T1, 
                    T2, 
                    Loss_rt):
    '''
    Returns the reflectivity of an arm cavity approximation from pdh_response_algebra.nb from my thesis repo.
    phi     = round trip phase
    T1      = power transmittance of input mirror
    T2      = power transmittance of output mirror
    Loss_rt = roundtrip power loss
    '''
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)

    ff = phi * scc.c/(4 * np.pi * L)
    f_zero = arm_zero(L, T1, T2)
    f_pole = arm_pole(L, T1, T2)

    r0 = arm_refl(0, T1, T2, Loss_rt)
    ra_approx = r0 - (( 1j * ff / f_zero) / (1 + 1j * ff / f_pole))
    return ra_approx

def arm_refl_derivative(phi, T1, T2, Loss_rt):
    '''
    Returns the derivative of the reflectivity of an arm cavity.
    phi     = relative phase of the light to the cavity
    T1      = power transmittance of input mirror
    T2      = power transmittance of output mirror
    Loss_rt = roundtrip power loss
    '''
    r2 = np.sqrt(1 - T2)
    invloss = np.sqrt(1 - Loss_rt)
    return r2 * invloss * np.exp(-2j * phi) * np.abs(arm_gain(phi, T1, T2, Loss_rt))**2


def finesse(T1, T2):
    '''Full finesse definition.
    T1      = power transmittance of input mirror
    T2      = power transmittance of output mirror
    '''
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    FF = (np.pi / 2) / ( np.arcsin( (1 - r1 * r2)/( 2 * np.sqrt(r1 * r2) ) ) )
    return FF

### Regehr functions
def rcav(phi, phi_m1, phi_m2, r1, r2):
    '''
    Reflectivity of a two mirror cavity.
    Inputs:
    phi = single trip phase of the cavity in radians.
          phi=0 is resonance, assuming phi_m1 = phi_m2 = 0.
          phi = pi * f / FSR, where f is audio freq
    phi_m1 = tuning of the input mirror in radians. phi_m1 = 0 is nominal.
    phi_m2 = tuning of the output mirror in radians. phi_m2 = 0 is nominal.
    r1 = input mirror amplitude reflectivity
    r2 = output mirror amplitude reflectivity
    Output:
    rcav = complex amplitude reflectivity of the coupled cavity
    '''
    t1 = np.sqrt(1 - r1**2)
    t2 = np.sqrt(1 - r2**2)
    numer = -r1*np.exp(2j*phi_m1) + r2*(r1**2 + t1**2) * np.exp(-1j * phi + 2j*phi_m2)
    denom = 1 - r1*r2*np.exp(-1j * (phi + phi_m1 - phi_m2))
    return numer/denom

def pdh_full(phi, phiMod, phi_m1, phi_m2, r1, r2):
    '''
    Pound-Drever-Hall error signal from a two mirror cavity.
    Full expression taken from Martin Regehr's Thesis, Equation 3.21, 
    https://thesis.library.caltech.edu/4180/1/Regehr_mw_1995.pdf
    Does not include the i*J0(modDepth)*J1(modDepth)*Pin prefactor.
    Inputs:
    phi = single trip phase of the cavity.  phi=0 is resonance.
    r1 = input mirror amplitude reflectivity
    r2 = output mirror amplitude reflectivity
    modFreq = modulation frequency in Hz
    Outputs:
    error = Complex PDH error signal, unitless.  Output should be scaled by i*J0(modDepth)*J1(modDepth)*Pin to get power.
    Usual error signal contained in the imaginary part of this output.
    '''
    demodFactor = 0.5
    # omega + Omega terms
    t1 =  np.conj(rcav(-phiMod - phi, phi_m1, phi_m2, r1, r2)) * rcav(0, phi_m1, phi_m2, r1, r2)            # steady carrier, conj(lower audio on lsb)
    t3 =  np.conj(rcav(-phiMod, phi_m1, phi_m2, r1, r2))       * rcav(phi, phi_m1, phi_m2, r1, r2)          # steady conj(lsb), upper audio carrier
    t5 =  np.conj(rcav(-phi, phi_m1, phi_m2, r1, r2))          * rcav(phiMod, phi_m1, phi_m2, r1, r2)       # steady usb, conj(lower audio carrier)
    t7 =  np.conj(rcav(0, phi_m1, phi_m2, r1, r2))             * rcav(phiMod + phi, phi_m1, phi_m2, r1, r2) # steady conj(carrier), upper audio usb

    # omega - Omega terms
    t2 =  rcav(-phiMod, phi_m1, phi_m2, r1, r2)       * np.conj(rcav(-phi, phi_m1, phi_m2, r1, r2))
    t4 =  rcav(-phiMod + phi, phi_m1, phi_m2, r1, r2) * np.conj(rcav(0, phi_m1, phi_m2, r1, r2))
    t6 =  rcav(0, phi_m1, phi_m2, r1, r2)             * np.conj(rcav(phiMod - phi, phi_m1, phi_m2, r1, r2))
    t8 =  rcav(phi, phi_m1, phi_m2, r1, r2)           * np.conj(rcav(phiMod, phi_m1, phi_m2, r1, r2))

    error = demodFactor * ( t1 + t2 + t3 + t4 + t5 + t6 + t7 + t8 )
    return error

def PCAL(fff, lambda0=1064e-9, L=3994.5):
    '''Estimated PCAL max strength from Sudarshan PCAL paper.
    '''
    nu0 = scc.c/1064e-9 # Hz
    PCAL_max_at_100_Hz = 1e-16 # m/rtHz
    PCAL_Hz = nu0/L * PCAL_max_at_100_Hz * (fff/100.0)**(-2) # Hz/rtHz
    return PCAL_Hz

def ESD(fff, lambda0=1064e-9, L=3994.5):
    '''Estimated ESD max strength.
    '''
    nu0 = scc.c/1064e-9 # Hz
    ESD_max_at_100_Hz = 1e-15 # m/rtHz
    ESD_Hz = nu0/L * ESD_max_at_100_Hz * (fff/100.0)**(-2) # Hz/rtHz
    return ESD_Hz

#####   Params   #####
fflin = np.linspace(-25000, 25000, 5000)
fflog = np.logspace(np.log10(1), np.log10(50000), 3000)

lambda0 = 1064e-9 # m
nu0 = scc.c/lambda0 # Hz
L = 3994.5 # m
FSR = scc.c / (2 * L)

darm_cal_m_to_Hz = nu0 / L # Hz/m

Ti_g = 0.96e-2  # ITM green transmission
Te_g = 7.9e-2   # ETM green transmission


#####   Green shot noise   #####
nu_g = 2 * nu0 
mod_depth = 0.15 # rads
round_trip_loss = 0 # 100e-6 # 100 ppm

F0 = finesse(Te_g, Ti_g)

P_in = 10e-3   # W

dr0dphi = arm_refl_derivative(0, Te_g, Ti_g, round_trip_loss)
g0 = arm_gain(0, Te_g, Ti_g, round_trip_loss)
P0_cav = np.abs(g0)**2

f_pole = arm_pole(L, Te_g, Ti_g) # Hz
f_zero = arm_zero(L, Te_g, Ti_g) # Hz

# Carrier on resonance
r0_refl = arm_refl(0, Te_g, Ti_g, round_trip_loss)
E0_refl = np.sqrt(P_in) * r0_refl
P0_refl = np.abs(E0_refl)**2
P0_trans = P_in * np.abs( arm_trans(0, Te_g, Ti_g, round_trip_loss) )**2

# Carrier off resonances
P0_refl_anti = (P_in * np.abs(arm_refl(np.pi, Te_g, Ti_g, round_trip_loss))**2)
P0_trans_anti = (P_in * np.abs(arm_trans(np.pi, Te_g, Ti_g, round_trip_loss))**2)

# Sidebands
rsb_refl = arm_refl(np.pi, Te_g, Ti_g, round_trip_loss)
Esb_refl = 1j * np.sqrt(P_in) * (0.5 * mod_depth) * rsb_refl # good for both plus and minus sidebands
Psb_refl = np.abs(Esb_refl)**2

# Total reflected power
Ptot_refl = P0_refl + 2 * Psb_refl

# Visibility definitions
visibility = 1 - P0_refl / P0_refl_anti # Hall Thesis Eq. C.13
visibility2 = (Ptot_refl - P0_refl)/(Ptot_refl + P0_refl) # Gautam soCal notebook

# # Derived PDH from E fields
# phis = 4 * np.pi * L * fflog / scc.c

# E0_refl_freq_resp = np.sqrt(P_in) * arm_refl(phis, Te_g, Ti_g, round_trip_loss)
# Esb_refl_freq_resp = 1j * np.sqrt(P_in) * (0.5 * mod_depth) * arm_refl(np.pi + phis, Te_g, Ti_g, round_trip_loss)
# pdh_response2 =  2 * (-E0_refl * np.conj(Esb_refl_freq_resp) + Esb_refl * np.conj(E0_refl_freq_resp)) / f_pole  # W/Hz

# optical_gain2 = -2 * P_in * mod_depth * rsb_refl * r0_refl / f_pole # W/Hz

# # From pdh_algebra_notebook
# E0_refl_freq_resp_approx = np.sqrt(P_in) * arm_refl_approx(phis, L, Te_g, Ti_g, round_trip_loss)

# Hall Eqs. C.23 and C.25
optical_gain = -P_in * mod_depth * 2 * np.pi * rsb_refl * dr0dphi / FSR # W/Hz
pdh_response = optical_gain / (1 + 1j * fflog/f_pole)

shot_noise_0 = np.sqrt(2 * scc.h * nu_g * P0_refl) # W/rtHz
shot_noise_sb = np.sqrt(3 * scc.h * nu_g * Psb_refl) # W/rtHz
shot_noise = shot_noise_0 + shot_noise_sb # W/rtHz

frequency_referred_shot_noise = np.abs(shot_noise / pdh_response) # Hz/rtHz



#### Beat PD shot noise ####
Pbeat = 30.0e-3 # W
power_to_rads = np.pi/Pbeat # rad/W

beat_shot_noise_power = np.sqrt(2 * scc.Planck * scc.c/1064e-9 * Pbeat) # W/rtHz
beat_shot_noise_rads = beat_shot_noise_power * power_to_rads # rad/rtHz
beat_shot_noise = beat_shot_noise_rads * fflog # Hz/rtHz


print()
print(f'finesse F0  = {F0}')
print(f'f_pole      = {f_pole} Hz')
print(f'f_zero      = {f_zero} Hz')
print(f'P_in        = {P_in*1e3} mW')
print()
print(f'g0^2        = {np.abs(g0)**2} W/W')
print(f'dr0dphi     = {dr0dphi} W/W')
print()
print(f'P0_cav      = {P0_cav} W')
print()
print(f'P0_refl     = {P0_refl*1e3} mW')
print(f'P0_trans    = {P0_trans*1e3} mW')
print()
print(f'Psb_refl    = {Psb_refl*1e3} mW')
print()
print(f'P0_refl_anti  = {P0_refl_anti*1e3} mW')
print(f'P0_trans_anti = {P0_trans_anti*1e3} mW')
print()
print(f'visibility  = {visibility}')
print(f'visibility2 = {visibility2}')
print()
print(f'optical_gain = {optical_gain} W/Hz')
# print(f'optical_gain2 = {optical_gain2} W/Hz')
# print(f'pdh_response2[0] = {np.abs(pdh_response2[0])} W/Hz')
print()
print(f'shot_noise = {shot_noise} W/rtHz')
print(f'DC frequency_referred_shot_noise = {frequency_referred_shot_noise[0]} Hz/rtHz')



#####   Read in txts   #####

txt_names = np.array([
    'LHO_O3_DARM_reference.txt',
    'refl_b_noise_projection_incident_frequency_noise.txt',
    'calibrated_als_comm_frequency_noise_asd.txt',
    # 'GDS_CALIB_STRAIN.txt', # terrible glitchy
])
labels = np.array([
    'DARM',
    'CARM',
    'ALS COMM',
    # 'GDS',
])
lss = np.array([
    '-',
    '-',
    '-',
    # '-',
])


data_dict = {}
for txt_name, label, ls in zip(txt_names, labels, lss):
    full_txt_name = f'{data_dir}/{txt_name}'
    temp_fff, temp_asd = load_txt(full_txt_name)

    if 'DARM' in txt_name:
        temp_asd = temp_asd * darm_cal_m_to_Hz

    if 'STRAIN' in txt_name:
        temp_asd = temp_asd * nu0

    temp_rms = rms(temp_fff, temp_asd)

    plot_ff, plot_asd = nu.linear_log_ASD(fflog, temp_fff, temp_asd)

    data_dict[txt_name] = {}
    data_dict[txt_name]['ff'] = temp_fff
    data_dict[txt_name]['asd'] = temp_asd
    data_dict[txt_name]['rms'] = temp_rms
    data_dict[txt_name]['label'] = label
    data_dict[txt_name]['ls'] = ls

    data_dict[txt_name]['fflog'] = plot_ff
    data_dict[txt_name]['asdlog'] = plot_asd




#####   Figures   #####
# Main noisebudget
fig = plt.figure()
s1 = fig.add_subplot(111)

for txt_name, txt_dict in data_dict.items():
    ff = txt_dict['fflog']
    asd = txt_dict['asdlog']
    label = txt_dict['label']
    ls = txt_dict['ls']

    if 'als_comm' in txt_name:
        idx = np.argwhere(ff > 900)[0,0]
        ff = ff[:idx]
        asd = asd[:idx]

    s1.loglog(ff, asd, ls=ls, label=label)

s1.loglog(fflog, frequency_referred_shot_noise, label='O3 green PDH\nshot noise limit')
s1.loglog(fflog, beat_shot_noise, label='Beat sensor shot noise\n'+r'$P_{beat} = 30~\mathrm{mW}$')

s1.loglog(fflog, PCAL(fflog), ls='--', alpha=0.6, label='Photon calibrator')
s1.loglog(fflog, ESD(fflog), ls='--', alpha=0.6, label='Electrostatic drive')

s1.set_xlim([10, 7000])
s1.set_ylim([1e-9, 1.01e0])

s1.set_yticks(nu.good_ticks(s1))

# s1.set_yticks(nu.good_ticks(s1))
s1.set_xlabel('Frequency [Hz]')
s1.set_ylabel(r'Frequency ASD [Hz/$\sqrt{\mathrm{Hz}}$]')
# s1.set_title('LHO Frequency Noisebudget - April 27, 2019')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=16)

plot_name = f'darm_carm_als_comm_noise.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Check pdh_responses 
# fig, (s1, s2) = plt.subplots(2, sharex=True)

# s1.loglog(fflog, np.abs(pdh_response), label='pdh tf from Hall C.23 and C.25')
# s2.semilogx(fflog, np.angle(pdh_response, deg=True), label='pdh tf from Hall C.23 and C.25')

# s1.loglog(fflog, np.abs(pdh_response2), label='pdh tf')
# s2.semilogx(fflog, np.angle(pdh_response2, deg=True), label='pdh tf')

# s1.set_ylabel('PDH error signal [W/Hz]')
# s2.set_ylabel('Phase [deg]')
# s2.set_xlabel('Frequency [Hz]')

# s1.grid()
# s1.grid(which='minor', ls='--', alpha=0.6)
# s2.grid()
# s2.grid(which='minor', ls='--', alpha=0.6)

# s1.legend(fontsize=16)

# plot_name = f'green_pdh_tfs.pdf'
# full_plot_name = '{}/{}'.format(fig_dir, plot_name)
# plot_names = np.append(plot_names, full_plot_name)
# print(f'Writing plot PDF to {full_plot_name}')
# plt.savefig(full_plot_name, bbox_inches='tight')
# plt.close()



# # Check pdh_responses 
# fig, (s1, s2) = plt.subplots(2, sharex=True)

# s1.loglog(fflog, np.abs(E0_refl_freq_resp), label='arm refl exact')
# s2.semilogx(fflog, np.angle(E0_refl_freq_resp, deg=True), label='arm refl exact')

# s1.loglog(fflog, np.abs(E0_refl_freq_resp_approx), label='arm refl approx')
# s2.semilogx(fflog, np.angle(E0_refl_freq_resp_approx, deg=True), label='arm refl approx')

# s1.set_ylabel('Arm reflection [rtW/rtW]')
# s2.set_ylabel('Phase [deg]')
# s2.set_xlabel('Frequency [Hz]')

# s1.grid()
# s1.grid(which='minor', ls='--', alpha=0.6)
# s2.grid()
# s2.grid(which='minor', ls='--', alpha=0.6)

# s1.legend(fontsize=16)

# plot_name = f'green_arm_refl_tfs.pdf'
# full_plot_name = '{}/{}'.format(fig_dir, plot_name)
# plot_names = np.append(plot_names, full_plot_name)
# print(f'Writing plot PDF to {full_plot_name}')
# plt.savefig(full_plot_name, bbox_inches='tight')
# plt.close()



# # Check phd_sweep 
# fig, (s1, s2) = plt.subplots(2, sharex=True)

# s1.plot(fflin, np.real(pdh_sweep), label='pdh sweep')
# s2.plot(fflin, np.imag(pdh_sweep), label='pdh sweep')

# s1.plot(fflin, np.real(pdh_sweep2), ls='--', label='pdh sweep Hall C.21')
# s2.plot(fflin, np.imag(pdh_sweep2), ls='--', label='pdh sweep Hall C.21')

# s1.plot(fflin, np.imag(pdh_sin_b24), ls='--', label='pdh sweep Driggers B.24')
# s2.plot(fflin, np.real(pdh_sin_b24), ls='--', label='pdh sweep Driggers B.24')

# s1.plot(fflin, np.imag(regehr_pdh), ls=':', label='pdh sweep Regehr 3.34')
# s2.plot(fflin, np.real(regehr_pdh), ls=':', label='pdh sweep Regehr 3.34')

# s1.set_ylabel('PDH Sweep real [W/Hz]')
# s2.set_ylabel('PDH Sweep imag [W/Hz]')
# s2.set_xlabel('Frequency [Hz]')

# s1.grid()
# s1.grid(which='minor', ls='--', alpha=0.6)
# s2.grid()
# s2.grid(which='minor', ls='--', alpha=0.6)

# s1.legend(fontsize=16)

# plot_name = f'pdh_sweep.pdf'
# full_plot_name = '{}/{}'.format(fig_dir, plot_name)
# plot_names = np.append(plot_names, full_plot_name)
# print(f'Writing plot PDF to {full_plot_name}')
# plt.savefig(full_plot_name, bbox_inches='tight')
# plt.close()


# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()