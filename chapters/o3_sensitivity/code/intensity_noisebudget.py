'''
Make a plot of the intensity noisebudget for LHO O3.

Craig Cahillane
December 30, 2020
'''
import os
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt

from nds2utils import linear_log_ASD

# plt.style.use('tableau-colorblind10')

mpl.rcParams.update({'text.usetex': True,
                     'figure.figsize': (12, 9),
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.9,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9,
                     'agg.path.chunksize': 10000})


#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])


#####   Functions   #####
def save_txt(fff, psd, filename, header, verbose=True):
    '''Stores a two-column txt of frequency and power spectral density
    fff = frequency vector in Hertz.  Must be np.array with single row of values.
    psd = power spectral density [units^2/Hz].  Must be np.array of same length as fff. 
    filename = full directory path to the file and filename
    header = text to go at the top of the txt
    '''
    save_data = np.vstack((fff, psd)).T
    if verbose:
        print()
        print(f'Saving data file...')
    np.savetxt(filename, save_data, header=header)
    if verbose:
        print(f'{filename}')
    return

def load_txt(filename):
    '''Reads and loads the data in filename.  
    Expects two columns of data, 
    1) Frequency vector in Hz
    2) Spectral density in units/rtHz
    Input:
    filename = full path to file to read
    Output: tuple(fff, psd)
    fff = frequency vector in Hz
    psd = spectral density
    '''
    data = np.loadtxt(filename)
    fff = data[:,0]
    psd = data[:,1]
    return fff, psd

def rms(x, y):
    '''Takes in freq vector x, and asd y,
    returns the cumulative root mean squared array
    '''
    diff = np.diff(np.squeeze(x))
    dx = np.concatenate(([diff[0]], diff), axis=0)

    # Return rms intergrated from high to low
    rms = np.flipud(np.sqrt(np.cumsum(np.flipud(np.squeeze(y)**2 * dx))))
    return rms


#####   Read in txts   #####
txt_names = np.array([
    'intensityNB_Out_of_loop_intensity_noise.txt',
    'intensityNB_In_loop_intensity_noise.txt',
    'intensityNB_Post_input_mode_cleaner_beam_jitter.txt',
    'intensityNB_Shot_noise.txt',
    'intensity_nb_Input_mode_cleaner_angular_controls.txt'
])
labels = np.array([
    'out of loop intensity noise',
    'in loop intensity noise',
    'post input mode cleaner beam jitter',
    'shot noise',
    'input mode cleaner angular controls',
])

data_dict = {}
for txt_name, label in zip(txt_names, labels):
    full_txt_name = f'{data_dir}/{txt_name}'
    temp_fff, temp_asd = load_txt(full_txt_name)

    data_dict[txt_name] = {}
    data_dict[txt_name]['ff'] = temp_fff
    data_dict[txt_name]['asd'] = temp_asd
    data_dict[txt_name]['label'] = label

#####   Frequency Vector   #####
fflog = np.logspace(np.log10(1e-2), np.log10(8192.0), 1000)

#####   Figures   #####
# Main noisebudget

fig = plt.figure()
s1 = fig.add_subplot(111)

for txt_name, txt_dict in data_dict.items():
    ff = txt_dict['ff']
    asd = txt_dict['asd']
    label = txt_dict['label']
    linestyle = '-' # Plot everything normally except shot noise

    rms0 = rms(ff, asd)
    print()
    print(f'{label} rms = {rms0[0]} RIN')

    if not label == 'shot noise':
        ffflog, asdlog = linear_log_ASD(fflog, ff, asd)
    else:
        ffflog = ff
        asdlog = asd
        linestyle = '--'
    
    if label == 'post input mode cleaner beam jitter':
        end_index = 590
        ffflog = ffflog[:end_index]
        asdlog = asdlog[:end_index]

    if label == 'out of loop intensity noise':
        p1, = s1.loglog(ffflog, asdlog, label=label, linestyle=linestyle, zorder=3)
        s1.loglog(ff, rms0, label=f'{label} RMS', color=p1.get_color(), linestyle='--', zorder=3)
    else:
        s1.loglog(ffflog, asdlog, label=label, linestyle=linestyle)

s1.set_ylim([1e-10, 2e-5])
# s1.set_yticks(goodTicks(s1))
s1.set_xlim([0.1, 7000])

s1.set_xlabel('Frequency [Hz]')
s1.set_ylabel(r'Relative Intensity ASD [$ 1 / \sqrt{\mathrm{Hz}} $]')

s1.legend(loc='upper right', ncol=1)
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.3)

plot_name = f'intensity_noisebudget.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()

# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()