'''
plot_range.py

Plots the O3 binary neutron star range.

Copied from Aaron Buikema plot_range.py in the O3 commissioning paper repo.
https://git.ligo.org/o3commissioning/o3-commissioning-paper

Craig Cahillane
December 20, 2020
'''

import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

import gpstime
from brokenaxes import brokenaxes

mpl.rcParams.update({   'text.usetex': True,
                        'figure.figsize': (12, 9),
                        'font.family': 'serif',
                        # 'font.serif': 'Georgia',
                        # 'mathtext.fontset': 'cm',
                        'lines.linewidth': 2.5,
                        'font.size': 22,
                        'xtick.labelsize': 'large',
                        'ytick.labelsize': 'large',
                        'legend.fancybox': True,
                        'legend.fontsize': 12,
                        'legend.framealpha': 0.7,
                        'legend.handletextpad': 0.5,
                        'legend.labelspacing': 0.2,
                        'legend.loc': 'best',
                        'savefig.dpi': 80,
                        'pdf.compression': 9})



#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])



###   Read data   ###
range_file_a = f'{data_dir}/range_timeseries_cleaned_o3a_raw.npy'
range_file_b = f'{data_dir}/range_timeseries_cleaned_o3b_raw.npy'

# Data are stored as a list of dictionaries (with IFO channels as keys) of times and range data
temp_a = np.load(range_file_a,allow_pickle=True)
times_a = temp_a[0] #GPS time
range_data_a = temp_a[1]# Mpc
temp_b = np.load(range_file_b,allow_pickle=True)
times_b = temp_b[0] #GPS time
range_data_b = temp_b[1]# Mpc

downsample_points = 30000

lho_range_chan = 'H1:DMT-SNSH_EFFECTIVE_RANGE_MPC.mean'
llo_range_chan = 'L1:DMT-SNSH_EFFECTIVE_RANGE_MPC.mean'
ifos = [lho_range_chan,llo_range_chan]

# Concatenate both parts of O3
times = {}
range_data = {}
for ifo in ifos:
    times[ifo] = np.concatenate([times_a[ifo],times_b[ifo]])
    range_data[ifo] = np.concatenate([range_data_a[ifo],range_data_b[ifo]])

# FIX ME:
# -Get exact O3 start time
# -Also want zoom of certain data?
# Format times for plotting
start_time = '1 April 2019' # FIX ME!!!!!-->want this to be a nice string
gps_start = 1238166018 #to_gps(start_time).gpsSeconds

# Now let's get the plots ready

labels = {
    ifos[0]: 'LHO',
    ifos[1]: 'LLO'
}
colors = {
    ifos[0]: '#ee0000',   #ifo_colors['H1'],
    ifos[1]: '#4ba6ff',   #ifo_colors['L1']
}

plt.figure(0)
plt.clf()

# Conversion from seconds to weeks
convert_secs = 60.*60*24*7
# Figure out limits for plots
a_end = 0
b_start = 0
b_end = 0
for ifo in ifos:
    a_end = max([a_end,(times_a[ifo][-1]-gps_start)/convert_secs])
    b_end = max([b_end,(times_b[ifo][-1]-gps_start)/convert_secs])
    b_start = min([b_end,(times_b[ifo][1]-gps_start)/convert_secs])
wspace = 0.02
bax = brokenaxes(xlims=((0,a_end),(b_start,b_end)),wspace=wspace)

end_time = 0
for ifo in ifos:
    to_plot=(times[ifo]-gps_start)/convert_secs
    bax.plot(to_plot,range_data[ifo],'o',color=colors[ifo],label=labels[ifo],
    markersize=0.3,rasterized=True,fillstyle='full')
    end_time = max(end_time,max(to_plot))


bax.set_xlabel('Weeks since '+start_time)
bax.set_ylabel('Range [Mpc]')
plt.grid(axis='x')
#plt.grid(which='minor', ls='--', alpha=0.7)
bax.legend(markerscale = 10,numpoints = 1, loc=4)

#plt.xlim(0,end_time)
bax.set_ylim(85, 145)
bax.minorticks_on()


plot_name = f'range_vs_time.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()


# Downsample data
#fig, ax = plt.subplots()
plt.clf()
ratio = max(to_plot.size // downsample_points, 1)
bax = brokenaxes(xlims=((0,a_end),(b_start,b_end)),wspace=wspace)

convert_secs = 60.*60*24*7
end_time = 0
ratio = max(to_plot.size // downsample_points, 1)
for ifo in ifos:
    to_plot=(times[ifo]-gps_start)/convert_secs
    bax.plot(to_plot[::ratio],range_data[ifo][::ratio],'o',color=colors[ifo],label=labels[ifo],
    markersize=1,rasterized=True,fillstyle='full')
    end_time = max(end_time,max(to_plot))


bax.set_xlabel('Weeks since '+start_time)
bax.set_ylabel('Range [Mpc]',labelpad=45) # For some reason the labelpad is changing position of BOTH axes labels
plt.grid(axis='x')
bax.minorticks_on()
plt.grid(True, which='minor', ls='--', alpha=0.7)
bax.legend(markerscale = 5,numpoints = 1, loc=4)

#bax.xlim(0,end_time)
bax.set_ylim(85, 145)


plot_name = f'range_vs_time_downsampled.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Now let's zoom in on a single region

# Date range to make for zoomed-in plot
# FIX ME: choose reasonable range for this
# I'm sure there's a smart way to just zoom other plot and convert units appropriately, but this works fine
zoom_start = '18 July 2019 00:00 UTC'
plot_length = 60*60*24.*2 # seconds. Two days worth.
gps_zoom_start = int( gpstime.parse(zoom_start).gps() )

plt.figure(3)

# Conversion from seconds to hours
convert_secs = 60.*60
for ifo in ifos:
    plt.plot((times[ifo] - gps_start) / convert_secs, range_data[ifo], 'o', color=colors[ifo], label=labels[ifo])

plt.xlabel('Hours since '+zoom_start)
plt.ylabel('Range [Mpc]')
plt.grid()
plt.grid(which='minor', ls='--', alpha=0.7)
plt.legend(numpoints=1)

plt.xlim(0, plot_length/convert_secs)
plt.ylim(85, 145)


plot_name = f'range_vs_time_zoom.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()


# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()