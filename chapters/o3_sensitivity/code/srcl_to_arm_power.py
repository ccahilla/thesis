'''
Script to analyze a SRCL dither for arm power measurement.
Compare DARM/Arm RIN, whose slope should tell us the arm power directly.

Craig Cahillane
Nov 14, 2019
'''

import os
import numpy as np
import dtt2hdf
import nds2utils as nu
import scipy.optimize as sco
import scipy.constants as scc

import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams.update({   'figure.figsize': (12, 12),
                        'text.usetex': True,
                        'font.family': 'serif',
                        # 'font.serif': 'Georgia',
                        # 'mathtext.fontset': 'cm',
                        'lines.linewidth': 2.5,
                        'font.size': 22,
                        'xtick.labelsize': 'large',
                        'ytick.labelsize': 'large',
                        'legend.fancybox': True,
                        'legend.fontsize': 18,
                        'legend.framealpha': 0.9,
                        'legend.handletextpad': 0.5,
                        'legend.labelspacing': 0.2,
                        'legend.loc': 'best',
                        'legend.columnspacing': 2,
                        'savefig.dpi': 80,
                        'pdf.compression': 9})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])


### Read in the swept sine from an xml 
# xml ='../data/20191113_SRCLInj_vs_ArmPowerTF_calibrated_template.xml'
xml = f'{data_dir}/20191121_SRCLInj_vs_ArmPowerTF_calibrated_template.xml'

armdict = dtt2hdf.read_diaggui(xml) # read
print(f'TFs in the xml {xml}')
print(armdict['results']['TF'].keys())

darm = 'H1:CAL-DELTAL_EXTERNAL_DQ'
src = 'H1:LSC-SRCL_OUT_DQ'
xa = 'H1:ASC-X_TR_A_NSUM_OUT_DQ'
xb = 'H1:ASC-X_TR_B_NSUM_OUT_DQ'
ya = 'H1:ASC-Y_TR_A_NSUM_OUT_DQ'
yb = 'H1:ASC-Y_TR_B_NSUM_OUT_DQ'
channels = [darm, src, xa, xb, ya, yb]

idx_xa = armdict['results']['COH'][src]['channelB_inv'][xa]
idx_xb = armdict['results']['COH'][src]['channelB_inv'][xb]
idx_ya = armdict['results']['COH'][src]['channelB_inv'][ya]
idx_yb = armdict['results']['COH'][src]['channelB_inv'][yb]
idx_darm = armdict['results']['COH'][src]['channelB_inv'][darm]

ff = armdict['results']['TF'][src]['FHz']

tf_dict = {}
for chan in channels:
    if chan == src:
        continue
    tf_dict[chan] = {}
    tf_dict[chan]['ff'] = armdict['results']['TF'][chan]['FHz']

    tf_dict[chan][src] = {}

    idx = armdict['results']['COH'][src]['channelB_inv'][chan]
    tf_dict[chan][src]['ff'] = armdict['results']['TF'][chan]['FHz']
    tf_dict[chan][src]['TF'] = np.conj(armdict['results']['TF'][src]['xfer'][idx])
    tf_dict[chan][src]['coh'] = armdict['results']['COH'][src]['coherence'][idx]

for chan in channels:
    if chan == darm or chan == src:
        continue
    tf_dict[darm][chan] = {}

    idx = armdict['results']['COH'][chan]['channelB_inv'][darm]
    tf_dict[darm][chan]['ff'] = armdict['results']['TF'][chan]['FHz']
    tf_dict[darm][chan]['TF'] = np.conj(armdict['results']['TF'][chan]['xfer'][idx])
    tf_dict[darm][chan]['coh'] = armdict['results']['COH'][chan]['coherence'][idx]

# Get the time series of the measurement #
gps_start = int( armdict['results']['TF'][darm]['gps_second'] )
duration = 1927
gps_stop = gps_start + duration

# dataDict = nu.acquire_data(channels, gps_start, gps_stop)


# Calibrate the dictionary #
zeros = 30 * np.ones(6)
poles = 0.3 * np.ones(6)
units = 'm'
tf_dict[darm]['calUnits'] = units

units = 'cts'
tf_dict[src] = {}
tf_dict[src]['calUnits'] = units

rinify_chans = [xa, xb, ya, yb]
rinify_labels = np.array(['TRX A', 'TRX B', 'TRY A', 'TRY B'])
rinify_medians = np.array([182214, 238584, 259335, 274178])
rinify_gains = np.array([])
for chan, median in zip(rinify_chans, rinify_medians):
    # data = dataDict[chan]['data']
    # mean = np.mean(data)
    # median = np.median(data)
    gain = 1.0/median

    print(f'gain = {gain}')
    units = 'RIN'
    tf_dict[chan]['calUnits'] = units
    tf_dict[chan][src]['calTF'] = tf_dict[chan][src]['TF'] * gain

    rinify_gains = np.append(rinify_gains, gain)

    ff = tf_dict[darm][chan]['ff']
    darm_cal = nu.tf_zpk(ff, zeros, poles)
    tf_dict[darm][chan]['calTF'] = tf_dict[darm][chan]['TF'] * darm_cal / gain

ff = tf_dict[darm][src]['ff']
darm_cal = nu.tf_zpk(ff, zeros, poles)
tf_dict[darm][src]['calTF'] = tf_dict[darm][src]['TF'] * darm_cal / 1.0

# Fit 1/f^2 line to DARM/Arm RIN #
def fit_scaler(fff, alpha):
    return alpha

def f_squared_fit(fff, alpha):
    return alpha/fff**2

p0 = [1e-6]
for chan in rinify_chans:

    ff = tf_dict[darm][chan]['ff']
    calTF = tf_dict[darm][chan]['calTF']

    idxs = nu.bandlimit_applier(ff, 10, 66)
    ff = ff[idxs]
    calTF = calTF[idxs]

    # flatten the TF so the fit is not biased to the lowest frequency points
    flat_calTF = ff**2 * np.abs(calTF)
    popt, pcov = sco.curve_fit(fit_scaler, ff, flat_calTF, p0=p0)

    tf_dict[darm][chan]['alpha'] = popt[0]
    tf_dict[darm][chan]['alpha_var'] = pcov[0][0]

    # now fit RIN/SRCL CTRL
    ff = tf_dict[chan][src]['ff']
    calTF = tf_dict[chan][src]['calTF']

    idxs = nu.bandlimit_applier(ff, 10, 66)
    ff = ff[idxs]
    calTF = calTF[idxs]

    # flatten the TF so the fit is not biased to the lowest frequency points
    flat_calTF = ff**2 * np.abs(calTF)
    popt, pcov = sco.curve_fit(fit_scaler, ff, flat_calTF, p0=p0)

    tf_dict[chan][src]['alpha'] = popt[0]
    tf_dict[chan][src]['alpha_var'] = pcov[0][0]

### Fit 1/f^4 line to DARM/SRCL ctrl signal 
def f_to_the_fourth_fit(fff, alpha):
    return alpha/fff**4

ff = tf_dict[darm][src]['ff']
coh = tf_dict[darm][src]['coh']
calTF = tf_dict[darm][src]['calTF']

idxs = nu.bandlimit_applier(ff, 10, 66)
ff = ff[idxs]
coh = coh[idxs]
calTF = calTF[idxs]

idxs = nu.coherence_threshold_applier(coh, 0.998)
ff = ff[idxs]
coh = coh[idxs]
calTF = calTF[idxs]

p0 = [1e-12]
flat_calTF = ff**4 * np.abs(calTF)
popt, pcov = sco.curve_fit(fit_scaler, ff, flat_calTF, p0=p0)

tf_dict[darm][src]['alpha'] = popt[0]
tf_dict[darm][src]['alpha_var'] = pcov[0][0]

print('frequency vector fit to:')
print(f'{ff}')

# Calculate the Arm Powers from each QPD #
m = 40 # kg
c = scc.c

print('\033[93m')
for chan in rinify_chans:
    alpha = tf_dict[darm][chan]['alpha']
    alpha_var = tf_dict[chan][src]['alpha_var']
    alpha_std = np.sqrt(alpha_var)

    arm_power = alpha * np.pi**2 * m * c / 2
    arm_power_std = alpha_std * np.pi**2 * m * c / 2

    print(f'DARM/{chan} Arm Power Estimate = {arm_power/1e3:.3f} +- {arm_power_std/1e3:.3f} [kW]')
print('\033[0m')

### Estimate the gamma factor from the DARM/SRCL ctrl signal 
m_SRM = 2.901 # kg
DAC = 20/2**18 # V/cts
driver = 2.83e-3  # A/V
coils = 0.00281 # N/A
beta = DAC * driver * coils * 1.18 # N/cts, 18% increase from CAL CS SRCL

# alpha2 = 8 * beta * gamma * Parm / (m * m_SRM * c * (2 * pi)^4  * (1 + s_rse))
# where alpha2 is the alpha/f^4 fit value
alpha2 = tf_dict[darm][src]['alpha']
Parm = 191568.5 # watts, Parm = (Px + Py)/2
gamma = alpha2 * m * m_SRM * c * (2 * np.pi)**4 / (8 * beta * Parm)
print('\033[93m')
print(f'alpha2 = {alpha2}')
print(f'gamma  = {gamma}')


# gamma = 8 gs^2 rs ra' epsilon k / (ts^2 (1 + s_rse))
# epsilon = k * DARM_offset_in_meters
T_SRM = 0.3234 #
ts = np.sqrt(T_SRM)
rs = np.sqrt(1 - T_SRM)
f_DARM = 410 # Hz, ignore s_rse for now

k = 2*np.pi/1064e-9 # rad/m
DARM_offset = 9.7e-12 # m
epsilon = k * DARM_offset # rads

gs2_times_raprime = gamma * T_SRM / (8 * rs * epsilon * k)

print(f'gs2_times_raprime = {gs2_times_raprime}')


### Estimate the gamma factor from the Arm RIN/SRCL CTRL signal
alpha3_xa = tf_dict[xa][src]['alpha']
gamma3_xa = alpha3_xa * m_SRM * (2*np.pi)**2 / beta

alpha3_ya = tf_dict[ya][src]['alpha']
gamma3_ya = alpha3_ya * m_SRM * (2*np.pi)**2 / beta

raprime_ratio_y_over_x = gamma3_ya / gamma3_xa

print('')
print(f'gamma3_xa = {gamma3_xa}')
print(f'gamma3_ya = {gamma3_ya}')
print(f'raprime_ratio_y_over_x = {raprime_ratio_y_over_x}')
print('\033[0m')


###   Plots   ###

# DARM/RIN_arms
fig, (s1, s2, s3) = plt.subplots(3, sharex=True)

markers = np.array(['o', 's', '^', 'v'])
for chan, label, marker in zip(rinify_chans, rinify_labels, markers):
    ff  = tf_dict[darm][chan]['ff'] 
    tf  = tf_dict[darm][chan]['calTF'] 
    coh = tf_dict[darm][chan]['coh']

    tf_angle = np.angle(tf * np.exp(-1j * np.pi/2)) + np.pi/2

    s1.loglog(ff, np.abs(tf), marker=marker, label=label)
    s2.semilogx(ff, 180/np.pi * tf_angle, marker=marker, label=label)
    s3.semilogx(ff, coh, marker=marker, label=label)

for chan in rinify_chans:
    alpha = tf_dict[darm][chan]['alpha']
    arm_power = alpha * np.pi**2 * m * c / 2
    s1.loglog(ff, f_squared_fit(ff, alpha), ls='--', label=f'Arm Power = {arm_power / 1e3:.1f} [kW]')

s1.set_ylabel(r'$|L_{D}/RIN|$' + ' [m/RIN]')
s2.set_ylabel('Phase [deg]')
s3.set_ylabel('Coherence')
s3.set_xlabel('Frequency [Hz]')

s1.set_ylim([1e-10, 1e-7])
s2.set_ylim([-90, 270])
s3.set_ylim([0.94, 1.0])

s1.set_yticks([1e-10, 1e-9, 1e-8, 1e-7])
s2.set_yticks([-90, 0, 90, 180, 270])
s3.set_yticks([0.94, 0.96, 0.98, 1.0])

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.5)
s3.grid()
s3.grid(which='minor', ls='--', alpha=0.5)

s1.legend(fontsize=12, ncol=2)

plot_name = f'arm_rin_to_darm.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()

# 
# fig = nu.plot_TFs_B(tf_dict, darm, plot_chans=rinify_chans,
#                     title='SRCL dither TFs for arm power measurement', units='m/RIN')
# ss = fig.get_axes()

# for s0 in ss:
#     lines = s0.lines
#     for line, marker in zip(lines, markers):
#         #line.set_linestyle(' ') # no line
#         line.set_linewidth(2)
#         line.set_marker(marker)
#         line.set_markersize(9)

# for chan in rinify_chans:
#     ff = tf_dict[darm][chan]['ff']
#     alpha = tf_dict[darm][chan]['alpha']
#     arm_power = alpha * np.pi**2 * m * c / 2
#     ss[0].loglog(ff, f_squared_fit(ff, alpha), ls='--', label='$%.2e/f^2$ => Arm Power = %.3f [kW]'%(alpha, arm_power/1e3))

# ss[0].legend()

# fig2 = nu.plot_TFs_B(tf_dict, darm, plot_chans=[src], title='DARM/SRCL CTRL', units='m/cts')
# ss = fig2.get_axes()
# markers = np.array(['o', 's', '^', 'v'])
# for s0 in ss:
#     lines = s0.lines
#     for line, marker in zip(lines, markers):
#         #line.set_linestyle(' ') # no line
#         line.set_linewidth(1)
#         line.set_marker(marker)
#         line.set_markersize(9)

# ff = tf_dict[darm][src]['ff']
# alpha = tf_dict[darm][src]['alpha']
# ss[0].loglog(ff, f_to_the_fourth_fit(ff, alpha), ls='--', label='$%.2e/f^4$'%(alpha))
# ss[0].legend()

# fig3 = nu.plot_TFs_A(tf_dict, src, plot_chans=rinify_chans, title='Arm RIN/SRCL CTRL', units='RIN/cts')
# ss = fig3.get_axes()
# markers = np.array(['o', 's', '^', 'v'])
# for s0 in ss:
#     lines = s0.lines
#     for line, marker in zip(lines, markers):
#         #line.set_linestyle(' ') # no line
#         line.set_linewidth(1)
#         line.set_marker(marker)
#         line.set_markersize(9)

# for chan in rinify_chans:
#     ff = tf_dict[chan][src]['ff']
#     alpha3 = tf_dict[chan][src]['alpha']
#     ss[0].loglog(ff, f_squared_fit(ff, alpha3), ls='--', label='$%.2e/f^2$'%(alpha3))

# ss[0].legend()

# fig = nu.make_legend_interactive(fig)
# fig2 = nu.make_legend_interactive(fig2)
# fig3 = nu.make_legend_interactive(fig3)

# plt.ioff() # at end of script, show the plot
# plt.show()
