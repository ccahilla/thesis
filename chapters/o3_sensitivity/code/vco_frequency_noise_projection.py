'''
vco_frequency_noise_projection.py

Craig Cahillane
December 25 2020
'''
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.optimize as sco
import scipy.constants as scc
import scipy.special as scp
import os

import nds2utils as nu
from nds2utils.make_interactive_svg import make_interactive_svg_multiple_subplots

# plt.style.use('tableau-colorblind10')

mpl.rcParams.update({'text.usetex': True,
                     'figure.figsize': (12, 9),
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9,
                     'agg.path.chunksize': 10000})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])



#####   Functions   #####
def save_asd_txt(fff, asd, filename, header, verbose=True):
    '''Stores a two-column txt of frequency and power spectral density
    fff = frequency vector in Hertz.  Must be np.array with single row of values.
    asd = amplitude spectral density [units/rtHz].  Must be np.array of same length as fff. 
    filename = full directory path to the file and filename
    header = text to go at the top of the txt
    '''
    save_data = np.vstack((fff, asd)).T
    if verbose:
        print()
        print(f'Saving data file...')
    np.savetxt(filename, save_data, header=header)
    if verbose:
        print(f'{filename}')
    return


###   Log frequency vector   ###
fflog = np.logspace(np.log10(5), np.log10(1e5), 3000)


###   Get Data   ###
# Get VCO FDD measured data
data = np.loadtxt(f'{data_dir}/VCO_FDD_frequency_noise_140415_E.txt')
ff_vco = data[:,0]
asd_vco = data[:,1] # should be in units of Hz/rtHz, https://alog.ligo-wa.caltech.edu/aLOG/uploads/11385_20140416105415_VCO_FDD_noise_140415_E.pdf

idx = np.argwhere(ff_vco[-1] > fflog)[0,0]
slope = 0.4
asd_vco_extrap = asd_vco[-1] * (fflog/ff_vco[-1])**slope

asdlog_vco_low = nu.logbin_ASD(fflog, ff_vco, asd_vco)

asdlog_vco = np.append( asdlog_vco_low[:idx], asd_vco_extrap[idx:])

# Get MC2 crossover
data = np.loadtxt(f'{data_dir}/20190423_MCL_Crossover_TF.txt')
ff_gmc2 = data[:,0]
tf_gmc2 = data[:,1] * np.exp(1j * np.pi/180 * data[:,2])

sup_mc2_full = 1 / (1 - tf_gmc2)

def sup_mc2(fff):
    '''mc2 supression interpolated'''
    return nu.get_complex_interp(fff, ff_gmc2, sup_mc2_full)


###   Definitions   ###
# Using /chapters/o3_sensitivity/mathematica/carm_calibration_algebra.nb in my thesis git repo

### A is the REFL A sensing chain
REFLA9I_sensing_chain = 1887.7 # V/W
def A(fff):
    return nu.tf_zpk(fff, [], [], gain=REFLA9I_sensing_chain)

SumNodeGain = +8.0 # dB
REFL_IN1Gain = +12 # dB, normally +6 dB for split sensing, see REFL_IN1Gain2
REFL_FASTGain = +16 # dB
IMC_IN2Gain = -22 # dB, # value as of April

def REFL_Boost1(fff):
    ''' Pole at 10 Hz, Zero at 500 Hz '''
    return nu.tf_zpk(fff, [500], [10], gain=500/10)
def REFL_Comp(fff):
    ''' Pole at 40 Hz, Zero at 4000 Hz '''
    return nu.tf_zpk(fff, [4000], [40], gain=4000/40)
def REFL_Fast(fff):
     ''' Two passive 5 Hz high pass filters '''
     return nu.tf_zpk(fff, [0,0], [5,5], gain=1/5**2)

### F is the CARM only analog filters and REFL A sensing chain###   
def F(fff):
    ff  = nu.dB2Mag(SumNodeGain) \
        * nu.dB2Mag(REFL_IN1Gain) \
        * nu.dB2Mag(REFL_FASTGain) \
        * nu.dB2Mag(IMC_IN2Gain) \
        * REFL_Fast(fff) \
        * REFL_Boost1(fff) \
        * REFL_Comp(fff)
    return ff

### P is the IMC trans passive pole (gain of 1, pole at 8 kHz)###   
IMC_pole = 8.6e3 # Hz
def P(fff):
    '''IMC passive pole at 8.6 kHz'''
    return nu.tf_zpk(fff, [], [IMC_pole], gain=1)

### I is the IMC plant in W/Hz ###   
Pin = 35.0 # W
L_IMC = 32.9434 # m
FSR = scc.c / L_IMC # Hz
T_MC1 = 6030e-6
T_MC3 = 5845e-6
r_MC1 = np.sqrt(1 - T_MC1)
r_MC3 = np.sqrt(1 - T_MC3)
mod_index_IMC = 0.0125 # rad
estimated_IMC_plant_gain = 4 * np.pi * scp.j0(mod_index_IMC) * scp.j1(mod_index_IMC) * Pin * r_MC3 * T_MC1 \
                         / (FSR * (1 - r_MC1 * r_MC3)**2)
print()
print(f'estimated_IMC_plant_gain = {estimated_IMC_plant_gain} W/Hz')
print()

mystery_IMC_gain = 9e-4 # probably power dumpage, W/W
IMC_plant_gain = estimated_IMC_plant_gain # W/Hz
IMC_REFL_sensing_chain = 1565.0 #3622./2 # V/W
IMC_IN1Gain = 2.0 # dB, high power on IMC REFL gain, +22 is low power number
def I(fff):
    ''' IMC optical response times the sensing chain gain times some mystery gain factor of 1e-2'''
    ii  = mystery_IMC_gain \
        * IMC_plant_gain \
        * P(fff)
    return ii

### K is the IMC REFL sensing chain, plus the analog IMC_IN1Gain slider
def K(fff):
    kk  = IMC_REFL_sensing_chain \
        * nu.dB2Mag(IMC_IN1Gain)
    return kk

### H is the VCO and analog filters after the CARM and IMC sum point ###   

IMC_fastdelay = 1.25e-6 # s
def IMC_fastdelayTF(fff):
    '''Delay of the CARM loop'''
    return np.exp(-1j*2*np.pi* IMC_fastdelay * fff)

IMC_FASTGain = -18 # dB

def IMC_Boost1(fff):
    ''' Pole at 1 kHz, Zero at 20 kHz '''
    return nu.tf_zpk(fff, [20e3], [1e3], gain=20e3/1e3)
def IMC_Boost2(fff):
    ''' Pole at 1 kHz, Zero at 20 kHz '''
    return nu.tf_zpk(fff, [20e3], [1e3], gain=20e3/1e3)
def IMC_Comp(fff):
    ''' Pole at 40 Hz, Zero at 4000 Hz '''
    return nu.tf_zpk(fff, [4e3], [4e1], gain=4e3/4e1)
def IMC_Fast(fff):
    ''' IMC Fast Option + High Pass Filter '''
    fastzeros = np.array([70.0e3])
    fastpoles = np.array([200e3, 2.4e6, 2.4e6, 140.0e3])
    fastgain = 1
    fastgain *= 2.276  # idk what's in the fast option, but it's on...
    # https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=29735
    return nu.tf_zpk(fff, fastzeros, fastpoles, gain=fastgain)

def H(fff):
    hh  = IMC_Comp(fff) \
        * IMC_Boost1(fff) \
        * IMC_Boost2(fff) \
        * IMC_Fast(fff) \
        * nu.dB2Mag(IMC_FASTGain) \
        * IMC_fastdelayTF(fff)
    return hh

### V is the VCO in Hz/V
IMC_AOM_doublepass = 2.0 # Hz/Hz
IMC_VCO_volts_to_hertz = 268302.0 # Hz/Vfast, # Evan had 536604 Hz/V for the doublepass
def IMC_VCO(fff):
    ''' Pole at 1.6 Hz, Zero at 40 Hz '''
    return nu.tf_zpk(fff, [40.], [1.6], gain=1)

def V(fff):
    vv  = IMC_AOM_doublepass \
        * IMC_VCO_volts_to_hertz \
        * IMC_VCO(fff)
    return vv

### C is the CARM plant in W/Hz ###   
CARM_plant_gain = -3.37e-3 # W/Hz
CARM_pole = 0.6 # Hz
def C(fff):
    cc = nu.tf_zpk(fff, [], [CARM_pole], gain=CARM_plant_gain)
    return cc



#####   Noise projection   #####
nv = asdlog_vco # original vco noise in Hz/rtHz
total_loop_gain =  ( I(fflog) * K(fflog) + A(fflog) * C(fflog) * F(fflog) * P(fflog) ) * H(fflog) * V(fflog)
total_loop_suppression = 1 / (1 - total_loop_gain)

nv_r = nv * np.abs( total_loop_suppression * sup_mc2(fflog) * P(fflog) ) # projected vco noise



#####   Save data   #####

full_save_filename = f'{data_dir}/vco_noise_projection_incident_frequency_noise.txt'
header = 'VCO noise projection from measured IMC VCO noise to incident on IFO\nFrequency [Hz], ASD noise [Hz/rtHz]'
save_asd_txt(fflog, nv_r, full_save_filename, header, verbose=True)



#####   Figures   #####
# Original VCO noise
fig, (s1) = plt.subplots(1)

s1.loglog(ff_vco, asd_vco, label='Original')
s1.loglog(fflog, asdlog_vco_low, label='Logbinned')
s1.loglog(fflog, asd_vco_extrap, label='Extrapolated')

s1.set_ylabel('Frequency ASD ' + r'[$\mathrm{Hz/\sqrt{Hz}}$]')
s1.set_xlabel('Frequency [Hz]')

# s1.set_ylim([1e-2, 1e2])

s1.set_yticks( nu.good_ticks(s1) )

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)

s1.legend()

plot_name = f'vco_noise_original.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# VCO noise projection
fig, (s1) = plt.subplots(1)

# s1.loglog(ff_vco, asd_vco, label='Original')
s1.loglog(fflog, nv_r, label='Projection')

s1.set_ylabel('Frequency ASD ' + r'[$\mathrm{Hz/\sqrt{Hz}}$]')
s1.set_xlabel('Frequency [Hz]')

# s1.set_ylim([1e-2, 1e2])

s1.set_yticks( nu.good_ticks(s1) )

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)

s1.legend()

plot_name = f'vco_noise_projection.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# total gain tfs
fig, (s1, s2) = plt.subplots(2, sharex=True)


s1.loglog(fflog, np.abs(total_loop_gain), label='Total CARM loop gain')
s2.semilogx(fflog, 180/np.pi * np.angle(total_loop_gain), label='Total CARM loop gain')

s1.set_ylabel('Crossover Mag')
s2.set_ylabel('Phase [deg]')
s2.set_xlabel('Frequency [Hz]')

# s1.set_ylim([1e-2, 1e2])
s2.set_ylim([-180, 180])

s1.set_yticks( nu.good_ticks(s1) )
s2.set_yticks([-180, -90, 0, 90, 180])

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.5)

s1.legend()

plot_name = f'total_carm_loop_tf.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()