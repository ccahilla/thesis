# coding: utf-8

# Loads, saves, and plots (as desired) range information for H1/L1 over the course of O3.
# It only uses data when the IFO was "observing"
# 9 Aug 2019 A. Buikema

# To fix/improve: remove data with glitches or otherwise smooth data

import numpy as np
from gwpy.timeseries import TimeSeries, TimeSeriesDict
from gwpy.segments import DataQualityFlag, DataQualityDict
import matplotlib.pyplot as plt
import bisect


### Toggle me to either grab new data or load local copy of data (only need to grab new data once)
grab_new_data = False


#run = 'a'
run = 'b'

print('Looking at O3'+run)
if run=='a':
    #O3a
    gps0 = 1238166018
    gps1 = 1253977218


else:
    #O3b
    gps0 = 1256655618
    gps1 = 1269363618

range_file_name = 'range_dataO3'+run+'.hdf5'
obs_status_file_name = 'obs_statusO3'+run+'.hdf5'


gps0 = gps0 - gps0%60
gps1 = gps1 - gps1%60
start_time = gps0
end_time = gps1

lho_range_chan = 'H1:DMT-SNSH_EFFECTIVE_RANGE_MPC.mean'
llo_range_chan = 'L1:DMT-SNSH_EFFECTIVE_RANGE_MPC.mean'

lho_obs_state = 'H1:DMT-ANALYSIS_READY:1'
llo_obs_state = 'L1:DMT-ANALYSIS_READY:1'

range_chan_list = [lho_range_chan,llo_range_chan]
obs_state_chans = [lho_obs_state,llo_obs_state]


if grab_new_data:
    print('Grabbing new data')
    # Grab the data via nds2
    data = TimeSeriesDict.get(range_chan_list, start_time, end_time, verbose=True)

    # Check data quality
    segs = DataQualityDict.query(obs_state_chans,start_time,end_time,verbose=True)

    # And save data
    data.write(range_file_name,overwrite=True)
    #range_data = data.copy()

    segs.write(obs_status_file_name,overwrite=True)
    #segments = segs.copy()

print('Loading local copy of file')
range_data = TimeSeriesDict.read(range_file_name)
segments = DataQualityDict.read(obs_status_file_name)
print(range_data)
print(segments)



# Old, slow way of doing this -AWB
'''
# Now downselect values of range when in observing mode
# What's a faster/smarter way of doing this?!?!?
# Maybe split up time series by segment instead of using a mask?

mask = {}
# Iterate through IFOs
for (ifo,ifo_obs) in zip(range_chan_list,obs_state_chans):
    # Initialize mask for each IFO
    mask[ifo] = np.zeros(range_data[ifo].size,dtype=bool)
    # Iterate over all segments to find when observing
    ii=1
    num_seg = len(segments[ifo_obs].active)
    for seg in segments[ifo_obs].active:
        print('Checking '+str(ifo[0:2])+' segment '+str(ii)+' of '+str(num_seg))
        observing = (range_data[ifo].times.value>seg.start) & (range_data[ifo].times.value<seg.end)
        mask[ifo] = mask[ifo] | observing
        ii+=1
'''


mask = {}
# Iterate through IFOs
cleaner_range = False # Do we want full observing data or ignore transients at beginning/end of lock?
if cleaner_range:
    pad = 300 # Ignore this many seconds before start/end of lock
    extra_str = ''
else:
    pad = 60 # We do need to remove a minute on both sides, since otherwise we'll have a bunch of range=0 values that will affect statistics
    extra_str = '_raw'
for (ifo,ifo_obs) in zip(range_chan_list,obs_state_chans):
    # Initialize mask for each IFO
    temp_mask = np.zeros(range_data[ifo].size,dtype=bool)
    # Iterate over all segments to find when observing
    ii=1
    num_seg = len(segments[ifo_obs].active)
    gpstimes = range_data[ifo].times.value
    hi_index = 0
    active_segs = segments[ifo_obs].active
    for seg in active_segs:
        print('Checking '+str(ifo[0:2])+' obs segment '+str(ii)+' of '+str(num_seg))
        # Since times and segments are sorted, we can search for first segment start time
        # Future searches can start after the last segment ended
        lo_index = bisect.bisect_left(gpstimes,seg.start+pad,lo=hi_index)
        hi_index = bisect.bisect_right(gpstimes,seg.end-pad,lo=lo_index)
        temp_mask[lo_index:hi_index] = True
        ii+=1
    mask[ifo] = temp_mask


masked_data = {}
masked_times = {}
for ifo in range_chan_list:
    masked_data[ifo] = range_data[ifo].value[mask[ifo]]
    masked_times[ifo] = range_data[ifo].times.value[mask[ifo]]

# And save data
data2save = [masked_times, masked_data]
savefile = 'range_timeseries_cleaned_o3'+run+extra_str+'.npy'

np.save(savefile,data2save)


# Now plot everything to make sure it works
# But DON'T use this for nice plot
for ifo in range_chan_list:
    plt.plot(masked_times[ifo],masked_data[ifo],'.')

plt.show()
