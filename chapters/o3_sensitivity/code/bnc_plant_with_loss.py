'''
bnc_plant_with_loss.py

Plots Buonanno and Chen DARM model including losses from https://arxiv.org/pdf/gr-qc/0102012.pdf

main(phi) function produces a plot using phi as the SRC detuning.

Usage: 
Produce and save plots of DARM plant
$ python BnC_vs_Ward_vs_Hall_vs_Cal_DARM_plant.py

Interactive plot of DARM plants
$ python BnC_vs_Ward_vs_Hall_vs_Cal_DARM_plant.py -i 

Interactive plot of DARM plants with residual comparisons
$ python BnC_vs_Ward_vs_Hall_vs_Cal_DARM_plant.py -i -r

Interactive plot of non-squeezing quantum noise ASDs
$ python BnC_vs_Ward_vs_Hall_vs_Cal_DARM_plant.py -i -a

Craig Cahillane
May 7, 2020

'''

import os
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
import scipy.constants as scc
import argparse

mpl.rcParams.update({'figure.figsize': (12, 9),
                     'text.usetex': False,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'lines.markersize': 3,
                     'font.size': 12, #22,
                    #  'xtick.labelsize': 'large',
                    #  'ytick.labelsize': 'large',
                    #  'legend.fancybox': True,
                     'legend.fontsize': 12, #18,
                     'legend.framealpha': 0.9,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'legend.columnspacing': 2,
                    #  'savefig.dpi': 80,
                    #  'pdf.compression': 9
                     })

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Functions   #####
def parse_args():
    ''' Parse command-line arguments using argparse library 
    '''
    parser = argparse.ArgumentParser(description='DARM plant model comparison')
    parser.add_argument('--interactive', '-i', action='store_true', 
                        help='Flag. If set, creates an interactive plot of each models with sliders for comparison.')
    parser.add_argument('--no_simple_pole_approximation', '-n', action='store_true', 
                        help='Flag. If set, does not use the simple pole approximation for BnC (does not work yet).')
    parser.add_argument('--residuals', '-r', action='store_true', 
                        help='Flag. If set, adds residuals to the  interactive plot.')
    parser.add_argument('--asd', '-a', action='store_true',
                        help='Flag.  If set, plots an interactive quantum ASD instead.')
    parser.add_argument('--quiet', '-q', action='store_true',
                        help='Flag.  If set, makes the lho_xcor.pdf plot for the o3 paper.')

    args = parser.parse_args()
    return args

### Buonanno and Chen Model Functions
def cavity_pole(L,
                T1,
                T2):
    '''Computes the cavity pole of a two mirror cavity in Hz.

    Inputs:
    L = length of the cavity in meters
    T1 = power transmission of the input mirror
    T2 = power transmission of the end mirror

    Outputs:
    pole = cavity pole in Hz
    '''
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    FSR = scc.c/(2 * L)
    pole = FSR * np.log(1 / (r1 * r2)) / (2 * np.pi)
    return pole

def SQL(ff,
        L,
        M=40.0,
        ):
    '''Calculates the standard quantum limit for graviational wave detection.
    Equation 2.12 of https://arxiv.org/pdf/gr-qc/0102012.pdf 
    Returns the limiting noise ASD in sqrt(S_h) with units of strain/rtHz.

    Inputs:
    ff  = frequency vector in Hz
    L   = length of the interferometer arms (nominal = 3994.5 meters)    
    M   = mass of the input and end test masses.  Default is 40 kg.

    Returns:
    sql = standard quantum limit for GW detection in strain/rtHz
    '''
    Omega = 2 * np.pi * ff
    S_h = 8 * scc.hbar/(M * Omega**2 * L**2) 
    sql = np.sqrt(S_h)
    return sql

def Pbs_SQL(L,
            arm_pole,
            M=40.0,
            lambda0=1064e-9,
            ):
    '''Calculates the power on the beamsplitter needed to achieve the standard quantum limit
    in a conventional interferometer at gamma = Omega, 
    where gamma is the arm pole and Omega is the GW audio frequency.
    Written as I_SQL.
    Equation 2.14 of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    L           = length of the interferometer arms (nominal = 3994.5 meters)   
    arm_pole    = arm cavity pole in Hz
    M           = mass of the input and end test masses.  Default is 40 kg.
    lambda0     = wavelength of the laser.  Default is 1064 nm.

    Output:
    i_sql = input intensity for standard quantum limit for GW detection in watts
    '''
    omega0 = 2 * np.pi * scc.c / lambda0    # radians
    gamma = 2 * np.pi * arm_pole            # radians
    pbs_sql = M * L**2 * gamma**4 / (4 * omega0) # watts
    return pbs_sql

def Phi(ff,
        l_SRC,
        lambda0=1064e-9):
    '''Calculates the net phase gained by the audio sideband frequency ff
    while in the signal recycling cavity with carrier detuning phi.
    From section after Equation 2.14 in https://arxiv.org/pdf/gr-qc/0102012.pdf

    Phi = Omega * length_SRC / c

    Inputs:
    ff      = frequency vector in Hz
    l_SRC   = length of the SRC in meters (nominal = 56.0)

    Output:
    Phi = audio sideband detuning for all ff in rads
    '''

    Omega = 2 * np.pi * ff
    Phi = -Omega * l_SRC / scc.c # minus sign for ward model convention

    return Phi

def beta(   ff,
            L,
            T1,
            T2,
            single_pole_approx=True,
        ):
    '''Calculates the net phase gained by the audio sideband frequency ff
    while in the arm cavity with pole arm_pole in Hz.
    
    There are two parts to this code.
    The first part is the beta assumed by KLMTV in 
    https://arxiv.org/pdf/gr-qc/0008026.pdf
    This is a result of the so-called single-pole approximation 
    done by KLMTV in Appendix B, Equation B13.
    Buonanno and Chen also use this convention.

    In reality, there are infinite poles for a two mirror cavity, if we solve
    the cavity eqs denom: 1 - r1 r2 Exp[2 I Omega L / c] == 0. 
    The second part is a model extension done by me to extend the beta param
    to be valid even if the FSR is not much greater than the cavity pole.
    This is important for future detectors, as the FSR goes down with long arms,
    but the DARM cavity bandwidth goes up.

    Use the single_pole_approx flag to switch between the KLMTV and Craig beta params.

    Inputs:
    ff          = frequency vector in Hz
    L = length of the cavity in meters
    T1 = power transmission of the input mirror
    T2 = power transmission of the end mirror

    Outputs:
    beta        = phase gained in radians for each audio frequency ff
    '''
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)

    arm_pole = cavity_pole(L, T1, T2)
    
    Omega = 2 * np.pi * ff
    gamma = 2 * np.pi * arm_pole
    if single_pole_approx:
        beta = -np.arctan2(Omega, gamma) # minus sign for ward model convention

    else:
        numer = r1 * r2 * np.sin(2 * Omega * L / scc.c)
        denom = 1 - r1 * r2 * np.cos(2 * Omega * L / scc.c)
        beta = -np.arctan2(numer, denom) # minus sign for ward model convention

    return beta

def kappa(  ff,
            Pbs,
            L,
            arm_pole,
            M=40.0,
            lambda0=1064e-9
            ):
    '''Calculates the effective coupling constant which governs the 
    coupling of amplitude fluctuations to phase fluctuations in the arms.
    Originally derived in KLMTV.
    Equation 2.13 of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    ff          = frequency vector in Hz
    Pbs         = power on beam splitter in watts
    L           = length of the interferometer arms (nominal = 3994.5 meters)   
    arm_pole    = arm cavity pole in Hz
    M           = mass of the input and end test masses.  Default is 40 kg.
    lambda0     = wavelength of the laser.  Default is 1064 nm.

    Output:
    kappa       = frequency dependent effective coupling constant 
                  from amplitude to phase quadratures
    '''
    Omega = 2 * np.pi * ff          # radians
    gamma = 2 * np.pi * arm_pole    # radians

    pbs_sql = Pbs_SQL(L, arm_pole, M=M, lambda0=lambda0)

    kappa = 2 * (Pbs / pbs_sql) * gamma**4 / (Omega**2 * (gamma**2 + Omega**2))

    return kappa

def C11(ff,
        Ts,
        Ti,
        Te,
        phi,
        Pbs,
        L,
        l_SRC,
        M=40.0,
        lambda0=1064e-9,
        single_pole_approx=True,
        ):
    '''Calculates matrix element 11 for the IFO AS port noise matrix.
    Used in Equation 2.20.
    Defined in Equation 2.22 of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    ff          = frequency vector in Hz
    Ts          = power transmission of the signal-recyling mirror
    Ti          = power transmission of ITM (nominal = 1.42%)
    Te          = power transmission of ETM (nominal = 4 ppm)
    phi         = SRC detuning in radians (nominal = pi/2)
    Pbs         = power on beam splitter in watts
    L           = length of the interferometer arms (nominal = 3994.5 meters)   
    l_SRC       = length of the signal recycling cavity (nominal = 56.0 meters)
    M           = mass of the input and end test masses.  Default is 40 kg.
    lambda0     = wavelength of the laser. Default is 1064e-9 m
    single_pole_approx = Assume the single pole approximation. Default is False

    Outputs:
    C11         = matrix element C11
    '''
    rs = np.sqrt(1 - Ts) # rho in the paper

    arm_pole = cavity_pole(L, Ti, Te)

    betas = beta(ff, L, Ti, Te, single_pole_approx=single_pole_approx)
    Phis = Phi(ff, l_SRC, lambda0=lambda0)
    kappas = kappa(ff, Pbs, L, arm_pole, M=M, lambda0=lambda0)

    C11_a = (1 + rs**2) * ( np.cos(2 * phi) + kappas * np.sin(2 * phi) / 2)
    C11_b = -2 * rs * np.cos( 2 * (betas + Phis) )
    C11 = C11_a + C11_b

    return C11

def C22(ff,
        Ts,
        Ti,
        Te,
        phi,
        Pbs,
        L,
        l_SRC,
        M=40.0,
        lambda0=1064e-9,
        single_pole_approx=True,
        ):
    '''Calculates matrix element 22 for the IFO AS port noise matrix.
    Used in Equation 2.20.
    Defined in Equation 2.22 of https://arxiv.org/pdf/gr-qc/0102012.pdf 
    C11 = C22, so this function just calls C11.

    Inputs:
    ff          = frequency vector in Hz
    Ts          = power transmission of the signal-recyling mirror
    Ti          = power transmission of ITM (nominal = 1.42%)
    Te          = power transmission of ETM (nominal = 4 ppm)
    phi         = SRC detuning in radians (nominal = pi/2)
    Pbs         = power on beam splitter in watts
    L           = length of the interferometer arms (nominal = 3994.5 meters)   
    l_SRC       = length of the signal recycling cavity (nominal = 56.0 meters)
    M           = mass of the input and end test masses.  Default is 40 kg.
    lambda0     = wavelength of the laser. Default is 1064e-9 m
    single_pole_approx = Assume the single pole approximation. Default is False

    Outputs:
    C22         = matrix element C22
    '''

    C22 = C11(  ff, Ts, Ti, Te, phi, Pbs, L, l_SRC, M=M, lambda0=lambda0,
                single_pole_approx=single_pole_approx)

    return C22

def C12(ff,
        Ts,
        Ti,
        Te,
        phi,
        Pbs,
        L,
        M=40.0,
        lambda0=1064e-9,
        ):
    '''Calculates matrix element 12 for the IFO AS port noise matrix.
    Used in Equation 2.20.
    Defined in Equation 2.23a of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    ff          = frequency vector in Hz
    Ts          = power transmission of the signal-recyling mirror
    Ti          = power transmission of ITM (nominal = 1.42%)
    Te          = power transmission of ETM (nominal = 4 ppm)
    phi         = SRC detuning in radians (nominal = pi/2)
    Pbs         = power on beam splitter in watts
    L           = length of the interferometer arms (nominal = 3994.5 meters)   
    arm_pole    = arm cavity pole in Hz
    M           = mass of the input and end test masses.  Default is 40 kg.
    lambda0     = wavelength of the laser. Default is 1064e-9 m

    Outputs:
    C12         = matrix element C12
    '''
    ts = np.sqrt(Ts) # tau in the paper

    arm_pole = cavity_pole(L, Ti, Te)

    kappas = kappa(ff, Pbs, L, arm_pole, M=M, lambda0=lambda0)

    C12 = -ts**2 * ( np.sin( 2 * phi ) + kappas * np.sin(phi)**2 )

    return C12

def C21(ff,
        Ts,
        Ti,
        Te,
        phi,
        Pbs,
        L,
        M=40.0,
        lambda0=1064e-9,
        ):
    '''Calculates matrix element 21 for the IFO AS port noise matrix.
    Used in Equation 2.20.
    Defined in Equation 2.23b of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    ff          = frequency vector in Hz
    Ts          = power transmission of the signal-recyling mirror
    Ti          = power transmission of ITM (nominal = 1.42%)
    Te          = power transmission of ETM (nominal = 4 ppm)
    phi         = SRC detuning in radians (nominal = pi/2)
    Pbs         = power on beam splitter in watts
    L           = length of the interferometer arms (nominal = 3994.5 meters)
    M           = mass of the input and end test masses.  Default is 40 kg.
    lambda0     = wavelength of the laser. Default is 1064e-9 m

    Outputs:
    C21         = matrix element C21
    '''
    ts = np.sqrt(Ts) # tau in the paper

    arm_pole = cavity_pole(L, Ti, Te)

    kappas = kappa(ff, Pbs, L, arm_pole, M=M, lambda0=lambda0)

    C21 = ts**2 * ( np.sin( 2 * phi ) - kappas * np.cos(phi)**2 )

    return C21

def D1( ff,
        Ts,
        Ti,
        Te,
        phi,
        L,
        l_SRC,
        lambda0=1064e-9,
        single_pole_approx=True,
        ):
    '''Calculates signal-to-output for the amplitude quadrature.
    Used in Equation 2.20.
    Defined in Equation 2.24a of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    ff          = frequency vector in Hz
    Ts          = power transmission of the signal-recyling mirror
    Ti          = power transmission of ITM (nominal = 1.42%)
    Te          = power transmission of ETM (nominal = 4 ppm)    
    phi         = SRC detuning in radians (nominal = pi/2)
    L           = length of the interferometer arms (nominal = 3994.5 meters)
    l_SRC       = length of the signal recycling cavity (nominal = 56.0 meters)
    lambda0     = wavelength of the laser. Default is 1064e-9 m.
    single_pole_approx = Assume the single pole approximation. Default is False

    Outputs:
    D1          = signal to amplitude quadrature output for all ff
    '''
    rs = np.sqrt(1 - Ts) # rho in the paper

    betas = beta(ff, L, Ti, Te, single_pole_approx=single_pole_approx)
    Phis = Phi(ff, l_SRC, lambda0=lambda0)

    D1 = -(1 + rs * np.exp(2j*(betas + Phis))) * np.sin(phi)

    return D1

def D2( ff,
        Ts,
        Ti,
        Te,
        phi,
        L,
        l_SRC,
        lambda0=1064e-9,
        single_pole_approx=True,
        ):
    '''Calculates signal-to-output for the phase quadrature.
    Used in Equation 2.20.
    Defined in Equation 2.24b of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    ff          = frequency vector in Hz
    Ts          = power transmission of the signal-recyling mirror
    Ti          = power transmission of ITM (nominal = 1.42%)
    Te          = power transmission of ETM (nominal = 4 ppm)    
    phi         = SRC detuning in radians (nominal = pi/2)
    L           = length of the interferometer arms (nominal = 3994.5 meters)
    l_SRC       = length of the signal recycling cavity (nominal = 56.0 meters)
    lambda0     = wavelength of the laser. Default is 1064e-9 m.
    single_pole_approx = Assume the single pole approximation. Default is False

    Outputs:
    D2          = signal to phase quadrature output for all ff
    '''
    rs = np.sqrt(1 - Ts) # rho in the paper

    betas = beta(ff, L, Ti, Te, single_pole_approx=single_pole_approx)
    Phis = Phi(ff, l_SRC, lambda0=lambda0)

    D2 = -(-1 + rs * np.exp(2j*(betas + Phis))) * np.cos(phi)

    return D2

def MM( ff,
        Ts,
        Ti,
        Te,
        phi,
        Pbs,
        L,
        l_SRC,
        M=40.0,
        lambda0=1064e-9,
        single_pole_approx=True,
        ):
    '''Calculates normalization factor for the IFO response to input noise and signal.
    Used in Equation 2.20.
    Defined in Equation 2.21 of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    ff          = frequency vector in Hz
    Ts          = power transmission of the signal-recyling mirror
    Ti          = power transmission of ITM (nominal = 1.42%)
    Te          = power transmission of ETM (nominal = 4 ppm) 
    phi         = SRC detuning in radians (nominal = pi/2)
    Pbs         = power on beam splitter in watts
    L           = length of the interferometer arms (nominal = 3994.5 meters)   
    l_SRC       = length of the signal recycling cavity (nominal = 56.0 meters)
    M           = mass of the input and end test masses.  Default is 40 kg.
    lambda0     = wavelength of the laser. Default is 1064e-9 m
    single_pole_approx = Assume the single pole approximation. Default is False

    Outputs:
    MM          = normalization factor for all ff      
    '''
    rs = np.sqrt(1 - Ts) # rho in the paper

    arm_pole = cavity_pole(L, Ti, Te)

    Phis = Phi(ff, l_SRC, lambda0=lambda0)
    betas = beta(ff, L, Ti, Te, single_pole_approx=single_pole_approx)
    kappas = kappa(ff, Pbs, L, arm_pole, M=M, lambda0=lambda0)

    MM  = 1 + rs**2 * np.exp(4j*(betas + Phis)) \
        - 2 * rs * np.exp(2j*(betas + Phis)) * (np.cos(2 * phi) + kappas * np.sin(2 * phi)/2.0)

    return MM

def D1L(ff,
        Ts,
        Ti,
        Te,
        phi,
        L,
        l_SRC,
        arm_loss,
        SRC_loss,
        post_SRM_loss,
        lambda0=1064e-9,
        single_pole_approx=True,
        ):
    '''Calculates signal-to-output for the amplitude quadrature,
    including lossy arms, SRC, and PD.
    Used in Equation 5.6.
    Defined in Equation 5.9a of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    ff          = frequency vector in Hz
    Ts          = power transmission of the signal-recyling mirror
    Ti          = power transmission of ITM (nominal = 1.42%)
    Te          = power transmission of ETM (nominal = 4 ppm)    
    phi         = SRC detuning in radians (nominal = pi/2)
    L           = length of the interferometer arms (nominal = 3994.5 meters)
    l_SRC       = length of the signal recycling cavity (nominal = 56.0 meters)
    lambda0     = wavelength of the laser. Default is 1064e-9 m.
    single_pole_approx = Assume the single pole approximation. Default is False

    Outputs:
    D1          = signal to amplitude quadrature output for all ff
    '''
    rs = np.sqrt(1 - Ts) # rho in the paper
    epsilon = 2 * arm_loss / Ti # epsilon in the paper
    lpd = post_SRM_loss  # lambdapd in the paper
    lsr = SRC_loss       # lambdasr in the paper

    betas = beta(ff, L, Ti, Te, single_pole_approx=single_pole_approx)
    Phis = Phi(ff, l_SRC, lambda0=lambda0)

    exp2b = np.exp(2j*(betas + Phis))
    #D1 = -(1 + rs * np.exp(2j*(betas + Phis))) * np.sin(phi)
    D1L = np.sqrt(1 - lpd) * ( \
              0.5 * lsr * rs * exp2b * np.sin(phi) \
            - (1 + rs * exp2b) * np.sin(phi) \
            + 0.25 * epsilon * np.sin(phi) * (3 + rs + 2 * rs * exp2b**2 + exp2b * (1 + 5 * rs) )
        )

    return D1L

def D2L(ff,
        Ts,
        Ti,
        Te,
        phi,
        L,
        l_SRC,
        arm_loss,
        SRC_loss,
        post_SRM_loss,
        lambda0=1064e-9,
        single_pole_approx=True,
        ):
    '''Calculates signal-to-output for the phase quadrature,
    including lossy arms, SRC, and PD.
    Used in Equation 5.6.
    Defined in Equation 5.9b of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    ff          = frequency vector in Hz
    Ts          = power transmission of the signal-recyling mirror
    Ti          = power transmission of ITM (nominal = 1.42%)
    Te          = power transmission of ETM (nominal = 4 ppm)    
    phi         = SRC detuning in radians (nominal = pi/2)
    L           = length of the interferometer arms (nominal = 3994.5 meters)
    l_SRC       = length of the signal recycling cavity (nominal = 56.0 meters)
    lambda0     = wavelength of the laser. Default is 1064e-9 m.
    single_pole_approx = Assume the single pole approximation. Default is False

    Outputs:
    D2          = signal to phase quadrature output for all ff
    '''
    rs = np.sqrt(1 - Ts) # rho in the paper
    epsilon = 2 * arm_loss / Ti # epsilon in the paper
    lpd = post_SRM_loss  # lambdapd in the paper
    lsr = SRC_loss       # lambdasr in the paper

    betas = beta(ff, L, Ti, Te, single_pole_approx=single_pole_approx)
    Phis = Phi(ff, l_SRC, lambda0=lambda0)

    exp2b = np.exp(2j*(betas + Phis))

    # D2 = -(-1 + rs * np.exp(2j*(betas + Phis))) * np.cos(phi)
    D2L = np.sqrt(1 - lpd) * ( \
              0.5 * lsr * rs * exp2b * np.cos(phi) \
            + (1 - rs * exp2b) * np.cos(phi) \
            + 0.25 * epsilon * np.cos(phi) * (-3 + rs + 2 * rs * exp2b**2 + exp2b * (-1 + 5 * rs) )
        )

    return D2L

def MML(ff,
        Ts,
        Ti,
        Te,
        phi,
        Pbs,
        L,
        l_SRC,
        arm_loss,
        SRC_loss,
        post_SRM_loss,
        M=40.0,
        lambda0=1064e-9,
        single_pole_approx=True,
        ):
    '''Calculates normalization factor for the IFO response to input noise and signal,
    including lossy arms, SRC, and PD.
    Used in Equation 5.6.
    Defined in Equation 5.7 of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    ff          = frequency vector in Hz
    Ts          = power transmission of the signal-recyling mirror
    Ti          = power transmission of ITM (nominal = 1.42%)
    Te          = power transmission of ETM (nominal = 4 ppm) 
    phi         = SRC detuning in radians (nominal = pi/2)
    Pbs         = power on beam splitter in watts
    L           = length of the interferometer arms (nominal = 3994.5 meters)   
    l_SRC       = length of the signal recycling cavity (nominal = 56.0 meters)
    M           = mass of the input and end test masses.  Default is 40 kg.
    lambda0     = wavelength of the laser. Default is 1064e-9 m
    single_pole_approx = Assume the single pole approximation. Default is False

    Outputs:
    MM          = normalization factor for all ff      
    '''
    rs = np.sqrt(1 - Ts) # rho in the paper
    epsilon = 2 * arm_loss / Ti # epsilon in the paper
    lpd = post_SRM_loss  # lambdapd in the paper
    lsr = SRC_loss       # lambdasr in the paper

    arm_pole = cavity_pole(L, Ti, Te)

    Phis = Phi(ff, l_SRC, lambda0=lambda0)
    betas = beta(ff, L, Ti, Te, single_pole_approx=single_pole_approx)
    kappas = kappa(ff, Pbs, L, arm_pole, M=M, lambda0=lambda0)

    exp2b = np.exp(2j*(betas + Phis))

    # MM  = 1 + rs**2 * np.exp(4j*(betas + Phis)) \
    #     - 2 * rs * np.exp(2j*(betas + Phis)) * (np.cos(2 * phi) + kappas * np.sin(2 * phi)/2.0)
    MML =   1 + rs**2 * exp2b**2 \
            - 2 * rs * exp2b * (np.cos(2 * phi) + 0.5 * kappas * np.sin(2 * phi)) \
            + lsr * rs * exp2b * (-rs * exp2b + np.cos(2 * phi) + 0.5 * kappas * np.sin(2 * phi)) \
            + epsilon * rs * exp2b * (2 * np.cos(betas)**2 * (np.cos(2 * phi) - rs * exp2b) + 0.5 * kappas * np.sin(2 * phi) * (3 + exp2b))

    return MML

def quantum_psd(ff,
                phi,
                zeta,
                E_LO,
                L,
                l_SRC,
                Ts,
                Ti,
                Te,
                Pbs,
                arm_loss,
                SRC_loss,
                post_SRM_loss,
                M=40.0,
                lambda0=1064e-9,
                single_pole_approx=True,
                ):
    '''Calculates the Buonanno and Chen DARM sensitivity.
    Based on Equation 3.5 of https://arxiv.org/pdf/gr-qc/0102012.pdf
    DOI: 10.1103/PhysRevD.64.042006

    The quantum ASD comes from the signal-to-noise ratio of the quantum
    shot noise input from the AS port, and the signal from the GW.

    Inputs:
    ff              = frequency vector
    phi             = SRC detuning in radians (nominal = pi/2)
    zeta            = homodyne angle in radians (nominal = 0)
    E_LO            = local oscillator amplitude 20 mA on DCPDs, sqrt(20 mA * 1e-3 / responsivity)
    L               = length of the interferometer arms (nominal = 3994.5 meters)
    l_SRC           = length of the SRC (nominal = 56.01 meters)
    Ts              = power transmission of SRM (nominal = 32%)
    Ti              = power transmission of ITM (nominal = 1.42%)
    Te              = power transmission of ETM (nominal = 4 ppm)
    Pbs             = power on the beamsplitter    (nominal = input * PRG)
    arm_loss        = losses in the arms      (nominal = 38 ppm)
    SRC_loss        = losses in the SRC, all grouped into SRM reflectivity losses (nominal = 1%)
    post_SRM_loss   = losses from back of SRM to OMC DCPDs (nominal = T_OFI * R_OMs * T_OMC * ModeMatchOMC)
    M               = mass of the input and end test masses.  Default is 40 kg.
    lambda0         = wavelength of the laser.  Default is 1064 nm.
    single_pole_approx = Assume the single pole approximation. Default is False

    Outputs:
    strain_psd = quantum-limited GW strain ASD in [strain/rtHz]
    '''
    ts = np.sqrt(Ts) # tau in the paper

    arm_pole = cavity_pole(L, Ti, Te)       # Hz
    h_SQL = SQL(ff, L, M=M)                 # standard quantum limit ASD
    kappas = kappa(ff, Pbs, L, arm_pole, M=M, lambda0=lambda0) # radiation pressure coefficient

    C11s = C11(ff, Ts, Ti, Te, phi, Pbs, L, l_SRC, M=M, lambda0=lambda0, single_pole_approx=single_pole_approx)
    C22s = C22(ff, Ts, Ti, Te, phi, Pbs, L, l_SRC, M=M, lambda0=lambda0, single_pole_approx=single_pole_approx)
    C12s = C12(ff, Ts, Ti, Te, phi, Pbs, L, M=M, lambda0=lambda0)
    C21s = C21(ff, Ts, Ti, Te, phi, Pbs, L, M=M, lambda0=lambda0)
    D1s  = D1L(ff, Ts, Ti, Te, phi, L, l_SRC, arm_loss, SRC_loss, post_SRM_loss, lambda0=lambda0, single_pole_approx=single_pole_approx)
    D2s  = D2L(ff, Ts, Ti, Te, phi, L, l_SRC, arm_loss, SRC_loss, post_SRM_loss, lambda0=lambda0, single_pole_approx=single_pole_approx)

    prefactor = h_SQL**2/(2 * kappas)
    numer = ( C11s * np.sin(zeta) + C21s * np.cos(zeta) )**2 + ( C12s * np.sin(zeta) + C22s * np.cos(zeta) )**2
    denom = ts**2 * np.abs( D1s * np.sin(zeta) + D2s * np.cos(zeta) )**2

    strain_psd = prefactor * numer / denom

    return strain_psd


def bnc_model(  ff,
                phi,
                zeta,
                E_LO,
                L,
                l_SRC,
                Ts,
                Ti,
                Te,
                Pbs,
                arm_loss,
                SRC_loss,
                post_SRM_loss,
                M=40.0,
                lambda0=1064e-9,
                single_pole_approx=True,
                ):
    '''Calculates the Buonanno and Chen DARM plant model.
    Based on Equation 2.26 of https://arxiv.org/pdf/gr-qc/0102012.pdf
    DOI: 10.1103/PhysRevD.64.042006

    The signal comes from the second half of the equation associated with h/h_SQL,
    where h_SQL = sqrt(S_h_SQL) = sqrt(8 hbar / (m Omega^2 L^2))
    and h is the GW strain incident on the IFO.

    I define DARM = Lx - Ly, and
    strain h = DARM / L

    Inputs:
    ff              = frequency vector
    phi             = SRC detuning in radians (nominal = pi/2)
    zeta            = homodyne angle in radians (nominal = 0)
    E_LO            = local oscillator amplitude 20 mA on DCPDs, sqrt(20 mA * 1e-3 / responsivity)
    L               = length of the interferometer arms (nominal = 3994.5 meters)
    l_SRC           = length of the SRC (nominal = 56.01 meters)
    Ts              = power transmission of SRM (nominal = 32%)
    Ti              = power transmission of ITM (nominal = 1.42%)
    Te              = power transmission of ETM (nominal = 4 ppm)
    Pbs             = power on the beamsplitter (nominal = input * PRG)
    arm_loss        = losses in the arms, round-trip (nominal = 100 ppm)
    SRC_loss        = losses in the SRC, all grouped into SRM reflectivity losses (nominal = 1%)
    post_SRM_loss   = losses from back of SRM to OMC DCPDs (nominal = T_OFI * R_OMs * T_OMC * ModeMatchOMC)
    M               = mass of the input and end test masses.  Default is 40 kg.
    lambda0         = wavelength of the laser.  Default is 1064 nm.
    single_pole_approx = Assume the single pole approximation. Default is False

    Outputs:
    darm_tf = DARM TF in [watts/meters], where watts are measured at the OMC DCPDs SUM, and meters are DARM meters

    '''
    ts = np.sqrt(Ts) # tau in paper

    omega0 = 2 * np.pi * scc.c / lambda0
    Omega = 2 * np.pi * ff
    delay = L / scc.c

    arm_pole = cavity_pole(L, Ti, Te)       # Hz
    Phis = Phi(ff, l_SRC, lambda0=lambda0)  # radians
    betas = beta(ff, L, Ti, Te, single_pole_approx=single_pole_approx)  # radians
    kappas = kappa(ff, Pbs, L, arm_pole, M=M, lambda0=lambda0)          # radiation pressure coefficient
    h_SQL = SQL(ff, L, M=M)                 # standard quantum limit ASD

    D1s  = D1L(ff, Ts, Ti, Te, phi, L, l_SRC, arm_loss, SRC_loss, post_SRM_loss, lambda0=lambda0, single_pole_approx=single_pole_approx)
    D2s  = D2L(ff, Ts, Ti, Te, phi, L, l_SRC, arm_loss, SRC_loss, post_SRM_loss, lambda0=lambda0, single_pole_approx=single_pole_approx)
    MMs  = MML(ff, Ts, Ti, Te, phi, Pbs, L, l_SRC, arm_loss, SRC_loss, post_SRM_loss, M=M, lambda0=lambda0, single_pole_approx=single_pole_approx)

    b_zeta = 1 / MMs *  (   np.sqrt(2 * kappas) * ts * np.exp( 1j * ( betas + Phis ) ) \
                            * ( D1s * np.sin(zeta) + D2s * np.cos(zeta) ) / h_SQL  \
                        ) # Equation 2.26 second line, units of [output electic field signal/strain]

    # conversion from b_zeta in quantum units to E_zeta in units of sqrt(watts)
    b_to_E_zeta = np.sqrt(scc.hbar * omega0)

    # exp_delay = np.exp(-1j * Omega * delay)

    darm_tf = np.sqrt(2) * b_zeta * b_to_E_zeta * E_LO / L # watts/meters = rtW/strain * rtW / meters, sqrt(2) for quadrature definition

    return darm_tf


### Ward Model Functions
def ward_model( ff,
                phi,
                zeta,
                E_LO,
                L,
                l_SRC,
                Ts,
                Ti,
                Te,
                Pbs,
                arm_loss,
                SRC_loss,
                post_SRM_loss,
                M=40.0,
                lambda0=1064e-9,
                ):
    ''' Calculates the Ward DARM plant model (DC readout, dual recycled Fabry Perot IFO)
    Based on Equation 3.83 from Ward's Thesis, doi:10.7907/20SX-2935
    From Ward's Thesis:
        It should be noted that this function is not exact—it depends (as usual)
        on the condition λgw >> L, but it also breaks down at the arm cavity free
        spectral range.  Moreover, this expression of the function ignores the
        effect of the finite signal recycling cavity length, which is a tiny correction

    Also included is the SRC_Phase, which is the phase accrued in the SRC by the audio sidebands.
    This is not explicitly in 3.83 but it's easy enough to include here.

    Inputs:
    ff              = frequency vector
    phi             = SRC detuning in radians (nominal = pi/2)
    zeta            = homodyne angle in radians (nominal = 0)
    E_LO            = local oscillator amplitude 20 mA on DCPDs, sqrt(20 mA * 1e-3 / responsivity)
    L               = length of the interferometer arms (nominal = 3994.5 meters)
    l_SRC           = length of the SRC (nominal = 32.01 meters)
    Ts              = power transmission of SRM (nominal = 32%)
    Ti              = power transmission of ITM (nominal = 1.42%)
    Te              = power transmission of ETM (nominal = 4 ppm)
    Pbs             = power on the beamsplitter    (nominal = input * PRG)
    arm_loss        = losses in the arms      (nominal = 38 ppm)
    SRC_loss        = losses in the SRC, all grouped into SRM reflectivity losses (nominal = 1%)
    post_SRM_loss   = losses from back of SRM to OMC DCPDs (nominal = T_OFI * R_OMs * T_OMC * ModeMatchOMC)
    M               = mass of the input and end test masses.  Default is 40 kg.
    lambda0         = wavelength of the laser.  Default is 1064 nm.

    Outputs:
    DARM TF in [watts/meters], where watts are measured at the OMC DCPDs SUM, and meters are DARM meters
    '''
    ww = 2*np.pi*ff
    w0 = 2*np.pi*scc.c/lambda0

    # tI = np.sqrt(Ti)
    # tE = np.sqrt(Te)
    tS = np.sqrt(Ts)

    rI = np.sqrt(1 - Ti)
    rE = np.sqrt(1 - Te - arm_loss)
    rS = np.sqrt(1 - Ts - SRC_loss)

    delay = L / scc.c

    FSR = scc.c/(2 * L)
    wa  = -FSR * np.log(rI * rE)

    SRC_Phase = -ww * l_SRC/scc.c
    beta = -np.arctan2(ww, wa)
    exp0 = np.exp(1j*(beta + SRC_Phase))

    kappa = 8 * Pbs * w0 / (M * L**2 * ww**2 * (wa**2 + ww**2))

    prefactor = (1 - post_SRM_loss) * E_LO / L * np.sqrt(2 * Pbs * w0**2 / (wa**2 + ww**2)) * np.exp(-2j * np.pi * ff * delay)
    numer = tS * exp0 * ((1 - rS*exp0**2) * np.cos(phi) * np.cos(zeta) - (1 + rS*exp0**2) * np.sin(phi) * np.sin(zeta))
    denom = 1 + rS**2 * exp0**4 - 2 * rS * exp0**2 * ( np.cos(2*phi) + kappa * np.sin(2*phi) / 2.0 )

    darm_tf = prefactor * numer / denom # * np.sqrt(2) mysterious factor of two :(
    
    return darm_tf

### Hall Model Functions
def homodyne_zero(  Ts,
                    phi,
                    zeta,
                    arm_pole):
    '''Computes the homodyne zero according to Equation 1 of https://arxiv.org/abs/1712.09719
    doi: 10.1088/1361-6382/ab368c

    Inputs:
    Ts = power transmission of the signal-recyling mirror
    phi = the microscopic one-way signal-recycling cavity phase in radians = 2 pi ls / lambda0
    zeta = homodyne angle between the local oscillator and signal quadratures
    arm_pole = cavity pole of the arms in Hz.

    Output:
    zero = homodyne zero of the DARM plant
    '''
    rs = np.sqrt(1 - Ts)
    numer = np.cos(phi + zeta) - rs * np.cos(phi - zeta)
    denom = np.cos(phi + zeta) + rs * np.cos(phi - zeta)
    zero = arm_pole * numer / denom
    return zero

def complex_DARM_pole(  Ts,
                        phi,
                        arm_pole):
    '''Computes the complex DARM pole according to Equation 2 of https://arxiv.org/abs/1712.09719
    doi: 10.1088/1361-6382/ab368c
    
    Inputs:
    Ts = power transmission of the signal-recyling mirror
    phi = the microscopic one-way signal-recycling cavity phase in radians = 2 pi ls / lambda0
    arm_pole = cavity pole of the arms in Hz.

    Output:
    pole = complex DARM coupled-cavity pole in Hz
    '''
    rs = np.sqrt(1 - Ts)
    numer = 1 - rs * np.exp(1j * 2 * phi)
    denom = 1 + rs * np.exp(1j * 2 * phi)
    pole = arm_pole * numer/denom
    return pole

def hall_gain(  arm_length,
                Ts,
                phi,
                zeta,
                arm_pole,
                Pbs,
                P_LO,
                lambda0=1064e-9):
    '''Calculates the gain of the Hall DARM plant model, based on Equation D.11 of https://thesis.library.caltech.edu/10031/
    doi: 10.7907/Z9PG1PQ9

    Inputs:
    arm_length = length of the cavity in meters
    Ts = power transmission of the signal-recyling mirror
    phi = the microscopic one-way signal-recycling cavity phase in radians = 2 pi ls / lambda0
    zeta = homodyne angle between the local oscillator and signal quadratures
    arm_pole = cavity pole of the arms in Hz
    Pbs = power on the beamsplitter in watts
    P_LO = power in the local oscillator that beats with the signal.  Should be around 20 mW for aLIGO.
    lambda0 = laser wavelength.  Default is 1064 nm.

    Output:
    gain = DARM optical gain in watts per meter (DARM def = Lx - Ly)
    '''
    w0 = 2 * np.pi * scc.c / lambda0
    wa = 2 * np.pi * arm_pole

    ts = np.sqrt(Ts)
    rs = np.sqrt(1 - Ts)

    prefactor = ts * np.sqrt(2 * Pbs * P_LO) * w0 / (wa * arm_length)
    numer = np.cos(phi + zeta) - rs * np.cos(phi - zeta)
    denom = 1 - 2 * rs * np.cos(2 * phi) + rs**2

    gain = prefactor * numer / denom

    return gain


def squared_spring_freq(arm_length,
                        Ts,
                        phi,
                        arm_pole,
                        Pbs,
                        M=40.0,
                        lambda0=1064e-9):
    '''Computes the squared spring frequency in Hz^2 according to Equation 5 of https://arxiv.org/abs/1712.09719
    doi: 10.1088/1361-6382/ab368c

    Inputs:
    arm_length = length of the cavity in meters
    Ts = power transmission of the signal-recyling mirror
    phi = the microscopic one-way signal-recycling cavity phase in radians = 2 pi ls / lambda0
    arm_pole = cavity pole of the arms in Hz
    Pbs = power on the beamsplitter in watts
    M = mass of the input and end test masses. Default is 40 kg.
    lambda0 = laser wavelength.  Default is 1064 nm.

    Output:
    xi2 = squared spring frequency in Hz^2
    '''
    rs = np.sqrt(1 - Ts)
    nu0 = scc.c / lambda0
    prefactor = nu0 * Pbs / (2 * np.pi**3 * arm_pole**2 * M * arm_length**2) # Hz^2
    numer = 2 * rs * np.sin(2 * phi)
    denom = 1 - 2 * rs * np.cos(2 * phi) + rs**2
    xi2 = prefactor * numer / denom
    return xi2

def hall_model( ff,
                arm_length,
                gain,
                zero,
                pole,
                xi2):
    '''Calculates the Hall DARM plant model, based on Equation 6 of https://arxiv.org/abs/1712.09719
    doi: 10.1088/1361-6382/ab368c

    Inputs:
    ff = frequency vector
    arm_length = length of the arm cavities in meters
    gain = DARM optical gain in [watts at the AS port/DARM meters]
    zero = homodyne zero
    pole = complex DARM pole
    xi2 = squared spring frequency in Hz^2.  Can be negative (spring) or positive (anti-spring).

    Outputs:
    DARM TF in [watts/meters], where watts are measured at the OMC DCPDs SUM, and meters are DARM meters
    '''
    delay = arm_length / scc.c    # arm time delay in seconds
    mag_p = np.abs(pole)                # Equation 3 of https://arxiv.org/abs/1712.09719
    Q_p = mag_p / (2 * np.real(pole))   # Equation 4 of https://arxiv.org/abs/1712.09719
    numer = np.exp(-2j * np.pi * ff * delay) * (1 + 1j * ff / zero)
    denom = 1 + 1j * ff/(mag_p * Q_p) - ff**2/mag_p**2 - xi2/ff**2

    darm_tf = gain * numer / denom

    return darm_tf

### Cal Group Model Functions
def cal_model(  ff, 
                arm_length,
                gain,
                DARM_pole,
                spring_frequency,
                spring_Q,
                spring_sign = -1):
    '''Calculates the Calibration group DARM plant model, based on Equation 9 of https://arxiv.org/pdf/1708.03023.pdf
    doi:10.1103/PhysRevD.96.102001
    (I assume kappa_C = 1, and C_R(f) = 1 for all f)

    Inputs:
    ff = frequency vector
    arm_length = length of the arm cavities in meters
    gain = DARM plant optical gain in watts at the AS port per DARM meters
    DARM_pole = differential arm coupled cavity pole in Hz
    spring_frequency = frequency of the DARM optical spring in Hz
    spring_Q = quality factor of the DARM optical spring
    spring_sign = pro or anti-spring.  Anti-spring is 1, spring is -1.

    Outputs:
    DARM TF in [watts/meters], where watts are measured at the OMC DCPDs SUM, and meters are DARM meters
    '''
    delay = arm_length / scc.c    # arm time delay in seconds
    fs = spring_sign * spring_frequency
    fs2 = spring_sign * spring_frequency**2

    numer = ff**2 * np.exp(-2j * np.pi * ff * delay)
    denom = (ff**2 + fs2 - 1j * ff * fs / spring_Q) * (1 + 1j * ff / DARM_pole)

    darm_tf = gain * numer / denom

    return darm_tf

### All models
def call_models(ff,
                phi,
                zeta,
                E_LO,
                L,
                l_SRC,
                Ts,
                Ti,
                Te,
                Pbs,
                arm_loss,
                SRC_loss,
                post_SRM_loss,
                M=40.0,
                lambda0=1064e-9,
                ):
    '''Calls each DARM TF model using the same parameters
    bnc_no_spa means bnc model, no simple pole approximation

    Inputs:
    ff = frequency vector in Hz
    phi = SRC detuning in radians (nominal = pi/2)
    zeta = homodyne angle in radians (nominal = 0)
    E_LO = local oscillator amplitude 20 mA on DCPDs, sqrt(20 mA * 1e-3 / responsivity)
    L = length of the interferometer arms (nominal = 3994.5 meters)
    l_SRC = length of the SRC (nominal = 32.01 meters)
    Ts = power transmission of SRM (nominal = 32%)
    Ti = power transmission of ITM (nominal = 1.42%)
    Te = power transmission of ETM (nominal = 4 ppm)
    Pbs = power on the beamsplitter    (nominal = input * PRG)
    arm_loss = losses in the arms      (nominal = 38 ppm)
    SRC_loss = losses in the SRC, all grouped into SRM reflectivity losses (nominal = 1%)
    post_SRM_loss = losses from back of SRM to OMC DCPDs (nominal = T_OFI * R_OMs * T_OMC * ModeMatchOMC)
    
    Outputs: tuple of three complex arrays of the DARM TFs.
    bnc_no_spa, bnc, ward, hall, calibration group = each DARM TF as a tuple
    '''
    P_LO = E_LO**2

    # BnC model, no simple pole approximation
    bnc_no_spa = bnc_model( ff,
                            phi,
                            zeta,
                            E_LO,
                            L,
                            l_SRC,
                            Ts,
                            Ti,
                            Te,
                            Pbs,
                            arm_loss,
                            SRC_loss,
                            post_SRM_loss,
                            M=M,
                            lambda0=lambda0,
                            single_pole_approx=True)

    # BnC model
    bnc = bnc_model(ff,
                    phi,
                    zeta,
                    E_LO,
                    L,
                    l_SRC,
                    Ts,
                    Ti,
                    Te,
                    Pbs,
                    arm_loss,
                    SRC_loss,
                    post_SRM_loss,
                    M=M,
                    lambda0=lambda0,
                    single_pole_approx=True)

    # Ward model
    ward = ward_model(  ff,
                        phi,
                        zeta,
                        E_LO,
                        L,
                        l_SRC,
                        Ts,
                        Ti,
                        Te,
                        Pbs,
                        arm_loss,
                        SRC_loss,
                        post_SRM_loss,
                        M=M,
                        lambda0=lambda0)

    # Hall model
    arm_pole = cavity_pole(L, Ti, Te)
    zero = homodyne_zero(Ts, phi, zeta, arm_pole)
    pole = complex_DARM_pole(Ts, phi, arm_pole)
    gain = hall_gain(L, Ts, phi, zeta, arm_pole, Pbs, P_LO)
    xi2 = squared_spring_freq(L, Ts, phi, arm_pole, Pbs, M=M, lambda0=lambda0)

    Qp = np.abs(pole)/(2 * np.real(pole))
    xi = np.sqrt(np.abs(xi2))

    hall = hall_model(  ff,
                        L,
                        gain,
                        zero,
                        pole, 
                        xi2)

    # Cal group model
    spring_Q = np.abs(pole)/xi * Qp # Equation 5.9 and 5.10 of Evan's thesis https://dcc.ligo.org/P1600295
    spring_sign = -1 * np.sign(xi2) # make the sign of the spring opposite 
                                    # what we would expect for a normal spring, 
                                    # thanks to a bad convention in the 
    cal = cal_model(ff,
                    L,
                    gain,
                    np.abs(pole),
                    np.sqrt(np.abs(xi2)),
                    spring_Q,
                    spring_sign=spring_sign
                    )

    return bnc_no_spa, bnc, ward, hall, cal


def interactive_plot(args):
    '''Creates an interactive plot with slides 
    for model comparison of the Ward, Hall, and calibration group models.
    Use with `-i` command line argument.
    '''

    ### IFO Parameters ###
    lambda0 = 1064e-9
    nu0 = scc.c/lambda0
    # w0 = 2 * np.pi * nu0

    L = 3994.5 # meters, length of arms
    l_SRC = 56.01 # meters, length of signal recycling cavity
    # FSR = scc.c / (2 * L) # Hz

    M = 40 # kg

    Ti = 0.0146  # ITM power trans
    Te = 4e-6   # ETM power trans
    Ts = 0.32   # SRM power trans

    phi = 89.5 * np.pi/180 # rads, SRM detuning
    zeta = 90.0 * np.pi/180 # rads, homodyne angle

    responsivity = scc.e * lambda0/(scc.h * scc.c) # A/W
    DCPD_QE = 0.98 # W/W
    P_LO = 20e-3 / responsivity / DCPD_QE # W = mA * A/mA * W/A
    E_LO = np.sqrt(P_LO)

    # Losses
    arm_loss = 100e-6   # round-trip
    SRC_loss = 0.03     # SRC losses, modeled as losses from reflection on SRM

    # numbers from https://git.ligo.org/haocun.yu/lho_squeezing/wikis/squeezer-budget
    T_OFI = 0.965
    R_OMs = 0.98
    T_OMC = 0.957
    ModeMatchOMC = 0.95

    post_SRM_loss = 1 - T_OFI * R_OMs * T_OMC * ModeMatchOMC

    # Preq = 36.5        # W, power requested
    Pin = 32.0         # W, power input
    PRG = 44.0          # W/W, power recycling gain
    Pbs = Pin * PRG   # W, Input Power watts * PRG = Power on beamsplitter in watts

    num_points = 500
    ff = np.logspace(np.log10(3), np.log10(5000), num_points)

    ### Initial Models ###
    _, bnc, _, _, _ = call_models(  ff,
                                    phi,
                                    zeta,
                                    E_LO,
                                    L,
                                    l_SRC,
                                    Ts,
                                    Ti,
                                    Te,
                                    Pbs,
                                    arm_loss,
                                    SRC_loss,
                                    post_SRM_loss,
                                    M=M,
                                    lambda0=lambda0)

    if args.no_simple_pole_approximation:
        plot_bnc = bnc_no_spa
        plot_bnc_label = 'BnC no SPA'
    else:
        plot_bnc = bnc
        plot_bnc_label = 'BnC'

    # if possible, read and plot LHO DARM trace in darmplant/data/LHO_O3_DARM_reference.txt
    filename = f'{data_dir}/2019_08_19_DARM_plant_mA_per_pm_Aug_Spots__No_SRCL_Offset.txt'
    try: #if os.path.exists(filename):
        data = np.loadtxt(filename, skiprows=1)
        ff_darm = data[:,0]
        tf_darm = data[:,1] * np.exp(1j * data[:,2])        # mA/pm
        tf_darm *= 1e-3 * 1e12 / responsivity / DCPD_QE * -1 # W/m = mA/pm * A/mA * pm/m * W/A * W/W, unknown sign flip

        plot_measured_tf = True
    except:
        plot_measured_tf = False
        print(f'Could not find {filename}, not plotting measured DARM TF')

    ### Slider plot ###
    if args.residuals:
        fig, ax = plt.subplots(2,2, sharex=True, figsize=(12,9))
        s1 = ax[0,0]
        s2 = ax[1,0]
        s3 = ax[0,1]
        s4 = ax[1,1]
    else:
        fig, ax = plt.subplots(2,1, sharex=True, figsize=(12,9))
        s1 = ax[0]
        s2 = ax[1]

    # saved traces
    l1s, = s1.loglog(ff, np.ones_like(ff), lw=4.5, alpha=0.0, ) #label='Saved BnC')
    l2s, = s2.semilogx(ff, np.zeros_like(ff), lw=4.5, alpha=0.0, ) # label='Saved Bnc')

    # l3s, = s1.loglog(ff, np.ones_like(ff), lw=3, alpha=0.0, ) #label='Saved Ward')
    # l4s, = s2.semilogx(ff, np.zeros_like(ff), lw=3, alpha=0.0, ) # label='Saved Ward')

    # l5s, = s1.loglog(ff, np.ones_like(ff), lw=3, ls='--', alpha=0.0, ) # label='Saved Hall')
    # l6s, = s2.semilogx(ff, np.zeros_like(ff), lw=3, ls='--', alpha=0.0, ) # label='Saved Hall')

    # l7s, = s1.loglog(ff, np.ones_like(ff), lw=3, ls=':', alpha=0.0, ) # label='Saved Cal')
    # l8s, = s2.semilogx(ff, np.zeros_like(ff), lw=3, ls=':', alpha=0.0, ) # label='Saved Cal')

    # main plots to be updated
    l1, = s1.loglog(ff, np.abs(plot_bnc), color=l1s.get_color(), label=plot_bnc_label)
    l2, = s2.semilogx(ff, 180/np.pi*np.angle(bnc), color=l2s.get_color(), label=plot_bnc_label)

    # l3, = s1.loglog(ff, np.abs(ward), color=l3s.get_color(), label='Ward')
    # l4, = s2.semilogx(ff, 180/np.pi*np.angle(ward), color=l4s.get_color(), label='Ward')

    # l5, = s1.loglog(ff, np.abs(hall), ls='--', color=l5s.get_color(), label='Hall')
    # l6, = s2.semilogx(ff, 180/np.pi*np.angle(hall), ls='--', color=l6s.get_color(), label='Hall')

    # l7, = s1.loglog(ff, np.abs(cal), ls=':', color=l7s.get_color(), label='Cal Group')
    # l8, = s2.semilogx(ff, 180/np.pi*np.angle(cal), ls=':', color=l8s.get_color(), label='Cal Group')

    if plot_measured_tf:
        s1.loglog(ff_darm, np.abs(tf_darm), marker='o', ms=6, label='Measured DARM TF, Aug 19 2019')
        s2.semilogx(ff_darm, 180/np.pi*np.angle(tf_darm), marker='o', ms=6, label='Measured DARM TF, Aug 19 2019')

    plt.suptitle('DARM Plant Model Comparison TFs with Parameter Sliders (SPA = single-pole approx)', y=1.00)

    s1.set_ylabel('Magnitude [W/m]')
    s1.set_xlim([ff[0], ff[-1]])
    s1.set_ylim([1e9, 3e11])
    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.3)
    s1.legend()

    s2.set_xlabel('Frequency [Hz]')
    s2.set_ylabel('Phase [degs]')
    s2.set_xlim([ff[0], ff[-1]])
    s2.set_ylim([-180, 180])
    s2.set_yticks(-45*np.arange(-4,5))
    s2.grid()
    s2.grid(which='minor', ls='--', alpha=0.3)

    if args.residuals:

        _, bnc_meas, _, _, _ = call_models( ff_darm,
                                            phi,
                                            zeta,
                                            E_LO,
                                            L,
                                            l_SRC,
                                            Ts,
                                            Ti,
                                            Te,
                                            Pbs,
                                            arm_loss,
                                            SRC_loss,
                                            post_SRM_loss,
                                            M=M,
                                            lambda0=lambda0)

        ratio_bnc_to_meas = tf_darm / bnc_meas
        # ratio_bnc_to_hall = hall / bnc
        # ratio_bnc_to_cal = cal / bnc
        # ratio_bnc_to_bnc_no_spa = bnc_no_spa / bnc

        l1r, = s3.semilogx(ff_darm, 100 * (np.abs(ratio_bnc_to_meas) - 1), color=l1.get_color(), marker='o', ms=6, label='meas/BnC')
        l2r, = s4.semilogx(ff_darm, 180/np.pi * np.angle(ratio_bnc_to_meas), color=l1.get_color(), marker='o', ms=6, label='meas/Bnc')

        # l3r, = s3.semilogx(ff, 100 * (np.abs(ratio_bnc_to_hall) - 1), ls='--', color=l5.get_color(), label='Hall/BnC')
        # l4r, = s4.semilogx(ff, 180/np.pi * np.angle(ratio_bnc_to_hall), ls='--', color=l5.get_color(), label='Hall/BnC')

        # l5r, = s3.semilogx(ff, 100 * (np.abs(ratio_bnc_to_cal) - 1), ls=':', color=l7.get_color(), label='Cal/BnC')
        # l6r, = s4.semilogx(ff, 180/np.pi * np.angle(ratio_bnc_to_cal), ls=':', color=l7.get_color(), label='Cal/BnC')

        # l7r, = s3.semilogx(ff, 100 * (np.abs(ratio_bnc_to_bnc_no_spa) - 1), ls='--', color=l9.get_color(), label='BnC no SPA/BnC')
        # l8r, = s4.semilogx(ff, 180/np.pi * np.angle(ratio_bnc_to_cal), ls='--', color=l9.get_color(), label='BnC no SPA/BnC')

        s3.set_ylabel('Mag Residual [%]')
        s3.set_xlim([ff[0], ff[-1]])
        s3.set_ylim([-30, 30])
        s3.grid()
        s3.grid(which='minor', ls='--', alpha=0.3)
        s3.legend()

        s4.set_xlabel('Frequency [Hz]')
        s4.set_ylabel('Phase Residual [degs]')
        s4.set_xlim([ff[0], ff[-1]])
        s4.set_ylim([-30, 30])
        s4.grid()
        s4.grid(which='minor', ls='--', alpha=0.3)

    plt.tight_layout()
    plt.subplots_adjust(bottom=0.4)

    axcolor = 'lightgoldenrodyellow'
    # spacing = 0.025
    axphi             = plt.axes([0.25, 0.315, 0.65, 0.01], facecolor=axcolor)
    axzeta            = plt.axes([0.25, 0.290, 0.65, 0.01], facecolor=axcolor)
    axE_LO            = plt.axes([0.25, 0.265, 0.65, 0.01], facecolor=axcolor)
    axTs              = plt.axes([0.25, 0.240, 0.65, 0.01], facecolor=axcolor)
    axTi              = plt.axes([0.25, 0.215, 0.65, 0.01], facecolor=axcolor)
    axTe              = plt.axes([0.25, 0.190, 0.65, 0.01], facecolor=axcolor)
    axPbs             = plt.axes([0.25, 0.165, 0.65, 0.01], facecolor=axcolor)
    axL               = plt.axes([0.25, 0.140, 0.65, 0.01], facecolor=axcolor)
    axl_SRC           = plt.axes([0.25, 0.115, 0.65, 0.01], facecolor=axcolor)
    axarm_loss        = plt.axes([0.25, 0.090, 0.65, 0.01], facecolor=axcolor)
    axSRC_loss        = plt.axes([0.25, 0.065, 0.65, 0.01], facecolor=axcolor)
    axpost_SRM_loss   = plt.axes([0.25, 0.040, 0.65, 0.01], facecolor=axcolor)
    # axpost_SRM_loss   = plt.axes([0.25, 0.04, 0.65, 0.01], facecolor=axcolor)

    sphi             = Slider(axphi,            r'Detuning $\phi$ [degs]',      85,  95, valinit=180/np.pi*phi)
    szeta            = Slider(axzeta,           r'Quadrature $\zeta$ [degs]', -180, 180, valinit=180/np.pi*zeta)
    sE_LO            = Slider(axE_LO,           r'DCPD (LO) Power $|E_\mathrm{LO}|^2$ [mW]', 10, 40, valinit=P_LO*1e3)
    saxTs            = Slider(axTs,             r'SRM Trans $t_s^2$ [%]', 0, 100, valinit=100*Ts)
    saxTi            = Slider(axTi,             r'ITM Trans $t_i^2$ [%]', 0, 10, valinit=100*Ti)
    saxTe            = Slider(axTe,             r'ETM Trans $t_e^2$ [ppm]', 0, 1000, valinit=1e6*Te)
    saxPbs           = Slider(axPbs,            r'Power on BS $P_{bs}$ [W]', 0, 3000, valinit=Pbs)
    saxL             = Slider(axL,              r'Arm Length $L$ [m]', 40, 40000, valinit=L)
    saxl_SRC         = Slider(axl_SRC,          r'SRC Length $l_\mathrm{SRC}$ [m]', 0, 1000, valinit=l_SRC)
    saxarm_loss      = Slider(axarm_loss,       r'Arm Loss $\mathcal{L}_\mathrm{rt}$ [ppm]', 0, 1000, valinit=arm_loss*1e6)
    saxSRC_loss      = Slider(axSRC_loss,       r'SRC Loss $\lambda_\mathrm{SR}$ [%]', 0, 100, valinit=SRC_loss*1e2)
    saxpost_SRM_loss = Slider(axpost_SRM_loss,  r'PD Loss $\lambda_\mathrm{PD}$ [%]', 0, 100, valinit=post_SRM_loss*1e2)
    # saxpost_SRM_loss = Slider(axpost_SRM_loss,  r'Post SRM Loss $\mathrm{Loss}_{OMC}$ [%]', 0, 100, valinit=post_SRM_loss0*1e2)

    def update(val):
        cur_phi = sphi.val * np.pi/180
        cur_zeta = szeta.val * np.pi/180
        cur_E_LO = np.sqrt(sE_LO.val * 1e-3)
        cur_Ts = saxTs.val * 1e-2
        cur_Ti = saxTi.val * 1e-2
        cur_Te = saxTe.val * 1e-6
        cur_Pbs = saxPbs.val
        cur_L = saxL.val
        cur_l_SRC = saxl_SRC.val
        cur_arm_loss  = saxarm_loss.val / 1e6
        cur_SRC_loss  = saxSRC_loss.val / 1e2
        cur_post_SRM_loss = saxpost_SRM_loss.val / 1e2

        new_params = [  cur_phi, 
                        cur_zeta, 
                        cur_E_LO, 
                        cur_L, 
                        cur_l_SRC, 
                        cur_Ts, 
                        cur_Ti, 
                        cur_Te, 
                        cur_Pbs, 
                        cur_arm_loss, 
                        cur_SRC_loss, 
                        cur_post_SRM_loss]

        _, new_bnc, _, _, _ = call_models(ff, *new_params)

        if args.no_simple_pole_approximation:
            new_plot_bnc = new_bnc_no_spa
        else:
            new_plot_bnc = new_bnc

        l1.set_ydata( np.abs(new_plot_bnc) )
        l2.set_ydata( 180/np.pi * np.angle(new_plot_bnc) )
        # l3.set_ydata( np.abs(new_ward) )
        # l4.set_ydata( 180/np.pi * np.angle(new_ward) )
        # l5.set_ydata( np.abs(new_hall) )
        # l6.set_ydata( 180/np.pi * np.angle(new_hall) )
        # l7.set_ydata( np.abs(new_cal) )
        # l8.set_ydata( 180/np.pi * np.angle(new_cal) )

        if args.residuals:
            _, new_bnc_meas, _, _, _ = call_models(ff_darm, *new_params)

            new_ratio_bnc_to_meas = tf_darm / new_bnc_meas 
            # new_ratio_bnc_to_hall = new_hall / new_bnc
            # new_ratio_bnc_to_cal = new_cal / new_bnc
            # new_ratio_bnc_to_bnc_no_spa = new_bnc_no_spa / new_bnc

            l1r.set_ydata( 100 * (np.abs(new_ratio_bnc_to_meas) - 1) )
            l2r.set_ydata( 180/np.pi * np.angle(new_ratio_bnc_to_meas ) )

            # l3r.set_ydata( 100 * (np.abs(new_ratio_bnc_to_hall) - 1) )
            # l4r.set_ydata( 180/np.pi * np.angle(new_ratio_bnc_to_hall ) )

            # l5r.set_ydata( 100 * (np.abs(new_ratio_bnc_to_cal) - 1) )
            # l6r.set_ydata( 180/np.pi * np.angle(new_ratio_bnc_to_cal ) )

            # l7r.set_ydata( 100 * (np.abs(new_ratio_bnc_to_bnc_no_spa) - 1) )
            # l8r.set_ydata( 180/np.pi * np.angle(new_ratio_bnc_to_bnc_no_spa ) )

        fig.canvas.draw_idle()

    sphi.on_changed(update)
    szeta.on_changed(update)
    sE_LO.on_changed(update)
    saxTs.on_changed(update)
    saxTi.on_changed(update)
    saxTe.on_changed(update)
    saxPbs.on_changed(update)
    saxL.on_changed(update)
    saxl_SRC.on_changed(update)
    saxarm_loss.on_changed(update)
    saxSRC_loss.on_changed(update)
    saxpost_SRM_loss.on_changed(update)
    # saxpost_SRM_loss.on_changed(update)

    resetax = plt.axes([0.8, 0.01, 0.1, 0.02])
    button_reset = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
    saveax = plt.axes([0.6, 0.01, 0.1, 0.02])
    button_save = Button(saveax, 'Save Trace', color=axcolor, hovercolor='0.975')

    def reset(event):
        sphi.reset()
        szeta.reset()
        sE_LO.reset()
        saxTs.reset()
        saxTi.reset()
        saxTe.reset()
        saxPbs.reset()
        saxL.reset()
        saxl_SRC.reset()
        saxarm_loss.reset()
        saxSRC_loss.reset()
        saxpost_SRM_loss.reset()
        # saxpost_SRM_loss.reset()
    button_reset.on_clicked(reset)

    def save(event):
        cur_data1 = l1.get_ydata()
        cur_data2 = l2.get_ydata()
        # cur_data3 = l3.get_ydata()
        # cur_data4 = l4.get_ydata()
        # cur_data5 = l5.get_ydata()
        # cur_data6 = l6.get_ydata()
        # cur_data7 = l7.get_ydata()
        # cur_data8 = l8.get_ydata()

        l1s.set_ydata( np.abs(cur_data1) )
        l2s.set_ydata( cur_data2 )
        # l3s.set_ydata( np.abs(cur_data3) )
        # l4s.set_ydata( cur_data4 )
        # l5s.set_ydata( np.abs(cur_data5) )
        # l6s.set_ydata( cur_data6 )
        # l7s.set_ydata( np.abs(cur_data7) )
        # l8s.set_ydata( cur_data8 )

        l1s.set_alpha(0.5)
        l2s.set_alpha(0.5)
        # l3s.set_alpha(0.5)
        # l4s.set_alpha(0.5)
        # l5s.set_alpha(0.5)
        # l6s.set_alpha(0.5)
        # l7s.set_alpha(0.5)
        # l8s.set_alpha(0.5)

        fig.canvas.draw_idle()
    button_save.on_clicked(save)

    # rax = plt.axes([0.025, 0.5, 0.15, 0.15], facecolor=axcolor)
    # radio = RadioButtons(rax, ('red', 'blue', 'green'), active=0)
    #
    #
    # def colorfunc(label):
    #     l.set_color(label)
    #     fig.canvas.draw_idle()
    # radio.on_clicked(colorfunc)

    plt.show()

def interactive_quantum_asd_plot(args):
    '''Creates an interactive quantum asd plot with sliders
    Use with `-i -a` command line arguments.
    '''

    ### IFO Parameters ###
    lambda0 = 1064e-9
    nu0 = scc.c/lambda0
    # w0 = 2 * np.pi * nu0

    L = 3994.5 # meters, length of arms
    l_SRC = 56.01 # meters, length of signal recycling cavity
    # FSR = scc.c / (2 * L) # Hz

    M = 40 # kg

    Ti = 0.014  # ITM power trans
    Te = 4e-6   # ETM power trans
    Ts = 0.32   # SRM power trans

    phi = 89.5 * np.pi/180 # rads, SRM detuning
    zeta = 90.0 * np.pi/180 # rads, homodyne angle

    responsivity = scc.e * lambda0/(scc.h * scc.c) # A/W
    DCPD_QE = 0.98 # W/W
    P_LO = 20e-3 / responsivity / DCPD_QE # W = mA * A/mA * W/A
    E_LO = np.sqrt(P_LO)

    # Losses
    # numbers from https://git.ligo.org/haocun.yu/lho_squeezing/wikis/squeezer-budget
    # T_OFI = 0.965
    # R_OMs = 0.98
    # T_OMC = 0.957
    # ModeMatchOMC = 0.95

    # Preq = 36.5        # W, power requested
    Pin = 32.0         # W, power input
    PRG = 44.0          # W/W, power recycling gain
    Pbs = Pin * PRG   # W, Input Power watts * PRG = Power on beamsplitter in watts

    num_points = 300
    ff = np.logspace(0, 4, num_points)

    ### Initial Models ###
    sql_asd = SQL(ff, L, M=M)
    bnc_psd = quantum_psd(  ff,
                            phi,
                            zeta,
                            E_LO,
                            L,
                            l_SRC,
                            Ts,
                            Ti,
                            Te,
                            Pbs,
                            0,
                            0,
                            0,
                            M=M,
                            lambda0=lambda0,
                            single_pole_approx=True)
    bnc_asd = np.sqrt(bnc_psd)

    ### Slider plot ###
    # if args.residuals:
    #     fig, ax = plt.subplots(2,2, sharex=True, figsize=(12,9))
    #     s1 = ax[0,0]
    #     s2 = ax[1,0]
    #     s3 = ax[0,1]
    #     s4 = ax[1,1]
    # else:
    fig, s1 = plt.subplots(1, figsize=(12,9))

    # saved traces
    l1s, = s1.loglog(ff, 1e-24*np.ones_like(ff), ls='-',  alpha=0.0, color='C3',) #label='Saved Ward')
    l2s, = s1.loglog(ff, 1e-24*np.ones_like(ff), ls='--', alpha=0.0, ) #label='Saved Ward')

    # main plots to be updated
    l1, = s1.loglog(ff, bnc_asd, ls='-',  color=l1s.get_color(), label='BnC')
    l2, = s1.loglog(ff, sql_asd, ls='--', color=l2s.get_color(), label='SQL')

    # if possible, read and plot LHO DARM trace in darmplant/data/LHO_O3_DARM_reference.txt
    filename = f'{data_dir}/LHO_O3_DARM_reference.txt'
    if os.path.exists(filename):
        data = np.loadtxt(filename)
        ff_darm = data[:,0]
        LHO_DARM = data[:,1]        # m/rtHz
        LHO_strain = LHO_DARM / L   # strain/rtHz

        s1.loglog(ff_darm, LHO_strain, ls='-', label='O3 LHO', zorder=0)
    else:
        print(f'Could not find {filename}, not plotting')

    s1.set_title('Quantum-limited ASDs with Parameter Sliders')
    s1.set_ylabel('ASD ' + r'[strain/rtHz]')
    s1.set_xlabel('Frequency [Hz]')

    s1.set_xlim([ff[0], ff[-1]])
    s1.set_ylim([1e-25, 1e-20])
    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.3)
    s1.legend()

    

    # if args.residuals:
    #     ratio_bnc_to_ward = ward / bnc
    #     ratio_bnc_to_hall = hall / ward
    #     ratio_bnc_to_cal = cal / ward

    #     l1r, = s3.semilogx(ff, 100 * (np.abs(ratio_bnc_to_ward) - 1), color=l3.get_color(), label='Ward/BnC')
    #     l2r, = s4.semilogx(ff, 180/np.pi * np.angle(ratio_bnc_to_ward), color=l3.get_color(), label='Ward/BnC')

    #     l3r, = s3.semilogx(ff, 100 * (np.abs(ratio_bnc_to_hall) - 1), ls='--', color=l5.get_color(), label='Hall/BnC')
    #     l4r, = s4.semilogx(ff, 180/np.pi * np.angle(ratio_bnc_to_hall), ls='--', color=l5.get_color(), label='Hall/BnC')

    #     l5r, = s3.semilogx(ff, 100 * (np.abs(ratio_bnc_to_cal) - 1), ls=':', color=l7.get_color(), label='Cal/BnC')
    #     l6r, = s4.semilogx(ff, 180/np.pi * np.angle(ratio_bnc_to_cal), ls=':', color=l7.get_color(), label='Cal/BnC')

    #     s3.set_ylabel('Mag Residual [%]')
    #     s3.set_xlim([ff[0], ff[-1]])
    #     s3.set_ylim([-30, 30])
    #     s3.grid()
    #     s3.grid(which='minor', ls='--', alpha=0.3)
    #     s3.legend()

    #     s4.set_xlabel('Frequency [Hz]')
    #     s4.set_ylabel('Phase Residual [degs]')
    #     s4.set_xlim([ff[0], ff[-1]])
    #     s4.set_ylim([-30, 30])
    #     s4.grid()
    #     s4.grid(which='minor', ls='--', alpha=0.3)

    plt.tight_layout()
    plt.subplots_adjust(bottom=0.32)

    axcolor = 'lightgoldenrodyellow'
    # spacing = 0.025
    axphi             = plt.axes([0.25, 0.240, 0.65, 0.01], facecolor=axcolor)
    axzeta            = plt.axes([0.25, 0.215, 0.65, 0.01], facecolor=axcolor)
    axE_LO            = plt.axes([0.25, 0.190, 0.65, 0.01], facecolor=axcolor)
    axTs              = plt.axes([0.25, 0.165, 0.65, 0.01], facecolor=axcolor)
    axTi              = plt.axes([0.25, 0.140, 0.65, 0.01], facecolor=axcolor)
    axTe              = plt.axes([0.25, 0.115, 0.65, 0.01], facecolor=axcolor)
    axPbs             = plt.axes([0.25, 0.090, 0.65, 0.01], facecolor=axcolor)
    axL               = plt.axes([0.25, 0.065, 0.65, 0.01], facecolor=axcolor)
    axl_SRC           = plt.axes([0.25, 0.040, 0.65, 0.01], facecolor=axcolor)
    # axpost_SRM_loss   = plt.axes([0.25, 0.04, 0.65, 0.01], facecolor=axcolor)

    sphi             = Slider(axphi,            r'Detuning $\phi$ [degs]',    -180, 180, valinit=180/np.pi*phi)
    szeta            = Slider(axzeta,           r'Quadrature $\zeta$ [degs]', -180, 180, valinit=180/np.pi*zeta)
    sE_LO            = Slider(axE_LO,           r'DCPD (LO) Power $|E_\mathrm{LO}|^2$ [mW]', 10, 40, valinit=P_LO*1e3)
    saxTs            = Slider(axTs,             r'SRM Trans $t_s^2$ [%]', 0, 100, valinit=100*Ts)
    saxTi            = Slider(axTi,             r'ITM Trans $t_i^2$ [%]', 0, 10, valinit=100*Ti)
    saxTe            = Slider(axTe,             r'ETM Trans $t_e^2$ [ppm]', 0, 1000, valinit=1e6*Te)
    saxPbs           = Slider(axPbs,            r'Power on BS $P_{bs}$ [W]', 0, 10000, valinit=Pbs)
    saxL             = Slider(axL,              r'Arm Length $L$ [m]', 40, 40000, valinit=L)
    saxl_SRC         = Slider(axl_SRC,          r'SRC Length $l_\mathrm{SRC}$ [m]', 0, 1000, valinit=l_SRC)
    # saxpost_SRM_loss = Slider(axpost_SRM_loss,  r'Post SRM Loss $\mathrm{Loss}_{OMC}$ [%]', 0, 100, valinit=post_SRM_loss0*1e2)

    def update(val):
        cur_phi = sphi.val * np.pi/180
        cur_zeta = szeta.val * np.pi/180
        cur_E_LO = np.sqrt(sE_LO.val * 1e-3)
        cur_Ts = saxTs.val * 1e-2
        cur_Ti = saxTi.val * 1e-2
        cur_Te = saxTe.val * 1e-6
        cur_Pbs = saxPbs.val
        cur_L = saxL.val
        cur_l_SRC = saxl_SRC.val
        # post_SRM_loss = saxpost_SRM_loss.val * 1e-2

        new_params = [  cur_phi, 
                        cur_zeta, 
                        cur_E_LO, 
                        cur_L, 
                        cur_l_SRC, 
                        cur_Ts, 
                        cur_Ti, 
                        cur_Te, 
                        cur_Pbs, 
                        0, 
                        0, 
                        0,
                        M,
                        lambda0,
                        True]

        new_sql_asd = SQL(ff, cur_L, M=M)
        new_bnc_psd = quantum_psd(ff, *new_params)
        new_bnc_asd = np.sqrt(new_bnc_psd)

        l1.set_ydata( new_bnc_asd )
        l2.set_ydata( new_sql_asd )

        # if args.residuals:
        #     new_ratio_bnc_to_meas = new_ward / new_bnc
        #     new_ratio_bnc_to_hall = new_hall / new_bnc
        #     new_ratio_bnc_to_cal = new_cal / new_bnc

        #     l1r.set_ydata( 100 * (np.abs(new_ratio_bnc_to_meas) - 1) )
        #     l2r.set_ydata( 180/np.pi * np.angle(new_ratio_bnc_to_meas ) )

        #     l3r.set_ydata( 100 * (np.abs(new_ratio_bnc_to_hall) - 1) )
        #     l4r.set_ydata( 180/np.pi * np.angle(new_ratio_bnc_to_hall ) )

        #     l5r.set_ydata( 100 * (np.abs(new_ratio_bnc_to_cal) - 1) )
        #     l6r.set_ydata( 180/np.pi * np.angle(new_ratio_bnc_to_cal ) )

        fig.canvas.draw_idle()

    sphi.on_changed(update)
    szeta.on_changed(update)
    sE_LO.on_changed(update)
    saxTs.on_changed(update)
    saxTi.on_changed(update)
    saxTe.on_changed(update)
    saxPbs.on_changed(update)
    saxL.on_changed(update)
    saxl_SRC.on_changed(update)
    # saxpost_SRM_loss.on_changed(update)

    resetax = plt.axes([0.8, 0.01, 0.1, 0.02])
    button_reset = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
    saveax = plt.axes([0.6, 0.01, 0.1, 0.02])
    button_save = Button(saveax, 'Save Trace', color=axcolor, hovercolor='0.975')

    def reset(event):
        sphi.reset()
        szeta.reset()
        sE_LO.reset()
        saxTs.reset()
        saxTi.reset()
        saxTe.reset()
        saxPbs.reset()
        saxL.reset()
        saxl_SRC.reset()
        # saxpost_SRM_loss.reset()
    button_reset.on_clicked(reset)

    def save(event):
        cur_data1 = l1.get_ydata()
        cur_data2 = l2.get_ydata()

        l1s.set_ydata( cur_data1 )
        l2s.set_ydata( cur_data2 )

        l1s.set_alpha(0.5)
        l2s.set_alpha(0.5)

        fig.canvas.draw_idle()
    button_save.on_clicked(save)

    # rax = plt.axes([0.025, 0.5, 0.15, 0.15], facecolor=axcolor)
    # radio = RadioButtons(rax, ('red', 'blue', 'green'), active=0)
    #
    #
    # def colorfunc(label):
    #     l.set_color(label)
    #     fig.canvas.draw_idle()
    # radio.on_clicked(colorfunc)

    plt.show()

def main():
    '''main function of this script.  
    Makes main DARM model plot for thesis
    '''
    mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 18,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

    ### IFO Parameters ###
    lambda0 = 1064e-9
    nu0 = scc.c/lambda0
    # w0 = 2 * np.pi * nu0

    L = 3994.5 # meters, length of arms
    l_SRC = 56.01 # meters, length of signal recycling cavity
    # FSR = scc.c / (2 * L) # Hz

    M = 40 # kg

    Ti = 0.0146  # ITM power trans
    Te = 4e-6   # ETM power trans
    Ts = 0.32   # SRM power trans

    phi = 89.37 * np.pi/180 # rads, SRM detuning
    zeta = 90.0 * np.pi/180 # rads, homodyne angle

    responsivity = scc.e * lambda0/(scc.h * scc.c) # A/W
    DCPD_QE = 0.98 # W/W
    P_LO = 20e-3 / responsivity / DCPD_QE # W = mA * A/mA * W/A
    E_LO = np.sqrt(P_LO)

    # Losses
    arm_loss = 100e-6   # round-trip
    SRC_loss = 0.068     # SRC losses, modeled as losses from reflection on SRM

    # numbers from https://git.ligo.org/haocun.yu/lho_squeezing/wikis/squeezer-budget
    T_OFI = 0.965
    R_OMs = 0.98
    T_OMC = 0.957
    ModeMatchOMC = 0.95

    post_SRM_loss = 0.25 #1 - T_OFI * R_OMs * T_OMC * ModeMatchOMC

    # Preq = 36.5        # W, power requested
    Pin = 32.0         # W, power input
    PRG = 44.0          # W/W, power recycling gain
    Pbs = Pin * PRG   # W, Input Power watts * PRG = Power on beamsplitter in watts

    num_points = 500
    ff = np.logspace(np.log10(3), np.log10(5000), num_points)

    nom_dict = {}
    nom_dict['phi'] = phi
    nom_dict['zeta'] = zeta
    nom_dict['E_LO'] = E_LO
    nom_dict['L'] = L
    nom_dict['l_SRC'] = l_SRC
    nom_dict['Ts'] = Ts
    nom_dict['Ti'] = Ti
    nom_dict['Te'] = Te
    nom_dict['Pbs'] = Pbs
    nom_dict['arm_loss'] = arm_loss
    nom_dict['SRC_loss'] = SRC_loss
    nom_dict['post_SRM_loss'] = post_SRM_loss
    nom_dict['M'] = M
    nom_dict['lambda0'] = lambda0

    ### Initial Models ###
    _, bnc, _, _, _ = call_models(ff, **nom_dict)

    # if possible, read and plot LHO DARM trace in darmplant/data/LHO_O3_DARM_reference.txt
    filename = f'{data_dir}/2019_08_19_DARM_plant_mA_per_pm_Aug_Spots__No_SRCL_Offset.txt'
    try: #if os.path.exists(filename):
        data = np.loadtxt(filename, skiprows=1)
        ff_darm = data[:,0]
        tf_darm = data[:,1] * np.exp(1j * data[:,2])            # mA/pm
        tf_darm *= 1e-3 * 1e12 / responsivity / DCPD_QE * -1    # W/m = mA/pm * A/mA * pm/m * W/A * W/W, unknown sign flip

        plot_measured_tf = True
    except:
        plot_measured_tf = False
        print(f'Could not find {filename}, not plotting measured DARM TF')

    _, bnc_meas, _, _, _ = call_models( ff_darm, **nom_dict)
    ratio_bnc_to_meas = tf_darm / bnc_meas



    #####   Figures   #####
    fig, ax = plt.subplots(2,2, sharex=True, figsize=(12,9))
    s1 = ax[0,0]
    s2 = ax[1,0]
    s3 = ax[0,1]
    s4 = ax[1,1]

    s1.loglog(ff, np.abs(bnc), label='BnC with loss')
    s2.semilogx(ff, np.angle(bnc, deg=True), label='BnC with loss')

    s1.loglog(ff_darm, np.abs(tf_darm), marker='o', ms=6, label='Measured DARM TF, Aug 19 2019')
    s2.semilogx(ff_darm, 180/np.pi*np.angle(tf_darm), marker='o', ms=6, label='Measured DARM TF, Aug 19 2019')

    s3.semilogx(ff_darm, 100 * (np.abs(ratio_bnc_to_meas) - 1), marker='o', ms=6, label='meas/BnC')
    s4.semilogx(ff_darm, 180/np.pi * np.angle(ratio_bnc_to_meas), marker='o', ms=6, label='meas/Bnc')

    s1.set_xlim([ff[0], ff[-1]])
    s2.set_xlim([ff[0], ff[-1]])
    s3.set_xlim([ff[0], ff[-1]])
    s4.set_xlim([ff[0], ff[-1]])

    s1.set_ylim([1e9, 3e11])
    s2.set_ylim([-180, 180])
    s3.set_ylim([-50, 50])
    s4.set_ylim([-30, 30])

    s2.set_yticks([-180, -90, 0, 90, 180])

    # s1.set_title('DARM Plant TF Model comparison ' + r'$\phi_\mathrm{SRC} = $' + f'{phi*180/np.pi:.1f} degs')
    s1.set_ylabel('DARM Plant Mag '+r'$|P_{as}/L_-|$ [W/m]')
    s2.set_ylabel('Phase [degs]')
    s3.set_ylabel('Mag Residual [\%]')
    s4.set_ylabel('Phase Residual [degs]')

    s2.set_xlabel('Frequency [Hz]')
    s4.set_xlabel('Frequency [Hz]')

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.5)
    s2.grid()
    s2.grid(which='minor', ls='--', alpha=0.5)
    s3.grid()
    s3.grid(which='minor', ls='--', alpha=0.5)
    s4.grid()
    s4.grid(which='minor', ls='--', alpha=0.5)

    s1.legend()
    s3.legend()

    plot_name = f'bnc_with_loss_darm_model_fit_with_residuals.pdf'
    full_plot_name = f'{fig_dir}/{plot_name}'
    print(f'Plotting PDF: {full_plot_name}')
    plt.tight_layout()
    plt.savefig(full_plot_name)

    return #full_plot_name
    
if __name__ == "__main__":

    args = parse_args()

    if args.interactive and not args.asd:
        print('Creating interactive DARM plant plot:')
        interactive_plot(args)
    elif args.interactive and args.asd:
        print('Creating interactive quantum ASD plot:')
        interactive_quantum_asd_plot(args)
    else:
        main()
