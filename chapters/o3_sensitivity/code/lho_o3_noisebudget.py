'''
PlotLhoNb_logbinned.py

Copied from PlotLhoNb.py (authored by Georgia Mansell and Aaron Buikema)
Logbins the noisebudget to make it a reasonable size.

Craig Cahillane
Oct 1, 2019
'''

import os
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
plt.ion()
from scipy.io import loadmat

import gwinc

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

# Load noisebudget from .mat
NB_LHO_contents = loadmat(f'{data_dir}/LHO_NB_data_o3a.mat',squeeze_me=True, struct_as_record=False)
nb = NB_LHO_contents['NB']
#nb_darm_betterTime = loadmat('LHO_NB_data_darmOnly.mat',squeeze_me=True, struct_as_record=False)

# Load + set up O1/O2 spectra
files = {
'o1': f'{data_dir}/2015_10_24_15_09_43_H1_O1_strain.txt',
'o2': f'{data_dir}/2017-06-10_DCH_C02_H1_O2_Sensitivity_strain_asd.txt'
}
# O2 data is with subtraction

spectra = {}
for f in files:
    # Remember, this in strain, not m
    spectra[f] = np.loadtxt(files[f])

mpl.rcParams.update({   'figure.figsize': (12, 9),
                        'text.usetex': True,
                        'font.family': 'serif',
                        # 'font.serif': 'Georgia',
                        # 'mathtext.fontset': 'cm',
                        'lines.linewidth': 2.5,
                        'lines.markersize': 3,
                        'font.size': 22,
                        'xtick.labelsize': 'large',
                        'ytick.labelsize': 'large',
                        'legend.fancybox': True,
                        'legend.fontsize': 18,
                        'legend.framealpha': 0.9,
                        'legend.handletextpad': 0.5,
                        'legend.labelspacing': 0.2,
                        'legend.loc': 'best',
                        'legend.columnspacing': 2,
                        'savefig.dpi': 80,
                        'pdf.compression': 9})

#####   Functions   #####
# Logbinning
def resampling_matrix_nonuniform(lorig, lresam, extrap = False):
    '''
    Logbinning stolen from some astro people: https://pypi.org/project/PySTARLIGHT/

    Compute resampling matrix R_o2r, useful to convert a spectrum sampled at
    wavelengths lorig to a new grid lresamp. Here, there is no necessity to have constant gris as on :py:func:`ReSamplingMatrix`.
    Input arrays lorig and lresamp are the bin centres of the original and final lambda-grids.
    ResampMat is a Nlresamp x Nlorig matrix, which applied to a vector F_o (with Nlorig entries) returns
    a Nlresamp elements long vector F_r (the resampled spectrum):

        [[ResampMat]] [F_o] = [F_r]

    Warning! lorig and lresam MUST be on ascending order!


    Parameters
    ----------
    lorig : array_like
            Original spectrum lambda array.

    lresam : array_like
             Spectrum lambda array in which the spectrum should be sampled.

    extrap : boolean, optional
           Extrapolate values, i.e., values for lresam < lorig[0]  are set to match lorig[0] and
                                     values for lresam > lorig[-1] are set to match lorig[-1].


    Returns
    -------
    ResampMat : array_like
                Resample matrix.

    Examples
    --------
    >>> lorig = np.linspace(3400, 8900, 9000) * 1.001
    >>> lresam = np.linspace(3400, 8900, 5000)
    >>> forig = np.random.normal(size=len(lorig))**2
    >>> matrix = slut.resampling_matrix_nonuniform(lorig, lresam)
    >>> fresam = np.dot(matrix, forig)
    >>> print np.trapz(forig, lorig), np.trapz(fresam, lresam)
    '''

    # Init ResampMatrix
    matrix = np.zeros((len(lresam), len(lorig)))

    # Define lambda ranges (low, upp) for original and resampled.
    lo_low = np.zeros(len(lorig))
    lo_low[1:] = (lorig[1:] + lorig[:-1])/2
    lo_low[0] = lorig[0] - (lorig[1] - lorig[0])/2

    lo_upp = np.zeros(len(lorig))
    lo_upp[:-1] = lo_low[1:]
    lo_upp[-1] = lorig[-1] + (lorig[-1] - lorig[-2])/2

    lr_low = np.zeros(len(lresam))
    lr_low[1:] = (lresam[1:] + lresam[:-1])/2
    lr_low[0] = lresam[0] - (lresam[1] - lresam[0])/2

    lr_upp = np.zeros(len(lresam))
    lr_upp[:-1] = lr_low[1:]
    lr_upp[-1] = lresam[-1] + (lresam[-1] - lresam[-2])/2


    # Iterate over resampled lresam vector
    for i_r in range(len(lresam)):

        # Find in which bins lresam bin within lorig bin
        bins_resam = np.where( (lr_low[i_r] < lo_upp) & (lr_upp[i_r] > lo_low) )[0]

        # On these bins, eval fraction of resamled bin is within original bin.
        for i_o in bins_resam:

            aux = 0

            d_lr = lr_upp[i_r] - lr_low[i_r]
            d_lo = lo_upp[i_o] - lo_low[i_o]
            d_ir = lo_upp[i_o] - lr_low[i_r]  # common section on the right
            d_il = lr_upp[i_r] - lo_low[i_o]  # common section on the left

            # Case 1: resampling window is smaller than or equal to the original window.
            # This is where the bug was: if an original bin is all inside the resampled bin, then
            # all flux should go into it, not then d_lr/d_lo fraction. --Natalia@IoA - 21/12/2012
            if (lr_low[i_r] > lo_low[i_o]) & (lr_upp[i_r] < lo_upp[i_o]):
                aux += 1.

            # Case 2: resampling window is larger than the original window.
            if (lr_low[i_r] < lo_low[i_o]) & (lr_upp[i_r] > lo_upp[i_o]):
                aux += d_lo / d_lr

            # Case 3: resampling window is on the right of the original window.
            if (lr_low[i_r] > lo_low[i_o]) & (lr_upp[i_r] > lo_upp[i_o]):
                aux += d_ir / d_lr

            # Case 4: resampling window is on the left of the original window.
            if (lr_low[i_r] < lo_low[i_o]) & (lr_upp[i_r] < lo_upp[i_o]):
                aux += d_il / d_lr

            matrix[i_r, i_o] += aux


    # Fix matrix to be exactly = 1 ==> TO THINK
    #print np.sum(matrix), np.sum(lo_upp - lo_low), (lr_upp - lr_low).shape


    # Fix extremes: extrapolate if needed
    if (extrap):

        bins_extrapl = np.where( (lr_low < lo_low[0])  )[0]
        bins_extrapr = np.where( (lr_upp > lo_upp[-1]) )[0]

        if (len(bins_extrapl) > 0) & (len(bins_extrapr) > 0):
            io_extrapl = np.where( (lo_low >= lr_low[bins_extrapl[0]])  )[0][0]
            io_extrapr = np.where( (lo_upp <= lr_upp[bins_extrapr[0]])  )[0][-1]

            matrix[bins_extrapl, io_extrapl] = 1.
            matrix[bins_extrapr, io_extrapr] = 1.


    return matrix

def logbin_ASD(log_ff, linear_ff, linear_ASD):
    '''Logbins an ASD given some log spaced frequency vector.
    Inputs:
    log_ff is the final vector we want the ASD to be spaced at
    linear_ff is the original frequency vector
    linear_ASD is the ASD
    '''
    matrix = resampling_matrix_nonuniform(linear_ff, log_ff)
    log_ASD = np.dot(matrix, linear_ASD)
    return log_ASD

def linear_log_ASD(log_ff, linear_ff, linear_ASD):
    '''
    Creates a linear- and log-binned ASD vector from overlapping linear and log frequency vectors,
    such that the coarsest frequency vector is used.
    This avoids the problem of logbinning where the low frequency points have too
    much resolution, i.e. the FFT binwidth > log_ff[1] - log_ff[0].

    Inputs:
    linear_ff  = linear frequency vector. Will be used for low frequency points.
    log_ff     = log frequency vector.  Will be used for high frequency points.
    linear_ASD = linear ASD. Should be the same length as linear_ff, usual output of scipy.signal.welch().

    Outputs:
    fff = stitched frequency vector of linear and log points
    linlog_ASD = stitched ASD of linear and log points
    '''
    df = linear_ff[1] - linear_ff[0]
    dfflog = np.diff(log_ff)
    log_index = np.argwhere(dfflog > df)[0][0] # first point where fflog has less resolution than the normal freq vector
    cutoff = log_ff[log_index]                 # cutoff frequency
    high_fff = log_ff[log_index+1:]

    linear_index = np.argwhere(cutoff < linear_ff)[0][0] # find where the cutoff frequency is first less than the linear frequency vector
    low_fff = linear_ff[:linear_index]
    low_ASD = linear_ASD[:linear_index]

    fff = np.concatenate((low_fff, high_fff)) # make the full frequency vector

    matrix = resampling_matrix_nonuniform(linear_ff, high_fff)
    high_ASD = np.dot(matrix, linear_ASD) # get HF part of spectrum

    linlog_ASD = np.concatenate((low_ASD, high_ASD))

    return fff, linlog_ASD

#####   IFO Params   #####
arm_length = 3995 #dat's all u need to know

ff = nb.freq
ff_O1 = spectra['o1'][:,0]
ff_O2 = spectra['o2'][:,0]

num_points = 1500
fflog = np.logspace(np.log10(ff[0]), np.log10(ff[-1]), num_points)
fflog_O1 = np.logspace(np.log10(ff_O1[0]), np.log10(ff_O1[-1]), num_points)
fflog_O2 = np.logspace(np.log10(ff_O2[0]), np.log10(ff_O2[-1]), num_points)

# logbin_matrix = ReSamplingMatrixNonUniform(ff, fflog)
# logbin_matrix_O1 = ReSamplingMatrixNonUniform(ff_O1, fflog_O1)
# logbin_matrix_O2 = ReSamplingMatrixNonUniform(ff_O2, fflog_O2)

plotDict = {}
labels = np.array([
'Measured noise (O3)',
'Sum of known noises',
'Quantum',
'Thermal',
'Seismic',
'Newtonian',
'Residual gas',
'Auxiliary length control',
'Alignment control',
'Beam jitter',
'Scattered light',
'Laser intensity',
'Laser frequency',
'Photodetector dark',
'Output mode cleaner length',
'Penultimate-mass actuator',
])

ASDs = np.array([
nb.DARM_reference,
nb.total,
nb.grouped_noises.quantum.total,
nb.grouped_noises.thermal.total,
nb.noises.seismic,
nb.noises.newtonian,
nb.noises.residual_gas,
nb.grouped_noises.lsc.total,
nb.grouped_noises.asc.total,
nb.grouped_noises.jitter.total,
nb.noises.scatter,
nb.noises.intensity,
nb.noises.frequency,
nb.noises.dark,
nb.noises.OMCLength,
nb.noises.DAC,
])

styles = np.array([
'C0-',
'k-',
'C4-',
'C3-',
'C2-',
'C9-',
'C8-',
'C1o',
'C3o',
'C0o',
'C4o',
'C2o',
'C6o',
'C7o',
'ko',
'C5o',
])

# aLIGO design 
budget = gwinc.load_budget('aLIGO', freq=fflog)
trace = budget.run()
aligo_design_asd = arm_length * trace.asd

#####   Figures   #####
fig = plt.figure()
s1 = fig.add_subplot(111)

for label, ASD, style in zip(labels, ASDs, styles):
    # ASD_logbinned = np.dot(logbin_matrix, ASD)
    lin_log_ff, lin_log_ASD = linear_log_ASD(fflog, ff, ASD)
    if label == 'Measured noise (O3)':
        s1.loglog(lin_log_ff, lin_log_ASD, style, label=label, zorder=3)
    else:
        s1.loglog(lin_log_ff, lin_log_ASD, style, label=label)

lin_log_ff_O1, lin_log_ASD_O1 = linear_log_ASD(fflog, ff_O1, arm_length*spectra['o1'][:,1])
lin_log_ff_O2, lin_log_ASD_O2 = linear_log_ASD(fflog, ff_O2, arm_length*spectra['o2'][:,1])

s1.loglog(lin_log_ff_O1, lin_log_ASD_O1, 'C1-', label='O1', alpha=0.5)
s1.loglog(lin_log_ff_O2, lin_log_ASD_O2, 'C7-', label='O2', alpha=0.5)

s1.set_xlabel('Frequency [Hz]')
s1.set_ylabel(r'DARM [$\mathrm{m}/\sqrt{\mathrm{Hz}}$]')
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.7)
s1.legend(ncol=2,markerscale=3,loc='upper right')

s1.set_xlim([10, 5e3])
s1.set_ylim([3e-21, 4e-17])

plot_name = f'lho_o3_noisebudget.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')

## Make same figure w/ aligo design sensitivity also plotted

s1.loglog(fflog, aligo_design_asd, ls='--', color='k', lw=3, label='aLIGO design')
s1.legend(ncol=2, markerscale=3, loc='upper right')

plot_name = f'lho_o3_noisebudget_with_design.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()

# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()