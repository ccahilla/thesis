'''
frequencyNB.py

Makes frequency noisebudget for the o3 paper

Craig Cahillane
August 7, 2020
'''

import os
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt

from nds2utils import linear_log_ASD

# plt.style.use('tableau-colorblind10')

mpl.rcParams.update({'text.usetex': True,
                     'figure.figsize': (12, 9),
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9,
                     'agg.path.chunksize': 10000})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Functions   #####
def save_txt(fff, psd, filename, header, verbose=True):
    '''Stores a two-column txt of frequency and power spectral density
    fff = frequency vector in Hertz.  Must be np.array with single row of values.
    psd = power spectral density [units^2/Hz].  Must be np.array of same length as fff. 
    filename = full directory path to the file and filename
    header = text to go at the top of the txt
    '''
    save_data = np.vstack((fff, psd)).T
    if verbose:
        print()
        print(f'Saving data file...')
    np.savetxt(filename, save_data, header=header)
    if verbose:
        print(f'{filename}')
    return

def load_txt(filename):
    '''Reads and loads the data in filename.  
    Expects two columns of data, 
    1) Frequency vector in Hz
    2) Spectral density in units/rtHz
    Input:
    filename = full path to file to read
    Output: tuple(fff, psd)
    fff = frequency vector in Hz
    psd = spectral density
    '''
    data = np.loadtxt(filename)
    fff = data[:,0]
    psd = data[:,1]
    return fff, psd

def rms(xx, yy): 
    '''Calculate the cumulative root mean squared value of yy from high to low xx
    Returns an array of the cumlative root mean of yy from xx[-1] to xx[ii]
    '''
    diff = np.diff(np.squeeze(xx)) 
    dx = np.concatenate(([diff[0]], diff), axis=0) 
 
    # Return rms intergrated from high to low 
    rms = np.flipud(np.sqrt(np.cumsum(np.flipud(np.squeeze(yy)**2 * dx)))) 
    return rms 
                                                                                     

#####   Read in txts   #####
txt_names = np.array([
    # 'frequencyNB_Out_of_loop_frequency_noise.txt',
    # 'frequencyNB_In_loop_frequency_noise.txt',
    # 'frequencyNB_Suppressed_input_mode_cleaner_shot_noise.txt',
    # 'frequencyNB_Common_arm_shot_noise.txt',
    'refl_b_noise_projection_incident_frequency_noise.txt',
    # 'refl_a_noise_projection_incident_frequency_noise.txt',
    'carm_out2_noise_projection_incident_frequency_noise.txt',
    'imc_sensing_noise_projection_incident_frequency_noise.txt',
    'carm_sensing_noise_projection_incident_frequency_noise.txt',
    'vco_noise_projection_incident_frequency_noise.txt',
])
labels = np.array([
    'Out-of-loop frequency noise',
    'In-loop frequency noise',
    'Suppressed input-mode-cleaner shot noise',
    'Common-arm shot noise',
    'VCO noise',
])
lss = np.array([
    '-',
    '-',
    '--',
    '-.',
    '-',
])

data_dict = {}
for txt_name, label, ls in zip(txt_names, labels, lss):
    full_txt_name = f'{data_dir}/{txt_name}'
    temp_fff, temp_asd = load_txt(full_txt_name)

    temp_rms = rms(temp_fff, temp_asd)

    data_dict[txt_name] = {}
    data_dict[txt_name]['ff'] = temp_fff
    data_dict[txt_name]['asd'] = temp_asd
    data_dict[txt_name]['rms'] = temp_rms
    data_dict[txt_name]['label'] = label
    data_dict[txt_name]['ls'] = ls

#####   Frequency Vector   #####
fflog = np.logspace(np.log10(1e-2), np.log10(8192.0), 1000)

#####   Figures   #####
# Main noisebudget
fig = plt.figure()
s1 = fig.add_subplot(111)

for txt_name, txt_dict in data_dict.items():
    ff = txt_dict['ff']
    asd = txt_dict['asd']
    label = txt_dict['label']
    ls = txt_dict['ls']

    s1.loglog(ff, asd, ls=ls, label=label)

s1.set_xlim([10, 1e5])
s1.set_ylim([1e-8, 1e-1])

# s1.set_yticks(nu.good_ticks(s1))
s1.set_xlabel('Frequency [Hz]')
s1.set_ylabel(r'Frequency Noise [Hz/$\sqrt{\mathrm{Hz}}$]')
# s1.set_title('LHO Frequency Noisebudget - April 27, 2019')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=16)

plot_name = f'frequency_noisebudget.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Full noisebudget
fig = plt.figure()
s1 = fig.add_subplot(111)

for txt_name, txt_dict in data_dict.items():
    ff = txt_dict['ff']
    asd = txt_dict['asd']
    rms = txt_dict['rms']
    label = txt_dict['label']
    ls = txt_dict['ls']

    p1, = s1.loglog(ff, asd, ls='-', alpha=0.7, label=label)
    s1.loglog(ff, rms, ls='--', color=p1.get_color(), label=f'{label} rms')

    print()
    print(f'{label} rms = {rms[0]} Hz')
print()
    

# s1.set_xlim([10, 5000])
# s1.set_ylim([1e-9, 1e-4])

# s1.set_yticks(nu.good_ticks(s1))
s1.set_xlabel('Frequency [Hz]')
s1.set_ylabel(r'Frequency Noise [Hz/$\sqrt{\mathrm{Hz}}$]')
# s1.set_title('LHO Frequency Noisebudget - April 27, 2019')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=16)

plot_name = f'frequency_noisebudget_full.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()


# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()