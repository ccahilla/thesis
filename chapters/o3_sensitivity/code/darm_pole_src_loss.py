'''
darm_pole_src_loss.py

Plots the expected DARM pole vs SRC loss,
and arm loss.

Craig Cahillane
January 3rd, 2020
'''
import os
import pickle
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc
import scipy.optimize as sco

import mpmath as mp
mp.dps = 30

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.95,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Functions   #####
def cavity_pole(L,
                T1,
                T2):
    '''Computes the cavity pole of a two mirror cavity in Hz.

    Inputs:
    L = length of the cavity in meters
    T1 = power transmission of the input mirror
    T2 = power transmission of the end mirror

    Outputs:
    pole = cavity pole in Hz
    '''
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    FSR = scc.c/(2 * L)
    pole = FSR * np.log(1 / (r1 * r2)) / (2 * np.pi)
    return pole

def Pbs_SQL(L,
            arm_pole,
            M=40.0,
            lambda0=1064e-9,
            ):
    '''Calculates the power on the beamsplitter needed to achieve the standard quantum limit
    in a conventional interferometer at gamma = Omega, 
    where gamma is the arm pole and Omega is the GW audio frequency.
    Written as I_SQL.
    Equation 2.14 of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    L           = length of the interferometer arms (nominal = 3994.5 meters)   
    arm_pole    = arm cavity pole in Hz
    M           = mass of the input and end test masses.  Default is 40 kg.
    lambda0     = wavelength of the laser.  Default is 1064 nm.

    Output:
    i_sql = input intensity for standard quantum limit for GW detection in watts
    '''
    omega0 = 2 * np.pi * scc.c / lambda0    # radians
    gamma = 2 * np.pi * arm_pole            # radians
    pbs_sql = M * L**2 * gamma**4 / (4 * omega0) # watts
    return pbs_sql

def Phi(ff,
        l_SRC,
        lambda0=1064e-9):
    '''Calculates the net phase gained by the audio sideband frequency ff
    while in the signal recycling cavity with carrier detuning phi.
    From section after Equation 2.14 in https://arxiv.org/pdf/gr-qc/0102012.pdf

    Phi = Omega * length_SRC / c

    Inputs:
    ff      = frequency vector in Hz
    l_SRC   = length of the SRC in meters (nominal = 56.0)

    Output:
    Phi = audio sideband detuning for all ff in rads
    '''

    Omega = 2 * np.pi * ff
    Phi = -Omega * l_SRC / scc.c # minus sign for ward model convention

    return Phi

def beta(   ff,
            L,
            T1,
            T2,
            single_pole_approx=True,
        ):
    '''Calculates the net phase gained by the audio sideband frequency ff
    while in the arm cavity with pole arm_pole in Hz.
    
    There are two parts to this code.
    The first part is the beta assumed by KLMTV in 
    https://arxiv.org/pdf/gr-qc/0008026.pdf
    This is a result of the so-called single-pole approximation 
    done by KLMTV in Appendix B, Equation B13.
    Buonanno and Chen also use this convention.

    In reality, there are infinite poles for a two mirror cavity, if we solve
    the cavity eqs denom: 1 - r1 r2 Exp[2 I Omega L / c] == 0. 
    The second part is a model extension done by me to extend the beta param
    to be valid even if the FSR is not much greater than the cavity pole.
    This is important for future detectors, as the FSR goes down with long arms,
    but the DARM cavity bandwidth goes up.

    Use the single_pole_approx flag to switch between the KLMTV and Craig beta params.

    Inputs:
    ff          = frequency vector in Hz
    L = length of the cavity in meters
    T1 = power transmission of the input mirror
    T2 = power transmission of the end mirror

    Outputs:
    beta        = phase gained in radians for each audio frequency ff
    '''
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)

    arm_pole = cavity_pole(L, T1, T2)
    
    Omega = 2 * np.pi * ff
    gamma = 2 * np.pi * arm_pole
    if single_pole_approx:
        beta = -np.arctan2(Omega, gamma) # minus sign for ward model convention

    else:
        numer = r1 * r2 * np.sin(2 * Omega * L / scc.c)
        denom = 1 - r1 * r2 * np.cos(2 * Omega * L / scc.c)
        beta = -np.arctan2(numer, denom) # minus sign for ward model convention

    return beta

def beta2(  ff,
            L,
            T1,
            T2,
        ):
    '''Another way of calculating beta, using Hall's equation D.3 of 
    https://dcc.ligo.org/DocDB/0138/P1600295/002/evan-hall-thesis.pdf
    (This doesn't work, it's just the simple-pole approximation phase component)
    '''
    arm_pole = cavity_pole(L, T1, T2)
    
    Omega = 2 * np.pi * ff
    gamma = 2 * np.pi * arm_pole
    xx = Omega / gamma
    betas = -1j * np.log(np.sqrt((1 - 1j * xx)/(1 + 1j * xx)))
    # betas = np.real(betas)
    return betas

def beta3(  ff,
            L,
            T1,
            T2,
        ):
    '''A third way of calculating beta, using the full expression 
    of the angle of the simple cavity response denominator.
    '''
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)

    Omega = 2 * np.pi * ff

    cav_denom = 1 - r1 * r2 * np.exp(2j * Omega * L / scc.c)
    betas = np.angle(cav_denom)

    return betas

def beta4(  ff,
            L,
            T1,
            T2,
        ):
    '''A third way of calculating beta, using the full expression 
    of the angle of the simple cavity response denominator.
    '''
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)

    Omega = 2 * np.pi * ff

    cav_denom = 1 - r1 * r2 * mp.exp(2j * Omega * L / scc.c)
    # betas = np.angle(cav_denom)
    betas = mp.arg(cav_denom)
    return betas

def beta5(  ff,
            L,
            T1,
            T2,
        ):
    '''Another way of calculating beta, using Hall's equation D.3 of 
    https://dcc.ligo.org/DocDB/0138/P1600295/002/evan-hall-thesis.pdf
    (This doesn't work, it's just the simple-pole approximation phase component)
    '''
    arm_pole = cavity_pole(L, T1, T2)
    
    Omega = 2 * np.pi * ff
    gamma = 2 * np.pi * arm_pole
    xx = Omega / gamma
    betas = -1j * mp.log(mp.sqrt((1 - 1j * xx)/(1 + 1j * xx)))
    # betas = mp.re(betas)
    return betas

def sqrt_xx(ff,
            L,
            T1,
            T2,
        ):
    '''Another way of calculating beta, using Hall's equation D.3 of 
    https://dcc.ligo.org/DocDB/0138/P1600295/002/evan-hall-thesis.pdf
    '''
    arm_pole = cavity_pole(L, T1, T2)
    
    Omega = 2 * np.pi * ff
    gamma = 2 * np.pi * arm_pole
    xx = Omega / gamma
    return np.sqrt((1 - 1j * xx)/(1 + 1j * xx))

def kappa(  ff,
            Pbs,
            L,
            arm_pole,
            M=40.0,
            lambda0=1064e-9
            ):
    '''Calculates the effective coupling constant which governs the 
    coupling of amplitude fluctuations to phase fluctuations in the arms.
    Originally derived in KLMTV.
    Equation 2.13 of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    ff          = frequency vector in Hz
    Pbs         = power on beam splitter in watts
    L           = length of the interferometer arms (nominal = 3994.5 meters)   
    arm_pole    = arm cavity pole in Hz
    M           = mass of the input and end test masses.  Default is 40 kg.
    lambda0     = wavelength of the laser.  Default is 1064 nm.

    Output:
    kappa       = frequency dependent effective coupling constant 
                  from amplitude to phase quadratures
    '''
    Omega = 2 * np.pi * ff          # radians
    gamma = 2 * np.pi * arm_pole    # radians

    pbs_sql = Pbs_SQL(L, arm_pole, M=M, lambda0=lambda0)

    kappa = 2 * (Pbs / pbs_sql) * gamma**4 / (Omega**2 * (gamma**2 + Omega**2))

    return kappa

def MML(ff,
        Ts,
        Ti,
        Te,
        phi,
        Pbs,
        L,
        l_SRC,
        arm_loss,
        SRC_loss,
        post_SRM_loss,
        M=40.0,
        lambda0=1064e-9,
        single_pole_approx=True,
        ):
    '''Calculates normalization factor for the IFO response to input noise and signal,
    including lossy arms, SRC, and PD.
    Used in Equation 5.6.
    Defined in Equation 5.7 of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    ff          = frequency vector in Hz
    Ts          = power transmission of the signal-recyling mirror
    Ti          = power transmission of ITM (nominal = 1.42%)
    Te          = power transmission of ETM (nominal = 4 ppm) 
    phi         = SRC detuning in radians (nominal = pi/2)
    Pbs         = power on beam splitter in watts
    L           = length of the interferometer arms (nominal = 3994.5 meters)   
    l_SRC       = length of the signal recycling cavity (nominal = 56.0 meters)
    M           = mass of the input and end test masses.  Default is 40 kg.
    lambda0     = wavelength of the laser. Default is 1064e-9 m
    single_pole_approx = Assume the single pole approximation. Default is False

    Outputs:
    MM          = normalization factor for all ff      
    '''
    rs = np.sqrt(1 - Ts) # rho in the paper
    epsilon = 2 * arm_loss / Ti # epsilon in the paper
    lpd = post_SRM_loss  # lambdapd in the paper
    lsr = SRC_loss       # lambdasr in the paper

    arm_pole = cavity_pole(L, Ti, Te) # Hz

    Phis = Phi(ff, l_SRC, lambda0=lambda0)
    # betas = beta(ff, L, Ti, Te, single_pole_approx=single_pole_approx)
    betas = beta2(ff, L, Ti, Te)
    kappas = kappa(ff, Pbs, L, arm_pole, M=M, lambda0=lambda0)

    # exp2b = np.exp(2j*(betas + Phis))
    exp2b = np.exp(2j*(betas + Phis))

    # MML =   1 + rs**2 * exp2b**2 \
    #         - 2 * rs * exp2b * (np.cos(2 * phi) + 0.5 * kappas * np.sin(2 * phi)) \
    #         + lsr * rs * exp2b * (-rs * exp2b + np.cos(2 * phi) + 0.5 * kappas * np.sin(2 * phi)) \
    #         + epsilon * rs * exp2b * (2 * np.cos(betas)**2 * (np.cos(2 * phi) - rs * exp2b) + 0.5 * kappas * np.sin(2 * phi) * (3 + exp2b))
    MML =   1 + rs**2 * exp2b**2 \
            - 2 * rs * exp2b * (np.cos(2 * phi) + 0.5 * kappas * np.sin(2 * phi)) \
            + lsr * rs * exp2b * (-rs * exp2b + np.cos(2 * phi) + 0.5 * kappas * np.sin(2 * phi)) \
            + epsilon * rs * exp2b * (2 * np.cos(betas)**2 * (np.cos(2 * phi) - rs * exp2b) + 0.5 * kappas * np.sin(2 * phi) * (3 + exp2b))

    return MML

def MML2(ff,
        Ts,
        Ti,
        Te,
        phi,
        Pbs,
        L,
        l_SRC,
        arm_loss,
        SRC_loss,
        post_SRM_loss,
        M=40.0,
        lambda0=1064e-9,
        single_pole_approx=True,
        ):
    '''Calculates normalization factor for the IFO response to input noise and signal,
    including lossy arms, SRC, and PD.
    Used in Equation 5.6.
    Defined in Equation 5.7 of https://arxiv.org/pdf/gr-qc/0102012.pdf 

    Inputs:
    ff          = frequency vector in Hz
    Ts          = power transmission of the signal-recyling mirror
    Ti          = power transmission of ITM (nominal = 1.42%)
    Te          = power transmission of ETM (nominal = 4 ppm) 
    phi         = SRC detuning in radians (nominal = pi/2)
    Pbs         = power on beam splitter in watts
    L           = length of the interferometer arms (nominal = 3994.5 meters)   
    l_SRC       = length of the signal recycling cavity (nominal = 56.0 meters)
    M           = mass of the input and end test masses.  Default is 40 kg.
    lambda0     = wavelength of the laser. Default is 1064e-9 m
    single_pole_approx = Assume the single pole approximation. Default is False

    Outputs:
    MM          = normalization factor for all ff      
    '''
    rs = np.sqrt(1 - Ts) # rho in the paper
    epsilon = 2 * arm_loss / Ti # epsilon in the paper
    lpd = post_SRM_loss  # lambdapd in the paper
    lsr = SRC_loss       # lambdasr in the paper

    arm_pole = cavity_pole(L, Ti, Te) # Hz

    Phis = Phi(ff, l_SRC, lambda0=lambda0)
    # betas = beta(ff, L, Ti, Te, single_pole_approx=single_pole_approx)
    betas = beta5(ff, L, Ti, Te)
    kappas = kappa(ff, Pbs, L, arm_pole, M=M, lambda0=lambda0)

    # exp2b = np.exp(2j*(betas + Phis))
    exp2b = mp.exp(2j*(betas + Phis))

    # MML =   1 + rs**2 * exp2b**2 \
    #         - 2 * rs * exp2b * (np.cos(2 * phi) + 0.5 * kappas * np.sin(2 * phi)) \
    #         + lsr * rs * exp2b * (-rs * exp2b + np.cos(2 * phi) + 0.5 * kappas * np.sin(2 * phi)) \
    #         + epsilon * rs * exp2b * (2 * np.cos(betas)**2 * (np.cos(2 * phi) - rs * exp2b) + 0.5 * kappas * np.sin(2 * phi) * (3 + exp2b))
    MML =   1 + rs**2 * exp2b**2 \
            - 2 * rs * exp2b * (mp.cos(2 * phi) + 0.5 * kappas * mp.sin(2 * phi)) \
            + lsr * rs * exp2b * (-rs * exp2b + mp.cos(2 * phi) + 0.5 * kappas * mp.sin(2 * phi)) \
            + epsilon * rs * exp2b * (2 * mp.cos(betas)**2 * (mp.cos(2 * phi) - rs * exp2b) + 0.5 * kappas * mp.sin(2 * phi) * (3 + exp2b))

    return MML

def MML_caller(ff, params):
    '''Calls MML, and seeds scipy.optimize.root() correctly so it can find a solution.
    params is dict of param kwargs
    '''
    ff0 = 1j * ff # trick fsolve into thinking it's solving a real equation
    temp_MML = MML(ff0, **params)
    residual = [np.imag(temp_MML[0])] # put into vector of length one, and look at imag part only
    return residual

def simple_coupled_cavity_pole(
        L,
        T1,
        T2,
        T3,
        eta1
    ):
    '''Computes the cavity pole of a two mirror cavity in Hz,
    according to Equation 2.20 of Evan's thesis, P1600295.

    Inputs:
    L = length of the cavity in meters
    T1 = power transmission of the signal recycling mirror
    T2 = power transmission of the input test mass
    T3 = power transmission of the end test mass
    eta1 = power loss in SRC, plus mode mismatch between arm and SRC

    Outputs:
    pole = cavity pole in Hz
    '''
    r1 = np.sqrt(1 - T1 - eta1)
    r2 = np.sqrt(1 - T2)
    r3 = np.sqrt(1 - T3)
    FSR = scc.c/(2 * L)

    pole = FSR * np.log((1 - r2 * r1) / (r3 * r2 - r3 * r1)) / (2 * np.pi)
    return pole


#####   Parameters   #####
suffix_new_itmy = '_new_itmy'
colors = {
    'lho' : '#ee0000',
    'llo' : '#4ba6ff'
}
runs = {
    'lho' : 'O4',
    'llo' : 'O3'
}


lambda0 = 1064e-9
nu0 = scc.c/lambda0
# w0 = 2 * np.pi * nu0

L = 3994.5 # meters, length of arms
l_SRC = 56.01 # meters, length of signal recycling cavity
# FSR = scc.c / (2 * L) # Hz

M = 40 # kg
Ti_dict = {}
Ti_dict['lho'] = 0.015
Ti_dict['llo'] = 0.0148  # ITM power trans
Ti = Ti_dict['lho']
Te = 4e-6   # ETM power trans
Ts = 0.3234   # SRM power trans

phi = 90.0 * np.pi/180 # rads, SRM detuning
zeta = 90.0 * np.pi/180 # rads, homodyne angle

responsivity = scc.e * lambda0/(scc.h * scc.c) # A/W
DCPD_QE = 0.98 # W/W
P_LO = 20e-3 / responsivity / DCPD_QE # W = mA * A/mA * W/A
E_LO = np.sqrt(P_LO)

# Losses
arm_loss = 0e-6   # round-trip
SRC_loss = 0.02     # SRC losses, modeled as losses from reflection on SRM

# numbers from https://git.ligo.org/haocun.yu/lho_squeezing/wikis/squeezer-budget
T_OFI = 0.965
R_OMs = 0.98
T_OMC = 0.957
ModeMatchOMC = 0.95

post_SRM_loss = 1 - T_OFI * R_OMs * T_OMC * ModeMatchOMC

# Preq = 36.5        # W, power requested
Pin = 32.0         # W, power input
PRG = 44.0          # W/W, power recycling gain
Pbs = Pin * PRG   # W, Input Power watts * PRG = Power on beamsplitter in watts

nom_params = [  Ts,
                Ti,
                Te,
                phi,
                Pbs,
                L,
                l_SRC,
                arm_loss,
                SRC_loss,
                post_SRM_loss,
            ]
nom_dict = {}
nom_dict['Ts'] = Ts
nom_dict['Ti'] = Ti
nom_dict['Te'] = Te
nom_dict['phi'] = phi
nom_dict['Pbs'] = Pbs
nom_dict['L'] = L
nom_dict['l_SRC'] = l_SRC
nom_dict['arm_loss'] = arm_loss
nom_dict['SRC_loss'] = SRC_loss
nom_dict['post_SRM_loss'] = post_SRM_loss

#####   Loss vectors   #####

ff = np.logspace(np.log10(1), np.log10(100000), 500)
SRC_losses = np.linspace(0, 0.055, 550)
arm_losses = np.linspace(0, 1000e-6, 500)


#####   Numerically solve for pole   #####
ff_linear = np.linspace(400, 500, 5000)
darm_poles_vs_SRC_loss_dict = {}
for ifo, Ti in Ti_dict.items():

    MMLs = MML(1j*ff, **nom_dict)
    # MML_callers = MML_caller(ff, nom_dict)

    betas1 = beta(ff, L, Ti, Te)
    betas2 = beta2(1j*ff, L, Ti, Te)
    betas3 = beta3(1j*ff, L, Ti, Te)

    expb1 = np.exp(1j * betas1)
    expb2 = sqrt_xx(ff, L, Ti, Te)


    darm_poles_vs_SRC_loss = np.array([])
    for temp_SRC_loss in SRC_losses:
        temp_dict = {}
        temp_dict['Ts'] = Ts
        temp_dict['Ti'] = Ti
        temp_dict['Te'] = Te
        temp_dict['phi'] = phi
        temp_dict['Pbs'] = Pbs
        temp_dict['L'] = L
        temp_dict['l_SRC'] = l_SRC
        temp_dict['arm_loss'] = arm_loss
        temp_dict['SRC_loss'] = temp_SRC_loss
        temp_dict['post_SRM_loss'] = post_SRM_loss

        # def MML_caller2(fff):
        #     return MML2(fff, **temp_dict)

        temp_MMLs = MML(1j * ff_linear, **temp_dict)
        idx = np.argmin(np.real(temp_MMLs))
        temp_pole = ff_linear[idx]

        # x0 = 410j
        # temp_pole_out = mp.findroot(MML_caller2, x0, tol=0.01)
        # temp_pole = np.imag(np.complex(str(temp_pole_out).replace(" ", "")))

        # temp_pole_array = sco.fsolve(MML_caller, x0, args=(temp_dict))
        # temp_pole = temp_pole_array[0]

        darm_poles_vs_SRC_loss = np.append(darm_poles_vs_SRC_loss, temp_pole)

    darm_poles_vs_SRC_loss_dict[ifo] = np.copy(darm_poles_vs_SRC_loss)


darm_poles_vs_arm_loss = np.array([])
for temp_arm_loss in arm_losses:
    temp_dict = {}
    temp_dict['Ts'] = Ts
    temp_dict['Ti'] = Ti
    temp_dict['Te'] = Te
    temp_dict['phi'] = phi
    temp_dict['Pbs'] = Pbs
    temp_dict['L'] = L
    temp_dict['l_SRC'] = l_SRC
    temp_dict['arm_loss'] = temp_arm_loss
    temp_dict['SRC_loss'] = SRC_loss
    temp_dict['post_SRM_loss'] = post_SRM_loss
    
    temp_MMLs = MML(1j * ff_linear, **temp_dict)
    idx = np.argmin(np.real(temp_MMLs))
    temp_pole = ff_linear[idx]

    darm_poles_vs_arm_loss = np.append(darm_poles_vs_arm_loss, temp_pole)


#####   KLMTV B.13 comparison   #####

FSR = scc.c/(2 * L)
ri = np.sqrt(1 - Ti)
re = np.sqrt(1 - Te)
rs = np.sqrt(1 - Ts)

gamma = Ti * scc.c / (4 * L)

LHS = 1 - ri * re * np.exp(2j * 2 * np.pi * ff * L / scc.c)
RHS = (1/FSR) * (gamma - 1j * 2 * np.pi * ff)


#####   Simple coupled cavity pole   #####

simple_cc_poles = simple_coupled_cavity_pole(L, Ts, Ti, Te, SRC_losses)


#####   Measurements   #####
darm_pole_design_dict = {}
for ifo, Ti in Ti_dict.items():
    ri = np.sqrt(1 - Ti)
    darm_pole_design = FSR/(2*np.pi) * np.log((1 - ri * rs)/(re * ri - re * rs ))
    darm_pole_design_dict[ifo] = darm_pole_design

darm_pole_hanford_o3 = 411 # Hz
darm_pole_hanford = 451 # Hz
darm_pole_livingston = 450 # Hz

idx_livingston = np.argwhere(darm_pole_livingston > darm_poles_vs_SRC_loss_dict['llo'])[0,0]
idx_hanford = np.argwhere(darm_pole_hanford > darm_poles_vs_SRC_loss_dict['lho'])[0,0]

SRC_loss_livingston = SRC_losses[idx_livingston]
SRC_loss_hanford = SRC_losses[idx_hanford]

print()
for ifo, Ti in Ti_dict.items():
    print(f'darm_pole_design    = {darm_pole_design_dict[ifo]:.1f} Hz')
print(f'darm_pole_hanford   = {darm_pole_hanford} Hz')
print(f'darm_pole_livingston = {darm_pole_livingston} Hz')
print()
print(f'SRC_loss_hanford    = {100*SRC_loss_hanford} %')
print(f'SRC_loss_livingston = {100*SRC_loss_livingston} %')
print()
print(f"Zero loss Hanford pole from BnC = {darm_poles_vs_SRC_loss_dict['lho'][0]}")
print()



#####   Figures   #####
plot_ylims = [430, 470]
plot_xlims = [0.0, 3.0]

# SRC losses vs DARM pole
fig, (s1) = plt.subplots(1)

for ifo, Ti in Ti_dict.items():
    color = colors[ifo]
    run = runs[ifo]
    plot_darm_poles_vs_SRC_loss = darm_poles_vs_SRC_loss_dict[ifo]
    s1.plot(100*SRC_losses, np.abs(plot_darm_poles_vs_SRC_loss), color=color, label=f'{ifo.upper()} {run} DARM pole model')


for ifo, Ti in Ti_dict.items():
    color = colors[ifo]
    run = runs[ifo]
    darm_pole_design = darm_pole_design_dict[ifo]
    s1.plot(plot_xlims, [darm_pole_design, darm_pole_design], color=color, ls=':', label=f'{ifo.upper()} {run} design {darm_pole_design:.0f} Hz')

p2, = s1.plot(plot_xlims, [darm_pole_hanford, darm_pole_hanford], color=colors['lho'], ls='--', label=f'Measured LHO O4 {darm_pole_hanford} Hz')
p1, = s1.plot(plot_xlims, [darm_pole_livingston, darm_pole_livingston], color=colors['llo'], ls='--', label=f'Measured LLO O3 {darm_pole_livingston} Hz')

s1.plot([100*SRC_loss_hanford, 100*SRC_loss_hanford], [400, darm_pole_hanford], color=colors['lho'], alpha=0.6, label=f'Estimated Hanford SRC loss = {100*SRC_loss_hanford:.1f} \%')
s1.plot([100*SRC_loss_livingston, 100*SRC_loss_livingston], [400, darm_pole_livingston], color=colors['llo'], alpha=0.6, label=f'Estimated Livingston SRC loss = {100*SRC_loss_livingston:.1f} \%')

s1.set_xlim(plot_xlims)
s1.set_ylim(plot_ylims)

# s1.set_title('Frequency to darm tf')
s1.set_ylabel('DARM pole '+r'$\omega_{rse}/(2\pi)$'+' [Hz]')
s1.set_xlabel('Signal-recycling cavity loss '+r'$\lambda_{sr}$'+' [\%]')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='upper right')

# Save the figure
plot_name = f'darm_pole_vs_src_losses{suffix_new_itmy}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# arm losses vs DARM pole
fig, (s1) = plt.subplots(1)

s1.plot(1e6*arm_losses, np.abs(darm_poles_vs_arm_loss), label='DARM pole vs arm losses')

plot_xlims = [1e6*arm_losses[0], 1e6*arm_losses[-1]]
s1.plot(plot_xlims, [darm_pole_design, darm_pole_design], ls='--', label='Design')
p1, = s1.plot(plot_xlims, [darm_pole_livingston, darm_pole_livingston], ls='--', label='Measured Livingston')
p2, = s1.plot(plot_xlims, [darm_pole_hanford, darm_pole_hanford], ls='--', label='Measured Hanford')

# plot_ylims = [400, 460]
# s1.plot([100*SRC_loss_livingston, 100*SRC_loss_livingston], [400, darm_pole_livingston], color=p1.get_color(), alpha=0.6, label=f'Estimated Livingston SRC loss = {100*SRC_loss_livingston:.1f} \%')
# s1.plot([100*SRC_loss_hanford, 100*SRC_loss_hanford], [400, darm_pole_hanford], color=p2.get_color(), alpha=0.6,label=f'Estimated Livingston SRC loss = {100*SRC_loss_hanford:.1f} \%')

# s1.set_xlim(plot_xlims)
# s1.set_ylim(plot_ylims)

# s1.set_title('Frequency to darm tf')
s1.set_ylabel('DARM pole '+r'$\omega_{rse}/(2\pi)$'+' [Hz]')
s1.set_xlabel('Arm loss '+r'$\mathcal{L}_{rt}$'+' [ppm]')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='upper right')

# Save the figure
plot_name = f'darm_pole_vs_arm_losses{suffix_new_itmy}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# BnC vs simple coupled cavity poles with same SRC losses
fig, (s1) = plt.subplots(1)

s1.plot(100*SRC_losses, np.abs(darm_poles_vs_SRC_loss), label='BnC DARM pole')
s1.plot(100*SRC_losses, simple_cc_poles, color='C5', label='Simple coupled cavity pole')


plot_xlims = [100*SRC_losses[0], 100*SRC_losses[-1]]
s1.plot(plot_xlims, [darm_pole_design, darm_pole_design], ls='--', label='Design')
p1, = s1.plot(plot_xlims, [darm_pole_livingston, darm_pole_livingston], ls='--', label='Measured Livingston')
p2, = s1.plot(plot_xlims, [darm_pole_hanford, darm_pole_hanford], ls='--', label='Measured Hanford')

s1.plot([100*SRC_loss_livingston, 100*SRC_loss_livingston], [400, darm_pole_livingston], color=p1.get_color(), alpha=0.6, label=f'Estimated Livingston SRC loss = {100*SRC_loss_livingston:.1f} \%')
s1.plot([100*SRC_loss_hanford, 100*SRC_loss_hanford], [400, darm_pole_hanford], color=p2.get_color(), alpha=0.6,label=f'Estimated Hanford SRC loss = {100*SRC_loss_hanford:.1f} \%')

s1.set_xlim(plot_xlims)
s1.set_ylim(plot_ylims)

# s1.set_title('Frequency to darm tf')
s1.set_ylabel('DARM pole '+r'$\omega_{rse}/(2\pi)$'+' [Hz]')
s1.set_xlabel('Signal-recycling cavity loss '+r'$\lambda_{sr}$'+' [\%]')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='upper right')

# Save the figure
plot_name = f'bnc_lossy_darm_pole_vs_simpilified_darm_pole{suffix_new_itmy}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



if False:
    # MML check
    fig, (s1, s2) = plt.subplots(2, sharex=True)

    s1.loglog(ff, np.abs(MMLs))
    s2.semilogx(ff, np.angle(MMLs, deg=True))

    # s1.set_xlim([fflog[0], fflog[-1]])
    # s1.set_ylim([1e-18, 1.01e-12])

    # s1.set_title('Frequency to darm tf')
    s1.set_ylabel('Frequency [Hz]')
    s1.set_xlabel('MML')

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.6)

    # s1.legend(fontsize=14)

    # Save the figure
    plot_name = 'ff_vs_mmls.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print(f'Writing plot PDF to {full_plot_name}')
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # beta check
    fig, (s1) = plt.subplots(1)

    s1.semilogx(ff, 180/np.pi * betas1, label='arctan2 beta')
    s1.semilogx(ff, 180/np.pi * betas2, ls='--', label='Hall thesis Eq. D.4 beta')
    s1.semilogx(ff, 180/np.pi * betas3, ls='-.', label='beta from angle'+r'$(1 - r_1 r_2 e^{i 2 \Omega L / c})$')

    # s1.set_xlim([fflog[0], fflog[-1]])
    # s1.set_ylim([1e-18, 1.01e-12])

    # s1.set_title('Frequency to darm tf')
    s1.set_ylabel(r'$\beta$'+' [degs]')
    s1.set_xlabel('Frequency [Hz]')

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.6)

    s1.legend(fontsize=14)

    # Save the figure
    plot_name = 'betas_comparison.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print(f'Writing plot PDF to {full_plot_name}')
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # exp(1j*beta) calculation comparison
    fig, (s1, s2) = plt.subplots(2, sharex=True)

    s1.loglog(ff, np.abs(expb1), label=r'$e^{i \beta}$')
    s1.loglog(ff, np.abs(expb2), ls='--', label='Hall thesis Eq. D.4 ' + r'$\sqrt{\frac{1 - i x}{1 + i x}}$')

    s2.semilogx(ff, np.angle(expb1, deg=True), label=r'$e^{i \beta}$')
    s2.semilogx(ff, np.angle(expb2, deg=True), ls='--', label='Hall thesis Eq. D.4 ' + r'$\sqrt{\frac{1 - i x}{1 + i x}}$')

    # s1.set_xlim([fflog[0], fflog[-1]])
    # s1.set_ylim([1e-18, 1.01e-12])

    # s1.set_title('Frequency to darm tf')
    s1.set_ylabel(r'$\beta$'+' [degs]')
    s1.set_xlabel('Frequency [Hz]')

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.6)

    s1.legend(fontsize=14)

    # Save the figure
    plot_name = 'exp_beta_comparison.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print(f'Writing plot PDF to {full_plot_name}')
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # simple pole approximation
    fig, (s1, s2) = plt.subplots(2, sharex=True)

    s1.loglog(ff, np.abs(LHS), label=r'$1 - r_1 r_2 e^{i 2 \Omega L / c}$')
    s1.loglog(ff, np.abs(RHS), ls='--', label=r'$(2 L / c) (\gamma + i \Omega)$')

    s2.semilogx(ff, np.angle(LHS, deg=True), label=r'$1 - r_1 r_2 e^{i 2 \Omega L / c}$')
    s2.semilogx(ff, np.angle(RHS, deg=True), ls='--', label=r'$(2 L / c) (\gamma + i \Omega)$')

    # s1.set_xlim([fflog[0], fflog[-1]])
    # s1.set_ylim([1e-18, 1.01e-12])

    # s1.set_title('Frequency to darm tf')
    s1.set_ylabel(r'FP Denominator'+' [degs]')
    s1.set_xlabel('Frequency [Hz]')

    s2.set_yticks([-180, -90, 0, 90, 180])

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.6)
    s2.grid()
    s2.grid(which='minor', ls='--', alpha=0.6)

    s1.legend(fontsize=14)

    # Save the figure
    plot_name = 'simple_pole_approx.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print(f'Writing plot PDF to {full_plot_name}')
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()


# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()