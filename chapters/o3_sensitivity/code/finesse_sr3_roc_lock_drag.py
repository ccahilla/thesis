'''
finesse_sr3_roc_lock_drag.py

Use Finesse to lock drag the SR3 RoC.  
Simulate the DARM plant transfer function in W/m at several locations.

Craig Cahillane
Jan 5, 2021
'''
import os
import pickle
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc
import scipy.interpolate as sci

import pykat
import pykat.ifo.aligo as aligo

import nds2utils as nu
from nds2utils.make_interactive_svg import make_interactive_svg_multiple_subplots, make_interactive_svg

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})


#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Functions   #####
def lock_drag(  base, 
                lock_drag_component_name,
                lock_drag_attributes,
                max_lock_drag_value,
                min_lock_drag_value,
                number_of_drag_points,
                lock_drag_pickle_filename,
                rerun_lock_drag=False,
                verbose=True
                ):
    ''' 
    lock_drag() moves a FINESSE component's attribute by some max and min amount defined by the user,
    over a user-defined number of steps.  At every step, FINESSE moves the lock points such that light
    continues to resonate in the IFO as intended.  These lock points are saved in a pickle file,
    and two FINESSE outputs are returns, the first for the increase the attribute value, 
    the second for the decrease.

    Inputs:
    base                        = base kat aligo FINESSE file.
    lock_drag_component_name    = name of component whose attribute we want to drag
    lock_drag_attributes        = list of attributes of component we want to drag
    max_lock_drag_value         = max value we want to change the attribute value by
    min_lock_drag_value         = min value we want to change the attribute value by
    number_of_drag_points       = number of points to evaluate at.  Smaller steps can preserve locks.
    lock_drag_pickle_filename   = name of pickle file to save results in.
    rerun_lock_drag             = reruns the lock drag even if .pkl is found
    '''

    lock_drag_done = False
    if os.path.exists(lock_drag_pickle_filename):
        if verbose:
            print()
            print(f'Lock drag pickle file found:')
            print(f'{lock_drag_pickle_filename}')
            print()
        lock_drag_done = True

    if not lock_drag_done or rerun_lock_drag:
        # make a copy of the base kat object contructed from the file
        kat = base.deepcopy()

        # Make a list of all the mirrors that the lock outputs are fed into
        mirrors = [ kat.ETMX, kat.ETMXAR,
                    kat.ITMX, kat.ITMXAR, 
                    kat.ETMY, kat.ETMYAR,
                    kat.ITMY, kat.ITMYAR, 
                    kat.SRM, kat.SRMAR, 
                    kat.PRM, kat.PRMAR]

        # Loop through each of the mirrors then add the extra commands
        for m in mirrors:
            kat.parse(f"""
            set _{m.name}_phi {m.name} phi
            func {m.name}_phi = $_{m.name}_phi 
            """)

        kat1 = kat.deepcopy()
        kat1_string = \
        f"""
        var dummy 0
        xaxis dummy re lin 0 {max_lock_drag_value} {number_of_drag_points-1}
        """
        for lock_drag_attribute in lock_drag_attributes:
            kat1_string += '\n'+ f"""put* {lock_drag_component_name} {lock_drag_attribute} $x1"""

        kat1.parse(kat1_string)
        if verbose:
            print(f'kat1_string:\n{kat1_string}')

        kat2 = kat.deepcopy()
        kat2_string = \
        f"""
        var dummy 0
        xaxis dummy re lin 0 {min_lock_drag_value} {number_of_drag_points-1}
        """
        for lock_drag_attribute in lock_drag_attributes:
            kat2_string += '\n'+ f"""put* {lock_drag_component_name} {lock_drag_attribute} $x1"""

        kat2.parse(kat2_string)
        if verbose:
            print(f'kat2_string:\n{kat2_string}')

        out1 = kat1.run(cmd_args=["-cr=on"])
        out2 = kat2.run(cmd_args=["-cr=on"])

        # Open a binary pickle file to dump to
        with open(lock_drag_pickle_filename, "wb") as f:
            pickle.dump((out1, out2), f)

    else: # if the lock drag is already done
        print()
        print(f'Lock drag already done for these settings, loading {lock_drag_pickle_filename}')
        print()
        with open(lock_drag_pickle_filename, "rb") as f:
            (out1, out2) = pickle.load(f)
    
    return out1, out2

###  Read in DARM TF data  ###
data_fn = np.loadtxt(f'{data_dir}/2019_08_19_DARM_plant_mA_per_pm_Aug_Spots__No_SRCL_Offset.txt')
meas_ff = data_fn[:,0]
meas_TF = data_fn[:,1] * np.exp(1j*data_fn[:,2])
meas_coh = data_fn[:,3]

QE   = 0.98
resp = 0.856 # A/W

meas_TF *= 1e-3/1e-12 # mA/pm -> A/m
meas_TF *= 1/(QE*resp) # A/m -> W/m
meas_TF *= -1 # sign flip for some reason

###  Sort the data  ###
indicies = np.argsort(meas_ff)
meas_ff = meas_ff[indicies]
meas_TF = meas_TF[indicies]
meas_coh = meas_coh[indicies]

###  Setup aligo Finesse model  ###
base = aligo.make_kat()

maxtem_order = 4
base.maxtem = maxtem_order
# print(base)

# base.IFO.remove_IMC_HAM2(True, False)
inputLosses = 0.1 # ~10% through IMC, FI, and steering mirrors
inputPower = 37 * (1 - inputLosses)

base.L0.P.value = inputPower # W

base.SRM.T    = 0.32
base.SR2.L    = 0.05
base.ETMX.L   = 35e-6
base.ETMY.L   = 35e-6

#https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=47113
base.mod1.midx = 0.16
base.mod2.midx = 0.18

base.ITMY.setRTL(None, 1.42/100., 40e-6)
base.ITMX.setRTL(None, 1.5/100., 40e-6)
base.PRM.L.value /= 4 # Hand fiddling to get PRC gain about right

base.yaxis = 'abs:deg'

# LHO geometry with galaxy optics https://galaxy.ligo.caltech.edu/optics/
# SR3_RoC = 36.013
# base.SR3.Rcx.value = SR3_RoC
# base.SR3.Rcy.value = SR3_RoC

# SR2_RoC = -6.424
# base.SR2.Rcx.value = SR2_RoC
# base.SR2.Rcy.value = SR2_RoC

# SRM_RoC = -5.678
# base.SRM.Rcx.value = SRM_RoC
# base.SRM.Rcy.value = SRM_RoC

# PR3_RoC = 36.021
# base.PR3.Rcx.value = PR3_RoC
# base.PR3.Rcy.value = PR3_RoC

# PR2_RoC = -4.543
# base.PR2.Rcx.value = PR2_RoC
# base.PR2.Rcy.value = PR2_RoC

# PRM_RoC = 10.948
# base.PRM.Rcx.value = PRM_RoC
# base.PRM.Rcy.value = PRM_RoC

# ITMX_RoC = -1940.3
# base.ITMX.Rcx.value = ITMX_RoC
# base.ITMX.Rcy.value = ITMX_RoC

# ITMY_RoC = -1939.20
# base.ITMY.Rcx.value = ITMY_RoC
# base.ITMY.Rcy.value = ITMY_RoC

# ETMX_RoC = 2244.2
# base.ETMX.Rcx.value = ETMX_RoC
# base.ETMX.Rcy.value = ETMX_RoC

# ETMY_RoC = 2246.9
# base.ETMY.Rcx.value = ETMY_RoC
# base.ETMY.Rcy.value = ETMY_RoC


# Change into diopters
base.ITMY_lens.p = 1/base.ITMY_lens.f.value + 0e-6
base.ITMX_lens.p = 1/base.ITMX_lens.f.value + 0e-6

base.IFO.CARM.port.phase = -87 #92.5

base.IFO.PRCL.port.phase = -60 #119.8
base.IFO.PRCL.quad = 'I'

base.IFO.SRCL.port = base.IFO.POP_f2
base.IFO.SRCL.quad = 'I'

base.IFO.MICH.port.phase = 103.4
base.IFO.MICH.quad = 'Q'

base = aligo.setup(base, verbose=True)

base.DARM_lock.offset = -20e-3/(QE*resp) # Get 20mA of DC signal
base.SRCL_lock.gain /= 2
base.PRCL_lock.gain /= 2
base.DARM_lock.gain /= 2
base.CARM_lock.gain /= 2
base.MICH_lock.gain /= 2

base.SRCL_lock.accuracy /= 5
base.PRCL_lock.accuracy /= 5
base.DARM_lock.accuracy /= 5
base.CARM_lock.accuracy /= 5
base.MICH_lock.accuracy /= 5

base.IFO.zero_locks()


# Add in amplitude detectors for carrier HOMs
run_ad_code = True
if run_ad_code:
    ad_list = np.array(['in',
                        'pop',
                        'mich_x',
                        'mich_y',
                        'arm_x',
                        'arm_y',
                        'src',
                        'omc',
                        'as',
                        ])

    ad_p45_list = np.array(['in_p45',
                            'pop_p45',
                            'mich_x_p45',
                            'mich_y_p45',
                            'arm_x_p45',
                            'arm_y_p45',
                            'src_p45',
                            'omc_p45',
                            'as_p45',
                            ])

    ad_m45_list = np.array(['in_m45',
                            'pop_m45',
                            'mich_x_m45',
                            'mich_y_m45',
                            'arm_x_m45',
                            'arm_y_m45',
                            'src_m45',
                            'omc_m45',
                            'as_m45',
                            ])

    ad_code = '' 
    for ii in range(maxtem_order+1):
        for jj in range(maxtem_order+1):
            if ii + jj > maxtem_order:
                continue
            ad_code += f'ad ad{ii}{jj}_in {ii} {jj} 0 nLaserOut' + '\n'   # input laser
            # ad_code += f'ad ad{ii}{jj}_imc {ii} {jj} 0 nIMCout' + '\n'  # input through IMC
            ad_code += f'ad ad{ii}{jj}_pop {ii} {jj} 0 nPOP' + '\n'       # POP
            ad_code += f'ad ad{ii}{jj}_mich_x {ii} {jj} 0 nITMX1a' + '\n' # coming from ITMX
            ad_code += f'ad ad{ii}{jj}_mich_y {ii} {jj} 0 nITMY1a' + '\n' # coming from ITMY
            ad_code += f'ad ad{ii}{jj}_arm_x {ii} {jj} 0 nETMX1' + '\n'   # coming from ITMX
            ad_code += f'ad ad{ii}{jj}_arm_y {ii} {jj} 0 nETMY1' + '\n'   # coming from ITMY
            ad_code += f'ad ad{ii}{jj}_src {ii} {jj} 0 nSRM1*' + '\n'     # incident on SRM
            ad_code += f'ad ad{ii}{jj}_omc {ii} {jj} 0 nOM3b' + '\n'      # incident on OMC
            ad_code += f'ad ad{ii}{jj}_as {ii} {jj} 0 nAS' + '\n'         # through the OMC

            ad_code += f'ad ad{ii}{jj}_in_p45 {ii} {jj} {int(base.IFO.f2)} nLaserOut' + '\n'   # input laser
            # ad_code += f'ad ad{ii}{jj}_imc {ii} {jj} {int(base.IFO.f2)} nIMCout' + '\n'      # input through IMC
            ad_code += f'ad ad{ii}{jj}_pop_p45 {ii} {jj} {int(base.IFO.f2)} nPOP' + '\n'       # POP
            ad_code += f'ad ad{ii}{jj}_mich_x_p45 {ii} {jj} {int(base.IFO.f2)} nITMX1a' + '\n' # coming from ITMX
            ad_code += f'ad ad{ii}{jj}_mich_y_p45 {ii} {jj} {int(base.IFO.f2)} nITMY1a' + '\n' # coming from ITMY
            ad_code += f'ad ad{ii}{jj}_arm_x_p45 {ii} {jj} {int(base.IFO.f2)} nETMX1' + '\n'   # coming from ITMX
            ad_code += f'ad ad{ii}{jj}_arm_y_p45 {ii} {jj} {int(base.IFO.f2)} nETMY1' + '\n'   # coming from ITMY
            ad_code += f'ad ad{ii}{jj}_src_p45 {ii} {jj} {int(base.IFO.f2)} nSRM1*' + '\n'     # incident on SRM
            ad_code += f'ad ad{ii}{jj}_omc_p45 {ii} {jj} {int(base.IFO.f2)} nOM3b' + '\n'      # incident on OMC
            ad_code += f'ad ad{ii}{jj}_as_p45 {ii} {jj} {int(base.IFO.f2)} nAS' + '\n'         # through the OMC

            ad_code += f'ad ad{ii}{jj}_in_m45 {ii} {jj} {-int(base.IFO.f2)} nLaserOut' + '\n'   # input laser
            # ad_code += f'ad ad{ii}{jj}_imc {ii} {jj} {-int(base.IFO.f2)} nIMCout' + '\n'      # input through IMC
            ad_code += f'ad ad{ii}{jj}_pop_m45 {ii} {jj} {-int(base.IFO.f2)} nPOP' + '\n'       # POP
            ad_code += f'ad ad{ii}{jj}_mich_x_m45 {ii} {jj} {-int(base.IFO.f2)} nITMX1a' + '\n' # coming from ITMX
            ad_code += f'ad ad{ii}{jj}_mich_y_m45 {ii} {jj} {-int(base.IFO.f2)} nITMY1a' + '\n' # coming from ITMY
            ad_code += f'ad ad{ii}{jj}_arm_x_m45 {ii} {jj} {-int(base.IFO.f2)} nETMX1' + '\n'   # coming from ITMX
            ad_code += f'ad ad{ii}{jj}_arm_y_m45 {ii} {jj} {-int(base.IFO.f2)} nETMY1' + '\n'   # coming from ITMY
            ad_code += f'ad ad{ii}{jj}_src_m45 {ii} {jj} {-int(base.IFO.f2)} nSRM1*' + '\n'     # incident on SRM
            ad_code += f'ad ad{ii}{jj}_omc_m45 {ii} {jj} {-int(base.IFO.f2)} nOM3b' + '\n'      # incident on OMC
            ad_code += f'ad ad{ii}{jj}_as_m45 {ii} {jj} {-int(base.IFO.f2)} nAS' + '\n'         # through the OMC

    base.parse(ad_code)

###  Do the lock drag ### 
lock_drag_component_name = 'SR3'                # Name of the component we want to lock drag
lock_drag_attributes = np.array(['Rcx', 'Rcy']) # The component attributes we want to drag
lock_drag_units = 'm'                           # Units of the lock drag attribute
max_lock_drag_value =  0.022                      # m, the max value to drag by, starting at the original value
min_lock_drag_value =  -0.022                      # m, the min value to drag by, starting at the original value
number_of_drag_points = 1001                     # Number of points to evaluate at.  Recall that finesse uses number of steps = number of points - 1

# Name of the file to save results to
lock_drag_attribute = ''.join(lock_drag_attributes)

lock_drag_plot_suffix = ''
# lock_drag_plot_suffix = '_updated_params'

rerun_lock_drag = True  # Set if we want to rerun the lock drag, even if we already have a pickle

lock_drag_pickle_filename = \
f'lock_drag_{lock_drag_component_name}_{lock_drag_attribute}' + \
f'_m{str(np.abs(min_lock_drag_value)).replace(".", "p")}_{str(max_lock_drag_value).replace(".", "p")}' + \
f'_pointnumber_{number_of_drag_points}' + \
f'_maxtem_{maxtem_order}' + \
f'{lock_drag_plot_suffix}.pkl' 

lock_drag_plot_label = r'$%s_\mathrm{%s}$'%('RoC', lock_drag_component_name.replace('_', ' '))

# kat = base.deepcopy()
# kat.IFO.suspend_mirrors_z()
# kat.IFO.zero_locks()
# kat.removeBlock('locks')

# kat.SRM.phi += 0.7
# #Matching peak
# #kat.SRM.phi += 0.85
# #kat.BS.phi += 0.15 #0.0045

# out = kat.run(cmd_args=['-cr=on'])


out1, out2 = lock_drag( base, 
                        lock_drag_component_name,
                        lock_drag_attributes,
                        max_lock_drag_value,
                        min_lock_drag_value,
                        number_of_drag_points,
                        lock_drag_pickle_filename,
                        rerun_lock_drag,
                        )

# Make interpolated functions for the IFO mirror tunings
etmx_tune = sci.interp1d(np.hstack((out1.x, out2.x)), np.hstack((np.real(out1["ETMX_phi"]), np.real(out2["ETMX_phi"]))))
itmx_tune = sci.interp1d(np.hstack((out1.x, out2.x)), np.hstack((np.real(out1["ITMX_phi"]), np.real(out2["ITMX_phi"]))))
etmy_tune = sci.interp1d(np.hstack((out1.x, out2.x)), np.hstack((np.real(out1["ETMY_phi"]), np.real(out2["ETMY_phi"]))))
itmy_tune = sci.interp1d(np.hstack((out1.x, out2.x)), np.hstack((np.real(out1["ITMY_phi"]), np.real(out2["ITMY_phi"]))))
prm_tune  = sci.interp1d(np.hstack((out1.x, out2.x)), np.hstack((np.real(out1["PRM_phi"]), np.real(out2["PRM_phi"]))))
srm_tune  = sci.interp1d(np.hstack((out1.x, out2.x)), np.hstack((np.real(out1["SRM_phi"]), np.real(out2["SRM_phi"]))))

tunes_dict = {}
tunes_dict['ETMX'] = etmx_tune
tunes_dict['ITMX'] = itmx_tune
tunes_dict['ETMY'] = etmy_tune
tunes_dict['ITMY'] = itmy_tune
tunes_dict['PRM'] = prm_tune
tunes_dict['SRM'] = srm_tune

inj_code = \
f'''
fsig sig ETMX z 1 180 1
#fsig sig ETMY z 1 0 1 

xaxis sig f log 1 2000 299

pd1 AS $fs nAS
pd DC_AS nAS
pd P_PRC nPRM2
pd P_X nETMX1
pd P_Y nETMY1

yaxis lin abs:deg
'''

original_value = getattr(getattr(getattr(base, lock_drag_component_name), lock_drag_attributes[0]), 'value')

# Start a new kat, this time to be run with DARM signal injected
num_of_injs = 9
delta_values = np.linspace(0, 0.019, num_of_injs) 

kats_tuned = {}
outs_tuned = {}
for ii, delta_value in enumerate(delta_values):
    print(f'Starting run {ii+1}/{num_of_injs}')

    kat_tuned = base.deepcopy()

    # Change in thermal lens that we want to look at
    for lock_drag_attribute in lock_drag_attributes:
        original_value = getattr(getattr(getattr(kat_tuned, lock_drag_component_name), lock_drag_attribute), 'value')
        new_value = original_value + delta_value
        setattr(getattr(getattr(kat_tuned, lock_drag_component_name), lock_drag_attribute), 'value', new_value)

    # Using our interpolated function to set the IFO mirror tunings
    kat_tuned.ETMX.phi = kat_tuned.ETMXAR.phi = etmx_tune(delta_value)
    kat_tuned.ITMX.phi = kat_tuned.ITMXAR.phi = itmx_tune(delta_value)
    kat_tuned.ETMY.phi = kat_tuned.ETMYAR.phi = etmy_tune(delta_value)
    kat_tuned.ITMY.phi = kat_tuned.ITMYAR.phi = itmy_tune(delta_value)
    kat_tuned.PRM.phi  = kat_tuned.PRMAR.phi  = prm_tune(delta_value)
    kat_tuned.SRM.phi                         = srm_tune(delta_value)

    # Inject freq noise into the lock-dragged IFO
    kat_tuned.parse(inj_code)

    kat_tuned.IFO.suspend_mirrors_pitch()
    kat_tuned.IFO.suspend_mirrors_z()
    kat_tuned.removeBlock("locks")

    # Run the freq noise injected lock-dragged IFO code
    out_tuned = kat_tuned.run()

    # Store data in dict
    kats_tuned[delta_value] = kat_tuned
    outs_tuned[delta_value] = out_tuned


# Scan Finesse error signals
run_scan_code = True
if run_scan_code:
    xlimits = [-3e-2, 3e-2]
    steps = 300
    dof_scales = np.array([1.5, 1.5, 100.0, 3000.0, 1000.0])

    kats_scanned = {}
    outs_scanned = {}
    for jj, delta_value in enumerate(delta_values):
        print(f'Starting error signal scan {jj+1}/{num_of_injs}')
        kats_scanned[delta_value] = {}
        outs_scanned[delta_value] = {}

        _kat = kats_tuned[delta_value]
        kat = _kat.deepcopy()
        kat.verbose = False
        kat.noxaxis = True
        kat.yaxis = 'abs'
        kat.removeBlock("locks", False)    
        dofs = [kat.IFO.DARM, kat.IFO.CARM, kat.IFO.PRCL, kat.IFO.SRCL, kat.IFO.MICH]

        for ii, dof in enumerate(dofs):
            # Create the scan of the DOF we want, and run the scan
            dof_scale = dof_scales[ii]
            scan_cmd = aligo.scan_optics_string(dof.optics, dof.factors, "scan", linlog="lin",
                                                xlimits=np.multiply(dof_scale, xlimits), steps=steps,
                                                axis=1, relative=True)
            kat.removeBlock("SCAN", False)
            kat.parse(scan_cmd, addToBlock="SCAN")
            out = kat.run(cmd_args=['-cr=on'])

            kats_scanned[delta_value][dof.name] = kat
            outs_scanned[delta_value][dof.name] = out



############################################################
#                                                          #
#                         Figures                          #
#                                                          #
############################################################

# Check the lock drag for jumps in the tunings
rows = 2
columns = 3
mirrors = [ base.ETMX,  base.ITMX, base.ETMY, base.ITMY,
            base.SRM,   base.PRM,
            # base.ETMXAR, base.ITMXAR, base.ETMYAR, base.ITMYAR,
            # base.SRMAR,  base.PRMAR
            ]

fig, ss = plt.subplots(rows, columns, sharex=True)

for rr in np.arange(rows):
    for cc in np.arange(columns):
        index = rr + rows * cc
        m = mirrors[index]

        s0 = ss[rr, cc]

        # Plot vertical lines for every injection point
        for ii, delta_value in enumerate(delta_values):
            new_value = original_value + delta_value
            color = f'C{np.mod(ii, 10)}'
            label = r'$\Delta\mathrm{RoC}_\mathrm{SR3}$ = '+f'{delta_value:.3f} {lock_drag_units}'

            s0.axvline(x=new_value, lw=1, ls='--', alpha=0.5, color=color, label=label)

        s0.plot(out1.x + original_value, out1[f'{m.name}_phi'])
        s0.plot(out2.x + original_value, out2[f'{m.name}_phi'])

        s0.tick_params(axis='both', which='major', labelsize=10)

        s0.grid()
        s0.grid(which='minor', ls='--')
        s0.set_title(m.name, fontsize=10)
        if cc == 0:
            s0.set_ylabel('Tuning [deg]', fontsize=10)
        if rr == rows - 1:
            s0.set_xlabel(r'$\Delta\mathrm{RoC}_\mathrm{SR3}$', fontsize=10)

# ss[0, 2].legend(bbox_to_anchor=(1.1, 1.00), fontsize=8) # put legend to the right of the top right
plt.suptitle('Mirror position tuning for a change in ' + r'$\mathrm{RoC}_\mathrm{SR3}$ = %f %s'%(original_value, lock_drag_units), y=1.02)
plt.tight_layout()

# Save the figure
plotname = f'finesse_lock_drag_mirror_tunings_for_{lock_drag_component_name}_{lock_drag_attribute}_change{lock_drag_plot_suffix}.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Amplitude plots of carrier everywhere in IFO
if run_ad_code:
    # Lock drag vs amplitude detector content
    rows = 3
    columns = 3

    fig, ss = plt.subplots(rows, columns, sharex=True)

    for rr in np.arange(rows):
        for cc in np.arange(columns):
            index = rr + rows * cc
            ad_name_suffix = ad_list[index]

            s0 = ss[rr, cc]

            # Plot vertical lines for every injection point
            for ii, delta_value in enumerate(delta_values):
                new_value = original_value + delta_value
                color = f'C{np.mod(ii,10)}'
                label = r'$\Delta\mathrm{RoC}_\mathrm{SR3}$ = '+f'{delta_value:.3f} {lock_drag_units}'

                s0.axvline(x=new_value, lw=1, ls='--', alpha=0.5, color=color)

            temp_out_x = np.hstack((out1.x, out2.x))
            indicies = np.argsort(temp_out_x)
            plot_out_x = temp_out_x[indicies] + original_value

            for ii in range(maxtem_order+1):
                for jj in range(maxtem_order+1):
                    if ii + jj > maxtem_order:
                        continue
                    index2 = jj + (maxtem_order+1) * ii
                    if index2 > 10:
                        ls = '--'
                    else:
                        ls = '-'

                    full_ad_name = f'ad{ii}{jj}_{ad_name_suffix}'
                    temp_out_y = np.hstack((out1[full_ad_name], out2[full_ad_name]))
                    plot_out_y = np.abs( temp_out_y[indicies] )
                    label = f'TEM{ii}{jj}'

                    s0.semilogy(plot_out_x, plot_out_y, ls=ls, alpha=0.5, label=label)

            s0.tick_params(axis='both', which='major', labelsize=10)

            s0.set_yticks(nu.good_ticks(s0))
            s0.grid()
            # s0.grid(which='minor', ls='--')
            s0.set_title(ad_name_suffix.replace('_', ' '), fontsize=10)
            if cc == 0:
                s0.set_ylabel('Mag ' + r'$|E_\mathrm{nm}| [\sqrt{\mathrm{W}}]$ ', fontsize=10)
            if rr == rows - 1:
                s0.set_xlabel(r'$\Delta\mathrm{RoC}_\mathrm{SR3}$', fontsize=10)

    ss[0, 2].legend(bbox_to_anchor=(1.1, 1.00), fontsize=8) # put legend to the right of the top right

    plt.suptitle('HOM amplitudes for a change in ' + r'$\mathrm{RoC}_\mathrm{SR3}$ = %f %s'%(original_value, lock_drag_units), y=1.02)
    plt.tight_layout()

    # Save the figure
    plotname = f'finesse_lock_drag_HOM_amplitudes_for_{lock_drag_component_name}_{lock_drag_attribute}_change{lock_drag_plot_suffix}.pdf'
    full_plotname = '{}/{}'.format(fig_dir, plotname)
    plot_names = np.append(plot_names, full_plotname)
    print('Writing plot PDF to {}'.format(full_plotname))
    plt.savefig(full_plotname, bbox_inches='tight')
    plt.close()



# Amplitude plots of PLUS 45 MHz everywhere in IFO
if run_ad_code:
    # Lock drag vs amplitude detector content
    rows = 3
    columns = 3

    fig, ss = plt.subplots(rows, columns, sharex=True)

    for rr in np.arange(rows):
        for cc in np.arange(columns):
            index = rr + rows * cc
            ad_name_suffix = ad_p45_list[index]

            s0 = ss[rr, cc]

            # Plot vertical lines for every injection point
            for ii, delta_value in enumerate(delta_values):
                new_value = original_value + delta_value
                color = f'C{np.mod(ii,10)}'
                label = r'$\Delta\mathrm{RoC}_\mathrm{SR3}$ = '+f'{delta_value:.3f} {lock_drag_units}'

                s0.axvline(x=new_value, lw=1, ls='--', alpha=0.5, color=color)

            temp_out_x = np.hstack((out1.x, out2.x))
            indicies = np.argsort(temp_out_x)
            plot_out_x = temp_out_x[indicies] + original_value

            for ii in range(maxtem_order+1):
                for jj in range(maxtem_order+1):
                    if ii + jj > maxtem_order:
                        continue
                    index2 = jj + (maxtem_order+1) * ii
                    if index2 > 10:
                        ls = '--'
                    else:
                        ls = '-'

                    full_ad_name = f'ad{ii}{jj}_{ad_name_suffix}'
                    temp_out_y = np.hstack((out1[full_ad_name], out2[full_ad_name]))
                    plot_out_y = np.abs( temp_out_y[indicies] )
                    label = f'TEM{ii}{jj}'

                    s0.semilogy(plot_out_x, plot_out_y, ls=ls, alpha=0.5, label=label)

            s0.tick_params(axis='both', which='major', labelsize=10)

            s0.set_yticks(nu.good_ticks(s0))
            s0.grid()
            # s0.grid(which='minor', ls='--')
            s0.set_title(ad_name_suffix.replace('_', ' '), fontsize=10)
            if cc == 0:
                s0.set_ylabel('Mag ' + r'$|E_\mathrm{nm}| [\sqrt{\mathrm{W}}]$ ', fontsize=10)
            if rr == rows - 1:
                s0.set_xlabel(r'$\Delta\mathrm{RoC}_\mathrm{SR3}$', fontsize=10)

    ss[0, 2].legend(bbox_to_anchor=(1.1, 1.00), fontsize=8) # put legend to the right of the top right

    plt.suptitle('HOM amplitudes for a change in ' + r'$\mathrm{RoC}_\mathrm{SR3}$ = %f %s'%(original_value, lock_drag_units), y=1.02)
    plt.tight_layout()

    # Save the figure
    plotname = f'finesse_lock_drag_plus_45MHz_HOM_amplitudes_for_{lock_drag_component_name}_{lock_drag_attribute}_change{lock_drag_plot_suffix}.pdf'
    full_plotname = '{}/{}'.format(fig_dir, plotname)
    plot_names = np.append(plot_names, full_plotname)
    print('Writing plot PDF to {}'.format(full_plotname))
    plt.savefig(full_plotname, bbox_inches='tight')
    plt.close()



# Amplitude plots of MINUS 45 MHz everywhere in IFO
if run_ad_code:
    # Lock drag vs amplitude detector content
    rows = 3
    columns = 3

    fig, ss = plt.subplots(rows, columns, sharex=True)

    for rr in np.arange(rows):
        for cc in np.arange(columns):
            index = rr + rows * cc
            ad_name_suffix = ad_m45_list[index]

            s0 = ss[rr, cc]

            # Plot vertical lines for every injection point
            for ii, delta_value in enumerate(delta_values):
                new_value = original_value + delta_value
                color = f'C{np.mod(ii,10)}'
                label = r'$\Delta\mathrm{RoC}_\mathrm{SR3}$ = '+f'{delta_value:.3f} {lock_drag_units}'

                s0.axvline(x=new_value, lw=1, ls='--', alpha=0.5, color=color)

            temp_out_x = np.hstack((out1.x, out2.x))
            indicies = np.argsort(temp_out_x)
            plot_out_x = temp_out_x[indicies] + original_value

            for ii in range(maxtem_order+1):
                for jj in range(maxtem_order+1):
                    if ii + jj > maxtem_order:
                        continue
                    index2 = jj + (maxtem_order+1) * ii
                    if index2 > 10:
                        ls = '--'
                    else:
                        ls = '-'

                    full_ad_name = f'ad{ii}{jj}_{ad_name_suffix}'
                    temp_out_y = np.hstack((out1[full_ad_name], out2[full_ad_name]))
                    plot_out_y = np.abs( temp_out_y[indicies] )
                    label = f'TEM{ii}{jj}'

                    s0.semilogy(plot_out_x, plot_out_y, ls=ls, alpha=0.5, label=label)

            s0.tick_params(axis='both', which='major', labelsize=10)

            s0.set_yticks(nu.good_ticks(s0))
            s0.grid()
            # s0.grid(which='minor', ls='--')
            s0.set_title(ad_name_suffix.replace('_', ' '), fontsize=10)
            if cc == 0:
                s0.set_ylabel('Mag ' + r'$|E_\mathrm{nm}| [\sqrt{\mathrm{W}}]$ ', fontsize=10)
            if rr == rows - 1:
                s0.set_xlabel(r'$\Delta\mathrm{RoC}_\mathrm{SR3}$', fontsize=10)

    ss[0, 2].legend(bbox_to_anchor=(1.1, 1.00), fontsize=8) # put legend to the right of the top right

    plt.suptitle('HOM amplitudes for a change in ' + r'$\mathrm{RoC}_\mathrm{SR3}$ = %f %s'%(original_value, lock_drag_units), y=1.02)
    plt.tight_layout()

    # Save the figure
    plotname = f'finesse_lock_drag_minus_45MHz_HOM_amplitudes_for_{lock_drag_component_name}_{lock_drag_attribute}_change{lock_drag_plot_suffix}.pdf'
    full_plotname = '{}/{}'.format(fig_dir, plotname)
    plot_names = np.append(plot_names, full_plotname)
    print('Writing plot PDF to {}'.format(full_plotname))
    plt.savefig(full_plotname, bbox_inches='tight')
    plt.close()



# Amplitude and phase plots of carrier everywhere in IFO
if run_ad_code:
    # Lock drag vs amplitude detector content
    rows = 6
    columns = 3

    fig, ss = plt.subplots(rows, columns, sharex=True, figsize=(12,12))

    for rr in np.arange(rows//2):
        for cc in np.arange(columns):
            index = rr + (rows//2) * cc
            ad_name_suffix = ad_list[index]

            s0 = ss[2*rr, cc]
            s2 = ss[2*rr+1, cc]

            # Plot vertical lines for every injection point
            for ii, delta_value in enumerate(delta_values):
                new_value = original_value + delta_value
                color = f'C{np.mod(ii,10)}'
                label = r'$\Delta\mathrm{RoC}_\mathrm{SR3}$ = '+f'{delta_value:.3f} {lock_drag_units}'

                s0.axvline(x=new_value, lw=1, ls='--', alpha=0.5, color=color)
                s2.axvline(x=new_value, lw=1, ls='--', alpha=0.5, color=color)

            temp_out_x = np.hstack((out1.x, out2.x))
            indicies = np.argsort(temp_out_x)
            plot_out_x = temp_out_x[indicies] + original_value

            for ii in range(maxtem_order+1):
                for jj in range(maxtem_order+1):
                    if ii + jj > maxtem_order:
                        continue
                    index2 = jj + (maxtem_order+1) * ii
                    if index2 > 10:
                        ls = '--'
                    else:
                        ls = '-'

                    full_ad_name = f'ad{ii}{jj}_{ad_name_suffix}'
                    temp_out_y = np.hstack((out1[full_ad_name], out2[full_ad_name]))
                    plot_out_y0 = np.abs(temp_out_y[indicies])
                    plot_out_y2 = np.angle(temp_out_y[indicies], deg=True)
                    label = f'TEM{ii}{jj}'

                    s0.semilogy(plot_out_x, plot_out_y0, ls=ls, alpha=0.5, label=label)
                    s2.plot(plot_out_x, plot_out_y2, ls=ls, alpha=0.5, label=label)

            s0.tick_params(axis='both', which='major', labelsize=10)
            s2.tick_params(axis='both', which='major', labelsize=10)

            s0.set_yticks(nu.good_ticks(s0))
            s0.grid()
            s2.set_yticks([-180, -90, 0, 90, 180])
            s2.grid()
            # s0.grid(which='minor', ls='--')
            s0.set_title(ad_name_suffix.replace('_', ' '), fontsize=10)
            if cc == 0:
                s0.set_ylabel('Mag ' + r'$|E_\mathrm{nm}| [\sqrt{\mathrm{W}}]$ ', fontsize=10)
            if rr == rows//2 - 1:
                s2.set_xlabel(r'$\Delta\mathrm{RoC}_\mathrm{SR3}$', fontsize=10)

    # ss[0, 2].legend(bbox_to_anchor=(1.1, 1.00), fontsize=8) # put legend to the right of the top right

    plt.suptitle('Carrier HOM amplitudes and phase for a change in ' + r'$\mathrm{RoC}_\mathrm{SR3}$ = %f %s'%(original_value, lock_drag_units), y=1.02)
    plt.tight_layout()

    # Save the figure
    plotname = f'finesse_lock_drag_HOM_amplitudes_and_phase_for_{lock_drag_component_name}_{lock_drag_attribute}_change{lock_drag_plot_suffix}.pdf'
    full_plotname = '{}/{}'.format(fig_dir, plotname)
    plot_names = np.append(plot_names, full_plotname)
    print('Writing plot PDF to {}'.format(full_plotname))
    plt.savefig(full_plotname) #, bbox_inches='tight')
    plt.close()



# Amplitude and phase plots of PLUS 45 MHz everywhere in IFO
if run_ad_code:
    # Lock drag vs amplitude detector content
    rows = 6
    columns = 3

    fig, ss = plt.subplots(rows, columns, sharex=True, figsize=(12,12))

    for rr in np.arange(rows//2):
        for cc in np.arange(columns):
            index = rr + (rows//2) * cc
            ad_name_suffix = ad_p45_list[index]

            s0 = ss[2*rr, cc]
            s2 = ss[2*rr+1, cc]

            # Plot vertical lines for every injection point
            for ii, delta_value in enumerate(delta_values):
                new_value = original_value + delta_value
                color = f'C{np.mod(ii,10)}'
                label = r'$\Delta\mathrm{RoC}_\mathrm{SR3}$ = '+f'{delta_value:.3f} {lock_drag_units}'

                s0.axvline(x=new_value, lw=1, ls='--', alpha=0.5, color=color)
                s2.axvline(x=new_value, lw=1, ls='--', alpha=0.5, color=color)

            temp_out_x = np.hstack((out1.x, out2.x))
            indicies = np.argsort(temp_out_x)
            plot_out_x = temp_out_x[indicies] + original_value

            for ii in range(maxtem_order+1):
                for jj in range(maxtem_order+1):
                    if ii + jj > maxtem_order:
                        continue
                    index2 = jj + (maxtem_order+1) * ii
                    if index2 > 10:
                        ls = '--'
                    else:
                        ls = '-'

                    full_ad_name = f'ad{ii}{jj}_{ad_name_suffix}'
                    temp_out_y = np.hstack((out1[full_ad_name], out2[full_ad_name]))
                    plot_out_y0 = np.abs(temp_out_y[indicies])
                    plot_out_y2 = np.angle(temp_out_y[indicies], deg=True)
                    label = f'TEM{ii}{jj}'

                    s0.semilogy(plot_out_x, plot_out_y0, ls=ls, alpha=0.5, label=label)
                    s2.plot(plot_out_x, plot_out_y2, ls=ls, alpha=0.5, label=label)

            s0.tick_params(axis='both', which='major', labelsize=10)
            s2.tick_params(axis='both', which='major', labelsize=10)

            s0.set_yticks(nu.good_ticks(s0))
            s0.grid()
            s2.set_yticks([-180, -90, 0, 90, 180])
            s2.grid()
            # s0.grid(which='minor', ls='--')
            s0.set_title(ad_name_suffix.replace('_', ' '), fontsize=10)
            if cc == 0:
                s0.set_ylabel('Mag ' + r'$|E_\mathrm{nm}| [\sqrt{\mathrm{W}}]$ ', fontsize=10)
            if rr == rows//2 - 1:
                s2.set_xlabel(r'$\Delta\mathrm{RoC}_\mathrm{SR3}$', fontsize=10)

    # ss[0, 2].legend(bbox_to_anchor=(1.1, 1.00), fontsize=8) # put legend to the right of the top right

    plt.suptitle('+45MHz HOM amplitudes for a change in ' + r'$\mathrm{RoC}_\mathrm{SR3}$ = %f %s'%(original_value, lock_drag_units), y=1.02)
    plt.tight_layout()

    # Save the figure
    plotname = f'finesse_lock_drag_plus_45MHz_HOM_amplitudes_and_phase_for_{lock_drag_component_name}_{lock_drag_attribute}_change{lock_drag_plot_suffix}.pdf'
    full_plotname = '{}/{}'.format(fig_dir, plotname)
    plot_names = np.append(plot_names, full_plotname)
    print('Writing plot PDF to {}'.format(full_plotname))
    plt.savefig(full_plotname) #, bbox_inches='tight')
    plt.close()



# Amplitude and phase plots of PLUS 45 MHz everywhere in IFO
if run_ad_code:
    # Lock drag vs amplitude detector content
    rows = 6
    columns = 3

    fig, ss = plt.subplots(rows, columns, sharex=True, figsize=(12,12))

    for rr in np.arange(rows//2):
        for cc in np.arange(columns):
            index = rr + (rows//2) * cc
            ad_name_suffix = ad_m45_list[index]

            s0 = ss[2*rr, cc]
            s2 = ss[2*rr+1, cc]

            # Plot vertical lines for every injection point
            for ii, delta_value in enumerate(delta_values):
                new_value = original_value + delta_value
                color = f'C{np.mod(ii,10)}'
                label = r'$\Delta\mathrm{RoC}_\mathrm{SR3}$ = '+f'{delta_value:.3f} {lock_drag_units}'

                s0.axvline(x=new_value, lw=1, ls='--', alpha=0.5, color=color)
                s2.axvline(x=new_value, lw=1, ls='--', alpha=0.5, color=color)

            temp_out_x = np.hstack((out1.x, out2.x))
            indicies = np.argsort(temp_out_x)
            plot_out_x = temp_out_x[indicies] + original_value

            for ii in range(maxtem_order+1):
                for jj in range(maxtem_order+1):
                    if ii + jj > maxtem_order:
                        continue
                    index2 = jj + (maxtem_order+1) * ii
                    if index2 > 10:
                        ls = '--'
                    else:
                        ls = '-'

                    full_ad_name = f'ad{ii}{jj}_{ad_name_suffix}'
                    temp_out_y = np.hstack((out1[full_ad_name], out2[full_ad_name]))
                    plot_out_y0 = np.abs(temp_out_y[indicies])
                    plot_out_y2 = np.angle(temp_out_y[indicies], deg=True)
                    label = f'TEM{ii}{jj}'

                    s0.semilogy(plot_out_x, plot_out_y0, ls=ls, alpha=0.5, label=label)
                    s2.plot(plot_out_x, plot_out_y2, ls=ls, alpha=0.5, label=label)

            s0.tick_params(axis='both', which='major', labelsize=10)
            s2.tick_params(axis='both', which='major', labelsize=10)

            s0.set_yticks(nu.good_ticks(s0))
            s0.grid()
            s2.set_yticks([-180, -90, 0, 90, 180])
            s2.grid()
            # s0.grid(which='minor', ls='--')
            s0.set_title(ad_name_suffix.replace('_', ' '), fontsize=10)
            if cc == 0:
                s0.set_ylabel('Mag ' + r'$|E_\mathrm{nm}| [\sqrt{\mathrm{W}}]$ ', fontsize=10)
            if rr == rows//2 - 1:
                s2.set_xlabel(r'$\Delta\mathrm{RoC}_\mathrm{SR3}$', fontsize=10)

    # ss[0, 2].legend(bbox_to_anchor=(1.1, 1.00), fontsize=8) # put legend to the right of the top right

    plt.suptitle('-45MHz HOM amplitudes for a change in ' + r'$\mathrm{RoC}_\mathrm{SR3}$ = %f %s'%(original_value, lock_drag_units), y=1.02)
    plt.tight_layout()

    # Save the figure
    plotname = f'finesse_lock_drag_minus_45MHz_HOM_amplitudes_and_phase_for_{lock_drag_component_name}_{lock_drag_attribute}_change{lock_drag_plot_suffix}.pdf'
    full_plotname = '{}/{}'.format(fig_dir, plotname)
    plot_names = np.append(plot_names, full_plotname)
    print('Writing plot PDF to {}'.format(full_plotname))
    plt.savefig(full_plotname) #, bbox_inches='tight')
    plt.close()



# make nine plots of each carrier cavity electric field
if run_ad_code:
    # Lock drag vs amplitude detector content
    rows = 3
    columns = 3

    for rr in np.arange(rows):
        for cc in np.arange(columns):
            index = rr + rows * cc
            ad_name_suffix = ad_list[index]

            fig, s1 = plt.subplots(1, figsize=(12,6))

            # Plot vertical lines for every injection point
            for ii, delta_value in enumerate(delta_values):
                new_value =  delta_value * 1e3 # cal to mm # + original_value
                color = f'C{np.mod(ii,10)}'
                label = r'$\Delta\mathrm{RoC}_\mathrm{SR3}$ = '+f'{delta_value:.3f} {lock_drag_units}'

                s1.axvline(x=new_value, lw=1.5, ls='--', alpha=0.8, color=color)

            temp_out_x = np.hstack((out1.x, out2.x))
            indicies = np.argsort(temp_out_x)
            plot_out_x = temp_out_x[indicies] * 1e3 # calibrate into mm #+ original_value

            for ii in range(maxtem_order+1):
                for jj in range(maxtem_order+1):
                    if ii + jj > maxtem_order:
                        continue
                    index2 = jj + (maxtem_order+1) * ii
                    if index2 > 10:
                        ls = '--'
                    else:
                        ls = '-'

                    full_ad_name = f'ad{ii}{jj}_{ad_name_suffix}'
                    temp_out_y = np.hstack((out1[full_ad_name], out2[full_ad_name]))
                    plot_out_y = np.abs( temp_out_y[indicies] )
                    label = f'TEM{ii}{jj}'

                    s1.semilogy(plot_out_x, plot_out_y, ls=ls, alpha=0.8, label=label)

            s1.legend(bbox_to_anchor=(1.00, 1.00), fontsize=18, title=f'{ad_name_suffix.replace("_", " ")} homs') # put legend to the right of the top right

            s1.tick_params(axis='both', which='major')

            s1.set_yticks(nu.good_ticks(s1))
            s1.grid()
            # s0.grid(which='minor', ls='--')
            # s1.set_title(ad_name_suffix.replace('_', ' '))
            s1.set_ylabel('Electric fields ' + r'$|E_\mathrm{nm}| [\sqrt{\mathrm{W}}]$ ')
            s1.set_xlabel(f'SR3 radius of curvature change '+r'$\Delta \mathrm{RoC}_\mathrm{SR3}$ [mm]')


            # Save the figure
            plotname = f'finesse_lock_drag_HOM_amplitudes_for_{lock_drag_component_name}_{lock_drag_attribute}_change_{lock_drag_plot_suffix}_{ad_name_suffix}.pdf'
            full_plotname = '{}/{}'.format(fig_dir, plotname)
            plot_names = np.append(plot_names, full_plotname)
            print('Writing plot PDF to {}'.format(full_plotname))
            plt.savefig(full_plotname, bbox_inches='tight')
            plt.close()



# make nine plots of each carrier cavity electric field
if run_ad_code:
    # Lock drag vs amplitude detector content
    rows = 3
    columns = 3

    for rr in np.arange(rows):
        for cc in np.arange(columns):
            index = rr + rows * cc
            ad_name_suffix = ad_p45_list[index]

            fig, s1 = plt.subplots(1, figsize=(12,6))

            # Plot vertical lines for every injection point
            for ii, delta_value in enumerate(delta_values):
                new_value =  delta_value * 1e3 # cal to mm # + original_value
                color = f'C{np.mod(ii,10)}'
                label = r'$\Delta\mathrm{RoC}_\mathrm{SR3}$ = '+f'{delta_value:.3f} {lock_drag_units}'

                s1.axvline(x=new_value, lw=1.5, ls='--', alpha=0.8, color=color)

            temp_out_x = np.hstack((out1.x, out2.x))
            indicies = np.argsort(temp_out_x)
            plot_out_x = temp_out_x[indicies] * 1e3 # calibrate into mm #+ original_value

            for ii in range(maxtem_order+1):
                for jj in range(maxtem_order+1):
                    if ii + jj > maxtem_order:
                        continue
                    index2 = jj + (maxtem_order+1) * ii
                    if index2 > 10:
                        ls = '--'
                    else:
                        ls = '-'

                    full_ad_name = f'ad{ii}{jj}_{ad_name_suffix}'
                    temp_out_y = np.hstack((out1[full_ad_name], out2[full_ad_name]))
                    plot_out_y = np.abs( temp_out_y[indicies] )
                    label = f'TEM{ii}{jj}'

                    s1.semilogy(plot_out_x, plot_out_y, ls=ls, alpha=0.8, label=label)

            s1.legend(bbox_to_anchor=(1.00, 1.00), fontsize=18, title=f'{ad_name_suffix.replace("_", " ")} homs') # put legend to the right of the top right

            s1.tick_params(axis='both', which='major')

            s1.set_yticks(nu.good_ticks(s1))
            s1.grid()
            # s0.grid(which='minor', ls='--')
            # s1.set_title(ad_name_suffix.replace('_', ' '))
            s1.set_ylabel('Electric fields ' + r'$|E_\mathrm{nm}| [\sqrt{\mathrm{W}}]$ ')
            s1.set_xlabel(f'SR3 radius of curvature change '+r'$\Delta \mathrm{RoC}_\mathrm{SR3}$ [mm]')


            # Save the figure
            plotname = f'finesse_lock_drag_HOM_amplitudes_for_{lock_drag_component_name}_{lock_drag_attribute}_change_{lock_drag_plot_suffix}_{ad_name_suffix}.pdf'
            full_plotname = '{}/{}'.format(fig_dir, plotname)
            plot_names = np.append(plot_names, full_plotname)
            print('Writing plot PDF to {}'.format(full_plotname))
            plt.savefig(full_plotname, bbox_inches='tight')
            plt.close()



# make nine plots of each MINUS 45 MHz cavity electric field
if run_ad_code:
    # Lock drag vs amplitude detector content
    rows = 3
    columns = 3

    for rr in np.arange(rows):
        for cc in np.arange(columns):
            index = rr + rows * cc
            ad_name_suffix = ad_m45_list[index]

            fig, s1 = plt.subplots(1, figsize=(12,6))

            # Plot vertical lines for every injection point
            for ii, delta_value in enumerate(delta_values):
                new_value =  delta_value * 1e3 # cal to mm # + original_value
                color = f'C{np.mod(ii,10)}'
                label = r'$\Delta\mathrm{RoC}_\mathrm{SR3}$ = '+f'{delta_value:.3f} {lock_drag_units}'

                s1.axvline(x=new_value, lw=1.5, ls='--', alpha=0.8, color=color)

            temp_out_x = np.hstack((out1.x, out2.x))
            indicies = np.argsort(temp_out_x)
            plot_out_x = temp_out_x[indicies] * 1e3 # calibrate into mm #+ original_value

            for ii in range(maxtem_order+1):
                for jj in range(maxtem_order+1):
                    if ii + jj > maxtem_order:
                        continue
                    index2 = jj + (maxtem_order+1) * ii
                    if index2 > 10:
                        ls = '--'
                    else:
                        ls = '-'

                    full_ad_name = f'ad{ii}{jj}_{ad_name_suffix}'
                    temp_out_y = np.hstack((out1[full_ad_name], out2[full_ad_name]))
                    plot_out_y = np.abs( temp_out_y[indicies] )
                    label = f'TEM{ii}{jj}'

                    s1.semilogy(plot_out_x, plot_out_y, ls=ls, alpha=0.8, label=label)

            s1.legend(bbox_to_anchor=(1.00, 1.00), fontsize=18, title=f'{ad_name_suffix.replace("_", " ")} homs') # put legend to the right of the top right

            s1.tick_params(axis='both', which='major')

            s1.set_yticks(nu.good_ticks(s1))
            s1.grid()
            # s0.grid(which='minor', ls='--')
            # s1.set_title(ad_name_suffix.replace('_', ' '))
            s1.set_ylabel('Electric fields ' + r'$|E_\mathrm{nm}| [\sqrt{\mathrm{W}}]$ ')
            s1.set_xlabel(f'SR3 radius of curvature change '+r'$\Delta \mathrm{RoC}_\mathrm{SR3}$ [mm]')


            # Save the figure
            plotname = f'finesse_lock_drag_HOM_amplitudes_for_{lock_drag_component_name}_{lock_drag_attribute}_change_{lock_drag_plot_suffix}_{ad_name_suffix}.pdf'
            full_plotname = '{}/{}'.format(fig_dir, plotname)
            plot_names = np.append(plot_names, full_plotname)
            print('Writing plot PDF to {}'.format(full_plotname))
            plt.savefig(full_plotname, bbox_inches='tight')
            plt.close()



# Plot AS_DC components and total, this value should be controlled by DARM
fig, s1 = plt.subplots(1)

# Plot vertical lines for every injection point
for ii, delta_value in enumerate(delta_values):
    new_value =  delta_value * 1e3 # cal to mm # + original_value
    color = f'C{np.mod(ii,10)}'
    label = r'$\Delta\mathrm{RoC}_\mathrm{SR3}$ = '+f'{delta_value:.3f} {lock_drag_units}'

    s1.axvline(x=new_value, lw=1, ls='--', alpha=0.5, color=color)

temp_out_x = np.hstack((out1.x, out2.x))
indicies = np.argsort(temp_out_x)
plot_out_x = temp_out_x[indicies] * 1e3 # calibrate into mm #+ original_value

ad_name_suffix = 'as'

total_power = np.zeros_like(plot_out_x)
for ii in range(maxtem_order+1):
    for jj in range(maxtem_order+1):
        if ii + jj > maxtem_order:
            continue
        index2 = jj + (maxtem_order+1) * ii
        if index2 > 10:
            ls = '--'
        else:
            ls = '-'

        full_ad_name = f'ad{ii}{jj}_{ad_name_suffix}'
        temp_out_y = np.hstack((out1[full_ad_name], out2[full_ad_name]))
        plot_out_y = temp_out_y[indicies] 
        plot_out_y2 = np.abs(plot_out_y)**2
        label = f'TEM{ii}{jj}'

        if not label == 'TEM00':
            total_power += plot_out_y2

        s1.semilogy(plot_out_x, plot_out_y2, ls=ls, alpha=0.5, label=label)

s1.semilogy(plot_out_x, total_power, ls='--', color='k', alpha=0.7, label='Total TEMnm power')

full_pd_name = 'AS_DC'
temp_out_y = np.hstack((out1[full_pd_name], out2[full_pd_name]))
plot_out_y = np.abs( temp_out_y[indicies] )

s1.semilogy(plot_out_x, plot_out_y, ls='--', color='red', alpha=0.7, label='AS DC')


s1.legend(bbox_to_anchor=(1.00, 1.00), fontsize=14, title=f'{ad_name_suffix.replace("_", " ")} homs') # put legend to the right of the top right
s1.tick_params(axis='both', which='major')

s1.set_yticks(nu.good_ticks(s1))
s1.grid()
# s0.grid(which='minor', ls='--')
# s1.set_title(ad_name_suffix.replace('_', ' '))
s1.set_ylabel('AS DC power ' + r'$P_\mathrm{nm} [\mathrm{W}]$ ')
s1.set_xlabel(f'SR3 radius of curvature change '+r'$\Delta \mathrm{RoC}_\mathrm{SR3}$ [mm]')


# Save the figure
plotname = f'finesse_lock_drag_AS_DC_power_for_{lock_drag_component_name}_{lock_drag_attribute}_change_{lock_drag_plot_suffix}_{ad_name_suffix}.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Plot the DARM TF in watts/meter for several parts of the lock
fig, (s1,s2) = plt.subplots(2, sharex=True)

for ii, delta_value in enumerate(delta_values):
    # plot_kat = kats_tuned[delta_value]
    plot_out = outs_tuned[delta_value]

    arm_power_x = max(np.abs(plot_out['P_X']))
    arm_power_y = max(np.abs(plot_out['P_Y']))
    arm_power = (arm_power_x + arm_power_y)/2

    label = r'$\Delta$%s'%(lock_drag_plot_label) + f' = {1e3*delta_value:.0f} mm' #' (arms = {arm_power:.0f} W)'
    alpha = 0.5

    # Adjust finesse darm to have correct sign
    plot_finesse_TF = plot_out['AS'] * np.exp(1j * np.pi)

    # Adjust to avoid phase wrapping
    plot_finesse_TF_phase = np.angle(plot_finesse_TF * np.exp(-1j * np.pi / 2), deg=True)
    plot_finesse_TF_phase += 90

    s1.loglog(plot_out.x, np.abs(plot_finesse_TF), alpha=alpha, label=label)
    s2.semilogx(plot_out.x, plot_finesse_TF_phase, alpha=alpha, label=label)

meas_TF_phase = np.angle(meas_TF * np.exp(-1j * np.pi / 2), deg=True)
meas_TF_phase += 90

s1.loglog(meas_ff, np.abs(meas_TF), '.', color='k', label='LHO Meas - August 19 2019')
s2.semilogx(meas_ff, meas_TF_phase, '.', color='k', label='LHO Meas - August 19 2019')

# s1.set_title(f'DARM TF with changes to ' + r'$\mathrm{RoC}_\mathrm{SR3}$ = %f %s'%(original_value, lock_drag_units))
s1.set_ylabel('Mag [W/m]')
s2.set_ylabel('Phase [degs]')
s2.set_xlabel('Frequency [Hz]')

s2.set_ylim([-90, 270])

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(90*np.arange(-1, 4))

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.5)

s2.legend(fontsize=12, ncol=2)

# Save the figure
plotname = f'finesse_lock_drag_DARM_TFs_for_{lock_drag_component_name}_{lock_drag_attribute}_change{lock_drag_plot_suffix}.pdf'
full_plotname = '{}/{}'.format(fig_dir, plotname)
plot_names = np.append(plot_names, full_plotname)
print('Writing plot PDF to {}'.format(full_plotname))
plt.savefig(full_plotname, bbox_inches='tight')
plt.close()



# Finesse error signals
if run_scan_code:
    rows = 2
    columns = 3

    fig, ss = plt.subplots(rows, columns, sharex=False)
    fig2, ss2 = plt.subplots(rows, columns, sharex=False)
    plt.delaxes(ss[rows-1, columns-1]) # delete the final subplot
    plt.delaxes(ss2[rows-1, columns-1]) # delete the final subplot

    for jj, delta_value in enumerate(delta_values):
        _kat = kats_tuned[delta_value]

        for ii, dof in enumerate(dofs):
            # get the simulation results
            plot_out = outs_scanned[delta_value][dof.name]

            # get index of the correct subplot
            rr = ii // columns
            cc = np.mod(ii, columns)
            s0 = ss[rr, cc]
            s02 = ss2[rr, cc]

            # Set the DC offset
            DC_Offset = None
            if (dof.name + '_lock') in _kat.commands:
                DC_Offset = _kat.commands[dof.name + '_lock'].offset

            if DC_Offset is None:
                DC_Offset = 0
            else:
                DC_Offset = float(DC_Offset)

            # Find how the lock drag has changed the lock point from nominal
            lock_drag_x = 0
            if dof.name == 'SRCL':
                verbose = True
            else:
                verbose = False

            if verbose:
                print()
                print(f'dof = {dof.name}')
            for optic, factor in zip(dof.optics, dof.factors):
                if verbose:
                    print(f'optic = {optic}    factor = {factor}')
                lock_drag_x += factor * tunes_dict[optic](delta_value)
            if dof.name == 'DARM':
                optic = 'ITMX'
                factor = 1
                lock_drag_x += factor * tunes_dict[optic](delta_value)
                optic = 'ITMY'
                factor = -1
                lock_drag_x += factor * tunes_dict[optic](delta_value)
            if verbose:
                print(f'lock_drag_x = {lock_drag_x}')

            # Translate the scan via how the lock drag moved the lock point
            scan_x = plot_out.x + lock_drag_x
            scan_y = np.real(plot_out[dof.signal_name()] + DC_Offset)

            # Find the maximum arm power during the scan
            arm_power_x = max(np.abs(plot_out['P_X']))
            arm_power_y = max(np.abs(plot_out['P_Y']))
            max_arm_power = (arm_power_x + arm_power_y)/2

            # Label and plot the scan results
            alpha = 0.5
            label = r'$\Delta$%s'%(lock_drag_plot_label) + f' = {1e3*delta_value:.0f} mm' #' (arms = {arm_power:.0f} W)'

            s0.plot(scan_x, scan_y, alpha=alpha, label=label)

            # Zoom in and plot the scan results again
            mid = steps//2
            low = mid - 3
            high = mid + 3

            # srcl_func = sci.interp1d(scan_x[low:high], scan_y[low:high])
            # zero_cross = srcl_func(0)
            label2 = r'$\Delta$%s'%(lock_drag_plot_label) + f' = {1e3*delta_value:.0f} mm' #' (arms = {arm_power:.0f} W)'

            s02.plot(scan_x, scan_y, alpha=alpha, label=label2)

    xlims = {}
    xlims['DARM'] = [-0.02, 0.02] # degs
    xlims['CARM'] = [-0.01, 0.01] # degs
    xlims['PRCL'] = [-1, 1]
    xlims['SRCL'] = [90 - 2, 90 + 2]
    xlims['MICH'] = [-0.5, 0.5]

    ylims = {}
    ylims['DARM'] = [-0.2, 3] 
    ylims['CARM'] = [-3, 3] 
    ylims['PRCL'] = [-0.025, 0.025]
    ylims['SRCL'] = [-0.01, 0.01]
    ylims['MICH'] = [-0.01, 0.01]
    for ii, dof in enumerate(dofs):
        # get index of the correct subplot
        rr = ii // columns
        cc = np.mod(ii, columns)
        s0 = ss[rr, cc]
        s02 = ss2[rr, cc]

        s0.set_xlabel("{} [deg]".format(dof.name))
        s0.grid(True)
        s0.tick_params(axis='both', which='major', labelsize=12)
        s0.xaxis.label.set_size(12)
        s0.yaxis.label.set_size(12)

        s02.set_xlim(xlims[dof.name])
        s02.set_ylim(ylims[dof.name])

        s02.set_xlabel("{} [deg]".format(dof.name))
        s02.grid(True)
        s02.tick_params(axis='both', which='major', labelsize=12)
        s02.xaxis.label.set_size(12)
        s02.yaxis.label.set_size(12)

    s02.legend(bbox_to_anchor=(1.05, 1), loc='upper left', fontsize=14) # put legend to the right of the final plot
    s0.legend(bbox_to_anchor=(1.1, 1.00), fontsize=14) # put legend to the right of the final plot
    
    plt.figure(fig.number) # select the first plot, then save it
    # plt.suptitle('Finesse error signal scans ' + r'$\mathrm{RoC}_\mathrm{SR3}$ = %f %s'%(original_value, lock_drag_units), y=0.98)
    # plt.tight_layout()
    # Save the figure
    plotname = f'finesse_error_signals_for_{lock_drag_component_name}_{lock_drag_attribute}_change{lock_drag_plot_suffix}.pdf'
    full_plotname = '{}/{}'.format(fig_dir, plotname)
    plot_names = np.append(plot_names, full_plotname)
    print('Writing plot PDF to {}'.format(full_plotname))
    plt.savefig(full_plotname, bbox_inches='tight')
    plt.close()

    plt.figure(fig2.number) # select the second plot, then save it
    # plt.suptitle('Finesse error signal scans ' + r'$\mathrm{RoC}_\mathrm{SR3}$ = %f %s'%(original_value, lock_drag_units), y=0.98)
    # plt.tight_layout()
    # Save the figure
    plotname = f'finesse_error_signals_zoomed_for_{lock_drag_component_name}_{lock_drag_attribute}_change{lock_drag_plot_suffix}.pdf'
    full_plotname = '{}/{}'.format(fig_dir, plotname)
    plot_names = np.append(plot_names, full_plotname)
    print('Writing plot PDF to {}'.format(full_plotname))
    plt.savefig(full_plotname, bbox_inches='tight')
    plt.close()



# End
print()
print('Plotnames')
print('open', end=' ')
for pn in plot_names:
    print('{}'.format(pn), end=' ')
print()