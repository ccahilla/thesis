'''
refl_carm_calibration.py

Makes frequency noisebudget for the o3 paper

Craig Cahillane
August 7, 2020
'''

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.optimize as sco
import scipy.constants as scc
import scipy.special as scp
import os

import nds2utils as nu
from nds2utils.make_interactive_svg import make_interactive_svg_multiple_subplots

# plt.style.use('tableau-colorblind10')

mpl.rcParams.update({'text.usetex': True,
                     'figure.figsize': (12, 9),
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9,
                     'agg.path.chunksize': 10000})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])


###   Channel dictonary###   
chans = {
'REFLA': 'H1:LSC-REFL_A_RF9_I_ERR',
'REFLB': 'H1:LSC-REFL_B_RF9_I_ERR',
'MC2':   'H1:SUS-MC2_M3_DRIVEALIGN_L2L_EXC',
'IMCF':  'H1:IMC-F_OUT_DQ',
}

###   
### FSS_OLG = F
###   
data_FSS_OLG = np.loadtxt(f'{data_dir}/20190222_FSS_OLG_CommonGain_28_FastGain_15.txt')
FSS_OLG_ff = data_FSS_OLG[:,0]
FSS_OLG_TF = -1 * data_FSS_OLG[:,1]/nu.dB2Mag(5.0) * np.exp(1j*np.pi/180.0*data_FSS_OLG[:,2]) # divide by 5 dB since FSS optical gain is changing
FSS_SUP_TF = 1 / (1 - FSS_OLG_TF)
FSS_CLG_TF = FSS_OLG_TF / (1 - FSS_OLG_TF)

#
# IMC_OLG = II V + C P H V
#'''
data_IMC_OLG = np.loadtxt(f'{data_dir}/20190423_214142_IMC_OLG_FullLock_35W_Input_0dBm_Exc_12dB_CMBIN1Gain.txt')
IMC_OLG_ff = data_IMC_OLG[:,0]
IMC_OLG_TF = -1 * data_IMC_OLG[:,1] * np.exp(1j*np.pi/180.0*data_IMC_OLG[:,2])
IMC_SUP_TF = 1 / (1 - IMC_OLG_TF)

#
# IMC_alone_OLG = I V
#
data_IMC_alone_OLG = np.loadtxt(f'{data_dir}/20190423_134854_20190423_IMC_alone_OLG_35W_IN1_22dB_Boost2On_0dBm_exc.txt')
IMC_alone_OLG_ff = data_IMC_alone_OLG[:,0]
IMC_alone_OLG_TF = -1 * data_IMC_alone_OLG[:,1] * np.exp(1j*np.pi/180.0*data_IMC_alone_OLG[:,2])

#
# CARM_OLG = C P H V / (1 - I V)
#
data_CARM_OLG = np.loadtxt(f'{data_dir}/20190423_214036_20190423_CARM_OLG_FullLock_35W_Input_60mV_Exc_12dB_CMBIN1Gain.txt')
CARM_OLG_ff = data_CARM_OLG[:,0]
CARM_OLG_TF = -1 * data_CARM_OLG[:,1] * np.exp(1j*np.pi/180.0*data_CARM_OLG[:,2])

#
# IMC_alone_SUP = 1 / (1 - I V)
#
IMC_alone_SUP_nonInterp = 1 / (1 - IMC_alone_OLG_TF)
IMC_alone_SUP = nu.get_complex_interp( CARM_OLG_ff,
                                    IMC_alone_OLG_ff,
                                    IMC_alone_SUP_nonInterp)
#
# IMC_alone_SUP = I V / (1 - I V)
#
IMC_alone_CLG_nonInterp = IMC_alone_OLG_TF / (1 - IMC_alone_OLG_TF)
IMC_alone_CLG = nu.get_complex_interp( CARM_OLG_ff,
                                    IMC_alone_OLG_ff,
                                    IMC_alone_CLG_nonInterp)

#
# CARM_alone_OLG = C P H V
#
CARM_alone_OLG_ff = CARM_OLG_ff
CARM_alone_OLG = CARM_OLG_TF / IMC_alone_SUP

#
# CARM_SUP = (1 - I V) / (1 - I V - C P H V)
#
CARM_SUP = 1 / (1 - CARM_OLG_TF)
est_IMC_full_lock_SUP = IMC_alone_SUP * CARM_SUP

est_IMC_full_lock_ff = CARM_OLG_ff
est_IMC_full_lock_OLG = 1 - 1/est_IMC_full_lock_SUP

#
# IMC_OLG_over_SUP = I V / (1 - I V - C P H V)
#
est_IMC_OLG_over_total_SUP_ff = CARM_OLG_ff
est_IMC_OLG_over_total_SUP_TF = CARM_SUP * IMC_alone_CLG # IMC alone OLG/(1 - Total OLG)

#
# CARM Loop Analog Filters
# C = CARM plant in W/Hz
# P = IMC passive pole in Hz/Hz
# H = CARM sensing change + CARM-only analog filters in V/W and V/V
# V = VCO + CARM and IMC analog filters in Hz/V and V/V
# 
# fflog = np.logspace(np.log10(1e3), np.log10(1e6), 500)

ff = CARM_OLG_ff


REFLA9I_sensing_chain = 1887.7 # V/W
SumNodeGain = +8.0 # dB
REFL_IN1Gain = +12 # dB, normally +6 dB for split sensing, see REFL_IN1Gain2
REFL_FASTGain = +16 # dB
IMC_IN2Gain = -22 # dB, # value as of April

def REFL_Boost1(fff):
    ''' Pole at 10 Hz, Zero at 500 Hz '''
    return nu.tf_zpk(fff, [500], [10], gain=500/10)
def REFL_Comp(fff):
    ''' Pole at 40 Hz, Zero at 4000 Hz '''
    return nu.tf_zpk(fff, [4000], [40], gain=4000/40)
def REFL_Fast(fff):
     ''' Two passive 5 Hz high pass filters '''
     return nu.tf_zpk(fff, [0,0], [5,5], gain=1/5**2)

###    H is the CARM only analog filters and REFL A sensing chain###   
def H(fff):
    hh  = REFLA9I_sensing_chain \
        * nu.dB2Mag(SumNodeGain) \
        * nu.dB2Mag(REFL_IN1Gain) \
        * nu.dB2Mag(REFL_FASTGain) \
        * nu.dB2Mag(IMC_IN2Gain) \
        * REFL_Fast(fff) \
        * REFL_Boost1(fff) \
        * REFL_Comp(fff)
    return hh

###    P is the IMC trans passive pole (gain of 1, pole at 8 kHz)###   
IMC_pole = 8.6e3 # Hz
def P(fff):
    '''IMC passive pole at 8.6 kHz'''
    return nu.tf_zpk(fff, [], [IMC_pole], gain=1)

###    I is the IMC plant in W/Hz ###   
Pin = 35.0 # W
L_IMC = 32.9434 # m
FSR = scc.c / L_IMC # Hz
T_MC1 = 6030e-6
T_MC3 = 5845e-6
r_MC1 = np.sqrt(1 - T_MC1)
r_MC3 = np.sqrt(1 - T_MC3)
mod_index_IMC = 0.0125 # rad
estimated_IMC_plant_gain = 4 * np.pi * scp.j0(mod_index_IMC) * scp.j1(mod_index_IMC) * Pin * r_MC3 * T_MC1 \
                         / (FSR * (1 - r_MC1 * r_MC3)**2)
print()
print(f'estimated_IMC_plant_gain = {estimated_IMC_plant_gain} W/Hz')
print()

mystery_IMC_gain = 9e-4 # probably power dumpage, W/W
IMC_plant_gain = estimated_IMC_plant_gain # W/Hz
IMC_REFL_sensing_chain = 1565.0 #3622./2 # V/W
IMC_IN1Gain = 22.0 # dB, this is +2 dB in full lock, but for the IMC Alone measurement this was +22 dB 
def I(fff):
    ''' IMC optical response times the sensing chain gain times some mystery gain factor of 1e-2'''
    ii  = mystery_IMC_gain \
        * nu.dB2Mag(IMC_IN1Gain) \
        * IMC_plant_gain \
        * IMC_REFL_sensing_chain \
        * P(fff)
    return ii

###    V is the VCO and analog filters after the CARM and IMC sum point ###   

IMC_fastdelay = 1.25e-6 # s
def IMC_fastdelayTF(fff):
    '''Delay of the CARM loop'''
    return np.exp(-1j*2*np.pi* IMC_fastdelay * fff)

IMC_FASTGain = -18 # dB
IMC_AOM_doublepass = 2.0 # Hz/Hz
IMC_VCO_volts_to_hertz = 268302.0 # Hz/Vfast, # Evan had 536604 Hz/V for the doublepass

def IMC_VCO(fff):
    ''' Pole at 1.6 Hz, Zero at 40 Hz '''
    return nu.tf_zpk(fff, [40.], [1.6], gain=1)
def IMC_Boost1(fff):
    ''' Pole at 1 kHz, Zero at 20 kHz '''
    return nu.tf_zpk(fff, [20e3], [1e3], gain=20e3/1e3)
def IMC_Boost2(fff):
    ''' Pole at 1 kHz, Zero at 20 kHz '''
    return nu.tf_zpk(fff, [20e3], [1e3], gain=20e3/1e3)
def IMC_Comp(fff):
    ''' Pole at 40 Hz, Zero at 4000 Hz '''
    return nu.tf_zpk(fff, [4e3], [4e1], gain=4e3/4e1)
def IMC_Fast(fff):
    ''' IMC Fast Option + High Pass Filter '''
    fastzeros = np.array([70.0e3])
    fastpoles = np.array([200e3, 2.4e6, 2.4e6, 140.0e3])
    fastgain = 1
    fastgain *= 2.276  # idk what's in the fast option, but it's on...
    # https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=29735
    return nu.tf_zpk(fff, fastzeros, fastpoles, gain=fastgain)

def V(fff):
    vv  = IMC_Comp(fff) \
        * IMC_Boost1(fff) \
        * IMC_Boost2(fff) \
        * IMC_Fast(fff) \
        * nu.dB2Mag(IMC_FASTGain) \
        * IMC_AOM_doublepass \
        * IMC_VCO_volts_to_hertz \
        * IMC_VCO(fff) \
        * IMC_fastdelayTF(fff)
    return vv

###    C is the CARM plant in W/Hz ###   
CARM_plant_gain = -3.37e-3 # W/Hz
CARM_pole = 0.6 # Hz
def C(fff):
    cc = nu.tf_zpk(fff, [], [CARM_pole], gain=CARM_plant_gain)
    return cc

C_meas = CARM_alone_OLG / ( P(ff) * H(ff) * V(ff) )


###   REFLA/MC2###   
dataA = np.loadtxt(f'{data_dir}/MC2_to_REFLA.txt')
ffA = dataA[:,0]
TFA = dataA[:,1] * np.exp(1j * dataA[:,2])
cohA = dataA[:,3]

###   REFLB/MC2###   
dataB = np.loadtxt(f'{data_dir}/MC2_to_REFLB.txt')
ffB = dataB[:,0]
TFB = dataB[:,1] * np.exp(1j * dataB[:,2])
cohB = dataB[:,3]

###   IMCF/MC2###   
dataI2 = np.loadtxt(f'{data_dir}/MC2_to_IMCF_full_lock.txt')
ffI2 = dataI2[:,0]
TFI2 = dataI2[:,1] * np.exp(1j * dataI2[:,2])
cohI2 = dataI2[:,3]

###   IMCF/MC2###   
dataI = np.loadtxt(f'{data_dir}/MC2_to_IMCF.txt')
ffI = dataI[:,0]
TFI = dataI[:,1] * np.exp(1j * dataI[:,2])
cohI = dataI[:,3]

###   Fit IMCF/MC2 with a 1/f^2 * np.exp(2j pi f time_delay)###   
def simpleSusFit(fff, alpha, delay):
    '''Return a function of frequency fitting the model alpha/f^2 * np.exp(2j*pi*f*delay)'''
    return alpha/fff**2 * np.exp(2j*np.pi*fff*delay)

fflog = np.logspace(np.log10(min(ffI)), np.log10(max(ffI)), 100)
index = 30
alpha = np.abs(TFI[index]) * ffI[index]**2
delay = np.angle(TFI[index]) / (2 * np.pi * ffI[index])
fitI = simpleSusFit(fflog, alpha, delay)

###   Find the slope of the Full Lock IMC Suppression between 3 and 15 kHz###   
def slopeFit(fff, alpha, slope):
    '''Returns a function of frequency fitting alpha * fff^beta'''
    return alpha * fff**slope
low_bw = 3000
high_bw = 15000
low_index = np.argwhere(est_IMC_full_lock_ff > low_bw)[0][0]
high_index = np.argwhere(est_IMC_full_lock_ff > high_bw)[0][0]

popt, pcov = sco.curve_fit( slopeFit,
                            est_IMC_full_lock_ff[low_index:high_index],
                            np.abs(est_IMC_full_lock_SUP[low_index:high_index]),
                            p0=(1e-19, 4.0))
fflog2 = np.logspace(np.log10(est_IMC_full_lock_ff[0]), np.log10(30000), 100)
IMC_SUP_TF_fit = slopeFit(fflog2, *popt)

###   Find the slope of the IMC OLG * Total Suppression between 3 and 15 kHz###   
low_bw = 3000
high_bw = 15000
low_index = np.argwhere(est_IMC_OLG_over_total_SUP_ff > low_bw)[0][0]
high_index = np.argwhere(est_IMC_OLG_over_total_SUP_ff > high_bw)[0][0]

popt2, pcov2 = sco.curve_fit( slopeFit,
                            est_IMC_OLG_over_total_SUP_ff[low_index:high_index],
                            np.abs(est_IMC_OLG_over_total_SUP_TF[low_index:high_index]),
                            p0=(1e-10, 2.0))
fflog3 = np.logspace(np.log10(est_IMC_OLG_over_total_SUP_ff[0]), np.log10(30000), 100)
est_IMC_OLG_over_total_SUP_TF_fit = slopeFit(fflog3, *popt2)

###   Find slope of C, the modeled CARM plant###   
low_bw = 3000
high_bw = 10000
low_index = np.argwhere(ff > low_bw)[0][0]
high_index = np.argwhere(ff > high_bw)[0][0]

popt3, pcov3 = sco.curve_fit( slopeFit,
                            ff[low_index:high_index],
                            np.abs(C_meas[low_index:high_index]),
                            p0=(1e-2, 1.0))
fflog4 = np.logspace(0, 6, 500)
est_C = slopeFit(fflog4, *popt3)

# Calibrate CARM
# using REFL/INJ = (IMCF/INJ) * CARM * FULL IMC SUPPRESSION
# or, CARM = (REFL/INJ)/(IMCF/INJ)/(FULL IMC SUPPRESSION)

# Recall that IMCF is in kHz, so
# 
kHz_to_Hz = 1000.0 # Hz/kHz
REFLA9Idigital_to_analog = 1.0  # 3.914e-3/4.969 # V/cts
REFLA9I_sensing_chain = 1887.7  # V/W # 4285./2 # V/W

REFLB9Idigital_to_analog = 1.0  # 3.914e-3/4.969 # V/cts
REFLB9I_sensing_chain = 1853.4  # V/W # 4208./2 # V/W

DiffAmpGain = 2.0 # mag, gain from after the demod board

# lam = 1064e-9 # m
# R = scc.e * lam / (scc.h * scc.c) # ~ 0.86 A/W
# QE = 0.9 # Quantum efficiency

CARM_Plant_ff = ffA
# W/Hz = (REFL A cts/MC2 cts) * (MC2 cts/kHz) * (V/V) * (kHz/Hz) * (V/cts) * (W/V)
CARM_Plant_TF = TFA / simpleSusFit(CARM_Plant_ff, alpha, delay) / slopeFit(CARM_Plant_ff, *popt2) \
                / kHz_to_Hz * REFLA9Idigital_to_analog / REFLA9I_sensing_chain / DiffAmpGain

CARM_Plant_ffB = ffB
# W/Hz = (REFL A cts/MC2 cts) * (MC2 cts/kHz) * (kHz/Hz) * (V/cts) * (W/V)
CARM_Plant_TFB = TFB / simpleSusFit(CARM_Plant_ff, alpha, delay) / slopeFit(CARM_Plant_ff, *popt2) \
                / kHz_to_Hz * REFLB9Idigital_to_analog / REFLB9I_sensing_chain / DiffAmpGain

###    Full CARM loop model (C P H V) with estimated CARM DC gain ###   

CARM_loop_model = C(fflog4) * P(fflog4) * H(fflog4) * V(fflog4)


#####   Figures   #####   
plotted_figures = np.array([])

fig, (s1,s2,s3) = plt.subplots(3)

s1.loglog(ffA, np.abs(TFA), 'o', label='REFL A 9I/SUS MC2 INJ')
s2.semilogx(ffA, 180/np.pi*np.angle(TFA), 'o')
s3.semilogx(ffA, cohA, 'o')

s1.loglog(ffB, np.abs(TFB), 's', label='REFL B 9I/SUS MC2 INJ')
s2.semilogx(ffB, 180/np.pi*np.angle(TFB), 's')
s3.semilogx(ffB, cohB, 's')

s1.loglog(ffI2, np.abs(TFI2), 'v', label='IMC F/SUS MC2 Full Lock [kHz/cts]')
s2.semilogx(ffI2, 180/np.pi*np.angle(TFI2), 'v')
s3.semilogx(ffI2, cohI2, 'v')

s1.loglog(fflog, np.abs(fitI), label='Fit IMC F/SUS MC2 IMC Alone [kHz/cts]')
s2.semilogx(fflog, 180/np.pi*np.angle(fitI))

s1.loglog(ffI, np.abs(TFI), '^', label='IMC F/SUS MC2 IMC Alone [kHz/cts]')
s2.semilogx(ffI, 180/np.pi*np.angle(TFI), '^')
s3.semilogx(ffI, cohI, '^')


s1.set_ylabel('TF Mag')
s2.set_ylabel('TF Phase [degs]')
s3.set_ylabel('Power Coherence')
s3.set_xlabel('Frequency [Hz]')

s1.grid()
s2.grid()
s3.grid()
s1.grid(which='minor', ls='--')
s2.grid(which='minor', ls='--')
s3.grid(which='minor', ls='--')

s1.legend()

plotName = 'refl_tfs.pdf'
fullPlotName = f'{fig_dir}/{plotName}'
plotted_figures = np.append(plotted_figures, fullPlotName)
print('Writing plot PDF to {}'.format(fullPlotName))
plt.savefig(fullPlotName, bbox_inches='tight')
plt.close()


###   IMC Loop Alone (I V F/(1 -F)), F is FSS###   

fig, (s1,s2) = plt.subplots(2)

s1.loglog(IMC_alone_OLG_ff, np.abs(IMC_alone_OLG_TF), label='Measured IMC loop alone '+r'$I V (\mathrm{CLG_{refcav}})$')
s2.semilogx(IMC_alone_OLG_ff, 180/np.pi*np.angle(IMC_alone_OLG_TF))

last_index = 600
IMC_model_ff = FSS_OLG_ff[:last_index]
IMC_model_TF = I(IMC_model_ff) * V(IMC_model_ff) * FSS_CLG_TF[:last_index]
p1, = s1.loglog(IMC_model_ff, np.abs(IMC_model_TF), 
            label='Estimated IMC loop alone\nDC plant gain = {:.2e} W/Hz'.format(IMC_plant_gain * mystery_IMC_gain))
s2.semilogx(IMC_model_ff, 180/np.pi*np.angle(IMC_model_TF))

fflog5 = np.logspace(np.log10(10.0), np.log10(1000.0), 300)
IMC_model_TF = I(fflog5) * V(fflog5) * -1 # assume FSS CLG is -1 at these freqs
s1.loglog(fflog5, np.abs(IMC_model_TF), color=p1.get_color())
s2.semilogx(fflog5, 180/np.pi*np.angle(IMC_model_TF), color=p1.get_color())


# last_index2 = 600
# IMC_model_ff2 = FSS_OLG_ff[:last_index2]
# IMC_model_TF2 = I(IMC_model_ff2) * V(IMC_model_ff2)/IMC_Comp(IMC_model_ff2) * FSS_CLG_TF[:last_index2]
# p1, = s1.loglog(IMC_model_ff2, np.abs(IMC_model_TF2), 
#             label='Estimated IMC Loop Alone no IMC Comp (I V F / (1 - F))\nDC Plant Gain = {:.2e} W/Hz'.format(IMC_plant_gain * mystery_IMC_gain))
# s2.semilogx(IMC_model_ff2, 180/np.pi*np.angle(IMC_model_TF2))

# # fflog5 = np.logspace(np.log10(10.0), np.log10(1000.0), 300)
# IMC_model_TF2 = I(fflog5) * V(fflog5)/IMC_Comp(fflog5) * -1 # assume FSS CLG is -1 at these freqs
# s1.loglog(fflog5, np.abs(IMC_model_TF2), color=p1.get_color())
# s2.semilogx(fflog5, 180/np.pi*np.angle(IMC_model_TF2), color=p1.get_color())

s1.set_xlim([1e3, 6e5])
s2.set_xlim([1e3, 6e5])
s1.set_ylim([1e-2, 1.01e3])
s2.set_ylim([-180, 180])

s1.set_yticks(nu.good_ticks(s1))

s1.set_ylabel('TF Mag')
s2.set_ylabel('TF Phase [degs]')
s2.set_xlabel('Frequency [Hz]')

s1.grid()
s2.grid()
s1.grid(which='minor', ls='--')
s2.grid(which='minor', ls='--')

s1.legend()

plotName = 'imc_loop_alone.pdf'
fullPlotName = f'{fig_dir}/{plotName}'
plotted_figures = np.append(plotted_figures, fullPlotName)

make_interactive_svg_multiple_subplots(fig, fullPlotName.split('.pdf')[0])

print('Writing plot PDF to {}'.format(fullPlotName))
plt.savefig(fullPlotName, bbox_inches='tight')
plt.close()


###   FSS OLG (F)###   
fig, (s1,s2) = plt.subplots(2)

s1.loglog(FSS_OLG_ff, np.abs(FSS_OLG_TF), label='Measured FSS OLG (F)')
s2.semilogx(FSS_OLG_ff, 180/np.pi*np.angle(FSS_OLG_TF))

s1.loglog(FSS_OLG_ff, np.abs(FSS_CLG_TF), label='FSS CLG (F/(1-F))')
s2.semilogx(FSS_OLG_ff, 180/np.pi*np.angle(FSS_CLG_TF))

s1.set_yticks(nu.good_ticks(s1))

s1.set_ylabel('TF Mag')
s2.set_ylabel('TF Phase [degs]')
s2.set_xlabel('Frequency [Hz]')

s1.grid()
s2.grid()
s1.grid(which='minor', ls='--')
s2.grid(which='minor', ls='--')

s1.legend()

plotName = 'fss_olg.pdf'
fullPlotName = f'{fig_dir}/{plotName}'
plotted_figures = np.append(plotted_figures, fullPlotName)
print('Writing plot PDF to {}'.format(fullPlotName))
plt.savefig(fullPlotName, bbox_inches='tight')
plt.close()


###   Full Lock IMC Suppression###   
fig, (s1,s2) = plt.subplots(2)

s1.loglog(IMC_OLG_ff, np.abs(IMC_SUP_TF), label='Measured Full Lock IMC Suppression')
s2.semilogx(IMC_OLG_ff, 180/np.pi*np.angle(IMC_SUP_TF))

s1.loglog(est_IMC_full_lock_ff, np.abs(est_IMC_full_lock_SUP), label='Estimated Full Lock IMC Suppression')
s2.semilogx(est_IMC_full_lock_ff, 180/np.pi*np.angle(est_IMC_full_lock_SUP))

s1.loglog(fflog2, IMC_SUP_TF_fit, label='Fit Full Lock IMC Suppression\n$%.2e \\times f^{%.2f}$'%(popt[0], popt[1]))

s1.set_ylabel('TF Mag')
s2.set_ylabel('TF Phase [degs]')
s2.set_xlabel('Frequency [Hz]')

s1.grid()
s2.grid()
s1.grid(which='minor', ls='--')
s2.grid(which='minor', ls='--')

s1.legend()

plotName = 'full_lock_imc_suppression.pdf'
fullPlotName = f'{fig_dir}/{plotName}'
plotted_figures = np.append(plotted_figures, fullPlotName)
print('Writing plot PDF to {}'.format(fullPlotName))
plt.savefig(fullPlotName, bbox_inches='tight')
plt.close()


###    IMC OLG times Total Suppression ###   
fig, (s1,s2) = plt.subplots(2)

s1.loglog(est_IMC_OLG_over_total_SUP_ff, np.abs(est_IMC_OLG_over_total_SUP_TF),
            label='Estimated IMC Alone OLG * Total Suppression')
s2.semilogx(est_IMC_OLG_over_total_SUP_ff, 180/np.pi*np.angle(est_IMC_OLG_over_total_SUP_TF))

s1.loglog(fflog3, est_IMC_OLG_over_total_SUP_TF_fit,
            label='Fit Full Lock IMC Suppression\n$%.2e \\times f^{%.2f}$'%(popt2[0], popt2[1]))

s1.set_ylabel('TF Mag')
s2.set_ylabel('TF Phase [degs]')
s2.set_xlabel('Frequency [Hz]')

s1.grid()
s2.grid()
s1.grid(which='minor', ls='--')
s2.grid(which='minor', ls='--')

s1.legend()

plotName = 'estimated_imc_alone_olg_times_total_suppression.pdf'
fullPlotName = f'{fig_dir}/{plotName}'
plotted_figures = np.append(plotted_figures, fullPlotName)
print('Writing plot PDF to {}'.format(fullPlotName))
plt.savefig(fullPlotName, bbox_inches='tight')
plt.close()


###   Estimated CARM Plant MC2 Dither ###   
fig, (s1,s2) = plt.subplots(2)

s1.loglog(CARM_Plant_ff, np.abs(CARM_Plant_TF), 'o', label='Reconstructed CARM Plant from REFL A')
s2.semilogx(CARM_Plant_ff, 180/np.pi*np.angle(CARM_Plant_TF), 'o')

s1.loglog(CARM_Plant_ffB, np.abs(CARM_Plant_TFB), '^', label='Reconstructed CARM Plant from REFL B')
s2.semilogx(CARM_Plant_ffB, 180/np.pi*np.angle(CARM_Plant_TFB), '^')

idx = -3
gammaA = np.abs(CARM_Plant_TF[idx]) * CARM_Plant_ff[idx]
gammaB = np.abs(CARM_Plant_TFB[idx]) * CARM_Plant_ffB[idx]
CARMpole = 0.66 # Hz
CARMoptgainA = gammaA/CARMpole
CARMoptgainB = gammaB/CARMpole
s1.loglog(CARM_Plant_ff, slopeFit(CARM_Plant_ff, np.abs(CARM_Plant_TF[idx]) * CARM_Plant_ff[idx], -1),
            label='$%.2e/f$ REFL A Fit'%(gammaA))
s1.loglog(CARM_Plant_ffB, slopeFit(CARM_Plant_ffB, np.abs(CARM_Plant_TFB[idx]) * CARM_Plant_ffB[idx], -1),
            label='$%.2e/f$ REFL B Fit'%(gammaB))

s1.set_yticks(nu.good_ticks(s1))

s1.set_title('Estimated CARM Plant - '+
            'pole = {:.2f} Hz, '.format(CARMpole) +
            'Opt Gain A = {:.2f} mW/Hz, '.format(CARMoptgainA*1e3) +
            'Opt Gain B = {:.2f} mW/Hz'.format(CARMoptgainB*1e3),
             fontsize=12 )
s1.set_ylabel('TF Mag [W/Hz]')
s2.set_ylabel('TF Phase [degs]')
s2.set_xlabel('Frequency [Hz]')

s1.grid()
s2.grid()
s1.grid(which='minor', ls='--')
s2.grid(which='minor', ls='--')

s1.legend()

plotName = 'reconstructed_carm_plant.pdf'
fullPlotName = f'{fig_dir}/{plotName}'
plotted_figures = np.append(plotted_figures, fullPlotName)
print('Writing plot PDF to {}'.format(fullPlotName))
plt.savefig(fullPlotName, bbox_inches='tight')
plt.close()



###    Check Gains of Servo Boards ###   
###    IMC passive pole ###   
fig, (s1,s2) = plt.subplots(2)

s1.loglog(fflog4, np.abs(P(fflog4)), label='IMC passive pole (= P )')
s2.semilogx(fflog4, 180/np.pi*np.angle(P(fflog4)))

s1.set_title('IMC passive pole')
s1.set_ylabel('TF Mag [Hz/Hz]')
s2.set_ylabel('TF Phase [degs]')
s2.set_xlabel('Frequency [Hz]')

s1.set_yticks(nu.good_ticks(s1))

s1.grid()
s2.grid()
s1.grid(which='minor', ls='--')
s2.grid(which='minor', ls='--')

s1.legend()

plotName = 'imc_passive_pole.pdf'
fullPlotName = f'{fig_dir}/{plotName}'
plotted_figures = np.append(plotted_figures, fullPlotName)
print('Writing plot PDF to {}'.format(fullPlotName))
plt.savefig(fullPlotName, bbox_inches='tight')
plt.close()


###    IMC VCO TF ###   
fig, (s1,s2) = plt.subplots(2)

s1.loglog(fflog4, IMC_AOM_doublepass * IMC_VCO_volts_to_hertz * np.abs(IMC_VCO(fflog4)), label='IMC VCO TF (= part of V )')
s2.semilogx(fflog4, 180/np.pi*np.angle(IMC_VCO(fflog4)))

s1.set_title('IMC VCO TF (DC = {:.0f} Hz/V)'.format(IMC_AOM_doublepass * IMC_VCO_volts_to_hertz))
s1.set_ylabel('TF Mag [Hz/V]')
s2.set_ylabel('TF Phase [degs]')
s2.set_xlabel('Frequency [Hz]')

s1.set_yticks(nu.good_ticks(s1))

s1.grid()
s2.grid()
s1.grid(which='minor', ls='--')
s2.grid(which='minor', ls='--')

s1.legend()

plotName = 'imc_vco_tf.pdf'
fullPlotName = f'{fig_dir}/{plotName}'
plotted_figures = np.append(plotted_figures, fullPlotName)
print('Writing plot PDF to {}'.format(fullPlotName))
plt.savefig(fullPlotName, bbox_inches='tight')
plt.close()


###    REFL Servo Board TFs ###   
fig, (s1,s2) = plt.subplots(2)

s1.loglog(fflog4, np.abs(REFL_Comp(fflog4)), label='REFL Compensation')
s2.semilogx(fflog4, 180/np.pi*np.angle(REFL_Comp(fflog4)))

s1.loglog(fflog4, np.abs(REFL_Boost1(fflog4)), label='REFL Boost 1')
s2.semilogx(fflog4, 180/np.pi*np.angle(REFL_Boost1(fflog4)))

s1.loglog(fflog4, np.abs(REFL_Fast(fflog4)), label='REFL Fast')
s2.semilogx(fflog4, 180/np.pi*np.angle(REFL_Fast(fflog4)))

s1.set_title('REFL Servo Board TFs')
s1.set_ylabel('TF Mag [V/V]')
s2.set_ylabel('TF Phase [degs]')
s2.set_xlabel('Frequency [Hz]')

s1.set_yticks(nu.good_ticks(s1))

s1.grid()
s2.grid()
s1.grid(which='minor', ls='--')
s2.grid(which='minor', ls='--')

s1.legend()

plotName = 'refl_servo_board_tfs.pdf'
fullPlotName = f'{fig_dir}/{plotName}'
plotted_figures = np.append(plotted_figures, fullPlotName)
print('Writing plot PDF to {}'.format(fullPlotName))
plt.savefig(fullPlotName, bbox_inches='tight')
plt.close()


###    IMC Servo Board TFs ###   
fig, (s1,s2) = plt.subplots(2)

s1.loglog(fflog4, np.abs(IMC_Comp(fflog4)), label='IMC Compensation')
s2.semilogx(fflog4, 180/np.pi*np.angle(IMC_Comp(fflog4)))

s1.loglog(fflog4, np.abs(IMC_Boost1(fflog4)), label='IMC Boost 1')
s2.semilogx(fflog4, 180/np.pi*np.angle(IMC_Boost1(fflog4)))

s1.loglog(fflog4, np.abs(IMC_Boost2(fflog4)), ls='--', label='IMC Boost 2')
s2.semilogx(fflog4, 180/np.pi*np.angle(IMC_Boost2(fflog4)), ls='--')

s1.loglog(fflog4, np.abs(IMC_Fast(fflog4)), label='IMC Fast')
s2.semilogx(fflog4, 180/np.pi*np.angle(IMC_Fast(fflog4)))

s1.set_title('IMC Servo Board TFs')
s1.set_ylabel('TF Mag [V/V]')
s2.set_ylabel('TF Phase [degs]')
s2.set_xlabel('Frequency [Hz]')

s1.set_yticks(nu.good_ticks(s1))

s1.grid()
s2.grid()
s1.grid(which='minor', ls='--')
s2.grid(which='minor', ls='--')

s1.legend()

plotName = 'imc_servo_board_tfs.pdf'
fullPlotName = f'{fig_dir}/{plotName}'
plotted_figures = np.append(plotted_figures, fullPlotName)
print('Writing plot PDF to {}'.format(fullPlotName))
plt.savefig(fullPlotName, bbox_inches='tight')
plt.close()


###    CARM Alone OLG with deconstructed parts (C, P, H, V) ###   
fig, (s1,s2) = plt.subplots(2)

s1.loglog(CARM_alone_OLG_ff, np.abs(CARM_alone_OLG), label='Measured CARM loop gain')
s2.semilogx(CARM_alone_OLG_ff, 180/np.pi*np.angle(CARM_alone_OLG))

s1.loglog(fflog4, np.abs(CARM_loop_model),
    label='Modeled CARM loop gain\n CARM DC gain = {} mW/Hz'.format(CARM_plant_gain*1e3))
s2.semilogx(fflog4, 180/np.pi*np.angle(CARM_loop_model))

#s1.loglog(fflog2, IMC_SUP_TF_fit, label='Fit Full Lock IMC Suppression\n$%.2e \\times f^{%.2f}$'%(popt[0], popt[1]))

# s1.set_title('Full CARM Loop Gain')
s1.set_ylabel('TF Mag')
s2.set_ylabel('TF Phase [degs]')
s2.set_xlabel('Frequency [Hz]')

s1.set_xlim([1e3, 1e5])
s2.set_xlim([1e3, 1e5])
s1.set_ylim([1e-1, 1e6])
s2.set_ylim([-180, 180])

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks([-180, -90, 0, 90, 180])

s1.grid()
s2.grid()
s1.grid(which='minor', ls='--')
s2.grid(which='minor', ls='--')

s1.legend()

plotName = 'carm_alone_loop_gain.pdf'
fullPlotName = f'{fig_dir}/{plotName}'
plotted_figures = np.append(plotted_figures, fullPlotName)
print('Writing plot PDF to {}'.format(fullPlotName))
plt.savefig(fullPlotName, bbox_inches='tight')
plt.close()


###   CARM loop components###   
fig, (s1,s2) = plt.subplots(2)

s1.loglog(ff, np.abs( IMC_REFL_sensing_chain * nu.dB2Mag(IMC_IN1Gain)/(C(ff) * H(ff)) ), label='Hi / C Hc')
s2.semilogx(ff, 180/np.pi*np.angle( IMC_REFL_sensing_chain * nu.dB2Mag(IMC_IN1Gain)/(C(ff) * H(ff)) ) )

# s1.loglog(ff, np.abs(P(ff)), label='P')
# s2.semilogx(ff, 180/np.pi*np.angle(P(ff)))

# s1.loglog(ff, np.abs(H(ff)), label='H')
# s2.semilogx(ff, 180/np.pi*np.angle(H(ff)))

# s1.loglog(ff, np.abs(V(ff)), label='V')
# s2.semilogx(ff, 180/np.pi*np.angle(V(ff)))

s1.set_title('CARM Loop Components')
s1.set_ylabel('TF Mag')
s2.set_ylabel('TF Phase [degs]')
s2.set_xlabel('Frequency [Hz]')

s1.set_yticks(nu.good_ticks(s1))

s1.grid()
s2.grid()
s1.grid(which='minor', ls='--')
s2.grid(which='minor', ls='--')

s1.legend()

plotName = 'carm_loop_components.pdf'
fullPlotName = f'{fig_dir}/{plotName}'
plotted_figures = np.append(plotted_figures, fullPlotName)
print('Writing plot PDF to {}'.format(fullPlotName))
plt.savefig(fullPlotName, bbox_inches='tight')
plt.close()


###   CARM Plant from the two different methods (Model based vs REFL based)###   
fig, (s1,s2) = plt.subplots(2)

s1.loglog(ff, np.abs(C_meas), label='C from Model and CARM OLG meas')
s2.semilogx(ff, 180/np.pi*np.angle(C_meas))

gammaC = popt3[0]
CARMoptgainC = gammaC/CARMpole
s1.loglog(fflog4, np.abs(est_C),
        label='$%.2e \\times f^{%.1f}$ C Estimated from OLG\nOpt Gain = %.2f mW/Hz'%(popt3[0], popt3[1], CARMoptgainC*1e3))

s1.loglog(CARM_Plant_ff, np.abs(CARM_Plant_TF), 'o',
        label='Reconstructed CARM Plant from REFL A\nOpt Gain = %.2f mW/Hz'%(CARMoptgainA*1e3))
s2.semilogx(CARM_Plant_ff, 180/np.pi*np.angle(CARM_Plant_TF), 'o')

s1.loglog(CARM_Plant_ffB, np.abs(CARM_Plant_TFB), '^',
        label='Reconstructed CARM Plant from REFL B\nOpt Gain = %.2f mW/Hz'%(CARMoptgainB*1e3))
s2.semilogx(CARM_Plant_ffB, 180/np.pi*np.angle(CARM_Plant_TFB), '^')

idx = -3
gammaA = np.abs(CARM_Plant_TF[idx]) * CARM_Plant_ff[idx]
gammaB = np.abs(CARM_Plant_TFB[idx]) * CARM_Plant_ffB[idx]
CARMpole = 0.66 # Hz
CARMoptgainA = gammaA/CARMpole
CARMoptgainB = gammaB/CARMpole
s1.loglog(CARM_Plant_ff, slopeFit(CARM_Plant_ff, np.abs(CARM_Plant_TF[idx]) * CARM_Plant_ff[idx], -1),
            label='$%.2e/f$ REFL A Fit'%(gammaA))
s1.loglog(CARM_Plant_ffB, slopeFit(CARM_Plant_ffB, np.abs(CARM_Plant_TFB[idx]) * CARM_Plant_ffB[idx], -1),
            label='$%.2e/f$ REFL B Fit'%(gammaB))

s1.set_xlim([1e1,1e5])
s2.set_xlim([1e1,1e5])
s1.set_yticks(nu.good_ticks(s1))


#s1.loglog(fflog2, IMC_SUP_TF_fit, label='Fit Full Lock IMC Suppression\n$%.2e \\times f^{%.2f}$'%(popt[0], popt[1]))

s1.set_title('CARM Plant Estimates')
s1.set_ylabel('TF Mag [W/Hz]')
s2.set_ylabel('TF Phase [degs]')
s2.set_xlabel('Frequency [Hz]')

s1.grid()
s2.grid()
s1.grid(which='minor', ls='--')
s2.grid(which='minor', ls='--')

s1.legend()

plotName = 'carm_plants_comparison.pdf'
fullPlotName = f'{fig_dir}/{plotName}'
plotted_figures = np.append(plotted_figures, fullPlotName)
print('Writing plot PDF to {}'.format(fullPlotName))
plt.savefig(fullPlotName, bbox_inches='tight')
plt.close()

###   Command to open all plots###   
command = 'open'
for pf in plotted_figures:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()
