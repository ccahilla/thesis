'''
Create a budget for the frequency to darm transfer function, 
inspired by Kiwamu's equation from https://dcc.ligo.org/LIGO-T1500559/public

Will create budget for aLIGO as it is in O3, and estimates for CE.

Should be able to run this code out-of-the-box, by sourcing the anaconda python environment
defined in environment.yml. See README.

Craig Cahillane
Jan 8, 2020
'''
import os
import pickle
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc
import scipy.special as scp
import scipy.optimize as sco
import scipy.interpolate as sci

import pykat
import pykat.ifo.aligo as aligo
import pykat.ifo.aligo.plot

import nds2utils as nu
from nds2utils.make_interactive_svg import make_interactive_svg_multiple_subplots, make_interactive_svg

import gwinc

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])



#####   Functions   #####
def load_tf_data(filename):
    ''' Function to read in data 
    Get the measured LHO tf [m/Hz]'''
    data = np.loadtxt(filename)
    ff  = data[:,0]
    tf  = data[:,1] * np.exp(1j*np.pi/180 * data[:,2])
    coh = data[:,3]

    idxs = np.argsort(ff)
    ff = ff[idxs]
    tf = tf[idxs]
    coh = coh[idxs]

    return ff, tf, coh

def get_logbinned_data(ff, tf, coh, fflog):
    '''Take in frequency vector, transfer function, coherence, and log frequency vector we want to interpolate onto
    return the logbinned frequency vector, tf, and coherence:
    return ff_log, tf_log, coh_log
    '''
    ff_log, tf_real = nu.linear_log_ASD(fflog, ff, np.real(tf))
    _, tf_imag = nu.linear_log_ASD(fflog, ff, np.imag(tf))
    _, coh_log = nu.linear_log_ASD(fflog, ff, coh)

    tf_log = tf_real + 1j * tf_imag
    return ff_log, tf_log, coh_log

def compile_data_dict(filename, fflog):
    '''Compiles a dictionary out of the data extracted from the .txt filename.
    Returns the dictionary with keys: ff, tf, coh, ff_log, tf_log, coh_log
    '''
    ff, tf, coh = load_tf_data(filename)
    ff_log, tf_log, coh_log = get_logbinned_data(ff, tf, coh, fflog)

    temp_dict = {}
    temp_dict['ff'] = ff
    temp_dict['tf'] = tf
    temp_dict['coh'] = coh
    temp_dict['ff_log'] = ff_log
    temp_dict['tf_log'] = tf_log
    temp_dict['coh_log'] = coh_log

    return temp_dict

#####   IFO Functions   #####
def arm_gain(phi, T1, T2, Loss_rt):
    '''
    Returns the amplitude gain of an arm cavity.
    phi     = relative phase of the light in single pass of the cavity
    T1      = power transmittance of input mirror
    T2      = power transmittance of output mirror
    Loss_rt = roundtrip power loss
    '''
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    invloss = np.sqrt(1 - Loss_rt)
    return t1/(1 - r1 * r2 * invloss * np.exp(1j*phi))

def arm_refl(phi, T1, T2, Loss_rt):
    '''
    Returns the reflectivity of an arm cavity.
    phi     = relative phase of the light to the cavity
    T1      = power transmittance of input mirror
    T2      = power transmittance of output mirror
    Loss_rt = roundtrip power loss
    '''
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    invloss = np.sqrt(1 - Loss_rt)
    return -r1 + t1 * r2 * invloss * arm_gain(phi, T1, T2, Loss_rt)

def arm_refl_derivative(phi, T1, T2, Loss_rt):
    '''
    Returns the derivative of the reflectivity of an arm cavity.
    phi     = relative phase of the light to the cavity
    T1      = power transmittance of input mirror
    T2      = power transmittance of output mirror
    Loss_rt = roundtrip power loss
    '''
    r2 = np.sqrt(1 - T2)
    invloss = np.sqrt(1 - Loss_rt)
    return r2 * invloss * np.exp(2j*phi) * np.abs(arm_gain(phi, T1, T2, Loss_rt))**2

def arm_pole(L, T1, T2, Loss_rt):
    '''
    Returns the arm cavity pole in Hz.
    phi     = relative phase of the light to the cavity
    L       = length of the cavity
    T1      = power transmittance of input mirror
    T2      = power transmittance of output mirror
    Loss_rt = roundtrip power loss
    '''
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    invloss = np.sqrt(1 - Loss_rt)    
    return scc.c/(2*L) * 1/(2*np.pi) * np.log(1/(r1*r2*invloss))

def coupled_cavity_pole(L, T1, T2, T3, Loss_rt):
    '''
    Returns the coupled cavity pole in Hz.
    L       = length of the longer cavity, shorter cavity length assumed to be zero.
    T1      = power transmittance of input mirror
    T2      = power transmittance of middle mirror
    T3      = power transmittance of end mirror
    Loss_rt = roundtrip power loss
    '''
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    r3 = np.sqrt(1 - T3)
    invloss = np.sqrt(1 - Loss_rt)  
    return scc.c/(2*L) * 1/(2*np.pi) * np.log((1 + r1 * r2)/(r2 * r3 + r1 * r3 * invloss**2))

def rse_pole(L, T1, T2, T3, Loss_rt):
    '''
    NOTE: This function is not valid for CE, the FSR is too close to the darm pole, making this approx invalid
    Use rse_pole_exact() instead

    Returns the darm resonant signal extraction pole in Hz.  
    Same as coupled_cavity_pole() but with sign changes.
    L       = length of the longer cavity, shorter cavity length assumed to be zero.
    T1      = power transmittance of input mirror
    T2      = power transmittance of middle mirror
    T3      = power transmittance of end mirror
    Loss_rt = roundtrip power loss
    '''
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    r3 = np.sqrt(1 - T3)
    invloss = np.sqrt(1 - Loss_rt)  
    return scc.c/(2*L) * 1/(2*np.pi) * np.log((1 - r1 * r2)/(r2 * r3 - r1 * r3 * invloss**2))

def rse_pole_exact(L1, L2, T1, T2, T3, Loss_rt):
    '''
    Returns the darm resonant signal extraction complex pole in Hz.  
    Uses scipy.root to find an exact solution.
    Same as coupled_cavity_pole() but with sign changes.
    L       = length of the longer cavity, shorter cavity length assumed to be zero.
    T1      = power transmittance of input mirror
    T2      = power transmittance of middle mirror
    T3      = power transmittance of end mirror
    Loss_rt = roundtrip power loss
    '''    
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    r3 = np.sqrt(1 - T3)
    invloss = np.sqrt(1 - Loss_rt)
    def root_func(fc, L1, L2, r1, r2, r3, invloss):
        f = complex(*fc)
        err = np.exp((4j * f * (L2 + L1) * np.pi)/scc.c) \
        - np.exp((4j * f * L2 * np.pi)/scc.c) * r1 * r2 \
        + invloss**2 * r1 * r3 \
        - np.exp((4j * f * L1 * np.pi)/scc.c) * r2 * r3
        return [err.real, err.imag]

    solution_dict = sco.root(root_func, [1000, 1000], args=(L1, L2, r1, r2, r3, invloss))
    f_rse = solution_dict['x'][0] + 1j*solution_dict['x'][1]
    return f_rse

def darm_full(fff, L1, L2, T1, T2, T3): 
    ''' Get normalized darm response (W/W)/m'''
    t2 = np.sqrt(T2)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    r3 = np.sqrt(1 - T3)
    exp1 = np.exp(1j*2*np.pi*L1*fff/scc.c) 
    exp2 = np.exp(1j*2*np.pi*L2*fff/scc.c) 
     
    return (1 - r1*r2 -r2*r3 + r1*r3)/(exp1**2 * exp2**2 - exp2**2*r1*r2 - exp1**2*r2*r3 + r1*r3*(r2**2 + t2**2)) 

def darm_simple(fff, L, T1, T2, T3):
    ''' Normalized Simple darm pole (W/W)/m'''
    f_rse = rse_pole(L, T1, T2, T3, 0)
    return 1/(1 + 1j*fff/f_rse)

def darm_HF_correction(fff, L1, L2, T1, T2, T3):
    ''' 
    Correction factor for darm response at HF, 
    c.f. the simple pole approximation vs the true darm response.
    Returns the normalized correction factor  = Simple Pole (W/m)/Full Response (W/m) [meters_full_resp / meters_simple_pole]
    '''
    L = L2 # arm length is bigger
    simple_pole = darm_simple(fff, L, T1, T2, T3)
    full_resp = darm_full(fff, L1, L2, T1, T2, T3)
    return simple_pole/full_resp

###  Kiwamu's DCC T1500559 Eq 29  ###
def analytic_freq_to_darm_tf(fff, ifo):
    ''' 
    Takes in an ifo dictionary in the style of pygwinc.
    Outputs three analytic tranfer functions from frequency noise to darm in [m/Hz] units corresponding to 
    1) reflectivity difference,
    2) arm pole (storage time) difference,
    3) Schnupp asymmetry
    Coming soon: Radiation pressure
    '''
    # Set up params
    wavelength = ifo['Laser']['Wavelength']
    k = 2 * np.pi / wavelength

    L = ifo['Infrastructure']['Length']
    Ls = ifo['Infrastructure']['SRC_length']
    L_darmoffset = ifo['Infrastructure']['darmoffset']
    epsilon = k * L_darmoffset # phase of the darm offset
    phi = 2 * np.pi * L_darmoffset / wavelength

    M = ifo['Suspension']['Stage'][0]['Mass'] # mass of the final test mass
    mu = M / 2.0 # reduced mass

    Ts = ifo['Optics']['SRM']['Transmittance']
    Tp = ifo['Optics']['PRM']['Transmittance']
    Ti = ifo['Optics']['ITM']['Transmittance']
    Te = ifo['Optics']['ETM']['Transmittance']
    Loss_rt = ifo['Optics']['Loss']

    print()
    print(f'ETM Trans = {Te}')
    print(f'ITM Trans = {Ti}')
    print(f'PRM Trans = {Tp}')
    print(f'SRM Trans = {Ts}')
    print(f'Loss rt = {Loss_rt}')

    l_schnupp = ifo['Infrastructure']['SchnuppAsymmetry']

    delta_ra = ifo['Infrastructure']['delta_ra']
    delta_f_c = ifo['Infrastructure']['delta_f_c']
    delta_omega_c = 2 * np.pi * delta_f_c

    # Use helper functions
    ra = arm_refl(phi, Ti, Te, Loss_rt)
    dradphi = arm_refl_derivative(phi, Ti, Te, Loss_rt)
    f_c = arm_pole(L, Ti, Te, Loss_rt)
    f_cc = coupled_cavity_pole(L, Tp, Ti, Te, Loss_rt)
    f_rse_complex = rse_pole_exact(Ls, L, Ts, Ti, Te, Loss_rt)
    f_rse = np.imag(f_rse_complex)
    omega_c = 2 * np.pi * f_c
    omega_cc = 2 * np.pi * f_cc
    omega_rse = 2 * np.pi * f_rse

    print()
    print(f'arm refl ra = {ra}')
    print(f'arm refl dervi dradphi = {dradphi}')
    print(f'arm pole f_c = {f_c} Hz')
    print(f'coupled cavity pole f_cc = {f_cc} Hz')
    print(f'darm pole f_rse = {f_rse} Hz')

    s_c = 1j * fff / f_c
    s_cc = 1j * fff / f_cc
    s_rse = 1j * fff / f_rse
    s_mu2 = -mu * (2 * np.pi * fff)**2 # s_mu^2 (Eq 8)

    prefactor = wavelength * (1 + ra) / (4 * omega_c * dradphi * (1 + s_cc))
    
    tf1 = prefactor * -delta_ra
    tf2 = prefactor * -delta_omega_c/omega_c * (1 + ra)
    tf3 = prefactor * (l_schnupp * ra * omega_c / scc.c) * (1 - s_c/ra) * (1 + s_c)

    # Radiation pressure induced from darm offset
    Pa = ifo['Infrastructure']['ArmPower']
    Ta = 1 - ra**2
    gs = arm_gain(np.pi, Ts, Ta, 0) # SRC is anti-resonant for carrier
    gp = arm_gain(0, Tp, Ta, 0)
    Gs = np.abs(gs)**2
    Gp = np.abs(gp)**2
    print(f'Arm power Pa = {Pa}')
    print(f'SRC gain Gs = {Gs}')
    print(f'PRC gain Gp = {Gp}')

    dLdarmdf = -16 * np.pi * Pa * Gs * dradphi * epsilon / (scc.c * omega_rse * s_mu2 * (1 + s_cc) * (1 + s_rse)) # 16, not 8, b/c of DARM definition
    tf4 = dLdarmdf # m/Hz

    G_darm = (1 + s_rse)/(4 * Gp * Gs * dradphi**2 * epsilon * k)    # m/W, from darm watts to meters
    
    # high_HOM_phase_to_watts = np.exp(1j*np.pi/4) * 1e-4 # W/rad
    # low_HOM_phase_to_watts = np.exp(1j*np.pi/4) * 1e-5 # W/rad

    # tf5 = high_HOM_phase_to_watts * G_darm / (1j * fff) # m/Hz
    # tf6 = low_HOM_phase_to_watts * G_darm / (1j * fff) # m/Hz

    # high_HOM_Hz_to_watts = 3e-8 # W/Hz
    # low_HOM_Hz_to_watts = 3e-9  # W/Hz

    # tf5 = high_HOM_Hz_to_watts * G_darm # m/Hz
    # tf6 = low_HOM_Hz_to_watts * G_darm  # m/Hz

    tf5 = 6e-16 * np.ones_like(fff) # m/Hz
    tf6 = 8e-17 * np.ones_like(fff) # m/Hz

    # Fudge factors for playing around with coupling levels
    # fudge_factor4 = 50
    # tf4 = tf4 * fudge_factor4

    return tf1, tf2, tf3, tf4, tf5, tf6



#####   Get tf data   #####
dates = np.array([
    '20190424',
    '20191101'
])
labels = {
    '20190424' : 'LHO April 24, 2019',
    '20191101' : 'LHO November 01, 2019',
}
styles = {
    '20190424' : 'C2-',
    '20191101' : 'C3-',
}

fflog = np.logspace(np.log10(1), np.log10(40000), 500)

data_dict = {}
for date in dates:
    filename = f'{data_dir}/{date}_H1_LSC-REFL_SERVO_ERR_OUT_DQ_to_DARM_TF.txt'

    temp_dict = compile_data_dict(filename, fflog)
    temp_dict['label'] = labels[date]
    temp_dict['style'] = styles[date]

    data_dict[date] = temp_dict



#####   IFO params   #####
ifos = {}

aligo_budget = gwinc.load_budget('aLIGO')
ce1_budget = gwinc.load_budget('CE1')
ce2_budget = gwinc.load_budget('CE2')

ifos['aLIGO'] = aligo_budget.ifo
ifos['CE1'] = ce1_budget.ifo
ifos['CE2'] = ce2_budget.ifo

ifos['aLIGO']['Infrastructure']['SRC_length'] = 55 # m
ifos['CE1']['Infrastructure']['SRC_length'] = 105.4 # m
ifos['CE2']['Infrastructure']['SRC_length'] = 105.4 # m

ifos['aLIGO']['Infrastructure']['SchnuppAsymmetry'] = 8e-2 # m
ifos['CE1']['Infrastructure']['SchnuppAsymmetry'] = 13.9e-3 # m, from Kevin Kuns
ifos['CE2']['Infrastructure']['SchnuppAsymmetry'] = 13.9e-3 # m

ifos['aLIGO']['Infrastructure']['darmoffset'] = 10e-12  # m
ifos['CE1']['Infrastructure']['darmoffset'] = 0
ifos['CE2']['Infrastructure']['darmoffset'] = 0

ifos['aLIGO']['Infrastructure']['ArmPower'] = 200e3 # W
ifos['CE1']['Infrastructure']['ArmPower'] = 1.38e6 # W
ifos['CE2']['Infrastructure']['ArmPower'] = 1.38e6 # W

ifos['aLIGO']['Infrastructure']['delta_ra'] = 5000e-6 / 2 # m
ifos['CE1']['Infrastructure']['delta_ra'] = 5000e-6 / 2
ifos['CE2']['Infrastructure']['delta_ra'] = 5000e-6 / 2

ifos['aLIGO']['Infrastructure']['delta_f_c'] = 3.0 / 2 # Hz
ifos['CE1']['Infrastructure']['delta_f_c'] = 0.04 / 2 # Hz
ifos['CE2']['Infrastructure']['delta_f_c'] = 0.04 / 2 # Hz


#####   Get analytic tfs   #####
print('\033[92m')
print('aLIGO')
tf1, tf2, tf3, tf4, tf5, tf6 = analytic_freq_to_darm_tf(fflog, ifos['aLIGO'])
print('\033[0m')

tf_total = tf1 + tf2 + tf3 + tf4 + tf5 #+ tf6
tf_total_low = tf1 + tf2 + tf3 + tf4 + tf6

print('\033[93m')
print('CE1')
tf1_CE1, tf2_CE1, tf3_CE1, tf4_CE1, tf5_CE1, tf6_CE1 = analytic_freq_to_darm_tf(fflog, ifos['CE1'])
print('\033[0m')

tf_total_CE1 = tf1_CE1 + tf2_CE1 + tf3_CE1 + tf4_CE1 + tf5_CE1 #+ tf6_CE1


####################################
###                              ###
###      Finesse Simulation      ###
###                              ###
####################################

def kat_setup(kat):
    kat.parse(freq_inj_code)

    QE   = 0.98
    resp = 0.856 # A/W
    kat.DARM_lock.offset = -20e-3/(QE*resp) # Get 20mA of DC signal
    kat.DARM_lock.accuracy /= 100

    kat.SRCL_lock.gain /= 2
    kat.PRCL_lock.gain /= 2
    kat.DARM_lock.gain /= 2
    kat.CARM_lock.gain /= 2
    kat.MICH_lock.gain /= 2

    kat.SRCL_lock.accuracy /= 5
    kat.PRCL_lock.accuracy /= 5
    kat.DARM_lock.accuracy /= 5
    kat.CARM_lock.accuracy /= 5
    kat.MICH_lock.accuracy /= 5

    kat.IFO.zero_locks()

    kat.removeBlock("locks")

    kat.SRM.phi += 0.7
    return kat

def kat_run(kat):
    kat = kat_setup(kat)
    out = kat.run()
    return kat, out

def print_HOMs(out):
    '''
    Print the HOM content in each at all ports of the IFO
    '''
    det_list_suffix = np.array([
        'in', 
        # 'imc', 
        'as', 
        'omc', 
        'src', 
        'prc', 
        'mich_x', 
        'mich_y', 
        'arm_x', 
        'arm_y'
        ])
    print()
    print('{:12}'.format('Detectors'), end = '')
    for ii in range(maxtem_order+1):
        for jj in range(maxtem_order+1):
            if ii + jj > maxtem_order:
                continue
            print(f'{ii:8}{jj}', end = ' ')
    print()
    for det in det_list_suffix:
        print(f'{det:12}', end = '')
        for ii in range(maxtem_order+1):
            for jj in range(maxtem_order+1):
                if ii + jj > maxtem_order:
                    continue
                full_det = f'ad{ii}{jj}_{det}'
                hom = np.abs(out[full_det][0])
                if hom < 1e-2:
                    print('', end='')
                elif hom < 1e-1:
                    print('\033[92m', end='')
                elif hom < 1e-0:
                    print('\033[93m', end='')
                else:
                    print('\033[91m', end='')

                print(f'{hom:9.2e}',end = ' ')
                print('\033[0m', end='')
        print()
    print()
    return

def extract_finesse_results(out):
    ''' 
    Takes in a finesse output of injected frequency noise
    Returns the following:
    ff = frequency vector 
    freq_to_darm_tf = frequency noise to darm transfer function
    freq_to_darm_disp_tf = frequency noise to darm displacement transfer function
    freq_to_X_power_tf = frequency noise to X arm power transfer function
    freq_to_Y_power_tf = frequency noise to Y arm power transfer function
    '''
    ff = out.x

    tf = out['pdAS'] # W/Hz
    X_tf = out['pdX'] # W/Hz
    Y_tf = out['pdY'] # W/Hz

    tf_disp = 0.5 * ( (out['xd_ETMX'] - out['xd_ITMX']) - (out['xd_ETMY'] - out['xd_ITMY']) ) # m/Hz

    darm_opt_gain = 1.5656e+10 #0.5 * 1/kat.DARM_lock.gain * 180/np.pi * 2*np.pi/1064e-9 # W/m
    darm_pole = 429 # Hz
    darm_gain = lambda fff : darm_opt_gain / (1 + 1j * fff/darm_pole)

    tf = tf / darm_gain(ff) # m/Hz = W/Hz * m/W 

    return ff, tf, tf_disp, X_tf, Y_tf

# Actual LHO Parameters
inputLosses = 0.1 # ~10% through IMC, FI, and steering mirrors
inputPower = 37 * (1 - inputLosses)

LHO_PRG = 44
LHO_Xarm_power = 175e3 # W +-10%
LHO_Yarm_power = 192e3 # W +-10%

LHO_CARMoptgain_A = 15.7e-3 # W/Hz
LHO_CARMoptgain_B = 13.0e-3 # W/Hz
LHO_CARMpole = 0.66 # Hz

base = aligo.make_kat()

maxtem_order = 4
base.maxtem = maxtem_order
# print(base)

# base.IFO.remove_IMC_HAM2(True, False)

base.L0.P.value = inputPower # W

base.SRM.T    = 0.32
base.SR2.L    = 0.05
base.ETMX.L   = 35e-6
base.ETMY.L   = 35e-6

#https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=47113
base.mod1.midx = 0.16
base.mod2.midx = 0.18

base.ITMY.setRTL(None, 1.42/100., 40e-6)
base.ITMX.setRTL(None, 1.5/100., 40e-6)
base.PRM.L.value /= 4 # Hand fiddling to get PRC gain about right

# Change into diopters
base.ITMY_lens.p = 1/base.ITMY_lens.f.value + 0e-6
base.ITMX_lens.p = 1/base.ITMX_lens.f.value + 0e-6

base.IFO.CARM.port.phase = -87 #92.5

base.IFO.PRCL.port.phase = -60 #119.8
base.IFO.PRCL.quad = 'I'

base.IFO.SRCL.port = base.IFO.POP_f2
base.IFO.SRCL.quad = 'I'

base.IFO.MICH.port.phase = 103.4
base.IFO.MICH.quad = 'Q'

base = aligo.setup(base, verbose=True)

base.IFO.suspend_mirrors_z()

freq_inj_code = \
f'''
pd1 pdAS 1 nAS

pd pdXDC nETMX1
pd pdYDC nETMY1
pd1 pdX 1 nETMX1
pd1 pdY 1 nETMY1

xd xd_ITMX ITMX z
xd xd_ETMX ETMX z
xd xd_ITMY ITMY z
xd xd_ETMY ETMY z

fsig freqNoise L0 1 0
xaxis freqNoise f log {fflog[0]} {fflog[-1]} 301
put pdAS f1 $x1
put pdX f1 $x1
put pdY f1 $x1

yaxis abs:deg
'''

# Add in amplitude detectors for carrier HOMs 
for ii in range(maxtem_order+1):
    for jj in range(maxtem_order+1):
        if ii + jj > maxtem_order:
            continue
        freq_inj_code += f'ad ad{ii}{jj}_in {ii} {jj} 0 nLaserOut' + '\n'   # input laser
        # freq_inj_code += f'ad ad{ii}{jj}_imc {ii} {jj} 0 nIMCout' + '\n'    # input through IMC
        freq_inj_code += f'ad ad{ii}{jj}_prc {ii} {jj} 0 nPRM2*' + '\n'     # incident on PRM
        freq_inj_code += f'ad ad{ii}{jj}_mich_x {ii} {jj} 0 nITMX1a' + '\n' # coming from ITMX
        freq_inj_code += f'ad ad{ii}{jj}_mich_y {ii} {jj} 0 nITMY1a' + '\n' # coming from ITMY
        freq_inj_code += f'ad ad{ii}{jj}_arm_x {ii} {jj} 0 nETMX1' + '\n'   # coming from ITMX
        freq_inj_code += f'ad ad{ii}{jj}_arm_y {ii} {jj} 0 nETMY1' + '\n'   # coming from ITMY
        freq_inj_code += f'ad ad{ii}{jj}_src {ii} {jj} 0 nSRM1*' + '\n'     # incident on SRM
        freq_inj_code += f'ad ad{ii}{jj}_omc {ii} {jj} 0 nOM3b' + '\n'      # incident on OMC
        freq_inj_code += f'ad ad{ii}{jj}_as {ii} {jj} 0 nAS' + '\n'         # through the OMC

tem_code = ''
for ii in range(maxtem_order+1):
    for jj in np.arange(maxtem_order+1):
        if ii + jj > maxtem_order:
            continue
        if ii + jj == 0:
            continue
        tem_code += f'tem L0 {ii} {jj}' + ' {tem_ratio} 0.0' + '\n'

# Parallelization Enable
parallel = False
if parallel:
    from pykat.parallel import parakat
    pk = parakat()

# Set up output dictionary to store results
kat_dict = {}
out_dict = {}

# ITMX_Ts = np.array([0.0142, 0.0150])
# ITMY_lens_fs = np.array([32000, 32500, 33000])

# SR3_RoCs = np.array([   
#     35.972841 * (1 - 0.02) ,                    
#     # 35.972841,
#     35.972841 * (1 + 0.02) ])

# MC2_RoCs = np.array([   27.24 * (1 - 0.01),
#                         27.24,
#                         27.24 * (1 + 0.01)])

# PRM_RoCs = np.array([   11.009 * (1 - 0.01),
#                         11.009,
#                         11.009 * (1 + 0.01)
#     ])

tem_ratios = np.array([
    0,
    # 0.001,
    # 0.01,
    ])

ITM_RoCs = np.array([   
    # -1934 * (1 - 0.003), 
    -1934, 
    # -1934 * (1 + 0.003)
    ])

ETM_RoCs = np.array([   
    # 2245.0 * (1 - 0.003),
    2245.0,
    # 2245.0 * (1 + 0.003)
    ])

# plot_suffix = 'ITMX_RoC_ITMY_RoC_ETMX_RoC_ETMY_RoC'
plot_suffix = 'tem_ratio_ITMX_RoC_ITMY_RoC_ETMX_RoC_ETMY_RoC'
# for SR3_RoC in SR3_RoCs:
for tem_ratio in tem_ratios:
    for ITMX_RoC in ITM_RoCs:
        for ITMY_RoC in ITM_RoCs:
            for ETMX_RoC in ETM_RoCs:
                for ETMY_RoC in ETM_RoCs:
                    name =  f'tem_ratio_{tem_ratio:.3f}' + \
                            '_' + \
                            f'ITMX_{ITMX_RoC:.2f}' + \
                            '_' + \
                            f'ITMY_{ITMY_RoC:.2f}' + \
                            '_' + \
                            f'ETMX_{ETMX_RoC:.2f}' + \
                            '_' + \
                            f'ETMY_{ETMY_RoC:.2f}'
                    
                    kat = base.deepcopy()

                    kat.parse( 
                        tem_code.format(tem_ratio=tem_ratio)
                    )

                    # kat.ITMX.T.value = ITMX_T

                    # kat.ITMY_lens.f.value = ITMY_lens_f

                    # kat.SR3.Rcx.value = SR3_RoC
                    # kat.SR3.Rcy.value = SR3_RoC            

                    kat.ITMX.Rcx.value = ITMX_RoC
                    kat.ITMX.Rcy.value = ITMX_RoC

                    kat.ITMY.Rcx.value = ITMY_RoC
                    kat.ITMY.Rcy.value = ITMY_RoC

                    kat.ETMX.Rcx.value = ETMX_RoC
                    kat.ETMX.Rcy.value = ETMX_RoC

                    kat.ETMY.Rcx.value = ETMY_RoC
                    kat.ETMY.Rcy.value = ETMY_RoC

                    if parallel:
                        kat_dict[name] = kat_setup(kat)
                    else:
                        kat_dict[name], out_dict[name] = kat_run(kat)

if parallel:
    names = np.array([])
    for name in kat_dict.keys():
        names = np.append(names, name)
        kat = kat_dict[name]

        pk.run(kat, cmd_args=["-cr=on"])

    outs = pk.getResults() # results list of outs
    pk.close()

    for out, name in zip(outs, names):
        out_dict[name] = out

    


######################################################################
###                                                                ###
###   Perform "Lock-Dragging" to keep the IFO on resonance         ###
###   while we significantly change the ITMs radius of curvature   ###
###                                                                ###
######################################################################

run_lock_drag = False
if run_lock_drag:
    lock_drag_component_name = 'ITMX'       # Name of the component we want to lock drag
    lock_drag_attribute = 'T'               # The component attribute we want to drag
    lock_drag_units = ''                    # Units of the lock drag attribute
    max_lock_drag_value =  0.0002            # m, the max value to drag by, starting at the original value
    min_lock_drag_value = -0.0008            # m, the min value to drag by, starting at the original value
    number_of_drag_points = 101            # Number of points to evaluate at.  Recall that finesse uses number of steps = number of points - 1

    # Name of the file to save results to
    lock_drag_pickle_filename = \
    f'lock_drag_{lock_drag_component_name}_{lock_drag_attribute}' + \
    f'_m{str(np.abs(min_lock_drag_value)).replace(".", "p")}_{str(max_lock_drag_value).replace(".", "p")}' + \
    f'_pointnumber_{number_of_drag_points}.pkl' 

    # How much to change the component attribute by
    delta_value = -0.0008        

    lock_drag_plot_label = r'$%s_\mathrm{%s}$'%(lock_drag_attribute, lock_drag_component_name.replace('_', ' '))

    def lock_drag(  base, 
                    lock_drag_component_name,
                    lock_drag_attribute,
                    max_lock_drag_value,
                    min_lock_drag_value,
                    number_of_drag_points,
                    lock_drag_pickle_filename,
                    ):
        ''' 
        lock_drag() moves a FINESSE component's attribute by some max and min amount defined by the user,
        over a user-defined number of steps.  At every step, FINESSE moves the lock points such that light
        continues to resonate in the IFO as intended.  These lock points are saved in a pickle file,
        and two FINESSE outputs are returns, the first for the increase the attribute value, 
        the second for the decrease.

        Inputs:
        base                        = base kat aligo FINESSE file.
        lock_drag_component_name    = name of component whose attribute we want to drag
        lock_drag_attribute         = attribute of component we want to drag
        max_lock_drag_value         = max value we want to change the attribute value by
        min_lock_drag_value         = min value we want to change the attribute value by
        number_of_drag_points       = number of points to evaluate at.  Smaller steps can preserve locks.
        lock_drag_pickle_filename   = name of pickle file to save results in.
        '''

        lock_drag_done = False
        if os.path.exists(lock_drag_pickle_filename):
            lock_drag_done = True

        if not lock_drag_done:
            # make a copy of the base kat object contructed from the file
            kat = base.deepcopy()

            # Make a list of all the mirrors that the lock outputs are fed into
            mirrors = [ kat.ETMX, kat.ETMXAR,
                        kat.ITMX, kat.ITMXAR, 
                        kat.ETMY, kat.ETMYAR,
                        kat.ITMY, kat.ITMYAR, 
                        kat.SRM, kat.SRMAR, 
                        kat.PRM, kat.PRMAR]

            # Loop through each of the mirrors thenadd the extra commands
            for m in mirrors:
                kat.parse(f"""
                set _{m.name}_phi {m.name} phi
                func {m.name}_phi = $_{m.name}_phi 
                """)

            kat1 = kat.deepcopy()
            kat1_string = f"""xaxis* {lock_drag_component_name} {lock_drag_attribute} lin 0 {max_lock_drag_value} {number_of_drag_points-1}"""
            kat1.parse(kat1_string)
            print(f'kat1_string:\n{kat1_string}')

            kat2 = kat.deepcopy()
            kat2_string = f"""xaxis* {lock_drag_component_name} {lock_drag_attribute} lin 0 {min_lock_drag_value} {number_of_drag_points-1}"""
            kat2.parse(kat2_string)
            print(f'kat2_string:\n{kat2_string}')

            out1 = kat1.run(cmd_args=["-cr=on"])
            out2 = kat2.run(cmd_args=["-cr=on"])

            # Open a binary pickle file to dump to
            with open(lock_drag_pickle_filename, "wb") as f:
                pickle.dump((out1, out2), f)

        else: # if the lock drag is already done
            print()
            print(f'Lock Drag already done for these settings, loading {lock_drag_pickle_filename}')
            print()
            with open(lock_drag_pickle_filename, "rb") as f:
                (out1, out2) = pickle.load(f)
        
        return out1, out2

    kat = aligo.setup(base, verbose=True)
    out1, out2 = lock_drag(
                    kat,
                    lock_drag_component_name,
                    lock_drag_attribute,
                    max_lock_drag_value,
                    min_lock_drag_value,
                    number_of_drag_points,
                    lock_drag_pickle_filename,
                    )

    # Make interpolated functions for the IFO mirror tunings
    etmx_tune = sci.interp1d(np.hstack((out1.x, out2.x)), np.hstack((out1["ETMX_phi"], out2["ETMX_phi"])))
    itmx_tune = sci.interp1d(np.hstack((out1.x, out2.x)), np.hstack((out1["ITMX_phi"], out2["ITMX_phi"])))
    etmy_tune = sci.interp1d(np.hstack((out1.x, out2.x)), np.hstack((out1["ETMY_phi"], out2["ETMY_phi"])))
    itmy_tune = sci.interp1d(np.hstack((out1.x, out2.x)), np.hstack((out1["ITMY_phi"], out2["ITMY_phi"])))
    prm_tune  = sci.interp1d(np.hstack((out1.x, out2.x)), np.hstack((out1["PRM_phi"], out2["PRM_phi"])))
    srm_tune  = sci.interp1d(np.hstack((out1.x, out2.x)), np.hstack((out1["SRM_phi"], out2["SRM_phi"])))

    # Start a new kat, this time to be run with significant frequency noise injected
    kat_tuned = kat.deepcopy()

    # Change in thermal lens that we want to look at
    original_value = getattr(getattr(getattr(kat_tuned, lock_drag_component_name), lock_drag_attribute), 'value')
    new_value = original_value + delta_value
    setattr(getattr(getattr(kat_tuned, lock_drag_component_name), lock_drag_attribute), 'value', new_value)

    # Using our interpolated function to set the IFO mirror tunings
    kat_tuned.ETMX.phi = kat_tuned.ETMXAR.phi = etmx_tune(delta_value)
    kat_tuned.ITMX.phi = kat_tuned.ITMXAR.phi = itmx_tune(delta_value)
    kat_tuned.ETMY.phi = kat_tuned.ETMYAR.phi = etmy_tune(delta_value)
    kat_tuned.ITMY.phi = kat_tuned.ITMYAR.phi = itmy_tune(delta_value)
    kat_tuned.PRM.phi  = kat_tuned.PRMAR.phi  = prm_tune(delta_value)
    kat_tuned.SRM.phi                         = srm_tune(delta_value)

    # Inject freq noise into the lock-dragged IFO
    kat_tuned.parse(freq_inj_code)

    kat_tuned.IFO.suspend_mirrors_pitch()
    kat_tuned.IFO.suspend_mirrors_z()
    kat_tuned.removeBlock("locks")

    # Run the freq noise injected lock-dragged IFO code
    out_tuned = kat_tuned.run()

# # Display the cavity mismatches table
# mmx, mmy, qs = pykat.ifo.mismatch_cavities(kat_tuned, kat_tuned.L0.nodes[0].name)
# print()
# print('Cavity mode matching')
# print(mmx)
# print(mmy)
# print()


##################
### Print Info ###
##################

for key in out_dict.keys():
    print(key)
    print_HOMs(out_dict[key])


#####   Figures   #####
###  Frequency to darm tf budget  ###

fig, (s1) = plt.subplots(1)

for date in dates:
    temp_dict = data_dict[date]

    plot_ff_log = temp_dict['ff_log']
    plot_tf_log = temp_dict['tf_log']
    style = temp_dict['style']
    label = temp_dict['label']

    s1.loglog(plot_ff_log, np.abs(plot_tf_log), label=label, zorder=3)

p1, = s1.loglog(fflog, np.abs(tf1), label='arm reflectivity difference')
p2, = s1.loglog(fflog, np.abs(tf2), label='cavity pole difference')
p3, = s1.loglog(fflog, np.abs(tf3), label='schnupp asymmetry')
p4, = s1.loglog(fflog, np.abs(tf4), label='radiation pressure induced from darm offset')
p5, = s1.loglog(fflog, np.abs(tf5)) #label='Phenominological higher-order-mode coupling')
p6, = s1.loglog(fflog, np.abs(tf6), color=p5.get_color()) #label='Phenominological higher-order-mode coupling')

s1.fill_between(fflog, np.abs(tf6), np.abs(tf5), color=p5.get_color(), alpha=0.3, label='phenomenological higher-order-mode coupling')

p8, = s1.loglog(fflog, np.abs(tf_total), color='k', ls='--', label='total estimated coupling')
s1.loglog(fflog, np.abs(tf_total_low), color='k', ls='--')

# pp, = s1.loglog(finesse_imbalanced_ff, np.abs(finesse_imbalanced_tf), alpha=0.5,
#             label='Finesse Imbalanced ITMs')
# s1.loglog(finesse_imbalanced_ff, np.abs(finesse_imbalanced_tf_disp), ls='--', color=pp.get_color(),
#             label='Finesse Displacement Imbalanced')

# pp, = s1.loglog(finesse_balanced_ff, np.abs(finesse_balanced_tf), alpha=0.5,
#             label='Finesse Balanced ITMs')
# s1.loglog(finesse_balanced_ff, np.abs(finesse_balanced_tf_disp), ls='--', color=pp.get_color(),
#             label='Finesse Displacement Balanced')

# pp, = s1.loglog(finesse_tuned_ff, np.abs(finesse_tuned_tf), alpha=0.5,
#             label=r'Finesse %s = %f m '%(lock_drag_plot_label, new_value))
# s1.loglog(finesse_tuned_ff, np.abs(finesse_tuned_tf_disp), ls='--', color=pp.get_color(),
#             label=r'Finesse Displacement %s = %f m '%(lock_drag_plot_label, new_value))

# alpha = 0.5
# s1.loglog(fflog, np.abs(tf1_CE1), color=p1.get_color(), alpha=alpha,
#             label='Reflectivity difference')
# s1.loglog(fflog, np.abs(tf2_CE1), color=p2.get_color(), alpha=alpha,
#             label='Cavity pole difference')
# s1.loglog(fflog, np.abs(tf3_CE1), color=p3.get_color(), alpha=alpha,
#             label='Schnupp asymmetry')
# s1.loglog(fflog, np.abs(tf4_CE1), color=p4.get_color(), alpha=alpha,
#             label='Radiation pressure induced from darm offset')

# s1.loglog(fflog, np.abs(tf_total_CE1),  color=p5.get_color(), ls='--', alpha=alpha,
#             label='Total estimated coupling')

s1.set_xlim([fflog[0], fflog[-1]])
s1.set_ylim([1e-18, 1.01e-12])

# s1.set_title('Frequency to darm tf')
s1.set_ylabel(r'$|L_- / \delta \nu|$ ' + '[m/Hz]')
s1.set_xlabel('Frequency [Hz]')

s1.set_yticks(nu.good_ticks(s1))

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=14)

# Save the figure
plot_name = 'frequency_transfer_function_budget.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



###  Frequency tf just the measurement, including phase and coherence  ###
T1 = 0.32   
T2 = 0.014  
T3 = 4e-6
L = 3994.5 # m
Ls = 56.9 # m
darm_hf_corr = np.abs(darm_HF_correction(fflog, Ls, L, T1, T2, T3))

fig, (s1, s2, s3) = plt.subplots(3, sharex=True)

for date in dates:
    temp_dict = data_dict[date]

    plot_ff = temp_dict['ff']
    plot_tf = temp_dict['tf']
    plot_coh = temp_dict['coh']
    plot_ff_log = temp_dict['ff_log']
    plot_tf_log = temp_dict['tf_log']
    plot_coh_log = temp_dict['coh_log']
    style = temp_dict['style']
    label = temp_dict['label']

    p1, = s1.loglog(plot_ff, np.abs(plot_tf), alpha=0.5, label=label)
    s2.semilogx(plot_ff, 180/np.pi*np.angle(plot_tf), alpha=0.5)
    s3.semilogx(plot_ff, plot_coh, alpha=0.5)

    s1.loglog(plot_ff_log, np.abs(plot_tf_log), color=p1.get_color(), label=label + ' logbinned')
    s2.semilogx(plot_ff_log, 180/np.pi*np.angle(plot_tf_log), color=p1.get_color())
    s3.semilogx(plot_ff_log, plot_coh_log, color=p1.get_color())

    # # Corrections
    # s1.loglog(fflog, np.abs(tflog_apr) * darm_hf_corr, 
    #             label='Corrected Logbinned')
    # s1.loglog(fflog, np.abs(tflog_nov) * darm_hf_corr, 
    #             label='Corrected Logbinned')

s1.set_ylim([5e-17, 1.01e-12])
s1.set_yticks(nu.good_ticks(s1))

# s1.set_title('LHO Frequency to DARM TF')
s1.set_ylabel('Mag [m/Hz]')
s2.set_ylabel('Phase [degs]')
s3.set_ylabel('Coherent Power')
s3.set_xlabel('Frequency [Hz]')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.6)
s3.grid()
s3.grid(which='minor', ls='--', alpha=0.6)

s1.legend(fontsize=16)

# Save the Figure
plot_name = 'frequency_tf_LHO_measurements.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Finesse frequency to darm tf collection
fig, (s1) = plt.subplots(1)

for date in dates:
    temp_dict = data_dict[date]

    plot_ff_log = temp_dict['ff_log']
    plot_tf_log = temp_dict['tf_log']
    style = temp_dict['style']
    label = temp_dict['label']

    s1.loglog(plot_ff_log, np.abs(plot_tf_log), label=label)

for key in out_dict.keys():
    name = key.replace('_', ' ')
    ff_plot, tf_plot, tf_disp_plot, tf_X_plot, tf_Y_plot = extract_finesse_results(out_dict[key])

    Y_arm_power = Y_arm_power = np.real(out_dict[key]['pdYDC'][0])
    pp, = s1.loglog(ff_plot, np.abs(tf_plot), alpha=0.5,
                label=f'{name}, Y Arm = {Y_arm_power:.0f} W')
    # s1.loglog(ff_plot, np.abs(tf_disp_plot), ls='--', color=pp.get_color(),)
    #             # label=f'Disp {name}')

s1.set_xlim([fflog[0], fflog[-1]])
s1.set_ylim([5e-21, 1.01e-10])

# s1.set_title('Frequency to darm tf')
s1.set_ylabel('Frequency to DARM TF [m/Hz]')
s1.set_xlabel('Frequency [Hz]')

s1.set_yticks(nu.good_ticks(s1))

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='lower left', fontsize=8)

# Save the figure
plot_name = f'frequency_tf_finesse_collection_{plot_suffix}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
make_interactive_svg(fig, full_plot_name.replace('.pdf', ''))
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()

###  Frequency to Arm Power tf finesse  ###
fig, (s1) = plt.subplots(1)

for key in out_dict.keys():
    name = key.replace('_', ' ')
    ff_plot, tf_plot, tf_disp_plot, tf_X_plot, tf_Y_plot = extract_finesse_results(out_dict[key])

    X_arm_power = np.real(out_dict[key]['pdXDC'][0])
    Y_arm_power = np.real(out_dict[key]['pdYDC'][0])

    pp, = s1.loglog(ff_plot, np.abs(tf_X_plot), alpha=0.5,
                label=f'{name}, Y Arm = {Y_arm_power:.0f} W')
    # s1.loglog(ff_plot, np.abs(tf_Y_plot), ls='--', color=pp.get_color(),)
                # label=f'{name}, Y Arm = {Y_arm_power:.0f} W')

s1.set_xlim([fflog[0], fflog[-1]])
# s1.set_ylim([5e-21, 1.01e-10])

# s1.set_title('Frequency to darm tf')
s1.set_ylabel('Frequency to Arm Power TF [W/Hz]')
s1.set_xlabel('Frequency [Hz]')

s1.set_yticks(nu.good_ticks(s1))

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.6)

s1.legend(loc='lower left', fontsize=8)

# Save the figure
plot_name = f'frequency_tf_finesse_frequency_to_arm_power_tf_{plot_suffix}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
make_interactive_svg(fig, full_plot_name.replace('.pdf', ''))
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()