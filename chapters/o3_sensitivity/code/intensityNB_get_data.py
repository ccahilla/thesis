'''
Acquire the data to make the plots in intensityNB.py,
and store in small .txts.

Also makes auxilliary plots, but not the main noisebudget ones.

Craig Cahillane
August 5th, 2020
'''
import sys
import os
import time
import pickle
import gpstime
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt

import scipy.constants as scc
import scipy.signal as sig
import nds2

from dataUtils import *

start_time = time.time()

mpl.rcParams.update({'text.usetex': True,
                     'figure.figsize': (12, 9),
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9,
                     'agg.path.chunksize': 10000})


#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Set up data directory   #####
data_dir = f'{script_dir}/data'

#####   Functions   #####
# Freerunning Laser Intensity Noise 
def nproRIN(fff):
    '''Takes in frequency vector.  Returns approximate freerunning NPRO laser RIN vector.
    Reference: https://www.osapublishing.org/oe/abstract.cfm?uri=oe-20-10-10617 (Fig 7)'''
    return 1e-3/fff

def save_txt(fff, psd, filename, header, verbose=True):
    '''Stores a two-column txt of frequency and power spectral density
    fff = frequency vector in Hertz.  Must be np.array with single row of values.
    psd = power spectral density [units^2/Hz].  Must be np.array of same length as fff. 
    filename = full directory path to the file and filename
    header = text to go at the top of the txt
    '''
    save_data = np.vstack((fff, psd)).T
    if verbose:
        print()
        print(f'Saving data file...')
    np.savetxt(filename, save_data, header=header)
    if verbose:
        print(f'{filename}')
    return

#####   Retrieve Intensity Noise Data   #####

# Channels to get data from using nds2
fast_channels = np.array([
    'H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ',
    'H1:PSL-ISS_SECONDLOOP_RIN_INNER_OUT_DQ',
    'H1:PSL-ISS_PDA_REL_OUT_DQ',
    'H1:PSL-ISS_PDB_REL_OUT_DQ',
    'H1:LSC-REFL_A_RIN_OUT_DQ',
    'H1:LSC-REFL_B_RIN_OUT_DQ',
    'H1:SUS-MC2_M3_ISCINF_L_IN1_DQ',
    'H1:IMC-MC1_PIT_OUT_DQ',
    'H1:IMC-MC2_PIT_OUT_DQ',
    # 'H1:IMC-MC3_PIT_OUT_DQ', # take out MC3 because it's the same as MC1
    'H1:IMC-PZT_PIT_OUT_DQ',
    'H1:IMC-MC1_YAW_OUT_DQ',
    'H1:IMC-MC2_YAW_OUT_DQ',
    # 'H1:IMC-MC3_YAW_OUT_DQ',
    'H1:IMC-PZT_YAW_OUT_DQ',
    'H1:PSL-ISS_SECONDLOOP_QPD_PIT_OUT_DQ',
    'H1:PSL-ISS_SECONDLOOP_QPD_YAW_OUT_DQ',
    'H1:IMC-IM4_TRANS_PIT_OUT_DQ',
    'H1:IMC-IM4_TRANS_YAW_OUT_DQ',
    'H1:IMC-IM4_TRANS_NSUM_OUT_DQ',
])
fast_channels_labels = np.array([
    'Out-of-loop intensity noise',
    'In-loop intensity noise',
    '1st ISS loop out-of-loop sensor',
    '1st ISS loop in-loop sensor',
    'REFL A LF RIN',
    'REFL B LF RIN',
    'IMC length control',
    'MC1 + MC3 Pitch Control',
    'MC2 Pitch Control',
    # 'H1:IMC-MC3_PIT_OUT_DQ', # take out MC3 because it's the same as MC1
    'IMC PZT Pitch Control',
    'MC1 + MC3 Yaw Control',
    'MC2 Yaw Control',
    # 'H1:IMC-MC3_YAW_OUT_DQ',
    'IMC PZT Yaw Control',
    'ISS QPD Pitch Sensor',
    'ISS QPD Yaw Sensor',
    'IM4 QPD Pitch Sensor',
    'IM4 QPD Yaw Sensor',
    'IM4 QPD Sum Sensor',
])
labels_dict = dict(zip(fast_channels, fast_channels_labels))

# Channel traces to exclude from main noisebudget
exclude_channels = np.array([
    'H1:LSC-REFL_A_RIN_OUT_DQ',
    'H1:LSC-REFL_B_RIN_OUT_DQ',
    'H1:PSL-ISS_PDA_REL_OUT_DQ',
    'H1:PSL-ISS_PDB_REL_OUT_DQ',
    'H1:IMC-IM4_TRANS_PIT_OUT_DQ',
    'H1:IMC-IM4_TRANS_YAW_OUT_DQ',
    'H1:IMC-IM4_TRANS_NSUM_OUT_DQ',
    'H1:SUS-MC2_M3_ISCINF_L_IN1_DQ',

])
imc_asc_channels = np.array([
    'H1:IMC-MC1_PIT_OUT_DQ',
    'H1:IMC-MC2_PIT_OUT_DQ',
    # 'H1:IMC-MC3_PIT_OUT_DQ', # take out MC3 because it's the same as MC1
    'H1:IMC-PZT_PIT_OUT_DQ',
    'H1:IMC-MC1_YAW_OUT_DQ',
    'H1:IMC-MC2_YAW_OUT_DQ',
    # 'H1:IMC-MC3_YAW_OUT_DQ',
    'H1:IMC-PZT_YAW_OUT_DQ',
])
post_imc_asc_channels = np.array([
    'H1:PSL-ISS_SECONDLOOP_QPD_PIT_OUT_DQ',
    'H1:PSL-ISS_SECONDLOOP_QPD_YAW_OUT_DQ',
    # 'H1:IMC-IM4_TRANS_PIT_OUT_DQ',
    # 'H1:IMC-IM4_TRANS_YAW_OUT_DQ',
    # 'H1:IMC-IM4_TRANS_NSUM_OUT_DQ',
])

# 1st loop channels
first_loop_channels = np.array([
    'H1:PSL-ISS_PDA_REL_OUT_DQ',
    'H1:PSL-ISS_PDB_REL_OUT_DQ',
    'H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ',
])

# Channels to use in the RIN-only plot
rin_channels = np.array([
    'H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ',
    'H1:PSL-ISS_SECONDLOOP_RIN_INNER_OUT_DQ',
    # 'H1:PSL-ISS_PDA_REL_OUT_DQ',
    'H1:PSL-ISS_PDB_REL_OUT_DQ',
    # 'H1:LSC-REFL_A_RIN_OUT_DQ',
    'H1:LSC-REFL_B_RIN_OUT_DQ',
])
# Channels to calibrate into RIN using their TFs with OUTER where coherence is high
calib_channels = np.array([
    'H1:SUS-MC2_M3_ISCINF_L_IN1_DQ',
    'H1:IMC-MC1_PIT_OUT_DQ',
    'H1:IMC-MC2_PIT_OUT_DQ',
    # 'H1:IMC-MC3_PIT_OUT_DQ', # take out MC3 because it's the same as MC1
    'H1:IMC-PZT_PIT_OUT_DQ',
    'H1:IMC-MC1_YAW_OUT_DQ',
    'H1:IMC-MC2_YAW_OUT_DQ',
    # 'H1:IMC-MC3_YAW_OUT_DQ',
    'H1:IMC-PZT_YAW_OUT_DQ',
    'H1:PSL-ISS_SECONDLOOP_QPD_PIT_OUT_DQ',
    'H1:PSL-ISS_SECONDLOOP_QPD_YAW_OUT_DQ',
    'H1:IMC-IM4_TRANS_PIT_OUT_DQ',
    'H1:IMC-IM4_TRANS_YAW_OUT_DQ',
    'H1:IMC-IM4_TRANS_NSUM_OUT_DQ',
])
# Channels for calculating the shot noise contribution
slow_channels = np.array([
    'H1:PSL-ISS_SECONDLOOP_PDSUMINNER_OUT16',
    'H1:PSL-ISS_SECONDLOOP_PDSUMOUTER_OUT16',
    'H1:PSL-ISS_PDA_OUT16',
    'H1:PSL-ISS_PDB_OUT16',
    'H1:LSC-REFL_A_LF_OUT16',
    'H1:LSC-REFL_B_LF_OUT16',
]) # mW

coherence_threshold = 0.2 # cutoff for plotting the noisebudget contributions

host_server = 'nds.ligo.caltech.edu' #'nds.ligo-wa.caltech.edu' # NDS network at LHO
port_number = 31200
allow_data_on_tape = 'True' # switch to 'True' if we want really old data

gps_starts = np.array([
    1246927918, # second loop locked,
    1247509048, # second loop unlocked gpstimes
])
titles = np.array([
    'ISS 1st and 2nd Loop Locked',
    'ISS 1st Loop Locked 2nd Loop Unlocked',
])
averages = np.array([
    50,
    100,
])
binwidths = np.array([
    0.01,
    0.1,
])
overlaps = np.array([
    0.5,
    0.5,
])
main_chans = np.array([
    'H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ',
    'H1:PSL-ISS_PDA_REL_OUT_DQ',
])

dataInfos = {}
for gs, avg, bw, ov, tt, mc in zip(gps_starts, averages, binwidths, overlaps, titles, main_chans):
    dataInfos[gs] = {}
    dataInfos[gs]['averages'] = avg
    dataInfos[gs]['binwidth'] = bw
    dataInfos[gs]['overlap'] = ov
    dataInfos[gs]['title'] = tt
    dataInfos[gs]['main_chan'] = mc


dataDicts = {}
for gps_start, avg, bw, ov in zip(gps_starts, averages, binwidths, overlaps):
    gps_start = int(gps_start)
    retrive_fast_data_with_nds2 = False

    duration = int(dttTime(avg, bw, ov, verbose=True))
    gps_stop = gps_start + duration

    # Read from intensityNB.pkl if possible 
    data_path = '/Volumes/SandiskSD/ccahilla/Git/o3-commissioning-paper/data'
    pickle_filename = 'intensityNB_GPS_start_{}.pkl'.format(gps_start)
    full_filename = f'{data_path}/{pickle_filename}'
    if os.path.isfile(full_filename):
        print()
        print('Reading data file at {}'.format(full_filename))
        print()
        dataDict = loadPickle(full_filename)
    else:
        retrive_fast_data_with_nds2 = True

    # Check if all parameters are the same
    if not retrive_fast_data_with_nds2:
        chan0 = fast_channels[0]
        try:
            dataDict[chan0]
            if not gps_start == dataDict[chan0]['gpsStart']:
                retrive_fast_data_with_nds2 = True
            elif not bw == dataDict[chan0]['df']:
                retrive_fast_data_with_nds2 = True
            elif not len(fast_channels) == len(dataDict.keys()):
                retrive_fast_data_with_nds2 = True
            elif not duration == len(dataDict[chan0]['data'])/dataDict[chan0]['fs']:
                retrive_fast_data_with_nds2 = True
            else:
                print('All parameter checks passed, no reading using NDS2')
        except KeyError:
            retrive_fast_data_with_nds2 = True

    # Fetch Data using nds2
    # Should take about 120 seconds the first time.
    # After the first time, this script automatically saves a large .pkl file in your local ./data/ directory,
    # containing the intensity noise data.
    # This will make future runs much faster, so long as the data retrieved is the same.'''
    if retrive_fast_data_with_nds2:

        buffers = acquireData(fast_channels, gps_start, gps_stop, host_server, port_number, allow_data_on_tape) # get data
        dataDict = extractDict(buffers, duration) # put data into convenient dicitionary
        dataDict = dataCSDs(dataDict, avg, bw, ov) # Take ASDs of all chans in dictionary

        savePickle(dataDict, full_filename) # Save the dictionary as a .pkl file for quick running of the script next time

    dataDicts[gps_start] = dataDict

# Get slow channels, no need to save this data or take ASDs 
slowDicts = {}
for gps_start in gps_starts:
    gps_start = int(gps_start)
    slow_buffers = acquireData(slow_channels, gps_start, gps_start+1, host_server, port_number, allow_data_on_tape, return_nds2_buffers=True)
    slowDict = extractDict(slow_buffers, duration=1)

    slowDicts[gps_start] = {}
    slowDicts[gps_start] = slowDict

print('Finished getting data in {} seconds'.format(time.time() - start_time))


# Intensity Shot Noise Limit
wavelength = 1064e-9 # m
nu0 = scc.c / wavelength
for gps_start in gps_starts:
    slowDict = slowDicts[gps_start]

    pda_power = np.median(slowDict['H1:PSL-ISS_PDA_OUT16']['data']) * 1e-3 # chan in mW, calibrate into W
    pda_shot_noise = np.sqrt(2 * scc.Planck * nu0 * pda_power)/pda_power # (W/W)/rtHz, single PD

    pdb_power = np.median(slowDict['H1:PSL-ISS_PDB_OUT16']['data']) * 1e-3 # chan in mW, calibrate into W
    pdb_shot_noise = np.sqrt(2 * scc.Planck * nu0 * pda_power)/pdb_power # (W/W)/rtHz, single PD

    inner_power = np.median(slowDict['H1:PSL-ISS_SECONDLOOP_PDSUMINNER_OUT16']['data']) * 1e-3 # chan in mW, calibrate into W
    split_inner_power = inner_power / 4.0 # ISS splits the shot noise over four PDs, but retains all the signal
    inner_shot_noise_single_pd = np.sqrt(2 * scc.Planck * nu0 * split_inner_power)/split_inner_power # (W/W)/rtHz, single PD
    inner_shot_noise = inner_shot_noise_single_pd / 2 # divide by sqrt(4) for the number of PDs in-loop

    outer_power = np.median(slowDict['H1:PSL-ISS_SECONDLOOP_PDSUMOUTER_OUT16']['data']) * 1e-3 # chan in mW, calibrate into W
    split_outer_power = outer_power / 4.0 # ISS splits the shot noise over four PDs, but retains all the signal
    outer_shot_noise_single_pd = np.sqrt(2 * scc.Planck * nu0 * split_outer_power)/split_outer_power # (W/W)/rtHz
    outer_shot_noise = outer_shot_noise_single_pd / 2

    total_shot_noise = np.sqrt(inner_shot_noise**2 + outer_shot_noise**2)

    refl_a_power = np.median(slowDict['H1:LSC-REFL_A_LF_OUT16']['data']) * 1e-3 # W
    refl_a_shot_noise = np.sqrt(2 * scc.Planck * nu0 * refl_a_power)/refl_a_power

    refl_b_power = np.median(slowDict['H1:LSC-REFL_B_LF_OUT16']['data']) * 1e-3 # W
    refl_b_shot_noise = np.sqrt(2 * scc.Planck * nu0 * refl_b_power)/refl_b_power

    slowDict['H1:PSL-ISS_PDA_OUT16']['shot_noise'] = pda_shot_noise
    slowDict['H1:PSL-ISS_PDB_OUT16']['shot_noise'] = pdb_shot_noise
    slowDict['H1:PSL-ISS_SECONDLOOP_PDSUMINNER_OUT16']['shot_noise'] = inner_shot_noise
    slowDict['H1:PSL-ISS_SECONDLOOP_PDSUMOUTER_OUT16']['shot_noise'] = outer_shot_noise
    slowDict['H1:LSC-REFL_A_LF_OUT16']['shot_noise'] = refl_a_shot_noise
    slowDict['H1:LSC-REFL_B_LF_OUT16']['shot_noise'] = refl_b_shot_noise

    print()
    print('Inner Power = {:.3e} W'.format(inner_power))
    print('Outer Power = {:.3e} W'.format(outer_power))
    print()
    print('Inner Shot Noise = {:.3e} (W/W)/rtHz'.format(inner_shot_noise))
    print('Outer Shot Noise = {:.3e} (W/W)/rtHz'.format(outer_shot_noise))
    print('Total Shot Noise = {:.3e} (W/W)/rtHz'.format(total_shot_noise))
    print()
    print('REFL A Shot Noise = {:.3e} (W/W)/rtHz'.format(refl_a_shot_noise))
    print('REFL B Shot Noise = {:.3e} (W/W)/rtHz'.format(refl_b_shot_noise))
    print()

plotted_filenames = np.array([])

# Set the data gathered correctly
gps_start = gps_starts[0] # The first gps_start refers to both ISS loops being locked
dataDict = dataDicts[gps_start]
slowDict = slowDicts[gps_start]
dataInfo = dataInfos[gps_start]

inner_shot_noise = slowDict['H1:PSL-ISS_SECONDLOOP_PDSUMINNER_OUT16']['shot_noise']
outer_shot_noise = slowDict['H1:PSL-ISS_SECONDLOOP_PDSUMOUTER_OUT16']['shot_noise']
total_shot_noise = np.sqrt(inner_shot_noise**2 + outer_shot_noise**2)
pdb_shot_noise = slowDict['H1:PSL-ISS_PDB_OUT16']['shot_noise']

main_chan = dataInfo['main_chan']
utc_start_str = gpstime.tconvert(gps_start)
title = dataInfo['title']
filename_suffix = title.replace(' ', '_')
averages = dataInfo['averages']
binwidth = dataInfo['binwidth']
overlap = dataInfo['overlap']

fflog = np.logspace(np.log10(1e-2), np.log10(8192.0), 1000) # for log binning the data

imc_asc_PSDs = np.array([]) # make some arrays of zeros, use PSDs for easy addition
post_imc_asc_PSDs = np.array([])


#####   Save data in txts   #####


#####   Figures   #####
# Main noisebudget
fig = plt.figure()
s1 = fig.add_subplot(111)

for chan, value in dataDict.items():
    label = labels_dict[chan]
    ff = value['ff']
    ASD = value['ASD']
    PSD = value['PSD']
    if chan == main_chan:
        smartff, smartASD = linear_log_ASD(fflog, ff, ASD)
        p1, = s1.loglog(smartff, smartASD, label=label, zorder=10)

        filename_label = label.replace(' ', '_').replace('-', '_')
        filename = f'{data_dir}/intensityNB_{filename_label}.txt'
        header = f'Frequency [Hz], {label} asd [RIN/rtHz]'
        save_txt(ff, ASD, filename, header)

        # rms = RMS(ff, ASD)
        # s1.loglog(ff, rms, color=p1.get_color(), ls='--', zorder=10,
        #        label=label+' RMS = {:.1e} RIN'.format(rms[0]))
        continue

    if chan in calib_channels:
        TF = dataDict[main_chan][chan]['TF']
        coh = dataDict[main_chan][chan]['coh']

        ASD = ASD * np.abs(TF) * np.sqrt(coh)  # calibrate and normalize for noise (see page 36 of iPad Lab Notebook)
        PSD = PSD * np.abs(TF)**2 * coh

    if chan in imc_asc_channels:
        if len(imc_asc_PSDs) == 0:
            imc_asc_ff = np.copy(ff)
            imc_asc_PSDs = PSD
        else:
            imc_asc_PSDs = np.vstack((imc_asc_PSDs, PSD))
    elif chan in post_imc_asc_channels:
        if len(post_imc_asc_PSDs) == 0:
            post_imc_asc_ff = np.copy(ff)
            post_imc_asc_PSDs = PSD
        else:
            post_imc_asc_PSDs = np.vstack((post_imc_asc_PSDs, PSD))
    elif chan not in exclude_channels:
        smartff, smartASD = linear_log_ASD(fflog, ff, ASD)
        s1.loglog(smartff, smartASD, label=label)

        filename_label = label.replace(' ', '_').replace('-', '_')
        filename = f'{data_dir}/intensityNB_{filename_label}.txt'
        header = f'Frequency [Hz], {label} asd [RIN/rtHz]'
        save_txt(ff, ASD, filename, header)

imc_asc_PSD = np.amax(imc_asc_PSDs, axis=0)
post_imc_asc_PSD = np.amax(post_imc_asc_PSDs, axis=0)

imc_asc_ASD = np.sqrt(imc_asc_PSD)
post_imc_asc_ASD = np.sqrt(post_imc_asc_PSD)

imc_asc_smartff, imc_asc_smartASD = linear_log_ASD(fflog, imc_asc_ff, imc_asc_ASD)
post_imc_asc_smartff, post_imc_asc_smartASD = linear_log_ASD(fflog, post_imc_asc_ff, post_imc_asc_ASD)

# save post imc jitter txt
label = 'Post-input mode cleaner beam jitter'
filename_label = label.replace(' ', '_').replace('-', '_')
filename = f'{data_dir}/intensityNB_{filename_label}.txt'
header = f'Frequency [Hz], {label} asd [RIN/rtHz]'
save_txt(post_imc_asc_ff, post_imc_asc_ASD, filename, header)

#s1.loglog(imc_asc_smartff, imc_asc_smartASD, label='IMC angular controls')
end_index = 590
s1.loglog(post_imc_asc_smartff[:end_index], post_imc_asc_smartASD[:end_index], label=label)

shortff = np.array( [ ff[1], s1.get_xlim()[1] ] )
# s1.loglog(shortff, nproRIN(shortff), ls='--',  alpha=0.75, label='Approx Freerunning NPRO RIN')
#s1.loglog(shortff, inner_shot_noise*np.ones(2), label='In-loop shot noise')
# s1.loglog(shortff, pdb_shot_noise*np.ones(2), ls='--', alpha=0.75, label='1st Loop In-loop Shot Noise')

# save shot noise txt
label = 'Shot noise'
filename_label = label.replace(' ', '_').replace('-', '_')
filename = f'{data_dir}/intensityNB_{filename_label}.txt'
header = f'Frequency [Hz], {label} asd [RIN/rtHz]'
save_txt(shortff, total_shot_noise*np.ones(2), filename, header)

s1.loglog(shortff, total_shot_noise*np.ones(2), label=label)


s1.set_ylim([1e-10, 2e-7])
# s1.set_yticks(goodTicks(s1))
s1.set_xlim([10, 5000])

s1.set_xlabel('Frequency [Hz]')
s1.set_ylabel(r'Relative Intensity ASD [$ 1 / \sqrt{\mathrm{Hz}} $]')

s1.legend(loc='upper right', ncol=1)
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.3)

plot_name = f'intensityNB.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Plot the 1st loop noisebudget 
gps_start = gps_starts[1] # The first gps_start refers to both ISS loops being locked
dataDict = dataDicts[gps_start]
slowDict = slowDicts[gps_start]
dataInfo = dataInfos[gps_start]

main_chan = dataInfo['main_chan']
utc_start_str = gpstime.tconvert(gps_start)
title = dataInfo['title']
filename_suffix = title.replace(' ', '_')
averages = dataInfo['averages']
binwidth = dataInfo['binwidth']
overlap = dataInfo['overlap']

inner_shot_noise = slowDict['H1:PSL-ISS_SECONDLOOP_PDSUMINNER_OUT16']['shot_noise']
pdb_shot_noise = slowDict['H1:PSL-ISS_PDB_OUT16']['shot_noise']

fig = plt.figure()
s1 = fig.add_subplot(111)

for chan, value in dataDict.items():
    label = labels_dict[chan]
    ff = value['ff']
    ASD = value['ASD']
    if chan == main_chan:
        rms = RMS(ff, ASD)
        s1.loglog(ff, rms, color='black', ls='--', lw=2, alpha=0.75, zorder=10, rasterized=True,
                label=label+' RMS = {:.1e} RIN'.format(rms[0]))
        s1.loglog(ff, ASD, color='black', lw=2, alpha=0.75, label=label, zorder=10, rasterized=True)
        continue

    if chan in first_loop_channels:
        s1.loglog(ff, ASD, lw=2, alpha=0.75, label=label, rasterized=True)

shortff = np.array( [ ff[1], s1.get_xlim()[1] ] )
s1.loglog(shortff, nproRIN(shortff), ls='--',  alpha=0.75, label='Approx Freerunning NPRO RIN')
s1.loglog(shortff, inner_shot_noise*np.ones(2), ls='--', alpha=0.75, label='2nd Loop In-loop Shot Noise')
s1.loglog(shortff, pdb_shot_noise*np.ones(2), ls='--', alpha=0.75, label='1st Loop In-loop Shot Noise')

s1.set_ylim([1e-10, 1e-4])
# s1.set_yticks(goodTicks(s1))

s1.set_xlabel('Frequency [Hz]')
s1.set_ylabel(r'ASD [$\mathrm{RIN} / \sqrt{\mathrm{Hz}} $]')
# Remove the two below for the actual paper plot
s1.set_title('LHO Intensity Noisebudget\n {}'.format(title))
fig.text(0.01, 0.01,
        'Date = {date}, GPS Start = {gps}, Averages = {avg}, Binwidth = {bw} Hz, Overlap = {ov} %'.format(
        date=utc_start_str, gps=gps_start, avg=averages, bw=binwidth, ov=overlap
        ),
        fontsize=10)

s1.legend(loc='upper right', ncol=2, fontsize=12)
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.3)

plot_name = f'intensityNB_{filename_suffix}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()

# Plot TFS and coherences 

gps_start = gps_starts[0] # The first gps_start refers to both ISS loops being locked
dataDict = dataDicts[gps_start]
slowDict = slowDicts[gps_start]
dataInfo = dataInfos[gps_start]

main_chan = dataInfo['main_chan']
utc_start_str = gpstime.tconvert(gps_start)
title = dataInfo['title']
filename_suffix = title.replace(' ', '_')
averages = dataInfo['averages']
binwidth = dataInfo['binwidth']
overlap = dataInfo['overlap']

fig, (s1, s2, s3) = plt.subplots(3, 1, sharex=True)
value2 = dataDict[main_chan]

for chan in fast_channels:
    if chan == main_chan:
        continue
    label = '{}/{}'.format(chan.replace('_', ' '), main_chan.replace('_',' '))
    ff = value2[chan]['ff']
    TF = value2[chan]['TF']
    coh = value2[chan]['coh']

    s1.loglog(ff, np.abs(TF), lw=2, alpha=0.75, label=label, rasterized=True)
    s2.semilogx(ff, 180/np.pi*np.angle(TF), lw=2, alpha=0.75, label=label, rasterized=True)
    s3.loglog(ff, coh, lw=2, alpha=0.75, label=label, rasterized=True)

s1.set_yticks(goodTicks(s1))
s2.set_yticks(90*np.arange(-2,3))
s3.set_ylim([1e-1, 1])

s3.set_xlabel('Frequency [Hz]')
s1.set_ylabel(r'Mag TF')
s2.set_ylabel(r'Angle TF [degs]')
s3.set_ylabel(r'Power Coherence')
# Remove the two below for the actual paper plot
s1.set_title('LHO Intensity Coherences\n {}'.format(title))
fig.text(0.01, 0.01,
        'Date = {date}, GPS Start = {gps}, Averages = {avg}, Binwidth = {bw} Hz, Overlap = {ov} %'.format(
        date=utc_start_str, gps=gps_start, avg=averages, bw=binwidth, ov=overlap
        ),
        fontsize=10)

s2.legend(loc='lower right', fontsize=10)

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.3)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.3)
s3.grid()
s3.grid(which='minor', ls='--', alpha=0.3)

fig.subplots_adjust(hspace=0)

plot_name = f'intensityNB_coherences_{filename_suffix}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()


# Plot only RIN 

gps_start = gps_starts[0]
dataDict = dataDicts[gps_start]
slowDict = slowDicts[gps_start]
dataInfo = dataInfos[gps_start]

inner_power  = np.median(slowDict['H1:PSL-ISS_SECONDLOOP_PDSUMINNER_OUT16']['data'])
pdb_power    = np.median(slowDict['H1:PSL-ISS_PDB_OUT16']['data'])
refl_b_power = np.median(slowDict['H1:LSC-REFL_B_LF_OUT16']['data'])
inner_shot_noise  = slowDict['H1:PSL-ISS_SECONDLOOP_PDSUMINNER_OUT16']['shot_noise']
pdb_shot_noise    = slowDict['H1:PSL-ISS_PDB_OUT16']['shot_noise']
refl_b_shot_noise = slowDict['H1:LSC-REFL_B_LF_OUT16']['shot_noise']

main_chan = dataInfo['main_chan']
utc_start_str = gpstime.tconvert(gps_start)
title = dataInfo['title']
filename_suffix = title.replace(' ', '_')
averages = dataInfo['averages']
binwidth = dataInfo['binwidth']
overlap = dataInfo['overlap']

fig = plt.figure()
s1 = fig.add_subplot(111)

for chan, value in dataDict.items():
    label = labels_dict[chan]
    ff = value['ff']
    ASD = value['ASD']
    if chan == main_chan:
        rms = RMS(ff, ASD)
        s1.loglog(ff, rms, color='black', ls='--', lw=2, alpha=0.75, zorder=10, rasterized=True,
                label=label+' RMS = {:.1e} RIN'.format(rms[0]))
        s1.loglog(ff, ASD, color='black', lw=2, alpha=0.75, label=label, zorder=10, rasterized=True)
        continue

    if chan in rin_channels:
        if 'PDA' in chan:
            continue
        s1.loglog(ff, ASD, lw=2, alpha=0.75, rasterized=True, label=label)

dataDict_1stloop = dataDicts[gps_starts[1]]
ff = dataDict_1stloop['H1:PSL-ISS_PDA_REL_OUT_DQ']['ff']
ASD = dataDict_1stloop['H1:PSL-ISS_PDA_REL_OUT_DQ']['ASD']
label = '1st ISS Loop out-of-loop-sensor\n Only 1st Loop Locked'
s1.loglog(ff, ASD, lw=2, alpha=0.75, rasterized=True, label=label)

UGF = 2.8e4 # Hz
ff1 = ff[1:]
OLG = simpleOLG(ff1, UGF) # no phase dep at all
suppressedASD = ASD[1:] / (1 + OLG)
s1.loglog(ff1, suppressedASD, lw=2, alpha=0.75, zorder=10, rasterized=True,
            label=label+'+ 28 kHz 2nd loop suppression')

shortff = np.array( [ ff[1], s1.get_xlim()[1] ] )
s1.loglog(shortff, nproRIN(shortff), ls='--',  alpha=0.75, label='Approx Freerunning NPRO RIN')
s1.loglog(shortff, inner_shot_noise*np.ones(2), ls='--', alpha=0.75,
        label='2nd Loop In-loop Shot Noise ($P_{dc} = %.1f$ mW)'%(inner_power*1e3))
# s1.loglog(shortff, outer_shot_noise*np.ones(2), alpha=0.75, label='Out-of-loop Shot Noise')
s1.loglog(shortff, pdb_shot_noise*np.ones(2), ls='--', alpha=0.75,
        label='1st Loop In-loop Shot Noise ($P_{dc} = %.1f$ mW)'%(pdb_power*1e3))
s1.loglog(shortff, refl_b_shot_noise*np.ones(2), ls='--', alpha=0.75,
        label='REFL B LF Shot Noise ($P_{dc} = %.1f$ mW)'%(refl_b_power*1e3))

s1.set_ylim([1e-10, 1e-3])
# s1.set_yticks(goodTicks(s1))

s1.set_xlabel('Frequency [Hz]')
s1.set_ylabel(r'ASD [$\mathrm{RIN} / \sqrt{\mathrm{Hz}} $]')
# Remove the two below for the actual paper plot
s1.set_title('LHO Intensity Witnesses\n {}'.format(title))

s1.legend(loc='upper right', ncol=2, fontsize=12)
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.3)

plot_name = f'intensityNB_coherences_{filename_suffix}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()


# Plot RIN witnesses coherences

gps_start = gps_starts[0]
dataDict = dataDicts[gps_start]
slowDict = slowDicts[gps_start]
dataInfo = dataInfos[gps_start]

main_chan = dataInfo['main_chan']
utc_start_str = gpstime.tconvert(gps_start)
title = dataInfo['title']
filename_suffix = title.replace(' ', '_')
averages = dataInfo['averages']
binwidth = dataInfo['binwidth']
overlap = dataInfo['overlap']

fig, (s1, s2, s3) = plt.subplots(3, 1, sharex=True)

value2 = dataDict[main_chan]
for chan in rin_channels:
    if chan == main_chan:
        continue
    label = '{}/{}'.format(chan.replace('_', ' '), main_chan.replace('_',' '))
    ff = value2[chan]['ff']
    TF = value2[chan]['TF']
    coh = value2[chan]['coh']

    s1.loglog(ff, np.abs(TF), lw=2, alpha=0.75, rasterized=True, label=label)
    s2.semilogx(ff, 180/np.pi*np.angle(TF), lw=2, alpha=0.75, rasterized=True, label=label)
    s3.loglog(ff, coh, lw=2, alpha=0.75, rasterized=True, label=label)


s1.set_yticks(goodTicks(s1))
s2.set_yticks(90*np.arange(-2,3))
s3.set_ylim([1e-1, 1])

s3.set_xlabel('Frequency [Hz]')
s1.set_ylabel(r'Mag TF')
s2.set_ylabel(r'Angle TF [degs]')
s3.set_ylabel(r'Power Coherence')

# Remove the two below for the actual paper plot
s1.set_title('LHO Intensity Coherences\n {}'.format(title))
fig.text(0.01, 0.01,
        'Date = {date}, GPS Start = {gps}, Averages = {avg}, Binwidth = {bw} Hz, Overlap = {ov} %'.format(
        date=utc_start_str, gps=gps_start, avg=averages, bw=binwidth, ov=overlap
        ),
        fontsize=10)

s2.legend(loc='lower right', fontsize=10)

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.3)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.3)
s3.grid()
s3.grid(which='minor', ls='--', alpha=0.3)

fig.subplots_adjust(hspace=0)

plot_name = f'intensityNB_RINwitnesses_coherences_{filename_suffix}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()

# Print command to open all plots generated by this script
print()
print('Command to open plots:')
command = 'open'
for pf in plot_names:
    command += ' {}'.format(pf)
print(command)
print()
