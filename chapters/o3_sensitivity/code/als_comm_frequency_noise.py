'''
als_comm_frequency_noise.py

Plots the ALS COMM noise 

Craig Cahillane
December 29, 2020
'''
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc

import nds2utils as nu

import dtt2hdf

mpl.rcParams.update({   'figure.figsize': (12, 9),
                        'text.usetex': True,
                        'font.family': 'serif',
                        # 'font.serif': 'Georgia',
                        # 'mathtext.fontset': 'cm',
                        'lines.linewidth': 2.5,
                        'font.size': 22,
                        'xtick.labelsize': 'large',
                        'ytick.labelsize': 'large',
                        'legend.fancybox': True,
                        'legend.fontsize': 18,
                        'legend.framealpha': 0.9,
                        'legend.handletextpad': 0.5,
                        'legend.labelspacing': 0.2,
                        'legend.loc': 'best',
                        'legend.columnspacing': 2,
                        'savefig.dpi': 80,
                        'pdf.compression': 9})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Functions   #####
def load_txt(filename):
    '''Takes in filename, and outputs tuple of the two columns, in this case (1) frequency vector and (2) asd'''
    data = np.loadtxt(filename)
    ff = data[:,0]
    asd = data[:,1]
    return ff, asd

def save_txt(fff, psd, filename, header, verbose=True):
    '''Stores a two-column txt of frequency and power spectral density
    fff = frequency vector in Hertz.  Must be np.array with single row of values.
    psd = power spectral density [units^2/Hz].  Must be np.array of same length as fff. 
    filename = full directory path to the file and filename
    header = text to go at the top of the txt
    '''
    save_data = np.vstack((fff, psd)).T
    if verbose:
        print()
        print(f'Saving data file...')
    np.savetxt(filename, save_data, header=header)
    if verbose:
        print(f'{filename}')
    return

def rms(x, y):
    '''Takes in freq vector x, and asd y,
    returns the cumulative root mean squared array
    '''
    diff = np.diff(np.squeeze(x))
    dx = np.concatenate(([diff[0]], diff), axis=0)

    # Return rms intergrated from high to low
    rms = np.flipud(np.sqrt(np.cumsum(np.flipud(np.squeeze(y)**2 * dx))))
    return rms


#####   Get data   #####
# from .txt, if possible
# from .xml, if necessary

xml_loaded = False
als_filename = f'{data_dir}/als_comm_frequency_noise_asd.txt'
refl_cal_filename = f'{data_dir}/als_comm_refl_calibration_sweep.txt'
try:
    ff_als, asd_als = load_txt(als_filename)
    print(f'Loaded {als_filename}')

    times, timeseries = load_txt(refl_cal_filename)
    print(f'Loaded {refl_cal_filename}')
except:
    xml_loaded = True
    print(f'Did not find {als_filename}, looking for .xml')

    data_dict = {}
    chan = 'H1:LSC-REFL_A_RF9_I_ERR_DQ'

    xml_filename = f'{data_dir}/20180802_ALSCOMM_FrequencyNoiseASD.xml'
    xml_dict = dtt2hdf.read_diaggui(xml_filename)
    print(f'Loaded {xml_filename}')

    # Get ALS COMM ASD
    ff_main = xml_dict['results']['PSD'][chan]['FHz']
    asd_main = xml_dict['results']['PSD'][chan]['FHz']

    for key in list(xml_dict['references'].keys()):
        data_dict[key] = {}
        data_dict[key]['ff'] = xml_dict['references'][key]['FHz']
        data_dict[key]['asd'] = xml_dict['references'][key]['PSD'][0,:]

    key = 0 
    ff_als = xml_dict['references'][key]['FHz']
    asd_als = xml_dict['references'][key]['PSD'][0,:]

    # Get REFL A 9I calibration sweep
    xml_sweep_filename = f'{data_dir}/20180802_IR_REFL9I_PDHSignalFreqSweep.xml'
    xml_sweep_dict = dtt2hdf.read_diaggui(xml_sweep_filename)

    timeseries = xml_sweep_dict['results']['TS'][chan]['timeseries']
    NN = xml_sweep_dict['results']['TS'][chan]['N']
    dt = xml_sweep_dict['results']['TS'][chan]['dt']
    times = np.arange(0, NN * dt, dt) 

    ###   Save data   ### 
    header = f'Frequency [Hz], ALS COMM [cts/rtHz]\nCreated by script in {os.path.abspath(__file__)}'
    save_txt(ff_als, asd_als, als_filename, header=header)

    header = f'Time [s], REFL A 9I output [cts]\nCreated by script in {os.path.abspath(__file__)}'
    save_txt(times, timeseries, refl_cal_filename, header=header)



#####   Set up data   #####
fflog = np.logspace(np.log10(ff_als[0]), np.log10(ff_als[-1]), 5000)

gain = 1/0.4598          # Hz/cts
x_arm_pole = 45.1   # Hz, red arm pole we are calibrating to

asd_als_cal = asd_als * gain * np.abs(1 + 1j * ff_als / x_arm_pole)

rms_als_cal = rms(ff_als, asd_als_cal)


# Sweep calibration
idx = np.argwhere(timeseries < 0)[0,0] # first point where cts < 0
ctspp = np.max(timeseries) - np.min(timeseries)

idx_max = np.argmax(timeseries)
idx_min = np.argmin(timeseries)

x_axis_calibration = 2 * x_arm_pole/(times[idx_min] - times[idx_max]) # Hz/seconds

times_Hz = (times - times[idx]) * x_axis_calibration # Hz, centered around the IR resonant peak

slope = -ctspp / x_arm_pole # cts/Hz, # slope should be cavity_pole / volts_peak_to_peak, https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=7054
linear_fit = slope * times_Hz # cts = cts/Hz * Hz

xx = times_Hz/x_arm_pole
full_pdh_fit = -ctspp * xx / (1 + xx**2)



#####   Save txt   #####
# Save txt of calibrated ALS COMM
save_cal_als_filename = f'{data_dir}/calibrated_als_comm_frequency_noise_asd.txt'
header = f'Frequency [Hz], ALS COMM [Hz/rtHz]\nCreated by script in {os.path.abspath(__file__)}'
save_txt(ff_als, asd_als_cal, save_cal_als_filename, header, verbose=True)



#####   Figures   #####
# Plot main ALS COMM out-of-loop frequency noise
fig, s1 = plt.subplots(1)

plot_ff, plot_asd = nu.linear_log_ASD(fflog, ff_als, asd_als_cal)
plot_ff, plot_rms = nu.linear_log_ASD(fflog, ff_als, rms_als_cal)

s1.loglog(plot_ff, plot_asd, ls='-', label='ALS COMM out-of-loop error signal')
s1.loglog(plot_ff, plot_rms, ls='--', label=f'RMS = {rms_als_cal[0]:.1f} Hz')

# s1.set_ylim([1e-10, 2e-7])
# # s1.set_yticks(goodTicks(s1))
# s1.set_xlim([10, 5000])

s1.set_xlabel('Frequency [Hz]')
s1.set_ylabel(r'ALS COMM ASD [$\mathrm{ Hz / \sqrt{Hz} }$]')

s1.legend(loc='upper right', ncol=1)
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.3)



plot_name = f'als_comm_asd_calibrated.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Plot time series IR sweep of REFL A 9I while ALS COMM is locked
fig, s1 = plt.subplots(1)

s1.plot(times_Hz, timeseries, ls='-', label='REFL A 9I calibration sweep')
s1.plot(times_Hz, linear_fit, ls='--', label=f'Slope fit = {slope:.2f} cts/Hz')
s1.plot(times_Hz, full_pdh_fit, ls='--', label='PDH error signal')

s1.set_ylim([-11, 11])
# # s1.set_yticks(goodTicks(s1))
s1.set_xlim([-300, 300])

s1.set_xlabel('IR frequency sweep [Hz]')
s1.set_ylabel('REFL A 9I [cts]')

s1.legend(loc='upper right', ncol=1)
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.3)



plot_name = f'als_comm_refl_a_9i_calibration_sweep.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



if xml_loaded:
    # All asds in xml
    fig, s1 = plt.subplots(1)

    ls = '-'
    for key, temp_dict in data_dict.items():
        ff = temp_dict['ff']
        asd = temp_dict['asd']

        plot_ff, plot_asd = nu.linear_log_ASD(fflog, ff, asd)
        if key == 2:
            ls = '--'
        s1.loglog(plot_ff, plot_asd, alpha=0.7, ls=ls, label=key)

    plot_ff, plot_asd = nu.linear_log_ASD(fflog, ff_main, asd_main)
    s1.loglog(plot_ff, plot_asd, alpha=0.7, ls=ls, label='main')

    # s1.set_ylim([1e-10, 2e-7])
    # # s1.set_yticks(goodTicks(s1))
    # s1.set_xlim([10, 5000])

    s1.set_xlabel('Frequency [Hz]')
    s1.set_ylabel(r'ALS COMM ASD [$\mathrm{ Hz / \sqrt{Hz} }$]')

    s1.legend(loc='upper right', ncol=1)
    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.3)



    plot_name = f'all_als_comm_asds.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print(f'Writing plot PDF to {full_plot_name}')
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()


# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()