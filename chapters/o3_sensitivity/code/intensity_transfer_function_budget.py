'''
Plots the raw TF and coherence
between the ISS and several IFO ports at the time of an injection.

Craig Cahillane
April 27, 2020
'''
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc
import scipy.optimize as sco

import nds2utils as nu

import pykat
from pykat.ifo import aligo

import gwinc

mpl.rcParams.update({   'figure.figsize': (12, 9),
                        'text.usetex': True,
                        'font.family': 'serif',
                        # 'font.serif': 'Georgia',
                        # 'mathtext.fontset': 'cm',
                        'lines.linewidth': 2.5,
                        'font.size': 22,
                        'xtick.labelsize': 'large',
                        'ytick.labelsize': 'large',
                        'legend.fancybox': True,
                        'legend.fontsize': 18,
                        'legend.framealpha': 0.9,
                        'legend.handletextpad': 0.5,
                        'legend.labelspacing': 0.2,
                        'legend.loc': 'best',
                        'legend.columnspacing': 2,
                        'savefig.dpi': 80,
                        'pdf.compression': 9})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Functions   #####
def read_tf_txt(filename):
    '''Reads transfer function .txt file with four columns:
    frequency, TF magnitude, TF phase in radians, and coherence
    Returns as three arrays:
    return ff, tf, coh'''

    data = np.loadtxt(filename)
    ff = data[:,0]
    tf = data[:,1] * np.exp(1j * data[:,2])
    coh = data[:,3]
    return ff, tf, coh

def load_tf_data(filename):
    ''' Function to read in data 
    Get the measured LHO tf [m/Hz]'''
    data = np.loadtxt(filename)
    ff  = data[:,0]
    tf  = data[:,1] * np.exp(1j*np.pi/180 * data[:,2])
    coh = data[:,3]

    idxs = np.argsort(ff)
    ff = ff[idxs]
    tf = tf[idxs]
    coh = coh[idxs]

    return ff, tf, coh

def get_logbinned_data(ff, tf, coh, fflog):
    '''Take in frequency vector, transfer function, coherence, and log frequency vector we want to interpolate onto
    return the logbinned frequency vector, tf, and coherence:
    return ff_log, tf_log, coh_log
    '''
    ff_log, tf_real = nu.linear_log_ASD(fflog, ff, np.real(tf))
    _, tf_imag = nu.linear_log_ASD(fflog, ff, np.imag(tf))
    _, coh_log = nu.linear_log_ASD(fflog, ff, coh)

    tf_log = tf_real + 1j * tf_imag
    return ff_log, tf_log, coh_log

def compile_data_dict(filename, fflog):
    '''Compiles a dictionary out of the data extracted from the .txt filename.
    Returns the dictionary with keys: ff, tf, coh, ff_log, tf_log, coh_log
    '''
    ff, tf, coh = load_tf_data(filename)
    ff_log, tf_log, coh_log = get_logbinned_data(ff, tf, coh, fflog)

    temp_dict = {}
    temp_dict['ff'] = ff
    temp_dict['tf'] = tf
    temp_dict['coh'] = coh
    temp_dict['ff_log'] = ff_log
    temp_dict['tf_log'] = tf_log
    temp_dict['coh_log'] = coh_log

    return temp_dict

#####   IFO Functions   #####
def arm_gain(phi, T1, T2, Loss_rt):
    '''
    Returns the amplitude gain of an arm cavity.
    phi     = relative phase of the light in single pass of the cavity
    T1      = power transmittance of input mirror
    T2      = power transmittance of output mirror
    Loss_rt = roundtrip power loss
    '''
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    invloss = np.sqrt(1 - Loss_rt)
    return t1/(1 - r1 * r2 * invloss * np.exp(1j*phi))

def arm_refl(phi, T1, T2, Loss_rt):
    '''
    Returns the reflectivity of an arm cavity.
    phi     = relative phase of the light to the cavity
    T1      = power transmittance of input mirror
    T2      = power transmittance of output mirror
    Loss_rt = roundtrip power loss
    '''
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    invloss = np.sqrt(1 - Loss_rt)
    return -r1 + t1 * r2 * invloss * arm_gain(phi, T1, T2, Loss_rt)

def arm_refl_derivative(phi, T1, T2, Loss_rt):
    '''
    Returns the derivative of the reflectivity of an arm cavity.
    phi     = relative phase of the light to the cavity
    T1      = power transmittance of input mirror
    T2      = power transmittance of output mirror
    Loss_rt = roundtrip power loss
    '''
    r2 = np.sqrt(1 - T2)
    invloss = np.sqrt(1 - Loss_rt)
    return r2 * invloss * np.exp(2j*phi) * np.abs(arm_gain(phi, T1, T2, Loss_rt))**2

def arm_pole(L, T1, T2, Loss_rt):
    '''
    Returns the arm cavity pole in Hz.
    phi     = relative phase of the light to the cavity
    L       = length of the cavity
    T1      = power transmittance of input mirror
    T2      = power transmittance of output mirror
    Loss_rt = roundtrip power loss
    '''
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    invloss = np.sqrt(1 - Loss_rt)    
    return scc.c/(2*L) * 1/(2*np.pi) * np.log(1/(r1*r2*invloss))

def coupled_cavity_pole(L, T1, T2, T3, Loss_rt):
    '''
    Returns the coupled cavity pole in Hz.
    L       = length of the longer cavity, shorter cavity length assumed to be zero.
    T1      = power transmittance of input mirror
    T2      = power transmittance of middle mirror
    T3      = power transmittance of end mirror
    Loss_rt = roundtrip power loss
    '''
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    r3 = np.sqrt(1 - T3)
    invloss = np.sqrt(1 - Loss_rt)  
    return scc.c/(2*L) * 1/(2*np.pi) * np.log((1 + r1 * r2)/(r2 * r3 + r1 * r3 * invloss**2))

def rse_pole(L, T1, T2, T3, Loss_rt):
    '''
    NOTE: This function is not valid for CE, the FSR is too close to the darm pole, making this approx invalid
    Use rse_pole_exact() instead

    Returns the darm resonant signal extraction pole in Hz.  
    Same as coupled_cavity_pole() but with sign changes.
    L       = length of the longer cavity, shorter cavity length assumed to be zero.
    T1      = power transmittance of input mirror
    T2      = power transmittance of middle mirror
    T3      = power transmittance of end mirror
    Loss_rt = roundtrip power loss
    '''
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    r3 = np.sqrt(1 - T3)
    invloss = np.sqrt(1 - Loss_rt)  
    return scc.c/(2*L) * 1/(2*np.pi) * np.log((1 - r1 * r2)/(r2 * r3 - r1 * r3 * invloss**2))

def rse_pole_exact(L1, L2, T1, T2, T3, Loss_rt):
    '''
    Returns the darm resonant signal extraction complex pole in Hz.  
    Uses scipy.root to find an exact solution.
    Same as coupled_cavity_pole() but with sign changes.
    L       = length of the longer cavity, shorter cavity length assumed to be zero.
    T1      = power transmittance of input mirror
    T2      = power transmittance of middle mirror
    T3      = power transmittance of end mirror
    Loss_rt = roundtrip power loss
    '''    
    t1 = np.sqrt(T1)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    r3 = np.sqrt(1 - T3)
    invloss = np.sqrt(1 - Loss_rt)
    def root_func(fc, L1, L2, r1, r2, r3, invloss):
        f = complex(*fc)
        err = np.exp((4j * f * (L2 + L1) * np.pi)/scc.c) \
        - np.exp((4j * f * L2 * np.pi)/scc.c) * r1 * r2 \
        + invloss**2 * r1 * r3 \
        - np.exp((4j * f * L1 * np.pi)/scc.c) * r2 * r3
        return [err.real, err.imag]

    solution_dict = sco.root(root_func, [1000, 1000], args=(L1, L2, r1, r2, r3, invloss))
    f_rse = solution_dict['x'][0] + 1j*solution_dict['x'][1]
    return f_rse

def darm_full(fff, L1, L2, T1, T2, T3): 
    ''' Get normalized darm response (W/W)/m'''
    t2 = np.sqrt(T2)
    r1 = np.sqrt(1 - T1)
    r2 = np.sqrt(1 - T2)
    r3 = np.sqrt(1 - T3)
    exp1 = np.exp(1j*2*np.pi*L1*fff/scc.c) 
    exp2 = np.exp(1j*2*np.pi*L2*fff/scc.c) 
     
    return (1 - r1*r2 -r2*r3 + r1*r3)/(exp1**2 * exp2**2 - exp2**2*r1*r2 - exp1**2*r2*r3 + r1*r3*(r2**2 + t2**2)) 

def darm_simple(fff, L, T1, T2, T3):
    ''' Normalized Simple darm pole (W/W)/m'''
    f_rse = rse_pole(L, T1, T2, T3, 0)
    return 1/(1 + 1j*fff/f_rse)

def darm_HF_correction(fff, L1, L2, T1, T2, T3):
    ''' 
    Correction factor for darm response at HF, 
    c.f. the simple pole approximation vs the true darm response.
    Returns the normalized correction factor  = Simple Pole (W/m)/Full Response (W/m) [meters_full_resp / meters_simple_pole]
    '''
    L = L2 # arm length is bigger
    simple_pole = darm_simple(fff, L, T1, T2, T3)
    full_resp = darm_full(fff, L1, L2, T1, T2, T3)
    return simple_pole/full_resp


###  Kiwamu's DCC T1500559 Eq 30  ###
def analytic_freq_to_darm_tf(fff, ifo):
    ''' 
    Takes in an ifo dictionary in the style of pygwinc.
    Outputs three analytic tranfer functions from intensity noise to darm in [m/RIN] units corresponding to 
    1) reflectivity difference,
    2) arm pole (storage time) difference,
    3) Schnupp asymmetry
    4) Radiation pressure

    Recall that Kiwamu uses reletive amplitude noise dA/A = dP/(2P)
    '''
    # Set up params
    wavelength = ifo['Laser']['Wavelength']
    k = 2 * np.pi / wavelength

    L = ifo['Infrastructure']['Length']
    Ls = ifo['Infrastructure']['SRC_length']
    L_darmoffset = ifo['Infrastructure']['darmoffset']
    epsilon = k * L_darmoffset # phase of the darm offset
    phi = 2 * np.pi * L_darmoffset / wavelength

    M = ifo['Suspension']['Stage'][0]['Mass'] # mass of the final test mass
    mu = M / 2.0 # reduced mass

    Tix = 1.5e-2
    Tiy = 1.42e-2
    Ti = (Tix + Tiy)/2

    Ts = ifo['Optics']['SRM']['Transmittance']
    Tp = ifo['Optics']['PRM']['Transmittance']
    # Ti = ifo['Optics']['ITM']['Transmittance']
    Te = ifo['Optics']['ETM']['Transmittance']
    Loss_rt = 100e-6 #ifo['Optics']['Loss']

    tp = np.sqrt(Tp)
    rp = np.sqrt(1 - Tp)
    ts = np.sqrt(Ts)
    rs = np.sqrt(1 - Ts)
    ti = np.sqrt(Ti)
    ri = np.sqrt(1 - Ti)
    te = np.sqrt(Te)
    re = np.sqrt(1 - Te)

    print()
    print(f'ETM Trans = {Te}')
    print(f'ITM Trans = {Ti}')
    print(f'PRM Trans = {Tp}')
    print(f'SRM Trans = {Ts}')
    print(f'Loss rt = {Loss_rt}')
    print(f'M = {M} kg')
    print(f'mu = {mu} kg')

    # Calculate differential arm gain
    rix = np.sqrt(1 - Tix)
    riy = np.sqrt(1 - Tiy)
    gx = arm_gain(0, Tix, Te, 0)
    gy = arm_gain(0, Tiy, Te, 0)
    ga = (gx + gy)/2
    # delta_g = (gx - gy)/2

    arm_power_x = 194e3 # W, +- 2e3
    arm_power_y = 207e3 # W, +- 2e3
    arm_power = (arm_power_x + arm_power_y) / 2
    delta_arm_power = (arm_power_x - arm_power_y) / 2

    delta_g = delta_arm_power * ga / (2 * arm_power) # delta_g / ga = delta_P / (2 P)

    print(f'gx = {gx}')
    print(f'gy = {gy}')
    print(f'ga = {ga}')
    print(f'delta_g = {delta_g}')

    # other differential terms
    l_schnupp = ifo['Infrastructure']['SchnuppAsymmetry']
    
    delta_ra = ifo['Infrastructure']['delta_ra']
    delta_f_c = ifo['Infrastructure']['delta_f_c']
    delta_omega_c = 2 * np.pi * delta_f_c
    delta_mu = mu * 0.001

    # Use helper functions
    ra = arm_refl(phi, Ti, Te, Loss_rt)
    dradphi = arm_refl_derivative(phi, Ti, Te, Loss_rt)
    f_c = arm_pole(L, Ti, Te, Loss_rt)
    f_cc = 0.6 #coupled_cavity_pole(L, Tp, Ti, Te, Loss_rt)
    f_rse_complex = rse_pole_exact(Ls, L, Ts, Ti, Te, Loss_rt)
    f_rse = 412 #np.imag(f_rse_complex)
    omega_c = 2 * np.pi * f_c
    omega_cc = 2 * np.pi * f_cc
    omega_rse = 2 * np.pi * f_rse

    print()
    print(f'arm refl ra = {ra}')
    print(f'arm refl dervi dradphi = {dradphi}')
    print(f'arm pole f_c = {f_c} Hz')
    print(f'coupled cavity pole f_cc = {f_cc} Hz')
    print(f'darm pole f_rse = {f_rse} Hz')

    s_c = 1j * fff / f_c
    s_cc = 1j * fff / f_cc
    s_rse = 1j * fff / f_rse
    s_mu2 = -mu * (2 * np.pi * fff)**2 # s_mu^2 (Eq 8)
    
    # Direct intensity via darm offset
    tf0 = (1 + ra)**2 * epsilon / (4 * k * (1 + s_cc))

    # Intensity via contrast defect
    prefactor = delta_ra / (4 * epsilon * k * dradphi**2 * (1 + s_cc))

    tf1 = prefactor * delta_ra * (1 + s_c)
    tf2 = prefactor * (delta_omega_c / omega_c) * s_c * (1 + ra)
    tf3 = prefactor * (l_schnupp * omega_c / scc.c) * ra * s_c * (1 + s_c) * (1 - s_c/ra)

    # Radiation pressure induced from darm offset, arm mismatches
    Pa = arm_power #ifo['Infrastructure']['ArmPower']
    Ta = 1 - ra**2
    gs = arm_gain(np.pi, Ts, Ta, 0) # SRC is anti-resonant for carrier
    gp = arm_gain(0, Tp, Ta, 0)
    Gs = np.abs(gs)**2
    Gp = np.abs(gp)**2
    print(f'Arm power Pa = {Pa}')
    print(f'SRC gain Gs = {Gs}')
    print(f'PRC gain Gp = {Gp}')
    
    rad_factor = 2 * Pa / (scc.c * s_mu2 * (1 + s_cc)) # no factor of two b/c of RIN = RAN/2 and DARM -> DARM/2

    tf4 = rad_factor * 2 * (delta_g / ga)
    tf5 = rad_factor * -1 * delta_ra * gs * rs * (2 - s_rse) / (ts * (1 + s_rse))
    tf6 = rad_factor * (delta_omega_c / omega_c) * gs * s_c * (1 - rs)/(ts * (1 - s_rse))
    tf7 = rad_factor * -2 * (delta_mu / mu)
    
    # OMC transmission of sidebands with intensity noise
    G_darm = (1 + s_rse) / (2 * Gp * Gs * dradphi**2 * epsilon * k)    # m/W, from darm watts to meters, sort of (needs input power)
    T_omc = 100e-6 #61.4e-6 
    mod_depth_m2 = 0.2 # rads
    omega_m2 = 2 * np.pi * 45.5e6 # Hz 

    ra_hat = -(re + ri) / (1 + re * ri)
    r_mich = ra_hat * np.cos(2 * omega_m2 * l_schnupp / scc.c)
    t_mich = ra_hat * np.sin(2 * omega_m2 * l_schnupp / scc.c)

    rsm = (r_mich + ra_hat**2 * rs) / (1 + r_mich * rs)
    tsm = ts * t_mich / (1 + rs * r_mich)

    g_sb = tp / (1 + rp * rsm)

    sidebands_m2_as_port = (0.5 * mod_depth_m2 * g_sb * tsm)**2
    sidebands_m2_dcpds = T_omc * sidebands_m2_as_port

    tf8 = G_darm * sidebands_m2_dcpds

    print()
    print(f'1/G_darm = {1e-9/np.abs(G_darm[0])} mW/pm')
    print(f'ra_hat = {ra_hat}')
    print(f'r_mich = {r_mich}')
    print(f't_mich = {t_mich}')
    print(f'rsm = {rsm}')
    print(f'tsm = {tsm}')
    print(f'g_sb = {g_sb}')
    print(f'sidebands_m2_as_port = {sidebands_m2_as_port}')
    print(f'sidebands_m2_dcpds = {sidebands_m2_dcpds}')

    tf9  = np.exp(1j*np.pi/4) * 1.3e-5 * G_darm # m/RIN_in = W/RIN_in * m/W
    tf10 = np.exp(1j*np.pi/4) * 2e-5 * G_darm # m/RIN_in = W/RIN_in * m/W

    # compile results into dict
    tf_list = np.array([tf0, tf1, tf2, tf3, tf4, tf5, tf6, tf7, tf8, tf9, tf10])
    tf_labels = np.array([
        'darm offset ' + r'$\Delta L_{dc}$',
        'arm reflectivity ' + r'$\delta r_a$',
        'arm pole ' + r'$\delta \omega_c$',
        'Schnupp asymmetry ' + r'$\ell_{Schnupp}$',
        'rad pressure from arm gain ' + r'$\delta g$',
        'rad pressure from arm reflectivity ' + r'$\delta r_a$',
        'rad pressure from arm pole ' + r'$\delta \omega_c$',
        'rad pressure from reduced mass ' + r'$\delta \mu$',
        '45 MHz trans through OMC ' + r'$T_{OMC}$',
        'corner higher order modes low '  + r'$q_{HOM}$',
        'corner higher order modes high ' + r'$q_{HOM}$',
    ])

    tf_total = np.zeros_like(fff, dtype='complex128')
    tf_dict = {}
    for tf, label in zip(tf_list, tf_labels):
        tf_dict[label] = tf
        if not 'higher order modes' in label:
            tf_total += tf
    tf_dict['total'] = tf_total

    return tf_dict



#####   IFO params   #####
fflog = np.logspace(np.log10(1), np.log10(40000), 500)

ifos = {}

aligo_budget = gwinc.load_budget('aLIGO')
ce1_budget = gwinc.load_budget('CE1')
ce2_budget = gwinc.load_budget('CE2')

ifos['aLIGO'] = aligo_budget.ifo
ifos['CE1'] = ce1_budget.ifo
ifos['CE2'] = ce2_budget.ifo

ifos['aLIGO']['Infrastructure']['SRC_length'] = 55 # m
ifos['CE1']['Infrastructure']['SRC_length'] = 105.4 # m
ifos['CE2']['Infrastructure']['SRC_length'] = 105.4 # m

ifos['aLIGO']['Infrastructure']['SchnuppAsymmetry'] = 8e-2 # m
ifos['CE1']['Infrastructure']['SchnuppAsymmetry'] = 13.9e-3 # m, from Kevin Kuns
ifos['CE2']['Infrastructure']['SchnuppAsymmetry'] = 13.9e-3 # m

ifos['aLIGO']['Infrastructure']['darmoffset'] = 10e-12  # m
ifos['CE1']['Infrastructure']['darmoffset'] = 0
ifos['CE2']['Infrastructure']['darmoffset'] = 0

ifos['aLIGO']['Infrastructure']['ArmPower'] = 200e3 # W
ifos['CE1']['Infrastructure']['ArmPower'] = 1.38e6 # W
ifos['CE2']['Infrastructure']['ArmPower'] = 1.38e6 # W

ifos['aLIGO']['Infrastructure']['delta_ra'] = 5e-3 / 2 # m
ifos['CE1']['Infrastructure']['delta_ra'] = 5e-3 / 2
ifos['CE2']['Infrastructure']['delta_ra'] = 5e-3 / 2

ifos['aLIGO']['Infrastructure']['delta_f_c'] = 3.0 / 2 # Hz
ifos['CE1']['Infrastructure']['delta_f_c'] = 0.04 / 2 # Hz
ifos['CE2']['Infrastructure']['delta_f_c'] = 0.04 / 2 # Hz



#####   Get analytic tfs   #####
print('\033[92m')
print('aLIGO')
tf_dict = analytic_freq_to_darm_tf(fflog, ifos['aLIGO'])
print('\033[0m')

# print('\033[93m')
# print('CE1')
# tf0_CE1, tf1_CE1, tf2_CE1, tf3_CE1, tf4_CE1, tf5_CE1, tf6_CE1, tf7_CE1, tf8_CE1 = analytic_freq_to_darm_tf(fflog, ifos['CE1'])
# print('\033[0m')

# tf_total_CE1 = tf0_CE1 + tf1_CE1 + tf2_CE1 + tf3_CE1 + tf4_CE1 + tf5_CE1 + tf6_CE1 + tf7_CE1 + tf8_CE1



### Get the data at the time of the intensity injections
channels = ['H1:CAL-DELTAL_EXTERNAL_DQ',
            'H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ', # secondloop witness
            # 'H1:PSL-ISS_PDA_REL_OUT_DQ', 'H1:PSL-ISS_PDB_REL_OUT_DQ', # firstloop witnesses
            # 'H1:IMC-IM4_TRANS_NSUM_OUT_DQ', # IM4 trans
            # 'H1:LSC-REFL_A_RIN_OUT_DQ', 'H1:LSC-REFL_B_RIN_OUT_DQ', # refl
            # 'H1:LSC-POP_A_RIN_OUT_DQ',  # pop
            'H1:OMC-DCPD_SUM_OUT_DQ',  # as port
            # 'H1:ASC-X_TR_A_NSUM_OUT_DQ', 'H1:ASC-X_TR_B_NSUM_OUT_DQ', # xarm
            # 'H1:ASC-Y_TR_A_NSUM_OUT_DQ', 'H1:ASC-Y_TR_B_NSUM_OUT_DQ', # yarm
            ]
non_rin_chans = np.array([
            # 'H1:IMC-IM4_TRANS_NSUM_OUT_DQ', # IM4 trans
            'H1:OMC-DCPD_SUM_OUT_DQ',  # as port
            # 'H1:ASC-X_TR_A_NSUM_OUT_DQ', 'H1:ASC-X_TR_B_NSUM_OUT_DQ', # xarm
            # 'H1:ASC-Y_TR_A_NSUM_OUT_DQ', 'H1:ASC-Y_TR_B_NSUM_OUT_DQ', # yarm
            ])
rin_chans = np.setdiff1d(channels, non_rin_chans)

darm = channels[0]
iss = channels[1]
dcpds = channels[2]

duration = 92 # s
binwidth = 1 # Hz, frequency vector spacing
overlap = 0.5 # FFT overlap ratio

gps_starts = np.array([
    1240122556, # 2019,  4, 24,  6, 28, 58, tzinfo=tzutc()
    1256622580, # 2019, 11,  1,  5, 49, 22, tzinfo=tzutc()
])
gps_start_labels = {
    1240122556 : 'LHO April 24, 2019',
    1256622580 : 'LHO November 01, 2019',
}
styles = {
    1240122556 : 'C2-',
    1256622580 : 'C3-',
}

data_dict = {}
for gps_start in gps_starts:
    gps_start = int(gps_start)
    
    filename = f'{data_dir}/intensity_noise_to_DARM_TF_gps_start_{gps_start}.txt'
    temp_dict = compile_data_dict(filename, fflog)
    data_dict[gps_start] = temp_dict


#####   Finesse intensity injection   #####
plot_finesse = False
if plot_finesse:

    # Some DCPD basics
    QE   = 0.98
    resp = 0.856 # A/W

    base = aligo.make_kat()
    #print(base)
    base.IFO.remove_IMC_HAM2(True, False)

    base.maxtem = 2
    base.L0.P = 32.8

    base.SRM.T   = 0.32
    base.SR2.L    = 0.05 # extremely lossy SRC
    base.ETMX.L   = 35e-6
    base.ETMY.L   = 35e-6

    #https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=47113
    base.mod1.midx = 0.16
    base.mod2.midx = 0.18

    base.ITMY.setRTL(None, 1.42/100., 40e-6)
    base.ITMX.setRTL(None, 1.5/100., 40e-6)
    base.PRM.L.value /= 4

    base.ITMY_lens.p = 1/base.ITMY_lens.f.value + 0e-6
    base.ITMX_lens.p = 1/base.ITMX_lens.f.value + 10e-6

    base.IFO.CARM.port.phase = -87 #92.5
    base.IFO.PRCL.port.phase = -60 #119.8
    base.IFO.PRCL.quad = 'I'

    base.IFO.SRCL.port = base.IFO.POP_f2
    base.IFO.SRCL.quad = 'I'

    base.IFO.MICH.port.phase = 103.4
    base.IFO.MICH.quad = 'Q'

    mmx, mmy, _ = aligo.mismatch_cavities(base, 'ni')

    base = aligo.setup(base, verbose=True)

    base.DARM_lock.offset = -20e-3/(QE*resp) # Get 20mA of DC signal
    base.SRCL_lock.gain /= 2
    base.PRCL_lock.gain /= 2
    base.DARM_lock.gain /= 2
    base.CARM_lock.gain /= 2
    base.MICH_lock.gain /= 2

    base.SRCL_lock.accuracy /= 5
    base.PRCL_lock.accuracy /= 5
    base.DARM_lock.accuracy /= 5
    base.CARM_lock.accuracy /= 5
    base.MICH_lock.accuracy /= 5

    base.IFO.zero_locks()

    base.noxaxis = True

    # Copy the base aligo file, and add PDs and an fsig on the laser amplitude
    kat = base.deepcopy()
    kat.parse("""
    fsig sig L0 amp 1 0

    xaxis sig f log 1 8000 1000

    xd xd_ITMX ITMX z
    xd xd_ETMX ETMX z
    xd xd_ITMY ITMY z
    xd xd_ETMY ETMY z


    pd1 AC_AS $fs nAS
    pd1 AC_REFL $fs nREFL
    pd1 AC_POP $fs nPRM2
    pd1 AC_X $fs nETMX1
    pd1 AC_Y $fs nETMY1

    pd P_AS nAS
    pd P_REFL nREFL
    pd P_POP nPRM2
    pd P_X nETMX1
    pd P_Y nETMY1

    yaxis re:im

    """)
    kat.IFO.suspend_mirrors_pitch()
    kat.IFO.suspend_mirrors_z()
    kat.IFO.zero_locks()
    kat.removeBlock('locks')
    out = kat.run(cmd_args=['-cr=on'])

    ff_finesse = out.x

    DARM_opt_gain = 2.6393e+10 #1.5656e+10 #0.5 * 1/kat.DARM_lock.gain * 180/np.pi * 2*np.pi/1064e-9 # W/m
    DARM_pole = 429 # Hz
    DARM_gain = lambda fff : DARM_opt_gain / (1 + 1j * fff/DARM_pole) # W/m
    RIN_to_DARM_finesse = out['AC_AS'] / DARM_gain(ff_finesse) # m/RIN 

    RIN_to_DCPDs_finesse = out['AC_AS'] / np.median(out['P_AS']) # RIN/RIN
    print(f'finesse P_AS = {np.median(out["P_AS"])} W')

    P_X = np.abs(out['P_X'][0])
    P_Y = np.abs(out['P_Y'][0])
    deltaP = P_X - P_Y

    print()
    print('Finesse Arm Powers')
    print('X = {} watts'.format(P_X))
    print('Y = {} watts'.format(P_Y))
    print('deltaP = {} watts'.format(deltaP))

#####   Analytic estimate of radiation pressure effect   #####
# fflog = np.logspace(0, np.log10(5000), 500)
M = 40 # kg
c = scc.c
arm_power_x = 194e3 # W, +- 2e3
arm_power_y = 207e3 # W, +- 2e3
# deltaP = arm_power_x - arm_power_y
deltaP = -8584 # W, difference in arm power by eye fit, Px-Py
carm_pole = 0.6 # Hz
darm_pole = 410 # Hz
input_RIN_2_arm_RIN = nu.tf_zpk(fflog, [], [carm_pole, darm_pole], gain=1)

input_RIN_2_DARM_TF = 4 * deltaP / c \
                    * -1/(M * (2*np.pi*fflog)**2) \
                    * input_RIN_2_arm_RIN  # m/RIN, 4 because 2*P/c for reflected light off one mirror, times 2 for ITM + ETM
                    # need to work out this factor of 4 vs 2 justification.  2 seems to work better, but I can't figure out why
analytic_label  = 'Analytic Radiation Pressure'#\
                #+ r'$\Delta L/\mathrm{RIN} = 4 \Delta P / (c m \omega^2) * 1 / (1 + i \omega/\omega_\mathrm{CARM}) * 1 / (1 + i \omega/\omega_\mathrm{DARM})$'



#### Plots ####
# intensityTFs mag only
fig, (s1) = plt.subplots(1)
for gps_start in gps_starts:
    label = gps_start_labels[gps_start]
    style = styles[gps_start]

    ff  = data_dict[gps_start]['ff']
    tf  = data_dict[gps_start]['tf']
    coh = data_dict[gps_start]['coh']

    s1.loglog(ff, np.abs(tf), label=label, zorder=3)

ii = 0
for label, tf in tf_dict.items():
    ii += 1
    if ii > 8:
        ls = '--'
    else:
        ls = '-'

    if 'higher order modes' in label:
        s1.loglog(fflog, np.abs(tf), color='C3', ls='--')
    elif 'total' in label:
        continue
    else:
        s1.loglog(fflog, np.abs(tf), ls=ls, label=label)

tf_low = tf_dict['corner higher order modes low '  + r'$q_{HOM}$']
tf_high = tf_dict['corner higher order modes high '  + r'$q_{HOM}$']
tf_total = tf_dict['total']

s1.fill_between(fflog, np.abs(tf_low), np.abs(tf_high), color='C3', alpha=0.3, label='corner higher order modes ' + r'$q_{HOM}$')

s1.loglog(fflog, np.abs(tf_total + tf_low), color='k', ls='--', label='total')
s1.loglog(fflog, np.abs(tf_total + tf_high), color='k', ls='--')

if plot_finesse:
    s1.loglog(ff_finesse, np.abs(RIN_to_DARM_finesse), color='gray', ls='--', label='Finesse')

# s1.set_ylim([5e-14, 1.01e-10])
s1.set_ylim([1e-16, 1.01e-10])
s1.set_xlim([10, 7e3])

s1.set_yticks(nu.good_ticks(s1))

s1.set_ylabel(r'$|L_-/(\delta P / P)|$ [$\mathrm{m}/\mathrm{RIN}_\mathrm{in}$]')
s1.set_xlabel('Frequency [Hz]')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)

s1.legend(fontsize=14, ncol=2, loc='upper right')

plot_name = f'intensity_transfer_function_budget.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# intensityTFs
fig, (s1, s2, s3) = plt.subplots(3, sharex=True)
for gps_start in gps_starts:
    label = gps_start_labels[gps_start]
    style = styles[gps_start]

    ff  = data_dict[gps_start]['ff']
    TF  = data_dict[gps_start]['tf']
    coh = data_dict[gps_start]['coh']

    s1.loglog(ff, np.abs(TF), label=label)
    s2.semilogx(ff, np.angle(TF, deg=True), label=label)
    s3.loglog(ff, coh, style, label=label)

if plot_finesse:
    s1.loglog(ff_finesse, np.abs(RIN_to_DARM_finesse), ls='--', label='Finesse')
    s2.semilogx(ff_finesse, np.angle(RIN_to_DARM_finesse, deg=True), ls='--', label='Finesse')

# s1.loglog(fflog, np.abs(input_RIN_2_DARM_TF), label=analytic_label)
# s2.semilogx(fflog, np.angle(input_RIN_2_DARM_TF, deg=True), label=analytic_label)

# s1.set_ylim([5e-14, 1.01e-10])
s1.set_ylim([1e-15, 1.01e-10])
s3.set_ylim([1e-2, 1.01e0])

s1.set_xlim([10, 7e3])
s2.set_xlim([10, 7e3])
s3.set_xlim([10, 7e3])

s1.set_yticks(nu.good_ticks(s1))
s2.set_yticks(90 * np.arange(-2, 3))
s3.set_yticks(nu.good_ticks(s3))

s1.set_ylabel('Mag ' + r'[$\mathrm{m/RIN_{in}}$]')
s2.set_ylabel('Phase [deg]')
s3.set_ylabel('Coherence')
s3.set_xlabel('Frequency [Hz]')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.5)
s3.grid()
s3.grid(which='minor', ls='--', alpha=0.5)

s1.legend(fontsize=12)

plot_name = f'intensity_transfer_function_budget_with_phase_and_coherence.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()