'''
Tests what the distribution of the product of Gaussian random variables is.

For X ~ N(0, sigma_x), Y ~ N(0, sigma_y), and Z = X * Y,
Z should follow a the zeroth-order modified Bessel function of the second kind, 
Z ~ K_0(Abs(z)/(sigma_x * sigma_y))/(pi * sigma_x * sigma_y)

If Z = X^2, then Z should follow a chi-squared distribution with degree = 1,
Z ~ chi^2 ~ 1/sqrt(2 pi sigma_x^2 z) exp(-z/(2 * sigma_x^2))

Craig Cahillane
May 30, 2020
'''

import os
import sys
import time
import copy
import numpy as np
# import scipy.signal as sig
# import scipy.fft as fft

from scipy.stats import expon, rayleigh, norm, chi2, f
from scipy.special import kn
from scipy.optimize import curve_fit

import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 18, # 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})



#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])



#####   Functions   #####
def mid_bins(bins):
    '''Returns the middle of bins given from the native mpl.hist() output'''
    diff_bins = np.diff(bins)
    middle_bins = bins[:-1] + diff_bins
    return middle_bins

def mean_squared_error(xx, yy, stats_rv, params):
    '''Calculate the mean squared error from measurement (xx, yy)
    and model (scipy.stats pdf function + fit params)
    Inputs:
    xx = x-axis of the measured histogram (use mid_bins())
    yy = y-axis of the measured histogram
    stats_rv = scipy.stats random variable class (like scipy.stats.norm)
    params = parameters of the fit to the stats_rv
    Output:
    mse = mean squared error of our fit
    '''
    error = yy - stats_rv.pdf(xx, loc=params[0], scale=params[1])
    mse = np.mean(error**2)
    return mse

def asymmetric_laplace(x, loc, scale, kappa):
    '''Defines an asymmetric laplace probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplacian
    scale   = scale parameter for broadening the PDF width
    kappa   = asymmetric parameter 
    Output:
    pdf     = asymmetric laplace probability distribution function on input x
    '''
    sign = np.sign(x - loc) # either +1 or -1 
    pdf = (scale / (kappa + 1/kappa)) * np.exp(-sign * (x - loc) * scale * kappa**sign)
    return pdf

def bessel_fit(xx, b, c):
    '''Create function to fit the exponentially scaled modified Bessel function of the second kind
    '''
    bessel_factor = kn( 0, b * np.sqrt(xx**2) )
    normalization = 1/c
    return bessel_factor * normalization

def bessel_exp_fit(xx, a, b, c):
    '''Create function to fit the exponentially scaled modified Bessel function of the second kind
    '''
    exp_factor = np.exp( xx / a )
    bessel_factor = kn( 0, b * np.sqrt(xx**2) )
    normalization = 1/c
    return exp_factor * bessel_factor * normalization

def bessel_param_a(sa, sb, sc):
    '''For three Gaussian white noises a,b,c, with stds sa,sb,sc
    The real part of the csd <x,y> = <a,b> + <a,c> + <c,b>, <c,c>.
    This parameter is in the exponential Exp[x/bessel_param_a]
    For use with bessel_exp_fit()
    '''
    return (sb**2 * sc**2 + sa**2 * (sb**2 + sc**2))/(sc**2)

def bessel_param_b(sa, sb, sc):
    '''For three Gaussian white noises a,b,c, with stds sa,sb,sc
    The real part of the csd <x,y> = <a,b> + <a,c> + <c,b>, <c,c>.
    This parameter is in the BesselK[0, |x| * bessel_param_b]
    For use with bessel_exp_fit()
    '''
    return np.sqrt( ((sa**2 + sc**2) * (sb**2 + sc**2))/(sb**2 * sc**2 + sa**2 * (sb**2 + sc**2))**2 )

def bessel_param_c(sa, sb, sc):
    '''For three Gaussian white noises a,b,c, with stds sa,sb,sc
    The real part of the csd <x,y> = <a,b> + <a,c> + <c,b>, <c,c>.
    This parameter is in the denominator 1/bessel_param_c
    For use with bessel_exp_fit()
    '''
    return np.pi * np.sqrt(sb**2 * sc**2 + sa**2 * (sb**2 + sc**2) )

def median_coherence(power_ratio):
    '''Given a power_ratio of uncorrelated noise over correlated noise, 
    ratio = sigma_uncorr^2 / sigma_corr^2,
    returns the power coherence gamma^2 that would result from median-averaged PSDs and CSDs.
    '''
    med_coh =   (power_ratio**2 * np.log(1 + 1/np.sqrt(1 + power_ratio))**2) \
                / (4 * np.log(2)**2 * (1 + power_ratio) * (2 + power_ratio - 2 * np.sqrt(1 + power_ratio)) )
    return med_coh

def mean_coherence(power_ratio):
    '''Given a power_ratio of uncorrelated noise over correlated noise, 
    ratio = sigma_uncorr^2 / sigma_corr^2,
    returns the usual power coherence one might expect from a mean-averaged PSDs and CSDs.
    '''
    mean_coh = 1/(1 + power_ratio)
    return mean_coh

#####   Samples   #####

sample_number = 1000000

# mean_x = 0
# mean_y = 0
sigma_x = 6.0
sigma_y = 4.0
sigma_c = 1.0

power_x = sigma_x**2
power_y = sigma_y**2
power_c = sigma_c**2

# X[k] = xx + 1j*aa
# Y[k] = yy + 1j*bb
# Z[k] = X[k] + Y[k]
# |X|^2 = xx^2 + aa^2
# X^* Y = (xx * yy + aa * bb) + 1j*(xx * bb - aa * yy)
# X^* Z = (xx^2 + aa^2 + xx * yy + aa * bb) + 1j*(xx * bb - aa * yy)

xx = np.random.normal(scale=sigma_x, size=sample_number)
aa = np.random.normal(scale=sigma_x, size=sample_number)

yy = np.random.normal(scale=sigma_y, size=sample_number)
bb = np.random.normal(scale=sigma_y, size=sample_number)

cc = np.random.normal(scale=sigma_c, size=sample_number)


pp = xx**2 # Generalized Chi Square
rr = yy**2 # Generalized Chi Square

qq = xx * yy # Modified Bessel Function of 2nd kind

uu = xx**2 + aa**2 # Exponential ~ Exp(2 sigma_x^2)
zz = yy**2 + bb**2 # Exponential ~ Exp(2 sigma_y^2)

dd = uu - zz    # Asymmetric Laplace
ee = uu + zz    # Difference of exponents ~ (exp(-ee/gamma) - exp(-ee/lambda))/(gamma - lambda)

vv = xx * yy + aa * bb # Laplace
ww = xx * bb - aa * yy # Laplace

ss = uu + vv # Asymmetric Laplace
tt = ww


#####   Figures   #####

# Histogram of time-domain samples
fig, (s1) = plt.subplots(1)

# hist, bin_edges = np.histogram(Sxxs_mid, bins=20)
num_bins = 200
low_bin  = -4 * sigma_y
high_bin =  4 * sigma_y
bins = np.linspace(low_bin, high_bin, num_bins+1)
xaxis = np.linspace(low_bin, high_bin, 3001)

norm_x_fit_params = norm.fit(xx)
print(f'norm_x_fit_params = {norm_x_fit_params}')
norm_x_pdf_fit = norm.pdf(xaxis, loc=norm_x_fit_params[0], scale=norm_x_fit_params[1])

norm_y_fit_params = norm.fit(yy)
print(f'norm_y_fit_params = {norm_y_fit_params}')
norm_y_pdf_fit = norm.pdf(xaxis, loc=norm_y_fit_params[0], scale=norm_y_fit_params[1])


NNx, bins, patches = s1.hist(xx, bins=bins, histtype='step', lw=2, density=True, label=r'$\mathcal{A}$ samples')
NNy, bins, patches = s1.hist(yy, bins=bins, histtype='step', lw=2, density=True, label=r'$\mathcal{C}$ samples')

# Calculate mean squared error
middle_bins = mid_bins(bins)
mse_x = mean_squared_error(middle_bins, NNx, norm, norm_x_fit_params)
mse_y = mean_squared_error(middle_bins, NNy, norm, norm_y_fit_params)

s1.plot(xaxis, norm_x_pdf_fit, ls='--',
        label= r'$\frac{1}{\sqrt{2 \pi \sigma_a^2}} \exp(\frac{-a^2}{2 \sigma_a^2})$')
s1.plot(xaxis, norm_y_pdf_fit, ls='--',
        label= r'$\frac{1}{\sqrt{2 \pi \sigma_c^2}} \exp(\frac{-c^2}{2 \sigma_c^2})$')

# s1.set_title(f'Time domain signal distributions - N = {sample_number} samples')
s1.set_xlabel(r'Sample values')
s1.set_ylabel('Normalized Occurances')

s1.set_xlim([-15, 15])

s1.legend()
s1.grid()

plot_name = f'raw_samples_histograms_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Histogram of products of random samples
fig, (s1) = plt.subplots(1)

num_bins = 900
low_bin  = 0
high_bin =  300
bins = np.linspace(low_bin, high_bin, num_bins+1)
xaxis = np.linspace(low_bin, high_bin, 3001)

chi2_p_fit_params = chi2.fit(pp, fdf=1)
print(f'chi2_p_fit_params = {chi2_p_fit_params}')
chi2_p_pdf_fit = chi2.pdf(xaxis, 1, loc=chi2_p_fit_params[-2], scale=chi2_p_fit_params[-1])

chi2_r_fit_params = chi2.fit(rr, fdf=1)
print(f'chi2_r_fit_params = {chi2_r_fit_params}')
chi2_r_pdf_fit = chi2.pdf(xaxis, 1, loc=chi2_r_fit_params[-2], scale=chi2_r_fit_params[-1])

NNp, bins, patches = s1.hist(pp, bins=bins, histtype='step', lw=2, density=True, label=r'$\mathcal{G}_a = \mathcal{A}^2$ samples')
NNr, bins, patches = s1.hist(rr, bins=bins, histtype='step', lw=2, density=True, label=r'$\mathcal{G}_c = \mathcal{C}^2$ samples')

s1.plot(xaxis, chi2_p_pdf_fit, ls='--', label=r'$\chi_1^2(\sigma_a)$ PDF')
s1.plot(xaxis, chi2_r_pdf_fit, ls='--', label=r'$\chi_1^2(\sigma_c)$ PDF')

# s1.set_title(f'Time domain signal distributions - N = {sample_number} samples')
s1.set_xlabel(r'Sample values')
s1.set_ylabel('Normalized Occurances')

s1.set_xlim([0, 30])
s1.set_ylim([0, 0.2])

s1.legend()
s1.grid()

plot_name = f'gaussian_product_X_squared_histograms_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Histogram of product of random gaussina AC, good for component of csd
fig, (s1) = plt.subplots(1)

num_bins = 900
low_bin  = -300
high_bin =  300
bins = np.linspace(low_bin, high_bin, num_bins+1)
xaxis = np.linspace(low_bin, high_bin, 3001)

NNx, bins, patches = s1.hist(qq, bins=bins, histtype='step', lw=2, density=True, label=r'$\mathcal{Z} = \mathcal{A}\mathcal{C}$ samples')

k0 = kn(0, np.abs(xaxis)/(sigma_x * sigma_y)) / (np.pi * sigma_x * sigma_y)
s1.plot(xaxis, k0, ls='--', label=r'$K_0(\frac{\|x\|}{\sigma_x \sigma_y})/\pi \sigma_x^2 \sigma_y^2$')

# s1.set_title(f'Time domain signal distributions - N = {sample_number} samples')
s1.set_xlabel(r'Sample values')
s1.set_ylabel('Normalized Occurances')

s1.set_xlim([-50, 50])
# s1.set_ylim([0, 0.05])

s1.legend()
s1.grid()

plot_name = f'gaussian_product_XY_histograms_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Histogram of sums of products of gaussians, asymmetric laplace for csd
fig, (s1) = plt.subplots(1)

num_bins = 900
low_bin  = -300
high_bin =  300
bins = np.linspace(low_bin, high_bin, num_bins+1)
xaxis = np.linspace(low_bin, high_bin, 3001)

NNss, bins, patches = s1.hist(ss, bins=bins, histtype='step', lw=2, density=True, 
                    label=r'$\mathcal{U} = \mathcal{A}(\mathcal{A} + \mathcal{C}) + \mathcal{B}(\mathcal{B} + \mathcal{D})$ samples')
NNtt, bins, patches = s1.hist(tt, bins=bins, histtype='step', lw=2, density=True, 
                    label=r'$\mathcal{V} = \mathcal{A}\mathcal{D} - \mathcal{B}\mathcal{C}$ samples')

scale = 1/(sigma_x * sigma_y)
kappa = -sigma_x/sigma_y * (1 - np.sqrt(1 + sigma_y**2 / sigma_x**2))

p0 = [0, scale, 1.0]
middle_bins = mid_bins(bins)
laplace_ss_fit_params, laplace_ss_fit_cov  = curve_fit(asymmetric_laplace, middle_bins, NNss, p0=p0)
print(f'laplace_ss_fit_params = {laplace_ss_fit_params}')
laplace_ss_pdf_fit = asymmetric_laplace(xaxis, *laplace_ss_fit_params)

laplace_tt_fit_params, laplace_tt_fit_cov  = curve_fit(asymmetric_laplace, middle_bins, NNtt, p0=p0)
print(f'laplace_tt_fit_params = {laplace_tt_fit_params}')
laplace_tt_pdf_fit = asymmetric_laplace(xaxis, *laplace_tt_fit_params)

ss_fit = asymmetric_laplace(xaxis, 0, scale, kappa)
tt_fit = asymmetric_laplace(xaxis, 0, scale, 1)

s1.plot(xaxis, laplace_ss_pdf_fit, ls='--', 
    label=r'$\mathrm{AL}(0, \sigma_a \sigma_c, -\frac{\sigma_a}{\sigma_c} \left( 1 - \sqrt{1 + \frac{\sigma_c^2}{\sigma_a^2} } \right)) $')
s1.plot(xaxis, laplace_tt_pdf_fit, ls='--', 
    label=r'$\mathrm{Laplace}(0, \sigma_a \sigma_c)$')

# Plot statistics
ss_mean = np.mean(ss)
ss_median = np.median(ss)

tt_mean = np.mean(tt)
tt_median = np.median(tt)

s1.plot([ss_mean, ss_mean],     [0, 0.025], ls='-',  color='C0', alpha=0.5, label=r'$\mathcal{U}$'+f' mean = {ss_mean:.1f}')
s1.plot([ss_median, ss_median], [0, 0.025], ls='--', color='C0', alpha=0.5, label=r'$\mathcal{U}$'+f' median = {ss_median:.1f}')

s1.plot([tt_mean, tt_mean],     [0, 0.025], ls='-',  color='C1', alpha=0.5, label=r'$\mathcal{V}$'+f' mean = {tt_mean:.1f}')
s1.plot([tt_median, tt_median], [0, 0.025], ls='--', color='C1', alpha=0.5, label=r'$\mathcal{V}$'+f' median = {tt_median:.1f}')

# s1.set_title(f'Time domain signal distributions - N = {sample_number} samples')
s1.set_xlabel(r'Sample values')
s1.set_ylabel('Normalized Occurances')

s1.set_xlim([-50, 100])
s1.set_ylim([0, 0.025])

s1.legend()
s1.grid()

plot_name = f'sum_of_products_of_gaussians_AC_BD_histograms_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Histogram of sums of products of gaussians, exponential, lapalce  and asymmetric laplace histograms
fig, (s1) = plt.subplots(1)

num_bins = 900
low_bin  = -300
high_bin =  300
bins = np.linspace(low_bin, high_bin, num_bins+1)
xaxis = np.linspace(low_bin, high_bin, 3001)

halfxaxis = xaxis[1500:]
lefthalfxaxis = xaxis[:1500]

NNuu, bins, patches = s1.hist(uu, bins=bins, histtype='step', lw=2, density=True, 
                    label=r'$\mathcal{U} = \mathcal{X}^2 + \mathcal{A}^2$ samples')
NNzz, bins, patches = s1.hist(zz, bins=bins, histtype='step', lw=2, density=True, 
                    label=r'$\mathcal{Z} = \mathcal{Y}^2 + \mathcal{B}^2$ samples')

# NNdd, bins, patches = s1.hist(dd, bins=bins, histtype='step', lw=2, density=True, 
#                     label=r'$\mathcal{D} = \mathcal{U} - \mathcal{Z}$ samples')
# NNee, bins, patches = s1.hist(ee, bins=bins, histtype='step', lw=2, density=True, 
#                     label=r'$\mathcal{E} = \mathcal{U} + \mathcal{Z}$ samples')

NNvv, bins, patches = s1.hist(vv, bins=bins, histtype='step', lw=2, density=True, 
                    label=r'$\mathcal{V} = \mathcal{A}\mathcal{D} - \mathcal{B}\mathcal{C}$ samples')
# NNtt, bins, patches = s1.hist(tt, bins=bins, histtype='step', lw=2, density=True, 
#                     label=r'$\mathcal{T} = \mathcal{X}\mathcal{Y} - \mathcal{A}\mathcal{B}$ samples')

NNss, bins, patches = s1.hist(ss, bins=bins, histtype='step', lw=2, density=True, 
                    label=r'$\mathcal{U} = \mathcal{A}(\mathcal{A} + \mathcal{C}) + \mathcal{B}(\mathcal{B} + \mathcal{D})$ samples')

# Fits
exp_scale_x = 2 * sigma_x**2
exp_scale_y = 2 * sigma_y**2

exp_fit_x = np.exp(-halfxaxis/exp_scale_x) / exp_scale_x
exp_fit_y = np.exp(-halfxaxis/exp_scale_y) / exp_scale_y

sum_exp_fit = (np.exp(-halfxaxis/exp_scale_x) - np.exp(-halfxaxis/exp_scale_y))/(exp_scale_x - exp_scale_y)

diff_exp_fit_left  = np.exp(lefthalfxaxis/exp_scale_y)/(exp_scale_x + exp_scale_y)
diff_exp_fit_right = np.exp(-halfxaxis/exp_scale_x)/(exp_scale_x + exp_scale_y)

gamma = 1/(sigma_x * sigma_y) # alpha_2
kappa = -sigma_x/sigma_y * (1 - np.sqrt(1 + sigma_y**2 / sigma_x**2))

p0 = [0, gamma, 1.0]
middle_bins = mid_bins(bins)
laplace_ss_fit_params, laplace_ss_fit_cov  = curve_fit(asymmetric_laplace, middle_bins, NNss, p0=p0)
print(f'laplace_ss_fit_params = {laplace_ss_fit_params}')
laplace_ss_pdf_fit = asymmetric_laplace(xaxis, *laplace_ss_fit_params)

laplace_tt_fit_params, laplace_tt_fit_cov  = curve_fit(asymmetric_laplace, middle_bins, NNtt, p0=p0)
print(f'laplace_tt_fit_params = {laplace_tt_fit_params}')
laplace_tt_pdf_fit = asymmetric_laplace(xaxis, *laplace_tt_fit_params)

ss_fit = asymmetric_laplace(xaxis, 0, gamma, kappa)
tt_fit = asymmetric_laplace(xaxis, 0, gamma, 1)

dd_fit = asymmetric_laplace(xaxis, 0, 1/(2 * sigma_x * sigma_y), sigma_y/sigma_x)

# Plot Fits
s1.plot(halfxaxis, exp_fit_x,   ls='--', label=r'$\mathrm{Exp}(2 \sigma_a^2)$')
s1.plot(halfxaxis, exp_fit_y,   ls='--', label=r'$\mathrm{Exp}(2 \sigma_c^2)$')
# s1.plot(halfxaxis, sum_exp_fit, ls='--', label=r'$(e^{-x/2 \sigma_x^2} - e^{-x/2 \sigma_y^2})/(2 \sigma_x^2 - 2 \sigma_y^2)$')

# s1.plot(lefthalfxaxis, diff_exp_fit_left, ls='--', label=r'$e^{x/2 \sigma_y^2}/(2 \sigma_x^2 + 2 \sigma_y^2)$')
# s1.plot(halfxaxis, diff_exp_fit_right, ls='--', label=r'$e^{-x/2 \sigma_x^2}/(2 \sigma_x^2 + 2 \sigma_y^2)$')

# s1.plot(xaxis, laplace_ss_pdf_fit, ls='--', label=r'$\mathrm{AsymLaplace(0, \frac{1}{\sigma_x \sigma_y}, \kappa) }$')
s1.plot(xaxis, laplace_tt_pdf_fit, ls='--', label=r'$\mathrm{Laplace}(0, \sigma_a \sigma_c)$')

s1.plot(xaxis, ss_fit, ls='-.', 
label=r'$\mathrm{AL}\left(0, \sigma_a \sigma_c, -\frac{\sigma_a}{\sigma_c} \left( 1 - \sqrt{1 + \frac{\sigma_c^2}{\sigma_a^2} } \right)\right)$')

# s1.set_title(f'Time domain signal distributions - N = {sample_number} samples')
s1.set_xlabel(r'Sample values')
s1.set_ylabel('Normalized Occurances')

s1.set_xlim([-50, 100])
# s1.set_ylim([0, 0.05])

s1.legend(fontsize=14)
s1.grid()

plot_name = f'exponential_and_laplace_histograms_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Histogram of exponentials
fig, (s1) = plt.subplots(1)

num_bins = 900
low_bin  = -300
high_bin =  300
bins = np.linspace(low_bin, high_bin, num_bins+1)
xaxis = np.linspace(low_bin, high_bin, 3001)

halfxaxis = xaxis[1500:]
lefthalfxaxis = xaxis[:1500]

NNuu, bins, patches = s1.hist(uu, bins=bins, histtype='step', lw=2, density=True, 
                    label=r'$\mathcal{Z}_a = \mathcal{A}^2 + \mathcal{B}^2$ samples')
NNzz, bins, patches = s1.hist(zz, bins=bins, histtype='step', lw=2, density=True, 
                    label=r'$\mathcal{Z}_c = \mathcal{C}^2 + \mathcal{D}^2$ samples')

# Fits
exp_scale_x = 2 * sigma_x**2
exp_scale_y = 2 * sigma_y**2

exp_fit_x = np.exp(-halfxaxis/exp_scale_x) / exp_scale_x
exp_fit_y = np.exp(-halfxaxis/exp_scale_y) / exp_scale_y

# Plot Fits
s1.plot(halfxaxis, exp_fit_x,   ls='--', label=r'$\mathrm{Exp}(2 \sigma_a^2)$')
s1.plot(halfxaxis, exp_fit_y,   ls='--', label=r'$\mathrm{Exp}(2 \sigma_c^2)$')

# Means and Medians
uu_mean = np.mean(uu)
uu_median = np.median(uu)

zz_mean = np.mean(zz)
zz_median = np.median(zz)

# Plot Means and Medians
s1.plot([uu_mean, uu_mean], [0, 0.035], color='C0', ls='-', alpha=0.5, label=r'$\mathcal{Z}_a$'+ f' mean = {uu_mean:.1f}')
s1.plot([uu_median, uu_median], [0, 0.035], color='C0', ls='--', alpha=0.5, label=r'$\mathcal{Z}_a$'+ f' median = {uu_median:.1f}')

s1.plot([zz_mean, zz_mean], [0, 0.035], color='C1', ls='-', alpha=0.5, label=r'$\mathcal{Z}_c$'+ f' mean = {zz_mean:.1f}')
s1.plot([zz_median, zz_median], [0, 0.035], color='C1', ls='--', alpha=0.5, label=r'$\mathcal{Z}_c$'+ f' median = {zz_median:.1f}')

# s1.set_title(f'Time domain signal distributions - N = {sample_number} samples')
s1.set_xlabel(r'Sample values')
s1.set_ylabel('Normalized Occurances')

s1.set_xlim([0, 150])
s1.set_ylim([0, 0.035])

s1.legend()
s1.grid()

plot_name = f'exponential_histograms_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Histogram of rayleighs
fig, (s1) = plt.subplots(1)

num_bins = 900
low_bin  = 0
high_bin = 100
bins = np.linspace(low_bin, high_bin, num_bins+1)
xaxis = np.linspace(low_bin, high_bin, 3001)

halfxaxis = xaxis[1500:]
lefthalfxaxis = xaxis[:1500]

NNuu, bins, patches = s1.hist(np.sqrt(uu), bins=bins, histtype='step', lw=2, density=True, 
                    label=r'$\sqrt{\mathcal{Z}_a} = \sqrt{ \mathcal{A}^2 + \mathcal{B}^2 }$ samples')
NNzz, bins, patches = s1.hist(np.sqrt(zz), bins=bins, histtype='step', lw=2, density=True, 
                    label=r'$\sqrt{\mathcal{Z}_c} = \sqrt{ \mathcal{C}^2 + \mathcal{D}^2 }$ samples')

# Fits
ray_fit_x = xaxis/sigma_x**2 * np.exp(-xaxis**2 / (2 * sigma_x**2))
ray_fit_y = xaxis/sigma_y**2 * np.exp(-xaxis**2 / (2 * sigma_y**2))

# Plot Fits
s1.plot(xaxis, ray_fit_x,   ls='--', label=r'$\mathrm{Rayleigh}(\sigma_a)$')
s1.plot(xaxis, ray_fit_y,   ls='--', label=r'$\mathrm{Rayleigh}(\sigma_c)$')

# Means and Medians
uu_mean = np.mean(np.sqrt(uu))
uu_median = np.median(np.sqrt(uu))
uu_rms = np.sqrt(np.mean(uu))

zz_mean = np.mean(np.sqrt(zz))
zz_median = np.median(np.sqrt(zz))
zz_rms = np.sqrt(np.mean(zz))

# Plot Means and Medians
s1.plot([uu_mean, uu_mean], [0, 0.2], color='C0', ls='-', alpha=0.5, label=r'$\sqrt{\mathcal{Z}_a}$'+ f' mean = {uu_mean:.1f}')
s1.plot([uu_median, uu_median], [0, 0.2], color='C0', ls='--', alpha=0.5, label=r'$\sqrt{\mathcal{Z}_a}$'+ f' median = {uu_median:.1f}')
s1.plot([uu_rms, uu_rms], [0, 0.2], color='C0', ls=':', alpha=0.5, label=r'$\sqrt{\mathcal{Z}_a}$'+ f' rms = {uu_rms:.1f}')

s1.plot([zz_mean, zz_mean], [0, 0.2], color='C1', ls='-', alpha=0.5, label=r'$\sqrt{\mathcal{Z}_c}$'+ f' mean = {zz_mean:.1f}')
s1.plot([zz_median, zz_median], [0, 0.2], color='C1', ls='--', alpha=0.5, label=r'$\sqrt{\mathcal{Z}_c}$'+ f' median = {zz_median:.1f}')
s1.plot([zz_rms, zz_rms], [0, 0.2], color='C1', ls=':', alpha=0.5, label=r'$\sqrt{\mathcal{Z}_c}$'+ f' rms = {zz_rms:.1f}')

# s1.set_title(f'Time domain signal distributions - N = {sample_number} samples')
s1.set_xlabel(r'Sample values')
s1.set_ylabel('Normalized Occurances')

s1.set_xlim([0, 20])
s1.set_ylim([0, 0.2])

s1.legend()
s1.grid()

plot_name = f'rayleigh_histograms_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Histogram of exp and laplace and their sum
fig, (s1) = plt.subplots(1)

sigma_a = 4.0
sigma_b = 6.0
# sample_number = 1000000

num_bins = 900
low_bin  = -300
high_bin =  300
bins = np.linspace(low_bin, high_bin, num_bins+1)
xaxis = np.linspace(low_bin, high_bin, 3001)

halfxaxis = xaxis[1500:]
lefthalfxaxis = xaxis[:1500]

exponential = np.random.normal(scale=sigma_a, size=sample_number)**2 + np.random.normal(scale=sigma_a, size=sample_number)**2
laplace = np.random.normal(scale=sigma_a, size=sample_number) * np.random.normal(scale=sigma_b, size=sample_number) \
        + np.random.normal(scale=sigma_a, size=sample_number) * np.random.normal(scale=sigma_b, size=sample_number)
sum_exp_lpc = exponential + laplace


NNexp, bins, patches = s1.hist(exponential, bins=bins, histtype='step', lw=2, density=True, 
                    label=r'$\mathcal{X}$ exponential samples')
NNlpc, bins, patches = s1.hist(laplace, bins=bins, histtype='step', lw=2, density=True, 
                    label=r'$\mathcal{Y}$ laplace samples ')

NNsum, bins, patches = s1.hist(sum_exp_lpc, bins=bins, histtype='step', lw=2, density=True, 
                    label=r'$\mathcal{Z} = \mathcal{X} + \mathcal{Y}$ exponential + laplace')

# Fits
exp_scale = 2 * sigma_a**2 # lambda
exp_fit = np.exp(-halfxaxis/exp_scale) / exp_scale

lpc_scale = sigma_a * sigma_b # gamma
lpc_fit = asymmetric_laplace(xaxis, 0, 1/lpc_scale, 1)

# p0 = [0, lpc_scale, 1.0]
# middle_bins = mid_bins(bins)
# sum_exp_lpc_fit_params, sum_exp_lpc_fit_cov  = curve_fit(asymmetric_laplace, middle_bins, NNss, p0=p0)
# print(f'sum_exp_lpc_fit_params = {sum_exp_lpc_fit_params}')
# sum_exp_lpc_pdf_fit = asymmetric_laplace(xaxis, *sum_exp_lpc_fit_params)

sum_exp_lpc_pdf_fit_left = np.exp(lefthalfxaxis / lpc_scale) / (2 * (lpc_scale + exp_scale))
sum_exp_lpc_pdf_fit_right =  np.exp(-halfxaxis / lpc_scale) / (2 * (lpc_scale - exp_scale)) \
                          + (np.exp(-halfxaxis / exp_scale) * exp_scale / (exp_scale**2 - lpc_scale**2) )

# Plot Fits
s1.plot(halfxaxis, exp_fit, ls='--', 
    label=r'$\mathrm{Exp}(\lambda)$')
s1.plot(xaxis, lpc_fit, ls='--', 
    label=r'$\mathrm{Laplace(0, \gamma)}$')
s1.plot(lefthalfxaxis, sum_exp_lpc_pdf_fit_left, ls='--', 
    label=r'$\frac{ \exp(\frac{x}{\gamma}) }{ 2 (\lambda + \gamma)) }$')
s1.plot(halfxaxis, sum_exp_lpc_pdf_fit_right, ls='--', 
    label=r'$ \frac{ \exp(-\frac{x}{\gamma})}{2 (\gamma - \lambda)} + \frac{ \exp(-\frac{x}{\lambda}) \lambda}{\lambda^2 - \gamma^2}$')

s1.set_title(f'Sum of Exponential and Laplace distributions - N = {sample_number} samples')
s1.set_xlabel(r'Sample values')
s1.set_ylabel('Normalized Occurances')

s1.set_xlim([-100, 150])
# s1.set_ylim([0, 0.05])

s1.legend()
s1.grid()

plot_name = f'test_exponential_and_laplace_sum_histograms_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Histogram of A^2 + AC
fig, (s1) = plt.subplots(1)

sigma_bleh1 = 7.0   # sigma_a
sigma_bleh2 = 3.0   # sigma_c
# sample_number = 1000000

bleh1 = np.random.normal(scale=sigma_bleh1, size=sample_number) # A
bleh2 = np.random.normal(scale=sigma_bleh2, size=sample_number) # C
blehbleh = bleh1 * (bleh1 + bleh2)

# bessel_modified = np.exp( xaxis / sigma_bleh2**2 ) \
#                 * kn(0, np.sqrt(1/sigma_bleh1**2 + 1/sigma_bleh2**2)/(np.sqrt(sigma_bleh2**2/xaxis**2)) ) \
#                 / (np.pi * sigma_bleh1 * sigma_bleh2)

bessel_exp_params = [   sigma_bleh2**2,
                        np.sqrt(1/sigma_bleh1**2 + 1/sigma_bleh2**2) / np.sqrt(sigma_bleh2**2),
                        np.pi * sigma_bleh1 * sigma_bleh2
                        ]
bessel_modified = bessel_exp_fit( xaxis, *bessel_exp_params )

NNexp, bins, patches = s1.hist(blehbleh, bins=bins, histtype='step', lw=2, density=True, 
                    label=r'$\mathcal{S} = \mathcal{A}(\mathcal{A} + \mathcal{C})$ samples')

# Plot Fits
s1.plot(xaxis, bessel_modified, ls='--', 
    label=r'$\frac{1}{\pi \sigma_a \sigma_c} e^{ s/\sigma_c^2 } K_0\left( \frac{|s|}{\sigma_c} \sqrt{\frac{1}{\sigma_a^2} + \frac{1}{\sigma_c^2}} \right)$')

# s1.plot(xaxis, test_func, ls=':', label='Test bessel func')

# s1.set_title(f'Sum of Gaussian squared plus Gaussian product - N = {sample_number} samples')
s1.set_xlabel(r'Sample values')
s1.set_ylabel('Normalized Occurances')

s1.set_xlim([-50, 150])
s1.set_ylim([0, 0.06])

s1.legend()
s1.grid()

plot_name = f'AA_plus_AC_normal_histogram_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Illustrate the mean is invariant with changing uncorrelated noise
fig, (s1) = plt.subplots(1)

sigma_corr = 5.0   # sigma_a
sigma_uncorrs = np.array([1.0, 5.0, 10.0, 25.0, 50.0])   # sigma_c
colors = np.array(['C0', 'C1', 'C2', 'C3', 'C4'])
# sample_number = 1000000

AA = np.random.normal(scale=sigma_corr, size=sample_number) # A
BB = np.random.normal(scale=sigma_corr, size=sample_number) # B

fits = {}
fit_paramses = {}
means = {}
medians = {}
for sigma_uncorr, color in zip(sigma_uncorrs, colors):
    CC = np.random.normal(scale=sigma_uncorr, size=sample_number) # C
    DD = np.random.normal(scale=sigma_uncorr, size=sample_number) # D

    RR = AA * (AA + CC) + BB * (BB + DD)
    RR_mean = np.mean(RR)
    RR_median = np.median(RR)
    print(f'RR_mean = {RR_mean}')
    print(f'RR_median = {RR_median}')
    
    NN, bins, patches = s1.hist(RR, bins=bins, histtype='step', color=color, lw=2, density=True, 
                                label=f'$\sigma_c$ = {sigma_uncorr:.1f}' + ', ' + f'$\sigma_a$ = {sigma_corr:.1f}')

    middle_bins = mid_bins(bins)
    fit_params, fit_cov  = curve_fit(asymmetric_laplace, middle_bins, NN, p0=p0)
    print(f'fit_params = {fit_params}')
    fit = asymmetric_laplace(xaxis, *fit_params)

    fits[sigma_uncorr] = fit
    fit_paramses[sigma_uncorr] = fit_params
    means[sigma_uncorr] = RR_mean
    medians[sigma_uncorr] = RR_median

for sigma_uncorr, color in zip(sigma_uncorrs, colors):
    fit = fits[sigma_uncorr]
    fit_params = fit_paramses[sigma_uncorr]
    s1.plot(xaxis, fit, color=color, ls='--', alpha=0.5, label=r'$\mathrm{AL}$' + f'({fit_params[0]:.2f}, {fit_params[1]:.2f}, {fit_params[2]:.2f})')

for sigma_uncorr, color in zip(sigma_uncorrs, colors):
    RR_median = medians[sigma_uncorr]
    # s1.plot([RR_mean, RR_mean], [0, 0.06], color=color, ls='-', alpha=0.5, label=f'mean {RR_mean:.1f}')
    s1.plot([RR_median, RR_median], [0, 0.06], color=color, ls='-', alpha=0.8, label=f'median {RR_median:.1f}')

s1.plot([RR_mean, RR_mean], [0, 0.06], color='k', ls='-', label=f'mean $2 \sigma_a^2$ = {RR_mean:.1f}')

s1.set_xlabel(r'CSD sample values $\langle x, z \rangle$ [$\mathrm{V^2/Hz}$]')
s1.set_ylabel('Normalized Occurances')

s1.set_xlim([-60, 60])
s1.set_ylim([0, 0.02])

s1.legend()
s1.grid()

plot_name = f'invariant_mean_of_asymmetric_laplace_histograms_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Plot half of real part of csd <x,y>, where x = a + c, y = b + c
# <x,y> = <a,b> + <a,c> + <c,b> + <c,c>
# Re(<x,y>) = (Ar*Br + Ai*Bi) + (Ar*Cr + Ai*Ci) + (Cr*Br + Ci*Bi) + Cr**2 + Ci**2
# Real random variables only: Mr = Ar*Br + Ar*Cr + Cr*Br + Cr**2
# Imag random variables only: Mi = Ai*Bi + Ai*Ci + Ci*Bi + Ci**2
# All (Ar, Ai, Br, Bi, Cr, Ci) are real idependent Gaussian variables
fig, (s1) = plt.subplots(1)


# Make random data
sigma_aa = 1e-3
sigma_bb = 1e-3
sigma_cc = 7e-4

Ar = np.random.normal(scale=sigma_aa, size=sample_number)
Br = np.random.normal(scale=sigma_bb, size=sample_number)
Cr = np.random.normal(scale=sigma_cc, size=sample_number)

Ai = np.random.normal(scale=sigma_aa, size=sample_number)
Bi = np.random.normal(scale=sigma_bb, size=sample_number)
Ci = np.random.normal(scale=sigma_cc, size=sample_number)

Mr = Ar*Br + Ar*Cr + Cr*Br + Cr**2
Mi = Ai*Bi + Ai*Ci + Ci*Bi + Ci**2
PP = Mr + Mi

QQ  = Ar * Bi - Ai * Br \
    + Ar * Ci - Ai * Cr \
    + Cr * Bi - Ci * Br

# Basic sample statistics

# Define bins
num_bins = 2000
low_bin  = -1e-5
high_bin =  1e-5
bins = np.linspace(low_bin, high_bin, num_bins)
xaxis = np.linspace(low_bin, high_bin, 6000)

# Make analytic fit
p0 = [  bessel_param_a(sigma_aa, sigma_bb, sigma_cc), 
        bessel_param_b(sigma_aa, sigma_bb, sigma_cc), 
        bessel_param_c(sigma_aa, sigma_bb, sigma_cc)]
bessel_modified_m = bessel_exp_fit(xaxis, *p0)

# Plot histogram
NNm, bins, patches = s1.hist(Mr, bins=bins, histtype='step', lw=2, density=True, 
    label=r'$\mathcal{P} = (\mathcal{A}_r + \mathcal{C}_r)(\mathcal{B}_r + \mathcal{C}_r)$' + '\n'
         +r'$\sigma_{a} =$'+ f' {sigma_aa:.1e}, '+r'$\sigma_{b} =$'+ f' {sigma_bb:.1e}, '+r'$\sigma_{c} =$'+ f' {sigma_cc:.1e}, ')

# Plot analytic fit
s1.plot(xaxis, bessel_modified_m, ls='--', 
    label=r'$\frac{e^{\frac{m \sigma_c^2}{\sigma_a^2 \left(\sigma_b^2+\sigma_c^2\right)+\sigma_b^2 \sigma_c^2}}'
         +r'K_0\left(\sqrt{m^2} \sqrt{\frac{\left(\sigma_a^2+\sigma_c^2\right) \left(\sigma_b^2+\sigma_c^2\right)}'
         +r'{\left(\left(\sigma_b^2+\sigma_c^2\right) \sigma_a^2+\sigma_b^2 \sigma_c^2\right)^2}}\right)}'
         +r'{\pi \sqrt{\sigma_a^2 \left(\sigma_b^2+\sigma_c^2\right)+\sigma_b^2 \sigma_c^2}}$'
         )

# s1.plot(xaxis[len(xaxis)//2:], gamma_fit, ls='--', color='C4', label='Gamma')
# s1.set_title(f'Random variable '+r'$\mathcal{M} = (\mathcal{A} + \mathcal{C}) (\mathcal{B} + \mathcal{C}) $ samples')
s1.set_xlabel(r'Sample values $m$')
s1.set_ylabel('Normalized Occurances')

s1.set_xlim([-5e-7, 8e-7])
# s1.set_ylim([0, 0.10])

s1.legend()
s1.grid()

plot_name = f'three_random_gaussians_correlated_signal_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Plot the real and imag parts of csd <x,y>, where x = a + c, y = b + c
# <x,y> = <a,b> + <a,c> + <c,b> + <c,c>
# Re(<x,y>) = (Ar*Br + Ai*Bi) + (Ar*Cr + Ai*Ci) + (Cr*Br + Ci*Bi) + Cr**2 + Ci**2
# Real random variables only: Mr = Ar*Br + Ar*Cr + Cr*Br + Cr**2
# Imag random variables only: Mi = Ai*Bi + Ai*Ci + Ci*Bi + Ci**2
# All (Ar, Ai, Br, Bi, Cr, Ci) are real idependent Gaussian variables
fig, (s1) = plt.subplots(1)


# Define bins
num_bins = 2000
low_bin  = -5e-5
high_bin =  5e-5
bins = np.linspace(low_bin, high_bin, num_bins)
xaxis = np.linspace(low_bin, high_bin, 6000)

# Make analytic fit
p_aa = sigma_aa**2
p_bb = sigma_bb**2
p_cc = sigma_cc**2

lambda1 = np.sqrt( p_aa * p_bb + p_aa * p_cc + p_bb * p_cc )
kappa1 = np.sqrt( 1 + (2 * p_cc**2 - 2 * p_cc * np.sqrt( (p_aa + p_cc) * (p_bb + p_cc) )) \
                        / (p_aa * p_bb + p_aa * p_cc + p_bb * p_cc) )

laplace_qq = asymmetric_laplace(xaxis, 0, 1/lambda1, 1.0)
asymmetric_laplace_pp = asymmetric_laplace(xaxis, 0, 1/lambda1, kappa1)

# Plot histogram
NNmm, bins, patches = s1.hist(PP, bins=bins, histtype='step', lw=2, density=True, 
    label=r'$\mathcal{P} = (\mathcal{A}_r + \mathcal{C}_r)(\mathcal{B}_r + \mathcal{C}_r) + $' + '\n'
        + r'$(\mathcal{A}_i + \mathcal{C}_i)(\mathcal{B}_i + \mathcal{C}_i)$')

NNpp, bins, patches = s1.hist(QQ, bins=bins, histtype='step', lw=2, density=True, 
    label=r'$\mathcal{Q} = \mathcal{A}_r \mathcal{B}_i - \mathcal{A}_i \mathcal{B}_r + \mathcal{A}_r \mathcal{C}_i $' + '\n'
        + r'$ - \mathcal{A}_i \mathcal{C}_r + \mathcal{C}_r \mathcal{B}_i - \mathcal{C}_i \mathcal{B}_r $' )

# Plot analytic fit
s1.plot(xaxis, laplace_qq, ls='--', label='Laplace')
s1.plot(xaxis, asymmetric_laplace_pp, ls='--', label='Asymmetric Laplace')

# Get statistics
PP_mean = np.mean(PP)
PP_median = np.median(PP)
PP_var = np.var(PP)

QQ_mean = np.mean(QQ)
QQ_median = np.median(QQ)
QQ_var = np.var(QQ)

# Plot statisitcs
ylims = [0, 400000]
s1.plot([PP_mean, PP_mean],     ylims, color='C0', ls='-',  alpha=0.6, label=r'$\mathcal{P}$' + f' mean = {PP_mean:.1e}')
s1.plot([PP_median, PP_median], ylims, color='C0', ls='--', alpha=0.6, label=r'$\mathcal{P}$' + f' median = {PP_median:.1e}')
s1.plot([QQ_mean, QQ_mean],     ylims, color='C1', ls='-',  alpha=0.6, label=r'$\mathcal{Q}$' + f' mean = {QQ_mean:.1e}')
s1.plot([QQ_median, QQ_median], ylims, color='C1', ls='--', alpha=0.6, label=r'$\mathcal{Q}$' + f' median = {QQ_median:.1e}')

# s1.plot(xaxis[len(xaxis)//2:], gamma_fit, ls='--', color='C4', label='Gamma')
# s1.set_title(f'Random variable '+r'$\mathcal{M} = (\mathcal{A} + \mathcal{C}) (\mathcal{B} + \mathcal{C}) $ samples')
s1.set_xlabel(r'Sample values')
s1.set_ylabel('Normalized Occurances')

s1.set_xlim([-3e-6, 6e-6])
s1.set_ylim(ylims)

s1.legend()
s1.grid()

plot_name = f'six_random_gaussians_correlated_signals_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()