'''
phase_csd.py

Plots the effect the phase has on the median-averaged CSD.

Craig Cahillane
September 12, 2020
'''

import os
import time
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt

mpl.rcParams.update({'figure.figsize': (12, 9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'lines.markersize': 3,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.9,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'legend.columnspacing': 2,
                     'savefig.dpi': 80,
                     'pdf.compression': 9})



#####   Git repo path   #####

script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

#####   Functions   #####

def csd_median_real(scaler, coherence, phi):
    '''Calculates the median along the real axis of a cross spectral density.
    Inputs:
    scaler      = cross power scaler, lambda in my thesis
    coherence   = mean-averaged coherence
    phi         = phase of the cross spectral density
    '''
    g = np.sqrt(coherence)
    g2 = coherence
    root = 1 - g2 + g2 * np.cos(phi)**2
    numer = scaler * np.sqrt(1 - g2) * np.log(( root - g * np.abs(np.cos(phi)) * np.sqrt(root) ) / (1 - g2)) 
    denom = g * np.cos(phi) - np.sign(np.cos(phi)) * np.sqrt(root)
    median = numer / denom
    return median

def csd_median_imag(scaler, coherence, phi):
    '''Calculates the median along the real axis of a cross spectral density.
    Inputs:
    scaler      = cross power scaler, lambda in my thesis
    coherence   = mean-averaged coherence
    phi         = phase of the cross spectral density
    '''
    g = np.sqrt(coherence)
    g2 = coherence
    root = 1 - g2 + g2 * np.sin(phi)**2
    numer = scaler * np.sqrt(1 - g2) * np.log(( root - g * np.abs(np.sin(phi)) * np.sqrt(root) ) / (1 - g2)) 
    denom = g * np.sin(phi) - np.sign(np.sin(phi)) * np.sqrt(root)
    median = numer / denom
    return median



#####   Parameters   #####
scaler = 1.0
coherence_list = np.array([0.1, 0.3, 0.9, 0.999])
phis = np.linspace(0, 2*np.pi, 500)

coherences = np.logspace(-4, 0, 3000)

#####   Calculate medians with changing phi   #####
data_dict = {}
for coherence in coherence_list:
    real_medians = csd_median_real(scaler, coherence, phis)
    imag_medians = csd_median_imag(scaler, coherence, phis)
    medians = np.sqrt(real_medians**2 + imag_medians**2)

    amp = csd_median_real(scaler, coherence, 0)

    normalized_medians = medians / amp

    data_dict[coherence] = {}
    data_dict[coherence]['real_medians'] = real_medians
    data_dict[coherence]['imag_medians'] = imag_medians
    data_dict[coherence]['medians'] = medians
    data_dict[coherence]['normalized_medians'] = normalized_medians


#####   Calculate medians with changing coherences   #####
phi0 = np.pi/4
real_medians0 = csd_median_real(scaler, coherences, phi0)
imag_medians0 = csd_median_imag(scaler, coherences, phi0)
medians0 = np.sqrt(real_medians0**2 + imag_medians0**2)

amps0 = csd_median_real(scaler, coherences, 0)

normalized_medians0 = medians0 / amps0



#####   Figures   #####
plot_names = np.array([])

# Plot medians with changed phase
fig, (s1) = plt.subplots(1)

plot_phis = phis
plot_coherence = 0.3
plot_real_medians = data_dict[plot_coherence]['real_medians']
plot_imag_medians = data_dict[plot_coherence]['imag_medians']
plot_medians = data_dict[plot_coherence]['medians']

s1.plot(plot_phis, plot_real_medians, label='Median real csd ' + r'$\rho_{\Re(\langle x, y \rangle)}$')
s1.plot(plot_phis, plot_imag_medians, label='Median imaginary csd ' + r'$\rho_{\Im(\langle x, y \rangle)}$')
s1.plot(plot_phis, plot_medians, label='Median magnitude csd ' + r'$\rho_{\langle x, y \rangle}$')

# s1.set_xlim([2e1, 5e3])
# s1.set_ylim([1e-21, 1e-18])

# s1.set_title(f'LHO correlated noise - {csd_date} - {tt_num} averages')
s1.set_xlabel(f'Cross spectral density phase ' + r'$\phi$ [radians]')
s1.set_ylabel(f'Median ' + r'$\rho$')

s1.set_xticks(np.pi * np.array([0, 1/2, 1, 3/2, 2]))
s1.set_xticklabels(['0', '$\pi/2$', '$\pi$', '$3\pi/2$', '$2\pi$'])

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.7)
s1.legend()

plot_name = f'medians_vs_csd_phase.pdf'
full_plot_name = f'{fig_dir}/{plot_name}'
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.tight_layout()
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Plot normalized medians
fig, (s1) = plt.subplots(1)

plot_phis = phis
for coherence in coherence_list:
    normalized_medians = data_dict[coherence]['normalized_medians']
    s1.plot(plot_phis, normalized_medians, label=r'$\gamma^2 = $' + f' {coherence:.1f}')

# s1.set_xlim([2e1, 5e3])
# s1.set_ylim([1e-21, 1e-18])

# s1.set_title(f'LHO correlated noise - {csd_date} - {tt_num} averages')
s1.set_xlabel(f'Cross spectral density phase ' + r'$\phi$ [radians]')
s1.set_ylabel(f'Normalized median ' + r'$\rho(\phi)/\rho(\phi = 0)$')

s1.set_xticks(np.pi * np.array([0, 1/2, 1, 3/2, 2]))
s1.set_xticklabels(['0', '$\pi/2$', '$\pi$', '$3\pi/2$', '$2\pi$'])

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.7)
s1.legend()

plot_name = f'normalized_medians_vs_csd_phase.pdf'
full_plot_name = f'{fig_dir}/{plot_name}'
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.tight_layout()
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Plot minimum normalized medians with changing coherence
fig, (s1) = plt.subplots(1)

plot_phis = 180/np.pi * phis
s1.semilogx(coherences, normalized_medians0, 
    # label='Normalized median magnitude csd ' + r'$\rho_{\langle x, y \rangle}(\gamma^2, \phi=\pi/2) / \rho_{\langle x, y \rangle}(\gamma^2, \phi=0)$'
    )

# s1.set_xlim([2e1, 5e3])
# s1.set_ylim([1e-21, 1e-18])

# s1.set_title(f'LHO correlated noise - {csd_date} - {tt_num} averages')
s1.set_xlabel(f'Coherence ' + r'$\gamma^2$')
s1.set_ylabel(f'Normalized median ' + r'$\rho(\gamma^2, \phi=\pi/4) / \rho(\gamma^2, \phi=0)$')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.7)
# s1.legend()

plot_name = f'normalized_medians_vs_coherence.pdf'
full_plot_name = f'{fig_dir}/{plot_name}'
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.tight_layout()
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Plot normalized medians
s1 = plt.subplot(111, projection='polar')

plot_phis = phis
for coherence in coherence_list:
    normalized_medians = data_dict[coherence]['normalized_medians']
    s1.plot(plot_phis, normalized_medians, label=r'$\gamma^2 = $' + f' {coherence:.1f}', zorder=3)

# s1.set_xlim([2e1, 5e3])
# s1.set_ylim([1e-21, 1e-18])

# s1.set_title(f'LHO correlated noise - {csd_date} - {tt_num} averages')
# s1.set_xlabel(f'Cross spectral density phase ' + r'$\phi$ [radians]')
# s1.set_ylabel(f'Normalized median ' + r'$\rho(\phi)/\rho(\phi = 0)$')

# s1.set_xticks(np.pi * np.array([0, 1/2, 1, 3/2, 2]))
# s1.set_xticklabels(['0', '$\pi/2$', '$\pi$', '$3\pi/2$', '$2\pi$'])

rlim = [0.90, 1.0]
s1.set_rlim( rlim )
s1.set_rticks( np.linspace(rlim[0], rlim[-1], 5) )

# s1.grid()
# s1.grid(which='minor', ls='--', alpha=0.7)
s1.legend(loc='center left')

plot_name = f'normalized_medians_vs_csd_phase_radial.pdf'
full_plot_name = f'{fig_dir}/{plot_name}'
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.tight_layout()
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()