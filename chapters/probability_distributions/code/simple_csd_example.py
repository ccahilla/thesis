'''
simple_csd_example.py

Makes a simple white noise cross spectral density (CSD) calculation and plot.
Focus on difference between mean and median-averaging of CSDs and PSDs.
scipy version: 1.4.1

Craig Cahillane
September 16, 2020
'''

import os

import numpy as np
import scipy.signal as sig
from scipy.optimize import fsolve, curve_fit # need for csd median-to-mean bias calculation
from scipy.special import kn

import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
# plt.ion()

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 18, # 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 14,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])



#####   Functions   #####
def calc_all_csds(x, y, fs=1.0, window='hann', nperseg=None, noverlap=None, nfft=None,
        detrend='constant', return_onesided=True, scaling='density', axis=-1):
    '''
    Calculates the cross spectral density from data arrays x and y.
    Copied docstring from scipy.signal._spectral_helper() function.

    The windows are not averaged over; 
    the result from each window is returned.

    Parameters
    ---------
    x : array_like
        Array or sequence containing the data to be analyzed.
    y : array_like
        Array or sequence containing the data to be analyzed. 
    fs : float, optional
        Sampling frequency of the time series. Defaults to 1.0.
    window : str or tuple or array_like, optional
        Desired window to use. If `window` is a string or tuple, it is
        passed to `get_window` to generate the window values, which are
        DFT-even by default. See `get_window` for a list of windows and
        required parameters. If `window` is array_like it will be used
        directly as the window and its length must be nperseg. Defaults
        to a Hann window.
    nperseg : int, optional
        Length of each segment. Defaults to None, but if window is str or
        tuple, is set to 256, and if window is array_like, is set to the
        length of the window.
    noverlap : int, optional
        Number of points to overlap between segments. If `None`,
        ``noverlap = nperseg // 2``. Defaults to `None`.
    nfft : int, optional
        Length of the FFT used, if a zero padded FFT is desired. If
        `None`, the FFT length is `nperseg`. Defaults to `None`.
    detrend : str or function or `False`, optional
        Specifies how to detrend each segment. If `detrend` is a
        string, it is passed as the `type` argument to the `detrend`
        function. If it is a function, it takes a segment and returns a
        detrended segment. If `detrend` is `False`, no detrending is
        done. Defaults to 'constant'.
    return_onesided : bool, optional
        If `True`, return a one-sided spectrum for real data. If
        `False` return a two-sided spectrum. Defaults to `True`, but for
        complex data, a two-sided spectrum is always returned.
    scaling : { 'density', 'spectrum' }, optional
        Selects between computing the cross spectral density ('density')
        where `Pxy` has units of V**2/Hz and computing the cross
        spectrum ('spectrum') where `Pxy` has units of V**2, if `x`
        and `y` are measured in V and `fs` is measured in Hz.
        Defaults to 'density'
    axis : int, optional
        Axis along which the FFTs are computed; the default is over the
        last axis (i.e. ``axis=-1``).

    Returns
    -------
    freqs : ndarray
        Array of sample frequencies.
    tt : ndarray
        Array of times corresponding to each data segment
    Sxys : ndarray
        2D array of cross spectral densities for each window


    '''
    if np.all(x == y):
        same_data = True
    else:
        same_data = False

    # Sanitize nperseg input
    if nperseg is not None:  # if specified by user
        nperseg = int(nperseg)
        if nperseg < 1:
            raise ValueError('nperseg must be a positive integer')
    else:
        nperseg = x.shape[-1]

    # Sanitize nfft input
    if nfft is None:
        nfft = nperseg
    elif nfft < nperseg:
        raise ValueError('nfft must be greater than or equal to nperseg.')
    else:
        nfft = int(nfft)

    # Calculate the FFTs of each data stream, with flag mode='complex'
    # Fxx and Fyy are the short time Fourier transforms
    freqs, tt, Fxx = sig.spectrogram(x, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap,
                nfft=nfft, detrend=detrend, return_onesided=return_onesided,
                scaling=scaling, axis=axis, mode='complex')
    _, _, Fyy = sig.spectrogram(y, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap,
                nfft=nfft, detrend=detrend, return_onesided=return_onesided,
                scaling=scaling, axis=axis, mode='complex')

    Sxys = np.conjugate(Fxx) * Fyy
    if return_onesided:
        if nfft % 2:
            Sxys[..., 1:] *= 2 # double because we want a one-sided csd from the stfts
        else:
            # Last point is unpaired Nyquist freq point, don't double
            Sxys[..., 1:-1] *= 2

    if same_data: # return real PSD
        Sxys = np.abs(Sxys)

    return freqs, tt, Sxys

def get_median_psd(Sxxs, axis=-1):
    '''Calculate the median PSD from a bunch of PSDs.  
    No median-to-mean bias is applied.
    '''
    Sxx_med = np.median(np.real(Sxxs), axis=axis) 
    return Sxx_med

def get_median_csd(Sxys, axis=-1):
    '''Calculate the median CSD from a bunch of CSDs.  
    No median-to-mean bias is applied.
    '''
    Sxy_med = np.median(np.real(Sxys), axis=axis) + 1j * np.median(np.imag(Sxys), axis=axis)
    return Sxy_med

def get_mean_csd(Sxys, axis=-1):
    '''Calculate the mean CSD from a bunch of CSDs.
    '''
    Sxy = np.mean(Sxys, axis=axis)
    return Sxy

# Functions for calculating the CSD bias (plz ignore)
def median_coherence(power_ratio):
    '''Given a power_ratio of uncorrelated noise over correlated noise, 
    power_ratio = sigma_uncorr^2 / sigma_corr^2,
    returns the power coherence gamma^2 that would result from median-averaged PSDs and CSDs.
    '''
    med_coh =   (power_ratio**2 * np.log(1 + 1/np.sqrt(1 + power_ratio))**2) \
                / (4 * np.log(2)**2 * (1 + power_ratio) * (2 + power_ratio - 2 * np.sqrt(1 + power_ratio)) )
    return med_coh

def residual_median_coherence(input_array, median_coherence_desired):
    '''Finds the residual between the coherence desired and the numerically attempt from fsolve().
    For use as the function input to fsolve().
    Inputs:
    input_array = array of length one, required by fsolve to find the power ratio numerically
    median_coherence_desired = median coherence estimated from the CSD signals.
    Output:
    residual = array of length one with residual from median_coherence() function
    '''
    func_results = median_coherence(input_array[0])
    residual = [func_results - median_coherence_desired]
    return residual

def bias_from_median_coherence_and_power_ratio(median_coherence, power_ratio):
    '''Calculates the mean-to-median bias factor from the median_coherence = gamma^2
    and the power_ratio = epsilon.
    '''
    bias = np.log(2) * np.sqrt( (1 + power_ratio) * median_coherence )
    return bias

def bias_from_median_coherence(median_coherence_estimated, initial_power_ratio=0.1):
    '''Estimates the median/mean bias = b given some median_coherence = gamma^2.

    Numerically solves for the uncorrelated/correated power_ratio = epsilon,
    using the median_coherence.
    Uses scipy.optimize.fsolve() to find the root of residual_median_coherence().
    fsolve() seems to work for gamma^2 between (0.999 and 1e-6)

    This function requires bias_from_median_coherence_and_power_ratio(),
    residual_median_coherence(), median_coherence(), and fsolve() functions.

    Inputs:
    median_coherence_estimated  =   median coherence estimated from |<x,z>|^2/(<x,x> <z,z>) 
                                    where all spectral densities are median-averaged
    initial_power_ratio         =   initial guess of the power_ratio, default is 1.0
    Output:
    bias    =   median/mean bias factor.  Divide median-averaged cross spectral density <x,z> 
                by bias to recover the mean-avearged cross spectral density.             
    '''
    # Numerically estimate the power ratio epsilon from the median coherence
    fsolve_array = fsolve(  residual_median_coherence, 
                            [initial_power_ratio], 
                            args=(median_coherence_estimated))
    power_ratio = fsolve_array[0]

    # Find the bias factor
    bias = bias_from_median_coherence_and_power_ratio(  median_coherence_estimated, 
                                                        power_ratio)
    return bias

def mid_bins(bins):
    '''Returns the middle of bins given from the native mpl.hist() output'''
    diff_bins = np.diff(bins)
    middle_bins = bins[:-1] + diff_bins
    return middle_bins

def asymmetric_laplace(x, loc, scale, kappa):
    '''Defines an asymmetric laplace probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplacian
    scale   = scale parameter for broadening the PDF width
    kappa   = asymmetric parameter 
    Output:
    pdf     = asymmetric laplace probability distribution function on input x
    '''
    sign = np.sign(x - loc) # either +1 or -1 
    pdf = (scale / (kappa + 1/kappa)) * np.exp(-sign * (x - loc) * scale * kappa**sign)
    return pdf

def csd_2d_probability(cc, dd, sigma_aa, sigma_cc, phi0):
    '''Two dimentional cross spectral density probability distribution function
    p(c,d) = exp(cc/sigma_cc**2) K0(sqrt(cc**2 + dd**2)/(gamma * sigma_cc**2))/(2*pi*sigma_aa**2*sigma_cc**2)

    Input:
    cc  = real axis of csd
    dd  = imaginary axis of csd
    sigma_aa    = amplitude of correlated noise
    sigma_cc    = amplitude of uncorrelated noise
    phi0 = angle of csd
    '''
    true_coherence =  1 / (1 + sigma_cc**2/sigma_aa**2)
    amp_coh = np.sqrt(true_coherence) # just gamma
    # numer1 = np.exp( np.add.outer(cc * np.cos(phi0), dd * np.sin(phi0)) / sigma_cc**2 ) 
    numer1 = np.exp( cc / sigma_cc**2 )
    numer2 = kn(0, np.sqrt( np.add.outer(cc**2, dd**2) ) / (amp_coh * sigma_cc**2)) 
    denom = 2 * np.pi * sigma_aa**2 * sigma_cc**2
    pdf = numer1 * numer2 / denom
    return pdf

def get_contours(X, Y, H, levels=None):
    '''Code stolen from corner.py: https://github.com/dfm/corner.py/blob/main/src/corner/corner.py
    Calculates the contours from the 2d array H, with bin edges X and Y.
    The contours are calculated at significance levels, between 0 and 1.
    Inputs:
    X   = 1d array. x-axis bin edges
    Y   = 1d array. y-axis bin edges
    H   = 2d array. joint distribution we would like to characterize with contours
    levels  = 1d array.  If None, selects the 1, 2, and 3-sigma levels for 2d distributions.

    Outputs:
    X2  = 1d array of x-axis points
    Y2  = 1d array of y-axis points
    H2  = 2d array of the joint distribution we are characterizing
    V   = 1d array of values at which to plot the contour.  Corresponds to the densities requested with the levels variable.

    Usage:
    X2, Y2, H2, V = get_contours(X, Y, H, levels)
    
    '''
    if levels is None:
        one_sigma_level = 1 - np.exp(-0.5 * 1**2) # one sigma for 2d pdfs
        two_sigma_level = 1 - np.exp(-0.5 * 2**2) # two sigma for 2d pdfs
        three_sigma_level = 1 - np.exp(-0.5 * 3**2) # three sigma for 2d pdfs
        levels = np.array([one_sigma_level, two_sigma_level, three_sigma_level])

    Hflat = H.flatten()
    inds = np.argsort(Hflat)[::-1]
    Hflat = Hflat[inds]
    sm = np.cumsum(Hflat)
    sm /= sm[-1]
    V = np.empty(len(levels))
    for i, v0 in enumerate(levels):
        try:
            V[i] = Hflat[sm <= v0][-1]
        except IndexError:
            V[i] = Hflat[0]
    V.sort()
    m = np.diff(V) == 0
    if np.any(m) and not quiet:
        logging.warning("Too few points to create valid contours")
    while np.any(m):
        V[np.where(m)[0][0]] *= 1.0 - 1e-4
        m = np.diff(V) == 0
    V.sort()

    # Compute the bin centers.
    X1, Y1 = 0.5 * (X[1:] + X[:-1]), 0.5 * (Y[1:] + Y[:-1])

    # Extend the array for the sake of the contours at the plot edges.
    H2 = H.min() + np.zeros((H.shape[0] + 4, H.shape[1] + 4))
    H2[2:-2, 2:-2] = H
    H2[2:-2, 1] = H[:, 0]
    H2[2:-2, -2] = H[:, -1]
    H2[1, 2:-2] = H[0]
    H2[-2, 2:-2] = H[-1]
    H2[1, 1] = H[0, 0]
    H2[1, -2] = H[0, -1]
    H2[-2, 1] = H[-1, 0]
    H2[-2, -2] = H[-1, -1]
    X2 = np.concatenate(
        [
            X1[0] + np.array([-2, -1]) * np.diff(X1[:2]),
            X1,
            X1[-1] + np.array([1, 2]) * np.diff(X1[-2:]),
        ]
    )
    Y2 = np.concatenate(
        [
            Y1[0] + np.array([-2, -1]) * np.diff(Y1[:2]),
            Y1,
            Y1[-1] + np.array([1, 2]) * np.diff(Y1[-2:]),
        ]
    )
    return X2, Y2, H2, V


#####   Parameters   #####

averages = 100000

N = (averages + 1) * 1000       # number of samples
fs = 2**14                      # Hz, sampling frequency, samples/second
total_time = N / fs             # seconds, total time

nperseg = 2**10                 # number of samples in a single fft segment
noverlap = 0 #nperseg // 2      # 50% overlap

bandwidth = fs / nperseg
overlap = noverlap / nperseg

# averages = (total_time * bandwidth - 1)/(1 - overlap) + 1

print('\033[92m')   # turns terminal text green
print(f'CSD Parameters')
print(f'total samples N = {N}')
print(f'sampling frequency = {fs} Hz')
print()
print(f'total_time = {total_time} seconds')
print(f'bandwidth = {bandwidth} Hz')
print(f'overlap = {100 * overlap} %')
print(f'averages = {averages}')
print('\033[0m')    # turns terminal text back to normal

# power density in V**2/Hz.  Should show up in the power spectral density.
noise_power_density = 1e-3

# correlated noise power density in V**2/Hz.  Should be the limit the CSD hits
corr_noise_power_density = 7e-4 

# uncorrelated + correlated noise power density.  Should be limit the PSD hits.
total_noise_power_density = noise_power_density + corr_noise_power_density

noise_power = noise_power_density * fs / 2              # total power in the noise spectrum in V**2.  
                                                        # Equal to the variance of the gaussian noise.  
corr_noise_power = corr_noise_power_density * fs / 2

# coherence
coherence = corr_noise_power**2 / (noise_power + corr_noise_power)**2

# Gaussian noise standard deviation
noise_amplitude = np.sqrt(noise_power)
corr_noise_amplitude = np.sqrt(corr_noise_power)

print('\033[93m')
print(f'Signals')
print(f'noise_power_density             = {noise_power_density:.1e} V^2/Hz')
print(f'correlated_noise_power_density  = {corr_noise_power_density:.1e} V^2/Hz')
print(f'total_noise_power_density       = {total_noise_power_density:.1e} V^2/Hz')
print()
print(f'true coherence = {coherence}')
print('\033[0m')



#####   Signals   #####

a = np.random.normal(scale=noise_amplitude, size=N) # sqrt(noise_power) = sigma on guassian noise
b = np.random.normal(scale=noise_amplitude, size=N)
c = np.random.normal(scale=corr_noise_amplitude, size=N)

corr_noise = c
shift_index = 0 #2 
shift_c = np.hstack(( c[shift_index:], c[:shift_index] )) # Shift the correlated noise by (2 / fs) seconds so the csd phase is not zero

x = a + c
y = b + shift_c
z = c

# Calculate all PSDs correctly
ff, tt, Zxxs = calc_all_csds(x, x, fs, nperseg=nperseg, noverlap=noverlap)
Zxx = Zxxs.mean(axis=-1)
Zxx_med = get_median_psd(Zxxs) # no bias applied
# Zxx_med = Zxx_med / np.log(2) # np.log(2) median-to-mean bias applied

# Calculate all CSDs correctly
_, _, Zxys = calc_all_csds(x, y, fs, nperseg=nperseg, noverlap=noverlap)
Zxy = Zxys.mean(axis=-1)
Zxy_med = get_median_csd(Zxys) # no bias applied

_, _, Zxzs = calc_all_csds(x, z, fs, nperseg=nperseg, noverlap=noverlap)
Zxz = Zxzs.mean(axis=-1)
Zxz_med = get_median_csd(Zxzs) # no bias applied

ff_index = len(ff)//2 - 200
Zxzs_mid = Zxzs[ff_index, :]

Zxzs_mid_real = np.real(Zxzs_mid)
Zxzs_mid_imag = np.imag(Zxzs_mid)

Zxzs_mid_real_mean = np.mean(Zxzs_mid_real)
Zxzs_mid_imag_mean = np.mean(Zxzs_mid_imag)

Zxzs_mid_real_median = np.median(Zxzs_mid_real)
Zxzs_mid_imag_median = np.median(Zxzs_mid_imag)

# Calculate coherence (works because Zxx ~ Zyy)
mean_cohs_xy = (np.abs(Zxy)/(Zxx))**2
median_cohs_xy = (np.abs(Zxy_med)/(Zxx_med) )**2

mean_coh_xy = np.mean( mean_cohs_xy[1:-1] )
median_coh_xy = np.mean( median_cohs_xy[1:-1] )

# Calculate CSD bias from median-averaged coherences
csd_bias = bias_from_median_coherence(median_coh_xy)

print('\033[91m')
print(f'mean_averaged_coherence_xy   = {mean_coh_xy} V^2/Hz')
print(f'median_averaged_coherence_xy = {median_coh_xy} V^2/Hz')
print()
print(f'CSD median/mean bias = {csd_bias}')
print('\033[0m')

# Calculate mean PSD via scipy (is correct)
_, Sxx = sig.welch(x, fs, nperseg=nperseg, noverlap=noverlap)

# Calculate median PSD via scipy (is correct, bias applied automatically)
_, Sxx_med = sig.welch(x, fs, nperseg=nperseg, noverlap=noverlap, average='median')
Sxx_med = Sxx_med * np.log(2) # Undo the PSD bias so we can compare methods directly

# Calculate mean CSD via scipy (is correct)
_, Sxy = sig.csd(x, y, fs, nperseg=nperseg, noverlap=noverlap)

# Calculate mean CSD via scipy (is incorrect, and PSD bias is applied automatically even though is in incorrect in general)
_, Sxy_med = sig.csd(x, y, fs, nperseg=nperseg, noverlap=noverlap, average='median')
Sxy_med = Sxy_med * np.log(2) # Undo the PSD bias so we can compare methods directly



#####   Figures   #####
plt.ioff()

# CSD mag and PSD comparison
fig, (s1, s2) = plt.subplots(1, 2)

s1.semilogy(ff, np.abs(Zxy), alpha=0.5, label=r'Mean-averaged CSD $\langle x, y \rangle$')
s1.semilogy(ff, np.abs(Zxy_med), alpha=0.5, label=r'Median-averaged CSD $\langle x, y \rangle$')

s1.semilogy(ff, np.abs(Zxx), alpha=0.5, label=r'Mean-averaged PSD $\langle x, x \rangle$')
s1.semilogy(ff, np.abs(Zxx_med), alpha=0.5, label=r'Median-averaged PSD $\langle x, x \rangle$')

s2.semilogy(ff, np.abs(Sxy), alpha=0.5, label=r'Mean-averaged CSD $\langle x, y \rangle$ (scipy)')
s2.semilogy(ff, np.abs(Sxy_med), alpha=0.5, label=r'Median-averaged CSD $\langle x, y \rangle$ (scipy)')

s2.semilogy(ff, np.abs(Sxx), alpha=0.5, label=r'Mean-averaged PSD $\langle x, x \rangle$ (scipy)')
s2.semilogy(ff, np.abs(Sxx_med), alpha=0.5, label=r'Median-averaged PSD $\langle x, x \rangle$ (scipy)')

s1.semilogy(ff, total_noise_power_density * np.ones_like(ff), ls='--', alpha=0.5, color='black', 
            label=f'Total noise = {total_noise_power_density:.1e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')
s1.semilogy(ff, noise_power_density * np.ones_like(ff), ls='-', alpha=0.5, color='black', 
            label=f'Uncorrelated noise = {noise_power_density:.0e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')
s1.semilogy(ff, corr_noise_power_density * np.ones_like(ff), ls=':', alpha=0.5, color='black', 
            label=f'Correlated noise = {corr_noise_power_density:.0e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')

s2.semilogy(ff, total_noise_power_density * np.ones_like(ff), ls='--', alpha=0.5, color='black', 
            label=f'Total noise = {total_noise_power_density:.1e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')
s2.semilogy(ff, noise_power_density * np.ones_like(ff), ls='-', alpha=0.5, color='black', 
            label=f'Uncorrelated noise = {noise_power_density:.0e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')
s2.semilogy(ff, corr_noise_power_density * np.ones_like(ff), ls=':', alpha=0.5, color='black', 
            label=f'Correlated noise = {corr_noise_power_density:.0e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')

s1.set_title(f'PSDs and CSDs (craig version)')
s2.set_title(f'PSDs and CSDs (scipy version)')
s1.set_ylabel(r'PSD CSD power [$\mathrm{V}^2/\mathrm{Hz}$]')
s1.set_xlabel('Frequency [Hz]')
s2.set_xlabel('Frequency [Hz]')

s1.set_xlim([ff[1], ff[-1]])
s1.set_ylim([1e-4, 3e-3])
s2.set_xlim([ff[1], ff[-1]])
s2.set_ylim([1e-4, 3e-3])

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)
s2.grid()
s2.grid(which='minor', ls='--', alpha=0.5)

s1.legend(loc='lower left', ncol=1)
s2.legend(loc='lower left', ncol=1)

# plt.tight_layout()
# plt.show()

plot_name = f'scipy_psd_and_csds_mean_vs_median_averaging_averages.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
# make_interactive_svg(fig, full_plot_name.split('.pdf')[0])
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# CSD 2D histograms
fig, ss = plt.subplots(2, 2, sharex="col", sharey="row", figsize=(9, 9),
                        gridspec_kw=dict(   height_ratios=[1, 3],
                                            width_ratios=[3, 1])
                        )
s1 = ss[0, 0]   # top histogram
s2 = ss[0, 1]   # delete
s3 = ss[1, 0]   # 2d hist
s4 = ss[1, 1]   # right histogram

fig.delaxes(s2) # delete

num_bins = 400
low_bin = -0.01
high_bin = 0.01
bins = np.linspace(low_bin, high_bin, num_bins+1)
xx = np.linspace(low_bin, high_bin, 1000)

reNN, rebins, repatches = s1.hist(Zxzs_mid_real, bins=bins, histtype='step', lw=2, zorder=3, density=True,
                                    label=r'$\Re{(\langle x, z \rangle)}$'+' samples')  
imNN, imbins, impatches = s4.hist(Zxzs_mid_imag, bins=bins, histtype='step', lw=2, zorder=3, density=True, orientation='horizontal',
                                    label=r'$\Im{(\langle x, z \rangle)}$'+' samples')  
H, X, Y, image = s3.hist2d(Zxzs_mid_real, Zxzs_mid_imag, bins=(rebins, imbins), density=True, 
                                    cmap=mpl.cm.copper, norm=mpl.colors.LogNorm())

s1.axvline(Zxzs_mid_real_mean, ymin=0, ymax=np.max(reNN)*1.2, color='C2', label=f'mean = {Zxzs_mid_real_mean:.2e}')
s1.axvline(Zxzs_mid_real_median, ymin=0, ymax=np.max(reNN)*1.2, ls='--', color='C3', label=f'median = {Zxzs_mid_real_median:.2e}')

s4.axhline(Zxzs_mid_imag_mean, xmin=0, xmax=np.max(imNN)*1.2, color='C2', label=f'mean = {Zxzs_mid_imag_mean:.2e}')
s4.axhline(Zxzs_mid_imag_median, xmin=0, xmax=np.max(imNN)*1.2, ls='--', color='C3', label=f'median = {Zxzs_mid_imag_median:.2e}')

# Model fits
p0 = [0, 250, 1]
middle_bins = mid_bins(bins)
laplace_xy_real_fit_params, laplace_xy_real_fit_cov  = curve_fit(asymmetric_laplace, middle_bins, reNN, p0=p0)
laplace_xy_imag_fit_params, laplace_xy_imag_fit_cov  = curve_fit(asymmetric_laplace, middle_bins, imNN, p0=p0)

laplace_xy_real_pdf_fit = asymmetric_laplace(xx, *laplace_xy_real_fit_params)
laplace_xy_imag_pdf_fit = asymmetric_laplace(xx, *laplace_xy_imag_fit_params)

s1.plot(xx, laplace_xy_real_pdf_fit, color='k',  lw=4, label=r'$f_{\Re(\langle x, z \rangle)}$' + ' asymmetric Laplace')
s4.plot(laplace_xy_imag_pdf_fit, xx, color='k',  lw=4, label=r'$f_{\Im(\langle x, z \rangle)}$' + ' Laplace')

# 2D model
phi0 = 0.0
csd_pdf = csd_2d_probability(xx, xx, np.sqrt(0.5 * corr_noise_power_density), np.sqrt(0.5 * noise_power_density), phi0=phi0)

# c1 = s3.contour(xx, xx, np.log(csd_pdf), levels=np.log(np.array([1e3, 3e3, 1e4, 3e4, 1e5])), cmap='spring')
# c1.collections[0].set_label(r'$f_{\mathcal{U},\mathcal{V}}(u, v)$' + ' contours')

# Create contours
levels = np.array([1-np.exp(-0.5*1**2), 1-np.exp(-0.5*2**2)])
X2, Y2, H2, V = get_contours( X, Y, H, levels=levels)

e1 = s3.contour(X2, Y2, np.log(H2.T), np.log(V), cmap='autumn')

# Compute the density levels.
H = csd_pdf
X = np.linspace(low_bin, high_bin, 1000+1)
Y = np.linspace(low_bin, high_bin, 1000+1)

X2, Y2, H2, V = get_contours(X, Y, H)

d1 = s3.contour(X2, Y2, np.log(H2), np.log(V), linewidths=1.5, cmap='winter')

# Arrow
a0 = s3.arrow(0, 0, Zxzs_mid_real_mean, 0, color='C2', length_includes_head=True, width=5e-5, head_width=2e-4, head_length=2e-4, zorder=2)

# Form the legend handles and labels combo
handles = np.array([])
labels = np.array([])
for ii, contour in enumerate(e1.collections[::-1]):
    handles = np.append(handles, contour)
    labels = np.append(labels, f'2d histogram {ii+1}' + r'$\sigma$ contour')

for ii, contour in enumerate(d1.collections[::-1]):
    handles = np.append(handles, contour)
    labels = np.append(labels, r'$f_{\langle x, z \rangle}$ '+f'{ii+1}' + r'$\sigma$ contour')

handles = np.append(handles, a0)
labels = np.append(labels, 'mean vector')

s1.set_xlim([-0.003, 0.007])
s3.set_xlim([-0.003, 0.007])

s3.set_ylim([-0.005, 0.005])
s4.set_ylim([-0.005, 0.005])

s3.set_xlabel(r'$\Re{(\langle x, z \rangle)}$ samples [$\mathrm{V^2}/\mathrm{Hz}$]')
s3.set_ylabel(r'$\Im{(\langle x, z \rangle)}$ samples [$\mathrm{V^2}/\mathrm{Hz}$]')

s1.legend(fontsize=12, loc='upper right')
s3.legend(handles, labels, fontsize=12, loc='upper left')
s4.legend(fontsize=12, loc='upper right')

s1.grid()
s3.grid()
s4.grid()

plot_name = f'csd_2d_histogram.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()

# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()