'''
coherence_comparison.py

Compares the coherence one might expect if using mean-averaging versus median-averaging.

Craig Cahillane
June 25, 2020
'''

import os
import sys
import time
import copy
import numpy as np
# import scipy.signal as sig
# import scipy.fft as fft

from scipy.stats import expon, rayleigh, norm, chi2
from scipy.special import kn
from scipy.optimize import curve_fit
from scipy.optimize import fsolve

import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 18, # 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Functions   #####
def median_coherence(power_ratio):
    '''Given a power_ratio of uncorrelated noise over correlated noise, 
    power_ratio = sigma_uncorr^2 / sigma_corr^2,
    returns the power coherence gamma^2 that would result from median-averaged PSDs and CSDs.
    '''
    med_coh =   (power_ratio**2 * np.log(1 + 1/np.sqrt(1 + power_ratio))**2) \
                / (4 * np.log(2)**2 * (1 + power_ratio) * (2 + power_ratio - 2 * np.sqrt(1 + power_ratio)) )
    return med_coh

def residual_median_coherence(input_array, median_coherence_desired):
    '''Finds the residual between the coherence desired and the numerically attempt from fsolve().
    For use as the function input to fsolve().
    Inputs:
    input_array = array of length one, required by fsolve to find the power ratio numerically
    median_coherence_desired = median coherence estimated from the CSD signals.
    Output:
    residual = array of length one with residual from median_coherence() function
    '''
    func_results = median_coherence(input_array[0])
    residual = [func_results - median_coherence_desired]
    return residual

def bias_from_median_coherence_and_power_ratio(median_coherence, power_ratio):
    '''Calculates the mean-to-median bias factor from the median_coherence = gamma^2
    and the power_ratio = epsilon.
    '''
    bias = np.log(2) * np.sqrt( (1 + power_ratio) * median_coherence )
    return bias

def bias_from_median_coherence(median_coherence_estimated, initial_power_ratio=0.1):
    '''Estimates the median/mean bias = b given some median_coherence = gamma^2.

    Numerically solves for the uncorrelated/correated power_ratio = epsilon,
    using the median_coherence.
    Uses scipy.optimize.fsolve() to find the root of residual_median_coherence().
    fsolve() seems to work for gamma^2 between (0.999 and 1e-6)

    This function requires bias_from_median_coherence_and_power_ratio(),
    residual_median_coherence(), median_coherence(), and fsolve() functions.

    Inputs:
    median_coherence_estimated  =   median coherence estimated from |<x,z>|^2/(<x,x> <z,z>) 
                                    where all spectral densities are median-averaged
    initial_power_ratio         =   initial guess of the power_ratio, default is 1.0
    Output:
    bias    =   median/mean bias factor.  Divide median-averaged cross spectral density <x,z> 
                by bias to recover the mean-avearged cross spectral density.             
    '''
    # Numerically estimate the power ratio epsilon from the median coherence
    fsolve_array = fsolve(  residual_median_coherence, 
                            [initial_power_ratio], 
                            args=(median_coherence_estimated))
    power_ratio = fsolve_array[0]

    # Find the bias factor
    bias = bias_from_median_coherence_and_power_ratio(  median_coherence_estimated, 
                                                        power_ratio)
    return bias

def biases_from_coherences(coherences, initial_power_ratios=None):
    '''Returns array of biases from an array of coherences.
    Inputs:
    coherences =    median-averaged coherences 
    initial_power_ratios =  power ratios to start estimation with.  
                            If None, uses 0.1 for all.  Default is None.
    Outputs:
    biases =    array of biases calculated from the coherences
    '''
    if initial_power_ratios is None:
        initial_power_ratios = 0.1 * np.ones_like(coherences)
    biases = np.array([])
    for coherence, initial_power_ratio in zip(coherences, initial_power_ratios):
        bias = bias_from_median_coherence(coherence, initial_power_ratio=initial_power_ratio)
        biases = np.append(biases, bias)
    return biases

def mean_coherence(power_ratio):
    '''Given a power_ratio of uncorrelated noise over correlated noise, 
    ratio = sigma_uncorr^2 / sigma_corr^2,
    returns the usual power coherence one might expect from a mean-averaged PSDs and CSDs.
    '''
    mean_coh = 1/(1 + power_ratio)
    return mean_coh

def median_coherence_approx(power_ratio):
    '''Given a power_ratio of uncorrelated noise over correlated noise, 
    ratio = sigma_uncorr^2 / sigma_corr^2,
    returns the approximation for median low-coherence signals.
    '''
    med_coh_approx = 1/(4 * power_ratio * np.log(2)**2)
    return med_coh_approx

def bias_small_kappa(power_ratio):
    '''Calculates the CSD median/mean bias factor, for kappa < 1
    given the power_ratio = uncorrelated_power/correlated_power.
    '''
    bias = - ( power_ratio * np.log((1 + power_ratio - np.sqrt(1 + power_ratio))/power_ratio) )/( 2 * (-1 + np.sqrt(1 + power_ratio)) )
    return bias

def bias_large_kappa(power_ratio):
    '''Calculates the CSD median/mean bias factor, for kappa > 1
    given the power_ratio = uncorrelated_power/correlated_power.
    This is not physically relevant.
    '''
    bias = (1/2) * (-1 + np.sqrt(1 + power_ratio)) * np.log((1 + power_ratio + np.sqrt(1 + power_ratio))/power_ratio) 
    return bias


#####   Preprocessing   #####
lower_power_ratio = 1e-2
upper_power_ratio = 1e4
power_ratios = np.logspace(np.log10(lower_power_ratio), np.log10(upper_power_ratio), 1001)
power_ratios_approx = np.logspace(np.log10(0.5), np.log10(upper_power_ratio), 501)

med_coh = median_coherence(power_ratios)
mean_coh = mean_coherence(power_ratios)
med_coh_approx = median_coherence_approx(power_ratios_approx)

biases = bias_small_kappa(power_ratios)
biases_large_kappa = bias_large_kappa(power_ratios)

#####   Figures   #####
fig, (s1) = plt.subplots(1)

s1.loglog(power_ratios, mean_coh, label='Mean coherence ' + \
    r'$\overline{ \gamma^2_{xz} } = \frac{1}{1 + \epsilon}$')
s1.loglog(power_ratios, med_coh, label='Median coherence ' + \
    r'$\widetilde{ \gamma^2_{xz} } = $'+\
    r'$\frac{\epsilon ^2 \log ^2\left(\frac{1}{\sqrt{\epsilon +1}}+1\right)}{4 (\epsilon +1) \left(\epsilon -2 \sqrt{\epsilon +1}+2\right) \log ^2(2)}$')
s1.loglog(power_ratios_approx, med_coh_approx, ls='--', label='Median coherence approx ' + \
    r'$\widetilde{ \gamma^2_{xz} } \approx \frac{1}{4 \epsilon \log^2(2)}$')

s1.set_xlabel(r'Power ratio $\epsilon = \sigma_c^2 / \sigma_a^2$')
s1.set_ylabel(r'Coherence $\gamma^2$')

s1.set_xlim([ lower_power_ratio, upper_power_ratio ])
s1.set_ylim([min(med_coh_approx), 1])

s1.legend()
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)

plot_name = f'coherence_comparison.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Median/Mean CSD bias factor 
fig, (s1) = plt.subplots(1)

s1.semilogx(power_ratios, biases, label='CSD bias for ' + r'$\kappa < 1$' + ': ' + \
    r'$b = \frac{\epsilon  \log \left(\frac{\epsilon -\sqrt{\epsilon +1}+1}{\epsilon }\right)}{2 \left(1-\sqrt{\epsilon +1}\right)}$')

# s1.semilogx(power_ratios, biases_large_kappa, label='CSD bias for ' + r'$\kappa > 1$' + ': ' + \
#     r'$b = \frac{1}{2} \left(\sqrt{\epsilon +1}-1\right) \log \left(\frac{\epsilon +\sqrt{\epsilon +1}+1}{\epsilon }\right)$')

s1.semilogx([lower_power_ratio, upper_power_ratio], [np.log(2), np.log(2)], ls='--', label=r'$\log(2)$')
s1.semilogx([lower_power_ratio, upper_power_ratio], [0.5, 0.5], ls='--', label=r'$1/2$')

s1.set_xlabel(r'Power ratio $\epsilon = \sigma_c^2 / \sigma_a^2$')
s1.set_ylabel(r'CSD Bias Factor $b = \rho/\mu$')

s1.set_xlim([ lower_power_ratio, upper_power_ratio ])
# s1.set_ylim([min(med_coh_approx), 1])

s1.legend(loc='best')
s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)

plot_name = f'bias_factor.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()