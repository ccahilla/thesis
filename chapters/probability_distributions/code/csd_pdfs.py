'''
test_csd_calcs.py
Tests the scipy 1.4.1 cross-spectral density calucations.

Craig Cahillane
4/20/2020 ;)
'''

import os
import sys
import time
import copy
import numpy as np
import scipy.signal as sig
import scipy.fft as fft

from scipy.stats import expon, rayleigh, norm, laplace
from scipy.optimize import fsolve, curve_fit
from scipy.special import kn

import matplotlib as mpl
import matplotlib.pyplot as plt

from nds2utils.make_interactive_svg import make_interactive_svg

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 18, # 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 14,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Functions   #####

def mid_bins(bins):
    '''Returns the middle of bins given from the native mpl.hist() output'''
    diff_bins = np.diff(bins)
    middle_bins = bins[:-1] + diff_bins
    return middle_bins

def mean_squared_error(xx, yy, stats_rv, params):
    '''Calculate the mean squared error from measurement (xx, yy)
    and model (scipy.stats pdf function + fit params)
    Inputs:
    xx = x-axis of the measured histogram (use mid_bins())
    yy = y-axis of the measured histogram
    stats_rv = scipy.stats random variable class (like scipy.stats.norm)
    params = parameters of the fit to the stats_rv
    Output:
    mse = mean squared error of our fit
    '''
    error = yy - stats_rv.pdf(xx, loc=params[0], scale=params[1])
    mse = np.mean(error**2)
    return mse

def asymmetric_laplace(x, loc, scale, kappa):
    '''Defines an asymmetric laplace probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplacian
    scale   = scale parameter for broadening the PDF width
    kappa   = asymmetric parameter 
    Output:
    pdf     = asymmetric laplace probability distribution function on input x
    '''
    sign = np.sign(x - loc) # either +1 or -1 
    pdf = (scale / (kappa + 1/kappa)) * np.exp(-sign * (x - loc) * scale * kappa**sign)
    return pdf

def calc_all_csds(x, y, fs=1.0, window='hann', nperseg=None, noverlap=None, nfft=None,
        detrend='constant', return_onesided=True, scaling='density', axis=-1):
    '''
    Calculates the cross spectral density from data arrays x and y.
    Copied docstring from scipy.signal._spectral_helper() function.

    The windows are not averaged over; 
    the result from each window is returned.

    Parameters
    ---------
    x : array_like
        Array or sequence containing the data to be analyzed.
    y : array_like
        Array or sequence containing the data to be analyzed. 
    fs : float, optional
        Sampling frequency of the time series. Defaults to 1.0.
    window : str or tuple or array_like, optional
        Desired window to use. If `window` is a string or tuple, it is
        passed to `get_window` to generate the window values, which are
        DFT-even by default. See `get_window` for a list of windows and
        required parameters. If `window` is array_like it will be used
        directly as the window and its length must be nperseg. Defaults
        to a Hann window.
    nperseg : int, optional
        Length of each segment. Defaults to None, but if window is str or
        tuple, is set to 256, and if window is array_like, is set to the
        length of the window.
    noverlap : int, optional
        Number of points to overlap between segments. If `None`,
        ``noverlap = nperseg // 2``. Defaults to `None`.
    nfft : int, optional
        Length of the FFT used, if a zero padded FFT is desired. If
        `None`, the FFT length is `nperseg`. Defaults to `None`.
    detrend : str or function or `False`, optional
        Specifies how to detrend each segment. If `detrend` is a
        string, it is passed as the `type` argument to the `detrend`
        function. If it is a function, it takes a segment and returns a
        detrended segment. If `detrend` is `False`, no detrending is
        done. Defaults to 'constant'.
    return_onesided : bool, optional
        If `True`, return a one-sided spectrum for real data. If
        `False` return a two-sided spectrum. Defaults to `True`, but for
        complex data, a two-sided spectrum is always returned.
    scaling : { 'density', 'spectrum' }, optional
        Selects between computing the cross spectral density ('density')
        where `Pxy` has units of V**2/Hz and computing the cross
        spectrum ('spectrum') where `Pxy` has units of V**2, if `x`
        and `y` are measured in V and `fs` is measured in Hz.
        Defaults to 'density'
    axis : int, optional
        Axis along which the FFTs are computed; the default is over the
        last axis (i.e. ``axis=-1``).

    Returns
    -------
    freqs : ndarray
        Array of sample frequencies.
    tt : ndarray
        Array of times corresponding to each data segment
    Sxys : ndarray
        2D array of cross spectral densities for each window


    '''
    # Sanitize nperseg input
    if nperseg is not None:  # if specified by user
        nperseg = int(nperseg)
        if nperseg < 1:
            raise ValueError('nperseg must be a positive integer')
    else:
        nperseg = x.shape[-1]

    # Sanitize nfft input
    if nfft is None:
        nfft = nperseg
    elif nfft < nperseg:
        raise ValueError('nfft must be greater than or equal to nperseg.')
    else:
        nfft = int(nfft)

    # Calculate the FFTs of each data stream, with flag mode='complex'
    # Fxx and Fyy are the short time Fourier transforms
    freqs, tt, Fxx = sig.spectrogram(x, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap,
                nfft=nfft, detrend=detrend, return_onesided=return_onesided,
                scaling=scaling, axis=axis, mode='complex')
    _, _, Fyy = sig.spectrogram(y, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap,
                nfft=nfft, detrend=detrend, return_onesided=return_onesided,
                scaling=scaling, axis=axis, mode='complex')

    Sxys = np.conjugate(Fxx) * Fyy
    if return_onesided:
        if nfft % 2:
            Sxys[..., 1:] *= 2 # double because we want a one-sided csd from the stfts
        else:
            # Last point is unpaired Nyquist freq point, don't double
            Sxys[..., 1:-1] *= 2

    return freqs, tt, Sxys

def get_median_csd(Sxys):
    '''Calculate the median CSD from a bunch of CSDs.  No bias is applied.
    '''
    Sab_med = np.median(np.real(Sxys), axis=1) + 1j * np.median(np.imag(Sxys), axis=1)
    return Sab_med

def get_phase_corrected_median_csd(Sxys, phases):
    '''Calculate the median CSD from a bunch of CSDs.  
    The phases represent the phase of the median CSD at each frequency in radians,
    calculated previously using get_median_csd().
    The phase will be used to rotate each cross spectral density so phi = 0, 
    then the median-averaging will be done,
    then the phase rotation undone to each the old phase.
    '''
    Sxys = Sxys * np.exp(-1j * phases)[:,np.newaxis]
    Sab_med = np.median(np.real(Sxys), axis=1) + 1j * np.median(np.imag(Sxys), axis=1)
    Sab_med = Sab_med * np.exp(1j * phases)
    return Sab_med

def csd_2d_probability(cc, dd, lambda0, coherence, phi0):
    '''Two dimentional cross spectral density probability distribution function
    p(c,d) = exp(cc/sigma_cc**2) K0(sqrt(cc**2 + dd**2)/(gamma * sigma_cc**2))/(2*pi*sigma_aa**2*sigma_cc**2)

    Input:
    cc  = real axis of csd
    dd  = imaginary axis of csd
    lambda0     = amplitude of uncorrelated noise
    coherence   = ratio of csd^2/(psd1 * psd2)
    phi0 = angle of csd
    '''
    amp_coh = np.sqrt(coherence)
    exp_coeff = amp_coh / (lambda0 * np.sqrt(1 - coherence))
    bessel_coeff = 1/(lambda0 * np.sqrt(1 - coherence))

    numer1 = np.exp( np.add.outer(cc * np.cos(phi0), dd * np.sin(phi0)) * exp_coeff )
    numer2 = kn(0, np.sqrt( np.add.outer(cc**2, dd**2) ) * bessel_coeff)
    denom = 2 * np.pi * lambda0**2
    pdf = numer1 * numer2 / denom
    return pdf

def get_contours(X, Y, H, levels=None):
    '''Code stolen from corner.py: https://github.com/dfm/corner.py/blob/main/src/corner/corner.py
    Calculates the contours from the 2d array H, with bin edges X and Y.
    The contours are calculated at significance levels, between 0 and 1.
    Inputs:
    X   = 1d array. x-axis bin edges
    Y   = 1d array. y-axis bin edges
    H   = 2d array. joint distribution we would like to characterize with contours
    levels  = 1d array.  If None, selects the 1, 2, and 3-sigma levels for 2d distributions.

    Outputs:
    X2  = 1d array of x-axis points
    Y2  = 1d array of y-axis points
    H2  = 2d array of the joint distribution we are characterizing
    V   = 1d array of values at which to plot the contour.  Corresponds to the densities requested with the levels variable.

    Usage:
    X2, Y2, H2, V = get_contours(X, Y, H, levels)
    
    '''
    if levels is None:
        one_sigma_level = 1 - np.exp(-0.5 * 1**2) # one sigma for 2d pdfs
        two_sigma_level = 1 - np.exp(-0.5 * 2**2) # two sigma for 2d pdfs
        three_sigma_level = 1 - np.exp(-0.5 * 3**2) # three sigma for 2d pdfs
        levels = np.array([one_sigma_level, two_sigma_level, three_sigma_level])

    Hflat = H.flatten()
    inds = np.argsort(Hflat)[::-1]
    Hflat = Hflat[inds]
    sm = np.cumsum(Hflat)
    sm /= sm[-1]
    V = np.empty(len(levels))
    for i, v0 in enumerate(levels):
        try:
            V[i] = Hflat[sm <= v0][-1]
        except IndexError:
            V[i] = Hflat[0]
    V.sort()
    m = np.diff(V) == 0
    if np.any(m) and not quiet:
        logging.warning("Too few points to create valid contours")
    while np.any(m):
        V[np.where(m)[0][0]] *= 1.0 - 1e-4
        m = np.diff(V) == 0
    V.sort()

    # Compute the bin centers.
    X1, Y1 = 0.5 * (X[1:] + X[:-1]), 0.5 * (Y[1:] + Y[:-1])

    # Extend the array for the sake of the contours at the plot edges.
    H2 = H.min() + np.zeros((H.shape[0] + 4, H.shape[1] + 4))
    H2[2:-2, 2:-2] = H
    H2[2:-2, 1] = H[:, 0]
    H2[2:-2, -2] = H[:, -1]
    H2[1, 2:-2] = H[0]
    H2[-2, 2:-2] = H[-1]
    H2[1, 1] = H[0, 0]
    H2[1, -2] = H[0, -1]
    H2[-2, 1] = H[-1, 0]
    H2[-2, -2] = H[-1, -1]
    X2 = np.concatenate(
        [
            X1[0] + np.array([-2, -1]) * np.diff(X1[:2]),
            X1,
            X1[-1] + np.array([1, 2]) * np.diff(X1[-2:]),
        ]
    )
    Y2 = np.concatenate(
        [
            Y1[0] + np.array([-2, -1]) * np.diff(Y1[:2]),
            Y1,
            Y1[-1] + np.array([1, 2]) * np.diff(Y1[-2:]),
        ]
    )
    return X2, Y2, H2, V

def median_coherence(power_ratio):
    '''Given a power_ratio of uncorrelated noise over correlated noise, 
    power_ratio = sigma_uncorr^2 / sigma_corr^2,
    returns the power coherence gamma^2 that would result from median-averaged PSDs and CSDs.
    '''
    med_coh =   (power_ratio**2 * np.log(1 + 1/np.sqrt(1 + power_ratio))**2) \
                / (4 * np.log(2)**2 * (1 + power_ratio) * (2 + power_ratio - 2 * np.sqrt(1 + power_ratio)) )
    return med_coh

def residual_median_coherence(input_array, median_coherence_desired):
    '''Finds the residual between the coherence desired and the numerically attempt from fsolve().
    For use as the function input to fsolve().
    Inputs:
    input_array = array of length one, required by fsolve to find the power ratio numerically
    median_coherence_desired = median coherence estimated from the CSD signals.
    Output:
    residual = array of length one with residual from median_coherence() function
    '''
    func_results = median_coherence(input_array[0])
    residual = [func_results - median_coherence_desired]
    return residual

def bias_from_median_coherence_and_power_ratio(median_coherence, power_ratio):
    '''Calculates the mean-to-median bias factor from the median_coherence = gamma^2
    and the power_ratio = epsilon.
    '''
    bias = np.log(2) * np.sqrt( (1 + power_ratio) * median_coherence )
    return bias

def bias_from_median_coherence(median_coherence_estimated, initial_power_ratio=0.1):
    '''Estimates the median/mean bias = b given some median_coherence = gamma^2.

    Numerically solves for the uncorrelated/correated power_ratio = epsilon,
    using the median_coherence.
    Uses scipy.optimize.fsolve() to find the root of residual_median_coherence().
    fsolve() seems to work for gamma^2 between (0.999 and 1e-6)

    This function requires bias_from_median_coherence_and_power_ratio(),
    residual_median_coherence(), median_coherence(), and fsolve() functions.

    Inputs:
    median_coherence_estimated  =   median coherence estimated from |<x,z>|^2/(<x,x> <z,z>) 
                                    where all spectral densities are median-averaged
    initial_power_ratio         =   initial guess of the power_ratio, default is 1.0
    Output:
    bias    =   median/mean bias factor.  Divide median-averaged cross spectral density <x,z> 
                by bias to recover the mean-avearged cross spectral density.             
    '''
    # Numerically estimate the power ratio epsilon from the median coherence
    fsolve_array = fsolve(  residual_median_coherence, 
                            [initial_power_ratio], 
                            args=(median_coherence_estimated))
    power_ratio = fsolve_array[0]

    # Find the bias factor
    bias = bias_from_median_coherence_and_power_ratio(  median_coherence_estimated, 
                                                        power_ratio)
    return bias

def biases_from_coherences(median_coherences, initial_power_ratios=None):
    '''Returns array of biases from an array of coherences.
    Inputs:
    median_coherences     = median-averaged coherence estimates
    initial_power_ratios  = power ratios to start estimation with.  
                            If None, uses 0.1 for all.  Default is None.
    Outputs:
    biases =    array of biases calculated from the coherences
    '''
    if initial_power_ratios is None:
        initial_power_ratios = 0.1 * np.ones_like(median_coherences)
    biases = np.array([])
    for coherence, initial_power_ratio in zip(median_coherences, initial_power_ratios):
        bias = bias_from_median_coherence(coherence, initial_power_ratio=initial_power_ratio)
        biases = np.append(biases, bias)
    return biases


#####   Figures Directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Main Loop   #####
averages_array = np.array([ 
                            # 100,
                            # 1000, 
                            # 10000, 
                            100000,
                            ])

for avg in averages_array:
    start_time = time.time()

    N = (avg + 1) * 1000            # number of samples
    fs = 1e4                        # Hz, sampling frequency, samples/second
    total_time = N / fs             # seconds, total time

    nperseg = 1000                  # number of samples in a single fft segment
    noverlap = 0 #nperseg // 2      # 50% overlap

    bandwidth = fs / nperseg
    overlap = noverlap / nperseg

    averages = (total_time * bandwidth - 1)/(1 - overlap) + 1

    print()
    print(f'total samples N = {N}')
    print(f'sampling frequency = {fs} Hz')
    print()
    print(f'total_time = {total_time} seconds')
    print(f'bandwidth = {bandwidth} Hz')
    print(f'overlap = {100 * overlap} %')
    print(f'averages = {averages}')

    noise_power_density = 1e-3      # power density in V**2/Hz.  Should show up in the power spectral density.
    corr_noise_power_density = 8e-4 # correlated noise power density in V**2/Hz.  
                                    # Should be the limit the CSD hits
    total_noise_power_density = noise_power_density + corr_noise_power_density

    noise_power = noise_power_density * fs / 2              # total power in the noise spectrum in V**2.  
                                                            # Equal to the variance of the gaussian noise.  
    corr_noise_power = corr_noise_power_density * fs / 2

    a = np.random.normal(scale=np.sqrt(noise_power), size=N) # sqrt(noise_power) = sigma on guassian noise
    b = np.random.normal(scale=np.sqrt(noise_power), size=N)
    c = np.random.normal(scale=np.sqrt(corr_noise_power), size=N)

    corr_noise = c
    shift_index = 2 #100
    shift_c = np.hstack(( c[shift_index:], c[:shift_index] )) 

    x = a + c
    y = b + shift_c

    # Return all FFTs directly
    ff, tt, zxxs = sig.spectral_helper(x, x, fs, nperseg=nperseg, noverlap=noverlap, mode='stft')
    ff, tt, zyys = sig.spectral_helper(y, y, fs, nperseg=nperseg, noverlap=noverlap, mode='stft')

    ff, tt, zaas = sig.spectral_helper(a, a, fs, nperseg=nperseg, noverlap=noverlap, mode='stft')
    # ff, tt, zbbs = sig.spectral_helper(b, b, fs, nperseg=nperseg, noverlap=noverlap, mode='stft')

    ff, tt, zccs = sig.spectral_helper(c, c, fs, nperseg=nperseg, noverlap=noverlap, mode='stft')

    zxx = zxxs.mean(axis=-1)
    zyy = zyys.mean(axis=-1)

    zaa = zxxs.mean(axis=-1)
    # zbb = zbbs.mean(axis=-1)

    zcc = zccs.mean(axis=-1) 

    # Calculate PSDs from FFTs
    Zxx = 2 * np.conjugate(zxx) * zxx
    Zyy = 2 * np.conjugate(zyy) * zyy

    # Calculate CSDs from FFTs
    Zxys = 2 * np.conjugate(zxxs) * zyys
    Zxy = Zxys.mean(axis=-1)

    ff_index = len(ff)//2 - 200
    Zxys_mid = Zxys[ff_index, :]

    Zxys_mid_real = np.real(Zxys_mid)
    Zxys_mid_imag = np.imag(Zxys_mid)

    Zxys_mid_real_mean = np.mean(Zxys_mid_real)
    Zxys_mid_real_median = np.median(Zxys_mid_real)
    Zxys_mid_real_rms = np.sqrt(np.mean(Zxys_mid_real**2))

    Zxys_mid_imag_mean = np.mean(Zxys_mid_imag)
    Zxys_mid_imag_median = np.median(Zxys_mid_imag)
    Zxys_mid_imag_rms = np.sqrt(np.mean(Zxys_mid_imag**2))

    # Use the function calc_all_csds()
    ff2, tt2, Zxys2 = calc_all_csds(x, y, fs, nperseg=nperseg, noverlap=noverlap)
    Zxy2 = Zxys2.mean(axis=-1)
    Zxy2_med = get_median_csd(Zxys2)

    # Use the function get_phase_corrected_median_csd()
    Zxy3_med = get_phase_corrected_median_csd(Zxys2, np.angle(Zxy2_med))

    # Return all PSDs directly: spectral_helper() output
    ff, tt, Sxxs = sig.spectral_helper(x, x, fs, nperseg=nperseg, noverlap=noverlap)
    Sxx = Sxxs.mean(axis=-1)
    Sxx_med = (np.median(np.real(Sxxs), axis=-1) + 1j * np.median(np.imag(Sxxs), axis=-1)) / sig.median_bias(Sxxs.shape[-1])

    # PSDs in one freq bin over time
    # ff_index = len(ff)//2 - 1
    Sxxs_mid = Sxxs[ff_index, :] # get array of Sxx values at a single frequency bin for every average taken
    Sxxs_mid_mean = np.mean(Sxxs_mid)
    Sxxs_mid_median = np.median(Sxxs_mid)
    Sxxs_mid_rms = np.sqrt(np.mean(Sxxs_mid**2))

    # ASDs over time
    Axxs_mid = np.sqrt(Sxxs_mid) # ASDs
    Axxs_mid_mean = np.mean(Axxs_mid)
    Axxs_mid_median = np.median(Axxs_mid)
    Axxs_mid_rms = np.sqrt(np.mean(Axxs_mid**2))

    # Return all PSDs directly: spectral_helper() output
    ff, tt, Saas = sig.spectral_helper(a, a, fs, nperseg=nperseg, noverlap=noverlap)
    Saa = Saas.mean(axis=-1)
    Saa_med = (np.median(np.real(Saas), axis=-1) + 1j * np.median(np.imag(Saas), axis=-1)) / sig.median_bias(Saas.shape[-1])

    # PSDs in one freq bin over time
    # ff_index = len(ff)//2 - 1
    Saas_mid = Saas[ff_index, :] # get array of Saa values at a single frequency bin for every average taken
    Saas_mid_mean = np.mean(Saas_mid)
    Saas_mid_median = np.median(Saas_mid)
    Saas_mid_rms = np.sqrt(np.mean(Saas_mid**2))

    # ASDs over time
    Aaas_mid = np.sqrt(Saas_mid) # ASDs
    Aaas_mid_mean = np.mean(Aaas_mid)
    Aaas_mid_median = np.median(Aaas_mid)
    Aaas_mid_rms = np.sqrt(np.mean(Aaas_mid**2))

    # Return all PSDs directly: spectral_helper() output
    ff, tt, Sccs = sig.spectral_helper(c, c, fs, nperseg=nperseg, noverlap=noverlap)
    Scc = Sccs.mean(axis=-1)
    Scc_med = (np.median(np.real(Sccs), axis=-1) + 1j * np.median(np.imag(Sccs), axis=-1)) / sig.median_bias(Sccs.shape[-1])

    # PSDs in one freq bin over time
    # ff_index = len(ff)//2 - 1
    Sccs_mid = Sccs[ff_index, :] # get array of Scc values at a single frequency bin for every average taken
    Sccs_mid_mean = np.mean(Sccs_mid)
    Sccs_mid_median = np.median(Sccs_mid)
    Sccs_mid_rms = np.sqrt(np.mean(Sccs_mid**2))

    # ASDs over time
    Accs_mid = np.sqrt(Sccs_mid) # ASDs
    Accs_mid_mean = np.mean(Accs_mid)
    Accs_mid_median = np.median(Accs_mid)
    Accs_mid_rms = np.sqrt(np.mean(Accs_mid**2))

    # Return all CSDs directly: spectral_helper() CSD output
    ff, tt, Sxys = sig.spectral_helper(x, y, fs, nperseg=nperseg, noverlap=noverlap)
    Sxy = Sxys.mean(axis=-1)
    Sxy_med = (np.median(np.real(Sxys), axis=-1) + 1j * np.median(np.imag(Sxys), axis=-1))

    Sxys_mid = Sxys[ff_index, :]

    Sxys_mid_real = np.real(Sxys_mid)
    Sxys_mid_imag = np.imag(Sxys_mid)

    Sxys_mid_real_mean = np.mean(Sxys_mid_real)
    Sxys_mid_real_median = np.median(Sxys_mid_real)
    Sxys_mid_real_rms = np.sqrt(np.mean(Sxys_mid_real**2))

    Sxys_mid_imag_mean = np.mean(Sxys_mid_imag)
    Sxys_mid_imag_median = np.median(Sxys_mid_imag)
    Sxys_mid_imag_rms = np.sqrt(np.mean(Sxys_mid_imag**2))

    # CSD Pab <a*|b>
    ff, tt, Sabs = sig.spectral_helper(a, b, fs, nperseg=nperseg, noverlap=noverlap)
    Sabs_mid = Sabs[ff_index, :]

    Sabs_mid_real = np.real(Sabs_mid)
    Sabs_mid_imag = np.imag(Sabs_mid)

    Sabs_mid_real_mean = np.mean(Sabs_mid_real)
    Sabs_mid_real_median = np.median(Sabs_mid_real)
    Sabs_mid_real_rms = np.sqrt(np.mean(Sabs_mid_real**2))

    Sabs_mid_imag_mean = np.mean(Sabs_mid_imag)
    Sabs_mid_imag_median = np.median(Sabs_mid_imag)
    Sabs_mid_imag_rms = np.sqrt(np.mean(Sabs_mid_imag**2))

    # Mean-averaged CSD
    _, Pxx = sig.welch(x, fs, nperseg=nperseg, noverlap=noverlap)
    _, Pyy = sig.welch(y, fs, nperseg=nperseg, noverlap=noverlap)
    _, Pcc = sig.welch(c, fs, nperseg=nperseg, noverlap=noverlap)

    f, Pxy = sig.csd(x, y, fs, nperseg=nperseg, noverlap=noverlap, average='mean')  # scipy 1.4.1
    _, Pxc = sig.csd(x, c, fs, nperseg=nperseg, noverlap=noverlap)

    _, Cxy = sig.coherence(x, y, fs, nperseg=nperseg, noverlap=noverlap)
    _, Cxc = sig.coherence(x, c, fs, nperseg=nperseg, noverlap=noverlap)

    # Median-averaged CSD
    # _, Pxx_med = sig.welch(x, fs, nperseg=nperseg, noverlap=noverlap, average='median')
    _, Pxx_med = sig.welch(x, fs, nperseg=nperseg, noverlap=noverlap, average='median') 
    _, Pyy_med = sig.welch(y, fs, nperseg=nperseg, noverlap=noverlap, average='median') 
    _, Pcc_med = sig.welch(c, fs, nperseg=nperseg, noverlap=noverlap, average='median') 
    Pxx_med *= np.log(2)
    Pyy_med *= np.log(2)
    Pcc_med *= np.log(2)

    f_med, Pxy_med = sig.csd(x, y, fs, nperseg=nperseg, noverlap=noverlap, average='median')    # scipy 1.4.1
    _, Pxc_med = sig.csd(x, c, fs, nperseg=nperseg, noverlap=noverlap, average='median')
    Pxy_med *= np.log(2)
    Pxc_med *= np.log(2)

    Cxy_med = (np.abs(Pxy_med)/ ( np.sqrt(Pxx_med) * np.sqrt(Pyy_med) ) )**2    # boneheaded coherence calculation
    Cxc_med = (np.abs(Pxc_med)/ ( np.sqrt(Pxx_med) * np.sqrt(Pcc_med) ) )**2    # boneheaded coherence calculation

    Cxy3_med = (np.abs(Zxy3_med)/ ( np.sqrt(Pxx_med) * np.sqrt(Pyy_med) ) )**2

    biases_xy = biases_from_coherences(Cxy_med)

    # Uncorrelated PSDs
    _, Paa = sig.csd(a, a, fs, nperseg=nperseg, noverlap=noverlap)
    _, Paa_med = sig.csd(a, a, fs, nperseg=nperseg, noverlap=noverlap, average='median')

    _, Pbb = sig.csd(b, b, fs, nperseg=nperseg, noverlap=noverlap)
    _, Pbb_med = sig.csd(b, b, fs, nperseg=nperseg, noverlap=noverlap, average='median')

    # Uncorrelated CSDs
    _, Pab = sig.csd(a, b, fs, nperseg=nperseg, noverlap=noverlap)
    _, Pab_med = sig.csd(a, b, fs, nperseg=nperseg, noverlap=noverlap, average='median')


    print(f'f[1] - f[0] = {f[1] - f[0]} Hz')
    print()
    print(f'Pxx = {Pxx[50]}')
    print(f'Pyy = {Pyy[50]}')
    print(f'Pxy = {Pxy[50]}')
    print(f'abs(Pxy) = {np.abs(Pxy[50])}')
    print()
    print(f'Pxx_med = {Pxx_med[50]}')
    print(f'Pyy_med = {Pyy_med[50]}')
    print(f'Pxy_med = {Pxy_med[50]}')
    print(f'abs(Pxy_med) = {np.abs(Pxy_med[50])}')
    print()

    ###########
    # Figures #
    ###########

    # CSD mag and PSD comparison
    fig, (s1) = plt.subplots(1)

    s1.semilogy(f, np.abs(Pxy), alpha=0.5, label=r'Mean-averaged CSD $\langle x, y \rangle$')
    s1.semilogy(f_med, np.abs(Pxy_med), alpha=0.5, label=r'Median-averaged CSD $\langle x, y \rangle$')

    # s1.semilogy(f, np.abs(Zxy), alpha=0.5, label=r'Mean-averaged CSD $Z_{xy}$')
    # s1.semilogy(f, np.abs(Zxx), alpha=0.5, label=r'Mean-averaged PSD $Z_{xx}$')

    s1.semilogy(f, np.abs(Pxx), alpha=0.5, label=r'Mean-averaged PSD $\langle x, x \rangle$')
    s1.semilogy(f_med, np.abs(Pxx_med), alpha=0.5, label=r'Median-averaged PSD $\langle x, x \rangle$')

    # s1.semilogy(f, np.abs(Pyy), alpha=0.5, label=r'Mean-averaged PSD $P_{yy}$')
    # s1.semilogy(f_med, np.abs(Pyy_med), alpha=0.5, label=r'Median-averaged PSD $P_{yy}$')

    s1.semilogy(f, np.abs(Pcc), alpha=0.5, label=r'Mean-averaged PSD $\langle c, c \rangle$')
    s1.semilogy(f_med, np.abs(Pcc_med), alpha=0.5, label=r'Median-averaged PSD $\langle c, c \rangle$')

    # s1.semilogy(f, np.abs(Paa), alpha=0.5, label=r'Mean-averaged PSD $P_{aa}$')
    # s1.semilogy(f_med, np.abs(Paa_med), alpha=0.5, label=r'Median-averaged PSD $P_{aa}$')

    # s1.semilogy(f, np.abs(Pbb), alpha=0.5, label=r'Mean-averaged PSD $P_{bb}$')
    # s1.semilogy(f_med, np.abs(Pbb_med), alpha=0.5, label=r'Median-averaged PSD $P_{bb}$')

    s1.semilogy(f, total_noise_power_density * np.ones_like(f), ls='--', alpha=0.5, color='black', 
                label=f'Total noise = {total_noise_power_density:.1e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')
    s1.semilogy(f, noise_power_density * np.ones_like(f), ls='-', alpha=0.5, color='black', 
                label=f'Uncorrelated noise = {noise_power_density:.0e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')
    s1.semilogy(f, corr_noise_power_density * np.ones_like(f), ls=':', alpha=0.5, color='black', 
                label=f'Correlated noise = {corr_noise_power_density:.0e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')

    # s1.semilogy(ff, np.abs(Sxx), label='spectral helper() mean average')


    # s1.set_title(f'Cross spectral density - Averages = {averages}')
    s1.set_ylabel(r'CSD Mag [$\mathrm{V}^2/\mathrm{Hz}$]')
    s1.set_xlabel('Frequency [Hz]')

    s1.set_xlim([f[1], f[-1]])
    s1.set_ylim([1e-4, 3e-3])

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.5)

    s1.legend(loc='lower left', ncol=2)

    plot_name = f'scipy_psd_and_csds_mean_vs_median_averaging_averages_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    # make_interactive_svg(fig, full_plot_name.split('.pdf')[0])
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # Full CSD and coherence direct comparison
    fig, (s1, s2, s3) = plt.subplots(3, sharex=True, figsize=(12,12))

    vpp = np.max(np.abs(Pxy_med[2:-2])) - np.min(np.abs(Pxy_med[2:-2]))
    vp = vpp/2
    biases2 = vpp * (np.sin(2 * np.angle(Pxy_med[2:-2]))**2 )

    s1.semilogy(f[2:-2], np.abs(Pxy[2:-2]), alpha=0.5, label=r'Mean-averaged CSD $\langle x, y \rangle$')
    s1.semilogy(f_med[2:-2], np.abs(Pxy_med[2:-2]), alpha=0.5, label=r'Median-averaged CSD $\langle x, y \rangle$')
    s1.semilogy(f_med[2:-2], np.abs(Zxy3_med[2:-2]), alpha=0.5, label=r'Phase-corrected median-averaged CSD $\langle x, y \rangle$')

    # s1.semilogy(f[2:-2], np.abs(Pxx[2:-2]), alpha=0.5, label=r'Mean-averaged PSD $\langle x, x \rangle$')
    # s1.semilogy(f_med[2:-2], np.abs(Pxx_med[2:-2]), alpha=0.5, label=r'Median-averaged PSD $\langle x, x \rangle$')

    # s1.semilogy(f, total_noise_power_density * np.ones_like(f), ls=':', alpha=0.5, color='black', 
    #             label=f'Total noise = {total_noise_power_density:.1e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')
    # s1.semilogy(f, noise_power_density * np.ones_like(f), ls='-', alpha=0.5, color='black', 
    #             label=f'Uncorrelated noise = {noise_power_density:.0e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')
    s1.semilogy(f, corr_noise_power_density * np.ones_like(f), ls='--', alpha=0.5, color='black', 
                label=f'Correlated noise = {corr_noise_power_density:.0e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')

    s2.plot(f[2:-2], 180/np.pi * np.angle(Pxy[2:-2]), alpha=0.5, label=r'Mean-averaged CSD $\langle x, y \rangle$')
    s2.plot(f_med[2:-2], 180/np.pi * np.angle(Pxy_med[2:-2]), alpha=0.5, label=r'Median-averaged CSD $\langle x, y \rangle$')
    s2.plot(f_med[2:-2], 180/np.pi * np.angle(Zxy3_med[2:-2]), alpha=0.5, label=r'Phase-corrected median-averaged CSD $\langle x, y \rangle$')

    s3.semilogy(f[2:-2], np.abs(Cxy[2:-2]), alpha=0.5, label='Mean-averaged coherence' + r'$\gamma^2_{xy}$')
    s3.semilogy(f_med[2:-2], np.abs(Cxy_med[2:-2]), alpha=0.5, label='Median-averaged coherence' + r'$\gamma^2_{xy}$')
    s3.semilogy(f_med[2:-2], np.abs(Cxy3_med[2:-2]), alpha=0.5, label='Phase-corrected median-averaged coherence' + r'$\gamma^2_{xy}$')

    s1.set_ylabel(r'CSD Mag [$\mathrm{V}^2/\mathrm{Hz}$]')
    s2.set_ylabel('Phase [degrees]')
    s3.set_ylabel('Power Coherence')
    s3.set_xlabel('Frequency [Hz]')

    s1.set_ylim([4e-4, 9e-4])
    s2.set_yticks([-180, -90, 0, 90, 180])
    # s3.set_ylim([0, 1])

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.5)
    s2.grid()
    s2.grid(which='minor', ls='--', alpha=0.5)
    s3.grid()
    s3.grid(which='minor', ls='--', alpha=0.5)

    s1.legend(ncol=2)

    plot_name = f'scipy_csds_and_coherence_mean_vs_median_averaging_averages_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # Histogram of time-domain samples
    fig, (s1) = plt.subplots(1)

    # hist, bin_edges = np.histogram(Sxxs_mid, bins=20)
    num_bins = 100
    low_bin  = -1.5 * noise_power
    high_bin =  1.5 * noise_power
    bins = np.linspace(low_bin, high_bin, num_bins+1)

    xx = np.linspace(low_bin, high_bin, 1001)

    norm_a_fit_params = norm.fit(a)
    print(f'norm_a_fit_params = {norm_a_fit_params}')
    norm_a_pdf_fit = norm.pdf(xx, loc=norm_a_fit_params[0], scale=norm_a_fit_params[1])

    norm_c_fit_params = norm.fit(c)
    print(f'norm_c_fit_params = {norm_c_fit_params}')
    norm_c_pdf_fit = norm.pdf(xx, loc=norm_c_fit_params[0], scale=norm_c_fit_params[1])

    norm_x_fit_params = norm.fit(x)
    print(f'norm_x_fit_params = {norm_x_fit_params}')
    norm_x_pdf_fit = norm.pdf(xx, loc=norm_x_fit_params[0], scale=norm_x_fit_params[1])

    NNa, bins, patches = s1.hist(a, bins=bins, histtype='step', lw=2, zorder=3, density=True,
                                label='a(t) samples')
    NNc, bins, patches = s1.hist(c, bins=bins, histtype='step', lw=2, zorder=3, density=True,
                                label='c(t) samples')
    NNx, bins, patches = s1.hist(x, bins=bins, histtype='step', lw=2, zorder=3, density=True,
                                label='x(t) = a(t) + c(t) samples')

    # Calculate mean squared error
    middle_bins = mid_bins(bins)
    mse_a = mean_squared_error(middle_bins, NNa, norm, norm_a_fit_params)
    mse_c = mean_squared_error(middle_bins, NNc, norm, norm_c_fit_params)
    mse_x = mean_squared_error(middle_bins, NNx, norm, norm_x_fit_params)

    s1.plot(xx, norm_a_pdf_fit, color='C0', ls='--', alpha=0.5,
            label=  f'a(t) PDF ' + \
                    r'$\frac{1}{%.2f \sqrt{2 \pi}} \exp(-\frac{1}{2}\left(\frac{a}{%.2f}\right)^2)$'%(norm_a_fit_params[1], norm_a_fit_params[1])
                )
    s1.plot(xx, norm_c_pdf_fit, color='C1', ls='--', alpha=0.5,
            label=  'c(t) PDF ' + \
                    r'$\frac{1}{%.2f \sqrt{2 \pi}} \exp(-\frac{1}{2}\left(\frac{c}{%.2f}\right)^2)$'%(norm_c_fit_params[1], norm_c_fit_params[1])
                )
    s1.plot(xx, norm_x_pdf_fit, color='C2', ls='--', alpha=0.5,
            label=  'x(t) PDF ' + \
                    r'$\frac{1}{%.2f \sqrt{2 \pi}} \exp(-\frac{1}{2}\left(\frac{x}{%.2f}\right)^2)$'%(norm_x_fit_params[1], norm_x_fit_params[1])
                )

    s1.set_xlim([xx[0], xx[-1]])

    # s1.set_title(f'Time domain signal distributions - N = {N} samples')
    s1.set_xlabel(r'Sample values [$\mathrm{V}$]')
    s1.set_ylabel('Normalized Occurances')

    s1.legend()
    s1.grid()

    plot_name = f'time_domain_histograms_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()
    


    # Histogram of FFTs
    fig, (s1, s2) = plt.subplots(2, sharex=True)

    zaas_reshape = np.reshape(zaas[1:-1,:], (len(ff)-2) * len(tt)) #remove the first and last frequency point 
    zccs_reshape = np.reshape(zccs[1:-1,:], (len(ff)-2) * len(tt)) 
    zxxs_reshape = np.reshape(zxxs[1:-1,:], (len(ff)-2) * len(tt)) 

    num_bins = 100
    low_bin = -0.05
    high_bin = 0.05
    bins = np.linspace(low_bin, high_bin, num_bins+1)

    norm_re_a_fit_params = norm.fit(np.real(zaas_reshape))
    print(f'norm_re_a_fit_params = {norm_re_a_fit_params}')
    norm_re_a_pdf_fit = norm.pdf(bins, loc=norm_re_a_fit_params[0], scale=norm_re_a_fit_params[1])

    norm_im_a_fit_params = norm.fit(np.imag(zaas_reshape))
    print(f'norm_im_a_fit_params = {norm_im_a_fit_params}')
    norm_im_a_pdf_fit = norm.pdf(bins, loc=norm_im_a_fit_params[0], scale=norm_im_a_fit_params[1])

    norm_re_c_fit_params = norm.fit(np.real(zccs_reshape))
    print(f'norm_re_c_fit_params = {norm_re_c_fit_params}')
    norm_re_c_pdf_fit = norm.pdf(bins, loc=norm_re_c_fit_params[0], scale=norm_re_c_fit_params[1])

    norm_im_c_fit_params = norm.fit(np.imag(zccs_reshape))
    print(f'norm_im_c_fit_params = {norm_im_c_fit_params}')
    norm_im_c_pdf_fit = norm.pdf(bins, loc=norm_im_c_fit_params[0], scale=norm_im_c_fit_params[1])

    norm_re_x_fit_params = norm.fit(np.real(zxxs_reshape))
    print(f'norm_re_x_fit_params = {norm_re_x_fit_params}')
    norm_re_x_pdf_fit = norm.pdf(bins, loc=norm_re_x_fit_params[0], scale=norm_re_x_fit_params[1])

    norm_im_x_fit_params = norm.fit(np.imag(zxxs_reshape))
    print(f'norm_im_x_fit_params = {norm_im_x_fit_params}')
    norm_im_x_pdf_fit = norm.pdf(bins, loc=norm_im_x_fit_params[0], scale=norm_im_x_fit_params[1])

    reNN, rebins, repatches = s1.hist(  np.real(zaas_reshape), bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Re(Z_{a})$'+' samples')
    imNN, imbins, impatches = s2.hist(  np.imag(zaas_reshape), bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Im(Z_{a})$'+' samples')

    reNN, rebins, repatches = s1.hist(  np.real(zccs_reshape), bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Re(Z_{c})$'+' samples')
    imNN, imbins, impatches = s2.hist(  np.imag(zccs_reshape), bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Im(Z_{c})$'+' samples')

    reNN, rebins, repatches = s1.hist(  np.real(zxxs_reshape), bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Re(Z_{x})$'+' samples')
    imNN, imbins, impatches = s2.hist(  np.imag(zxxs_reshape), bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Im(Z_{x})$'+' samples')

    s1.plot(bins, norm_re_a_pdf_fit, color='C0', ls='--', lw=4, alpha=0.5,
            label='PDF ' + r'$\frac{1}{%.2e \sqrt{2 \pi}} \exp(-\frac{1}{2}\left(\frac{A}{%.2e}\right)^2)$'%(norm_re_a_fit_params[1], norm_re_a_fit_params[1]))
    s2.plot(bins, norm_im_a_pdf_fit, color='C0', ls='--', lw=4, alpha=0.5,
            label='PDF ' + r'$\frac{1}{%.2e \sqrt{2 \pi}} \exp(-\frac{1}{2}\left(\frac{A}{%.2e}\right)^2)$'%(norm_im_a_fit_params[1], norm_im_a_fit_params[1]))

    s1.plot(bins, norm_re_c_pdf_fit, color='C1', ls='--', lw=4, alpha=0.5,
            label='PDF ' + r'$\frac{1}{%.2e \sqrt{2 \pi}} \exp(-\frac{1}{2}\left(\frac{C}{%.2e}\right)^2)$'%(norm_re_c_fit_params[1], norm_re_c_fit_params[1]))
    s2.plot(bins, norm_im_c_pdf_fit, color='C1', ls='--', lw=4, alpha=0.5,
            label='PDF ' + r'$\frac{1}{%.2e \sqrt{2 \pi}} \exp(-\frac{1}{2}\left(\frac{C}{%.2e}\right)^2)$'%(norm_im_c_fit_params[1], norm_im_c_fit_params[1]))

    s1.plot(bins, norm_re_x_pdf_fit, color='C2', ls='--', lw=4, alpha=0.5,
            label='PDF ' + r'$\frac{1}{%.2e \sqrt{2 \pi}} \exp(-\frac{1}{2}\left(\frac{X}{%.2e}\right)^2)$'%(norm_re_x_fit_params[1], norm_re_x_fit_params[1]))
    s2.plot(bins, norm_im_x_pdf_fit, color='C2', ls='--', lw=4, alpha=0.5,
            label='PDF ' + r'$\frac{1}{%.2e \sqrt{2 \pi}} \exp(-\frac{1}{2}\left(\frac{X}{%.2e}\right)^2)$'%(norm_im_x_fit_params[1], norm_im_x_fit_params[1]))

    s1.set_xlim([bins[0], bins[-1]])
    s2.set_xlim([bins[0], bins[-1]])

    # s1.set_title(f'FFT histograms - {avg} averages')
    s1.set_xlabel(r'$\Re{(\mathrm{FFT})}$ values [$\mathrm{V}/\sqrt{\mathrm{Hz}}$]')
    s1.set_ylabel('Normalized Counts')

    # s2.set_title(r'$\Im{(\mathrm{CSD})}$ ' + f'{avg} averages')
    s2.set_xlabel(r'$\Im{(\mathrm{FFT})}$ values [$\mathrm{V}/\sqrt{\mathrm{Hz}}$]')
    s2.set_ylabel('Normalized Counts')

    s1.legend()
    s1.grid()

    s2.legend()
    s2.grid()

    plot_name = f'FFT_histograms_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()   


    # Histogram of PSD
    fig, (s1) = plt.subplots(1)

    # hist, bin_edges = np.histogram(Sxxs_mid, bins=20)
    num_bins = 100
    low_bin = 0.0
    high_bin = 0.005
    bins = np.linspace(low_bin, high_bin, num_bins+1)
    xx = np.linspace(low_bin, high_bin, 1001)

    expon_a_fit_params = expon.fit(Saas_mid)
    print(f'expon_a_fit_params = {expon_a_fit_params}')
    expon_a_pdf_fit = expon.pdf(xx, loc=expon_a_fit_params[0], scale=expon_a_fit_params[1])

    expon_c_fit_params = expon.fit(Sccs_mid)
    print(f'expon_c_fit_params = {expon_c_fit_params}')
    expon_c_pdf_fit = expon.pdf(xx, loc=expon_c_fit_params[0], scale=expon_c_fit_params[1])

    expon_x_fit_params = expon.fit(Sxxs_mid)
    print(f'expon_x_fit_params = {expon_x_fit_params}')
    expon_x_pdf_fit = expon.pdf(xx, loc=expon_x_fit_params[0], scale=expon_x_fit_params[1])

    NN, bins, patches = s1.hist(Saas_mid, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$S_{a}$'+' samples')
    NN, bins, patches = s1.hist(Sccs_mid, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$S_{c}$'+' samples')
    NN, bins, patches = s1.hist(Sxxs_mid, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$S_{x}$'+' samples')

    s1.plot(xx, expon_a_pdf_fit, color='C0', ls='--', lw=4, alpha=0.5,
            label=r'$S_{a}$'+' Exponential PDF ' + r'$\frac{1}{%.2e} \exp(\frac{-a}{%.2e})$'%(expon_a_fit_params[1], expon_a_fit_params[1]))
    s1.plot(xx, expon_c_pdf_fit, color='C1', ls='--', lw=4, alpha=0.5,
            label=r'$S_{c}$'+' Exponential PDF ' + r'$\frac{1}{%.2e} \exp(\frac{-c}{%.2e})$'%(expon_c_fit_params[1], expon_c_fit_params[1]))
    s1.plot(xx, expon_x_pdf_fit, color='C2', ls='--', lw=4, alpha=0.5,
            label=r'$S_{x}$'+' Exponential PDF ' + r'$\frac{1}{%.2e} \exp(\frac{-x}{%.2e})$'%(expon_x_fit_params[1], expon_x_fit_params[1]))

    s1.axvline(Saas_mid_mean,   ymin=0, ymax=np.max(NN)*1.2, ls='--', color='C0', label=r'$S_{a}$'+f' mean = {Saas_mid_mean:.2e}')
    s1.axvline(Saas_mid_median, ymin=0, ymax=np.max(NN)*1.2, ls=':',  color='C0', label=r'$S_{a}$'+f' median = {Saas_mid_median:.2e}')

    s1.axvline(Sccs_mid_mean,   ymin=0, ymax=np.max(NN)*1.2, ls='--', color='C1', label=r'$S_{c}$'+f' mean = {Sccs_mid_mean:.2e}')
    s1.axvline(Sccs_mid_median, ymin=0, ymax=np.max(NN)*1.2, ls=':',  color='C1', label=r'$S_{c}$'+f' median = {Sccs_mid_median:.2e}')

    s1.axvline(Sxxs_mid_mean,   ymin=0, ymax=np.max(NN)*1.2, ls='--', color='C2', label=r'$S_{x}$'+f' mean = {Sxxs_mid_mean:.2e}')
    s1.axvline(Sxxs_mid_median, ymin=0, ymax=np.max(NN)*1.2, ls=':',  color='C2', label=r'$S_{x}$'+f' median = {Sxxs_mid_median:.2e}')
    # s1.axvline(Sxxs_mid_rms,    ymin=0, ymax=np.max(NN)*1.2, ls=':', color='C2', label=r'$S_{x}$'+f'rms = {Sxxs_mid_rms:.2e}')

    s1.set_xlim([bins[0], bins[-1]])

    # s1.set_title(f'PSD distribution for freq bin = {ff[ff_index]} Hz and {avg} averages')
    s1.set_xlabel(r'PSD values [$\mathrm{V}^2/\mathrm{Hz}$]')
    s1.set_ylabel('Number of Occurances')

    s1.legend()
    s1.grid()

    plot_name = f'PSD_histogram_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # Histogram of ASD
    fig, (s1) = plt.subplots(1)

    # hist, bin_edges = np.histogram(Sxxs_mid, bins=20)
    num_bins = 100
    low_bin = 0.0
    high_bin = 0.12
    bins = np.linspace(low_bin, high_bin, num_bins+1)

    rayleigh_a_fit_params = rayleigh.fit(Aaas_mid)
    print(f'rayleigh_a_fit_params = {rayleigh_a_fit_params}')
    rayleigh_a_pdf_fit = rayleigh.pdf(bins, *rayleigh_a_fit_params)

    rayleigh_c_fit_params = rayleigh.fit(Accs_mid)
    print(f'rayleigh_c_fit_params = {rayleigh_c_fit_params}')
    rayleigh_c_pdf_fit = rayleigh.pdf(bins, *rayleigh_c_fit_params)

    rayleigh_x_fit_params = rayleigh.fit(Axxs_mid)
    print(f'rayleigh_x_fit_params = {rayleigh_x_fit_params}')
    rayleigh_x_pdf_fit = rayleigh.pdf(bins, *rayleigh_x_fit_params)

    NN, bins, patches = s1.hist(Aaas_mid, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\sqrt{S_{a}}$'+' samples')
    NN, bins, patches = s1.hist(Accs_mid, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\sqrt{S_{c}}$'+' samples')
    NN, bins, patches = s1.hist(Axxs_mid, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\sqrt{S_{x}}$'+' samples')

    s1.plot(bins, rayleigh_a_pdf_fit, color='C0', ls='--', lw=4, alpha=0.5,
            label=r'$\sqrt{S_{a}}$'+' Rayleigh PDF ' + r'$\frac{a}{%.2e} \exp(-\frac{a^2}{2 \times %.2e})$'%(rayleigh_a_fit_params[1], rayleigh_a_fit_params[1]))
    s1.plot(bins, rayleigh_c_pdf_fit, color='C1', ls='--', lw=4, alpha=0.5,
            label=r'$\sqrt{S_{c}}$'+' Rayleigh PDF ' + r'$\frac{c}{%.2e} \exp(-\frac{c^2}{2 \times %.2e})$'%(rayleigh_c_fit_params[1], rayleigh_c_fit_params[1]))
    s1.plot(bins, rayleigh_x_pdf_fit, color='C2', ls='--', lw=4, alpha=0.5,
            label=r'$\sqrt{S_{x}}$'+' Rayleigh PDF ' + r'$\frac{x}{%.2e} \exp(-\frac{x^2}{2 \times %.2e})$'%(rayleigh_x_fit_params[1], rayleigh_x_fit_params[1]))

    # s1.axvline(Aaas_mid_mean, ymin=0, ymax=np.max(NN)*1.2, ls='--', color='C0', label=f'mean = {Aaas_mid_mean:.2e}')
    s1.axvline(Aaas_mid_median, ymin=0, ymax=np.max(NN)*1.2, ls=':', color='C0', label=r'$\sqrt{S_{a}}$'+f' median = {Aaas_mid_median:.2e}')
    s1.axvline(Aaas_mid_rms, ymin=0, ymax=np.max(NN)*1.2, ls='-', color='C0', label=r'$\sqrt{S_{a}}$'+f' rms = {Aaas_mid_rms:.2e}')

    # s1.axvline(Accs_mid_mean, ymin=0, ymax=np.max(NN)*1.2, ls='--', color='C1', label=f'mean = {Accs_mid_mean:.2e}')
    s1.axvline(Accs_mid_median, ymin=0, ymax=np.max(NN)*1.2, ls=':', color='C1', label=r'$\sqrt{S_{c}}$'+f' median = {Accs_mid_median:.2e}')
    s1.axvline(Accs_mid_rms, ymin=0, ymax=np.max(NN)*1.2, ls='-', color='C1', label=r'$\sqrt{S_{c}}$'+f' rms = {Accs_mid_rms:.2e}')

    # s1.axvline(Axxs_mid_mean, ymin=0, ymax=np.max(NN)*1.2, ls='--', color='C2', label=f'mean = {Axxs_mid_mean:.2e}')
    s1.axvline(Axxs_mid_median, ymin=0, ymax=np.max(NN)*1.2, ls=':', color='C2', label=r'$\sqrt{S_{x}}$'+f' median = {Axxs_mid_median:.2e}')
    s1.axvline(Axxs_mid_rms, ymin=0, ymax=np.max(NN)*1.2, ls='-', color='C2', label=r'$\sqrt{S_{x}}$'+f' rms = {Axxs_mid_rms:.2e}')

    s1.set_xlim([bins[0], bins[-1]])

    # s1.set_title(f'ASD distribution for freq bin = {ff[ff_index]} Hz and {avg} averages')
    s1.set_xlabel(r'ASD values [$\mathrm{V}/\sqrt{\mathrm{Hz}}$]')
    s1.set_ylabel('Number of Occurances')

    s1.legend()
    s1.grid()

    plot_name = f'ASD_histogram_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # CSD real and imaginary histograms
    fig, (s1, s2) = plt.subplots(2, sharex=True)

    num_bins = 100
    low_bin = -0.003
    high_bin = 0.003
    bins = np.linspace(low_bin, high_bin, num_bins+1)

    reNNxy, rebins, repatches = s1.hist(Sxys_mid_real, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Re(S_{xy})$'+' samples')
    # reNN, rebins, repatches = s1.hist(Zxys_mid_real, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Re(Z_{xy})$'+' samples')
    reNNab, rebins, repatches = s1.hist(Sabs_mid_real, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Re(S_{ab})$'+' samples')

    s1.axvline(Sxys_mid_real_mean,   ymin=0, ymax=np.max(reNNab)*1.2,          color='C8', label=r'$\Re(S_{xy})$'+f' mean = {Sxys_mid_real_mean:.2e}')
    s1.axvline(Sxys_mid_real_median, ymin=0, ymax=np.max(reNNab)*1.2, ls='--', color='C3', label=r'$\Re(S_{xy})$'+f' median = {Sxys_mid_real_median:.2e}')
    # s1.axvline(Sxys_mid_real_rms,    ymin=0, ymax=np.max(reNN)*1.2, ls=':',  color='C4', label=r'$\Re(S_{xy})$'+f' rms = {Sxys_mid_real_rms:.2e}')

    s1.axvline(Sabs_mid_real_mean,   ymin=0, ymax=np.max(reNNab)*1.2,          color='C5', label=r'$\Re(S_{ab})$'+f' mean = {Sabs_mid_real_mean:.2e}')
    s1.axvline(Sabs_mid_real_median, ymin=0, ymax=np.max(reNNab)*1.2, ls='--', color='C6', label=r'$\Re(S_{ab})$'+f' median = {Sabs_mid_real_median:.2e}')
    # s1.axvline(Sabs_mid_real_rms,    ymin=0, ymax=np.max(reNN)*1.2, ls=':',  color='C7', label=r'$\Re(S_{ab})$'+f' rms = {Sabs_mid_real_rms:.2e}')

    xx = np.linspace(low_bin, high_bin, 1001)
    p0 = [0, 250, 1]
    middle_bins = mid_bins(bins)
    laplace_xy_real_fit_params, laplace_xy_real_fit_cov  = curve_fit(asymmetric_laplace, middle_bins, reNNxy, p0=p0)
    print(f'laplace_xy_real_fit_params = {laplace_xy_real_fit_params}')
    laplace_xy_real_pdf_fit = asymmetric_laplace(xx, *laplace_xy_real_fit_params)

    laplace_ab_real_fit_params = laplace.fit(Sabs_mid_real)
    print(f'laplace_ab_real_fit_params = {laplace_ab_real_fit_params}')
    laplace_ab_real_pdf_fit = laplace.pdf(xx, *laplace_ab_real_fit_params)

    s1.plot(xx, laplace_xy_real_pdf_fit, color='k',  lw=4, label='Asymmetric Laplace PDF ' r'$\Re(S_{xy})$')
    s1.plot(xx, laplace_ab_real_pdf_fit, color='C9', lw=4, label='Laplace PDF ' r'$\Re(S_{ab})$')

    imNNxy, imbins, impatches = s2.hist(Sxys_mid_imag, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Im(S_{xy})$'+' samples')  
    # imNN, imbins, impatches = s2.hist(Zxys_mid_imag, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Im(Z_{xy})$'+' samples') 
    imNNab, imbins, impatches = s2.hist(Sabs_mid_imag, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Im(S_{ab})$'+' samples')  

    s2.axvline(Sxys_mid_imag_mean,   ymin=0, ymax=np.max(imNNab)*1.2,          color='C8', label=r'$\Im(S_{xy})$'+f' mean = {Sxys_mid_imag_mean:.2e}')
    s2.axvline(Sxys_mid_imag_median, ymin=0, ymax=np.max(imNNab)*1.2, ls='--', color='C3', label=r'$\Im(S_{xy})$'+f' median = {Sxys_mid_imag_median:.2e}')
    # s2.axvline(Sxys_mid_imag_rms,    ymin=0, ymax=np.max(imNN)*1.2, ls=':',  color='C4', label=r'$\Im(S_{xy})$'+f' rms = {Sxys_mid_imag_rms:.2e}')

    s2.axvline(Sabs_mid_imag_mean,   ymin=0, ymax=np.max(imNNab)*1.2,          color='C5', label=r'$\Im(S_{ab})$'+f' mean = {Sabs_mid_imag_mean:.2e}')
    s2.axvline(Sabs_mid_imag_median, ymin=0, ymax=np.max(imNNab)*1.2, ls='--', color='C6', label=r'$\Im(S_{ab})$'+f' median = {Sabs_mid_imag_median:.2e}')
    # s2.axvline(Sabs_mid_imag_rms,    ymin=0, ymax=np.max(imNN)*1.2, ls=':',  color='C7', label=r'$\Im(S_{ab})$'+f' rms = {Sabs_mid_imag_rms:.2e}')

    laplace_xy_imag_fit_params, laplace_xy_imag_fit_cov  = curve_fit(asymmetric_laplace, middle_bins, imNNxy, p0=p0)
    print(f'laplace_xy_imag_fit_params = {laplace_xy_imag_fit_params}')
    laplace_xy_imag_pdf_fit = asymmetric_laplace(xx, *laplace_xy_imag_fit_params)

    laplace_ab_imag_fit_params = laplace.fit(Sabs_mid_imag)
    print(f'laplace_ab_imag_fit_params = {laplace_ab_imag_fit_params}')
    laplace_ab_imag_pdf_fit = laplace.pdf(xx, *laplace_ab_imag_fit_params)

    s2.plot(xx, laplace_xy_imag_pdf_fit, color='k',  lw=4, label='Asymmetric Laplace PDF ' r'$\Im(S_{xy})$')
    s2.plot(xx, laplace_ab_imag_pdf_fit, color='C9', lw=4, label='Laplace PDF ' r'$\Im(S_{ab})$')

    s1.set_xlim(bins[0], bins[-1])
    s2.set_xlim(bins[0], bins[-1])

    # s1.set_title(f'CSD histograms - {avg} averages')
    s1.set_xlabel(r'$\Re{(\mathrm{CSD})}$ values [$\mathrm{V^2}/\mathrm{Hz}$]')
    s1.set_ylabel('Normalized Counts')

    # s2.set_title(r'$\Im{(\mathrm{CSD})}$ ' + f'{avg} averages')
    s2.set_xlabel(r'$\Im{(\mathrm{CSD})}$ values [$\mathrm{V^2}/\mathrm{Hz}$]')
    s2.set_ylabel('Normalized Counts')

    s1.legend()
    s1.grid()

    s2.legend()
    s2.grid()

    plot_name = f'CSD_histograms_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()    



    # CSD 2D histograms
    fig, ss = plt.subplots(2, 2, sharex="col", sharey="row", figsize=(9, 9),
                            gridspec_kw=dict(   height_ratios=[1, 3],
                                                width_ratios=[3, 1])
                            )
    s1 = ss[0, 0]   # top histogram
    s2 = ss[0, 1]   # delete
    s3 = ss[1, 0]   # 2d hist
    s4 = ss[1, 1]   # right histogram

    fig.delaxes(s2) # delete

    num_bins = 400
    low_bin = -0.01
    high_bin = 0.01
    bins = np.linspace(low_bin, high_bin, num_bins+1)
    xx = np.linspace(low_bin, high_bin, 1000)

    reNN, rebins, repatches = s1.hist(Sxys_mid_real, bins=bins, histtype='step', lw=2, zorder=3, density=True,
                                        label=r'$\Re{(\langle x, y \rangle)}$'+' samples')  
    imNN, imbins, impatches = s4.hist(Sxys_mid_imag, bins=bins, histtype='step', lw=2, zorder=3, density=True, orientation='horizontal',
                                        label=r'$\Im{(\langle x, y \rangle)}$'+' samples')  
    H, X, Y, image = s3.hist2d(Sxys_mid_real, Sxys_mid_imag, bins=(rebins, imbins), density=True, 
                                        cmap=mpl.cm.copper, norm=mpl.colors.LogNorm())

    s1.axvline(Sxys_mid_real_mean, ymin=0, ymax=np.max(reNN)*1.2, color='C2', label=f'mean = {Sxys_mid_real_mean:.2e}')
    s1.axvline(Sxys_mid_real_median, ymin=0, ymax=np.max(reNN)*1.2, ls='--', color='C3', label=f'median = {Sxys_mid_real_median:.2e}')

    s4.axhline(Sxys_mid_imag_mean, xmin=0, xmax=np.max(imNN)*1.2, color='C2', label=f'mean = {Sxys_mid_imag_mean:.2e}')
    s4.axhline(Sxys_mid_imag_median, xmin=0, xmax=np.max(imNN)*1.2, ls='--', color='C3', label=f'median = {Sxys_mid_imag_median:.2e}')

    # Model fits
    p0 = [0, 250, 1]
    middle_bins = mid_bins(bins)
    laplace_xy_real_fit_params, laplace_xy_real_fit_cov  = curve_fit(asymmetric_laplace, middle_bins, reNN, p0=p0)
    laplace_xy_imag_fit_params, laplace_xy_imag_fit_cov  = curve_fit(asymmetric_laplace, middle_bins, imNN, p0=p0)

    laplace_xy_real_pdf_fit = asymmetric_laplace(xx, *laplace_xy_real_fit_params)
    laplace_xy_imag_pdf_fit = asymmetric_laplace(xx, *laplace_xy_imag_fit_params)

    s1.plot(xx, laplace_xy_real_pdf_fit, color='k',  lw=4, label=r'$f_{\Re(\langle x, y \rangle)}$' + ' asymmetric Laplace')
    s4.plot(laplace_xy_imag_pdf_fit, xx, color='k',  lw=4, label=r'$f_{\Im(\langle x, y \rangle)}$' + ' asymmetric Laplace')

    # 2D model
    phi0 = np.angle(np.mean(Sxys_mid))
    lambda0 = 0.5 * np.sqrt(corr_noise_power_density * noise_power_density + corr_noise_power_density * noise_power_density + noise_power_density**2)
    coherence = (corr_noise_power_density)**2 / (noise_power_density + corr_noise_power_density)**2
    csd_pdf = csd_2d_probability(xx, xx, lambda0, coherence, phi0=phi0)

    # Create contours
    levels = np.array([1-np.exp(-0.5*1**2), 1-np.exp(-0.5*2**2)])
    X2, Y2, H2, V = get_contours( X, Y, H, levels=levels)

    e1 = s3.contour(X2, Y2, np.log(H2.T), np.log(V), cmap='autumn')

    # Compute the density levels.
    H = csd_pdf
    X = np.linspace(low_bin, high_bin, 1000+1)
    Y = np.linspace(low_bin, high_bin, 1000+1)

    X2, Y2, H2, V = get_contours(X, Y, H)

    d1 = s3.contour(X2, Y2, np.log(H2.T), np.log(V), linewidths=1.5, cmap='cool_r')

    # Arrow
    a0 = s3.arrow(0, 0, Sxys_mid_real_mean, Sxys_mid_imag_mean, color='C2', length_includes_head=True, width=5e-5, head_width=2e-4, head_length=2e-4, zorder=2)

    # Form the legend handles and labels combo
    handles = np.array([])
    labels = np.array([])
    for ii, contour in enumerate(e1.collections[::-1]):
        handles = np.append(handles, contour)
        labels = np.append(labels, f'2d histogram {ii+1}' + r'$\sigma$ contour')

    for ii, contour in enumerate(d1.collections[::-1]):
        handles = np.append(handles, contour)
        labels = np.append(labels, r'$f_{\langle x, y \rangle}$ '+f'{ii+1}' + r'$\sigma$ contour')

    handles = np.append(handles, a0)
    labels = np.append(labels, 'mean vector')

    s1.set_xlim([-0.004, 0.006])
    s3.set_xlim([-0.004, 0.006])

    s3.set_ylim([-0.004, 0.006])
    s4.set_ylim([-0.004, 0.006])

    s3.set_xlabel(r'$\Re{(\langle x, y \rangle)}$ samples [$\mathrm{V^2}/\mathrm{Hz}$]')
    s3.set_ylabel(r'$\Im{(\langle x, y \rangle)}$ samples [$\mathrm{V^2}/\mathrm{Hz}$]')

    s1.legend(fontsize=12, loc='upper right')
    s3.legend(handles, labels, fontsize=12, ncol=2, framealpha=0.9, loc='lower right')
    s4.legend(fontsize=12, loc='upper right')

    s1.grid()
    s3.grid()
    s4.grid()

    plot_name = f'csd_2d_histogram_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()


    print()
    print(f'Finished with averages = {averages} after {time.time() - start_time} seconds')
    print()
    

# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()

# if __name__ == '__main__':
#     main()