\documentclass[12pt]{article}
\usepackage{libertine}
\usepackage{amsmath} % I want dfrac
\usepackage[margin=1.0in]{geometry}
\usepackage[onehalfspacing]{setspace}
\usepackage{graphicx}

\usepackage{hyperref}
\hypersetup{
  colorlinks = true,
  citecolor=[rgb]{0.58039216, 0.        , 0.82745098},
  linkcolor=[rgb]{0.13333333, 0.50509804, 0.13333333},
  urlcolor=[rgb]{0.2745098 , 0.50980392, 0.70588235},
}

\begin{document}

\title{Sum of exponential and laplace distributions}
\author{Craig Cahillane}
\date{June 16, 2020}

\maketitle


I proved the following result while working on my thesis.
The result turned out to be not relevant to my thesis, 
but I wanted to record the result here since working through it was instructive.

\section{Problem}

Suppose we have two independent random variables $\mathcal{X} \sim \mathrm{Exp}(\lambda)$ and $\mathcal{Y} \sim \mathrm{Laplace}(\mu = 0, \gamma)$.
Let $\mathcal{Z} = \mathcal{X} + \mathcal{Y}$.
What is the probability density function $f_\mathcal{Z}(z)$ of $\mathcal{Z}$?

$\mathrm{Exp}(\lambda)$ is the exponential distribution, defined such that $\lambda$ is its mean:
\begin{align}
    \label{eq:exponential}
    \mathrm{Exp}(\lambda) = f_\mathcal{X}(x) = \dfrac{1}{\lambda} e^{-\frac{x}{\lambda}} \qquad x \in [0, \infty)
\end{align}
where $f_\mathcal{X}$ is the probability density function (PDF), and $x$ is the parameter of $\mathcal{X}$.

$\mathrm{Laplace}(\mu = 0, \gamma)$ is the laplace distribution, with its mean $\mu$ defined to be zero and scale parameter $\gamma$:
\begin{align}
    \label{eq:laplace}
    \mathrm{Laplace}(\mu, \gamma) = f_\mathcal{Y}(y) = \dfrac{1}{2 \gamma} e^{-\frac{|y - \mu|}{\gamma}} \qquad y \in (\infty, \infty)
\end{align}

\section{Solution}

Since $\mathcal{X}, \mathcal{Y}$ are independent, 
we can define the joint probability distribution $f_{\mathcal{X}\mathcal{Y}}(x,y)$ as the product of the two PDFs:
\begin{align}
    \label{eq:joint_prob}
    f_{\mathcal{X}\mathcal{Y}}(x,y) &= f_\mathcal{X}(x) f_\mathcal{Y}(y) \\
    &= \dfrac{1}{2 \lambda \gamma} e^{-\frac{x}{\lambda}} e^{-\frac{|y|}{\gamma}}  \qquad x \in [0, \infty) \quad y \in (\infty, \infty)
\end{align}

We will start with the cumulative distribution function (CDF) $F_\mathcal{Z}(z)$.
The CDF is defined as the probability that the random variable $\mathcal{Z}$ takes a value that is less than $z$: $F_\mathcal{Z}(z) = P(\mathcal{Z} < z)$.
Since as $z \rightarrow \infty$ all values must be less than $z$, $F_\mathcal{Z}(z) \rightarrow 1$, 
and we can write $F_\mathcal{Z}(z) = 1 - P(\mathcal{Z} \geq z)$.
Recall that the PDF is equal to the derivative of the CDF: $f_\mathcal{Z} = \dfrac{d F_\mathcal{Z}}{dz}$.

\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{/Users/ccahilla//Git/thesis/chapters/probability_distributions/figures/illustrations_of_the_limits_of_integration.pdf}
    \caption{
        Illustrations of the limits of integration for Eqs. \ref{eq:part_z_less_than_zero}, \ref{eq:z_geq_zero_part_one}, and \ref{eq:z_geq_zero_part_two}.
    }
    \label{fig:histograms}
\end{figure}

\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{/Users/ccahilla/Git/thesis/chapters/probability_distributions/figures/product_gaussian_random_variables/test_exponential_and_laplace_sum_histograms_1000000.pdf}
    \caption{
        Plot of 1000000 samples from a exponential distribution $f_\mathcal{X}$, laplace distribution $f_\mathcal{Y}$, and their sum $f_\mathcal{Z}$.
        In this example, $\lambda = 32$ and $\gamma = 24$.
    }
    \label{fig:histograms}
\end{figure}

We split the solution into parts.  
The first part is finding $F_\mathcal{Z}(z)$ for $z < 0$.
The only way for $z = x + y < 0$ to be less than zero is if $y < -x$, because $x \in [0, \infty)$.
Also, since we know $y < 0$, we can write $|y| = -y$ for the joint probability distribution.
Thus we have
\begin{align}
    \label{eq:part_z_less_than_zero}
    \nonumber F_\mathcal{Z}(z<0) &= P(x + y < z) \\
    \nonumber &= P(y < z - x) \\
    \nonumber &= \int_{0}^{\infty} dx \int_{-\infty}^{z-x} dy\, f_{\mathcal{X}\mathcal{Y}}(x,y) \\
    \nonumber &= \dfrac{1}{2 \lambda \gamma} \int_{0}^{\infty} dx \int_{-\infty}^{z-x} dy\, e^{-\frac{x}{\lambda}} e^{\frac{y}{\gamma}} \\
    \nonumber &= \dfrac{ e^{\frac{z}{\gamma}} }{2 \lambda} \int_{0}^{\infty} dx e^{-\frac{x (\lambda + \gamma)}{\gamma \lambda}} \\
              &= \dfrac{ \gamma e^{\frac{z}{\gamma}} }{2 (\lambda + \gamma)} \qquad z \in [0, \infty)
\end{align}
This concludes the first part, $z < 0$.


The second part is finding $F_\mathcal{Z}(z)$ for $z > 0$.
\begin{align}
    \label{eq:part_z_greater_than_zero}
    \nonumber F_\mathcal{Z}(z > 0)  &= P(x + y < z) \\
    \nonumber          &= 1 - P(x + y > z) \\
    \nonumber          &= 1 - P(y > z - x)\\
                       &= 1 - \dfrac{1}{2 \lambda \gamma} \int_{0}^{\infty} dx \int_{z-x}^{\infty} dy\, e^{-\frac{x}{\lambda}} e^{\frac{|y|}{\gamma}} 
\end{align}
There are two ways for $z = x + y > 0$: $y < 0$ but $x > -y$, and simply $y > 0$.
The difficulty lies in the change in PDF $f_\mathcal{Y}$ at $y = 0$.
We will split this integral into two more parts: $x > z$ and $x < z$, at which point we will switch between PDFs.

First, let $x > z$.
Then we have the sum of two integrals, one with $f_\mathcal{Y}(y < 0)$ and the other with $f_\mathcal{Y}(y > 0)$:
\begin{align}
    \label{eq:z_geq_zero_part_one}
    \nonumber P(y > z - x | x > z) 
    \nonumber &= \dfrac{1}{2 \lambda \gamma} \int_{z}^{\infty} dx \int_{z-x}^{\infty} dy\, e^{-\frac{x}{\lambda}} e^{\frac{|y|}{\gamma}} \\
    \nonumber &= \dfrac{1}{2 \lambda \gamma} \int_{z}^{\infty} dx \left[ \int_{z-x}^{0} dy\, e^{-\frac{x}{\lambda}} e^{\frac{y}{\gamma}} 
    + \int_{0}^{\infty} dy\, e^{-\frac{x}{\lambda}} e^{-\frac{y}{\gamma}} \right] \\
    \nonumber &= \dfrac{1}{2 \lambda } \int_{z}^{\infty} dx \left[ 
        e^{-x \left(\frac{1}{\gamma }+\frac{1}{\lambda }\right)} \left(e^{x/\gamma }-e^{z/\gamma }\right)
        + e^{-\frac{x}{\lambda }}
        \right] \\
    \nonumber &= \dfrac{1}{2 \lambda } \int_{z}^{\infty} dx \left[ 
        e^{-x \left(\frac{1}{\gamma }+\frac{1}{\lambda }\right)} \left(2 e^{x/\gamma }-e^{z/\gamma }\right) 
        \right] \\
    &= \dfrac{(\gamma +2 \lambda ) e^{-\frac{z}{\lambda }}}{2 (\gamma +\lambda )}
\end{align}

Second, let $x < z$.
Now for $z > x + y$, $y > 0$, so we can just consider $f_\mathcal{Y}(y > 0)$:
\begin{align}
    \label{eq:z_geq_zero_part_two}
    \nonumber P(y > z - x | x < z) 
    \nonumber &= \dfrac{1}{2 \lambda \gamma} \int_{0}^{z} dx \int_{z-x}^{\infty} dy\, e^{-\frac{x}{\lambda}} e^{\frac{|y|}{\gamma}} \\
    \nonumber &= \dfrac{1}{2 \lambda \gamma} \int_{0}^{z} dx \int_{z-x}^{\infty} dy\, e^{-\frac{x}{\lambda}} e^{-\frac{y}{\gamma}} \\
    \nonumber &= \dfrac{1}{2 \lambda } \int_{0}^{z} dx e^{\frac{x}{\gamma }-\frac{x}{\lambda }-\frac{z}{\gamma }} \\
    &= \dfrac{\gamma  \left(e^{-\frac{z}{\gamma }}-e^{-\frac{z}{\lambda }}\right)}{2 (\gamma -\lambda )}
\end{align}

Summing Eqs. \ref{eq:z_geq_zero_part_one} and \ref{eq:z_geq_zero_part_two}:
\begin{align}
    \label{eq:z_geq_zero}
    \nonumber F_\mathcal{Z}(z>0)  &= 1 - P(y > z - x) \\
    \nonumber &= 1 - P(y > z - x | x > z) + P(y > z - x | x < z) \\
    \nonumber &= 1 - \dfrac{(\gamma +2 \lambda ) e^{-\frac{z}{\lambda }}}{2 (\gamma +\lambda )} 
        - \dfrac{\gamma  \left(e^{-\frac{z}{\gamma }}-e^{-\frac{z}{\lambda }}\right)}{2 (\gamma -\lambda )} \\
    &= 1 + \dfrac{\lambda ^2 e^{-\frac{z}{\lambda }}}{\gamma ^2-\lambda ^2}-\dfrac{\gamma  e^{-\frac{z}{\gamma }}}{2 (\gamma -\lambda )} \qquad z \in [0, \infty]
\end{align}
This concludes the second part, $z > 0$.

Thus the total CDF is given by 
\begin{align}
    \label{eq:CDF}
    F_\mathcal{Z}(z) = \left\{
        \begin{array}{ll}
            \dfrac{ \gamma e^{\frac{z}{\gamma}} }{2 (\lambda + \gamma)} & \qquad z < 0 \\
            1 + \dfrac{\lambda ^2 e^{-\frac{z}{\lambda }}}{\gamma ^2-\lambda ^2} - \dfrac{\gamma  e^{-\frac{z}{\gamma }}}{2 (\gamma -\lambda )} & \qquad z \geq 0
        \end{array}
    \right.
\end{align}

Taking the derivative of Eq \ref{eq:CDF} gives the PDF $f_\mathcal{Z}(z)$:
\begin{align}
    \label{eq:PDF}
    f_\mathcal{Z}(z) = \left\{
        \begin{array}{ll}
            \dfrac{ e^{\frac{z}{\gamma}} }{2 (\lambda + \gamma)} & \qquad z < 0 \\
            \dfrac{\lambda  e^{-\frac{z}{\lambda }}}{\lambda ^2-\gamma ^2} + \dfrac{e^{-\frac{z}{\gamma }}}{2 \gamma -2 \lambda } & \qquad z \geq 0
        \end{array}
    \right.
\end{align}
These formula can be seen as the brown and pink dashed curves in Figure \ref{fig:histograms}. 



\end{document}