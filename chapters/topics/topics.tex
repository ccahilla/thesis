\chapter{Topics in advanced interferometry}
\label{chap:topics}

This appendix will overview assorted small studies done prior to O3.
They are collected here because they represent relatively small, technically detailed techniques required for advanced interferometry.










\section{Calibration of radio-frequency photodetector response}
\label{sec:rfpd_calibration}

\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/topics/figures/rfpd_sensing_chain.pdf}
    \caption[Radio-frequency photodetector signal chain]{
        Radio-frequency photodetector signal chain.
    }
    \label{fig:radio_frequency_photodetector_signal_chain}
\end{figure}

Understanding the signal-chain response of the radio-frequency photodetectors (RFPDs) is important to an accurate calibration of the length error signals in Advanced LIGO.
The CARM, PRCL, SRCL, and MICH lengths all rely on Pound-Drever-Hall (PDH) locking, which detects the radio-frequency beatnote between the carrier and RF sidebands.
Good overviews of PDH locking are found in \cite{DriggersThesis} and \cite{HallThesis}.
The results here were used in the CARM calibration in Section~\ref{subsec:carm_calibration}.

\subsection{RF signal chain diagram}
\label{subsec:rf_signal_chain_diagram}

Figure~\ref{fig:radio_frequency_photodetector_signal_chain} shows a simplified schematic of the radio-frequency sensing chain electronics.
The full schematic is shown in \cite{RFPDschematic}.

The incoming light power has both an RF $P_{rf}$ and DC $P_{dc}$ component.
The RFPD senses and records both in two separate signal paths.

The power is converted to current by the photodiode, in this case wide-area InGaAs diodes \cite{ingaas_pd}.
The responsivity $R$ of the photodiode to light is 
\begin{align}
    \label{eq:responsivity_definition}
    R = \dfrac{e \lambda}{h c}
\end{align}
where $e$ is the electron charge,
$\lambda$ is the laser wavelength,
and $h$ is Planck's constant.
This assumes that each incident photon with energy $h c / \lambda$ excites a single electron in the biased semiconducting surface of the photodiode.
In reality, not all power incident on the photodiode is perfectly absorbed.  
This is captured by the quantum efficiency $\eta$, so that the DC current $I_{dc}$ produced by the incident power $P_{dc}$ is
\begin{align}
    \label{eq:dc_power_to_dc_current}
    I_{dc} = \eta \dfrac{e \lambda}{h c} P_{dc}
\end{align}

Next, the RF current is converted to voltage early by a transimpedance circuit.
The gain of this circuit is referred to as a the transimpedance $T$.

Finally, the output is sent to an demodulation electronics board to bring the voltage signal down from RF to audio frequency \cite{DemodBoardschematic}.
Usually demodulation has a factor of $1/2$ associated with it as half of the signal goes to DC and the other half goes to twice the RF frequency $2 \Omega$.
However, the demod board has other gain factors associated with it, yielding a total demod gain of $D = 5.4$.

This yields the final audio frequency voltage $V_{af}$ carrying the signal at radio frequency $\Omega$.
The signal chain be written as the product of all of the above components.


\subsection{RFPD shot noise}
\label{subsec:rfpd_shot_noise}

Shot noise can be used to measure the sensing chain.
First, we must think about the how the shot noise will appear on for the RFPD \cite{AndoThesis}.
Shot noise is white over the full bandwidth of the detector.
For a DC current $I_{dc}$, the shot noise power spectral density is 
\begin{align}
    \label{eq:shot_noise_definition}
    S_{I}(\omega) = 2 e I_{dc} \, \left[ \mathrm{\dfrac{A^2}{Hz}} \right]
\end{align}

If the current is dominated by RF sidebands, then we have \textit{cyclostationary shot noise} \cite{Niebauer1991, Gray1993, FrickeThesis}, 
where shot noise is increased in one quadrature readout due to the cyclic nature of the RF power on the photodetector.
We will assume that the power on the RFPDs are roughly constant, i.e. that DC carrier ``junk light'' dominates the light incident on the detector.

The same level of shot noise from Eq.~\ref{eq:shot_noise_definition} appears at both audio sidebands on the RF sideband, i.e $S_I\Omega - \omega)$ and $S_I(\Omega + \omega)$.
When we demodulate at $\Omega$, we ``fold'' the noise from negative frequency $-\omega$ on top of the noise at $\omega$, leading to an additional factor of two of shot noise:
\begin{align}
    \label{eq:folded_shot_noise}
    S_{I}^{\Omega}(\omega) = 4 e I_{dc} \, \left[ \mathrm{\dfrac{A^2}{Hz}} \right]
\end{align}

Now, propagating the shot noise ASD $\sqrt{ S_{V}^{shot,\Omega}(\omega) }$ through the sensing chain block diagram in Figure~\ref{fig:radio_frequency_photodetector_signal_chain} to the output audio frequency,
and then substituting in DC power from Eq.~\ref{eq:dc_power_to_dc_current} yields
\begin{align}
    \label{eq:folded_voltage_referred_shot_noise}
    \sqrt{ S_{V}^{\Omega}(\omega) } &= 2 T D \sqrt{e I_{dc}} \\
                                    &= 2 T D \sqrt{ \eta \dfrac{e^2 \lambda}{h c} P_{dc} } \, \left[ \mathrm{\dfrac{V}{\sqrt{Hz}}} \right]
\end{align}


\subsection{RFPD dark noise}
\label{subsec:rfpd_dark_noise}

The electronics that make up the photodetector and demod board have fundamental thermal noise associated their components as well, like Johnson noise.
This is known as ``dark noise'', because it is noise intrinsic to the photodetector even with no like on the photodiode.

In our case, the most likely source of noise is the current induced on the photodiode due to thermal noise.
This noise will also be flat in frequency, but will not depend on the incident power $P_{dc}$, only the temperature and electronics of the photodiode.

Because the dark noise is always there, it is equivalent to some constant level of light always incident on the photodiode producing shot noise.
We define this light level as $P_{dark}$, and add it to our RFPD shot noise equation:
\begin{align}
    \label{eq:folded_voltage_referred_shot_and_dark_noise}
    \sqrt{ S_{V}^{\Omega}(\omega) } &= 2 T D \sqrt{ \eta \dfrac{e^2 \lambda}{h c} (P_{dc} + P_{dark}) } \, \left[ \mathrm{\dfrac{V}{\sqrt{Hz}}} \right]
\end{align}


\subsection{Shot noise calibration}
\label{subsec:shot_noise_calibration}

\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/topics/figures/refl_imc_photodetector_noise/all_lsc_refl_pds_dark_and_shot_noise_levels.pdf}
    \caption[Measured Hanford RFPD response]{
        Measured Hanford RFPD responses for dark noise and shot noise \cite{alog46483}.
        The dark noise equivalent power $P_{dark}$ is the amount of power on the PD required such that shot noise and dark noise contributions are equal.
    }
    \label{fig:measured_refl_pds_signal_chains}
\end{figure}

We can use shot noise from Eq.~\ref{eq:folded_voltage_referred_shot_and_dark_noise} to calibrate the RFPD sensing chain.
First, we measure the dark noise voltage output of an RFPD by shutting off the laser.
Then, we increase the laser power on the RFPD until the noise starts increasing, and measure the flat ASD at several light levels $P_{dc}$.
Finally, we fit Eq.~\ref{eq:folded_voltage_referred_shot_and_dark_noise} to our measured ASD levels.

For the fit, we assume we know the demod gain $D = 5.4$, and collect all unknowns in the effective transimpedance $T$.
Also fit is the $P_{dark}$ parameter, which is entirely determined by the dark noise measurement.

Figure~\ref{fig:measured_refl_pds_signal_chains} shows the shot noise calibration results for the 9~MHz REFL A and B PDs, and the 24~MHz IMC REFL PD.
These numbers were important for the calibration of CARM, see Section~\ref{subsec:carm_calibration}.

IMC REFL previously had only $1~\mathrm{mW}$ of light on it in full lock, which was less than the equivalent dark noise power,
meaning the IMC sensing noise was dominated by dark noise.
This was increased to around $9~\mathrm{mW}$, putting IMC REFL squarely in the shot-noise limited regime.










\section{DARM cavity beam profile}
\label{sec:darm_cavity_beam_profile}

In O3, the DARM optical plant exhibited an unexpectedly low DARM cavity pole (411~Hz) compared to design (457~Hz), see Figure~\ref{fig:darm_pole_vs_src_losses}.
In O1, the SRC exhibited some higher order mode-hopping when locking the dual-recycled Michelson interferometer \cite{Fulda2015}.
Also in O1, SRCL detuning was first measured via the DARM optical spring, and has been a problem in all runs since \cite{CalUncPaper, Sun2020, HallThesis}.

A low DARM pole is indicative of excessive SRC losses.
Excess SRC losses could be caused by mode mismatch between the arms cavity beams and the SRC beam.

The DARM cavity is made up of two coupled cavities, the 4~km arm cavity and the 56~m signal recycling cavity.
As the arm cavities achieve high power, thermal effects cause the cavity geometry to change.
In a case with high point absorbers on the ITMs, the cavity geometry can change significantly.

Below are the modeling efforts showing the high sensitivity of the SRC Gouy phase accumulation to the input beam parameter.
It quantifies how the SRC is particularly vulnerable to position and mirror radius of curvature changes.

A python3 based beam profile library, \href{https://pypi.org/project/beamtrace/}{\texttt{beamtrace}}, was used for these simulations.
A general overview of beam profile tracing is provided in Section~\ref{subsec:cavity_geometry}.



\subsection{Arm cavity geometric parameters}
\label{subsec:arm_cavity_geometric_parameters}

The design of the Advanced LIGO arm cavity geometry is a balancing act of several considerations.

The biggest concern for the arm cavity geometry design is the mirror size.
LIGO mirrors are 34~cm in diameter, so the beam radius at the mirror is set to be 6~cm to avoid excessive scatter losses.
The beam must be large to mitigate excessively high power density and absorption, and to minimize thermal noise as well.
However, a large beam radius $w$ also dramatically increases the hard mode radiation-pressure based torque on the test masses \cite{Sidles2006}.

The second concern for arm cavity geometry design is angular controls.
In initial LIGO, the arm cavity geometry was planar, with $R_\mathrm{ITM} = 7.4~\mathrm{km}$ and $R_\mathrm{ETM} = 14.6~\mathrm{km}$.
In Advanced LIGO, the arm cavity geometry is concentric, with $R_\mathrm{ITM} = 1.940~\mathrm{km}$, and the ETM $R_\mathrm{ETM} = 2.247~\mathrm{km}$.
This change was largely due to help handle the higher expected radiation-pressure based torque from the higher circulating arm power.
Although an statically unstable torsional mode is always present in the interferometer at high resonating power, 
there is a choice of which mode is unstable via the cavity geometry.
For Advanced LIGO, the hard mode degrees of freedom were chosen to be stable due to its much larger torsional constant $\kappa$,
while the soft mode degrees of freedom were chosen to be unstable \cite{Sidles2006}. 

When a mirror in a concentric resonator is misaligned, the cavity axis will mostly tilt, i.e. displace in the hard mode.
When a mirror in a planar resonator is misaligned, the cavity axis will most shift, i.e. displace in the soft mode.
Because soft modes are statically unstable for high arm power, it is preferable to have concentric cavities where misalignment causes self-restoring hard mode motion.

Table~\ref{tab:arm_cavity_geometric_parameters} shows the arm cavity geometric parameters.
Figure~\ref{fig:arm_cavity_beamtrace} plots the expected fundamental Gaussian beam profile inside the arm cavity.

% Arm cavity geometric parameters
\begin{table}

    \begin{center}
    \caption[Arm cavity geometric parameters]{
        Table of the Advanced LIGO arm cavity geometric parameters.
        The arm cavity beam profile is plotted in Figure~\ref{fig:arm_cavity_beamtrace}. 
    } 
    \label{tab:arm_cavity_geometric_parameters}

    \def\arraystretch{1.5}
        \begin{tabular}{ c c c c }
            Parameters                      & Variable              & Value                             & Units \\ 
            \hline
            Arm length                      & $L$                   & $3.9944$                          & km \\  
            ITM radius of curvature         & $R_\mathrm{ITM}$      & $1.940$                           & km \\
            ETM radius of curvature         & $R_\mathrm{ETM}$      & $2.247$                           & km \\
            ITM $g$-factor                  & $g_1$                 & $-1.06$                           & - \\
            ETM $g$-factor                  & $g_2$                 & $-0.78$                           & - \\
            Cavity $g$-factor               & $g_1 g_2$             & $0.82$                            & - \\
            Beam waist location             & $z_0$                 & $1.833$                           & km \\
            Beam waist                      & $w_0$                 & $1.22$                            & cm \\
            ITM beam radius                 & $w_1$                 & $5.25$                            & cm \\
            ETM beam radius                 & $w_2$                 & $6.12$                            & cm \\
            Round-trip Gouy phase           & $\varphi$             & $310$                             & deg \\
            Free spectral range             & $\nu_\mathrm{FSR}$    & $37.5$                            & kHz \\
            Transverse mode spacing         & $\nu_\mathrm{TMS}$    & $32.3$                            & kHz \\
            ABCD matrix                     & 
            $\begin{bmatrix}
                A & B \\
                C & D
            \end{bmatrix}$ 
            &
            $\begin{bmatrix}
                -2.56               & -6.21 \times 10^3 \\
                1.74 \times 10^{-3} &  3.85
            \end{bmatrix}$ 
            & - \\
        
    \end{tabular}


    \end{center}
\end{table}


% LIGO arm cavity plot
\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/topics/figures/arm_cavity_beamtrace/arm_cavity_beamtrace.pdf}
    \caption[Advanced LIGO arm cavity beam profile]{
        The Advanced LIGO arm cavity transverse beam profile.
        The top plot shows the beam waist at any given point along the cavity axis $z$.
        The bottom plot shows the single-pass accumulated Gouy phase along the cavity axis.
        The arrows denote the beam waist, the region of highest beam intensity.
        Table~\ref{tab:arm_cavity_geometric_parameters} shows the arm cavity geometric parameters.
        This plot was made using the python-based \href{https://pypi.org/project/beamtrace/}{\texttt{beamtrace}} library.
    }
    \label{fig:arm_cavity_beamtrace}
\end{figure}



\subsection{Signal recycling cavity geometric parameters}
\label{subsec:signal_recycling_cavity_geometric_parameters}

The signal recycling cavity (SRC) is a folded convex-concave cavity, 
with three convex mirrors (ITMY, SR2, SRM),
and one strongly concave mirror (SR3).

The SRC is folded to bring the large arm cavity beam down to a manageable size (2~mm) at the antisymmetric port. 
The designed SRC Gouy phase accumulation is 19 degrees \cite{Arain2012}.
The model defined here gives a 18 degree Gouy phase accumulation.
Measurements at LIGO Hanford suggest a much higher SRC Gouy phase, 25 degrees \cite{alog52639, alog52658}.

The SRC is designed to accept the beam from the arm cavities. 
If the cavity Gouy phase is badly off, its possible the arm to SRC mode mismatch was high.

The Gouy phase discrepancy between measurement and model is likely due to small perturbations in the as-built cavity geometry versus the design.
Figure~\ref{fig:signal_recycling_cavity_beamtrace} plots the expected fundamental Gaussian beam profile inside the signal recycling cavity.
The Gouy phase is very dynamic at the end of the single-pass of the beam.
This is due to the fact that the cavity eigenmode beam waist occurs very near the SRM reflective face.
Gouy phase accumulates very fast near the beam waist, leading to the high uncertainty in overall SRC Gouy phase.

Table~\ref{tab:signal_recycling_cavity_geometric_parameters} reports the signal recycling cavity geometric parameters.
% Figure~\ref{fig:signal_recycling_cavity_beamtrace} plots the expected fundamental Gaussian beam profile inside the signal recycling cavity.


% Signal recycling cavity geometric parameters
\begin{table}

    \begin{center}
    \caption[Signal recycling cavity geometric parameters]{
        Table of the Advanced LIGO signal recycling cavity geometric parameters.
        The path from ITMY to SRM is used because it doesn't require two transmissions through the beamsplitter substrate.
        The beam waist occurs very near the SRM surface.
        This leads to to a highly variable Gouy phase under cavity parameter uncertainty.
        The signal recycling cavity beam profile is plotted in Figure~\ref{fig:signal_recycling_cavity_beamtrace}. 
    } 
    \label{tab:signal_recycling_cavity_geometric_parameters}

    \def\arraystretch{1.5}
        \begin{tabular}{ c c c c }
            Parameters                      & Variable                  & Value                             & Units \\ 
            \hline
            Optic index of refraction       & $n$                       & $1.45$                            & - \\
            Lengths                         &                           &                                   &   \\
            ITMY thickness                  & $t_\mathrm{ITMY}$         & $0.2$                             & m \\  
            ITMYAR to BS                    & $L_\mathrm{ITMYAR-BS}$    & $5.013$                           & m \\
            BS substrate                    & $t_\mathrm{BS}$           & $0.069$                           & m \\
            BSAR to SR3                     & $L_\mathrm{BSAR-SR3}$     & $19.37$                           & m \\
            SR3 to SR2                      & $L_\mathrm{SR3-SR2}$      & $15.46$                           & m \\
            SR2 to SRM                      & $L_\mathrm{SR2-SRM}$      & $15.74$                           & m \\
            Radii of curvature              &                           &                                   &   \\
            ITM                             & $R_\mathrm{ITM}$          & $-1940$                           & m \\
            BS                              & $R_\mathrm{BS}$           & $\infty$                          & m \\
            SR3                             & $R_\mathrm{SR3}$          & $36.013$                          & m \\
            SR2                             & $R_\mathrm{SR2}$          & $-6.424$                          & m \\
            SRM                             & $R_\mathrm{SRM}$          & $-5.678$                          & m \\
            ITMY lens                       & $f_\mathrm{ITMY}$         & $34.5$                            & km \\
            Beam waist                      & $w_0$                     & $2.0$                             & mm \\
            ITM beam radius                 & $w_1$                     & $51.75$                           & mm \\
            SRM beam radius                 & $w_2$                     & $2.0$                             & mm \\
            Round-trip Gouy phase           & $\varphi$                 & $40.4$                            & deg \\
            Free spectral range             & $\nu_\mathrm{FSR}$        & $2.683$                           & MHz \\
            Transverse mode spacing         & $\nu_\mathrm{TMS}$        & $0.301$                           & MHz \\
            ABCD matrix                     & 
            $\begin{bmatrix}
                A & B \\
                C & D
            \end{bmatrix}$ 
            &
            $\begin{bmatrix}
                -3.07                   &  5.13 \times 10^3 \\
                -2.95 \times 10^{-3}    &  4.59
            \end{bmatrix}$ 
            & - \\
        
    \end{tabular}


    \end{center}
\end{table}


% LIGO SRC cavity plot
\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/topics/figures/signal_recycling_beam_trace/signal_recycling_cavity_beam_trace.pdf}
    \caption[Advanced LIGO signal recycling cavity beam profile]{
        The Advanced LIGO signal recycling cavity transverse beam profile.
        The top plot shows the beam waist at any given point along the cavity axis $z$.
        The bottom plot shows the single-pass accumulated Gouy phase along the cavity axis.
        The beam waist occurs very near the SRM face.
        Therefore, the Gouy phase accumulation rate there is high.
        This makes the SRC Gouy phase very sensitive to cavity parameter uncertainty.
        This plot was made using the python-based \href{https://pypi.org/project/beamtrace/}{\texttt{beamtrace}} library.
    }
    \label{fig:signal_recycling_cavity_beamtrace}
\end{figure}



% \subsection{Signal recycling cavity parameter sweeps}
% \label{subsec:signal_recycling_cavity_parameter_sweeps}

Figures~\ref{fig:optics_move_vs_single_pass_src_gouy_phase} and \ref{fig:optics_change_roc_vs_single_pass_src_gouy_phase} show how 
the SRC Gouy phase model changes with small changes to the nominal parameters defined in Table~\ref{tab:signal_recycling_cavity_geometric_parameters}.
Also plotted are the actual measurements of the SRC Gouy phase from \cite{alog52639, alog52658}.

Most likely, the cavity lengths are built very slightly different than designed.
Figure~\ref{fig:optics_move_vs_single_pass_src_gouy_phase} illustrates the effect of optic moves on the Gouy phase.
Displacement of SR2 and SR3 by around $7~\mathrm{mm}$ backward along the cavity axis would approximately match the measured Gouy phase from \cite{alog52658}.
Displacement of ITMY or SRM does not significantly impact the beam parameter.

There are two measurements of the SRC Gouy phase, one with the SR3 disk heater off ($25.3~\mathrm{degs}$) and one with it on at $4~\mathrm{W}$ ($29.0~\mathrm{degs}$).
The SR3 disk heater is designed to heat the SR3 mirror to adjust the beam parameter resonant in the SRC,
such that it is better matched with the beam parameter coming in from the arms.
The SR3 disk heater is has been measured to move the SR3 radius of curvature by $-3.05 \pm 0.15~\mathrm{mm}/\mathrm{Watt}$ \cite{alog27262}.
Thus, the SR3 radius of curvature should move by around $-12~\mathrm{mm}$ when on at $4~\mathrm{W}$.
These measurements are consistent with the analysis in Figure~\ref{fig:optics_change_roc_vs_single_pass_src_gouy_phase}.

% Optics move vs SRC Gouy Phase
\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/topics/figures/sample_SRC_gouy_phase/optics_move_vs_single_pass_src_gouy_phase.pdf}
    \caption[Displaced SRC optics vs SRC Gouy phase]{
        Single-pass SRC Gouy phase $\varphi$ when displacing SRC optics along the cavity axis $z$.
    }
    \label{fig:optics_move_vs_single_pass_src_gouy_phase}
\end{figure}


% Optics RoC change vs SRC Gouy Phase
\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/topics/figures/sample_SRC_gouy_phase/optics_change_roc_vs_single_pass_src_gouy_phase.pdf}
    \caption[SRC optics radius of curvature vs SRC Gouy phase]{
        Single-pass SRC Gouy phase $\varphi$ when changing the radii of curvature of SRC optics.
    }
    \label{fig:optics_change_roc_vs_single_pass_src_gouy_phase}
\end{figure}









% \section{Contrast defect measurement}
% \label{sec:contrast_defect_measurement}