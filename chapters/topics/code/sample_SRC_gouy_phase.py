'''
Samples the possible parameters for the SRC given the LHO SRC gouy phase measurements.

LHO SRC Gouy Phase Measurement 1: https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=52658
LHO SRC Gouy Phase Measurement 2: https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=52639
LLO SR3 Disk Heater Radius of Curvature per Watt of Heating Calibration : https://alog.ligo-la.caltech.edu/aLOG/index.php?callRep=27262

SRC Gouy Phase with SR3 Disk at 4W = 28.97deg +-0.11deg (statistical) +-0.43deg (systematic).
SRC Gouy Phase with SR3 Disk at 0W = 25.25deg +-0.06deg (statistical) +-1.8deg (systematic).
Approx calibration of SR3 Disk heater = -3.05 +/- 0.15 mm/W

Craig Cahillane
October 25, 2019
'''
import os
import copy
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

import scipy.constants as scc
import scipy.special as scp

# import emcee
# import corner

import beamtrace
from beamtrace.tracer import BeamTrace

mpl.rcParams.update({'text.usetex': True,
                     'figure.figsize': (12, 9),
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 16,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 12,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])


### Initial SRC parameters
# Finesse numbers native to the aLIGO design kat file.
# Fulda numbers from T1500230
# Multiply the substrate thicknesses by index of refraction to get the proper
# effective path length for the total length caclulations
###
ITMX_params = {
    "absorption": 2.1e-7,
    #"nominal_roc_D": abs(1/base.ITMX.Rc.value),
    "lens_D_per_absorb_W": 4.87e-4,
    "lens_D_per_RH_W": -9e-6,
    "lens_D_per_CO2_W": 2.5e-5,
    "roc_D_per_RH_W": 10e-7,
    "roc_D_per_absorb_W": -3.6e-5,
    "substrate_lens": 1/304989 #T1300954
}

ITMY_params = {
    "absorption": 2.1e-7,
    #"nominal_roc_D": abs(1/base.ITMX.Rc.value),
    "lens_D_per_absorb_W": 4.87e-4,
    "lens_D_per_RH_W": -9e-6,
    "lens_D_per_CO2_W": 2.5e-5,
    "roc_D_per_RH_W": 10e-7,
    "roc_D_per_absorb_W": -3.6e-5,
    "substrate_lens": 1/(-82424) #T1300954
}

ITMY_RH_watts = 1.39 * 2
ITMX_RH_watts = 0.49 * 2

CO2Y_watts = 0.9
CO2X_watts = 0.8

SR3_disk_heater = np.array([0, 4.0])
SR3_disk_heater_calibration = -3.05e-3 # +- 0.15 meters of radius of Curvature per watt of heating

nn = 1.45 # index of refraction of silica

# Radii of Curvature
RoC_SRM  = -5.678 # from galaxy #-5.7150 Fulda # m = 0.0212 m Finesse - Fulda
RoC_SR2  = -6.4240 # m = 0.0180 m Finesse - Fulda
RoC_SR3  = 36.0130 # m = -0.0522 m Finesse - Fulda
RoC_ITMY = -1939.3900 #+ ITMY_params['roc_D_per_RH_W'] * ITMY_RH_watts )**(-1) # m Finesse - Fulda
RoC_ITMX = -1939.3900 #+ ITMX_params['roc_D_per_RH_W'] * ITMX_RH_watts )**(-1)

# ITM lens calculated from 18 W on PRM
f_ITMY = 34500.0000 # m = 0.0000 m Finesse - Fulda
f_ITMX = 34500.0000 # m = 0.0000 m Finesse - Fulda
f_CPY = np.inf # 1/(ITMY_params['lens_D_per_CO2_W'] * CO2Y_watts)
f_CPX = np.inf # 1/(ITMX_params['lens_D_per_CO2_W'] * CO2X_watts)

# Common lengths
ll_SRM_SR2   = 15.7400 #+ 5e-3 # m 
ll_SR2_SR3   = 15.4601 #+ 5e-3 # m
ll_SR3_BSAR  = 19.3661 # m = 0.0000 m Finesse - Fulda
ll_BSsub     = 68.5e-3 # m = 0.0002 m Finesse - Fulda
ll_SRC_common = ll_SRM_SR2 + ll_SR2_SR3 + ll_SR3_BSAR + ll_BSsub * nn

# SRC Y only
ll_BSHR_CPY   = 4.847  # m
ll_CPYsub     = 100e-3 # m
ll_CPY_ITMYAR = 20e-3  # m
ll_ITMYsub    = 200e-3 # m = 0.0000 m Finesse - Fulda
ll_SRC_Y_only = ll_BSHR_CPY + ll_CPYsub * nn + ll_CPY_ITMYAR + ll_ITMYsub * nn

# SRC X only, be sure to include BSHR->BSAR for the second time
ll_BSAR_CPX   = 4.829  # m
ll_CPXsub     = 100e-3 # m
ll_CPX_ITMXAR = 20e-3  # m
ll_ITMXsub    = 200e-3  # m = 0.0000 m Finesse - Fulda
ll_SRC_X_only = ll_BSsub * nn + ll_BSAR_CPX + ll_CPXsub * nn + ll_CPX_ITMXAR + ll_ITMXsub * nn

# Total SRC lengths
ll_SRCY = ll_SRC_common + ll_SRC_Y_only
ll_SRCX = ll_SRC_common + ll_SRC_X_only
ll_SRC = np.mean([ll_SRCY, ll_SRCX])

### Make a dictionary of nominal values to quickly create SRC models ###
SRC_dict = {}
arms = np.array(['X', 'Y'])
for arm in arms:
    SRC_dict[arm] = {}
    SRC_dict[arm]['index_of_refraction'] = nn

    SRC_dict[arm]['RoC_SRM'] = RoC_SRM
    SRC_dict[arm]['RoC_SR2'] = RoC_SR2
    SRC_dict[arm]['RoC_SR3'] = RoC_SR3

    SRC_dict[arm]['ll_SRM_SR2'] = ll_SRM_SR2
    SRC_dict[arm]['ll_SR2_SR3'] = ll_SR2_SR3
    SRC_dict[arm]['ll_SR3_BSAR'] = ll_SR3_BSAR
    SRC_dict[arm]['ll_BSsub'] = ll_BSsub

    SRC_dict[arm]['SR3_disk_heater'] = SR3_disk_heater
    SRC_dict[arm]['SR3_RoC_m_per_W'] = SR3_disk_heater_calibration

SRC_dict['Y']['RoC_ITM'] = RoC_ITMY
SRC_dict['Y']['f_ITM'] = f_ITMY
SRC_dict['Y']['f_CP'] = f_CPY

SRC_dict['Y']['ll_BS_CP'] = ll_BSHR_CPY
SRC_dict['Y']['ll_CPsub'] = ll_CPYsub
SRC_dict['Y']['ll_CP_ITM_AR'] = ll_CPY_ITMYAR
SRC_dict['Y']['ll_ITMsub'] = ll_ITMYsub

SRC_dict['X']['RoC_ITM'] = RoC_ITMX
SRC_dict['X']['f_ITM'] = f_ITMX
SRC_dict['X']['f_CP'] = f_CPX

SRC_dict['X']['ll_BS_CP'] = ll_BSAR_CPX
SRC_dict['X']['ll_CPsub'] = ll_CPXsub
SRC_dict['X']['ll_CP_ITM_AR'] = ll_CPX_ITMXAR
SRC_dict['X']['ll_ITMsub'] = ll_ITMXsub

#SRC Gouy Phase with SR3 Disk at 0W = 25.25 #deg +-0.06deg (statistical) +-1.8deg (systematic).
#SRC Gouy Phase with SR3 Disk at 4W = 28.97 #deg +-0.11deg (statistical) +-0.43deg (systematic).
data = np.array([25.25, 28.97])
data_unc = np.array([
np.sqrt(0.06**2 + 1.8**2),
np.sqrt(0.11**2 + 0.43**2)
])
data_labels = np.array([
    'Measured SRC Gouy Phase with SR3 Disk at 0W',
    'Measured SRC Gouy Phase with SR3 Disk at 4W',
])
data_colors = np.array(['C5', 'C6'])

print()
print('Length SRC common path = {:.3f} m'.format(ll_SRC_common))
print('Length SRC Y = {:.3f} m'.format(ll_SRCY))
print('Length SRC X = {:.3f} m'.format(ll_SRCX))
print('Schnupp Asymmetry = {:.3f} cm'.format(1e2 * (ll_SRCX - ll_SRCY)))
print('Average SRC length = {:.3f} m'.format(ll_SRC))
print()

def SRC_model(SRC_dict, arm='Y', name=None):
    '''SRC beam trace model
    Inputs are a dictionary of parameter values, and an arm choice
    Returns BeamTrace class with your input parameters
    '''
    SRC_ABCD = BeamTrace()

    nn = SRC_dict[arm]['index_of_refraction']

    SRC_ABCD.add_mirror(SRC_dict[arm]['RoC_ITM'], index_of_refraction=nn, name='ITM HR')
    SRC_ABCD.add_space(SRC_dict[arm]['ll_ITMsub'], index_of_refraction=nn, name='ITM substrate')
    SRC_ABCD.add_lens(SRC_dict[arm]['f_ITM'], name='ITM AR')
    SRC_ABCD.add_space(SRC_dict[arm]['ll_CP_ITM_AR'], name='L CP to ITM AR')
    SRC_ABCD.add_space(SRC_dict[arm]['ll_CPsub'], index_of_refraction=nn, name='CP substrate')
    # SRC_ABCD.add_lens(f_CP, name='CP lens')

    if arm == 'Y':
        SRC_ABCD.add_space(SRC_dict[arm]['ll_BS_CP'], name='L BSHR to CPY')
        SRC_ABCD.add_lens(np.inf, name='BS HR')
    else:
        # SRC_ABCD.add_lens(f_CPX, name='CPX lens')
        SRC_ABCD.add_space(SRC_dict[arm]['ll_BS_CP'], name='L BSAR to CPX')
        SRC_ABCD.add_lens(np.inf, name='BS AR 2')
        SRC_ABCD.add_space(SRC_dict[arm]['ll_BSsub'], index_of_refraction=nn, name='BS substrate 2')
        SRC_ABCD.add_mirror(np.inf, name='BS HR')

    SRC_ABCD.add_space(SRC_dict[arm]['ll_BSsub'], index_of_refraction=nn, name='BS substrate')
    SRC_ABCD.add_lens(np.inf, name='BS AR')
    SRC_ABCD.add_space(SRC_dict[arm]['ll_SR3_BSAR'], name='L SR3 to BSAR')
    SRC_ABCD.add_mirror(SRC_dict[arm]['RoC_SR3'], name='SR3')
    SRC_ABCD.add_space(SRC_dict[arm]['ll_SR2_SR3'], name='L SR2 to SR3')
    SRC_ABCD.add_mirror(SRC_dict[arm]['RoC_SR2'], name='SR2')
    SRC_ABCD.add_space(SRC_dict[arm]['ll_SRM_SR2'], name='L SRM to SR2')
    SRC_ABCD.add_mirror(SRC_dict[arm]['RoC_SRM'], name='SRM')

    return SRC_ABCD

def log_likelihood(theta, fff, meas, unc):
    RoC_SR3, ll_SRM_SR2, ll_SR2_SR3, SR3_disk_heater_calibration = theta

    E_LO = E_LO0
    TS = T_SRM0
    TI = T_ITM0
    TE = T_ETM0
    l_SRC = l_SRC0
    model = ward_model(fff, phi, zeta, E_LO0, L, l_SRC, TS, TI, TE, Pbs, arm_loss, SRC_loss, post_SRM_loss)

    if  phi < -np.pi/2 or phi > 3*np.pi/2 or \
        zeta < -np.pi or zeta > np.pi or \
        Pbs < 1420*0.9 or Pbs > 1420*1.3 or \
        arm_loss < 0 or arm_loss > 500e-6 or \
        SRC_loss < 0 or SRC_loss > 1 or \
        post_SRM_loss < 0.0 or post_SRM_loss > 1.0:
        log_like = -np.inf
    else:
        sigma = np.abs(meas) * unc * 100
        err = -0.5 * np.sum( np.abs((meas - model)/sigma)**2 )

        # magErr   = -np.sum( (np.log(np.abs( meas/model )))**2 ) #/(2*measUncs**2) )
        # phaseErr = -np.sum( (     np.angle( meas/model ) )**2 ) #/(2*measUncs**2) )

        log_like = err

    return log_like

### SRC Y and X nominal models ###
SRCY_ABCD_nom = SRC_model(SRC_dict, arm='Y', name='SRCY ABCD Nominal')
SRCX_ABCD_nom = SRC_model(SRC_dict, arm='X', name='SRCX ABCD Nominal')

### Move some optics by a +- 1cm ###
xx = np.linspace(-10e-3, 20e-3, 100)
optics = np.array(['SRM', 'SR2', 'SR3'])
move_dict = {}
arm = 'Y'
move_dict[arm] = {}
for optic in optics:
    print()
    print('Moving {}'.format(optic))

    temp_SRC_dict = copy.deepcopy(SRC_dict)
    temp_gouy_phase = np.array([])

    for dx in xx:
        for key in SRC_dict[arm].keys(): # add dx length to all elements required
            if 'll' in key and optic in key:
                temp_SRC_dict[arm][key] = SRC_dict[arm][key] + dx
                if dx == xx[0]:
                    print('Adding to {}'.format(key))
                    print('Nominal {} = {}'.format(key, SRC_dict[arm][key]))
                    print('New {} = {}'.format(key, temp_SRC_dict[arm][key]))
        SRC_ABCD = SRC_model(temp_SRC_dict, arm=arm, name='SRC ABCD')
        SRC_ABCD.calculate_cavity_ABCD()
        temp_gouy_phase = np.append(temp_gouy_phase, SRC_ABCD.get_total_cavity_gouy_phase())
    move_dict[arm][optic] = temp_gouy_phase

### Move some radii of curvature by a +- 1cm ###
rr = np.linspace(-30e-3, 5e-3, 100)
rr_optics = np.array(['SRM', 'SR2', 'SR3', 'ITM'])
radii_dict = {}
arm = 'Y'
radii_dict[arm] = {}
for optic in rr_optics:
    print()
    print('Moving {}'.format(optic))

    temp_SRC_dict = copy.deepcopy(SRC_dict)
    temp_gouy_phase = np.array([])

    for dr in rr:
        for key in SRC_dict[arm].keys(): # add dr length to all elements required
            if 'RoC' in key and optic in key:
                temp_SRC_dict[arm][key] = SRC_dict[arm][key] + dr
                if dr == rr[0]:
                    print('Adding to {}'.format(key))
                    print('Nominal {} = {}'.format(key, SRC_dict[arm][key]))
                    print('New {} = {}'.format(key, temp_SRC_dict[arm][key]))
        SRC_ABCD = SRC_model(temp_SRC_dict, arm=arm, name='SRC ABCD')
        SRC_ABCD.calculate_cavity_ABCD()
        temp_gouy_phase = np.append(temp_gouy_phase, SRC_ABCD.get_total_cavity_gouy_phase())
    radii_dict[arm][optic] = temp_gouy_phase

### Calculate some basic facts ###
print()
print('SRCY_ABCD_nom')
# SRCY_ABCD_nom.print()
SRCY_ABCD_nom.calculate_cavity_ABCD()
print('SRY ABCD \n{}\n'.format(SRCY_ABCD_nom.ABCD))
print('SRY Accumulated Gouy Phase = {} deg'.format(SRCY_ABCD_nom.get_total_cavity_gouy_phase()))

print()
print('SRCX_ABCD_nom')
# SRCX_ABCD_nom.print()
SRCX_ABCD_nom.calculate_cavity_ABCD()
print('SRX ABCD \n{}\n'.format(SRCX_ABCD_nom.ABCD))
print('SRX Accumulated Gouy Phase = {} deg'.format(SRCX_ABCD_nom.get_total_cavity_gouy_phase()))

### Get cavity scans ###
round_trip = False
if False:
    zzx, wwx, gouyx, qqx = SRCX_ABCD_nom.scan_cavity(round_trip=round_trip)
    zzy, wwy, gouyy, qqy = SRCY_ABCD_nom.scan_cavity(round_trip=round_trip)
    print()
    print('SRX gouyx last data point = {} deg'.format(180/np.pi*gouyx[-1]))
    print('SRY gouyy last data point = {} deg'.format(180/np.pi*gouyy[-1]))



### Figures ###
# Nominal SRC gouy phase plot
fig = SRCX_ABCD_nom.plot_cavity_scan(round_trip=round_trip, label='SRC X')
fig = SRCY_ABCD_nom.plot_cavity_scan(round_trip=round_trip, label='SRC Y', plot_beam_waists=False, color='C0', ls='--', fig=fig)
fig.set_size_inches(12, 9)

s1, s2 = fig.get_axes()
s1.set_title('SRC Cavity Scans')
s1.legend().set_visible(False)

plot_name = 'nominal_src_gouy_phase_beamtrace.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()


# Plot gouy phase change as a function of length changes
fig, s1 = plt.subplots(1)

for optic in optics:
    temp_single_pass_gouy = move_dict[arm][optic]/2.0 # divide by two for single-pass
    s1.plot(xx * 1e3, temp_single_pass_gouy, label='{}'.format(optic))

for dp, du, dl, dc in zip(data, data_unc, data_labels, data_colors):
    s1.axhline(y=dp, color=dc, label=dl)
    s1.fill_between(xx * 1e3, y1=dp-du, y2=dp+du, color=dc, alpha=0.3, edgecolor=None, label='Uncertainty')

s1.set_xlim([min(xx * 1e3), max(xx * 1e3)])

# s1.set_title('Single-Pass SRC Gouy Phase when Moving Optics')
s1.set_xlabel('Length Change from Nominal Position [mm]')
s1.set_ylabel('Single-Pass SRC Gouy Phase [deg]')

s1.legend()
s1.grid()

plot_name = 'optics_move_vs_single_pass_src_gouy_phase.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Plot gouy phase change as a function of radii of curvature changes
fig, s1 = plt.subplots(1)

for optic in rr_optics:
    temp_single_pass_gouy = radii_dict[arm][optic]/2.0 # divide by two for single-pass
    s1.plot(rr * 1e3, temp_single_pass_gouy, label='{} Nominal RoC = {:.2f} m'.format(optic, SRC_dict[arm]['RoC_{}'.format(optic)]))

for dp, du, dl, dc in zip(data, data_unc, data_labels, data_colors):
    s1.axhline(y=dp, color=dc, label=dl)
    s1.fill_between(rr * 1e3, y1=dp-du, y2=dp+du, color=dc, alpha=0.3, edgecolor=None, label='Uncertainty')
    # s1.axhline(y=dp+du, color=dc, ls='--', label=dl)
    # s1.axhline(y=dp-du, color=dc, ls='--', label=dl)

s1.set_xlim([min(rr * 1e3), max(rr * 1e3)])

# s1.set_title('Single-Pass SRC Gouy Phase when Changing Radii of Curvature')
s1.set_xlabel('Radius of Curvature Change from Nominal [mm]')
s1.set_ylabel('Single-Pass SRC Gouy Phase [deg]')

s1.legend()
s1.grid()

plot_name = 'optics_change_roc_vs_single_pass_src_gouy_phase.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()


# End
command = 'gopen'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()
