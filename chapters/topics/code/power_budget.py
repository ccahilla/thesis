'''
power_budget.py

Uses the full model of the interferometer to estimate the arm power uncertainties.

Craig Cahillane
January 12, 2021
'''

#####   Imports   #####
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc

import copy

mpl.rcParams.update({   'figure.figsize': (12, 9),
                        'text.usetex': True,
                        'font.family': 'serif',
                        # 'font.serif': 'Georgia',
                        # 'mathtext.fontset': 'cm',
                        'lines.linewidth': 2.5,
                        'font.size': 22,
                        'xtick.labelsize': 'large',
                        'ytick.labelsize': 'large',
                        'legend.fancybox': True,
                        'legend.fontsize': 18,
                        'legend.framealpha': 0.9,
                        'legend.handletextpad': 0.5,
                        'legend.labelspacing': 0.2,
                        'legend.loc': 'best',
                        'legend.columnspacing': 2,
                        'savefig.dpi': 80,
                        'pdf.compression': 9})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Functions   #####
def estimate_arm_power(Pin, PRG, Garm):
    '''Return final arm power Parm from input power Pin, 
    power-recycling gain PRG, arm gain Garm. 

    Inputs:
    Pin     = nominal input power incident on back of PRM in watts
    PRG     = nominal power recycling gain calculated via ratio of light
              on POP with single arm lock and full IFO
    Garm    = arm power gain
    '''
    Parm = 0.5 * Pin * PRG * Garm
    return Parm

def estimate_arm_power_gain(Pin, PRG, Parm):
    '''Return final arm power Garm = garm^2 from input power Pin, 
    power-recycling gain PRG, and measured arm power Parm. 

    Inputs:
    Pin     = nominal input power incident on back of PRM in watts
    PRG     = nominal power recycling gain calculated via ratio of light
              on POP with single arm lock and full IFO
    Parm    = measured arm power from the SRCL measurement
    '''    
    Garm = 2 * Parm / (Pin * PRG)
    return Garm

def full_ifo_x_arm_electric_field(  k, Tbs, Tix, Tiy, Tex, Tey, Tp, Ts, phi_X, phi_Y, phi_x, phi_y, phi_P, phi_S, eta_bs, eta_xarm, eta_yarm, eta_x, eta_y, eta_p, eta_s):
    '''Returns the electric field in the x-arm.
    Takes in the lengths and reflectivities of each mirror in the full IFO.
    Inputs:
    k       = wavenumber 2pi/lambda0
    Tbs     = power tranmssion of beamsplitter
    Tix     = power tranmssion of ITMX
    Tiy     = power tranmssion of ITMY
    Tex     = power tranmssion of ETMX
    Tey     = power tranmssion of ETMY
    Tp      = power tranmssion of PRM
    Ts      = power tranmssion of SRM
    phi_X  = length of X-arm
    phi_Y  = length of Y-arm
    phi_x      = length of Michelson x-arm
    phi_y      = length of Michelson y-arm
    phi_P      = length of PRM to beamsplitter
    phi_S      = length of SRM to beamsplitter
    eta_bs  = power loss from reflection off beamsplitter
    eta_xarm = power loss from reflection off ETMX
    eta_yarm = power loss from reflection off ETMY
    eta_x   = power loss from reflection off ITMX
    eta_y   = power loss from reflection off ITMY
    eta_p   = power loss from reflection off PRM
    eta_s   = power loss from reflection of SRM

    Output:
    Earmx = complex. estimated X-arm electric field
    '''
    # Set up the inputs
    tBS = np.sqrt(Tbs)
    rBS = np.sqrt(1 - Tbs - eta_bs)
    tIX = np.sqrt(Tix)
    rIX = np.sqrt(1 - Tix - eta_x)
    tIY = np.sqrt(Tiy)
    rIY = np.sqrt(1 - Tiy - eta_y)
    tEX = np.sqrt(Tex)
    rEX = np.sqrt(1 - Tex - eta_xarm)
    tEY = np.sqrt(Tey)
    rEY = np.sqrt(1 - Tey - eta_yarm)    
    tP = np.sqrt(Tp)
    rP = np.sqrt(1 - Tp - eta_p)
    tS = np.sqrt(Ts)
    rS = np.sqrt(1 - Ts - eta_s)

    phi_C = phi_X + phi_Y
    phi_D = phi_X - phi_Y
    phi_q = phi_x + phi_y
    phi_m = phi_x - phi_y

    if  not isinstance(eta_bs, np.ndarray) \
        and not isinstance(eta_x, np.ndarray) \
        and not isinstance(eta_y, np.ndarray) \
        and not isinstance(eta_xarm, np.ndarray) \
        and not isinstance(eta_yarm, np.ndarray)\
        and not isinstance(eta_p, np.ndarray) \
        and not isinstance(eta_s, np.ndarray):
        print()
        print(f'tBS = {tBS}')
        print(f'rBS = {rBS}')
        print(f'tIX = {tIX}')
        print(f'rIX = {rIX}')
        print(f'tIY = {tIY}')
        print(f'rIY = {rIY}')
        print(f'tEX = {tEX}')
        print(f'rEX = {rEX}')
        print(f'tEY = {tEY}')
        print(f'rEY = {rEY}')
        print(f'tP  = {tP}')
        print(f'rP  = {rP}')
        print(f'tS  = {tS}')
        print(f'rS  = {rS}')
        print()
        print(f'phi_C = {phi_C}')
        print(f'phi_D = {phi_D}')
        print(f'phi_q = {phi_q}')
        print(f'phi_m = {phi_m}')
        print(f'phi_P = {phi_P}')
        print(f'phi_S = {phi_S}')
        print()

    # use the mathematica equation
    Exarm = -((tBS*tIX*tP*(-(rEY*rIY*np.exp(2*1j*phi_C)) + np.exp(2*1j*phi_D))*np.exp(1j*(phi_C + phi_D + 3*phi_m + phi_P + phi_q)))/(rEY*rIY*np.exp(2*1j*(phi_C + phi_m)) - np.exp(2*1j*(phi_D + phi_m)) - rEX*rEY*rIX*rIY*np.exp(2*1j*(2*phi_C + phi_D + phi_m)) + rEX*rIX*np.exp(2*1j*(phi_C + 2*phi_D + phi_m)) + rBS**2*rEY*rP*(rIY**2 + tIY**2)*np.exp(2*1j*(phi_C + phi_P + phi_q)) - rBS**2*rIY*rP*np.exp(2*1j*(phi_D + phi_P + phi_q)) - rBS**2*rEX*rEY*rIX*rP*(rIY**2 + tIY**2)*np.exp(2*1j*(2*phi_C + phi_D + phi_P + phi_q)) + rBS**2*rEX*rIX*rIY*rP*np.exp(2*1j*(phi_C + 2*phi_D + phi_P + phi_q)) + rEY*rIX*rIY*rP*tBS**2*np.exp(2*1j*(phi_C + 2*phi_m + phi_P + phi_q)) - rIX*rP*tBS**2*np.exp(2*1j*(phi_D + 2*phi_m + phi_P + phi_q)) - rEX*rEY*rIY*rP*tBS**2*(rIX**2 + tIX**2)*np.exp(2*1j*(2*phi_C + phi_D + 2*phi_m + phi_P + phi_q)) + rEX*rP*tBS**2*(rIX**2 + tIX**2)*np.exp(2*1j*(phi_C + 2*phi_D + 2*phi_m + phi_P + phi_q))))
    return Exarm

def full_ifo_y_arm_electric_field(  k, Tbs, Tix, Tiy, Tex, Tey, Tp, Ts, 
                                    phi_X, phi_Y, phi_x, phi_y, phi_P, phi_S, 
                                    eta_bs, eta_xarm, eta_yarm, eta_x, eta_y, eta_p, eta_s):
    '''Returns the electric field in the y-arm.
    Takes in the lengths and reflectivities of each mirror in the full IFO.
    Inputs:
    k       = wavenumber 2pi/lambda0
    Tbs     = power tranmssion of beamsplitter
    Tix     = power tranmssion of ITMX
    Tiy     = power tranmssion of ITMY
    Tex     = power tranmssion of ETMX
    Tey     = power tranmssion of ETMY
    Tp      = power tranmssion of PRM
    Ts      = power tranmssion of SRM
    phi_X  = length of X-arm
    phi_Y  = length of Y-arm
    phi_x      = length of Michelson x-arm
    phi_y      = length of Michelson y-arm
    phi_P      = length of PRM to beamsplitter
    phi_S      = length of SRM to beamsplitter
    eta_bs  = power loss from reflection off beamsplitter
    eta_xarm = power loss from reflection off ETMX
    eta_yarm = power loss from reflection off ETMY
    eta_x   = power loss from reflection off ITMX
    eta_y   = power loss from reflection off ITMY
    eta_p   = power loss from reflection off PRM
    eta_s   = power loss from reflection of SRM

    Output:
    Earmy = complex. estimated Y-arm electric field
    '''
    # Set up the inputs
    tBS = np.sqrt(Tbs)
    rBS = np.sqrt(1 - Tbs - eta_bs)
    tIX = np.sqrt(Tix)
    rIX = np.sqrt(1 - Tix - eta_x)
    tIY = np.sqrt(Tiy)
    rIY = np.sqrt(1 - Tiy - eta_y)
    tEX = np.sqrt(Tex)
    rEX = np.sqrt(1 - Tex - eta_xarm)
    tEY = np.sqrt(Tey)
    rEY = np.sqrt(1 - Tey - eta_yarm)    
    tP = np.sqrt(Tp)
    rP = np.sqrt(1 - Tp - eta_p)
    tS = np.sqrt(Ts)
    rS = np.sqrt(1 - Ts - eta_s)

    phi_C = phi_X + phi_Y
    phi_D = phi_X - phi_Y
    phi_q = phi_x + phi_y
    phi_m = phi_x - phi_y

    # use the mathematica equation
    Eyarm = (rBS*tIY*tP*(-1 + rEX*rIX*np.exp(2*1j*(phi_C + phi_D)))*np.exp(1j*(phi_C + phi_D + phi_m + phi_P + phi_q)))/(rEY*rIY*np.exp(2*1j*(phi_C + phi_m)) - np.exp(2*1j*(phi_D + phi_m)) - rEX*rEY*rIX*rIY*np.exp(2*1j*(2*phi_C + phi_D + phi_m)) + rEX*rIX*np.exp(2*1j*(phi_C + 2*phi_D + phi_m)) + rBS**2*rEY*rP*(rIY**2 + tIY**2)*np.exp(2*1j*(phi_C + phi_P + phi_q)) - rBS**2*rIY*rP*np.exp(2*1j*(phi_D + phi_P + phi_q)) - rBS**2*rEX*rEY*rIX*rP*(rIY**2 + tIY**2)*np.exp(2*1j*(2*phi_C + phi_D + phi_P + phi_q)) + rBS**2*rEX*rIX*rIY*rP*np.exp(2*1j*(phi_C + 2*phi_D + phi_P + phi_q)) + rEY*rIX*rIY*rP*tBS**2*np.exp(2*1j*(phi_C + 2*phi_m + phi_P + phi_q)) - rIX*rP*tBS**2*np.exp(2*1j*(phi_D + 2*phi_m + phi_P + phi_q)) - rEX*rEY*rIY*rP*tBS**2*(rIX**2 + tIX**2)*np.exp(2*1j*(2*phi_C + phi_D + 2*phi_m + phi_P + phi_q)) + rEX*rP*tBS**2*(rIX**2 + tIX**2)*np.exp(2*1j*(phi_C + 2*phi_D + 2*phi_m + phi_P + phi_q)))
    return Eyarm

def full_ifo_prc_electric_field(k, Tbs, Tix, Tiy, Tex, Tey, Tp, Ts, 
                                phi_X, phi_Y, phi_x, phi_y, phi_P, phi_S, 
                                eta_bs, eta_xarm, eta_yarm, eta_x, eta_y, eta_p, eta_s):
    '''Returns the electric field tf in the for light from the beamsplitter to the power recycling mirror.
    Takes in the lengths and reflectivities of each mirror in the full IFO.
    Inputs:
    k       = wavenumber 2pi/lambda0
    Tbs     = power tranmssion of beamsplitter
    Tix     = power tranmssion of ITMX
    Tiy     = power tranmssion of ITMY
    Tex     = power tranmssion of ETMX
    Tey     = power tranmssion of ETMY
    Tp      = power tranmssion of PRM
    Ts      = power tranmssion of SRM
    phi_X  = length of X-arm
    phi_Y  = length of Y-arm
    phi_x      = length of Michelson x-arm
    phi_y      = length of Michelson y-arm
    phi_P      = length of PRM to beamsplitter
    phi_S      = length of SRM to beamsplitter
    eta_bs  = power loss from reflection off beamsplitter
    eta_xarm = power loss from reflection off ETMX
    eta_yarm = power loss from reflection off ETMY
    eta_x   = power loss from reflection off ITMX
    eta_y   = power loss from reflection off ITMY
    eta_p   = power loss from reflection off PRM
    eta_s   = power loss from reflection of SRM

    Output:
    Ebstoprm = complex. estimated PRC electric field
    '''
    # Set up the inputs
    tBS = np.sqrt(Tbs)
    rBS = np.sqrt(1 - Tbs - eta_bs)
    tIX = np.sqrt(Tix)
    rIX = np.sqrt(1 - Tix - eta_x)
    tIY = np.sqrt(Tiy)
    rIY = np.sqrt(1 - Tiy - eta_y)
    tEX = np.sqrt(Tex)
    rEX = np.sqrt(1 - Tex - eta_xarm)
    tEY = np.sqrt(Tey)
    rEY = np.sqrt(1 - Tey - eta_yarm)    
    tP = np.sqrt(Tp)
    rP = np.sqrt(1 - Tp - eta_p)
    tS = np.sqrt(Ts)
    rS = np.sqrt(1 - Ts - eta_s)

    phi_C = phi_X + phi_Y
    phi_D = phi_X - phi_Y
    phi_q = phi_x + phi_y
    phi_m = phi_x - phi_y

    # use the mathematica equation
    Ebstoprm = -((tP*(rBS**2*rEY*(rIY**2 + tIY**2)*np.exp(2*1j*phi_C) - rBS**2*rIY*np.exp(2*1j*phi_D) - rBS**2*rEX*rEY*rIX*(rIY**2 + tIY**2)*np.exp(2*1j*(2*phi_C + phi_D)) + rBS**2*rEX*rIX*rIY*np.exp(2*1j*(phi_C + 2*phi_D)) + rEY*rIX*rIY*tBS**2*np.exp(2*1j*(phi_C + 2*phi_m)) - rIX*tBS**2*np.exp(2*1j*(phi_D + 2*phi_m)) - rEX*rEY*rIY*tBS**2*(rIX**2 + tIX**2)*np.exp(2*1j*(2*phi_C + phi_D + 2*phi_m)) + rEX*tBS**2*(rIX**2 + tIX**2)*np.exp(2*1j*(phi_C + 2*(phi_D + phi_m))))*np.exp(2*1j*(phi_P + phi_q)))/(rEY*rIY*np.exp(2*1j*(phi_C + phi_m)) - np.exp(2*1j*(phi_D + phi_m)) - rEX*rEY*rIX*rIY*np.exp(2*1j*(2*phi_C + phi_D + phi_m)) + rEX*rIX*np.exp(2*1j*(phi_C + 2*phi_D + phi_m)) + rBS**2*rEY*rP*(rIY**2 + tIY**2)*np.exp(2*1j*(phi_C + phi_P + phi_q)) - rBS**2*rIY*rP*np.exp(2*1j*(phi_D + phi_P + phi_q)) - rBS**2*rEX*rEY*rIX*rP*(rIY**2 + tIY**2)*np.exp(2*1j*(2*phi_C + phi_D + phi_P + phi_q)) + rBS**2*rEX*rIX*rIY*rP*np.exp(2*1j*(phi_C + 2*phi_D + phi_P + phi_q)) + rEY*rIX*rIY*rP*tBS**2*np.exp(2*1j*(phi_C + 2*phi_m + phi_P + phi_q)) - rIX*rP*tBS**2*np.exp(2*1j*(phi_D + 2*phi_m + phi_P + phi_q)) - rEX*rEY*rIY*rP*tBS**2*(rIX**2 + tIX**2)*np.exp(2*1j*(2*phi_C + phi_D + 2*phi_m + phi_P + phi_q)) + rEX*rP*tBS**2*(rIX**2 + tIX**2)*np.exp(2*1j*(phi_C + 2*phi_D + 2*phi_m + phi_P + phi_q))))
    return Ebstoprm

def get_electric_fields(    k, Tbs, Tix, Tiy, Tex, Tey, Tp, Ts, 
                            phi_X, phi_Y, phi_x, phi_y, phi_P, phi_S, 
                            eta_bs, eta_xarm, eta_yarm, eta_x, eta_y, eta_p, eta_s):
    '''Returns a dictionary of Ex, Ey, and Ep, the electric fields in the X-arm, Y-arm, and PRC
    k       = wavenumber 2pi/lambda0
    Tbs     = power tranmssion of beamsplitter
    Tix     = power tranmssion of ITMX
    Tiy     = power tranmssion of ITMY
    Tex     = power tranmssion of ETMX
    Tey     = power tranmssion of ETMY
    Tp      = power tranmssion of PRM
    Ts      = power tranmssion of SRM
    phi_X  = length of X-arm
    phi_Y  = length of Y-arm
    phi_x      = length of Michelson x-arm
    phi_y      = length of Michelson y-arm
    phi_P      = length of PRM to beamsplitter
    phi_S      = length of SRM to beamsplitter
    eta_bs  = power loss from reflection off beamsplitter
    eta_xarm = power loss from reflection off ETMX
    eta_yarm = power loss from reflection off ETMY
    eta_x   = power loss from reflection off ITMX
    eta_y   = power loss from reflection off ITMY
    eta_p   = power loss from reflection off PRM
    eta_s   = power loss from reflection of SRM

    Output:
    Ebstoprm = complex. estimated PRC electric field
    '''
    Ex = full_ifo_x_arm_electric_field(  k, Tbs, Tix, Tiy, Tex, Tey, Tp, Ts, phi_X, phi_Y, phi_x, phi_y, phi_P, phi_S, eta_bs, eta_xarm, eta_yarm, eta_x, eta_y, eta_p, eta_s)
    Ey = full_ifo_y_arm_electric_field(  k, Tbs, Tix, Tiy, Tex, Tey, Tp, Ts, phi_X, phi_Y, phi_x, phi_y, phi_P, phi_S, eta_bs, eta_xarm, eta_yarm, eta_x, eta_y, eta_p, eta_s)
    Ep = full_ifo_prc_electric_field(    k, Tbs, Tix, Tiy, Tex, Tey, Tp, Ts, phi_X, phi_Y, phi_x, phi_y, phi_P, phi_S, eta_bs, eta_xarm, eta_yarm, eta_x, eta_y, eta_p, eta_s)

    out = {}
    out['Ex'] = Ex
    out['Ey'] = Ey
    out['Ep'] = Ep

    return out

def index_finder(array, number):
    '''Look for a number in a monotonic array,
    and if found, return the nearest index of the array.
    '''
    # Check if number is inside array limits
    front_limit = number - array[0]
    back_limit = number - array[-1]
    signs_sum = np.sign(front_limit) + np.sign(back_limit)
    if not signs_sum == 0:
        print()
        print(f'number = {number} is not in array, or array is not monotonic')
        return None

    if front_limit > 0: # the array must be increasing, so look for first point > number
        idx = np.argwhere(array > number)[0, 0]
    else:
        idx = np.argwhere(array < number)[0, 0]
    return idx


#####   Parameters   #####
### From Tables II and III

## Print arm powers estimated from dead-reckoned arm gains
# LHO
lambda0 = 1064e-9
kk = 2 * np.pi / lambda0

# Transmissions
Tbs_lho = 0.5
Tix_lho = 0.0150
Tiy_lho = 0.0142
Tex_lho = 4.0e-6
Tey_lho = 4.0e-6
Tp_lho  = 0.031
Ts_lho  = 1.0 #0.3234

# single-pass phase
phi_xarm = 0
phi_yarm = 0
phi_prc = 0 # PRM tuning for carrier to be resonant?
phi_src = np.pi / 2

# approximate macroscopic lengths
phi_X0 = 3994.5
phi_Y0 = 3994.5
phi_P0 = 57.656
phi_S0 = 56.008

# adjusted for microscopic tuning: macro + micro
phi_X = phi_xarm  #int(np.round(phi_X0 * kk)) / kk + np.mod(phi_xarm, 2*np.pi) / kk
phi_Y = phi_yarm  #int(np.round(phi_Y0 * kk)) / kk + np.mod(phi_yarm, 2*np.pi) / kk
phi_x = 0           # simplification
phi_y = 0
phi_P = phi_prc  #int(np.round(phi_P0 * kk)) / kk + phi_prc / kk
phi_S = phi_src  #int(np.round(phi_S0 * kk)) / kk + phi_src / kk

# Measurement results
Pin_lho = 33.6
PRG_lho = 43.7
Pbs_lho = Pin_lho * PRG_lho
Parmx_lho_meas = 194e3 # +- 2e3
Parmy_lho_meas = 207e3 #

# Nominal losses 
Loss_bs_lho = 0
Loss_xarm_lho = 55e-6
Loss_yarm_lho = 55e-6
Loss_xmich_lho = 0
Loss_ymich_lho = 0
Loss_prc_lho = 0.005
Loss_src_lho = 0.05

nominal_params_dict = {}
nominal_params_dict['k'] = kk
nominal_params_dict['Tbs'] = Tbs_lho
nominal_params_dict['Tix'] = Tix_lho
nominal_params_dict['Tiy'] = Tiy_lho
nominal_params_dict['Tex'] = Tex_lho 
nominal_params_dict['Tey'] = Tey_lho 
nominal_params_dict['Tp'] = Tp_lho 
nominal_params_dict['Ts'] = Ts_lho 
nominal_params_dict['phi_X'] = phi_X 
nominal_params_dict['phi_Y'] = phi_Y 
nominal_params_dict['phi_x'] = phi_x 
nominal_params_dict['phi_y'] = phi_y 
nominal_params_dict['phi_P'] = phi_P 
nominal_params_dict['phi_S'] = phi_S 
nominal_params_dict['eta_bs'] = Loss_bs_lho 
nominal_params_dict['eta_xarm'] = Loss_xarm_lho
nominal_params_dict['eta_yarm'] = Loss_yarm_lho 
nominal_params_dict['eta_x'] = Loss_xmich_lho 
nominal_params_dict['eta_y'] = Loss_ymich_lho 
nominal_params_dict['eta_p'] = Loss_prc_lho 
nominal_params_dict['eta_s'] = Loss_src_lho

nominal_results = get_electric_fields(**nominal_params_dict)

print()
print(f'nominal_results')
print(f'Ex = {nominal_results["Ex"]}')
print(f'Ey = {nominal_results["Ey"]}')
print(f'Ep = {nominal_results["Ep"]}')
print()

# LLO
# Tix_llo = 0.0148
# Tiy_llo = 0.0148
# Tex_llo = 3.8e-6
# Tey_llo = 3.9e-6
# Loss_arm_llo = 80e-6

# Pin_llo = 38.4
# PRG_llo = 47.4
# Pbs_llo = Pin_llo * PRG_llo
# Parmx_llo_meas = 232e3
# Parmy_llo_meas = 245e3


# Loss vectors
bs_losses = np.linspace(0.0, 0.01, 200)
common_arm_losses = np.linspace(0.0, 200e-6, 200)
diff_arm_losses = np.linspace(0.0, 100e-6, 200)
prc_losses = np.linspace(0.0, 0.02, 200)
# src_losses = np.linspace(0.0, 0.1, 200)

loss_vectors = np.array([
    bs_losses,
    common_arm_losses,
    diff_arm_losses,
    prc_losses,
    # src_losses,
])

loss_multipliers = np.array([
    1e2,
    1e6,
    1e6,
    1e2,
    # 1e2
])

loss_units = np.array([
    '\%',
    'ppm',
    'ppm',
    '\%',
    # '\%',
])


# Model labels
labels = np.array([
    'Beamsplitter losses',
    'Common arm losses',
    'Differential arm losses',
    'Power recycling cavity losses',
    # 'Signal recycling cavity losses',
])

# Set up loss parameter dictionaries with mostphi_y nominal params, and one vector apiece
loss_dicts = {}
for label, loss_vector in zip(labels, loss_vectors):
    loss_dicts[label] = copy.deepcopy( nominal_params_dict )

loss_dicts['Beamsplitter losses']['eta_bs'] = bs_losses

loss_dicts['Common arm losses']['eta_xarm'] = common_arm_losses / 2
loss_dicts['Common arm losses']['eta_yarm'] = common_arm_losses / 2

loss_dicts['Differential arm losses']['eta_xarm'] = nominal_params_dict['eta_xarm'] + diff_arm_losses / 2
loss_dicts['Differential arm losses']['eta_yarm'] = nominal_params_dict['eta_yarm'] - diff_arm_losses / 2

loss_dicts['Power recycling cavity losses']['eta_p'] = prc_losses

# loss_dicts['Signal recycling cavity losses']['eta_s'] = src_losses

# Call models
data_dict = {}
for label, loss_vector in zip(labels, loss_vectors):
    temp_loss_dict = loss_dicts[label]
    data_dict[label] = get_electric_fields(**temp_loss_dict)
    data_dict[label]['loss_vector'] = loss_vector



#####   Figures   #####
# Plot LHO powers vs losses

for label, loss_multiplier, loss_unit in zip(labels, loss_multipliers, loss_units):

    temp_dict = data_dict[label]

    loss_vector = temp_dict['loss_vector']
    plot_loss_vector = loss_vector * loss_multiplier

    Ex = temp_dict['Ex']
    Ey = temp_dict['Ey']
    Ep = temp_dict['Ep']

    plot_Px = Pin_lho * np.abs(Ex)**2 * 1e-3 # plot kW units
    plot_Py = Pin_lho * np.abs(Ey)**2 * 1e-3 # plot kW units
    plot_PRG = np.abs(Ep)**2

    plot_Px_meas = Parmx_lho_meas * 1e-3
    plot_Py_meas = Parmy_lho_meas * 1e-3
    plot_PRG_meas = PRG_lho

    # Find nearest associated loss
    Px_idx = index_finder(plot_Px, plot_Px_meas)
    Py_idx = index_finder(plot_Py, plot_Py_meas)
    PRG_idx = index_finder(plot_PRG, plot_PRG_meas)

    # Plot loss vs power figure
    fig, (s1, s2) = plt.subplots(2, sharex=True)

    px, = s1.plot(plot_loss_vector, plot_Px, alpha=0.8, label='X arm')
    py, = s1.plot(plot_loss_vector, plot_Py, alpha=0.8, label='Y arm')
    pg, = s2.plot(plot_loss_vector, np.abs(Ep)**2, color='C2', alpha=0.8, label='PRG')

    s1.plot([plot_loss_vector[0], plot_loss_vector[-1]], [plot_Px_meas, plot_Px_meas], color=px.get_color(), ls='--', label=r'LHO $P_{X}$ = '+f'{plot_Px_meas} kW')
    s1.plot([plot_loss_vector[0], plot_loss_vector[-1]], [plot_Py_meas, plot_Py_meas], color=py.get_color(), ls='--', label=r'LHO $P_{Y}$ = '+f'{plot_Py_meas} kW')
    s2.plot([plot_loss_vector[0], plot_loss_vector[-1]], [plot_PRG_meas, plot_PRG_meas], color=pg.get_color(), ls='--', label=r'LHO PRG = '+f'{plot_PRG_meas}')

    if Px_idx is not None:
        loss_Px = plot_loss_vector[Px_idx]
        s1.plot([loss_Px, loss_Px], [np.min(plot_Px), plot_Px[Px_idx]], color=px.get_color(), ls=':', label=f'{label} = '+f'{loss_Px:.1f} {loss_unit}')
    if Py_idx is not None:
        loss_Py = plot_loss_vector[Py_idx]
        s1.plot([loss_Py, loss_Py], [np.min(plot_Py), plot_Py[Py_idx]], color=py.get_color(), ls=':', label=f'{label} = '+f'{loss_Py:.1f} {loss_unit}')
    if PRG_idx is not None:
        loss_PRG = plot_loss_vector[PRG_idx]
        s2.plot([loss_PRG, loss_PRG], [np.min(plot_PRG), plot_PRG[PRG_idx]], color=pg.get_color(), ls=':', label=f'{label} = '+f'{loss_PRG:.1f} {loss_unit}')        

    # s1.set_ylim([1e-20, 5e-16])
    # s1.set_yticks(goodTicks(s1))
    # s1.set_xlim([10, 6000])

    
    s1.set_ylabel(f'Arm power [kW]')
    s2.set_ylabel(f'Power recycling gain')
    s2.set_xlabel(f'{label} [{loss_unit}]')

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.3)
    s2.grid()
    s2.grid(which='minor', ls='--', alpha=0.3)

    s1.legend(ncol=1)
    s2.legend(ncol=1)


    plot_prefix = label.lower().replace(' ', '_')  

    plot_name = f'{plot_prefix}_power_levels_lho.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print(f'Writing plot PDF to {full_plot_name}')
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()


# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()
