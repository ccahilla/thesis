'''
Example 4
SRC cavity at LHO
Create a simple two mirror cavity with radius of curvature of 1.0 meter,
and distance between the mirrors of 0.5 meters
SRM R = -5.69 m                   SR2 R = -6.4 m
 ___                             ___
|   \         L1 = 15.75        /   |
|    | ----------------------- |    |
|___/                   -----   \___|
        L2 = 0.5 m -----
 ____         -----                                ____              ____
|   /    -----         L3 = 19.36 m               /   /  L4 ~ 5.0 m |   /
|  |---------------------------------------------/---/--------------|--|
|___\                                           /___/               |___\
R3 = 35.972                                   BS R = inf          ITMY R = -1934
'''
import os
import numpy as np
import matplotlib as mpl
mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.95,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})
import matplotlib.pyplot as plt
import scipy.constants as scc

import beamtrace
from beamtrace.tracer import BeamTrace

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

### SRC Parameters ###
nn = 1.44963 # index of refraction of silica

# RoC_SRM  = -5.6938 #m vs -5.7150 m = 0.0212 m Finesse - Fulda
# RoC_SR2  = -6.4060 #m vs -6.4240 m = 0.0180 m Finesse - Fulda
# RoC_SR3  = 35.9728 #m vs 36.0130 m = -0.0402 m Finesse - Fulda
# RoC_ITMY = -1934.0000 #m vs -1939.3900 m = 5.3900 m Finesse - Fulda
RoC_SRM  = -5.678 # from galaxy #-5.7150 Fulda # m = 0.0212 m Finesse - Fulda
RoC_SR2  = -6.4240 # m = 0.0180 m Finesse - Fulda
RoC_SR3  = 36.0130 # m = -0.0522 m Finesse - Fulda
RoC_ITMY = -1940.2 #+ ITMY_params['roc_D_per_RH_W'] * ITMY_RH_watts )**(-1) # m Finesse - Fulda
RoC_ITMX = -1940.3 #+ ITMX_params['roc_D_per_RH_W'] * ITMX_RH_watts )**(-1)

ll_SRM_SR2   = 15.7400 #m vs 15.7400 m = 0.0186 m Finesse - Fulda
ll_SR2_SR3   = 15.4601 #m vs 15.4601 m = -0.0166 m Finesse - Fulda
ll_SR3_BSAR  = 19.3661 #m vs 19.3661 m = 0.0000 m Finesse - Fulda
ll_BSsub     = 68.5e-3 #m vs 0.0685 m = 0.0002 m Finesse - Fulda
ll_BS_ITMYAR = 5.0126 #m vs 4.9670 m = 0.0456 m Finesse - Fulda
ll_ITMYsub   = 0.2000 #m vs 0.2000 m = 0.0000 m Finesse - Fulda

total_length_SRC = 55.8495 #m vs 55.8017 m = 0.0478 m Finesse - Fulda

f_ITMY = 34500.0000 #m vs 34500.0000 m = 0.0000 m Finesse - Fulda



### Set up BeamTrace ###
src_cav = BeamTrace()

src_cav.add_mirror(RoC_ITMY, index_of_refraction=nn, name='ITMY HR') # reflection from inside ITMY changes effective RoC
src_cav.add_space(ll_ITMYsub, index_of_refraction=nn, name='ITMY substrate')
src_cav.add_lens(f_ITMY, name='ITMY AR')
src_cav.add_space(ll_BS_ITMYAR, name='L BS to ITMYAR')
src_cav.add_lens(np.inf, name='BS HR')
src_cav.add_space(ll_BSsub, index_of_refraction=nn, name='BS substrate')
src_cav.add_lens(np.inf, name='BS AR')
src_cav.add_space(ll_SR3_BSAR, name='L SR3 to BSAR')
src_cav.add_mirror(RoC_SR3, name='SR3')
src_cav.add_space(ll_SR2_SR3, name='L SR2 to SR3')
src_cav.add_mirror(RoC_SR2, name='SR2')
src_cav.add_space(ll_SRM_SR2, name='L SRM to SR2')
src_cav.add_mirror(RoC_SRM, name='SRM')

src_cav.calculate_cavity_ABCD()

src_cav.scan_cavity(round_trip=False)

# ### Params
w1 = src_cav.ww[0]
w2 = src_cav.ww[-1]
total_gouy = 2 * src_cav.gouy[-1]
fsr = scc.c / (2 * total_length_SRC)
tms = fsr * total_gouy / (2 * np.pi)
fsr_tms_diff = fsr - tms 

print()
print('SRC ABCD = \n{}\n'.format(src_cav.ABCD))
print()
print('Input Parameters')
print('q = {}'.format(src_cav.q_input))
print('w = {}'.format(src_cav.w_input))
print('RoC = {}'.format(src_cav.RoC_input))
print()
print('Accum Gouy Phase = {:.2f} deg'.format(src_cav.get_total_cavity_gouy_phase()))
print(f'total_gouy = {total_gouy * 180/np.pi} deg')
print(f'fsr = {fsr*1e-3} kHz')
print(f'tms = {tms*1e-3} kHz')
print(f'fsr - tms = {fsr_tms_diff*1e-3} kHz')

# src_cav.print()

# fig = src_cav.plot_cavity_scan(round_trip=False, label='Fundamental SRC mode')

### Change input beam so it doesn't match the fundamental mode ###
# src_cav.q_input = (1834.203+427.841j) # estimated q_input from arm cavity (Example 2)

# ll_post_SRM = 10.0 # m
# src_cav.add_space(ll_post_SRM, name='L post SRM')
# src_cav.q_input = (1303.5973411419088+199.5166770259138j) # SRC input eigenmode

fig = src_cav.plot_cavity_scan(steps=1000, round_trip=False, label='SRC beam trace')#, fig=fig)
fig.set_size_inches(12, 9)
s1 = fig.axes[0]
s2 = fig.axes[1]
s1.set_title('')
s1.legend().set_visible(False)
s2.set_ylim([0, 20])
s2.set_yticks([0, 5, 10, 15, 20])

plot_name = 'signal_recycling_cavity_beam_trace.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()

# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()
