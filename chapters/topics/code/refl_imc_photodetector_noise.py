'''
refl_imc_photodetector_noise.py

Plots the dark and shot noise from REFL A, REFL B, and IMC REFL I and Q ports.

Craig Cahillane
December 28, 2020
'''


import os
import glob
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc
import scipy.optimize as sco

mpl.rcParams.update({   'text.usetex': True,
                        'figure.figsize': (12, 9),
                        'font.family': 'serif',
                        # 'font.serif': 'Georgia',
                        # 'mathtext.fontset': 'cm',
                        'lines.linewidth': 2.5,
                        'font.size': 14,
                        'xtick.labelsize': 'large',
                        'ytick.labelsize': 'large',
                        'legend.fancybox': True,
                        'legend.fontsize': 12,
                        'legend.framealpha': 0.7,
                        'legend.handletextpad': 0.5,
                        'legend.labelspacing': 0.2,
                        'legend.loc': 'best',
                        'savefig.dpi': 80,
                        'pdf.compression': 9})



#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])



#####   Functions   #####
def read_tf_sr_data(filename):
    '''Takes in GPIB-written SR785 file, and outputs (1) frequency vector and (2) complex TF in units of magnitude and radians'''
    data = np.loadtxt(filename)
    ff = data[:,0]
    TF = data[:,1] * np.exp(1j * np.pi/180.0 * data[:,2])
    return ff, TF

def read_spectra_sr_data(filename):
    '''Takes in GPIB-written SR785 Spectrum file, and outputs (1) frequency vector and (2) complex TF in units of magnitude and radians'''
    data = np.loadtxt(filename)
    ff = data[:,0]
    spec = data[:,1]
    return ff, spec

def parse_spectra_files_into_dict(files):
    '''Takes in an array of files, reads them, and parses them into dicts'''
    data_dict = {}
    for file1 in files:
        file_label = file1.split('/')[-1][16:-4]
        data_dict[file_label] = {}

        ff, spec = read_spectra_sr_data(file1)
    
        data_dict[file_label]['ff'] = ff
        data_dict[file_label]['spectrum'] = spec
    return data_dict

def plot_spectra_dicts(data_dict, plot_names, title='Spectra Data', ylabel='ASD [$\mathrm{V}/\sqrt{\mathrm{Hz}}$]'):
    '''Takes in a dictionary of spectra and plots them on top of one another'''
    fig = plt.figure(figsize=(30, 10))
    s1 = fig.add_subplot(111)
    for key in data_dict.keys():
        temp_ff = data_dict[key]['ff']
        temp_spec = data_dict[key]['spectrum']

        for key_dc in pdc.keys():
            if key_dc in key:
                sub_dict_pdc = pdc[key_dc]
                for key_dc2 in sub_dict_pdc.keys():
                    if key_dc2 in key:
                        temp_label = '{} {} = {:.1f} mW'.format(key_dc.replace('_', ' '), key_dc2.replace('_', ' '), 1e3 * sub_dict_pdc[key_dc2])
        if 'temp_label' not in locals():
            temp_label = key
        
        s1.loglog(temp_ff, temp_spec, label=temp_label)

    s1.set_title( title )
    s1.set_xlabel('Frequency [Hz]')
    s1.set_ylabel(ylabel)
    s1.legend(ncol=2)
    s1.grid(which='minor', ls='--')

    
    # Save the figure
    plot_name = f'{title.replace(" ", "_")}.pdf'
    full_plot_name = f'{fig_dir}/{plot_name}'
    plot_names = np.append(plot_names, full_plot_name)
    print(f'Writing plot PDF to {full_plot_name}')
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()

    return plot_names

def pd_noise_model(pdc, transimpedance, p_dark, quantum_efficiency=0.9, demod_gain=5.4):
    '''Returns the estimated noise floor given a vector of DC powers (pdc), transimpedance, and detector power'''
    responsitivity = 1064e-9*scc.e/(scc.h*scc.c)
    asd_volts = transimpedance * demod_gain * 2 * np.sqrt( (pdc + p_dark) * scc.e * responsitivity * quantum_efficiency )
    return asd_volts


# Power on each PD during each measurement
pdc = {}
pdc['REFL_A'] = {}
pdc['REFL_B'] = {}
pdc['IMC_REFL'] = {}

pdc['REFL_A']['Dark'] = 1.0e-5
pdc['REFL_B']['Dark'] = 1.0e-5
pdc['IMC_REFL']['Dark'] = 1.0e-5

pdc['REFL_A']['1W'] = 3.51 * 1e-3   # W
pdc['REFL_B']['1W'] = 3.24 * 1e-3   # W
pdc['IMC_REFL']['1W'] = 1.87 * 1e-3 # W

pdc['REFL_A']['2W'] = 7.38 * 1e-3
pdc['REFL_B']['2W'] = 6.80 * 1e-3
pdc['IMC_REFL']['2W'] = 3.92 * 1e-3

pdc['REFL_A']['4W'] = 15.08 * 1e-3
pdc['REFL_B']['4W'] = 13.90 * 1e-3
pdc['IMC_REFL']['4W'] = 8.01 * 1e-3

pdc['REFL_A']['8W'] = 30.97 * 1e-3
pdc['REFL_B']['8W'] = 28.52 * 1e-3



# CMB_files     = glob.glob(data_dir+'*20190115*CARM_ServoBoard*')
refl_a_9_i_files = glob.glob(f'{data_dir}/*20190115*REFL_A_9I*') + glob.glob(f'{data_dir}/StitchedSpectrum_20190118_REFL_A_9I_ShotNoise_8WInput.txt')
refl_a_9_q_files = glob.glob(f'{data_dir}/*20190115*REFL_A_9Q*')
refl_b_9_i_files = glob.glob(f'{data_dir}/*20190115*REFL_B_9I*') + glob.glob(f'{data_dir}/StitchedSpectrum_20190118_REFL_B_9I_ShotNoise_8WInput.txt')
refl_b_9_q_files = glob.glob(f'{data_dir}/*20190115*REFL_B_9Q*')
# IMCB_files    = glob.glob(f'{data_dir}/*20190115*IMC_ServoBoard*')
imc_refl_i_files  = glob.glob(f'{data_dir}/*20190115*IMC_REFL_I*')
imc_refl_q_files  = glob.glob(f'{data_dir}/*20190115*IMC_REFL_Q*')

darkFiles = glob.glob(f'{data_dir}/*20190115*Dark*')
shot1WFiles = glob.glob(f'{data_dir}/*20190115*1W*')
shot2WFiles = glob.glob(f'{data_dir}/*20190115*2W*')
shot4WFiles = glob.glob(f'{data_dir}/*20190115*4W*')



pd_names = np.array(['REFL_A_9I', 'REFL_A_9Q', 'REFL_B_9I', 'REFL_B_9Q', 'IMC_REFL_I', 'IMC_REFL_Q'])

# CMB_dict     = parse_spectra_files_into_dict(CMB_files)
refl_a_9_i_dict = parse_spectra_files_into_dict(refl_a_9_i_files)
refl_a_9_q_dict = parse_spectra_files_into_dict(refl_a_9_q_files)
refl_b_9_i_dict = parse_spectra_files_into_dict(refl_b_9_i_files)
refl_b_9_q_dict = parse_spectra_files_into_dict(refl_b_9_q_files)
# IMCB_dict    = parse_spectra_files_into_dict(IMCB_files)
imc_refl_i_dict  = parse_spectra_files_into_dict(imc_refl_i_files)
imc_refl_q_dict  = parse_spectra_files_into_dict(imc_refl_q_files)
pd_dicts = np.array([ refl_a_9_i_dict, refl_a_9_q_dict, refl_b_9_i_dict, refl_b_9_q_dict, imc_refl_i_dict, imc_refl_q_dict])

darkdict = parse_spectra_files_into_dict(darkFiles)
shot1Wdict = parse_spectra_files_into_dict(shot1WFiles)
shot2Wdict = parse_spectra_files_into_dict(shot2WFiles)
shot4Wdict = parse_spectra_files_into_dict(shot4WFiles)



freqRange = [3000, 10000] # Hz, frequency range to average over

noise_levels = {}
noise_levels_stds = {}
for pd_name, pd_dict in zip(pd_names, pd_dicts):
    noise_levels[pd_name] = {}
    noise_levels_stds[pd_name] = {}
    print(pd_name)
    for key in pd_dict.keys():
        if 'Dark' in key:
            new_key = 'Dark'
        elif '1W' in key:
            new_key = '1W'
        elif '2W' in key:
            new_key = '2W'
        elif '4W' in key:
            new_key = '4W'
        elif '8W' in key:
            new_key = '8W'
            
        temp_ff = pd_dict[key]['ff']
        temp_spec = pd_dict[key]['spectrum']
        low_index = np.argwhere(temp_ff > freqRange[0])[0][0]
        high_index = np.argwhere(temp_ff > freqRange[1])[0][0]

        noise_levels[pd_name][new_key] = np.median(temp_spec[low_index:high_index])
        noise_levels_stds[pd_name][new_key] = np.std(temp_spec[low_index:high_index])
        print(new_key)
        print(noise_levels[pd_name][new_key])
    print()



quantum_efficiency = 0.9 
lambda0 = 1064e-9
demod_gain = 5.4
responsitivity = lambda0 * scc.e / (scc.h * scc.c)
print('Quantum Efficiency = {}'.format(quantum_efficiency))
print('Responsivity = {} A/W'.format(responsitivity))

#####   Figures   #####

# plot_names = plot_spectra_dicts(CMB_dict, plot_names, title='Common Mode Board - Dark and Shot Noise Spectra')
plot_names = plot_spectra_dicts(refl_a_9_i_dict, plot_names, title='refl a 9I dark and shot noise')
plot_names = plot_spectra_dicts(refl_a_9_q_dict, plot_names, title='refl a 9Q dark and shot noise')
plot_names = plot_spectra_dicts(refl_b_9_i_dict, plot_names, title='refl b 9I dark and shot noise')
plot_names = plot_spectra_dicts(refl_b_9_q_dict, plot_names, title='refl b 9Q dark and shot noise')
# plot_names = plot_spectra_dicts(IMCB_dict, plot_names, title='IMC Servo Board - dark and Shot noise')
plot_names = plot_spectra_dicts(imc_refl_i_dict, plot_names,  title='imc refl I dark and shot noise')
plot_names = plot_spectra_dicts(imc_refl_q_dict, plot_names,  title='imc refl Q dark and shot noise')



###   REFL A and B and IMC REFL photodetector   ###
fig, subplots = plt.subplots(3, 2, figsize=(12, 9), sharex='all', sharey='all')
for ii, pd_name in enumerate( sorted(noise_levels.keys()) ):
    print(f'{ii} pd_name = {pd_name}')
    print(f'{ii // 2}, {ii % 2}')
    subplot = subplots[ii // 2, ii % 2]
    for temp_pdc_key in pdc.keys():
        if temp_pdc_key in pd_name:
            pdc_key = temp_pdc_key
            
    noises_keys = noise_levels[pd_name].keys()
    pdcs = np.array( [ pdc[pdc_key][x] for x in noises_keys ] ) 
    noises = np.array( list(noise_levels[pd_name].values()) )
    noises_stds = np.array( list(noise_levels_stds[pd_name].values()) )
    
    # Idcs = resp * np.array(pdcs)
    # print pdcs
    # print noises
    full_pdcs = np.logspace(np.log10(0.9e-6), np.log10(1e-1), 1000) # W
    transimpediance0 = 500
    p_dark0 = 1e-3 # W
    p0 = [transimpediance0, p_dark0]
    
    popt, pcov = sco.curve_fit(pd_noise_model, pdcs, noises, p0, noises_stds)
    fit_eval = pd_noise_model(full_pdcs, *popt)
    
    # zz = np.polyfit(np.log(pdcs[1:]), np.log(noises[1:]), 1)
    # fit = np.poly1d(zz)
    # fit_eval = np.exp( fit(np.log(full_pdcs)) )
    # print zz
    
    # Find where shot and dark noise meet
    trans0 = popt[0]
    p_dark0 = 1e3 * popt[1]
    print('Transimpedance = {:.1f} V/A'.format(trans0))
    print('p_dark = {:.1f} mW'.format(p_dark0))
    
    # plot line for shot noise
    shot_line = pd_noise_model(full_pdcs, trans0, 0, quantum_efficiency=0.9, demod_gain=5.4)
    
    ylims = [5e-8, 5e-7]

    # Plot
    subplot.errorbar(1e3 * pdcs, noises, yerr=noises_stds, fmt='.', markersize=14, label='measurement')
    subplot.loglog(1e3 * full_pdcs, fit_eval, label=f'PD noise model fit\n$T$ = {trans0:.0f} V/A')
    subplot.loglog(1e3 * full_pdcs, shot_line, ls='--', label='shot noise')
    subplot.loglog([p_dark0, p_dark0], ylims, ls='--', label=r'$P_\mathrm{dark}$'+f' = {p_dark0:.1f} mW')
    subplot.loglog(1e3 * np.array([min(full_pdcs), max(full_pdcs)]), [noises[0], noises[0]], ls='--', label=f'dark noise') 
    
    if ii > 3:
        subplot.set_xlabel('DC power on PD [mW]')
    if ii % 2 == 0:
        subplot.set_ylabel('ASD ' + r'[$\mathrm{ V / \sqrt{Hz} }$]')
    subplot.set_title('{}'.format(pd_name.replace("_", " ")))
    
    subplot.set_xlim([9e-4, 1e2])
    subplot.set_ylim(ylims)

    subplot.set_xticks([1e-3, 1e-2, 1e-1, 1e0, 1e1, 1e2])
    subplot.set_yticks([1e-7])

    subplot.set_yticklabels([], minor=True)
    
    subplot.grid()
    subplot.grid(which='minor', ls='--', alpha=0.6)
    subplot.legend()
    


# Save the figure
plot_name = 'all_lsc_refl_pds_dark_and_shot_noise_levels.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()


# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()