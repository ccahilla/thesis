\chapter{Future work}
\label{chap:future_work}

This thesis overviewed topics in advanced interferometry and spectral statistics for the purpose of 
increasing the sensitivity of the Advanced LIGO detectors for observing run three.
With the successful introduction of squeezing and high laser power, 
and the suppression of the technical laser noises that would limit the advantage of those upgrades,
Advanced LIGO achieved the lowest quantum noise ever in long-baseline interferometers,
and the best sensitivity yet to gravitational waves.

\section{Noise considerations}
\label{sec:noise_considerations}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{./chapters/future_work/figures/darm_comparison/darm_comparison.pdf}
    \caption[DARM comparison]{
        DARM sensitivity between Hanford and Livingston during O3a,
        plus Advanced LIGO design sensitivity (125~W input power, no squeezing).
    }
    \label{fig:darm_comparison}
\end{figure}

The biggest challenge for O4 and beyond is the achievement of design sensitivity.
Figure~\ref{fig:darm_comparison} shows the difference in DARM sensitivity between sites in O3.
Overall, Livingston enjoys a better sensitivity across the entire GW detection band.
Livingston's better sensitivity is due to a number of reasons:
\begin{enumerate}
    \item Better squeezing overall, including less frequency-dependent loss (see Figure~\ref{fig:squeezing_level_estimates}),
    \item Higher arm power (see Table~\ref{table:arm_powers}),
    \item Much lower length and angular controls noise,
    \item Higher detector bandwidth, likely due to lower SRC losses (see Figure~\ref{fig:darm_pole_vs_src_losses})
\end{enumerate}

The most important frequency region for astrophysical range is the low-frequency region, where binary inspirals spend more time orbiting ($\sim$seconds rather than $\sim$milliseconds at merger).
Thus the most critcal region for Hanford to improve sensitivity to GWs is the 30 to 70~Hz region.
However, some new astrophysics is being done with high frequency gravitational waves near merger, 
including higher multipole GW detection \cite{potentialNSBH} and neutron star equation of state measurements \cite{GW170817neutronstarequationofstate}.


The biggest obstacle to achieving design sensitivity is the ``mystery noise'' limiting GW sensitivity around 30~Hz.
Mystery noise is not coherent with any other witnesses in Advanced LIGO, so mitigation is not straightforward.
However, there is less mystery noise at Livingston than Hanford, which accounts for most of their increased range compared to Hanford in O3.
This suggests some difference between the detectors is causing the excess mystery noise at Hanford.
The most likely culprit for mystery noise is excess low frequency motion due to angular and length sensing and control upconverting to higher frequencies,
polluting the DARM spectrum.

Angular controls noise is the next biggest obstacle to achieving design sensitivity.
This is the dominant noise everywhere below 30~Hz.
Angular motion must be suppressed to keep the interferometer locked.
Residual angular motion will couple to length degrees of freedom both linearly and nonlinearly, 
from beam miscentering to spontaneous power fluctuations from poor alignment causing radiation pressure fluctuations.

Sensing the residual angular motion above 10~Hz is limited by the wavefront sensor noise.
To filter this noise, aggressive low-pass filters in the angular control loops are engaged. 
This reduces the angular control gain above 10Hz, and drastically reduces angular control noise coupling to DARM. 
However, this also reduces the phase margin of the loops to close to few tens of degrees.

With increasing power in the arm cavities, the optical plant of the angular degrees of freedom dynamically change due to increasing radiation pressure torque, which must be compensated.
Additionally, the cross-coupling of the loops with length degrees of freedom and other angular degrees of freedom is relatively high with the purposeful beam spot miscentering due to the point absorbers.
If cross-coupling is high, then controls noise from one degree of freedom pollutes another degree of freedom, causing additional noise.

Because of the nonlinearity of angular motion coupling to DARM, its very likely controls noise goes hand-in-hand with mystery noise at both sites. 
Improvements here will either directly mitigate mystery noise, or reveal more of the underlying noise. 

Finally, the source of frequency-dependent squeezing must be found and mitigated to achieve the full benefits of injected squeezing.
This will be more important with the addition of the filter cavity for O4.



\section{O4 upgrades}
\label{sec:o4_upgrades}

Currently upgrades for observing run four are underway at both Hanford and Livingston.

The biggest upgrade is the addition of the filter cavity,
a 300~m long cavity for the injection of frequency-dependent squeezing \cite{McCuller2020, filtercavitydesign}.
This will allow reduction in both quantum shot noise and quantum radiation pressure noise at the DARM readout.
Quantum radiation pressure noise has been measured to limit DARM at Livingston \cite{Yu2020}, and to a lesser extent Hanford.
Work on low-frequency controls is required to see the full advantage of the filter cavity.

Next, at Hanford ITMY has been replaced.
ITMY had a large point absorber during O3, requiring beam spot moves to mitigate some of its impact.
The intentional beam miscentering which adversely affects the angular control loops will no longer be necessary.
Additionally, the new ITMY transmission is 1.50\%, which matches ITMX and should balance the arm powers and arm poles at Hanford.



\section{Future projects}
\label{sec:future_projects}

Work on building the filter cavity and improving the performance of the squeezer are already underway at the site.
Commissioning the filter cavity and achieving higher circulating power in the arms must happen prior to O4.

Other projects may yield significant insight into Hanford's performance.
Some future projects for understanding detector noise include
\begin{enumerate}
    \item Create MIMO control models for interferometer angular and length controls using pytickle \cite{pytickle}.
    \item Create MIMO control models for angle to length couplings \cite{alog50498}.
    \item Thoroughly test and model the effects of thermal compensation on DARM and the SRC together.
    \item Create a plausible power budget for the interferometer carrier and sidebands.
    \item Create plausible RF phase and amplitude noise projections to DARM.
    \item Develop infrastructure for long-term median-averaged coherence measurements for ``quiescent'' transfer functions.
    \item Investigation of calibration error requirement for future detectors.
    \item Development of sub-percent calibration precision schemes.
\end{enumerate}



\section{Conclusions}
\label{sec:conclusions}

Observing run three by Advanced LIGO and Advanced Virgo was the most successful search for gravitational waves in history, 
with 39 confirmed detections in only the first half of the run \cite{SecondCatalogPaper}.
It was also only the third successful search for gravitational waves.
Our sensitivity to these extreme astrophysical phenomena is only in its infancy. 

In gravitational wave detector science, a small improvement in DARM sensitivity can have a tremendous impact on astrophysics.
Now we push our current detectors to their sensitivity limits, to further expand the horizons of the new field of gravitational wave astronomy.