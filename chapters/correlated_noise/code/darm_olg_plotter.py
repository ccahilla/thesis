'''
darm_olg_plotter.py

Made some sliders to see how DARM loop changes affect the gated mean averaged
and median averaged correlated noise plot.

May 19, 2020
'''
import os
import time
import copy
import numpy as np

import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
# from matplotlib.widgets import Slider, Button, RadioButtons

# plt.ion()
from scipy.io import loadmat
from scipy.interpolate import interp1d
import scipy.constants as scc

import argparse

import nds2utils as nu

mpl.rcParams.update({'figure.figsize': (12, 9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'lines.markersize': 5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.9,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'legend.columnspacing': 2,
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Git Repo Directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

data_dir = f'{chapter_dir}/data'

#####   Functions   #####
def read_psd_txt(filename):
    '''Takes in filename and reads it. 
    Assumes two columns, frequency vector and PSD
    Avoids reading first element, as it is ff = 0 Hz.
    Returns two-element tuple = (ff, PSD)
    '''
    data = np.loadtxt(filename)
    ff = data[1:,0] # take out the first point which is freq = 0 Hz
    psd = data[1:,1]
    return ff, psd

def read_csd_txt(filename):
    '''Takes in filename and reads it. 
    Assumes three columns, frequency vector, CSD mag, and CSD phase in radians.
    Avoids reading first element, as it is ff = 0 Hz.
    Could also be used to read TFs.
    Returns two-element tuple = (ff, complex CSD)
    '''
    data = np.loadtxt(filename)
    ff = data[1:,0] # take out the first point which is freq = 0 Hz
    csd = data[1:,1] * np.exp(1j * data[1:,2])
    return ff, csd

def read_cal_group_olg_tf_txt(filename):
    '''Takes in cal group transfer function filename and reads it. 
    Assumes three columns: frequency vector, TF real, and TF imag.
    I flip to sign of the OLG to be 
    Could also be used to read TFs.
    Returns two-element tuple = (ff, complex TF)
    '''
    data = np.loadtxt(filename)
    ff = data[:,0] # take out the first point which is freq = 0 Hz
    tf = -1 * (data[:,1] + 1j * data[:,2])
    return ff, tf

def read_cal_group_sensing_tf_txt(filename):
    '''Takes in cal group sensing transfer function filename and reads it. 
    Columns are as follows:
    frequency vector, sensing mag [cts/m], err [mA/pm], sensing phase [deg], unc [degs], 
    response function [mag], response function [deg], response residual [mag], response residual [degs]
    
    Could also be used to read TFs.
    Returns two-element tuple = (ff, complex TF)
    '''
    data = np.loadtxt(filename)
    ff = data[:,0] # take out the first point which is freq = 0 Hz
    tf = data[:,1] * np.exp(1j * np.pi/180 * data[:,3])
    return ff, tf

def compute_tf_residuals(ff_a, tf_a, ff_b, tf_b):
    '''Takes in frequency vector and transfer function A for the denominator,
    and frequency vector and transfer function B for the numerator.
    Output:
    (ff_residuals, residuals) = smaller frequency vector of ff_a and ff_b and matching complex residuals.
    '''
    if len(ff_a) >= len(ff_b):
        ff_residuals = ff_b
        small_tf_a = nu.get_complex_interp(ff_residuals, ff_a, tf_a)
        small_tf_b = tf_b
    else:
        ff_residuals = ff_a
        small_tf_b = nu.get_complex_interp(ff_residuals, ff_b, tf_b)
        small_tf_a = tf_a

    residuals = small_tf_b/small_tf_a

    return ff_residuals, residuals

def main():
    '''main function of this script.  
    Makes darm_olg.pdf comparing some DARM open loop gains.
    '''
    # ratio_sq = 1894812100000.0 # (mA/cts)^2, from 

    olg_files = np.array([
        'LHO_pyDARM_OLG.txt',
        # '2019-10-28_H1_0ctSRCLOffset_DARM_OLGTF_LF_SS_A_DARMIN2_B_DARMIN1_tf.txt',
        '2019-10-30_H1_100ctSRCLOffset_DARM_OLGTF_LF_SS_A_DARMIN2_B_DARMIN1_tf.txt',
        '2019-10-31_H1_50ctSRCLOffset_DARM_OLGTF_LF_SS_A_DARMIN2_B_DARMIN1_tf.txt',
        # '2019-10-28_H1_0ctSRCLOffset_DARM_OLGTF_LF_SS_A_DARMIN2_B_DARMEXC_tf.txt',
        '2019-10-28_H1_0ctSRCLOffset_DARM_OLGTF_LF_SS_A_DARMIN2_B_DARMIN1_tf_corrected.txt',
        '2020-08-28_H1_DARM_OLGTF_LF_SS_A_DARMIN2_B_DARMIN1_tf.txt',
    ])
    olg_labels = np.array([
        'pyDARM OLG',
        # '2019 Oct 28 - 0 SRCL offset uncorrected',
        '2019 Oct 30 - 100 SRCL offset',
        '2019 Oct 31 - 50 SRCL offset',
        # '2019 Oct 28 - 0 SRCL offset - EXC/IN2',
        '2019 Oct 28 - 0 SRCL offset',
        '2020 Aug 28',
    ])

    sensing_files = np.array([
        'LHO_pyDARM_sensing_function_mA_per_meter.txt', # is actually in cts/m
        '2019-10-31_H1_sensingFunction_processedOpticalResponse.txt',
    ])
    sensing_labels = np.array([
        'pyDARM sensing',
        '2019 Oct 31 meas - 50 SRCL offset'
    ])

    olg_dict = {}
    for ii, olg_file, olg_label in zip(np.arange(len(olg_files)), olg_files, olg_labels):
        if ii == 0:
            read_func = read_csd_txt
            temp_style = '-'
        elif ii == 4:
            read_func = read_csd_txt
            temp_style = '^'
        else:
            read_func = read_cal_group_olg_tf_txt
            temp_style = 'o'

        full_file = f'{data_dir}/{olg_file}'
        temp_ff, temp_tf = read_func(full_file)
        olg_dict[olg_label] = {}
        olg_dict[olg_label]['ff'] = temp_ff
        olg_dict[olg_label]['tf'] = temp_tf
        olg_dict[olg_label]['style'] = temp_style

    sensing_dict = {}
    for ii, sensing_file, sensing_label in zip(np.arange(len(sensing_files)), sensing_files, sensing_labels):
        if ii == 0:
            read_func = read_csd_txt
            temp_style = '-'
        else:
            read_func = read_cal_group_sensing_tf_txt
            temp_style = 'o'

        full_file = f'{data_dir}/{sensing_file}'
        temp_ff, temp_tf = read_func(full_file)
        sensing_dict[sensing_label] = {}
        sensing_dict[sensing_label]['ff'] = temp_ff
        sensing_dict[sensing_label]['tf'] = temp_tf
        sensing_dict[sensing_label]['style'] = temp_style

    # Extrapolate the good measurement

    pyDARM_ff = olg_dict['pyDARM OLG']['ff']
    pyDARM_tf = olg_dict['pyDARM OLG']['tf']

    interp_ff = olg_dict['pyDARM OLG']['ff']
    old_ff = olg_dict['2020 Aug 28']['ff']
    old_tf = olg_dict['2020 Aug 28']['tf']
    interp_re_func = interp1d(
        np.log(old_ff), 
        np.real(old_tf),
        kind='cubic',
        fill_value='extrapolate'
        )
    interp_im_func = interp1d(
        np.log(old_ff), 
        np.imag(old_tf),
        kind='cubic',
        fill_value='extrapolate'
        )
    temp_interp_tf = interp_re_func(np.log(interp_ff)) + 1j * interp_im_func(np.log(interp_ff))
    
    # stitch with pyDARM at some intermediate frequency, with pyDARM at HF and meas at LF
    intermediate_frequency = 275.0 # Hz, center of logistic
    rate = 0.1 # 1/Hz, rate of logistic
    def logistic(xx, center, rate):
        '''Logistic function like 1/(1 + exp(-rate * (xx - center)))'''
        return 1/(1 + np.exp(-rate * (xx - center)))
    temp_logistic = logistic(interp_ff, intermediate_frequency, rate)

    interp_tf = pyDARM_tf * temp_logistic + temp_interp_tf * (1 - temp_logistic)

    # save it
    # save_filename = 'LHO_pyDARM_OLG_for_HF_interp_from_2019_10_28_H1_0ctSRCLOffset_for_LF.txt'
    save_filename = 'LHO_pyDARM_OLG_for_HF_interp_from_2020_08_28.txt'
    save_full_filename = f'{data_dir}/{save_filename}'
    save_data = np.vstack((interp_ff, np.abs(interp_tf), np.angle(interp_tf))).T
    print('\033[93m')
    print(f'Saving file: {save_full_filename}')
    print('\033[0m')
    np.savetxt(save_full_filename, save_data, header='Frequency [Hz], pyDARM Open Loop Gain Mag [none], Phase [rads]')


    ############
    #  Plots   #
    ############

    plot_names = np.array([])
    
    # DARM OLGs
    fig, (s1, s2) = plt.subplots(2, sharex=True)

    for olg_label in olg_labels:
        ff = olg_dict[olg_label]['ff']
        tf = olg_dict[olg_label]['tf']
        style = olg_dict[olg_label]['style']

        s1.loglog(ff, np.abs(tf), style, label=olg_label)
        s2.semilogx(ff, 180/np.pi * np.angle(tf), style, label=olg_label)

    s1.loglog(interp_ff, np.abs(temp_interp_tf), 'C3', label='Interp - 2019 Oct 28 - 0 SRCL offset')
    s2.semilogx(interp_ff, 180/np.pi * np.angle(temp_interp_tf), 'C3', label='Interp - 2019 Oct 28 - 0 SRCL offset')

    s1.loglog(interp_ff, np.abs(interp_tf), '--', label='Interp + Stitch - 2019 Oct 28 - 0 SRCL offset')
    s2.semilogx(interp_ff, 180/np.pi * np.angle(interp_tf), '--', label='Interp + Stitch - 2019 Oct 28 - 0 SRCL offset')

    s1.set_title('pyDARM and measured OLGs near correlated noise time')
    s1.set_ylabel('Mag')
    s2.set_ylabel('Phase [deg]')
    s2.set_xlabel('Frequency [Hz]')

    s1.set_xlim([4, 2e3])
    s1.set_ylim([1e-2, 1.01e2])

    s1.set_yticks(nu.good_ticks(s1))
    s2.set_yticks(90 * np.arange(-2, 3))

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.5)
    s2.grid()
    s2.grid(which='minor', ls='--', alpha=0.5)

    s1.legend(fontsize=12)

    plot_name = f'measured_and_pyDARM_OLGs.pdf'
    full_plot_name = f'{fig_dir}/{plot_name}'
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.tight_layout()
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # DARM OLGs with residuals
    fig, ss = plt.subplots(2, 2)
    s1 = ss[0, 0]
    s2 = ss[0, 1]
    s3 = ss[1, 0]
    s4 = ss[1, 1]

    ff_pyDARM = olg_dict[olg_labels[0]]['ff']
    tf_pyDARM = olg_dict[olg_labels[0]]['tf']
    for olg_label in olg_labels:
        ff = olg_dict[olg_label]['ff']
        tf = olg_dict[olg_label]['tf']
        style = olg_dict[olg_label]['style']

        s1.loglog(ff, np.abs(tf), style, label=olg_label)
        s3.semilogx(ff, 180/np.pi * np.angle(tf), style, label=olg_label)

        
        ff_residuals, residuals = compute_tf_residuals(ff_pyDARM, tf_pyDARM, ff, tf)
        plot_mag_residuals = 100 * (np.abs(residuals) - 1)
        plot_phase_residuals = 180/np.pi * np.angle(residuals)

        s2.semilogx(ff_residuals, plot_mag_residuals, style, label=olg_label)
        s4.semilogx(ff_residuals, plot_phase_residuals, style, label=olg_label)

    s1.loglog(interp_ff, np.abs(temp_interp_tf), 'C3', label='Interp')
    s3.semilogx(interp_ff, 180/np.pi * np.angle(temp_interp_tf), 'C3', label='Interp')

    ff_residuals, residuals = compute_tf_residuals(ff_pyDARM, tf_pyDARM, interp_ff, temp_interp_tf)
    plot_mag_residuals = 100 * (np.abs(residuals) - 1)
    plot_phase_residuals = 180/np.pi * np.angle(residuals)

    s2.semilogx(ff_residuals, plot_mag_residuals, 'C3', label='Interp')
    s4.semilogx(ff_residuals, plot_phase_residuals,'C3', label='Interp')

    s1.loglog(interp_ff, np.abs(interp_tf), '--', label='Interp + Stitch')
    s3.semilogx(interp_ff, 180/np.pi * np.angle(interp_tf), '--', label='Interp + Stitch')

    ff_residuals, residuals = compute_tf_residuals(ff_pyDARM, tf_pyDARM, interp_ff, interp_tf)
    plot_mag_residuals = 100 * (np.abs(residuals) - 1)
    plot_phase_residuals = 180/np.pi * np.angle(residuals)

    s2.semilogx(ff_residuals, plot_mag_residuals, '--', label='Interp + Stitch')
    s4.semilogx(ff_residuals, plot_phase_residuals,'--', label='Interp + Stitch')

    s1.set_title('pyDARM and measured OLGs')
    s2.set_title('Residuals over pyDARM')
    s1.set_ylabel('Mag')
    s2.set_ylabel(r'Mag Residuals [\%]')
    s3.set_ylabel('Phase [deg]')
    s4.set_ylabel('Phase Residuals [deg]')

    s3.set_xlabel('Frequency [Hz]')
    s4.set_xlabel('Frequency [Hz]')

    s1.set_xlim([4, 2e3])
    s2.set_xlim([4, 2e3])
    s3.set_xlim([4, 2e3])
    s4.set_xlim([4, 2e3])

    s1.set_ylim([1e-2, 1.01e2])
    s2.set_ylim([-10, 10])
    s4.set_ylim([-10, 10])

    s1.set_yticks(nu.good_ticks(s1))
    s3.set_yticks(90 * np.arange(-2, 3))

    xmin, xmax = s1.get_xlim()
    xTicks = np.array([10**x for x in np.arange(np.ceil(np.log10(xmin)), np.ceil(np.log10(xmax)))])
    s1.set_xticks(xTicks)
    s2.set_xticks(xTicks)
    s3.set_xticks(xTicks)
    s4.set_xticks(xTicks)

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.5)
    s2.grid()
    s2.grid(which='minor', ls='--', alpha=0.5)
    s3.grid()
    s3.grid(which='minor', ls='--', alpha=0.5)
    s4.grid()
    s4.grid(which='minor', ls='--', alpha=0.5)

    locmin = mpl.ticker.LogLocator(base=10.0,subs=(0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,),numticks=12)
    s1.xaxis.set_minor_locator(locmin)
    s1.xaxis.set_minor_formatter(mpl.ticker.NullFormatter())
    s2.xaxis.set_minor_locator(locmin)
    s2.xaxis.set_minor_formatter(mpl.ticker.NullFormatter())
    s3.xaxis.set_minor_locator(locmin)
    s3.xaxis.set_minor_formatter(mpl.ticker.NullFormatter())
    s4.xaxis.set_minor_locator(locmin)
    s4.xaxis.set_minor_formatter(mpl.ticker.NullFormatter())


    s1.legend(fontsize=12)

    plot_name = f'measured_and_pyDARM_OLGs_residuals.pdf'
    full_plot_name = f'{fig_dir}/{plot_name}'
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.tight_layout()
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # DARM sensing
    fig, (s1, s2) = plt.subplots(2, sharex=True)

    for sensing_label in sensing_labels:
        ff = sensing_dict[sensing_label]['ff']
        tf = sensing_dict[sensing_label]['tf']
        style = sensing_dict[sensing_label]['style']

        s1.loglog(ff, np.abs(tf), style, label=sensing_label)
        s2.semilogx(ff, 180/np.pi * np.angle(tf), style, label=sensing_label)

    s1.set_title('pyDARM and measured sensing function near correlated noise time')
    s1.set_ylabel('Mag [cts/m]')
    s2.set_ylabel('Phase [deg]')
    s2.set_xlabel('Frequency [Hz]')

    # s1.set_xlim([2e1, 8e3])
    s1.set_ylim([1e5, 5e7])

    s1.set_yticks(nu.good_ticks(s1))
    s2.set_yticks(90 * np.arange(-2, 3))

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.5)
    s2.grid()
    s2.grid(which='minor', ls='--', alpha=0.5)

    s1.legend()

    plot_name = f'measured_and_pyDARM_sensing.pdf'
    full_plot_name = f'{fig_dir}/{plot_name}'
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.tight_layout()
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # print open command
    command = 'open'
    for pf in plot_names:
        command = '{} {}'.format(command, pf)
    print()
    print('Command to open plots generated by this script:')
    print(command)
    print()

    return 
    
if __name__ == "__main__":

    # args = parse_args()
    main()
    