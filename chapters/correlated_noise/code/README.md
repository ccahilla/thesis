# Instructions

The code here should be run in the following order:

1) lho_xcor_get_data.py
This gets the data from nds2 and stores the DCPD time series on my Sandisk driver as a .pkl

2) lho_xcor_calculate_psds_and_csds.py
This gets the time series data from the .pkl, and calculates the PSDs and CSD of the DCPD time seires, then saves those as another .pkl

3) get_raw_mean_spectral_density_txts.py
This gets the PSDs and CSDs .pkl, and uses PSD rejection to find non-glitchy PSDs and CSDs, and takes their average.  Saves these averaged PSDs and CSD as .txts in the /data directory of this thesis chapter.

4) lho_xcor_noisebudget.py 
Calculates the correlated noise from the DCPD PSDs and CSD and removing the DARM OLG, calibrates using the sensing function, and plots the correlated noise, measured DARM sum, measured DARM null, alongside the correlated noisebudget traces.

Optional:
5) squeeze_to_unsqueezed_correlated_noise.py
Same as lho_xcor_noisebudget.py, but also tries to remove the squeezing from the correlated noise using the null channel and level of squeezing measured.  Uses a simplistic frequency-independent squeeze level assumption.

6) correlated_noise_squeeze_remover_interactive.py
Same as squeeze_to_unsqueezed_correlated_noise.py, but interactive and can change the level of frequency-independent squeezing we have with an interactive slider.
