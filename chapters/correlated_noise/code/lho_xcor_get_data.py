'''
lho_xcor_get_data.py
Get the Hanford cross correlation data from a time with no squeezing.

Usage:
$ python lho_xcor_get_data.py 
Reads and saves the data from nds2 as a .pkl file, so we don't have to reread the same data
from the clusters over and over.

Craig Cahillane
September 17, 2020
'''

import os
import time
import numpy as np
import nds2utils as nu

#################
# Git repo path #
#################
# __file__ = '/Users/ccahilla/Git/o3-commissioning-paper'
__file__ = os.path.abspath(__file__)
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = os.path.join(f'{chapter_dir}', 'data')


# Place to save large data
if 'ccahilla' in data_dir:
    external_hd_dir = '/Volumes/SandiskSD/ccahilla/Git/thesis/chapters/correlated_noise/data' # Craig-specific code
else:
    external_hd_dir = f'{data_dir}' # save on personal computer, might be pretty large (~10 GB data)

if not os.path.exists(external_hd_dir):
    os.makedirs(external_hd_dir)

print(f'external_hd_dir = {external_hd_dir}')

#####   Functions   #####
def pickle_saver(filename, channels, gps_start, gps_stop, host_server, port_number, allow_data_on_tape, verbose=True):
    '''Reads a .pkl file from filename if it exists and read_flag is False.  
    If not, reads from nds2 the channels from gps_start to gps_stop.
    Returns a data_dict.
    '''
    if verbose:
        print('Reading DCPD data fron nds2')
    data_dict = nu.acquire_data(channels, gps_start, gps_stop, host_server=host_server, port_number=port_number, allow_data_on_tape=allow_data_on_tape)

    if verbose:
        print('Done')
        print(f'Saving .pkl to {filename}')
    nu.save_pickle(data_dict, filename)
    if verbose:
        print('Done')

    return 

def pickle_saver_caller(gps_start, gps_stop):
    '''pickle_saver(filename, channels, gps_start, gps_stop, verbose=True)
    '''
    channels = np.array([
        'H1:OMC-DCPD_A_OUT_DQ', 
        'H1:OMC-DCPD_B_OUT_DQ', 
        'H1:OMC-DCPD_SUM_OUT_DQ', 
        'H1:OMC-DCPD_NULL_OUT_DQ', 
        'H1:LSC-DARM_IN1_DQ',       # needed for comparing DARM cts to DCPD mA
        ])

    filename = f'{external_hd_dir}/DCPD_data_gps_start_{gps_start}_gps_stop_{gps_stop}.pkl'
    host_server = 'nds.ligo-wa.caltech.edu'
    port_number = 31200
    allow_data_on_tape = 'True'
    verbose = True

    pickle_saver(filename, channels, gps_start, gps_stop, host_server, port_number, allow_data_on_tape, verbose)
    return



def main():
    #####   Measurement time   #####

    # gps_start = 1284249618 # Sep 16 00:00:00 2020, sqz time
    # gps_stop  = 1284270167 # Sep 16 05:42:29 2020

    gps_start = 1284270168 # Sep 16 05:42:30 2020, no sqz time
    gps_stop  = 1284304916 # Sep 16 15:21:38 2020

    # gps_start = 1283317218 # September 5, 2020 05:00:00.000 UTC
    # gps_stop  = 1283346018 # September 5, 2020 13:00:00.000 UTC

    # gps_start = 1282730359  # August 29 2020 09:59:01.000 UTC
    # gps_stop  = 1282763937  # August 29 2020 19:18:39.000 UTC

    duration = gps_stop - gps_start 
    print()
    print(f'duration = {duration / 3600} hours')
    print()

    # multiprocessing
    # gps_times = np.arange(gps_start, gps_stop, 3600)
    # p1 = mp.Pool(target=square_list, args=(mylist, q))

    #####   Load data   #####
    start_time = time.time()

    pickle_saver_caller(gps_start, gps_stop) # read raw data

    load_time = time.time() - start_time
    print(f'Total data acquistion time = {load_time} seconds')

if __name__ == '__main__':
    main()