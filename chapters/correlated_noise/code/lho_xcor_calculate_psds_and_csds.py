'''
lho_xcor_calculate_psds_and_csds.py

Read in time-domain data, and DARM OLG and sensing function, 
to produce calibrated DARM power spectral densities and cross spectral densities 
representing the correlated noise from the interferomter.

Usage:
$ python lho_xcor_calculate_psds_and_csds.py

Craig Cahillane
September 29, 2020
'''

import os
import time
import numpy as np
import scipy.signal as sig
import pickle



#####   Git repo path   #####

script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

# Place to save large data
if 'ccahilla' in chapter_dir:
    external_hd_dir = '/Volumes/SandiskSD/ccahilla/Git/thesis/chapters/correlated_noise/data' # Craig-specific code
else:
    external_hd_dir = f'{data_dir}' # save on personal computer, might be pretty large (~10 GB data)

if not os.path.exists(external_hd_dir):
    os.makedirs(external_hd_dir)
print(f'external_hd_dir = {external_hd_dir}')



#####   Functions   #####

def median_coherence(power_ratio):
    '''Given a power_ratio of uncorrelated noise over correlated noise, 
    power_ratio = sigma_uncorr^2 / sigma_corr^2,
    returns the power coherence gamma^2 that would result from median-averaged PSDs and CSDs.
    '''
    med_coh =   (power_ratio**2 * np.log(1 + 1/np.sqrt(1 + power_ratio))**2) \
                / (4 * np.log(2)**2 * (1 + power_ratio) * (2 + power_ratio - 2 * np.sqrt(1 + power_ratio)) )
    return med_coh

def residual_median_coherence(input_array, median_coherence_desired):
    '''Finds the residual between the coherence desired and the numerically attempt from fsolve().
    For use as the function input to fsolve().
    Inputs:
    input_array = array of length one, required by fsolve to find the power ratio numerically
    median_coherence_desired = median coherence estimated from the CSD signals.
    Output:
    residual = array of length one with residual from median_coherence() function
    '''
    func_results = median_coherence(input_array[0])
    residual = [func_results - median_coherence_desired]
    return residual

def bias_from_median_coherence_and_power_ratio(median_coherence, power_ratio):
    '''Calculates the mean-to-median bias factor from the median_coherence = gamma^2
    and the power_ratio = epsilon.
    '''
    bias = np.log(2) * np.sqrt( (1 + power_ratio) * median_coherence )
    return bias

def bias_from_median_coherence(median_coherence_estimated, initial_power_ratio=0.1):
    '''Estimates the median/mean bias = b given some median_coherence = gamma^2.

    Numerically solves for the uncorrelated/correated power_ratio = epsilon,
    using the median_coherence.
    Uses scipy.optimize.fsolve() to find the root of residual_median_coherence().
    fsolve() seems to work for gamma^2 between (0.999 and 1e-6)

    This function requires bias_from_median_coherence_and_power_ratio(),
    residual_median_coherence(), median_coherence(), and fsolve() functions.

    Inputs:
    median_coherence_estimated  =   median coherence estimated from |<x,z>|^2/(<x,x> <z,z>) 
                                    where all spectral densities are median-averaged
    initial_power_ratio         =   initial guess of the power_ratio, default is 1.0
    Output:
    bias    =   median/mean bias factor.  Divide median-averaged cross spectral density <x,z> 
                by bias to recover the mean-avearged cross spectral density.             
    '''
    # Numerically estimate the power ratio epsilon from the median coherence
    fsolve_array = fsolve(  residual_median_coherence, 
                            [initial_power_ratio], 
                            args=(median_coherence_estimated))
    power_ratio = fsolve_array[0]

    # Find the bias factor
    bias = bias_from_median_coherence_and_power_ratio(  median_coherence_estimated, 
                                                        power_ratio)
    return bias

def biases_from_coherences(median_coherences, initial_power_ratios=None):
    '''Returns array of biases from an array of coherences.
    Inputs:
    median_coherences =    median-averaged coherence estimates
    initial_power_ratios =  power ratios to start estimation with.  
                            If None, uses 0.1 for all.  Default is None.
    Outputs:
    biases =    array of biases calculated from the coherences
    '''
    if initial_power_ratios is None:
        initial_power_ratios = 0.1 * np.ones_like(median_coherences)
    biases = np.array([])
    for coherence, initial_power_ratio in zip(median_coherences, initial_power_ratios):
        bias = bias_from_median_coherence(coherence, initial_power_ratio=initial_power_ratio)
        biases = np.append(biases, bias)
    return biases

def get_all_csds(pickle_filename, chan_a, chan_b, nperseg, noverlap, balance_dict):
    '''Function that outputs in all power and cross spectral densities taken of the two DCPDs,
    so the user might make a PDF of their distribution themselves.
    This function will dump the dataDict from memory at the end, 
    saving my computer from having to store it all'''
    print('Start reading data')
    start_time = time.time()

    #####   Read in huge datefile   #####
    dataDict = nu.load_pickle(pickle_filename)

    print(f'Finished reading data after {time.time() - start_time} seconds')

    #####   Take CSDs   #####
    # record all of them using spectral_helper
    print('Start taking PSDs and CSDs')
    start_time = time.time()

    aa = dataDict[chan_a]['data'] * balance_dict[chan_a]
    bb = dataDict[chan_b]['data'] * balance_dict[chan_b]

    _, _, Saas = sig.spectral_helper(aa, aa, fs, nperseg=nperseg, noverlap=noverlap) # PSDs of DCPD A
    _, _, Sbbs = sig.spectral_helper(bb, bb, fs, nperseg=nperseg, noverlap=noverlap) # PSDs of DCPD B
    ff, tt, Sabs = sig.spectral_helper(aa, bb, fs, nperseg=nperseg, noverlap=noverlap) # CSDs of A and B

    print(f'Finished taking PSDs and CSDs after {time.time() - start_time} seconds')
    return ff, tt, Saas, Sbbs, Sabs

def get_all_csds_plus_sum(pickle_filename, chan_a, chan_b, nperseg, noverlap, balance_dict):
    '''Same as get_all_csds, but also gets psds of dcpd sum out.
    Function that outputs in all power and cross spectral densities taken of the two DCPDs,
    so the user might make a PDF of their distribution themselves.
    This function will dump the dataDict from memory at the end, 
    saving my computer from having to store it all'''
    print('Start reading data')
    start_time = time.time()

    #####   Read in huge datefile   #####
    dataDict = nu.load_pickle(pickle_filename)

    print(f'Finished reading data after {time.time() - start_time} seconds')

    #####   Take CSDs   #####
    # record all of them using spectral_helper
    print('Start taking PSDs and CSDs')
    start_time = time.time()

    aa = dataDict[chan_a]['data'] * balance_dict[chan_a]
    bb = dataDict[chan_b]['data'] * balance_dict[chan_b]

    _, _, Saas = sig.spectral_helper(aa, aa, fs, nperseg=nperseg, noverlap=noverlap) # PSDs of DCPD A
    _, _, Sbbs = sig.spectral_helper(bb, bb, fs, nperseg=nperseg, noverlap=noverlap) # PSDs of DCPD B
    ff, tt, Sabs = sig.spectral_helper(aa, bb, fs, nperseg=nperseg, noverlap=noverlap) # CSDs of A and B

    cc = dataDict['H1:OMC-DCPD_SUM_OUT_DQ']['data']
    _, _, Sccs = sig.spectral_helper(cc, cc, fs, nperseg=nperseg, noverlap=noverlap) # PSDs of DCPD B

    dd = dataDict['H1:OMC-DCPD_NULL_OUT_DQ']['data']
    _, _, Sdds = sig.spectral_helper(dd, dd, fs, nperseg=nperseg, noverlap=noverlap) # PSDs of DCPD B

    ee = dataDict['H1:LSC-DARM_IN1_DQ']['data']
    _, _, Sees = sig.spectral_helper(ee, ee, fs, nperseg=nperseg, noverlap=noverlap) # PSDs of DCPD B

    print(f'Finished taking PSDs and CSDs after {time.time() - start_time} seconds')
    return ff, tt, Saas, Sbbs, Sabs, Sccs, Sdds, Sees

def calc_all_csds(x, y, fs=1.0, window='hann', nperseg=None, noverlap=None, nfft=None,
        detrend='constant', return_onesided=True, scaling='density', axis=-1):
    '''
    Calculates the cross spectral density from data arrays x and y.
    Copied docstring from scipy.signal._spectral_helper() function.

    The windows are not averaged over; 
    the result from each window is returned.

    Parameters
    ---------
    x : array_like
        Array or sequence containing the data to be analyzed.
    y : array_like
        Array or sequence containing the data to be analyzed. 
    fs : float, optional
        Sampling frequency of the time series. Defaults to 1.0.
    window : str or tuple or array_like, optional
        Desired window to use. If `window` is a string or tuple, it is
        passed to `get_window` to generate the window values, which are
        DFT-even by default. See `get_window` for a list of windows and
        required parameters. If `window` is array_like it will be used
        directly as the window and its length must be nperseg. Defaults
        to a Hann window.
    nperseg : int, optional
        Length of each segment. Defaults to None, but if window is str or
        tuple, is set to 256, and if window is array_like, is set to the
        length of the window.
    noverlap : int, optional
        Number of points to overlap between segments. If `None`,
        ``noverlap = nperseg // 2``. Defaults to `None`.
    nfft : int, optional
        Length of the FFT used, if a zero padded FFT is desired. If
        `None`, the FFT length is `nperseg`. Defaults to `None`.
    detrend : str or function or `False`, optional
        Specifies how to detrend each segment. If `detrend` is a
        string, it is passed as the `type` argument to the `detrend`
        function. If it is a function, it takes a segment and returns a
        detrended segment. If `detrend` is `False`, no detrending is
        done. Defaults to 'constant'.
    return_onesided : bool, optional
        If `True`, return a one-sided spectrum for real data. If
        `False` return a two-sided spectrum. Defaults to `True`, but for
        complex data, a two-sided spectrum is always returned.
    scaling : { 'density', 'spectrum' }, optional
        Selects between computing the cross spectral density ('density')
        where `Pxy` has units of V**2/Hz and computing the cross
        spectrum ('spectrum') where `Pxy` has units of V**2, if `x`
        and `y` are measured in V and `fs` is measured in Hz.
        Defaults to 'density'
    axis : int, optional
        Axis along which the FFTs are computed; the default is over the
        last axis (i.e. ``axis=-1``).

    Returns
    -------
    freqs : ndarray
        Array of sample frequencies.
    tt : ndarray
        Array of times corresponding to each data segment
    Sxys : ndarray
        2D array of cross spectral densities for each window


    '''
    # Calculate the FFTs of each data stream, with flag mode='complex'
    # Fxx and Fyy are the short time Fourier transforms
    freqs, tt, Fxx = sig.spectrogram(x, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap,
                nfft=nfft, detrend=detrend, return_onesided=return_onesided,
                scaling=scaling, axis=axis, mode='complex')
    _, _, Fyy = sig.spectrogram(y, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap,
                nfft=nfft, detrend=detrend, return_onesided=return_onesided,
                scaling=scaling, axis=axis, mode='complex')

    Sxys = np.conjugate(Fxx) * Fyy
    if return_onesided:
        if nfft % 2:
            Sxys[..., 1:] *= 2 # double because we want a one-sided csd from the stfts
        else:
            # Last point is unpaired Nyquist freq point, don't double
            Sxys[..., 1:-1] *= 2

    return freqs, tt, Sxys


def get_median_psd(Saas):
    '''Calculate the median PSD from a bunch of PSDs.  No bias is applied.
    '''
    Saa_med = np.median(np.real(Saas), axis=1)
    return Saa_med

def get_median_csd(Sabs):
    '''Calculate the median CSD from a bunch of CSDs.  No bias is applied.
    '''
    Sab_med = np.median(np.real(Sabs), axis=1) + 1j * np.median(np.imag(Sabs), axis=1)
    return Sab_med

def get_median_coherences(Saas, Sbbs, Sabs):
    '''Calculate the median coherences from all the PSDs and CSDs.
    '''
    Saa_med = get_median_psd(Saas)
    Sbb_med = get_median_psd(Sbbs)
    Sab_med = get_median_csd(Sabs)

    coherences = np.abs(Sab_med)**2 / (Saa_med * Sbb_med)

    return coherences

def get_mean_psd(Saas):
    '''Calculate the mean PSD from a bunch of PSDs.
    '''
    Saa_mean = np.mean(np.real(Saas), axis=1)
    return Saa_mean

def get_mean_csd(Sabs):
    '''Calculate the mean CSD from a bunch of CSDs.
    '''
    Sab_mean = np.mean(np.real(Sabs), axis=1) + 1j * np.mean(np.imag(Sabs), axis=1)
    return Sab_mean

def get_mean_coherences(Saas, Sbbs, Sabs):
    '''Calculate the mean coherences from all the PSDs and CSDs.
    '''
    Saa_mean = get_mean_psd(Saas)
    Sbb_mean = get_mean_psd(Sbbs)
    Sab_mean = get_mean_csd(Sabs)

    coherences = np.abs(Sab_mean)**2 / (Saa_mean * Sbb_mean)

    return coherences

def remove_darm_olg_psds(Saas, darm_olg):
    '''Removes the DARM open loop gain from a single DCPD PSD.
    Solves for (H x) squared in Equation 4 of 
    https://dcc.ligo.org/DocDB/0141/T1700131/001/OnlineCrossCorr.pdf
    '''
    num_ff, num_tt = np.shape(Saas) # all must be the same shape

    Sxxs = np.zeros_like(Saas, dtype=complex)
    for ii in np.arange(num_tt):
        Saa = Saas[:,ii]
        Sxx = Saa * 4 * np.abs(1 + darm_olg)**2
        Sxxs[:,ii] = Sxx

    return Sxxs

def remove_darm_olg_from_psd_sum(Sccs, darm_olg):
    '''Removes the DARM open loop gain from a single DCPD PSD.
    Solves for (H x) squared in Equation 4 of 
    https://dcc.ligo.org/DocDB/0141/T1700131/001/OnlineCrossCorr.pdf
    '''
    num_ff, num_tt = np.shape(Sccs) # all must be the same shape

    Szzs = np.zeros_like(Sccs, dtype=complex)
    for ii in np.arange(num_tt):
        Scc = Sccs[:,ii]
        Szz = Scc * np.abs(1 + darm_olg)**2
        Szzs[:,ii] = Szz

    return Szzs

def remove_darm_olg_csds(Saas, Sbbs, Sabs, darm_olg):
    '''Removes the DARM open loop gain from every DCPD CSD,
    via equation 10 of Kiwamu LIGO DCC-T1700131 
    '''
    num_ff, num_tt = np.shape(Saas) # all must be the same shape

    Sxys = np.zeros_like(Saas, dtype=complex)
    for ii in np.arange(num_tt):
        Saa = Saas[:,ii]
        Sbb = Sbbs[:,ii]
        Sab = Sabs[:,ii]

        corr_noise_full =   (np.abs(darm_olg)**2 + 2 * darm_olg) * Saa + \
                            (np.abs(darm_olg)**2 + 2 * np.conj(darm_olg)) * Sbb + \
                            np.abs(darm_olg)**2 * np.conj(Sab) + \
                            np.abs(2 + darm_olg)**2 * Sab

        Sxys[:,ii] = corr_noise_full

    return Sxys

def calibrate_into_meters(Sxys, sensing_function):
    '''Calibrate from mA to meters using the DARM sensing function C in meters/mA
    '''
    num_ff, num_tt = np.shape(Sxys)

    Sxys_cal = np.zeros_like(Sxys,  dtype=complex)
    for ii in np.arange(num_tt):
        Sxy = Sxys[:,ii]
        Sxys_cal[:,ii] = Sxy / np.abs(sensing_function)**2
    return Sxys_cal

def asymmetric_laplace(x, loc, scale, kappa):
    '''Defines an asymmetric laplace probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplacian
    scale   = scale parameter for broadening the PDF width
    kappa   = asymmetric parameter 
    Output:
    pdf     = asymmetric laplace probability distribution function on input x
    '''
    sign = np.sign(x - loc) # either +1 or -1 
    pdf = ( 1 / (scale * (kappa + 1/kappa)) ) * np.exp(-sign * (x - loc) * kappa**sign / scale)
    return pdf

def mid_bins(bins):
    '''Returns the middle of bins given from the native mpl.hist() output'''
    diff_bins = np.diff(bins)
    middle_bins = bins[:-1] + diff_bins
    return middle_bins

def kappa_to_mean_coherence(kappa):
    '''Takes in kappa from asymmetric Laplace distribution.
    Returns what the mean-averaged coherence would be for such a kappa.
    '''
    coherence = ( ( 1 - kappa**2 )/( 1 + kappa**2 ) )**2
    return coherence

#####   Signal processing   #####
channels = np.array([
    'H1:OMC-DCPD_A_OUT_DQ', 
    'H1:OMC-DCPD_B_OUT_DQ', 
    'H1:OMC-DCPD_SUM_OUT_DQ', 
    'H1:OMC-DCPD_NULL_OUT_DQ', 
    'H1:LSC-DARM_IN1_DQ',       # needed for comparing DARM cts to DCPD mA
    ])
dcpd_a = 'H1:OMC-DCPD_A_OUT_DQ'
dcpd_b = 'H1:OMC-DCPD_B_OUT_DQ'

# Balance the pds
ratio_A_over_B = 1.0441 # 1.0 
mutliplier = 1 + (ratio_A_over_B - 1)/2.0
balance_dict = {
    dcpd_a   : 1.01039 / mutliplier,
    dcpd_b   : 0.98961 * mutliplier,
}

### Data file ###
gps_start = 1284249618 # Sep 16 00:00:00 2020, sqz time
gps_stop  = 1284270167 # Sep 16 05:42:29 2020
csd_date = '16 September 2020 sqz'

# gps_start = 1284270168 # Sep 16 05:42:30 2020, no sqz time
# gps_stop  = 1284304916 # Sep 16 15:21:38 2020
# csd_date = '16 September 2020 no sqz'

# gps_start = 1283317218    # September 5, 2020 05:00:00.000 UTC
# gps_stop  = 1283346018    # September 5, 2020 13:00:00.000 UTC
# csd_date = '5 September 2020'

# gps_start = 1282730359          # August 29 2020 09:59:01
# gps_stop  = 1282763937          # August 29 2020 19:18:39
# csd_date = 'August 2020'

# gps_start = 1256380478          # October 2019
# gps_stop = 1256390478
# csd_date = 'October 2019'
# pickle_filename = f'/Volumes/SandiskSD/ccahilla/Git/o3-commissioning-paper/data/gated_DCPD_data_gps_start_1256380478_gps_stop_1256390478.pkl'

pickle_filename = f'/Volumes/SandiskSD/ccahilla/Git/thesis/chapters/correlated_noise/data/DCPD_data_gps_start_{gps_start}_gps_stop_{gps_stop}.pkl'


duration = gps_stop - gps_start
fs = 16384                      # Hz
NN = fs * duration              # total samples

binwidth = 2  # Hz
overlap = 0.0
averages = int( nu.dtt_averages(duration, binwidth, overlap) ) # should have 20000 averages
bin_num = (fs / 2) / binwidth # nyquist / binwidth, should be 4196 

nperseg = NN / averages         # samples per fft segment
noverlap = nperseg * overlap    # samples of overlap between segments
print()
print(f'Averages = {averages}')
print(f'Number of bins = {bin_num}')
print()

# #####   Read in DARM stuff   #####
# DARM_multiplier = 1.0   #
# ratio_sq = 1894811700000.0 # (mA/cts)^2, from ratio of PSD_SUM / PSD_ERR

# # Get calibration functions
# DARM_sensing_data = np.loadtxt(f'{chapter_dir}/data/LHO_pyDARM_sensing_function_mA_per_meter.txt') # DCPD SUM to DARM [actually in cts/meter]
# ff_C = DARM_sensing_data[:,0]
# C_orig = DARM_sensing_data[:,1] * np.exp(1j * DARM_sensing_data[:,2]) # cts/m
# C_orig = C_orig * np.sqrt(ratio_sq) # mA/m = cts/m * mA/cts

# # DARM_OLG_data = np.loadtxt(f'{chapter_dir}/data/LHO_pyDARM_OLG.txt')
# DARM_OLG_data = np.loadtxt(f'{chapter_dir}/data/LHO_pyDARM_OLG_for_HF_interp_from_2019_10_28_H1_0ctSRCLOffset_for_LF.txt')
# ff_G = DARM_OLG_data[:,0]
# G_orig = DARM_OLG_data[:,1] * np.exp(1j * DARM_OLG_data[:,2]) * DARM_multiplier



#####   Get all PSDs and CSDs   #####
# This will be memory heavy
# freq vector is the first axis, time vector is the second
# in other words, the first PSD by itself is Saas[:,0], etc
start_time = time.time()
print('\033[92m')
print(f'Reading data and taking PSDs and CSDs: {pickle_filename}')
print('\033[0m')

# ff, tt, Saas, Sbbs, Sabs = get_all_csds(pickle_filename, dcpd_a, dcpd_b, nperseg, noverlap, balance_dict)
ff, tt, Saas, Sbbs, Sabs, Sccs, Sdds, Sees = get_all_csds_plus_sum(pickle_filename, dcpd_a, dcpd_b, nperseg, noverlap, balance_dict)

print('\033[92m')
print(f'Done in {time.time() - start_time} seconds')
print('\033[0m')

save_pkl = False
save_txts = True

#####   Save PSDs and CSDs  #####
start_time = time.time()

if save_pkl:
    save_filename = f'lho_uncalibrated_dcpd_psds_and_csds_averages_{averages}_gps_start_{gps_start}_gps_stop_{gps_stop}.pkl'
    save_full_filename = f'{external_hd_dir}/{save_filename}'

    save_data_dict = {}
    save_data_dict['dcpd_a_psds'] = Saas
    save_data_dict['dcpd_b_psds'] = Sbbs
    save_data_dict['darm_csds'] = Sabs
    save_data_dict['dcpd_sum'] = Sccs
    save_data_dict['dcpd_null'] = Sdds
    save_data_dict['darm_error'] = Sees

    print('\033[93m')
    print(f'Saving file: {save_full_filename}')
    print('\033[0m')

    with open(save_full_filename, 'wb') as outfile:
        pickle.dump(save_data_dict, outfile, protocol=4)

    print(f'Saved {save_full_filename} in {time.time() - start_time} seconds')

if save_txts:
    #####   Do PSD rejection   #####
    # Reject any PSD value above 14 times the median PSD value.
    # The CDF of the exponential F(14 * rho) = 1 - exp(-14 * log(2) ) = 0.999938
    ff_index = 20 
    outlier_psd_indicies = np.argwhere(np.median(Saas[ff_index,:]) * 14 < Saas[ff_index,:])[:,0]
    good_psd_indicies    = np.argwhere(np.median(Saas[ff_index,:]) * 14 >= Saas[ff_index,:])[:,0]

    bad_averages = len(outlier_psd_indicies)
    good_averages = len(good_psd_indicies)
    print()
    print(f'There were {good_averages} good averages out of {good_averages + bad_averages} total')
    print()

    #####   Postprocess   #####

    good_Saa = get_mean_psd(Saas[:, good_psd_indicies])
    good_Sbb = get_mean_psd(Sbbs[:, good_psd_indicies])
    good_Sab = get_mean_csd(Sabs[:, good_psd_indicies])
    good_Scc = get_mean_psd(Sccs[:, good_psd_indicies])
    good_Sdd = get_mean_psd(Sdds[:, good_psd_indicies])
    good_See = get_mean_psd(Sees[:, good_psd_indicies])

    #####   Save the spectral densities   #####

    save_filename = f'raw_psd_a_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    save_full_filename = f'{data_dir}/{save_filename}'
    save_data = np.vstack((ff, good_Saa)).T
    print('\033[93m')
    print(f'Saving file: {save_full_filename}')
    print('\033[0m')
    np.savetxt(save_full_filename, save_data, header='Frequency [Hz], raw DCPD A PSD [cts/rtHz]')

    save_filename = f'raw_psd_b_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    save_full_filename = f'{data_dir}/{save_filename}'
    save_data = np.vstack((ff, good_Sbb)).T
    print('\033[93m')
    print(f'Saving file: {save_full_filename}')
    print('\033[0m')
    np.savetxt(save_full_filename, save_data, header='Frequency [Hz], raw DCPD A PSD [cts/rtHz]')

    save_filename = f'raw_csd_ab_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    save_full_filename = f'{data_dir}/{save_filename}'
    save_data = np.vstack(( ff, np.abs(good_Sab), np.angle(good_Sab) )).T
    print('\033[93m')
    print(f'Saving file: {save_full_filename}')
    print('\033[0m')
    np.savetxt(save_full_filename, save_data, header='Frequency [Hz], raw DCPD A vs DCPD B CSD [cts/rtHz]')

    save_filename = f'raw_psd_sum_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    save_full_filename = f'{data_dir}/{save_filename}'
    save_data = np.vstack((ff, good_Scc)).T
    print('\033[93m')
    print(f'Saving file: {save_full_filename}')
    print('\033[0m')
    np.savetxt(save_full_filename, save_data, header='Frequency [Hz], raw DCPD SUM PSD [cts/rtHz]')


    save_filename = f'raw_psd_null_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    save_full_filename = f'{data_dir}/{save_filename}'
    save_data = np.vstack((ff, good_Sdd)).T
    print('\033[93m')
    print(f'Saving file: {save_full_filename}')
    print('\033[0m')
    np.savetxt(save_full_filename, save_data, header='Frequency [Hz], raw DCPD SUM PSD [cts/rtHz]')

    save_filename = f'raw_darm_error_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    save_full_filename = f'{data_dir}/{save_filename}'
    save_data = np.vstack((ff, good_See)).T
    print('\033[93m')
    print(f'Saving file: {save_full_filename}')
    print('\033[0m')
    np.savetxt(save_full_filename, save_data, header='Frequency [Hz], raw DARM error [cts/rtHz]')

# else: # This is not saving everything rn
#     #####   Do PSD rejection   #####
#     # Reject any PSD value above 14 times the median PSD value.
#     # The CDF of the exponential F(14 * rho) = 1 - exp(-14 * log(2) ) = 0.999938
#     ff_index = 20 
#     outlier_psd_indicies = np.argwhere(np.median(Saas[ff_index,:]) * 14 < Saas[ff_index,:])[:,0]
#     good_psd_indicies = np.argwhere(np.median(Saas[ff_index,:]) * 14 >= Saas[ff_index,:])[:,0]

#     good_averages = len(good_psd_indicies)



#     #####   Remove DARM OLG from CSDs   #####
#     # If freq vectors are not the same
#     if (len(ff) != len(ff_C)) or (len(ff) != len(ff_G)):
#         C = nu.get_complex_interp(ff, ff_C, C_orig)
#         G = nu.get_complex_interp(ff, ff_G, G_orig)

#     Sxxs = remove_darm_olg_psds(Saas, G)
#     Syys = remove_darm_olg_psds(Sbbs, G)
#     Sxys = remove_darm_olg_csds(Saas, Sbbs, Sabs, G)
#     Szzs = remove_darm_olg_from_psd_sum(Sccs, G)


#     #####   Calibrate into meters   #####
#     Sxxs_cal = calibrate_into_meters(Sxxs, C)
#     Syys_cal = calibrate_into_meters(Syys, C)
#     Sxys_cal = calibrate_into_meters(Sxys, C)
#     Szzs_cal = calibrate_into_meters(Szzs, C)

#     Sxxs = 0
#     Syys = 0
#     Sxys = 0
#     Szzs = 0


#     #####   Make PSDs real   #####
#     Sxxs_cal = np.real(Sxxs_cal)
#     Syys_cal = np.real(Syys_cal)
#     Szzs_cal = np.real(Szzs_cal)



#     #####   Save PSDs and CSDs  #####
#     start_time = time.time()

#     save_filename = f'lho_correlated_noise_psds_and_csds_averages_{averages}_gps_start_{gps_start}_gps_stop_{gps_stop}.pkl'
#     save_full_filename = f'{external_hd_dir}/{save_filename}'

#     save_data_dict = {}
#     save_data_dict['dcpd_a_psds'] = Sxxs_cal
#     save_data_dict['dcpd_b_psds'] = Syys_cal
#     save_data_dict['darm_csds'] = Sxys_cal
#     save_data_dict['dcpd_sum'] = Szzs_cal

#     print('\033[93m')
#     print(f'Saving file: {save_full_filename}')
#     print('\033[0m')

#     with open(save_full_filename, 'wb') as outfile:
#         pickle.dump(save_data_dict, outfile, protocol=4)

#     print(f'Saved {save_full_filename} in {time.time() - start_time} seconds')



# nu.save_pickle(save_data_dict, save_full_filename)

# np.savetxt(save_full_filename, save_data, 
#     header= 'LHO DCPD A PSDs [m^2/Hz]' + '\n' + \
#             f'gps_start = {gps_start}' + '\n' + \
#             f'gps_stop = {gps_stop}' + '\n' + \
#             f'averages = {averages}' + '\n' + \
#             f'channels = H1:OMC-DCPD_A_OUT_DQ'
#             )

# print(f'Saved {save_full_filename} in {time.time() - start_time} seconds')
# start_time = time.time()

# save_filename = f'lho_dcpd_b_psds_averages_{averages}_gps_start_{gps_start}_gps_stop_{gps_stop}.pkl'
# save_full_filename = f'{external_hd_dir}/{save_filename}'
# save_data = Syys_cal
# print('\033[93m')
# print(f'Saving file: {save_full_filename}')
# print('\033[0m')
# np.savetxt(save_full_filename, save_data, 
#     header= 'LHO DCPD B PSDs [m^2/Hz]' + '\n' + \
#             f'gps_start = {gps_start}' + '\n' + \
#             f'gps_stop = {gps_stop}' + '\n' + \
#             f'averages = {averages}' + '\n' + \
#             f'channels = H1:OMC-DCPD_A_OUT_DQ'
#             )

# print(f'Saved {save_full_filename} in {time.time() - start_time} seconds')
# start_time = time.time()

# save_filename = f'lho_correlated_noise_csds_averages_{averages}_gps_start_{gps_start}_gps_stop_{gps_stop}.pkl'
# save_full_filename = f'{external_hd_dir}/{save_filename}'
# save_data = Sxys_cal
# print('\033[93m')
# print(f'Saving file: {save_full_filename}')
# print('\033[0m')
# np.savetxt(save_full_filename, save_data, 
#     header= 'LHO correlated noise CSDs [m^2/Hz]' + '\n' + \
#             f'gps_start = {gps_start}' + '\n' + \
#             f'gps_stop = {gps_stop}' + '\n' + \
#             f'averages = {averages}' + '\n' + \
#             f'channels = H1:OMC-DCPD_A_OUT_DQ + H1:OMC-DCPD_B_OUT_DQ'
#             )

# print(f'Saved {save_full_filename} in {time.time() - start_time} seconds')
# start_time = time.time()