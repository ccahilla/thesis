'''
lho_xcor_animation.py

Animates the CSD data averaging down.

Craig Cahillane
September 29, 2020
'''

import os
import time
import pickle
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt

import nds2utils as nu
from nds2utils.make_interactive_svg import make_interactive_svg

from scipy.optimize import fsolve
from scipy.optimize import curve_fit
from matplotlib.animation import FuncAnimation

mpl.rcParams.update({'figure.figsize': (12, 9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'lines.markersize': 3,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.9,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'legend.columnspacing': 2,
                     'savefig.dpi': 80,
                     'pdf.compression': 9})



#####   Git repo path   #####

script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

# Place to save large data
if 'ccahilla' in chapter_dir:
    external_hd_dir = '/Volumes/SandiskSD/ccahilla/Git/thesis/chapters/correlated_noise/data' # Craig-specific code
else:
    external_hd_dir = f'{chapter_dir}' # save on personal computer, might be pretty large (~10 GB data)

#####   Functions   #####

def load_psd_txt(filename):
    '''Loads 2D array of psds and csds from filename
    '''
    data = np.loadtxt(filename, dtype=complex)
    return data

def median_coherence(power_ratio):
    '''Given a power_ratio of uncorrelated noise over correlated noise, 
    power_ratio = sigma_uncorr^2 / sigma_corr^2,
    returns the power coherence gamma^2 that would result from median-averaged PSDs and CSDs.
    '''
    med_coh =   (power_ratio**2 * np.log(1 + 1/np.sqrt(1 + power_ratio))**2) \
                / (4 * np.log(2)**2 * (1 + power_ratio) * (2 + power_ratio - 2 * np.sqrt(1 + power_ratio)) )
    return med_coh

def residual_median_coherence(input_array, median_coherence_desired):
    '''Finds the residual between the coherence desired and the numerically attempt from fsolve().
    For use as the function input to fsolve().
    Inputs:
    input_array = array of length one, required by fsolve to find the power ratio numerically
    median_coherence_desired = median coherence estimated from the CSD signals.
    Output:
    residual = array of length one with residual from median_coherence() function
    '''
    func_results = median_coherence(input_array[0])
    residual = [func_results - median_coherence_desired]
    return residual

def bias_from_median_coherence_and_power_ratio(median_coherence, power_ratio):
    '''Calculates the mean-to-median bias factor from the median_coherence = gamma^2
    and the power_ratio = epsilon.
    '''
    bias = np.log(2) * np.sqrt( (1 + power_ratio) * median_coherence )
    return bias

def bias_from_median_coherence(median_coherence_estimated, initial_power_ratio=0.1):
    '''Estimates the median/mean bias = b given some median_coherence = gamma^2.

    Numerically solves for the uncorrelated/correated power_ratio = epsilon,
    using the median_coherence.
    Uses scipy.optimize.fsolve() to find the root of residual_median_coherence().
    fsolve() seems to work for gamma^2 between (0.999 and 1e-6)

    This function requires bias_from_median_coherence_and_power_ratio(),
    residual_median_coherence(), median_coherence(), and fsolve() functions.

    Inputs:
    median_coherence_estimated  =   median coherence estimated from |<x,z>|^2/(<x,x> <z,z>) 
                                    where all spectral densities are median-averaged
    initial_power_ratio         =   initial guess of the power_ratio, default is 1.0
    Output:
    bias    =   median/mean bias factor.  Divide median-averaged cross spectral density <x,z> 
                by bias to recover the mean-avearged cross spectral density.             
    '''
    # Numerically estimate the power ratio epsilon from the median coherence
    fsolve_array = fsolve(  residual_median_coherence, 
                            [initial_power_ratio], 
                            args=(median_coherence_estimated))
    power_ratio = fsolve_array[0]

    # Find the bias factor
    bias = bias_from_median_coherence_and_power_ratio(  median_coherence_estimated, 
                                                        power_ratio)
    return bias

def biases_from_coherences(median_coherences, initial_power_ratios=None):
    '''Returns array of biases from an array of coherences.
    Inputs:
    median_coherences =    median-averaged coherence estimates
    initial_power_ratios =  power ratios to start estimation with.  
                            If None, uses 0.1 for all.  Default is None.
    Outputs:
    biases =    array of biases calculated from the coherences
    '''
    if initial_power_ratios is None:
        initial_power_ratios = 0.1 * np.ones_like(median_coherences)
    biases = np.array([])
    for coherence, initial_power_ratio in zip(median_coherences, initial_power_ratios):
        bias = bias_from_median_coherence(coherence, initial_power_ratio=initial_power_ratio)
        biases = np.append(biases, bias)
    return biases

def get_median_psd(Saas):
    '''Calculate the median PSD from a bunch of PSDs.  No bias is applied.
    '''
    Saa_med = np.median(np.real(Saas), axis=1)
    return Saa_med

def get_median_csd(Sxys, phase_corrected=True, axis=-1):
    '''Calculate the median CSD from a bunch of CSDs.  
    No median-to-mean bias is applied.
    phase_corrected=True means the median csd is found, 
    then the phase is used to rotate each CSD such that the median CSD phase = 0,
    then the median is found again.
    This avoids the biasing due to the taking the median with some non-zero phase.
    '''
    Sxy_med = np.median(np.real(Sxys), axis=axis) + 1j * np.median(np.imag(Sxys), axis=axis)
    
    if phase_corrected:
        phases = np.angle(Sxy_med)
        Sxys = Sxys * np.exp(-1j * phases)[:,np.newaxis]
        Sxy_med = np.median(np.real(Sxys), axis=1) + 1j * np.median(np.imag(Sxys), axis=1)
        Sxy_med = Sxy_med * np.exp(1j * phases)

    return Sxy_med

def get_median_coherences(Saas, Sbbs, Sabs):
    '''Calculate the median coherences from all the PSDs and CSDs.
    '''
    Saa_med = get_median_psd(Saas)
    Sbb_med = get_median_psd(Sbbs)
    Sab_med = get_median_csd(Sabs)

    coherences = np.abs(Sab_med)**2 / (Saa_med * Sbb_med)

    return coherences

def get_mean_psd(Saas):
    '''Calculate the mean PSD from a bunch of PSDs.
    '''
    Saa_mean = np.mean(np.real(Saas), axis=1)
    return Saa_mean

def get_mean_csd(Sabs):
    '''Calculate the mean CSD from a bunch of CSDs.
    '''
    Sab_mean = np.mean(np.real(Sabs), axis=1) + 1j * np.mean(np.imag(Sabs), axis=1)
    return Sab_mean

def get_mean_coherences(Saas, Sbbs, Sabs):
    '''Calculate the mean coherences from all the PSDs and CSDs.
    '''
    Saa_mean = get_mean_psd(Saas)
    Sbb_mean = get_mean_psd(Sbbs)
    Sab_mean = get_mean_csd(Sabs)

    coherences = np.abs(Sab_mean)**2 / (Saa_mean * Sbb_mean)

    return coherences

def remove_darm_olg_psds(Saas, darm_olg):
    '''Removes the DARM open loop gain from a single DCPD PSD.
    Solves for (H x) squared in Equation 4 of 
    https://dcc.ligo.org/DocDB/0141/T1700131/001/OnlineCrossCorr.pdf
    '''
    num_ff, num_tt = np.shape(Saas) # all must be the same shape

    Sxxs = np.zeros_like(Saas, dtype=complex)
    for ii in np.arange(num_tt):
        Saa = Saas[:,ii]
        Sxx = Saa * 4 * np.abs(1 + darm_olg)**2
        Sxxs[:,ii] = Sxx

    return Sxxs


def remove_darm_olg_psds_and_csds(Saas, Sbbs, Sabs, darm_olg):
    '''Removes the DARM open loop gain from a single DCPD PSD.
    Solves for (H x) squared in Equation 4 of 
    https://dcc.ligo.org/DocDB/0141/T1700131/001/OnlineCrossCorr.pdf
    '''
    num_ff, num_tt = np.shape(Saas) # all must be the same shape

    Sxxs = np.zeros_like(Saas, dtype=complex)
    for ii in np.arange(num_tt):
        Saa = Saas[:,ii]
        Sbb = Sbbs[:,ii]
        Sab = Sabs[:,ii]

        Sxx = Saa + Sbb + 2 * np.real(Sab)
        Sxx = Sxx * np.abs(1 + darm_olg)**2

        Sxxs[:,ii] = Sxx

    return Sxxs

def darm_psd_from_dcpd_sum(Scc, darm_olg):
    '''Removes the DARM open loop gain from a single DCPD PSD.
    Solves for (H x) squared in Equation 4 of 
    https://dcc.ligo.org/DocDB/0141/T1700131/001/OnlineCrossCorr.pdf
    '''
    darm_psd = Scc * np.abs(1 + darm_olg)**2
    return darm_psd

def darm_psd_from_dcpds(Saa, Sbb, Sab, darm_olg):
    '''Removes the DARM open loop gain from a single DCPD PSD.
    Solves for (H x) squared in Equation 4 of 
    https://dcc.ligo.org/DocDB/0141/T1700131/001/OnlineCrossCorr.pdf
    '''
    Sxx = Saa + Sbb + 2 * np.real(Sab)
    darm_psd = Sxx * np.abs(1 + darm_olg)**2
    return darm_psd

def remove_darm_olg_from_psd_sum(Sccs, darm_olg):
    '''Removes the DARM open loop gain from a single DCPD PSD.
    Solves for (H x) squared in Equation 4 of 
    https://dcc.ligo.org/DocDB/0141/T1700131/001/OnlineCrossCorr.pdf
    '''
    num_ff, num_tt = np.shape(Sccs) # all must be the same shape

    Szzs = np.zeros_like(Sccs, dtype=complex)
    for ii in np.arange(num_tt):
        Scc = Sccs[:,ii]
        Szz = Scc * np.abs(1 + darm_olg)**2
        Szzs[:,ii] = Szz

    return Szzs

def remove_darm_olg_csds(Saas, Sbbs, Sabs, darm_olg):
    '''Removes the DARM open loop gain from every DCPD CSD,
    via equation 10 of Kiwamu LIGO DCC-T1700131 
    '''
    num_ff, num_tt = np.shape(Saas) # all must be the same shape

    Sxys = np.zeros_like(Saas, dtype=complex)
    for ii in np.arange(num_tt):
        Saa = Saas[:,ii]
        Sbb = Sbbs[:,ii]
        Sab = Sabs[:,ii]

        corr_noise_full =   (np.abs(darm_olg)**2 + 2 * darm_olg) * Saa + \
                            (np.abs(darm_olg)**2 + 2 * np.conj(darm_olg)) * Sbb + \
                            np.abs(darm_olg)**2 * np.conj(Sab) + \
                            np.abs(2 + darm_olg)**2 * Sab

        Sxys[:,ii] = corr_noise_full

    return Sxys

def calibrate_into_meters(Sxys, sensing_function):
    '''Calibrate from mA to meters using the DARM sensing function C in meters/mA
    '''
    num_ff, num_tt = np.shape(Sxys)

    Sxys_cal = np.zeros_like(Sxys,  dtype=complex)
    for ii in np.arange(num_tt):
        Sxy = Sxys[:,ii]
        Sxys_cal[:,ii] = Sxy / np.abs(sensing_function)**2
    return Sxys_cal

def asymmetric_laplace(x, loc, scale, kappa):
    '''Defines an asymmetric laplace probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplacian
    scale   = scale parameter for broadening the PDF width
    kappa   = asymmetric parameter 
    Output:
    pdf     = asymmetric laplace probability distribution function on input x
    '''
    sign = np.sign(x - loc) # either +1 or -1 
    pdf = ( 1 / (scale * (kappa + 1/kappa)) ) * np.exp(-sign * (x - loc) * kappa**sign / scale)
    return pdf

def mid_bins(bins):
    '''Returns the middle of bins given from the native mpl.hist() output'''
    diff_bins = np.diff(bins)
    middle_bins = bins[:-1] + diff_bins
    return middle_bins

def kappa_to_mean_coherence(kappa):
    '''Takes in kappa from asymmetric Laplace distribution.
    Returns what the mean-averaged coherence would be for such a kappa.
    '''
    coherence = ( ( 1 - kappa**2 )/( 1 + kappa**2 ) )**2
    return coherence



#####   Load PSDs and CSDs   #####

gps_start = 1283317218    # September 5, 2020 05:00:00.000 UTC
gps_stop  = 1283346018    # September 5, 2020 13:00:00.000 UTC
averages = 57600
csd_date = 'September 2020'
filename = f'{external_hd_dir}/lho_uncalibrated_dcpd_psds_and_csds_averages_57600_gps_start_1283317218_gps_stop_1283346018.pkl'

# gps_start = 1282730359          # August 29 2020 09:59:01
# gps_stop  = 1282763937          # August 29 2020 19:18:39
# averages = 67156
# csd_date = 'August 2020'
# filename = f'{external_hd_dir}/lho_uncalibrated_dcpd_psds_and_csds_averages_67156_gps_start_1282730359_gps_stop_1282763937.pkl'

# gps_start = 1256380478          # October 2019
# gps_stop = 1256390478
# averages = 20000
# csd_date = 'October 2019'
# filename = f'{external_hd_dir}/lho_uncalibrated_dcpd_psds_and_csds_averages_20000_gps_start_1256380478_gps_stop_1256390478.pkl'

fs = 16384    # Hz
binwidth = 2  # Hz
fnyquist = fs // 2  # Hz
ff = np.linspace(0, fnyquist, fnyquist // binwidth + 1)

#####   Read in DARM stuff   #####

if csd_date == 'September 2020':
    ratio_psd = 3906644989609.
elif csd_date == 'August 2020':
    ratio_psd = 1927663208073.
elif csd_date == 'October 2019':
    ratio_psd = 1895693131538.  # mA^2/cts^2

ratio_sq = np.sqrt(ratio_psd) # 1894811700000.0 # (mA/cts)^2, from ratio of PSD_SUM / PSD_ERR
DARM_multiplier = 1.0   #

# Get calibration functions
DARM_sensing_data = np.loadtxt(f'{chapter_dir}/data/LHO_pyDARM_sensing_function_mA_per_meter.txt') # DCPD SUM to DARM [actually in cts/meter]
ff_C = DARM_sensing_data[:,0]
C_orig = DARM_sensing_data[:,1] * np.exp(1j * DARM_sensing_data[:,2]) # cts/m
C_orig = C_orig * ratio_sq # mA/m = cts/m * mA/cts

# DARM_OLG_data = np.loadtxt(f'{chapter_dir}/data/LHO_pyDARM_OLG.txt')
DARM_OLG_data = np.loadtxt(f'{chapter_dir}/data/LHO_pyDARM_OLG_for_HF_interp_from_2019_10_28_H1_0ctSRCLOffset_for_LF.txt')
ff_G = DARM_OLG_data[:,0]
G_orig = DARM_OLG_data[:,1] * np.exp(1j * DARM_OLG_data[:,2]) * DARM_multiplier



start_time = time.time()

with open(filename, 'rb') as infile:
    data_dict = pickle.load(infile)
Saas = data_dict['dcpd_a_psds']
Sbbs = data_dict['dcpd_b_psds'] 
Sabs = data_dict['darm_csds']
Sccs = data_dict['dcpd_sum'] 

print(f'Loaded {filename} in {time.time() - start_time} seconds')



#####   Do PSD rejection   #####
# Reject any PSD value above 14 times the median PSD value.
# The CDF of the exponential F(14 * rho) = 1 - exp(-14 * log(2) ) = 0.999938
ff_index = 20 
outlier_psd_indicies = np.argwhere(np.median(Saas[ff_index,:]) * 14 < Saas[ff_index,:])[:,0]
good_psd_indicies = np.argwhere(np.median(Saas[ff_index,:]) * 14 >= Saas[ff_index,:])[:,0]

good_averages = len(good_psd_indicies)



#####   Remove DARM OLG from CSDs   #####
# If freq vectors are not the same
if (len(ff) != len(ff_C)) or (len(ff) != len(ff_G)):
    C = nu.get_complex_interp(ff, ff_C, C_orig)
    G = nu.get_complex_interp(ff, ff_G, G_orig)

Sxxs = remove_darm_olg_psds(Saas, G)
Syys = remove_darm_olg_psds(Sbbs, G)
Sxys = remove_darm_olg_csds(Saas, Sbbs, Sabs, G)
Szzs = remove_darm_olg_from_psd_sum(Sccs, G)
Svvs = remove_darm_olg_psds_and_csds(Saas, Sbbs, Sabs, G)

#####   Calibrate into meters   #####
Sxxs_cal = calibrate_into_meters(Sxxs, C)
Syys_cal = calibrate_into_meters(Syys, C)
Sxys_cal = calibrate_into_meters(Sxys, C)
Szzs_cal = calibrate_into_meters(Szzs, C)
Svvs_cal = calibrate_into_meters(Svvs, C)

Sxxs = 0
Syys = 0
Sxys = 0
Szzs = 0
Svvs = 0

#####   Make PSDs real   #####
Sxxs_cal = np.real(Sxxs_cal)
Syys_cal = np.real(Syys_cal)
Szzs_cal = np.real(Szzs_cal)
Svvs_cal = np.real(Svvs_cal)

#####   Postprocess   #####

plot_Sxxs = Sxxs_cal
plot_Syys = Syys_cal
plot_Sxys = Sxys_cal
plot_Szzs = Szzs_cal
plot_Svvs = Svvs_cal

units = 'meters'
plot_label = f'gps_start_{gps_start}'

# mean_darm_psd, median_darm_psd, mean_csd, median_csd = get_mean_and_median_psds_and_csds(plot_Sxxs, plot_Syys, plot_Sxys)

mean_darm_psd   = get_mean_psd(plot_Szzs)
median_darm_psd = get_median_psd(plot_Szzs)
mean_csd        = get_mean_csd(plot_Sxys)
median_csd      = get_median_csd(plot_Sxys)

good_mean_darm_psd  = get_mean_psd(plot_Svvs[:, good_psd_indicies])
good_mean_csd       = get_mean_csd(plot_Sxys[:, good_psd_indicies])

# Mean coherence from PSD rejection  
good_mean_coherence = get_mean_coherences(  plot_Sxxs[:, good_psd_indicies], 
                                            plot_Syys[:, good_psd_indicies], 
                                            plot_Sxys[:, good_psd_indicies])



# Get median coherences and biases
median_coherences = get_median_coherences( plot_Sxxs, plot_Syys, plot_Sxys )
biases = biases_from_coherences(median_coherences)

# PSDs and CSDs, with mean-to-median bias correction
mean_darm_asd = np.sqrt(mean_darm_psd)
median_darm_asd = np.sqrt(median_darm_psd / np.log(2))
mean_asd = np.sqrt(np.abs(mean_csd))
median_asd_bias_corrected = np.sqrt( np.abs(median_csd) / biases )

good_mean_darm_asd = np.sqrt(good_mean_darm_psd)
good_mean_asd = np.sqrt(np.abs(good_mean_csd))

# Save the median asd 
data_dir = f'{chapter_dir}/data'

save_filename = f'lho_correlated_noise_median_averaged_asd_bias_corrected_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
save_full_filename = f'{data_dir}/{save_filename}'
save_data = np.vstack((ff, median_asd_bias_corrected)).T
print('\033[93m')
print(f'Saving file: {save_full_filename}')
print('\033[0m')
np.savetxt(save_full_filename, save_data, header='Frequency [Hz], LHO correlated noise median averaged asd bias corrected [m/rtHz]')

save_filename = f'lho_darm_noise_asd_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
save_full_filename = f'{data_dir}/{save_filename}'
save_data = np.vstack((ff, median_darm_asd)).T
print('\033[93m')
print(f'Saving file: {save_full_filename}')
print('\033[0m')
np.savetxt(save_full_filename, save_data, header='Frequency [Hz], LHO DARM noise median averaged asd bias corrected [m/rtHz]')



#####   Figures   #####
plot_names = np.array([])

# Plot every nth Sxys_cal we've taken, along with the 90% confidence intervals
fig, (s1) = plt.subplots(1)

ff_num, tt_num = np.shape(plot_Sxys)

s1.loglog(ff, mean_darm_asd, color='C6', label='Mean PSD')
s1.loglog(ff, median_darm_asd, color='C8', label='Median PSD (' + r'$\log(2)$' + f' bias applied)')

s1.loglog(ff, mean_asd, color='C1', label='Mean CSD')
s1.loglog(ff, median_asd_bias_corrected, ls='-', color='C3', alpha=1.0, label=f'Median CSD with bias applied')

s1.loglog(ff, good_mean_darm_asd, ls='--', color='C9', label=f'Mean PSD (using PSD rejection) - {good_averages} averages')
s1.loglog(ff, good_mean_asd, ls='--', color='C2', label=f'Mean CSD (using PSD rejection) - {good_averages} averages')

s1.set_xlim([2e1, 5e3])
s1.set_ylim([1e-21, 1e-18])

s1.set_title(f'LHO correlated noise - {csd_date} - {tt_num} averages')
s1.set_xlabel(f'Frequency [Hz]')
s1.set_ylabel(r'DARM [$\mathrm{m}/\sqrt{\mathrm{Hz}}$]')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.7)
s1.legend(fontsize=12, markerscale=3)

plot_name = f'lho_correlated_noise_all_csds_averages_{int(averages)}_{plot_label}.pdf'
full_plot_name = f'{fig_dir}/{plot_name}'

make_interactive_svg( fig, full_plot_name.replace('.pdf','') )

plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.tight_layout()
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Plot phase, coherence, and biases
fig, (s1, s2, s3) = plt.subplots(3)

s1.loglog(ff, good_mean_coherence, color='C4', label=f'Mean coherence (via PSD rejection) - {good_averages} averages')
s1.loglog(ff, median_coherences, color='C5', label='Median coherence')
s2.semilogx(ff, biases, color='C6', label='Mean to median biases')
s3.semilogx(ff, np.angle(good_mean_csd, deg=True), label='Phase')
# s2.semilogx(ff, biases2, color='C7', alpha=0.6, label='Mean to median biases (Eq. A.87)')


s1.set_title(f'Median coherence and bias - {tt_num} averages')
s1.set_ylabel(f'Median coherence ' + r'$\widetilde{ \gamma^2_{xy} }$')
s2.set_ylabel(f'Bias ' + r'$b = \rho / \mu $')
s3.set_ylabel(f'CSD Phase')
s3.set_xlabel(f'Frequency [Hz]')

s1.set_yticks(nu.good_ticks(s1))

s1.set_xlim([4e1, 5e3])
s2.set_xlim([4e1, 5e3])
s2.set_ylim([0.5, np.log(2)])
s3.set_xlim([4e1, 5e3])
s3.set_ylim([-180, 180])

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.7)
s1.legend()

s2.grid()
s2.grid(which='minor', ls='--', alpha=0.7)
s2.legend()

plot_name = f'coherence_and_bias_and_csd_phase_averages_{averages}_{plot_label}.pdf'
full_plot_name = f'{fig_dir}/{plot_name}'
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.tight_layout()
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Plot phase
fig, (s1) = plt.subplots(1)

# manipulate the phase calculation so it wraps not at 180 degrees
temp = good_mean_csd * np.exp(-1j * np.pi/2)
temp_angle = np.angle(temp)
plot_angle = temp_angle + np.pi/2

s1.semilogx(ff, 180/np.pi * plot_angle, label='Phase')
# s2.semilogx(ff, biases2, color='C7', alpha=0.6, label='Mean to median biases (Eq. A.87)')

s1.set_ylabel(f'CSD Phase')
s1.set_xlabel(f'Frequency [Hz]')

s1.set_xlim([4e1, 5e3])
s1.set_ylim([-90, 270])

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.7)
# s1.legend()

plot_name = f'csd_phase_averages_{averages}_{plot_label}.pdf'
full_plot_name = f'{fig_dir}/{plot_name}'
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.tight_layout()
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



make_2d_plots = False
if make_2d_plots:
    # CSD 2D histogram of some individual frequency bins
    plot_ffs = np.array([30, 50, 200, 700, 2300, 4500]) #, 350, 450, 700, 1400, 2400, 4500])
    plot_ff_indicies = plot_ffs // 2

    for plot_ff, plot_ff_index in zip(plot_ffs, plot_ff_indicies):
        # if not plot_ff == 200:
        #     continue

        Sxys_mid = plot_Sxys[plot_ff_index, good_psd_indicies] # get every CSDs result at plot_ff frequency, except the outliers
        Sxys_mid_real = np.real(Sxys_mid)
        Sxys_mid_imag = np.imag(Sxys_mid)

        Sxys_mid_real_mean = np.mean(Sxys_mid_real)
        Sxys_mid_real_median = np.median(Sxys_mid_real)

        Sxys_mid_imag_mean = np.mean(Sxys_mid_imag)
        Sxys_mid_imag_median = np.median(Sxys_mid_imag)


        fig, ss = plt.subplots(2, 2, sharex="col", sharey="row", figsize=(12, 12),
                                gridspec_kw=dict(   height_ratios=[1, 2.5],
                                                    width_ratios=[2.5, 1])
                                )
        s1 = ss[0, 0]   # top histogram
        s2 = ss[0, 1]   # delete
        s3 = ss[1, 0]   # 2d hist
        s4 = ss[1, 1]   # right histogram

        fig.delaxes(s2) # delete

        num_bins = 300

        confidence_interval = 99.9
        re_low_bin  = np.percentile(Sxys_mid_real, 50 - confidence_interval/2)
        re_high_bin = np.percentile(Sxys_mid_real, 50 + confidence_interval/2)
        im_low_bin  = np.percentile(Sxys_mid_imag, 50 - confidence_interval/2)
        im_high_bin = np.percentile(Sxys_mid_imag, 50 + confidence_interval/2)

        rebins = np.linspace(re_low_bin, re_high_bin, num_bins+1)
        imbins = np.linspace(im_low_bin, im_high_bin, num_bins+1)    

        # Histograms
        reNN, rebins, repatches = s1.hist(Sxys_mid_real, bins=rebins, histtype='step', lw=2, zorder=3, density=True,
                                            label=r'$\Re(S_{xy})$ ' + 'CSD samples')  
        imNN, imbins, impatches = s4.hist(Sxys_mid_imag, bins=imbins, histtype='step', lw=2, zorder=3, density=True, orientation='horizontal',
                                            label=r'$\Im(S_{xy})$ ' + 'CSD samples')  
        h, xedges, yedges, image = s3.hist2d(Sxys_mid_real, Sxys_mid_imag, bins=(rebins, imbins), density=True, 
                                            cmap=mpl.cm.copper, norm=mpl.colors.LogNorm())   

        # Fits
        xx = np.linspace(rebins[0], rebins[-1], 1001)

        rescaler = np.max(reNN) # should be a huge number
        rep0 = [0, 1, 0.9]
        middle_bins = mid_bins(rebins)
        laplace_xy_real_fit_params, laplace_xy_real_fit_cov  = curve_fit(asymmetric_laplace, middle_bins * rescaler, reNN/rescaler, p0=rep0)
        laplace_xy_real_fit_params[0] /= rescaler
        laplace_xy_real_fit_params[1] /= rescaler
        print(f'laplace_xy_real_fit_params = {laplace_xy_real_fit_params}')
        laplace_xy_real_pdf_fit = asymmetric_laplace(xx, *laplace_xy_real_fit_params)

        yy = np.linspace(imbins[0], imbins[-1], 1001)

        imscaler = np.max(imNN) # should be a huge number
        imp0 = [0, 1, 1.0]
        middle_bins = mid_bins(imbins)
        laplace_xy_imag_fit_params, laplace_xy_imag_fit_cov  = curve_fit(asymmetric_laplace, middle_bins * imscaler, imNN/imscaler, p0=imp0)
        laplace_xy_imag_fit_params[0] /= imscaler
        laplace_xy_imag_fit_params[1] /= imscaler
        print(f'laplace_xy_imag_fit_params = {laplace_xy_imag_fit_params}')
        laplace_xy_imag_pdf_fit = asymmetric_laplace(yy, *laplace_xy_imag_fit_params)

        # Plot fit and stats
        s1.plot(xx, laplace_xy_real_pdf_fit, color='k', lw=4, 
                label='Asymmetric Laplace PDF ' + r'$\Re(S_{xy})$' + '\n' + \
                    f'center $m$ = {laplace_xy_real_fit_params[0]:.2e}' + '\n' + \
                    f'scale $\lambda$ = {laplace_xy_real_fit_params[1]:.2e}' + '\n' + \
                    f'skew $\kappa$ = {laplace_xy_real_fit_params[2]:.2e}'
                    )

        s1.axvline(Sxys_mid_real_mean, ymin=0, ymax=np.max(reNN)*1.2, color='C2', label=f'mean = {Sxys_mid_real_mean:.2e}')
        s1.axvline(Sxys_mid_real_median, ymin=0, ymax=np.max(reNN)*1.2, ls='--', color='C3', label=f'median = {Sxys_mid_real_median:.2e}')
        # s1.axvline(Sxys_mid_real_rms, ymin=0, ymax=np.max(reNN)*1.2, ls=':', color='C4', label=f'rms = {Sxys_mid_real_rms:.2e}')

        # imNN, imbins, impatches = s2.hist(Sxys_mid_imag, bins=bins, histtype='step', lw=2, zorder=3, density=True,
        #                             label='CSD imaginary samples')   

        s4.plot(laplace_xy_imag_pdf_fit, yy, color='k', lw=4, 
                label='Asymmetric Laplace PDF ' + r'$\Im(S_{xy})$' + '\n' + \
                    f'center $m$ = {laplace_xy_imag_fit_params[0]:.2e}' + '\n' + \
                    f'scale $\lambda$ = {laplace_xy_imag_fit_params[1]:.2e}' + '\n' + \
                    f'skew $\kappa$ = {laplace_xy_imag_fit_params[2]:.2e}'
                    )
        s4.axhline(Sxys_mid_imag_mean, xmin=0, xmax=np.max(imNN)*1.2, color='C2', label=f'mean = {Sxys_mid_imag_mean:.2e}')
        s4.axhline(Sxys_mid_imag_median, xmin=0, xmax=np.max(imNN)*1.2, ls='--', color='C3', label=f'median = {Sxys_mid_imag_median:.2e}')
        # s4.axhline(Sxys_mid_imag_rms, xmin=0, xmax=np.max(imNN)*1.2, ls=':', color='C4', label=f'rms = {Sxys_mid_imag_rms:.2e}')

        s1.set_title(f'LHO correlated CSD '+r'$f =$' + f' {plot_ff} Hz', x=0.7)

        s3.set_xlabel(r'$\Re{(\mathrm{CSD})}$ values [$\mathrm{m^2}/\mathrm{Hz}$]')
        s3.set_ylabel(r'$\Im{(\mathrm{CSD})}$ values [$\mathrm{m^2}/\mathrm{Hz}$]')

        # s2.set_title(r'$\Im{(\mathrm{CSD})}$ ' + f'{avg} averages')
        # s2.set_xlabel(r'$\Im{(\mathrm{CSD})}$ values [$\mathrm{V^2}/\mathrm{Hz}$]')
        # s2.set_ylabel('Number of Occurances')

        s1.legend(fontsize=12, bbox_to_anchor=(1.0, 1.0))
        s4.legend(fontsize=12, bbox_to_anchor=(0.95, 1.25))

        s1.grid()
        s3.grid()
        s4.grid()

        # s2.legend()
        # s2.grid()

        plot_name = f'dcpd_csd_2d_histograms_for_frequency_bin_{plot_ff}_Hz_{plot_label}.pdf'
        full_plot_name = '{}/{}'.format(fig_dir, plot_name)
        plot_names = np.append(plot_names, full_plot_name)
        print('Writing plot PDF to {}'.format(full_plot_name))
        plt.savefig(full_plot_name, bbox_inches='tight')
        plt.close()


# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()



make_animation = False
if make_animation:
    ###  Make animation of CSD averaging  ###
    fig, s1 = plt.subplots(1) 

    ln_list = []
    alpha = 1.0

    ln0, = s1.loglog(ff, np.ones_like(ff), alpha=alpha, label='Mean correlated noise')
    ln1, = s1.loglog(ff, np.ones_like(ff), alpha=alpha, label='Median correlated noise')
    ln2, = s1.loglog(ff, np.ones_like(ff), alpha=alpha, label='Mean PSD')
    ln3, = s1.loglog(ff, np.ones_like(ff), alpha=alpha, label='Median PSD')

    ln_list.append(ln0)
    ln_list.append(ln1)
    ln_list.append(ln2)
    ln_list.append(ln3)

    def init_welch(): 
        frame = 0
        s1.set_title('LHO correlated noise - August 2020 - ' + r'Averages n = ' + f'{frame}')
        s1.set_xlabel('Frequency [Hz]')
        s1.set_ylabel('DARM [$\mathrm{m}/\sqrt{ \mathrm{Hz} }$]')

        s1.set_xlim([2e1, 5e3])
        s1.set_ylim([5e-21, 1e-18])

        s1.grid()
        s1.grid(which='minor', ls='--', alpha=0.5)

        s1.legend(loc='upper right')

        return ln_list


    num_of_frames = 100 #266 #100 # should take around 45 minutes with 100 frames
    aa = np.arange(1, num_of_frames+1)

    # index_list = np.array(averages * (aa/ aa[-1])**2.3, dtype=int) # ~Quadractic increase

    slider_width = 15000 #1500 #15000
    index_list = np.array(((averages - slider_width) / num_of_frames) * aa, dtype=int)  # sliding averages

    def update_welch(frame):
        index = index_list[frame]
        print('\033[93m')
        print(f'frame = {frame} / {num_of_frames}')
        print(f'index = {index} / {averages}')
        print('\033[0m')

        end_index = index + slider_width

        temp_Sxxs = plot_Sxxs[:,index:end_index]
        temp_Syys = plot_Syys[:,index:end_index]
        temp_Sxys = plot_Sxys[:,index:end_index]

        mean_darm_psd, median_darm_psd, mean_csd, median_csd = get_mean_and_median_psds_and_csds(temp_Sxxs, temp_Syys, temp_Sxys)

        mean_darm_asd = np.sqrt(mean_darm_psd)
        median_darm_asd = np.sqrt(median_darm_psd / np.log(2))
        mean_asd = np.sqrt(np.abs(mean_csd))
        median_asd_bias_corrected = np.sqrt( np.abs(median_csd) / biases )

        s1.set_title('LHO correlated noise - August 2020 - ' + r'Sliding averages = ' + f'{index} to {end_index}')

        ln0.set_data(ff, mean_asd) 
        ln1.set_data(ff, median_asd_bias_corrected)
        ln2.set_data(ff, mean_darm_asd)
        ln3.set_data(ff, median_darm_asd)

        return ln_list
    
    ani = FuncAnimation(fig, update_welch, frames=num_of_frames, interval=100, repeat_delay=3000,
                        init_func=init_welch, blit=True) 

    ani_name = f'lho_correlated_noise_sliding_animation_sliding_averages_{slider_width}_averages_{averages}_{plot_label}.mp4'
    full_ani_name = f'{fig_dir}/{ani_name}'
    print('Writing animation to {}'.format(full_ani_name))
    ani.save(full_ani_name)
    print('\033[92m')
    print('Done! Wrote animation to {}'.format(full_ani_name))
    print('\033[0m')