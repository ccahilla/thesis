'''
lho_xcor_interactive.py

Made some sliders to see how DARM loop changes affect the gated mean averaged
and median averaged correlated noise plot.

May 19, 2020
'''
import os
import time
import copy
import numpy as np

import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons

# plt.ion()
from scipy.io import loadmat
import scipy.constants as scc

import argparse

import nds2utils as nu

mpl.rcParams.update({'figure.figsize': (12, 9),
                     'text.usetex': False,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'lines.markersize': 3,
                     'font.size': 12, #22,
                    #  'xtick.labelsize': 'large',
                    #  'ytick.labelsize': 'large',
                    #  'legend.fancybox': True,
                     'legend.fontsize': 12, #18,
                     'legend.framealpha': 0.9,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'legend.columnspacing': 2,
                    #  'savefig.dpi': 80,
                    #  'pdf.compression': 9
                     })

#####   Git Repo Directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

#####   Functions   #####
def parse_args():
    ''' Parse command-line arguments using argparse library 
    '''
    parser = argparse.ArgumentParser(description='DARM plant model comparison')
    parser.add_argument('--uncalibrated', '-u', action='store_true', 
                        help='Flag. If set, plots the uncalibrated correlated noise in mA^2/Hz.')
    parser.add_argument('--logbin', '-l', action='store_false',
                        help='Flag. If set, disables logbinning for the interactive plot.')
    # parser.add_argument('--residuals', '-r', action='store_true', 
    #                     help='Flag. If set, adds residuals to the  interactive plot.')

    args = parser.parse_args()
    return args

def read_psd_data(filename):
    '''Takes in filename and reads it. 
    Assumes two columns, frequency vector and PSD
    Returns two-element tuple = (ff, PSD)
    '''
    data = np.loadtxt(filename)
    ff = data[1:,0] # take out the first point which is freq = 0 Hz
    psd = data[1:,1]
    return ff, psd

def read_csd_data(filename):
    '''Takes in filename and reads it. 
    Assumes three columns, frequency vector, CSD mag, and CSD phase in radians.
    Could also be used to read TFs.
    Returns two-element tuple = (ff, complex CSD)
    '''
    data = np.loadtxt(filename)
    ff = data[1:,0] # take out the first point which is freq = 0 Hz
    csd = data[1:,1] * np.exp(1j * data[1:,2])
    return ff, csd

def read_noisebudget(fflog):
    '''Reads and returns dictionary of noisebudget terms.
    Input:
    fflog   = logbinned frequency vector to apply to noisebudget ASDs

    Output:
    plot_dict = dictionary of noisebudget terms we want to plot
    '''
    # Get the official LHO Noisebudget
    data_dir = f'{chapter_dir}/data'
    print(f'data_dir = {data_dir}')
    NB_LHO_contents = loadmat(f'{data_dir}/LHO_NB_data_o3a.mat', squeeze_me=True, struct_as_record=False)
    # NB_LHO_contents = loadmat('NB_dataOctIss.mat',squeeze_me=True, struct_as_record=False)
    nb = NB_LHO_contents['NB']

    ff_nb = nb.freq
    fflog = np.logspace(np.log10(ff_nb[0]), np.log10(ff_nb[-1]), 1000)
    logbin_matrix = nu.resampling_matrix_nonuniform(ff_nb, fflog)

    labels = np.array([
        'Measured noise (O3)',
        'Sum of known noises',
        'Quantum',
        'Thermal',
        'Seismic',
        'Newtonian',
        'Residual gas',
        'Aux length control',
        'Alignment control',
        'Beam jitter',
        'Scattered light',
        'Laser intensity',
        'Laser frequency',
        'Photodetector dark',
        'OMC length',
        'PUM DAC',
    ])

    ASDs = np.array([
        nb.DARM_reference,
        nb.total,
        nb.grouped_noises.quantum.total,
        nb.grouped_noises.thermal.total,
        nb.noises.seismic,
        nb.noises.newtonian,
        nb.noises.residual_gas,
        nb.grouped_noises.lsc.total,
        nb.grouped_noises.asc.total,
        nb.grouped_noises.jitter.total,
        nb.noises.scatter,
        nb.noises.intensity,
        nb.noises.frequency/np.sqrt(2),
        nb.noises.dark,
        nb.noises.OMCLength,
        nb.noises.DAC,
    ])

    styles = np.array([
        'C0-',
        'k-',
        'C4-',
        'C3-',
        'C2-',
        'C9-',
        'C8-',
        'C1o',
        'C3o',
        'C0o',
        'C4o',
        'C2o',
        'C6o',
        'C7o',
        'ko',
        'C5o',
    ])

    plot_dict = {}
    for label, ASD, style in zip(labels, ASDs, styles):
        plot_dict[label] = {}
        ASD_log = np.dot(logbin_matrix, ASD)
        plot_dict[label]['ASD'] = ASD
        plot_dict[label]['ASD_log'] = ASD_log
        plot_dict[label]['style'] = style

    # correlated noise labels to sum for the comparison plot
    sum_labels = np.array([
        # 'Measured noise (O3)',
        # 'Sum of known noises',
        # 'Quantum',
        'Thermal',
        'Seismic',
        'Newtonian',
        'Residual gas',
        'Aux length control',
        'Alignment control',
        'Beam jitter',
        'Scattered light',
        'Laser intensity',
        'Laser frequency',
        # 'Photodetector dark',
        'OMC length',
        'PUM DAC',    
    ])

    sum_xcor_noises_PSD = np.zeros_like(plot_dict[label]['ASD'])
    sum_xcor_noises_minus_thermal_PSD = np.zeros_like(plot_dict[label]['ASD'])
    for label in sum_labels:
        sum_xcor_noises_PSD += plot_dict[label]['ASD']**2

        if label == 'Thermal':
            continue
        sum_xcor_noises_minus_thermal_PSD += plot_dict[label]['ASD']**2

    log_sum_xcor_noises_PSD = np.dot(logbin_matrix, sum_xcor_noises_PSD)
    log_sum_xcor_noises_minus_thermal_PSD = np.dot(logbin_matrix, sum_xcor_noises_minus_thermal_PSD)

    # Put in plot_dict
    sum_label = 'Sum of correlated noises'
    plot_dict[sum_label] = {}
    plot_dict[sum_label]['ASD'] = np.sqrt(sum_xcor_noises_PSD)
    plot_dict[sum_label]['ASD_log'] = np.sqrt(log_sum_xcor_noises_PSD)
    plot_dict[sum_label]['style'] = 'k-'    

    sum_minus_label = 'Sum of correlated noises minus thermal'
    plot_dict[sum_minus_label] = {}
    plot_dict[sum_minus_label]['ASD'] = np.sqrt(sum_xcor_noises_minus_thermal_PSD)
    plot_dict[sum_minus_label]['ASD_log'] = np.sqrt(log_sum_xcor_noises_minus_thermal_PSD)
    plot_dict[sum_minus_label]['style'] = 'k-'    

    return plot_dict

def darm_psd_from_dcpd_sum(Scc, darm_olg):
    '''Removes the DARM open loop gain from a single DCPD PSD.
    Solves for (H x) squared in Equation 4 of 
    https://dcc.ligo.org/DocDB/0141/T1700131/001/OnlineCrossCorr.pdf
    '''
    darm_psd = Scc * np.abs(1 + darm_olg)**2
    return darm_psd

def darm_psd_from_dcpds(Saa, Sbb, Sab, darm_olg):
    '''Removes the DARM open loop gain from a single DCPD PSD.
    Solves for (H x) squared in Equation 4 of 
    https://dcc.ligo.org/DocDB/0141/T1700131/001/OnlineCrossCorr.pdf
    '''
    Sxx = Saa + Sbb + 2 * np.real(Sab)
    darm_psd = Sxx * np.abs(1 + darm_olg)**2
    return darm_psd

def plottable_asd(args, ff, psd, darm_sensing):
    '''Applies user arguments like calibration and logbinning to the data.

    Inputs:
    args                = struct with user arguments.  Output from parse_args()
    psd                 = DCPD PSD
    darm_sensing        = calibration TF from mA/m to apply to data if needed.

    Outputs:
    (plot_ff, plot_correlated_noise) = two arrays tuple,  
    plot_ff                 = frequency vector
    plot_correlated_noise   = correlated noise ASD to be plotted
    '''
    if args.uncalibrated:
        temp_psd = psd
    else:    
        temp_psd = psd / np.abs(darm_sensing)**2          # m^2/Hz

    if args.logbin:
        fflog = np.logspace(np.log10(10), np.log10(7000), 1000)
        plot_ff, temp_psd = nu.linear_log_ASD(fflog, ff, temp_psd)
    else:
        plot_ff = ff

    plot_asd = np.sqrt(np.abs(temp_psd))

    return plot_ff, plot_asd

def correlated_noise(psd_a, psd_b, csd_ab, darm_olg):
    '''Calculates the correlated noise between the two DCPDs using Equation 10 of 
    https://dcc.ligo.org/public/0141/T1700131/001/OnlineCrossCorr.pdf
    All inputs should have the same frequency vector.

    Inputs:
    psd_a       = PSD from DCPD A
    psd_b       = PSD from DCPD B
    csd_ab      = CSD from DCPDs A and B, data from A is conjugated like <A*|B>
    darm_olg    = DARM open loop gain

    Output:
    corr_noise  = the complex array correlated noise.
    '''
    corr_noise =    (np.abs(darm_olg)**2 + 2 * darm_olg) * psd_a + \
                    (np.abs(darm_olg)**2 + 2 * np.conj(darm_olg)) * psd_b + \
                    np.abs(darm_olg)**2 * np.conj(csd_ab) + \
                    np.abs(2 + darm_olg)**2 * csd_ab

    return corr_noise

def plottable_correlated_noise(args, ff, correlated_noise, darm_sensing):
    '''Applies user arguments like calibration and logbinning to the data.

    Inputs:
    args                = struct with user arguments.  Output from parse_args()
    correlated_noise    = correlated noise CSD
    darm_sensing        = calibration TF from mA/m to apply to data if needed.

    Outputs:
    (plot_ff, plot_correlated_noise) = two arrays tuple,  
    plot_ff                 = frequency vector
    plot_correlated_noise   = correlated noise ASD to be plotted
    '''
    if args.uncalibrated:
        temp_correlated_noise = correlated_noise
    else:    
        temp_correlated_noise = correlated_noise / np.abs(darm_sensing)**2          # m^2/Hz

    if args.logbin:
        fflog = np.logspace(np.log10(10), np.log10(7000), 1000)
        plot_ff, temp_correlated_noise = nu.linear_log_ASD(fflog, ff, temp_correlated_noise)
    else:
        plot_ff = ff

    # plot_correlated_noise = np.sqrt(np.abs(temp_correlated_noise))
    plot_correlated_noise = temp_correlated_noise

    return plot_ff, plot_correlated_noise


def get_squeezing_psd(db_sqz):
    '''Input squeezing level in dB.
    Output s_sqz/s_nosqz = exp(-2r) = 10**(db_sqz/10)
    '''
    return 10**(db_sqz/10.0)

def make_interactive_correlated_noise_plot(args):
    '''Creates an interactive correlated noise plot.
    '''

    #####   Read data   #####
    gps_start = 1284249618 # Sep 16 00:00:00 2020, sqz time
    gps_stop  = 1284270167 # Sep 16 05:42:29 2020
    averages = 41098
    csd_date = '16 September 2020 sqz'
    darm_label = 'measured noise during xcor (sum channel)'

    gps_start_no_sqz = 1284270168 # Sep 16 05:42:30 2020, no sqz time
    gps_stop_no_sqz  = 1284304916 # Sep 16 15:21:38 2020
    averages_no_sqz = 69496
    csd_date_no_sqz = '16 September 2020 no sqz'
    darm_label_no_sqz = 'measured noise (no sqz)'

    # gps_start = 1283317218    # September 5, 2020 05:00:00.000 UTC
    # gps_stop  = 1283346018    # September 5, 2020 13:00:00.000 UTC
    # averages = 57600
    # csd_date = '5 September 2020'
    # darm_label = 'Measured noise during xcor (sum channel)'

    # load sqz
    data_dir = f'{chapter_dir}/data'
    psd_a_filename  = f'{data_dir}/raw_psd_a_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    psd_b_filename  = f'{data_dir}/raw_psd_b_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    csd_ab_filename = f'{data_dir}/raw_csd_ab_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    psd_sum_filename = f'{data_dir}/raw_psd_sum_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    psd_null_filename = f'{data_dir}/raw_psd_null_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    darm_error_filename = f'{data_dir}/raw_darm_error_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'

    ff, psd_a = read_psd_data(psd_a_filename)
    _, psd_b = read_psd_data(psd_b_filename)
    _, csd_ab = read_csd_data(csd_ab_filename)
    _, psd_sum = read_psd_data(psd_sum_filename)
    _, psd_null = read_psd_data(psd_null_filename)
    _, darm_error = read_psd_data(darm_error_filename)

    # load no sqz
    psd_a_filename_no_sqz  = f'{data_dir}/raw_psd_a_gps_start_{gps_start_no_sqz}_gps_stop_{gps_stop_no_sqz}.txt'
    psd_b_filename_no_sqz  = f'{data_dir}/raw_psd_b_gps_start_{gps_start_no_sqz}_gps_stop_{gps_stop_no_sqz}.txt'
    csd_ab_filename_no_sqz = f'{data_dir}/raw_csd_ab_gps_start_{gps_start_no_sqz}_gps_stop_{gps_stop_no_sqz}.txt'
    psd_sum_filename_no_sqz = f'{data_dir}/raw_psd_sum_gps_start_{gps_start_no_sqz}_gps_stop_{gps_stop_no_sqz}.txt'
    psd_null_filename_no_sqz = f'{data_dir}/raw_psd_null_gps_start_{gps_start_no_sqz}_gps_stop_{gps_stop_no_sqz}.txt'
    darm_error_filename_no_sqz = f'{data_dir}/raw_darm_error_gps_start_{gps_start_no_sqz}_gps_stop_{gps_stop_no_sqz}.txt'

    ff, psd_a_no_sqz = read_psd_data(psd_a_filename_no_sqz)
    _, psd_b_no_sqz = read_psd_data(psd_b_filename_no_sqz)
    _, csd_ab_no_sqz = read_csd_data(csd_ab_filename_no_sqz)
    _, psd_sum_no_sqz = read_psd_data(psd_sum_filename_no_sqz)
    _, psd_null_no_sqz = read_psd_data(psd_null_filename_no_sqz)
    _, darm_error_no_sqz = read_psd_data(darm_error_filename_no_sqz)

    _, darm_olg = read_csd_data(f'{data_dir}/LHO_pyDARM_OLG.txt')
    G = darm_olg[1::2]

    ratio_psd = np.median(psd_sum / darm_error)
    ratio_psd_no_sqz = np.median(psd_sum_no_sqz / darm_error_no_sqz)
    print()
    print(f'ratio_psd        = {ratio_psd} mA^2/cts^2')
    print(f'ratio_psd_no_sqz = {ratio_psd_no_sqz} mA^2/cts^2')
    print()

    ratio_sq = np.sqrt(ratio_psd) # 1894811700000.0 # (mA/cts)^2, from ratio of PSD_SUM / PSD_ERR
    ratio_sq_no_sqz = np.sqrt(ratio_psd_no_sqz)
    DARM_multiplier = 1.0   #
    
    _, darm_sensing = read_csd_data(f'{data_dir}/LHO_pyDARM_sensing_function_mA_per_meter.txt') # actually cts/m
    darm_sensing = darm_sensing[1::2]
    C = darm_sensing * ratio_sq
    C_no_sqz = darm_sensing * ratio_sq_no_sqz

    #####   Squeezing levels   #####
    sqz_db = 2.0 # +- 0.1 dB
    r_sqz = sqz_db/20.0 * np.log(10.0)
    S_sqz = np.exp(-2.0 * r_sqz)
    


    #####   Calculate correlated noise   #####
    corr_noise_with_sqz = correlated_noise(psd_a, psd_b, csd_ab, G)
    corr_noise_sqz_removed = corr_noise_with_sqz - psd_null * (S_sqz - 1)
    # corr_noise_sqz_removed = get_unsqueezed_correlated_noise_from_squeezed_data(psd_a, psd_b, csd_ab, G)

    plot_ff, plot_corr_noise_with_sqz = plottable_correlated_noise(args, ff, corr_noise_with_sqz, C)
    plot_ff, plot_corr_noise_sqz_removed = plottable_correlated_noise(args, ff, corr_noise_sqz_removed, C)

    plot_asd_corr_noise_with_sqz = np.sqrt(np.abs(plot_corr_noise_with_sqz))
    plot_asd_corr_noise_sqz_removed = np.sqrt(np.abs(plot_corr_noise_sqz_removed))

    psd_sum0 = darm_psd_from_dcpd_sum(psd_sum, G)
    plot_ff, plot_psd_sum = plottable_asd(args, ff, psd_sum0, C)
    plot_ff, plot_psd_null = plottable_asd(args, ff, psd_null, C)

    # no sqz
    corr_noise_no_sqz = correlated_noise(psd_a_no_sqz, psd_b_no_sqz, csd_ab_no_sqz, G)
    plot_ff, plot_corr_noise_no_sqz = plottable_correlated_noise(args, ff, corr_noise_no_sqz, C_no_sqz)

    plot_asd_corr_noise_no_sqz = np.sqrt(np.abs(plot_corr_noise_no_sqz))

    psd_sum0_no_sqz = darm_psd_from_dcpd_sum(psd_sum_no_sqz, G)
    plot_ff, plot_psd_sum_no_sqz = plottable_asd(args, ff, psd_sum0_no_sqz, C_no_sqz)



    #####   Noisebudget   #####
    plot_names = np.array([])

    fig, s1 = plt.subplots(1, figsize=(12,9))

    # saved traces
    l1s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-', alpha=0.0, color='C4') 
    l2s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-', alpha=0.0, color='C1')
    l3s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-', alpha=0.0, color='C2')
    l4s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-', alpha=0.0, color='C5')
    l5s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-', alpha=0.0, color='C3')
    l6s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-', alpha=0.0, color='C8')

    # main plots to be updated
    l1, = s1.loglog(plot_ff, plot_asd_corr_noise_sqz_removed, ls='-',  color=l1s.get_color(), zorder=4, label='correlated noise squeezing removed')
    l2, = s1.loglog(plot_ff, plot_asd_corr_noise_with_sqz, ls='-', color=l2s.get_color(), zorder=3, label='correlated noise with squeezing')
    l3, = s1.loglog(plot_ff, plot_psd_sum, ls='-',  color=l3s.get_color(), zorder=3, label=darm_label)
    l4, = s1.loglog(plot_ff, plot_psd_null, ls='-',  color=l4s.get_color(), zorder=3, label='null channel')
    l5, = s1.loglog(plot_ff, plot_asd_corr_noise_no_sqz, ls='-',  color=l5s.get_color(), zorder=3, label='correlated noise no squeezing')
    l6, = s1.loglog(plot_ff, plot_psd_sum_no_sqz, ls='-',  color=l6s.get_color(), zorder=3, label=darm_label_no_sqz)

    # if possible, read and plot LHO noisebudget traces
    if args.uncalibrated:
        xlims = [30, 8000]
        ylims = [4e-9, 1.01e-6]
        units = 'mA'
    else:
        xlims = [30, 8000]
        ylims = [1e-21, 1.01e-18]
        units = 'm'
        try:
            fflog = np.logspace(np.log10(10), np.log10(7000), 1000)
            plot_dict = read_noisebudget(fflog)

            # correlated noise labels to plot in the final comparison plot
            plot_labels = np.array([
                'Measured noise (O3)',
                'Thermal',
                'Residual gas',
                'Beam jitter',
                'Laser intensity',
                'Laser frequency',
                'Photodetector dark', # This one is ignored in the final plot, lho_xcor.pdf
                'Scattered light',
                'Sum of correlated noises',
                # 'Aux length control',
            ])

            for label in plot_labels:
                # ASD = plot_dict[label]['ASD']
                ASD_log = plot_dict[label]['ASD_log']
                style = plot_dict[label]['style']
                s1.loglog(fflog, ASD_log, style, label=label)
        except:
            print()
            print('LHO noisebudget not plotted')
            print()

    s1.set_title('Correlated DARM noise with sqz level slider')
    s1.set_ylabel('ASD ' + f'[{units}/rtHz]')
    s1.set_xlabel('Frequency [Hz]')

    s1.set_xlim(xlims)
    s1.set_ylim(ylims)
    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.3)
    s1.legend()

    plt.tight_layout()
    plt.subplots_adjust(bottom=0.15)

    axcolor = 'lightgoldenrodyellow'
    # spacing = 0.025

    ax_sqz_level       = plt.axes([0.25, 0.065, 0.65, 0.01], facecolor=axcolor)
    sax_sqz_level      = Slider(ax_sqz_level,   r'Squeezing level [dB]', -3.0, 6.0, valinit=-2.0)

    def update(val):
        cur_sqz_db = sax_sqz_level.val
        cur_s_sqz = get_squeezing_psd(cur_sqz_db)

        corr_noise_sqz_removed = corr_noise_with_sqz - psd_null * (cur_s_sqz - 1)
        plot_ff, plot_corr_noise_sqz_removed = plottable_correlated_noise(args, ff, corr_noise_sqz_removed, C)

        plot_asd_corr_noise_sqz_removed = np.sqrt(np.abs(plot_corr_noise_sqz_removed))

        l1.set_ydata( plot_asd_corr_noise_sqz_removed )

        fig.canvas.draw_idle()

    sax_sqz_level.on_changed(update)

    resetax = plt.axes([0.8, 0.01, 0.1, 0.02])
    button_reset = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
    saveax = plt.axes([0.6, 0.01, 0.1, 0.02])
    button_save = Button(saveax, 'Save Trace', color=axcolor, hovercolor='0.975')

    def reset(event):
        sax_sqz_level.reset()

    button_reset.on_clicked(reset)

    def save(event):
        cur_data1 = l1.get_ydata()
        l1s.set_ydata( cur_data1 )
        l1s.set_alpha(0.5)
        fig.canvas.draw_idle()

    button_save.on_clicked(save)

    plt.show()
    return

def main(args):
    '''main function of this script.  Makes an interactive plot.
    '''
    make_interactive_correlated_noise_plot(args)
    return 
    
if __name__ == "__main__":

    args = parse_args()
    main(args)
    
