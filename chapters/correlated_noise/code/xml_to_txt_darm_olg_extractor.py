'''
xml_to_txt_darm_olg_extractor.py

Take a DARM OLG .xml file and produce a .txt file from it.

Nov 10, 2020
'''
import os
import time

import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt

import dtt2hdf

mpl.rcParams.update({'figure.figsize': (12, 9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'lines.markersize': 5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.9,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'legend.columnspacing': 2,
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Git Repo Directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

data_dir = f'{chapter_dir}/data'

#####   Functions   #####
def read_tf_and_coh_from_xml(chanA, chanB, xml_dict):
    '''Takes in chanA and chanB names and a dtt2hdf.read_diaggui(xmlName.xml) dictionary output, 
    and returns the ff, TF, coherence, and Bendat and Piersol 1 sigma uncertainty'''
    index = xml_dict['results']['COH'][chanA]['channelB_inv'][chanB]
    
    ff = xml_dict['results']['TF'][chanA]['FHz']
    Navgs = xml_dict['results']['TF'][chanA]['averages']

    tf = np.conj(xml_dict['results']['TF'][chanA]['xfer'][index])
    coh = np.sqrt( xml_dict['results']['COH'][chanA]['coherence'][index] ) # DTT gives power coherence
    unc = np.sqrt( (1.0 - coh**2) / (2 * Navgs * coh) ) 
    return ff, tf, coh, unc

def main():
    '''main function of this script.  
    Makes a .txt from an .xml.
    '''

    xml_file = f'{data_dir}/2020-08-28_H1_DARM_OLGTF_LF_SS_5to1100Hz_15min.xml'
    dd = dtt2hdf.read_diaggui(xml_file) 

    chan_b = 'H1:LSC-DARM1_IN1'
    chan_a = 'H1:LSC-DARM1_IN2'
    ff, tf, coh, unc = read_tf_and_coh_from_xml(chan_a, chan_b, dd)
    tf *= -1 # fix the sign for darm_olg_plotter

    # Save the .txt file
    save_filename = '2020-08-28_H1_DARM_OLGTF_LF_SS_A_DARMIN2_B_DARMIN1_tf.txt'
    save_full_filename = f'{data_dir}/{save_filename}'
    save_data = np.vstack((ff, np.abs(tf), np.angle(tf), coh)).T
    print('\033[93m')
    print(f'Saving file: {save_full_filename}')
    print('\033[0m')
    np.savetxt(save_full_filename, save_data, header='Frequency [Hz], Open Loop Gain Mag [none], Phase [rads], Coherence [none]')

if __name__ == "__main__":

    # args = parse_args()
    main()