'''
lho_xcor_noisebudget.py

Fast, easy version of the LHO correlated noisebudget.

May 19, 2020
'''
import os
import time
import copy
import numpy as np

import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt

from scipy.io import loadmat

import argparse

import nds2utils as nu

mpl.rcParams.update({'figure.figsize': (12, 9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'lines.markersize': 3,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.9,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'legend.columnspacing': 2,
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Git Repo Directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

#####   Functions   #####
def parse_args():
    ''' Parse command-line arguments using argparse library 
    '''
    parser = argparse.ArgumentParser(description='DARM plant model comparison')
    parser.add_argument('--uncalibrated', '-u', action='store_true', 
                        help='Flag. If set, plots the uncalibrated correlated noise in mA^2/Hz.')
    parser.add_argument('--logbin', '-l', action='store_false',
                        help='Flag. If set, disables logbinning for the interactive plot.')
    # parser.add_argument('--residuals', '-r', action='store_true', 
    #                     help='Flag. If set, adds residuals to the  interactive plot.')

    args = parser.parse_args()
    return args

def read_psd_txt(filename):
    '''Takes in filename and reads it. 
    Assumes two columns, frequency vector and PSD
    Returns two-element tuple = (ff, PSD)
    '''
    data = np.loadtxt(filename)
    ff = data[1:,0] # take out the first point which is freq = 0 Hz
    psd = data[1:,1]
    return ff, psd

def read_csd_txt(filename):
    '''Takes in filename and reads it. 
    Assumes three columns, frequency vector, CSD mag, and CSD phase in radians.
    Could also be used to read TFs.
    Returns two-element tuple = (ff, complex CSD)
    '''
    data = np.loadtxt(filename)
    ff = data[1:,0] # take out the first point which is freq = 0 Hz
    csd = data[1:,1] * np.exp(1j * data[1:,2])
    return ff, csd

def read_noisebudget(fflog):
    '''Reads and returns dictionary of noisebudget terms.
    Input:
    fflog   = logbinned frequency vector to apply to noisebudget ASDs

    Output:
    plot_dict = dictionary of noisebudget terms we want to plot
    '''
    # Get the official LHO Noisebudget
    data_dir = f'{chapter_dir}/data'
    NB_LHO_contents = loadmat(f'{data_dir}/LHO_NB_data_o3a.mat', squeeze_me=True, struct_as_record=False)
    # NB_LHO_contents = loadmat('NB_dataOctIss.mat',squeeze_me=True, struct_as_record=False)
    nb = NB_LHO_contents['NB']

    ff_nb = nb.freq
    fflog = np.logspace(np.log10(ff_nb[0]), np.log10(ff_nb[-1]), 1000)
    logbin_matrix = nu.resampling_matrix_nonuniform(ff_nb, fflog)

    labels = np.array([
        'Measured noise (O3)',
        'Sum of known noises',
        'Quantum',
        'Thermal',
        'Seismic',
        'Newtonian',
        'Residual gas',
        'Aux length control',
        'Alignment control',
        'Beam jitter',
        'Scattered light',
        'Laser intensity',
        'Laser frequency',
        'Photodetector dark',
        'OMC length',
        'PUM DAC',
    ])

    ASDs = np.array([
        nb.DARM_reference,
        nb.total,
        nb.grouped_noises.quantum.total,
        nb.grouped_noises.thermal.total,
        nb.noises.seismic,
        nb.noises.newtonian,
        nb.noises.residual_gas,
        nb.grouped_noises.lsc.total,
        nb.grouped_noises.asc.total,
        nb.grouped_noises.jitter.total,
        nb.noises.scatter,
        nb.noises.intensity,
        nb.noises.frequency/np.sqrt(2),
        nb.noises.dark,
        nb.noises.OMCLength,
        nb.noises.DAC,
    ])

    styles = np.array([
        'C0-',
        'k-',
        'C4-',
        'C3-',
        'C2-',
        'C9-',
        'C8-',
        'C1o',
        'C3o',
        'C0o',
        'C4o',
        'C2o',
        'C6o',
        'C7o',
        'ko',
        'C5o',
    ])

    plot_dict = {}
    for label, ASD, style in zip(labels, ASDs, styles):
        plot_dict[label] = {}
        ASD_log = np.dot(logbin_matrix, ASD)
        plot_dict[label]['ASD'] = ASD
        plot_dict[label]['ASD_log'] = ASD_log
        plot_dict[label]['style'] = style

    # correlated noise labels to sum for the comparison plot
    sum_labels = np.array([
        # 'Measured noise (O3)',
        # 'Sum of known noises',
        # 'Quantum',
        'Thermal',
        'Seismic',
        'Newtonian',
        'Residual gas',
        'Aux length control',
        'Alignment control',
        'Beam jitter',
        'Scattered light',
        'Laser intensity',
        'Laser frequency',
        # 'Photodetector dark',
        'OMC length',
        'PUM DAC',    
    ])

    sum_xcor_noises_PSD = np.zeros_like(plot_dict[label]['ASD'])
    sum_xcor_noises_minus_thermal_PSD = np.zeros_like(plot_dict[label]['ASD'])
    for label in sum_labels:
        sum_xcor_noises_PSD += plot_dict[label]['ASD']**2

        if label == 'Thermal':
            continue
        sum_xcor_noises_minus_thermal_PSD += plot_dict[label]['ASD']**2

    log_sum_xcor_noises_PSD = np.dot(logbin_matrix, sum_xcor_noises_PSD)
    log_sum_xcor_noises_minus_thermal_PSD = np.dot(logbin_matrix, sum_xcor_noises_minus_thermal_PSD)

    # Put in plot_dict
    sum_label = 'Sum of correlated noises'
    plot_dict[sum_label] = {}
    plot_dict[sum_label]['ASD'] = np.sqrt(sum_xcor_noises_PSD)
    plot_dict[sum_label]['ASD_log'] = np.sqrt(log_sum_xcor_noises_PSD)
    plot_dict[sum_label]['style'] = 'k-'    

    sum_minus_label = 'Sum of correlated noises minus thermal'
    plot_dict[sum_minus_label] = {}
    plot_dict[sum_minus_label]['ASD'] = np.sqrt(sum_xcor_noises_minus_thermal_PSD)
    plot_dict[sum_minus_label]['ASD_log'] = np.sqrt(log_sum_xcor_noises_minus_thermal_PSD)
    plot_dict[sum_minus_label]['style'] = 'k-'    

    return plot_dict

def darm_psd_from_dcpd_sum(Scc, darm_olg):
    '''Removes the DARM open loop gain from a single DCPD PSD.
    Solves for (H x) squared in Equation 4 of 
    https://dcc.ligo.org/DocDB/0141/T1700131/001/OnlineCrossCorr.pdf
    '''
    darm_psd = Scc * np.abs(1 + darm_olg)**2
    return darm_psd

def darm_psd_from_dcpds(Saa, Sbb, Sab, darm_olg):
    '''Removes the DARM open loop gain from a single DCPD PSD.
    Solves for (H x) squared in Equation 4 of 
    https://dcc.ligo.org/DocDB/0141/T1700131/001/OnlineCrossCorr.pdf
    '''
    Sxx = Saa + Sbb + 2 * np.real(Sab)
    darm_psd = Sxx * np.abs(1 + darm_olg)**2
    return darm_psd

def plottable_asd(args, ff, psd, darm_sensing):
    '''Applies user arguments like calibration and logbinning to the data.

    Inputs:
    args                = struct with user arguments.  Output from parse_args()
    psd                 = DCPD PSD
    darm_sensing        = calibration TF from mA/m to apply to data if needed.

    Outputs:
    (plot_ff, plot_correlated_noise) = two arrays tuple,  
    plot_ff                 = frequency vector
    plot_correlated_noise   = correlated noise ASD to be plotted
    '''
    if args.uncalibrated:
        temp_psd = psd
    else:    
        temp_psd = psd / np.abs(darm_sensing)**2          # m^2/Hz

    if args.logbin:
        fflog = np.logspace(np.log10(10), np.log10(7000), 1000)
        plot_ff, temp_psd = nu.linear_log_ASD(fflog, ff, temp_psd)
    else:
        plot_ff = ff

    plot_asd = np.sqrt(np.abs(temp_psd))

    return plot_ff, plot_asd

def correlated_noise(psd_a, psd_b, csd_ab, darm_olg):
    '''Calculates the correlated noise between the two DCPDs using Equation 10 of 
    https://dcc.ligo.org/public/0141/T1700131/001/OnlineCrossCorr.pdf
    All inputs should have the same frequency vector.

    Inputs:
    psd_a       = PSD from DCPD A
    psd_b       = PSD from DCPD B
    csd_ab      = CSD from DCPDs A and B, data from A is conjugated like <A*|B>
    darm_olg    = DARM open loop gain

    Output:
    corr_noise  = the complex array correlated noise.
    '''
    corr_noise =    (np.abs(darm_olg)**2 + 2 * darm_olg) * psd_a + \
                    (np.abs(darm_olg)**2 + 2 * np.conj(darm_olg)) * psd_b + \
                    np.abs(darm_olg)**2 * np.conj(csd_ab) + \
                    np.abs(2 + darm_olg)**2 * csd_ab

    return corr_noise

def plottable_correlated_noise(args, ff, correlated_noise, darm_sensing):
    '''Applies user arguments like calibration and logbinning to the data.

    Inputs:
    args                = struct with user arguments.  Output from parse_args()
    correlated_noise    = correlated noise CSD
    darm_sensing        = calibration TF from mA/m to apply to data if needed.

    Outputs:
    (plot_ff, plot_correlated_noise) = two arrays tuple,  
    plot_ff                 = frequency vector
    plot_correlated_noise   = correlated noise ASD to be plotted
    '''
    if args.uncalibrated:
        temp_correlated_noise = correlated_noise
    else:    
        temp_correlated_noise = correlated_noise / np.abs(darm_sensing)**2          # m^2/Hz

    if args.logbin:
        fflog = np.logspace(np.log10(10), np.log10(7000), 1000)
        plot_ff, temp_correlated_noise = nu.linear_log_ASD(fflog, ff, temp_correlated_noise)
    else:
        plot_ff = ff

    # plot_correlated_noise = np.sqrt(np.abs(temp_correlated_noise))
    plot_correlated_noise = temp_correlated_noise

    return plot_ff, plot_correlated_noise

def make_interactive_correlated_noise_plot(args):
    '''Creates an interactive correlated noise plot.
    '''

    #####   Load PSDs and CSDs   #####
    gps_start = 1284249618 # Sep 16 00:00:00 2020, sqz time
    gps_stop  = 1284270167 # Sep 16 05:42:29 2020
    averages = 41098
    csd_date = '16 September 2020 sqz'
    darm_label = 'Measured noise during xcor'

    # gps_start = 1284270168 # Sep 16 05:42:30 2020, no sqz time
    # gps_stop  = 1284304916 # Sep 16 15:21:38 2020
    # averages = 69496
    # csd_date = '16 September 2020 no sqz'
    # darm_label = 'Measured noise during xcor (no sqz)'

    # gps_start = 1283317218    # September 5, 2020 05:00:00.000 UTC
    # gps_stop  = 1283346018    # September 5, 2020 13:00:00.000 UTC
    # averages = 57600
    # csd_date = '5 September 2020'
    # darm_label = 'Measured noise during xcor'

    # gps_start = 1282730359          # August 29 2020 09:59:01
    # gps_stop  = 1282763937          # August 29 2020 19:18:39
    # averages = 67156
    # csd_date = 'August 2020'
    # darm_label = 'Measured noise during xcor (no sqz)'

    # gps_start = 1256380478          # October 2019
    # gps_stop = 1256390478
    # averages = 20000
    # csd_date = 'October 2019'
    # darm_label = 'Measured noise during xcor (no sqz)'

    ###   Read Data   ###
    fig_dir = f'{chapter_dir}/figures/{script_name}'
    data_dir = f'{chapter_dir}/data'

    ff, raw_psd_a = read_psd_txt(f'{data_dir}/raw_psd_a_gps_start_{gps_start}_gps_stop_{gps_stop}.txt')
    _, raw_psd_b = read_psd_txt(f'{data_dir}/raw_psd_b_gps_start_{gps_start}_gps_stop_{gps_stop}.txt')
    _, raw_csd_ab = read_csd_txt(f'{data_dir}/raw_csd_ab_gps_start_{gps_start}_gps_stop_{gps_stop}.txt')
    _, raw_psd_sum = read_psd_txt(f'{data_dir}/raw_psd_sum_gps_start_{gps_start}_gps_stop_{gps_stop}.txt')
    _, raw_psd_null = read_psd_txt(f'{data_dir}/raw_psd_null_gps_start_{gps_start}_gps_stop_{gps_stop}.txt')
    _, raw_darm_error = read_psd_txt(f'{data_dir}/raw_darm_error_gps_start_{gps_start}_gps_stop_{gps_stop}.txt')

    # _, darm_olg = read_csd_txt(f'{data_dir}/LHO_pyDARM_OLG_for_HF_interp_from_2020_08_28.txt')
    _, darm_olg = read_csd_txt(f'{data_dir}/LHO_pyDARM_OLG.txt')
    darm_olg = darm_olg[1::2]

    if csd_date == '5 September 2020':
        ratio_psd = 3906644989609.
    elif csd_date == 'August 2020':
        ratio_psd = 1927663208073.
    elif csd_date == 'October 2019':
        ratio_psd = 1895693131538.  # mA^2/cts^2
    else:
        ratio_psd = np.median(raw_psd_sum / raw_darm_error)
        print()
        print(f'ratio_psd = {ratio_psd} mA^2/cts^2')
        print()

    ratio_sq = np.sqrt(ratio_psd) # 1894811700000.0 # (mA/cts)^2, from ratio of PSD_SUM / PSD_ERR
    DARM_multiplier = 1.0   #
    _, darm_sensing = read_csd_txt(f'{data_dir}/LHO_pyDARM_sensing_function_mA_per_meter.txt') # actually cts/m
    darm_sensing = darm_sensing[1::2]
    darm_sensing *= ratio_sq

    ###   Initial Models   ###
    raw_psd = darm_psd_from_dcpds(raw_psd_a, raw_psd_b, raw_csd_ab, darm_olg)
    raw_corr_noise = correlated_noise(raw_psd_a, raw_psd_b, raw_csd_ab, darm_olg)
    raw_psd_sum0 = darm_psd_from_dcpd_sum(raw_psd_sum, darm_olg)

    plot_ff, plot_asd = plottable_asd(args, ff, raw_psd, darm_sensing)
    plot_ff, plot_corr_noise = plottable_correlated_noise(args, ff, raw_corr_noise, darm_sensing)
    plot_ff, plot_asd_sum = plottable_asd(args, ff, raw_psd_sum0, darm_sensing)
    plot_ff, plot_asd_null = plottable_asd(args, ff, raw_psd_null, darm_sensing)

    plot_asd_corr_noise = np.sqrt(np.abs(plot_corr_noise))

    #####   Noisebudget   #####
    plot_names = np.array([])

    fig, s1 = plt.subplots(1, figsize=(12,9))

    # saved traces
    l2s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-', alpha=0.0, color='C1')
    l3s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-', alpha=0.0, color='C2')
    l1s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-',  alpha=0.0, color='C5') 

    # main plots to be updated
    l2, = s1.loglog(plot_ff, plot_asd_corr_noise, ls='-', color=l2s.get_color(), zorder=3, label='Correlated noise')
    l3, = s1.loglog(plot_ff, plot_asd_sum, ls='-',  color=l3s.get_color(), zorder=3, label=darm_label)
    l1, = s1.loglog(plot_ff, plot_asd_null, ls='-',  color=l1s.get_color(), zorder=3, label='Null channel')

    # if possible, read and plot LHO noisebudget traces
    if args.uncalibrated:
        xlims = [30, 7000]
        ylims = [4e-9, 1.01e-6]
        units = 'mA'
    else:
        xlims = [30, 7000]
        ylims = [1e-21, 1.01e-18]
        units = 'm'
        try:
            fflog = np.logspace(np.log10(10), np.log10(7000), 1000)
            plot_dict = read_noisebudget(fflog)

            # correlated noise labels to plot in the final comparison plot
            plot_labels = np.array([
                'Measured noise (O3)',
                'Thermal',
                'Residual gas',
                'Beam jitter',
                'Laser intensity',
                'Laser frequency',
                'Photodetector dark', # This one is ignored in the final plot, lho_xcor.pdf
                'Scattered light',
                'Sum of correlated noises',
                # 'Aux length control',
            ])

            for label in plot_labels:
                # ASD = plot_dict[label]['ASD']
                ASD_log = plot_dict[label]['ASD_log']
                style = plot_dict[label]['style']
                s1.loglog(fflog, ASD_log, style, label=label)
        except:
            print()
            print('LHO noisebudget not plotted')
            print()

    # s1.set_title('Correlated noise with DARM loop sliders')
    s1.set_ylabel('DARM ' + r'[$\mathrm{' + f'{units}' + r'}/\sqrt{\mathrm{Hz}}$]')
    s1.set_xlabel('Frequency [Hz]')

    s1.set_xlim(xlims)
    s1.set_ylim(ylims)
    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.3)
    s1.legend(ncol=2)

    
    plot_name = f'lho_correlated_noisebudget_{csd_date.replace(" ", "_")}.pdf'
    full_plot_name = f'{fig_dir}/{plot_name}'
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.tight_layout()
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # Plot phase
    fig, (s1) = plt.subplots(1)

    # manipulate the phase calculation so it wraps not at 180 degrees
    temp = plot_corr_noise * np.exp(-1j * np.pi/2)
    temp_angle = np.angle(temp)
    plot_angle = temp_angle + np.pi/2

    s1.semilogx(plot_ff, 180/np.pi * plot_angle, label='Phase')
    # s2.semilogx(ff, biases2, color='C7', alpha=0.6, label='Mean to median biases (Eq. A.87)')

    s1.set_ylabel(f'CSD Phase [degs]')
    s1.set_xlabel(f'Frequency [Hz]')

    s1.set_xlim([4e1, 5e3])
    s1.set_ylim([-90, 270])
    s1.set_yticks([-90, 0, 90, 180, 270])

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.7)
    # s1.legend()

    plot_name = f'csd_phase_averages_{csd_date.replace(" ", "_")}.pdf'
    full_plot_name = f'{fig_dir}/{plot_name}'
    plot_names = np.append(plot_names, full_plot_name)
    print(f'Writing plot PDF to {full_plot_name}')
    plt.tight_layout()
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()

    # print open command
    command = 'open'
    for pf in plot_names:
        command = '{} {}'.format(command, pf)
    print()
    print('Command to open plots generated by this script:')
    print(command)
    print()

    return

def main(args):
    '''main function of this script.  Makes an interactive plot.
    '''
    make_interactive_correlated_noise_plot(args)
    return 
    
if __name__ == "__main__":

    args = parse_args()
    main(args)
    
