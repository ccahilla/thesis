'''
get_raw_mean_spectral_density_txts.py

Made some sliders to see how DARM loop changes affect the gated mean averaged
and median averaged correlated noise plot.

May 19, 2020
'''
import os
import time
import pickle
import numpy as np


#####   Git Repo Directory   #####
script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

data_dir = f'{chapter_dir}/data'

# Place to save large data
if 'ccahilla' in chapter_dir:
    external_hd_dir = '/Volumes/SandiskSD/ccahilla/Git/thesis/chapters/correlated_noise/data' # Craig-specific code

#####   Functions   #####
def get_mean_psd(Saas):
    '''Calculate the mean PSD from a bunch of PSDs.
    '''
    Saa_mean = np.mean(np.real(Saas), axis=1)
    return Saa_mean

def get_mean_csd(Sabs):
    '''Calculate the mean CSD from a bunch of CSDs.
    '''
    Sab_mean = np.mean(np.real(Sabs), axis=1) + 1j * np.mean(np.imag(Sabs), axis=1)
    return Sab_mean


#####   Load PSDs and CSDs   #####

# gps_start = 1283317218    # September 5, 2020 05:00:00.000 UTC
# gps_stop  = 1283346018    # September 5, 2020 13:00:00.000 UTC
# averages = 57600
# csd_date = 'September 2020'
# filename = f'{external_hd_dir}/lho_uncalibrated_dcpd_psds_and_csds_averages_57600_gps_start_1283317218_gps_stop_1283346018.pkl'

gps_start = 1282730359          # August 29 2020 09:59:01
gps_stop  = 1282763937          # August 29 2020 19:18:39
averages = 67156
csd_date = 'August 2020'
filename = f'{external_hd_dir}/lho_uncalibrated_dcpd_psds_and_csds_averages_67156_gps_start_1282730359_gps_stop_1282763937.pkl'

# gps_start = 1256380478          # October 2019
# gps_stop = 1256390478
# averages = 20000
# csd_date = 'October 2019'
# filename = f'{external_hd_dir}/lho_uncalibrated_dcpd_psds_and_csds_averages_20000_gps_start_1256380478_gps_stop_1256390478.pkl'

fs = 16384    # Hz
binwidth = 2  # Hz
fnyquist = fs // 2  # Hz
ff = np.linspace(0, fnyquist, fnyquist // binwidth + 1)



start_time = time.time()

with open(filename, 'rb') as infile:
    data_dict = pickle.load(infile)
Saas = data_dict['dcpd_a_psds']
Sbbs = data_dict['dcpd_b_psds'] 
Sabs = data_dict['darm_csds']
Sccs = data_dict['dcpd_sum']
Sdds = data_dict['dcpd_null']
Sees = data_dict['darm_error']

print(f'Loaded {filename} in {time.time() - start_time} seconds')


#####   Do PSD rejection   #####

# Reject any PSD value above 14 times the median PSD value.
# The CDF of the exponential F(14 * rho) = 1 - exp(-14 * log(2) ) = 0.999938
ff_index = 20 
outlier_psd_indicies = np.argwhere(np.median(Saas[ff_index,:]) * 14 < Saas[ff_index,:])[:,0]
good_psd_indicies    = np.argwhere(np.median(Saas[ff_index,:]) * 14 >= Saas[ff_index,:])[:,0]

bad_averages = len(outlier_psd_indicies)
good_averages = len(good_psd_indicies)
print()
print(f'There were {good_averages} good averages out of {good_averages + bad_averages} total')
print()

#####   Postprocess   #####

good_Saa = get_mean_psd(Saas[:, good_psd_indicies])
good_Sbb = get_mean_psd(Sbbs[:, good_psd_indicies])
good_Sab = get_mean_csd(Sabs[:, good_psd_indicies])
good_Scc = get_mean_psd(Sccs[:, good_psd_indicies])
good_Sdd = get_mean_psd(Sdds[:, good_psd_indicies])
good_See = get_mean_psd(Sees[:, good_psd_indicies])

#####   Save the spectral densities   #####

save_filename = f'raw_psd_a_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
save_full_filename = f'{data_dir}/{save_filename}'
save_data = np.vstack((ff, good_Saa)).T
print('\033[93m')
print(f'Saving file: {save_full_filename}')
print('\033[0m')
np.savetxt(save_full_filename, save_data, header='Frequency [Hz], raw DCPD A PSD [cts/rtHz]')

save_filename = f'raw_psd_b_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
save_full_filename = f'{data_dir}/{save_filename}'
save_data = np.vstack((ff, good_Sbb)).T
print('\033[93m')
print(f'Saving file: {save_full_filename}')
print('\033[0m')
np.savetxt(save_full_filename, save_data, header='Frequency [Hz], raw DCPD A PSD [cts/rtHz]')

save_filename = f'raw_csd_ab_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
save_full_filename = f'{data_dir}/{save_filename}'
save_data = np.vstack(( ff, np.abs(good_Sab), np.angle(good_Sab) )).T
print('\033[93m')
print(f'Saving file: {save_full_filename}')
print('\033[0m')
np.savetxt(save_full_filename, save_data, header='Frequency [Hz], raw DCPD A vs DCPD B CSD [cts/rtHz]')

save_filename = f'raw_psd_sum_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
save_full_filename = f'{data_dir}/{save_filename}'
save_data = np.vstack((ff, good_Scc)).T
print('\033[93m')
print(f'Saving file: {save_full_filename}')
print('\033[0m')
np.savetxt(save_full_filename, save_data, header='Frequency [Hz], raw DCPD SUM PSD [cts/rtHz]')


save_filename = f'raw_psd_null_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
save_full_filename = f'{data_dir}/{save_filename}'
save_data = np.vstack((ff, good_Sdd)).T
print('\033[93m')
print(f'Saving file: {save_full_filename}')
print('\033[0m')
np.savetxt(save_full_filename, save_data, header='Frequency [Hz], raw DCPD SUM PSD [cts/rtHz]')

save_filename = f'raw_darm_error_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
save_full_filename = f'{data_dir}/{save_filename}'
save_data = np.vstack((ff, good_See)).T
print('\033[93m')
print(f'Saving file: {save_full_filename}')
print('\033[0m')
np.savetxt(save_full_filename, save_data, header='Frequency [Hz], raw DARM error [cts/rtHz]')