'''
lho_xcor_stats.py

Runs the statistics on the LHO cross-correlation of the DCPDs 
to get the probability density functions.

Craig Cahillane
June 8, 2020
'''

import os
import time
import copy
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
# plt.ion()

import scipy.constants as scc
import scipy.signal as sig
from scipy.optimize import curve_fit
from scipy.optimize import fsolve

import argparse

import nds2utils as nu
from nds2utils.make_interactive_svg import make_interactive_svg
from nds2utils.make_interactive_svg import make_interactive_svg_multiple_subplots

# from scipy.io import loadmat

# # Get the official LHO Noisebudget
# NB_LHO_contents = loadmat('LHO_NB_data_o3a.mat',squeeze_me=True, struct_as_record=False)
# # NB_LHO_contents = loadmat('NB_dataOctIss.mat',squeeze_me=True, struct_as_record=False)
# nb = NB_LHO_contents['NB']

mpl.rcParams.update({'figure.figsize': (12, 9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'lines.markersize': 3,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.9,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'legend.columnspacing': 2,
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#################
# Git repo path #
#################

script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

data_dir = f'{chapter_dir}/data'

plot_names = np.array([])



#####   Functions   #####
def median_coherence(power_ratio):
    '''Given a power_ratio of uncorrelated noise over correlated noise, 
    power_ratio = sigma_uncorr^2 / sigma_corr^2,
    returns the power coherence gamma^2 that would result from median-averaged PSDs and CSDs.
    '''
    med_coh =   (power_ratio**2 * np.log(1 + 1/np.sqrt(1 + power_ratio))**2) \
                / (4 * np.log(2)**2 * (1 + power_ratio) * (2 + power_ratio - 2 * np.sqrt(1 + power_ratio)) )
    return med_coh

def residual_median_coherence(input_array, median_coherence_desired):
    '''Finds the residual between the coherence desired and the numerically attempt from fsolve().
    For use as the function input to fsolve().
    Inputs:
    input_array = array of length one, required by fsolve to find the power ratio numerically
    median_coherence_desired = median coherence estimated from the CSD signals.
    Output:
    residual = array of length one with residual from median_coherence() function
    '''
    func_results = median_coherence(input_array[0])
    residual = [func_results - median_coherence_desired]
    return residual

def bias_from_median_coherence_and_power_ratio(median_coherence, power_ratio):
    '''Calculates the mean-to-median bias factor from the median_coherence = gamma^2
    and the power_ratio = epsilon.
    '''
    bias = np.log(2) * np.sqrt( (1 + power_ratio) * median_coherence )
    return bias

def bias_from_median_coherence(median_coherence_estimated, initial_power_ratio=0.1):
    '''Estimates the median/mean bias = b given some median_coherence = gamma^2.

    Numerically solves for the uncorrelated/correated power_ratio = epsilon,
    using the median_coherence.
    Uses scipy.optimize.fsolve() to find the root of residual_median_coherence().
    fsolve() seems to work for gamma^2 between (0.999 and 1e-6)

    This function requires bias_from_median_coherence_and_power_ratio(),
    residual_median_coherence(), median_coherence(), and fsolve() functions.

    Inputs:
    median_coherence_estimated  =   median coherence estimated from |<x,z>|^2/(<x,x> <z,z>) 
                                    where all spectral densities are median-averaged
    initial_power_ratio         =   initial guess of the power_ratio, default is 1.0
    Output:
    bias    =   median/mean bias factor.  Divide median-averaged cross spectral density <x,z> 
                by bias to recover the mean-avearged cross spectral density.             
    '''
    # Numerically estimate the power ratio epsilon from the median coherence
    fsolve_array = fsolve(  residual_median_coherence, 
                            [initial_power_ratio], 
                            args=(median_coherence_estimated))
    power_ratio = fsolve_array[0]

    # Find the bias factor
    bias = bias_from_median_coherence_and_power_ratio(  median_coherence_estimated, 
                                                        power_ratio)
    return bias

def biases_from_coherences(median_coherences, initial_power_ratios=None):
    '''Returns array of biases from an array of coherences.
    Inputs:
    median_coherences =    median-averaged coherence estimates
    initial_power_ratios =  power ratios to start estimation with.  
                            If None, uses 0.1 for all.  Default is None.
    Outputs:
    biases =    array of biases calculated from the coherences
    '''
    if initial_power_ratios is None:
        initial_power_ratios = 0.1 * np.ones_like(median_coherences)
    biases = np.array([])
    for coherence, initial_power_ratio in zip(median_coherences, initial_power_ratios):
        bias = bias_from_median_coherence(coherence, initial_power_ratio=initial_power_ratio)
        biases = np.append(biases, bias)
    return biases

def get_all_csds(pickle_filename, chan_a, chan_b, nperseg, noverlap, balance_dict):
    '''Function that outputs in all power and cross spectral densities taken of the two DCPDs,
    so the user might make a PDF of their distribution themselves.
    This function will dump the dataDict from memory at the end, 
    saving my computer from having to store it all'''
    print('Start reading data')
    start_time = time.time()

    #####   Read in huge datefile   #####
    dataDict = nu.load_pickle(pickle_filename)

    print(f'Finished reading data after {time.time() - start_time} seconds')

    #####   Take CSDs   #####
    # record all of them using spectral_helper
    print('Start taking PSDs and CSDs')
    start_time = time.time()

    aa = dataDict[chan_a]['data'] * balance_dict[chan_a]
    bb = dataDict[chan_b]['data'] * balance_dict[chan_b]

    _, _, Saas = sig.spectral_helper(aa, aa, fs, nperseg=nperseg, noverlap=noverlap) # PSDs of DCPD A
    _, _, Sbbs = sig.spectral_helper(bb, bb, fs, nperseg=nperseg, noverlap=noverlap) # PSDs of DCPD B
    ff, tt, Sabs = sig.spectral_helper(aa, bb, fs, nperseg=nperseg, noverlap=noverlap) # CSDs of A and B

    print(f'Finished taking PSDs and CSDs after {time.time() - start_time} seconds')
    return ff, tt, Saas, Sbbs, Sabs

def calc_all_csds(x, y, fs=1.0, window='hann', nperseg=None, noverlap=None, nfft=None,
        detrend='constant', return_onesided=True, scaling='density', axis=-1):
    '''
    Calculates the cross spectral density from data arrays x and y.
    Copied docstring from scipy.signal._spectral_helper() function.

    The windows are not averaged over; 
    the result from each window is returned.

    Parameters
    ---------
    x : array_like
        Array or sequence containing the data to be analyzed.
    y : array_like
        Array or sequence containing the data to be analyzed. 
    fs : float, optional
        Sampling frequency of the time series. Defaults to 1.0.
    window : str or tuple or array_like, optional
        Desired window to use. If `window` is a string or tuple, it is
        passed to `get_window` to generate the window values, which are
        DFT-even by default. See `get_window` for a list of windows and
        required parameters. If `window` is array_like it will be used
        directly as the window and its length must be nperseg. Defaults
        to a Hann window.
    nperseg : int, optional
        Length of each segment. Defaults to None, but if window is str or
        tuple, is set to 256, and if window is array_like, is set to the
        length of the window.
    noverlap : int, optional
        Number of points to overlap between segments. If `None`,
        ``noverlap = nperseg // 2``. Defaults to `None`.
    nfft : int, optional
        Length of the FFT used, if a zero padded FFT is desired. If
        `None`, the FFT length is `nperseg`. Defaults to `None`.
    detrend : str or function or `False`, optional
        Specifies how to detrend each segment. If `detrend` is a
        string, it is passed as the `type` argument to the `detrend`
        function. If it is a function, it takes a segment and returns a
        detrended segment. If `detrend` is `False`, no detrending is
        done. Defaults to 'constant'.
    return_onesided : bool, optional
        If `True`, return a one-sided spectrum for real data. If
        `False` return a two-sided spectrum. Defaults to `True`, but for
        complex data, a two-sided spectrum is always returned.
    scaling : { 'density', 'spectrum' }, optional
        Selects between computing the cross spectral density ('density')
        where `Pxy` has units of V**2/Hz and computing the cross
        spectrum ('spectrum') where `Pxy` has units of V**2, if `x`
        and `y` are measured in V and `fs` is measured in Hz.
        Defaults to 'density'
    axis : int, optional
        Axis along which the FFTs are computed; the default is over the
        last axis (i.e. ``axis=-1``).

    Returns
    -------
    freqs : ndarray
        Array of sample frequencies.
    tt : ndarray
        Array of times corresponding to each data segment
    Sxys : ndarray
        2D array of cross spectral densities for each window


    '''
    # Calculate the FFTs of each data stream, with flag mode='complex'
    # Fxx and Fyy are the short time Fourier transforms
    freqs, tt, Fxx = sig.spectrogram(x, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap,
                nfft=nfft, detrend=detrend, return_onesided=return_onesided,
                scaling=scaling, axis=axis, mode='complex')
    _, _, Fyy = sig.spectrogram(y, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap,
                nfft=nfft, detrend=detrend, return_onesided=return_onesided,
                scaling=scaling, axis=axis, mode='complex')

    Sxys = np.conjugate(Fxx) * Fyy
    if return_onesided:
        if nfft % 2:
            Sxys[..., 1:] *= 2 # double because we want a one-sided csd from the stfts
        else:
            # Last point is unpaired Nyquist freq point, don't double
            Sxys[..., 1:-1] *= 2

    return freqs, tt, Sxys


def get_median_psd(Saas):
    '''Calculate the median PSD from a bunch of PSDs.  No bias is applied.
    '''
    Saa_med = np.median(np.real(Saas), axis=1)
    return Saa_med

def get_median_csd(Sabs):
    '''Calculate the median CSD from a bunch of CSDs.  No bias is applied.
    '''
    Sab_med = np.median(np.real(Sabs), axis=1) + 1j * np.median(np.imag(Sabs), axis=1)
    return Sab_med

def get_median_coherences(Saas, Sbbs, Sabs):
    '''Calculate the median coherences from all the PSDs and CSDs.
    '''
    Saa_med = get_median_psd(Saas)
    Sbb_med = get_median_psd(Sbbs)
    Sab_med = get_median_csd(Sabs)

    coherences = np.abs(Sab_med)**2 / (Saa_med * Sbb_med)

    return coherences

def get_mean_psd(Saas):
    '''Calculate the mean PSD from a bunch of PSDs.
    '''
    Saa_mean = np.mean(np.real(Saas), axis=1)
    return Saa_mean

def get_mean_csd(Sabs):
    '''Calculate the mean CSD from a bunch of CSDs.
    '''
    Sab_mean = np.mean(np.real(Sabs), axis=1) + 1j * np.mean(np.imag(Sabs), axis=1)
    return Sab_mean

def get_mean_and_median_psds_and_csds(Sxxs, Syys, Sxys):
    '''Retrieve the means and medians of the PSDs and CSDs provided.
    No mean-to-median biases are applied to the output.

    Output:
    tuple: (mean_psd, median_psd, mean_csd, median_csd)
    '''
    # PSD calcs
    # mean
    mean_xx_psd = get_mean_psd(Sxxs)
    mean_yy_psd = get_mean_psd(Syys)
    mean_psd = (mean_xx_psd + mean_yy_psd) / 2

    # median
    median_xx_psd = get_median_psd(Sxxs)
    median_yy_psd = get_median_psd(Syys)
    median_psd = (median_xx_psd + median_yy_psd) / 2

    # CSD calcs
    # mean
    mean_csd = get_mean_csd(Sxys)

    # median
    median_csd = get_median_csd(Sxys)

    return mean_psd, median_psd, mean_csd, median_csd

def get_mean_coherences(Saas, Sbbs, Sabs):
    '''Calculate the mean coherences from all the PSDs and CSDs.
    '''
    Saa_mean = get_mean_psd(Saas)
    Sbb_mean = get_mean_psd(Sbbs)
    Sab_mean = get_mean_csd(Sabs)

    coherences = np.abs(Sab_mean)**2 / (Saa_mean * Sbb_mean)

    return coherences

def remove_darm_olg_psds(Saas, darm_olg):
    '''Removes the DARM open loop gain from a single DCPD PSD.
    Solves for (H x) squared in Equation 4 of 
    https://dcc.ligo.org/DocDB/0141/T1700131/001/OnlineCrossCorr.pdf
    '''
    num_ff, num_tt = np.shape(Saas) # all must be the same shape

    Sxxs = np.zeros_like(Saas, dtype=complex)
    for ii in np.arange(num_tt):
        Saa = Saas[:,ii]
        Sxx = Saa * 4 * np.abs(1 + darm_olg)**2
        Sxxs[:,ii] = Sxx

    return Sxxs

def remove_darm_olg_csds(Saas, Sbbs, Sabs, darm_olg):
    '''Removes the DARM open loop gain from every DCPD CSD,
    via equation 10 of Kiwamu LIGO DCC-T1700131 
    '''
    num_ff, num_tt = np.shape(Saas) # all must be the same shape

    Sxys = np.zeros_like(Saas, dtype=complex)
    for ii in np.arange(num_tt):
        Saa = Saas[:,ii]
        Sbb = Sbbs[:,ii]
        Sab = Sabs[:,ii]

        corr_noise_full =   (np.abs(darm_olg)**2 + 2 * darm_olg) * Saa + \
                            (np.abs(darm_olg)**2 + 2 * np.conj(darm_olg)) * Sbb + \
                            np.abs(darm_olg)**2 * np.conj(Sab) + \
                            np.abs(2 + darm_olg)**2 * Sab

        Sxys[:,ii] = corr_noise_full

    return Sxys

def calibrate_into_meters(Sxys, sensing_function):
    '''Calibrate from mA to meters using the DARM sensing function C in meters/mA
    '''
    num_ff, num_tt = np.shape(Sxys)

    Sxys_cal = np.zeros_like(Sxys,  dtype=complex)
    for ii in np.arange(num_tt):
        Sxy = Sxys[:,ii]
        Sxys_cal[:,ii] = Sxy / np.abs(sensing_function)**2
    return Sxys_cal

def asymmetric_laplace(x, loc, scale, kappa):
    '''Defines an asymmetric laplace probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplacian
    scale   = scale parameter for broadening the PDF width
    kappa   = asymmetric parameter 
    Output:
    pdf     = asymmetric laplace probability distribution function on input x
    '''
    sign = np.sign(x - loc) # either +1 or -1 
    pdf = ( 1 / (scale * (kappa + 1/kappa)) ) * np.exp(-sign * (x - loc) * kappa**sign / scale)
    return pdf

def mid_bins(bins):
    '''Returns the middle of bins given from the native mpl.hist() output'''
    diff_bins = np.diff(bins)
    middle_bins = bins[:-1] + diff_bins
    return middle_bins

def kappa_to_mean_coherence(kappa):
    '''Takes in kappa from asymmetric Laplace distribution.
    Returns what the mean-averaged coherence would be for such a kappa.
    '''
    coherence = ( ( 1 - kappa**2 )/( 1 + kappa**2 ) )**2
    return coherence

#####   Signal processing   #####
channels = np.array([
    'H1:OMC-DCPD_A_OUT_DQ', 
    'H1:OMC-DCPD_B_OUT_DQ', 
    'H1:OMC-DCPD_SUM_OUT_DQ', 
    'H1:OMC-DCPD_NULL_OUT_DQ', 
    'H1:LSC-DARM_IN1_DQ',       # needed for comparing DARM cts to DCPD mA
    ])
dcpd_a = 'H1:OMC-DCPD_A_OUT_DQ'
dcpd_b = 'H1:OMC-DCPD_B_OUT_DQ'

# Balance the pds
ratio_A_over_B = 1.0441 # 1.0 
mutliplier = 1 + (ratio_A_over_B - 1)/2.0
balance_dict = {
    dcpd_a   : 1.01039 / mutliplier,
    dcpd_b   : 0.98961 * mutliplier,
}

### Data file ###
# gps_start = 1283317218    # September 5, 2020 05:00:00.000 UTC
# gps_stop  = 1283346018    # September 5, 2020 13:00:00.000 UTC
# csd_date = 'September 2020'
# pickle_filename = '/Volumes/SandiskSD/ccahilla/Git/thesis/chapters/correlated_noise/data/DCPD_data_gps_start_1283317218_gps_stop_1283346018.pkl'

gps_start = 1282730359          # August 29 2020 09:59:01
gps_stop  = 1282763937          # August 29 2020 19:18:39
csd_date = 'August 2020'
pickle_filename = '/Volumes/SandiskSD/ccahilla/Git/thesis/chapters/correlated_noise/data/DCPD_data_gps_start_1282730359_gps_stop_1282763937.pkl'

# gps_start = 1256380478          # October 2019
# gps_stop = 1256390478
# csd_date = 'October 2019'
# pickle_filename = '/Volumes/SandiskSD/ccahilla/Git/o3-commissioning-paper/data/gated_DCPD_data_gps_start_1256380478_gps_stop_1256390478.pkl'

duration = gps_stop - gps_start
fs = 16384                      # Hz
NN = fs * duration              # total samples

binwidth = 2  # Hz
overlap = 0.0
averages = nu.dtt_averages(duration, binwidth, overlap) # should have 20000 averages
bin_num = (fs / 2) / binwidth # nyquist / binwidth, should be 4196 

nperseg = NN / averages         # samples per fft segment
noverlap = nperseg * overlap    # samples of overlap between segments
print()
print(f'Averages = {averages}')
print(f'Number of bins = {bin_num}')
print()

#####   Read in DARM stuff   #####
if gps_start == 1283317218:
    DARM_multiplier = 1.0
    DARM_OLG_file = f'{data_dir}/LHO_pyDARM_OLG_for_HF_interp_from_2020_08_28.txt'
elif gps_start == 1282730359:
    DARM_multiplier = 1.0
    DARM_OLG_file = f'{data_dir}/LHO_pyDARM_OLG_for_HF_interp_from_2020_08_28.txt'
else:
    DARM_multiplier = 1.0
    DARM_OLG_file = f'{data_dir}/LHO_pyDARM_OLG_for_HF_interp_from_2019_10_28_H1_0ctSRCLOffset_for_LF.txt'

ratio_sq = 1894811700000.0 # (mA/cts)^2, from ratio of PSD_SUM / PSD_ERR

# Get calibration functions
DARM_sensing_file = f'{data_dir}/LHO_pyDARM_sensing_function_mA_per_meter.txt'
DARM_sensing_data = np.loadtxt(DARM_sensing_file) # DCPD SUM to DARM [actually in cts/meter]
ff_C = DARM_sensing_data[:,0]
C_orig = DARM_sensing_data[:,1] * np.exp(1j * DARM_sensing_data[:,2]) # cts/m
C_orig = C_orig * np.sqrt(ratio_sq) # mA/m = cts/m * mA/cts

print(f'DARM_OLG_file = {DARM_OLG_file}')
DARM_OLG_data = np.loadtxt(DARM_OLG_file)
ff_G = DARM_OLG_data[:,0]
G_orig = DARM_OLG_data[:,1] * np.exp(1j * DARM_OLG_data[:,2]) * DARM_multiplier



#####   Get all PSDs and CSDs   #####
# This will be memory heavy
# freq vector is the first axis, time vector is the second
# in other words, the first PSD by itself is Saas[:,0], etc

ff, tt, Saas, Sbbs, Sabs = get_all_csds(pickle_filename, dcpd_a, dcpd_b, nperseg, noverlap, balance_dict)



#####   Do PSD rejection   #####
# Reject any PSD value above 14 times the median PSD value.
# The CDF of the exponential F(14 * rho) = 1 - exp(-14 * log(2) ) = 0.999938
ff_index = 20 
outlier_psd_indicies = np.argwhere(np.median(Saas[ff_index,:]) * 14 < Saas[ff_index,:])[:,0]
good_psd_indicies = np.argwhere(np.median(Saas[ff_index,:]) * 14 >= Saas[ff_index,:])[:,0]

good_averages = len(good_psd_indicies)



#####   Remove DARM OLG from CSDs   #####
# If freq vectors are not the same
if (len(ff) != len(ff_C)) or (len(ff) != len(ff_G)):
    C = nu.get_complex_interp(ff, ff_C, C_orig)
    G = nu.get_complex_interp(ff, ff_G, G_orig)

Sxxs = remove_darm_olg_psds(Saas, G)
Syys = remove_darm_olg_psds(Sbbs, G)
Sxys = remove_darm_olg_csds(Saas, Sbbs, Sabs, G)



#####   Calibrate into meters   #####
Sxxs_cal = calibrate_into_meters(Sxxs, C)
Syys_cal = calibrate_into_meters(Syys, C)
Sxys_cal = calibrate_into_meters(Sxys, C)



#####   Postprocess   #####
plot_Sxxs = Sxxs_cal
plot_Syys = Syys_cal
plot_Sxys = Sxys_cal

units = 'meters'
plot_label = f'gps_start_{gps_start}'

mean_darm_psd, median_darm_psd, mean_csd, median_csd = get_mean_and_median_psds_and_csds(plot_Sxxs, plot_Syys, plot_Sxys)
good_mean_darm_psd, _, good_mean_csd, _ = get_mean_and_median_psds_and_csds(plot_Sxxs[:, good_psd_indicies], 
                                                                            plot_Syys[:, good_psd_indicies], 
                                                                            plot_Sxys[:, good_psd_indicies])

# Mean coherence from PSD rejection  
good_mean_coherence = get_mean_coherences(  plot_Sxxs[:, good_psd_indicies], 
                                            plot_Syys[:, good_psd_indicies], 
                                            plot_Sxys[:, good_psd_indicies])

# Get median coherences and biases
median_coherences = get_median_coherences(Sxxs_cal, Syys_cal, Sxys_cal)
biases = biases_from_coherences(median_coherences)

# PSDs and CSDs, with mean-to-median bias correction
mean_darm_asd = np.sqrt(mean_darm_psd)
median_darm_asd = np.sqrt(median_darm_psd / np.log(2))
mean_asd = np.sqrt(np.abs(mean_csd))
median_asd_bias_corrected = np.sqrt( np.abs(median_csd) / biases )

good_mean_darm_asd = np.sqrt(good_mean_darm_psd)
good_mean_asd = np.sqrt(np.abs(good_mean_csd))


# save the median asd 
data_dir = f'{chapter_dir}/data'

save_filename = f'lho_correlated_noise_median_averaged_asd_bias_corrected_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
save_full_filename = f'{data_dir}/{save_filename}'
save_data = np.vstack((ff, median_asd_bias_corrected)).T
print('\033[93m')
print(f'Saving file: {save_full_filename}')
print('\033[0m')
np.savetxt(save_full_filename, save_data, header='Frequency [Hz], LHO correlated noise median averaged asd bias corrected [m/rtHz]')

save_filename = f'lho_darm_noise_asd_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
save_full_filename = f'{data_dir}/{save_filename}'
save_data = np.vstack((ff, median_darm_asd)).T
print('\033[93m')
print(f'Saving file: {save_full_filename}')
print('\033[0m')
np.savetxt(save_full_filename, save_data, header='Frequency [Hz], LHO DARM noise median averaged asd bias corrected [m/rtHz]')


#####   Figures   #####
plot_names = np.array([])

# Plot every nth Sxys_cal we've taken, along with the 90% confidence intervals
fig, (s1) = plt.subplots(1)

ff_num, tt_num = np.shape(plot_Sxys)

s1.loglog(ff, mean_darm_asd, color='C6', label='Mean PSD')
s1.loglog(ff, median_darm_asd, color='C8', label='Median PSD (' + r'$\log(2)$' + f' bias applied)')

s1.loglog(ff, mean_asd, color='C1', label='Mean CSD')
s1.loglog(ff, median_asd_bias_corrected, ls='-', color='C3', alpha=1.0, label=f'Median CSD with bias applied')

s1.loglog(ff, good_mean_darm_asd, ls='--', color='C9', label=f'Mean PSD (using PSD rejection) - {good_averages} averages')
s1.loglog(ff, good_mean_asd, ls='--', color='C2', label=f'Mean CSD (using PSD rejection) - {good_averages} averages')

s1.set_xlim([2e1, 5e3])
s1.set_ylim([5e-21, 1e-18])

s1.set_title(f'LHO correlated noise - {csd_date} - {tt_num} averages')
s1.set_xlabel(f'Frequency [Hz]')
s1.set_ylabel(r'DARM [$\mathrm{m}/\sqrt{\mathrm{Hz}}$]')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.7)
s1.legend(fontsize=12, markerscale=3)

plot_name = f'lho_correlated_noise_all_csds_averages_{int(averages)}_{plot_label}.pdf'
full_plot_name = f'{fig_dir}/{plot_name}'
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.tight_layout()
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Plot coherence and biases
fig, (s1, s2) = plt.subplots(2)

s1.loglog(ff, good_mean_coherence, color='C4', label=f'Mean coherence (via PSD rejection) - {good_averages} averages')
s1.loglog(ff, median_coherences, color='C5', label='Median coherence')
s2.semilogx(ff, biases, color='C6', label='Mean to median biases')


s1.set_title(f'Median coherence and bias - {tt_num} averages')
s1.set_ylabel(f'Median coherence ' + r'$\widetilde{ \gamma^2_{xy} }$')
s2.set_ylabel(f'Bias ' + r'$b = \rho / \mu $')
s2.set_xlabel(f'Frequency [Hz]')

s1.set_yticks(nu.good_ticks(s1))

s1.set_xlim([4e1, 5e3])
s2.set_xlim([4e1, 5e3])
s2.set_ylim([0.5, np.log(2)])

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.7)
s1.legend()

s2.grid()
s2.grid(which='minor', ls='--', alpha=0.7)
s2.legend()

plot_name = f'coherence_and_bias_averages_{int(averages)}_{plot_label}.pdf'
full_plot_name = f'{fig_dir}/{plot_name}'
plot_names = np.append(plot_names, full_plot_name)
print(f'Writing plot PDF to {full_plot_name}')
plt.tight_layout()
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# CSD 2D histogram of some individual frequency bins
plot_ffs = np.array([20, 50, 200, 700, 2300, 4500]) #, 350, 450, 700, 1400, 2400, 4500])
plot_ff_indicies = plot_ffs // 2

for plot_ff, plot_ff_index in zip(plot_ffs, plot_ff_indicies):
    # if not plot_ff == 200:
    #     continue

    Sxys_mid = plot_Sxys[plot_ff_index, :] # get every CSDs result at plot_ff frequency
    Sxys_mid_real = np.real(Sxys_mid)
    Sxys_mid_imag = np.imag(Sxys_mid)

    Sxys_mid_real_mean = np.mean(Sxys_mid_real)
    Sxys_mid_real_median = np.median(Sxys_mid_real)

    Sxys_mid_imag_mean = np.mean(Sxys_mid_imag)
    Sxys_mid_imag_median = np.median(Sxys_mid_imag)


    fig, ss = plt.subplots(2, 2, sharex="col", sharey="row", figsize=(12, 12),
                            gridspec_kw=dict(   height_ratios=[1, 2.5],
                                                width_ratios=[2.5, 1])
                            )
    s1 = ss[0, 0]   # top histogram
    s2 = ss[0, 1]   # delete
    s3 = ss[1, 0]   # 2d hist
    s4 = ss[1, 1]   # right histogram

    fig.delaxes(s2) # delete

    num_bins = 300

    confidence_interval = 99.0
    re_low_bin  = np.percentile(Sxys_mid_real, 50 - confidence_interval/2)
    re_high_bin = np.percentile(Sxys_mid_real, 50 + confidence_interval/2)
    im_low_bin  = np.percentile(Sxys_mid_imag, 50 - confidence_interval/2)
    im_high_bin = np.percentile(Sxys_mid_imag, 50 + confidence_interval/2)

    rebins = np.linspace(re_low_bin, re_high_bin, num_bins+1)
    imbins = np.linspace(im_low_bin, im_high_bin, num_bins+1)    

    # Histograms
    reNN, rebins, repatches = s1.hist(Sxys_mid_real, bins=rebins, histtype='step', lw=2, zorder=3, density=True,
                                        label=r'$\Re(S_{xy})$ ' + 'CSD samples')  
    imNN, imbins, impatches = s4.hist(Sxys_mid_imag, bins=imbins, histtype='step', lw=2, zorder=3, density=True, orientation='horizontal',
                                        label=r'$\Im(S_{xy})$ ' + 'CSD samples')  
    h, xedges, yedges, image = s3.hist2d(Sxys_mid_real, Sxys_mid_imag, bins=(rebins, imbins), density=True, 
                                        cmap=mpl.cm.copper, norm=mpl.colors.LogNorm())   

    # Fits
    xx = np.linspace(rebins[0], rebins[-1], 1001)

    rescaler = np.max(reNN) # should be a huge number
    rep0 = [0, 1, 0.9]
    middle_bins = mid_bins(rebins)
    laplace_xy_real_fit_params, laplace_xy_real_fit_cov  = curve_fit(asymmetric_laplace, middle_bins * rescaler, reNN/rescaler, p0=rep0)
    laplace_xy_real_fit_params[0] /= rescaler
    laplace_xy_real_fit_params[1] /= rescaler
    print(f'laplace_xy_real_fit_params = {laplace_xy_real_fit_params}')
    laplace_xy_real_pdf_fit = asymmetric_laplace(xx, *laplace_xy_real_fit_params)

    yy = np.linspace(imbins[0], imbins[-1], 1001)

    imscaler = np.max(imNN) # should be a huge number
    imp0 = [0, 1, 1.0]
    middle_bins = mid_bins(imbins)
    laplace_xy_imag_fit_params, laplace_xy_imag_fit_cov  = curve_fit(asymmetric_laplace, middle_bins * imscaler, imNN/imscaler, p0=imp0)
    laplace_xy_imag_fit_params[0] /= imscaler
    laplace_xy_imag_fit_params[1] /= imscaler
    print(f'laplace_xy_imag_fit_params = {laplace_xy_imag_fit_params}')
    laplace_xy_imag_pdf_fit = asymmetric_laplace(yy, *laplace_xy_imag_fit_params)

    # Plot fit and stats
    s1.plot(xx, laplace_xy_real_pdf_fit, color='k', lw=4, 
            label='Asymmetric Laplace PDF ' + r'$\Re(S_{xy})$' + '\n' + \
                f'center $m$ = {laplace_xy_real_fit_params[0]:.2e}' + '\n' + \
                f'scale $\lambda$ = {laplace_xy_real_fit_params[1]:.2e}' + '\n' + \
                f'skew $\kappa$ = {laplace_xy_real_fit_params[2]:.2e}'
                )

    s1.axvline(Sxys_mid_real_mean, ymin=0, ymax=np.max(reNN)*1.2, color='C2', label=f'mean = {Sxys_mid_real_mean:.2e}')
    s1.axvline(Sxys_mid_real_median, ymin=0, ymax=np.max(reNN)*1.2, ls='--', color='C3', label=f'median = {Sxys_mid_real_median:.2e}')
    # s1.axvline(Sxys_mid_real_rms, ymin=0, ymax=np.max(reNN)*1.2, ls=':', color='C4', label=f'rms = {Sxys_mid_real_rms:.2e}')

    # imNN, imbins, impatches = s2.hist(Sxys_mid_imag, bins=bins, histtype='step', lw=2, zorder=3, density=True,
    #                             label='CSD imaginary samples')   

    s4.plot(laplace_xy_imag_pdf_fit, yy, color='k', lw=4, 
            label='Asymmetric Laplace PDF ' + r'$\Im(S_{xy})$' + '\n' + \
                f'center $m$ = {laplace_xy_imag_fit_params[0]:.2e}' + '\n' + \
                f'scale $\lambda$ = {laplace_xy_imag_fit_params[1]:.2e}' + '\n' + \
                f'skew $\kappa$ = {laplace_xy_imag_fit_params[2]:.2e}'
                )
    s4.axhline(Sxys_mid_imag_mean, xmin=0, xmax=np.max(imNN)*1.2, color='C2', label=f'mean = {Sxys_mid_imag_mean:.2e}')
    s4.axhline(Sxys_mid_imag_median, xmin=0, xmax=np.max(imNN)*1.2, ls='--', color='C3', label=f'median = {Sxys_mid_imag_median:.2e}')
    # s4.axhline(Sxys_mid_imag_rms, xmin=0, xmax=np.max(imNN)*1.2, ls=':', color='C4', label=f'rms = {Sxys_mid_imag_rms:.2e}')

    s1.set_title(f'LHO correlated CSD '+r'$f =$' + f' {plot_ff} Hz', x=0.7)

    s3.set_xlabel(r'$\Re{(\mathrm{CSD})}$ values [$\mathrm{m^2}/\mathrm{Hz}$]')
    s3.set_ylabel(r'$\Im{(\mathrm{CSD})}$ values [$\mathrm{m^2}/\mathrm{Hz}$]')

    # s2.set_title(r'$\Im{(\mathrm{CSD})}$ ' + f'{avg} averages')
    # s2.set_xlabel(r'$\Im{(\mathrm{CSD})}$ values [$\mathrm{V^2}/\mathrm{Hz}$]')
    # s2.set_ylabel('Number of Occurances')

    s1.legend(fontsize=12, bbox_to_anchor=(1.0, 1.0))
    s4.legend(fontsize=12, bbox_to_anchor=(0.95, 1.25))

    s1.grid()
    s3.grid()
    s4.grid()

    # s2.legend()
    # s2.grid()

    plot_name = f'dcpd_csd_2d_histograms_for_frequency_bin_{plot_ff}_Hz_{plot_label}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()


# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()
