'''
lho_xcor_animation.py

Animates the CSD data averaging down.

Craig Cahillane
November 18, 2020
'''

import os
import time
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt

import argparse

import nds2utils as nu

mpl.rcParams.update({'figure.figsize': (12, 9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'lines.markersize': 3,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.9,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'legend.columnspacing': 2,
                     'savefig.dpi': 80,
                     'pdf.compression': 9})



#####   Git repo path   #####

script_path = os.path.abspath(__file__)
chapter_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
data_dir = f'{chapter_dir}/data'

fig_dir = f'{chapter_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

# Place to save large data
if 'ccahilla' in chapter_dir:
    external_hd_dir = '/Volumes/SandiskSD/ccahilla/Git/thesis/chapters/correlated_noise/data' # Craig-specific code
else:
    external_hd_dir = f'{chapter_dir}' # save on personal computer, might be pretty large (~10 GB data)

#####   Functions   #####
def parse_args():
    ''' Parse command-line arguments using argparse library 
    '''
    parser = argparse.ArgumentParser(description='DARM plant model comparison')
    parser.add_argument('--uncalibrated', '-u', action='store_true', 
                        help='Flag. If set, plots the uncalibrated correlated noise in mA^2/Hz.')
    parser.add_argument('--logbin', '-l', action='store_false',
                        help='Flag. If set, disables logbinning for the interactive plot.')
    # parser.add_argument('--residuals', '-r', action='store_true', 
    #                     help='Flag. If set, adds residuals to the  interactive plot.')

    args = parser.parse_args()
    return args

def read_psd_data(filename):
    '''Read two columns of data from .txt, Frequency, PSD.
    Return two arrays.
    '''
    data = np.loadtxt(filename)
    ff = data[1:,0]
    psd = data[1:,1] 
    return ff, psd

def read_csd_data(filename):
    '''Read three columns of data from .txt, Frequency, CSD Mag, CSD Phase.  
    Return two arrays.
    '''
    data = np.loadtxt(filename)
    ff = data[1:,0]
    csd = data[1:,1] * np.exp(1j * data[1:,2]) 
    return ff, csd

def darm_psd_from_dcpd_sum(Scc, darm_olg):
    '''Removes the DARM open loop gain from a single DCPD PSD.
    Solves for (H x) squared in Equation 4 of 
    https://dcc.ligo.org/DocDB/0141/T1700131/001/OnlineCrossCorr.pdf
    '''
    darm_psd = Scc * np.abs(1 + darm_olg)**2
    return darm_psd

def plottable_asd(args, ff, psd, darm_sensing):
    '''Applies user arguments like calibration and logbinning to the data.

    Inputs:
    args                = struct with user arguments.  Output from parse_args()
    psd                 = DCPD PSD
    darm_sensing        = calibration TF from mA/m to apply to data if needed.

    Outputs:
    (plot_ff, plot_correlated_noise) = two arrays tuple,  
    plot_ff                 = frequency vector
    plot_correlated_noise   = correlated noise ASD to be plotted
    '''
    if args.uncalibrated:
        temp_psd = psd
    else:    
        temp_psd = psd / np.abs(darm_sensing)**2          # m^2/Hz

    if args.logbin:
        fflog = np.logspace(np.log10(10), np.log10(7000), 1000)
        plot_ff, temp_psd = nu.linear_log_ASD(fflog, ff, temp_psd)
    else:
        plot_ff = ff

    plot_asd = np.sqrt(np.abs(temp_psd))

    return plot_ff, plot_asd

def correlated_noise(psd_a, psd_b, csd_ab, darm_olg):
    '''Calculates the correlated noise between the two DCPDs using Equation 10 of 
    https://dcc.ligo.org/public/0141/T1700131/001/OnlineCrossCorr.pdf
    All inputs should have the same frequency vector.

    Inputs:
    psd_a       = PSD from DCPD A
    psd_b       = PSD from DCPD B
    csd_ab      = CSD from DCPDs A and B, data from A is conjugated like <A*|B>
    darm_olg    = DARM open loop gain

    Output:
    corr_noise  = the complex array correlated noise.
    '''
    corr_noise =    (np.abs(darm_olg)**2 + 2 * darm_olg) * psd_a + \
                    (np.abs(darm_olg)**2 + 2 * np.conj(darm_olg)) * psd_b + \
                    np.abs(darm_olg)**2 * np.conj(csd_ab) + \
                    np.abs(2 + darm_olg)**2 * csd_ab

    return corr_noise

def plottable_correlated_noise(args, ff, correlated_noise, darm_sensing):
    '''Applies user arguments like calibration and logbinning to the data.

    Inputs:
    args                = struct with user arguments.  Output from parse_args()
    correlated_noise    = correlated noise CSD
    darm_sensing        = calibration TF from mA/m to apply to data if needed.

    Outputs:
    (plot_ff, plot_correlated_noise) = two arrays tuple,  
    plot_ff                 = frequency vector
    plot_correlated_noise   = correlated noise ASD to be plotted
    '''
    if args.uncalibrated:
        temp_correlated_noise = correlated_noise
    else:    
        temp_correlated_noise = correlated_noise / np.abs(darm_sensing)**2          # m^2/Hz

    if args.logbin:
        fflog = np.logspace(np.log10(10), np.log10(7000), 1000)
        plot_ff, temp_correlated_noise = nu.linear_log_ASD(fflog, ff, temp_correlated_noise)
    else:
        plot_ff = ff

    # plot_correlated_noise = np.sqrt(np.abs(temp_correlated_noise))
    plot_correlated_noise = temp_correlated_noise

    return plot_ff, plot_correlated_noise


def get_unsqueezed_correlated_noise_from_squeezed_data(psd_a, psd_b, csd_ab, darm_olg):
    '''Calculates the unsqueezed correlated noise from squeezed spectral densities
    of each DCPD.
    This assumes the shot noise PDs are anti-correlated and in the squeezing-dominated regime.
    Math found in the mathematica notebook:
    split_sensor_simple_control_loop_system_system_of_equations.nb
    '''
    G = darm_olg

    Gc = np.conjugate(G)
    csd_ba = np.conjugate(csd_ab)

    # total_prefix = 1/(2 - G - Gc)**2
    # psd_a_coeff = 2 * (G * (1 - Gc) + Gc) * (2 * G + 2 * Gc -2 - G**2 - Gc**2)
    # psd_b_coeff = -4 * (1 - G) * (1 - Gc) * (G * (1 - Gc) + Gc) 
    # csd_ab_coeff = 2 * (2 - G * (2 - G)) * (1 - Gc) * (2 - G - Gc)
    # csd_ba_coeff = 2 * (1 - G) * (2 - G - Gc) * (2 - Gc * (2 - Gc))

    # corr_noise = total_prefix * ( psd_a_coeff * psd_a +
    #                               psd_b_coeff * psd_b +
    #                               csd_ab_coeff * csd_ab +
    #                               csd_ba_coeff * csd_ba
    #                             )
    corr_noise  = (2*(csd_ab*(2 - 2*G + G**2)*(-1 + Gc)*(-2 + G + Gc) + csd_ba*(-1 + G)*(-2 + G + Gc)*(2 - 2*Gc + Gc**2) + 
                - (G*(-1 + Gc) - Gc)*(2*psd_b*(-1 + G)*(-1 + Gc) + psd_a*(2 - 2*G + G**2 - 2*Gc + Gc**2))))/((-2 + G + Gc)**2)
    return corr_noise

def phase_domain_shift(csd, phase_shift=np.pi/2):
    '''Shifts the domain we take the angle of a csd 
    (or any array of complex numbers) by phase_shift.
    '''
    temp = csd * np.exp(-1j * phase_shift)
    temp_angle = np.angle(temp)
    shift_angle = temp_angle + phase_shift
    return shift_angle

def make_unsqueezed_correlated_noise_plot(args):
    '''Tests the idea of measuring correlated noise during a time with squeezing
    '''
    #####   Read data   #####
    gps_start = 1284249618 # Sep 16 00:00:00 2020, sqz time
    gps_stop  = 1284270167 # Sep 16 05:42:29 2020
    averages = 41098
    csd_date = '16 September 2020 sqz'
    darm_label = 'Measured noise with squeezing'

    gps_start_no_sqz = 1284270168 # Sep 16 05:42:30 2020, no sqz time
    gps_stop_no_sqz  = 1284304916 # Sep 16 15:21:38 2020
    averages_no_sqz = 69496
    csd_date_no_sqz = '16 September 2020 no sqz'
    darm_label_no_sqz = 'Measured noise no squeezing'

    # gps_start = 1283317218    # September 5, 2020 05:00:00.000 UTC
    # gps_stop  = 1283346018    # September 5, 2020 13:00:00.000 UTC
    # averages = 57600
    # csd_date = '5 September 2020'
    # darm_label = 'Measured noise during xcor (sum channel)'

    # load sqz
    data_dir = f'{chapter_dir}/data'
    psd_a_filename  = f'{data_dir}/raw_psd_a_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    psd_b_filename  = f'{data_dir}/raw_psd_b_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    csd_ab_filename = f'{data_dir}/raw_csd_ab_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    psd_sum_filename = f'{data_dir}/raw_psd_sum_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    psd_null_filename = f'{data_dir}/raw_psd_null_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'
    darm_error_filename = f'{data_dir}/raw_darm_error_gps_start_{gps_start}_gps_stop_{gps_stop}.txt'

    ff, psd_a = read_psd_data(psd_a_filename)
    _, psd_b = read_psd_data(psd_b_filename)
    _, csd_ab = read_csd_data(csd_ab_filename)
    _, psd_sum = read_psd_data(psd_sum_filename)
    _, psd_null = read_psd_data(psd_null_filename)
    _, darm_error = read_psd_data(darm_error_filename)

    # load no sqz
    psd_a_filename_no_sqz  = f'{data_dir}/raw_psd_a_gps_start_{gps_start_no_sqz}_gps_stop_{gps_stop_no_sqz}.txt'
    psd_b_filename_no_sqz  = f'{data_dir}/raw_psd_b_gps_start_{gps_start_no_sqz}_gps_stop_{gps_stop_no_sqz}.txt'
    csd_ab_filename_no_sqz = f'{data_dir}/raw_csd_ab_gps_start_{gps_start_no_sqz}_gps_stop_{gps_stop_no_sqz}.txt'
    psd_sum_filename_no_sqz = f'{data_dir}/raw_psd_sum_gps_start_{gps_start_no_sqz}_gps_stop_{gps_stop_no_sqz}.txt'
    psd_null_filename_no_sqz = f'{data_dir}/raw_psd_null_gps_start_{gps_start_no_sqz}_gps_stop_{gps_stop_no_sqz}.txt'
    darm_error_filename_no_sqz = f'{data_dir}/raw_darm_error_gps_start_{gps_start_no_sqz}_gps_stop_{gps_stop_no_sqz}.txt'

    ff, psd_a_no_sqz = read_psd_data(psd_a_filename_no_sqz)
    _, psd_b_no_sqz = read_psd_data(psd_b_filename_no_sqz)
    _, csd_ab_no_sqz = read_csd_data(csd_ab_filename_no_sqz)
    _, psd_sum_no_sqz = read_psd_data(psd_sum_filename_no_sqz)
    _, psd_null_no_sqz = read_psd_data(psd_null_filename_no_sqz)
    _, darm_error_no_sqz = read_psd_data(darm_error_filename_no_sqz)

    _, darm_olg = read_csd_data(f'{data_dir}/LHO_pyDARM_OLG.txt')
    G = darm_olg[1::2]

    ratio_psd = np.median(psd_sum / darm_error)
    ratio_psd_no_sqz = np.median(psd_sum_no_sqz / darm_error_no_sqz)
    print()
    print(f'ratio_psd        = {ratio_psd} mA^2/cts^2')
    print(f'ratio_psd_no_sqz = {ratio_psd_no_sqz} mA^2/cts^2')
    print()

    ratio_sq = np.sqrt(ratio_psd) # 1894811700000.0 # (mA/cts)^2, from ratio of PSD_SUM / PSD_ERR
    ratio_sq_no_sqz = np.sqrt(ratio_psd_no_sqz)
    DARM_multiplier = 1.0   #

    _, darm_sensing = read_csd_data(f'{data_dir}/LHO_pyDARM_sensing_function_mA_per_meter.txt') # actually cts/m
    darm_sensing = darm_sensing[1::2]
    C = darm_sensing * ratio_sq
    C_no_sqz = darm_sensing * ratio_sq_no_sqz

    #####   Squeezing levels   #####
    # may use the sum_over_null variables below in measure of sqz instead
    sqz_db = 2.0 # +- 0.1 dB
    r_sqz = sqz_db/20.0 * np.log(10.0)
    # S_sqz = np.exp(-2.0 * r_sqz)



    #####   Calculate DARM   #####
    psd_sum0_no_sqz = darm_psd_from_dcpd_sum(psd_sum_no_sqz, G)
    plot_ff, plot_psd_sum_no_sqz = plottable_asd(args, ff, psd_sum0_no_sqz, C_no_sqz)

    psd_sum0 = darm_psd_from_dcpd_sum(psd_sum, G)
    plot_ff, plot_psd_sum = plottable_asd(args, ff, psd_sum0, C)
    plot_ff, plot_psd_null = plottable_asd(args, ff, psd_null, C)



    #####   Calculate correlated noise   #####
    # no sqz
    corr_noise_no_sqz = correlated_noise(psd_a_no_sqz, psd_b_no_sqz, csd_ab_no_sqz, G)
    plot_ff, plot_corr_noise_no_sqz = plottable_correlated_noise(args, ff, corr_noise_no_sqz, C_no_sqz)

    plot_asd_corr_noise_no_sqz = np.sqrt(np.abs(plot_corr_noise_no_sqz))

    # measure of sqz
    sum_over_null_ratio = psd_sum0 / psd_null
    sum_over_null_ratio_db = 10.0 * np.log10(sum_over_null_ratio) # dB

    sum_sqz_over_sum_ratio = psd_sum0 / psd_sum0_no_sqz
    sum_sqz_over_sum_ratio_db = 10.0 * np.log10(sum_sqz_over_sum_ratio) # dB

    sum_over_null_ratio_minus_corr = (psd_sum0 - corr_noise_no_sqz) / psd_null
    sum_over_null_ratio_minus_corr_db = 10.0 * np.log10(sum_over_null_ratio_minus_corr) # dB

    S_sqz = sum_over_null_ratio_minus_corr

    # with sqz
    corr_noise_with_sqz = correlated_noise(psd_a, psd_b, csd_ab, G)
    corr_noise_sqz_removed = corr_noise_with_sqz - psd_null * (S_sqz - 1)
    # corr_noise_sqz_removed = get_unsqueezed_correlated_noise_from_squeezed_data(psd_a, psd_b, csd_ab, G)

    plot_ff, plot_corr_noise_with_sqz = plottable_correlated_noise(args, ff, corr_noise_with_sqz, C)
    plot_ff, plot_corr_noise_sqz_removed = plottable_correlated_noise(args, ff, corr_noise_sqz_removed, C)

    plot_asd_corr_noise_with_sqz = np.sqrt(np.abs(plot_corr_noise_with_sqz))
    plot_asd_corr_noise_sqz_removed = np.sqrt(np.abs(plot_corr_noise_sqz_removed))






    #####   Noisebudget   #####
    plot_names = np.array([])

    fig, s1 = plt.subplots(1, figsize=(12,9))

    # saved traces
    l1s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-', alpha=0.0, color='C4') 
    l2s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-', alpha=0.0, color='C1')
    l3s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-', alpha=0.0, color='C2')
    l4s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-', alpha=0.0, color='C5')
    l5s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-', alpha=0.0, color='C3')
    l6s, = s1.loglog(plot_ff, 1e-20*np.ones_like(plot_ff), ls='-', alpha=0.0, color='C0')

    # main plots to be updated
    # l1, = s1.loglog(plot_ff, plot_asd_corr_noise_sqz_removed,   ls='--', color=l1s.get_color(), zorder=4, label='Correlated noise squeezing removed')
    l2, = s1.loglog(plot_ff, plot_asd_corr_noise_with_sqz,      ls='-',  color=l2s.get_color(), zorder=3, label='Correlated noise with squeezing')
    l3, = s1.loglog(plot_ff, plot_psd_sum,                      ls='-',  color=l3s.get_color(), zorder=3, label=darm_label)
    l4, = s1.loglog(plot_ff, plot_psd_null,                     ls='-',  color=l4s.get_color(), zorder=3, label='Null channel')
    l5, = s1.loglog(plot_ff, plot_asd_corr_noise_no_sqz,        ls='-',  color=l5s.get_color(), zorder=3, label='Correlated noise no squeezing')
    l6, = s1.loglog(plot_ff, plot_psd_sum_no_sqz,               ls='-',  color=l6s.get_color(), zorder=3, label=darm_label_no_sqz)

    # if possible, read and plot LHO noisebudget traces
    if args.uncalibrated:
        xlims = [30, 7000]
        ylims = [4e-9, 1.01e-6]
        units = 'mA'
    else:
        xlims = [30, 7000]
        ylims = [1e-21, 1.01e-18]
        units = 'm'
        try:
            fflog = np.logspace(np.log10(10), np.log10(7000), 1000)
            plot_dict = read_noisebudget(fflog)

            # correlated noise labels to plot in the final comparison plot
            plot_labels = np.array([
                'Measured noise (O3)',
                'Thermal',
                'Residual gas',
                'Beam jitter',
                'Laser intensity',
                'Laser frequency',
                'Photodetector dark', # This one is ignored in the final plot, lho_xcor.pdf
                'Scattered light',
                'Sum of correlated noises',
                # 'Aux length control',
            ])

            for label in plot_labels:
                # ASD = plot_dict[label]['ASD']
                ASD_log = plot_dict[label]['ASD_log']
                style = plot_dict[label]['style']
                s1.loglog(fflog, ASD_log, style, label=label)
        except:
            print()
            print('LHO noisebudget not plotted')
            print()

    # s1.set_title('Correlated noise with DARM loop sliders')
    s1.set_ylabel('DARM ' + r'[$\mathrm{' + f'{units}' + r'}/\sqrt{\mathrm{Hz}}$]')
    s1.set_xlabel('Frequency [Hz]')

    s1.set_xlim(xlims)
    s1.set_ylim(ylims)
    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.3)
    s1.legend(loc='lower right')

    fig_dir = f'{chapter_dir}/figures/{script_name}'
    plot_name = f'lho_correlated_noisebudget_with_sqz_removed_{csd_date.replace(" ", "_")}.pdf'
    full_plot_name = f'{fig_dir}/{plot_name}'
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.tight_layout()
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # Plot phase
    fig, (s1) = plt.subplots(1)

    # manipulate the phase calculation so it wraps not at 180 degrees
    phase_shift = np.pi/2

    plot_angle_with_sqz = phase_domain_shift(plot_corr_noise_with_sqz, phase_shift=phase_shift)
    # plot_angle_sqz_removed = phase_domain_shift(plot_corr_noise_sqz_removed, phase_shift=phase_shift)
    plot_angle_no_sqz = phase_domain_shift(plot_corr_noise_no_sqz, phase_shift=phase_shift)

    s1.semilogx(plot_ff, 180/np.pi * plot_angle_with_sqz, color='C1', label='Correlated noise with squeezing')
    # s1.semilogx(plot_ff, 180/np.pi * plot_angle_sqz_removed, color='C4', label='Correlated noise squeezing removed')
    s1.semilogx(plot_ff, 180/np.pi * plot_angle_no_sqz, color='C3', label='Correlated noise no squeezing')

    s1.set_ylabel(f'CSD Phase [degs]')
    s1.set_xlabel(f'Frequency [Hz]')

    s1.set_xlim(xlims)
    s1.set_ylim([-90, 270])
    s1.set_yticks([-90, 0, 90, 180, 270])

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.7)
    s1.legend()

    plot_name = f'csd_phase_averages_{csd_date.replace(" ", "_")}.pdf'
    full_plot_name = f'{fig_dir}/{plot_name}'
    plot_names = np.append(plot_names, full_plot_name)
    print(f'Writing plot PDF to {full_plot_name}')
    plt.tight_layout()
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # Plot squeeze factor proxy
    fig, (s1) = plt.subplots(1)

    fflog = np.logspace(np.log10(10), np.log10(7000), 1000)
    plot_ff, plot_sum_over_null_ratio_minus_corr_db = nu.linear_log_ASD(fflog, ff, sum_over_null_ratio_minus_corr_db)

    # s1.semilogx(ff, sum_over_null_ratio_db, label='Sum / null channel')
    s1.semilogx(plot_ff, plot_sum_over_null_ratio_minus_corr_db, label='(Sum - Corr) / null channel')

    s1.set_ylabel(f'Estimated squeezing level [dB]')
    s1.set_xlabel(f'Frequency [Hz]')

    s1.set_xlim(xlims)
    s1.set_ylim([-3, 5])

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.7)
    # s1.legend()

    plot_name = f'sum_over_null_db_{csd_date.replace(" ", "_")}.pdf'
    full_plot_name = f'{fig_dir}/{plot_name}'
    plot_names = np.append(plot_names, full_plot_name)
    print(f'Writing plot PDF to {full_plot_name}')
    plt.tight_layout()
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # Plot squeeze factor proxy
    fig, (s1) = plt.subplots(1)

    fflog = np.logspace(np.log10(10), np.log10(7000), 1000)
    # plot_ff, plot_sum_over_null_ratio_minus_corr_db = nu.linear_log_ASD(fflog, ff, sum_over_null_ratio_minus_corr_db)
    plot_ff, plot_sum_over_null_ratio_db = nu.linear_log_ASD(fflog, ff, sum_over_null_ratio_db)
    plot_ff, plot_sum_sqz_over_sum_ratio_db = nu.linear_log_ASD(fflog, ff, sum_sqz_over_sum_ratio_db)

    s1.semilogx(plot_ff, plot_sum_over_null_ratio_minus_corr_db, label='(sum - corr) / null', zorder=3)
    s1.semilogx(plot_ff, plot_sum_over_null_ratio_db, label='sum / null')
    s1.semilogx(plot_ff, plot_sum_sqz_over_sum_ratio_db, label='sum sqz / sum no sqz')
    

    s1.set_ylabel(f'Estimated squeezing level [dB]')
    s1.set_xlabel(f'Frequency [Hz]')

    s1.set_xlim(xlims)
    s1.set_ylim([-3, 5])

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.7)
    s1.legend()

    plot_name = f'squeeze_estimate_comparison_db_{csd_date.replace(" ", "_")}.pdf'
    full_plot_name = f'{fig_dir}/{plot_name}'
    plot_names = np.append(plot_names, full_plot_name)
    print(f'Writing plot PDF to {full_plot_name}')
    plt.tight_layout()
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # print open command
    command = 'open'
    for pf in plot_names:
        command = '{} {}'.format(command, pf)
    print()
    print('Command to open plots generated by this script:')
    print(command)
    print()

    return

def main(args):
    '''main function of this script.  Makes an interactive plot.
    '''
    make_unsqueezed_correlated_noise_plot(args)
    return 
    
if __name__ == "__main__":

    args = parse_args()
    main(args)
    
