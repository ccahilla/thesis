\chapter{Correlated Noise}
\label{chap:correlated_noise}

Advanced LIGO operates in DC-readout configuration, with two DC photodetectors (DCPDs) at the antisymmetric port \cite{Hild_2009, Fricke2012}.
The signal from differential arm motion (DARM), and therefore gravitational waves (GWs), appears in the sum of these two DCPDs.
Noise from the interferometer, known as \textit{correlated noise}, is measured identically in both DCPDs.
Sensing noises, like shot noise and photodetector dark noise, are incoherent between each DCPD.

Quantum shot noise tends to dominate the DARM spectrum over most of the GW detection band \cite{KLMTV, Buonanno2001}.
It is possible to measure the correlated noise by measuring the cross spectral density (CSD) between the two DCPDs \cite{PhysRevA.95.043831, IzumiXcorr2017}.
Correlated noise measurements are useful because they expose noise sources under the quantum shot noise.



\section{Introduction}
\label{sec:correlated_noise_introduction}

There are a number of complications that must be considered for the correlated noise measurement:
\begin{enumerate}
    \item the DARM loop must be measured and removed,
    \item injecting squeezed light correlates the shot noise in the DCPDs,
    \item a large number of averages must be taken to integrate away the shot noise to reach the true correlated noise floor, and
    \item detector glitches which inject large transients into DARM must be avoided.
\end{enumerate}

The correlated noise measurement requires hours of data to integrate away shot noise.
However, large glitches occur on the order of one every fifteen minutes, spoiling the mean-averaged power spectral densities (PSDs) of the DCDPs.
Large glitches can be ``gated'' by removing the glitchy data by hand, but the measurement is still susceptible to small glitches that do not meet our gating criteria.

To mitigate having undetected glitches in the DARM data, it is often useful to use median-averaging for estimating PSDs and CSDs.
Median-averaging has an intrinsic bias as opposed to mean-averaging.  
For PSDs the bias factor is understood to asymptote to $\log(2)$ \cite{PhysRevD.85.122006, HallThesis}.
For CSDs, the result is covered in Chapter~\ref{chap:probability_distributions}.
Another useful technique for removing CSDs containing glitches while still using mean-averaging is ``PSD rejection'', covered in Appendix \ref{chap:psd_rejection}.

In this chapter, we first review the DARM control loop,
review the method for extracting the correlated noise from the DCPD data, 
and show results from the correlated noise floor estimation of the LIGO Hanford interferometer during Observing Run 3.



\section{Method}
\label{sec:correlated_noise_method}

Here we overview the method of extracting the correlated noise spectrum, assuming there is no squeezing injected into the antisymmetric port. 
This section follows the procedure in \cite{IzumiXcorr2017}.



\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/correlated_noise/figures/darm_loop_diagram_for_correlated_noise.pdf}
    \caption[Simplified DARM loop]{
        Simplified DARM control loop diagram, including the two-photodetector sensing scheme.
        $A$ and $B$ are the transfer functions of the two DCPDs, plus the photodetector analog electronics and ADCs, with units of $[\mathrm{cts/W}]$.
        $Y$ is the actuator with units $[\mathrm{m/cts}]$.
        $Z$ is the interferometer response to differential arm motion, in $[\mathrm{W/m}]$.
        A 50:50 beamsplitter splits the light from the interferometer onto the two photodetectors.
        The signals from DCPD A and B, $d_a$ and $d_b$, are summed to form the DARM error signal, $d_+$, and subtracted to form the null channel, $d_-$.
        The shot noises $n_a$ and $n_b$ are uncorrelated sensing noises added to $d_a$ and $d_b$, respectively.
        The correlated noise $n_c$ is here simplified as being entirely displacement noise causing actual motion of the interferometer optics.
    }
    \label{fig:correlated_noise_diagram} 
\end{figure}



\subsection{Correlated noise without squeezing}
\label{subsec:correlated_noise_method_no_squeezing}

Figure~\ref{fig:correlated_noise_diagram} shows a simplified DARM loop.
We would like to measure the correlated noise coming from the interferometer, $\inner{n_c}{n_c}$.
However, shot noises $\inner{n_a}{n_a}$ and $\inner{n_b}{n_b}$ drown out the correlated noise for most of the bandwidth.
Additionally, the DARM control loop that keeps the interferometer on resonance correlates sensing noise by injecting it into the loop as mirror motion.
This is how shot noise on DCPD A can appear on DCPD B, only in the bandwidth of the DARM loop ($<~50~\mathrm{Hz}$).
By cross-correlating the DCPDs $\inner{d_a}{d_b}$ and removing the DARM loop, the correlated noise can be extracted.

The open loop gain $G$ of the DARM loop is just the product of all the loop transfer functions,
while the DARM sensing function $C$ is usually calculated from photon calibrator displacement to DARM error signal in units $[\mathrm{cts/m}]$:
\begin{align}
    \label{eq:darm_open_loop_gain}
    G &= \dfrac{1}{2} Y Z (A + B) \\
    C &= \dfrac{1}{2} Z (A + B)
\end{align}

Solving the loop in Figure~\ref{fig:correlated_noise_diagram} for the DARM error signal, aka the sum channel $d_+$,
and the null channel $d_-$:
\begin{align}
    \label{eq:darm_sum_channel}
    d_+ &= \dfrac{1}{1 - G} \left( C \, n_c + n_a + n_b \right) \\
    \label{eq:darm_null_channel}
    d_- &= n_a - n_b
\end{align}

Taking the power spectral density of the sum and null channels yields
\begin{align}
    \label{eq:darm_sum_psd}
    \inner{d_+}{d_+} &= \dfrac{1}{|1 - G|^2} \left( |C|^2 \, \inner{n_c}{n_c} + \inner{n_a}{n_a} + \inner{n_b}{n_b} \right) \\
    \label{eq:darm_null_psd}
    \inner{d_-}{d_-} &= \inner{n_a}{n_a} + \inner{n_b}{n_b}
\end{align}
assuming that $n_c$, $n_a$, and $n_b$ are all independent, so e.g. $\inner{n_a}{n_b} = 0$.
If we calibrate the DARM error PSD into meters by multiplying $\inner{d_+}{d_+}$ by $|1 - G|^2/|C|^2$,
the shot noise terms $\inner{n_a}{n_a}$ and $\inner{n_b}{n_b}$ still appear.

Solving the diagram in Figure~\ref{fig:correlated_noise_diagram} for split DARM error signals $d_a$ and $d_b$ yields
\begin{align}
    \label{eq:dcpd_a_signal}
    d_a = \dfrac{1}{2 (1 - G)} \left( (2 - G) n_a + G n_b + C n_c \right) \\
    \label{eq:dcpd_b_signal}
    d_b = \dfrac{1}{2 (1 - G)} \left( G n_a + (2 - G) n_b + C n_c \right)
\end{align}

If we look at the power spectral densities of each individual $\inner{d_a}{d_a}$ and $\inner{d_b}{d_b}$ 
and the cross spectral density $\inner{d_a}{d_b}$, we get
\begin{align}
    \label{eq:dcpd_a_psd}
    \inner{d_a}{d_a} &= \dfrac{1}{4 |1 - G|^2} \left( |2 - G|^2 \inner{n_a}{n_a} + |G|^2 \inner{n_b}{n_b} + |C|^2 \inner{n_c}{n_c} \right) \\
    \label{eq:dcpd_b_psd}
    \inner{d_b}{d_b} &= \dfrac{1}{4 |1 - G|^2} \left( |G|^2 \inner{n_a}{n_a} + |2 - G|^2 \inner{n_b}{n_b} + |C|^2 \inner{n_c}{n_c} \right) \\
    \label{eq:dcpd_ab_csd}
    \inner{d_a}{d_b} &= \dfrac{1}{4 |1 - G|^2} \left( G (2 - G^*) \inner{n_a}{n_a} + G^* (2 - G) \inner{n_b}{n_b} + |C|^2 \inner{n_c}{n_c} \right).
\end{align}

Using Eqs.~\ref{eq:dcpd_a_psd}, \ref{eq:dcpd_b_psd}, and \ref{eq:dcpd_ab_csd}, we can solve for the correlated noise $\inner{n_c}{n_c}$.
Recalling that $\inner{d_b}{d_a} = \inner{d_a}{d_b}^*$, the correlated noise is
\begin{align}
    \label{eq:correlated_noise_psd}
    \nonumber |C|^2 \inner{n_c}{n_c} = \Bigg(   & |2 - G|^2 \inner{d_a}{d_b} 
                                                + |G|^2 \inner{d_b}{d_a} \\
                                                & - G (2 - G^*) \inner{d_a}{d_a} 
                                                - G^* (2 - G) \inner{d_b}{d_b} 
                                                \Bigg)
\end{align}
By measuring the individual DCPD signals $d_a$ and $d_b$ and applying the DARM loop gain $G$ and sensing function $C$,
the correlated noise from the interferometer can be directly estimated.

Figure~\ref{fig:correlated_noise_budget} shows the O3 correlated noise budget for $\inner{n_c}{n_c}$.



\subsection{DC readout with squeezing}
\label{subsec:dc_readout_with_squeezing}



\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/correlated_noise/figures/dc_readout_with_squeezing_diagram.pdf}
    \caption[DC readout shot noise with split photodetection]{
        Diagram of the DC readout detection scheme with split photodetection \cite{Hall2018_bhd_null_channel}.
        The beamsplitter is 50:50 with the reflection convention designated by the plus and minus signs.
        The DCPD sum $n_+ = n_a + n_b$ contains the information from the interferometer, 
        including squeezed vacuum and gravitational wave signal.
        The DCPD null $n_- = n_a - n_b$ contains the information from the quantum vacuum.
    }
    \label{fig:correlated_noise_with_squeezing_shot_noise_diagram} 
\end{figure}



Now suppose that squeezed light is injected into the antisymmetric port of the interferometer.
This correlates the noise on each DCPD, i.e. $\inner{n_a}{n_b} \neq 0$ \cite{McCullerCrossCorr2018}.
Here we review the DC readout detection scheme, 
how squeezing correlates the shot noise appearing on each photodetector,
and calculate the squeezed shot noise cross spectral density $\inner{n_a}{n_b}$ for a DC readout interferometer.

To calculate $\inner{n_a}{n_b}$ we briefly review shot noise in a DC readout interferometer with split photodetection, as shown in Figure~\ref{fig:correlated_noise_with_squeezing_shot_noise_diagram}.
This will follow the derivation and notation in \cite{Hall2018_bhd_null_channel}.

From Figure~\ref{fig:correlated_noise_with_squeezing_shot_noise_diagram},
$\vec{L}$ is the local oscillator, 
$\vec{\ell}$ is the quantum vacuum, 
$\vec{S}$ is the DARM offset light, and
$\vec{s}$ is the gravitational wave signal plus the output squeezed vacuum.

The capital letters refer to the carrier, 
while the lowercase letters refer to the audio sidebands that beat with the carrier.
DC readout operates with no local oscillator $\vec{L} = 0$, 
% signal-recycling tuning angle of $\phi = \pi/2$, 
and a homodyne angle $\zeta = \pi/2$,
which puts the gravitational wave signal entirely in the amplitude quadrature upon exit from the interferometer.
$\vec{S}$ is set to some non-zero value to beat against the squeezed vacuum $\vec{s}$.
For the shot noise derivation, we assume that the GW signal and interferometer correlated noise is zero.

The light incident on each DCPD $\vec{A} + \vec{a}$ and $\vec{B} + \vec{b}$ is
\begin{align}
    \label{eq:dcpd_a_incident_light}
    \vec{A} + \vec{a} &= \dfrac{1}{\sqrt{2}} \left( \vec{S} + \vec{s} + \vec{L} + \vec{\ell} \right) \\
    \label{eq:dcpd_b_incident_light}
    \vec{B} + \vec{b} &= \dfrac{1}{\sqrt{2}} \left( \vec{S} + \vec{s} - \vec{L} - \vec{\ell} \right)
\end{align}

First, recall that the local oscillator $\vec{L} = 0$.
Second, the homodyne angle definition from \cite{Buonanno2001} for the signal $\vec{s}$ is:
\begin{align}
    \label{eq:bnc_homodyne_angle_definition}
    s_\zeta = s_1 \sin(\zeta) + s_2 \cos(\zeta).
\end{align}
The Advanced LIGO interferometers are dual-recycled resonant-sideband extraction in DC readout configuration, 
meaning the signal-recycling cavity is tuned to $\phi = \pi/2$.
This puts the GW signal, upon exit from the interferometer, in the \textit{amplitude} quadrature $s_1$.
It also places the static DARM offset light $\vec{S}$, in the amplitude quadrature.

For now, we ignore contrast defect and set the homodyne angle $\zeta = \pi/2$ using Eq.~\ref{eq:bnc_homodyne_angle_definition}, so
\begin{align}
    \label{eq:darm_offset_quadrature}
    \vec{S} = \begin{pmatrix}
        S \\
        0
    \end{pmatrix}
\end{align}
picks out the output amplitude quadratures $s_1 = s$ and $\ell_1 = \ell$.
The light is converted into current on the DCPDs, represented by $N_a + n_a$ and $N_b + n_b$.
\begin{align}
    \label{eq:dcpd_a_incident_power}
    N_a + n_a &= |\vec{A} + \vec{a}|^2 = \dfrac{1}{2} S^2 + S s + S \ell \\
    \label{eq:dcpd_b_incident_power}
    N_b + n_b &= |\vec{B} + \vec{b}|^2 = \dfrac{1}{2} S^2 + S s - S \ell
\end{align}
where terms proportional to $s^2$, $s \ell$, and $\ell^2$ are small enough to be negligible.
$N_a = N_b = S^2/2$ represents the DARM offset light being split in half by the beamsplitter, nominally $N_a = N_b \approx 20~\mathrm{mW}$ in Advanced LIGO.

Removing DC components represented by the capital letters from Eqs.~\ref{eq:dcpd_a_incident_power} and \ref{eq:dcpd_b_incident_power},
we can calculate the shot noise sum $n_+$ and null $n_-$:
\begin{align}
    \label{eq:shot_noise_sum}
    n_+ &= 2 S s \\
    \label{eq:shot_noise_null}
    n_- &= 2 S \ell.
\end{align}
This illustrates how, with the DC readout scheme, 
the sum signal picks out the squeezed vacuum signal from the interferometer $s$ and the null signal picks out the unsqueezed vacuum $\ell$.

The squeeze parameter $r$ is used to quantify how quantum measurement uncertainty increases and decreases between quadratures \cite{GerryKnight2004}.
The output squeezed vacuum in $s$ is phased such that the maximum squeezing $e^{-r}$ occurs in the amplitude quadrature at the output.
The quantum vacuum in $\ell$ has no squeezing.
Calculating the power spectral densities of the sum and null signals:
\begin{align}
    \label{eq:shot_noise_sum_psd}
    \inner{n_+}{n_+} &= 4 S^2 \inner{s}{s} = 4 S^2 e^{-2 r} \\
    \label{eq:shot_noise_null_psd}
    \inner{n_-}{n_-} &= 4 S^2 \inner{\ell}{\ell} = 4 S^2.
\end{align}
Here we have assumed that the unsqueezed quantum vacuum shot noise $\inner{\ell}{\ell} = 1$, 
which follows from our definition of quadratures in Section~\ref{sec:quadratures}.
The sum shot noise PSD is reduced by the squeeze factor $e^{-2 r}$, 
which is often expressed in dB by setting $e^{-2 r} = 10^{ -\frac{\mathrm{dB_{sqz}}}{10} }$.

The shot noise cross spectral density $\inner{n_a}{n_b}$ is the important quantity that arises when calculating the correlated noise between DCPDs.
From Eqs.~\ref{eq:dcpd_a_incident_power} and \ref{eq:dcpd_b_incident_power}, the cross spectral density is found:
\begin{align}
    \label{eq:shot_noise_csd}
    \nonumber \inner{n_a}{n_b} &= S^2 \inner{s + \ell}{s - \ell} \\
    \nonumber &= S^2 \left( \inner{s}{s} - \inner{\ell}{\ell} + \inner{s}{\ell} - \inner{\ell}{s} \right) \\
    \inner{n_a}{n_b} &= S^2 \left( e^{-2 r} - 1 \right)
\end{align}
where we have no correlation between our squeezed and unsqueezed vacuum, so $\inner{s}{\ell} = \inner{\ell}{s} = 0$,
and we have used the definitions from Eqs.~\ref{eq:shot_noise_sum_psd} and \ref{eq:shot_noise_null_psd} and written $\inner{s}{s} = e^{-2 r}$ and $\inner{\ell}{\ell} = 1$.

A key observation here is that, for true squeezing where $r > 0$, $\inner{n_a}{n_b}$ is \textit{real} and \textit{negative}.
This implies that 
\begin{enumerate}
    \item when detecting squeezed light, the power measured on each DCPD is \textit{anti-correlated},
    \item because $\inner{n_a}{n_b}$ is real, $\inner{n_b}{n_a} = \inner{n_a}{n_b}$,
    \item correlated noise due to squeezing will have an opposite sign to correlated noise coming from the interferometer $\inner{n_c}{n_c}$, which must be positive.
\end{enumerate}
Figure~\ref{fig:correlated_noise_phase} plots the measured correlated noise with squeezing, 
illustrating how in the shot noise dominated frequency band the phase is $180^\circ$.



\subsection{Correlated noise with squeezing}
\label{subsec:correlated_noise_method_with_squeezing}


Recalculating the DCPD spectral densities including $\inner{n_a}{n_b}$ terms yields
\begin{align}
    \label{eq:dcpd_a_psd_sqz}
    \nonumber \inner{d_a}{d_a} = \dfrac{1}{4 |1 - G|^2} \Bigg(  & |2 - G|^2 \inner{n_a}{n_a} + |G|^2 \inner{n_b}{n_b} + |C|^2 \inner{n_c}{n_c} \\
                                                                & + 2 (G + G^* - |G|^2) \inner{n_a}{n_b} \Bigg) \\
    \label{eq:dcpd_b_psd_sqz}
    \nonumber \inner{d_b}{d_b} = \dfrac{1}{4 |1 - G|^2} \Bigg(  & |G|^2 \inner{n_a}{n_a} + |2 - G|^2 \inner{n_b}{n_b} + |C|^2 \inner{n_c}{n_c} \\ 
                                                                & + 2 (G + G^* - |G|^2) \inner{n_a}{n_b} \Bigg) \\
    \label{eq:dcpd_ab_csd_sqz}
    \nonumber \inner{d_a}{d_b} = \dfrac{1}{4 |1 - G|^2} \Bigg(  & G (2 - G^*) \inner{n_a}{n_a} + G^* (2 - G) \inner{n_b}{n_b} + |C|^2 \inner{n_c}{n_c} \\ 
                                                                & + 2 (2 - G - G^* + 2 |G|^2) \inner{n_a}{n_b} \Bigg).
\end{align}

If we compute the correlated noise using the RHS of Eq.~\ref{eq:correlated_noise_psd} with Eqs.~\ref{eq:dcpd_a_psd_sqz}, \ref{eq:dcpd_b_psd_sqz}, and \ref{eq:dcpd_ab_csd_sqz},
we get
\begin{align}
    \label{eq:correlated_noise_psd_sqz}
    \nonumber |C|^2 \inner{n_c}{n_c} + 4 \inner{n_a}{n_b} = \Bigg(  & |2 - G|^2 \inner{d_a}{d_b} 
                                                + |G|^2 \inner{d_b}{d_a} \\
                                                & - G (2 - G^*) \inner{d_a}{d_a} 
                                                - G^* (2 - G) \inner{d_b}{d_b} 
                                                \Bigg)
\end{align}
Figure~\ref{fig:correlated_noise_budget_with_squeezing} plots this expression as the correlated noise with squeezing trace.



\subsection{Squeezing level estimate from correlated noise}
\label{subsec:squeezing_from_correlated_noise}


If we have already estimated the correlated noise $\inner{n_c}{n_c}$ during a time without squeezing, 
then it's possible to estimate the squeezing level.
This can be especially useful if the squeezing is frequency dependent, as it was at LIGO Hanford during O3.

We assume here that the correlated noise is the same for both squeezing and non-squeezing times.
This is not true where quantum radiation pressure noise (QRPN) is significant, 
as anti-squeezing will enhance the QRPN contribution to correlated noise \cite{Yu2020}.

First, we write the new expressions for the DCPD sum and null PSDs including squeezing:
\begin{align}
    \label{eq:darm_sum_psd_sqz}
    \nonumber \inner{d_+}{d_+} &= \dfrac{1}{|1 - G|^2} \left( \inner{n_a}{n_a} + \inner{n_b}{n_b} + 2 \inner{n_a}{n_b} + |C|^2 \inner{n_c}{n_c} \right) \\
    \nonumber \inner{d_+}{d_+} &= \dfrac{1}{|1 - G|^2} \left( \inner{n_+}{n_+} + |C|^2 \inner{n_c}{n_c} \right) \\
    \inner{d_+}{d_+} &= \dfrac{1}{|1 - G|^2} \left( 4 S^2 e^{-2 r} + |C|^2 \inner{n_c}{n_c} \right) \\
    \label{eq:darm_null_psd_sqz}
    \nonumber \inner{d_-}{d_-} &= \inner{n_a}{n_a} + \inner{n_b}{n_b} - 2 \inner{n_a}{n_b} \\
    \nonumber \inner{d_-}{d_-} &= \inner{n_-}{n_-} \\
    \inner{d_-}{d_-} &= 4 S^2
\end{align}
where we used Eqs.~\ref{eq:shot_noise_sum_psd} and \ref{eq:shot_noise_null_psd} to simplify to the final expressions.

Then, solving for the squeeze ratio $e^{-2 r}$ using Eqs.~\ref{eq:darm_sum_psd_sqz}, \ref{eq:darm_null_psd_sqz}, 
and the correlated noise $\inner{n_c}{n_c}$ calculated via Eq.~\ref{eq:correlated_noise_psd}:
\begin{align}
    \label{eq:sqz_level_estimate}
    e^{-2 r} = \dfrac{ |1 - G|^2 \inner{d_+}{d_+} - |C|^2 \inner{n_c}{n_c} }{ \inner{d_-}{d_-} }
\end{align}
Figure~\ref{fig:squeezing_level_estimates} plots the squeezing levels estimated via Eq.~\ref{eq:sqz_level_estimate}.

% As a verification, we can use the result of remove squeezing from the correlated noise trace from Eq.~\ref{eq:correlated_noise_psd_sqz}



\section{Results}
\label{sec:correlated_noise_results}

All spectral densities in this section were taken using median-averaging to avoid the frequent glitches, 
with phase compensated and mean-to-median biasing corrected according to Chapter~\ref{chap:probability_distributions}.
They were also verified using the ``PSD rejection'' technique for removing glitches described in Appendix~\ref{chap:psd_rejection}.

The unsqueezed correlated spectrum in Figure~\ref{fig:correlated_noise_budget} gives a broader picture of the ``mystery noise'' limiting LIGO sensitivity at 30~Hz and below.
Above $3~\mathrm{kHz}$, the correlated noise is consistent with the laser intensity noise coupling to DARM.
Around $300~\mathrm{Hz}$ the correlated noise approaches the coatings thermal noise limit.
Below $300~\mathrm{Hz}$, conventional ``mystery noise'' which limits DARM also limits the correlated noise.
Around $1~\mathrm{kHz}$, there is a large gap between the measured correlated noise and the expected sum that is also not understood.

\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/correlated_noise/figures/lho_xcor_noisebudget/lho_correlated_noisebudget_August_2020.pdf}
    \caption[Correlated noise budget]{
        LIGO Hanford correlated noise budget during nine hours without squeezing in August 2020.
        The correlated noise from the interferometer, seen here in orange, is calculated via Eq.~\ref{eq:correlated_noise_psd}.
        This can be directly compared to the sum of correlated noise budget traces, seen in black.
    }
    \label{fig:correlated_noise_budget} 
\end{figure}



The remaining plots compare correlated noise results from a single fifteen hour lock stretch, 
where squeezing was injected for six hours, then the squeezer was turned off for nine hours.
Figures~\ref{fig:correlated_noise_budget_with_squeezing} and \ref{fig:correlated_noise_phase} plot the amplitude and phase of the correlated noise 
as calculated for both the no squeezing time (Eq.~\ref{eq:correlated_noise_psd}) and the squeezing time (Eq.~\ref{eq:correlated_noise_psd_sqz}).
Also plotted in Figure~\ref{fig:correlated_noise_budget_with_squeezing} is the unsqueezed DCPD sum PSD (Eq.~\ref{eq:darm_sum_psd}),
the squeezed DCPD sum PSD (Eq.~\ref{eq:darm_sum_psd_sqz}), and 
the DCPD null (Eq.~\ref{eq:darm_null_psd_sqz}).

Eq.~\ref{eq:correlated_noise_psd_sqz} shows how classical and quantum correlated noise both show up in the final expression.
Recall from Eq.~\ref{eq:shot_noise_csd} that the quantum correlated noise is negative.
This causes the classical and quantum correlated noise to cancel each other out, 
leading to classical and quantum correlated noise dominated regimes.
In Figure~\ref{fig:correlated_noise_budget_with_squeezing}, 
the crossing of the squeezed sum PSD in green and the null PSD in brown,
corresponds to the dips in the correlated noise, 
signifying the change from classical- to quantum-dominated correlated noise.



\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/correlated_noise/figures/squeeze_to_unsqueezed_correlated_noise/lho_correlated_noisebudget_with_sqz_removed_16_September_2020_sqz.pdf}
    \caption[Correlated noise budget with squeezing]{
        LIGO Hanford correlated noise during six hours with squeezing and nine hours without squeezing on September 16, 2020.
        Squeezed light correlates the shot noise detected on each DCPD, as calculated in Eq.~\ref{eq:correlated_noise_psd_sqz}.
        The dip at 150~Hz and 5~kHz comes from the interaction of squeezed shot noise and classical noise canceling each other out.
        Figure~\ref{fig:correlated_noise_phase} plots the phase of the correlated noise traces shown here.
        This plot mirrors a similar study done at LIGO Livingston \cite{alog44882}.
    }
    \label{fig:correlated_noise_budget_with_squeezing} 
\end{figure}



In Figure~\ref{fig:correlated_noise_phase}, the measured phase of the squeezed correlated noise is shown to be $180^\circ$ in the quantum-dominated regime.
The unsqueezed spectrum does not exhibit this phase change.



\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/correlated_noise/figures/squeeze_to_unsqueezed_correlated_noise/csd_phase_averages_16_September_2020_sqz.pdf}
    \caption[Squeezing levels estimated from the correlated noise]{
        Phase of the correlated noise with and without squeezing.
        The sign flip at at 150~Hz corresponds to the transition from classical correlated noise $\inner{n_c}{n_c}$ dominating the spectrum to squeezed shot noise $\inner{n_a}{n_b}$ dominating.
        Figure~\ref{fig:correlated_noise_budget_with_squeezing} plots the amplitude of the correlated noise traces shown here.
    }
    \label{fig:correlated_noise_phase} 
\end{figure}



Because we have the squeezed DCPD sum, null, and the classical correlated noise estimates all from the same lock stretch, 
we can better estimate the squeezing levels using Eq.~\ref{eq:sqz_level_estimate}.
Expressing the squeeze ratio in terms of dB such that $e^{-2 r} = 10^{-\frac{\mathrm{dB_{sqz}}}{10}}$
yields the estimate shown in Figure~\ref{fig:squeezing_level_estimates}.
The squeezing exhibited by the LIGO Hanford detector is frequency-dependent,
with the largest squeezing of $\sim2.5$~dB in the 100 to 300~Hz region, up to 1~dB above 1~kHz.
Hanford's O3 unintentional frequency-dependent squeezing is currently under further investigation.



\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/correlated_noise/figures/squeeze_to_unsqueezed_correlated_noise/sum_over_null_db_16_September_2020_sqz.pdf}
    \caption[Squeezing levels estimated from the correlated noise]{
        Squeezing levels estimated by removing correlated noise from the squeezed DCPD sum, according to Eq.~\ref{eq:sqz_level_estimate}.
        This estimate is good in the region where shot noise and correlated noise are about equivalent, or everywhere below $\sim70$~Hz.
    }
    \label{fig:squeezing_level_estimates} 
\end{figure}



% \begin{figure}[hbt!]
%     \centering
%     \includegraphics[width=\textwidth]{./chapters/correlated_noise/figures/lho_xcor_animation/coherence_and_bias_averages_67156_gps_start_1282730359.pdf}
%     \caption[Median-averaged CSD bias correction factor]{
%         Mean- and median-averaged coherence, 
%         and the associated mean-to-median bias factor for correcting the median DCPD cross spectral density $\inner{d_a}{d_b}$.
%         The bias factor divided the measured median-averaged correlated noise trace in Figure~\ref{fig:correlated_noise_budget} to make it equivalent to a mean-averaged version,
%         as calculated in Eq.~\ref{eq:csd_bias_in_coherence_part1}.
%     }
%     \label{fig:dcpd_all_csds} 
% \end{figure}

% \begin{figure}[hbt!]
%     \centering
%     \includegraphics[width=\textwidth]{./chapters/correlated_noise/figures/lho_xcor_stats/dcpd_csd_2d_histograms_for_frequency_bin_200_Hz.pdf}
%     \caption{
%         LIGO Hanford DCPD correlated noise cross spectral density 2D histogram of the 200~Hz frequency bin .
%         This plot illustrates how phase information helps ``integrates away'' uncorrelated noise, 
%         revealing correlated noise buried underneath.
%         Asymmetric Laplace distributions are fit to both the real and imaginary parts of the cross spectral density.
%         Glitches cause large transients in the data, and are off the scale of this plot.  
%         The glitches are so large that even after 20000 averages, they bias the mean away from its true value. 
%         The mean calculated from the asymmetric Laplace distribution is $1.37 \times 10^{-40} \: \mathrm{m^2/Hz}$.
%         The median is robust to glitches, and so is more trustworthy as an estimator of the true power in the measurement.
%     }
%     \label{fig:dcpd_csd_histogram_200_Hz} 
% \end{figure}

% LLO Cross correlation with squeezing: https://alog.ligo-la.caltech.edu/aLOG/index.php?callRep=44882

\section{Future Work}
\label{sec:correlated_noise_future_work}

The correlated noise budget is useful for verifying the DARM noise budget traces, and determining where the classical noise is under the quantum shot noise.
The broad range of unexplained noise in the correlated noise, from familiar ``mystery noise'' at low frequency, 
to the ``correlated mystery noise'' between 1 and 3~kHz which is only a factor of 3 below squeezed DARM,
means there is much important work remaining to be done understanding what lies below the shot noise.
If they can be improved, correlated noise spectra could verify the Advanced LIGO thermal noise floor estimated from the coating Brownian noise for the titania-doped silica tantala optic coatings \cite{Gras2018}.

The correlated noise could potentially be used to improve sensitivity to continuous gravitational wave signals, 
such as the stochastic background or continuous waves from spinning neutron stars.
The injection of squeezing can confuse such an analysis if the quantum or classical correlated noise is not stable.

Future detectors, including A+, are expected to use balanced homodyne detection, rather than DC readout detection \cite{Fritschel2014}.
Balanced homodyne uses a local oscillator to beat with the GW signal rather than light from the interferometer via a DC offset in the DARM loop.
The usual two-photodetector detection scheme would not allow for the correlated noise spectrum to be measured, 
since the amplitude noise on the local oscillator would dominate that spectrum \cite{Hall2018_bhd_null_channel}.
To recover the correlated noise spectrum a four-photodetector scheme would need to be employed.
