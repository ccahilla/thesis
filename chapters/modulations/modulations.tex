\chapter{Modulation, quadratures, and electric field units}
\label{chap:modulation}


Here we will derive the expected gravitational wave signal from a simple Michelson interferometer,
alongside the expected laser frequency and intensity noise couplings, as has been done before \cite{Saulson1994, Sigg1997, Camp2000, Somiya2006, Izumi2017}.

In other chapters we explored the frequency and intensity stabilization schemes, 
as well are the full detector response to GWs for accurate calibration.
The purpose of this chapter is to introduce the technical origins of concepts important to this thesis,
including the sideband representation,
amplitude and phase quadratures,
homodyne angle,
optical gain,
contrast defect,
and DC offset.

This appendix helps draw the connection between the theoretical representation and the instrumental results.
Often, random scale factors are applied to help data match up with theory.
If losses are a parameter we care about, which they are in Advanced LIGO for O3, 
random scale factors are degenerate with those parameters.

The overview facilitates a direct comparison of analytic results like those in \cite{Buonanno2001, Somiya2006},
to numerical results like those calculated from Finesse \cite{finesse, Brown2020},
to actual measurements taken at LIGO Hanford.


\section{Modulation}
\label{sec:modulation}

First we review the physics of modulation in the sideband picture \cite{RegehrThesis}.
The modulation picture emphasizes the different frequencies of light that are created by different interactions.
GWs are detected as infinitesimal modulations applied to highly stabilized laser light.
Noise in the laser light, whether quantum or classical, is modulations not caused by GWs.


A perfectly noiseless electric field $E$ is known as the \textit{carrier}:
\begin{align}
    \label{eq:noiseless_carrier}
    E = E_0 e^{i \omega_0 t}
\end{align}
where $\omega_0$ is the carrier frequency, 
$E_0$ is the carrier amplitude,
and $t$ is time.



\subsection{Phase modulation}
\label{subsec:phase_modulation}

A \textit{phase modulation} of amplitude $\delta \phi$ at frequency $\omega$ can be applied to the carrier light:
\begin{align}
    \label{eq:phase_modulation}
    \nonumber E_{\delta \phi} &= E_0 e^{i \phi} \\
    E_{\delta \phi} &= E_0 e^{i (\omega_0 t + \delta \phi \cos(\omega t)) }
\end{align}

This can be thought of as splitting the carrier power, which is always a sine wave at $\omega_0$,
off into \textit{sidebands} at frequencies $\omega_0 \pm \omega$.
Using the Jacobi-Anger expansion on Eq.~\ref{eq:phase_modulation} yields:
\begin{align}
    \label{eq:phase_modulation_jacobi_anger}
    E_{\delta \phi} &= E_0 e^{i \omega_0 t} \displaystyle \sum_{n=-\infty}^{\infty} i^n J_n(\delta \phi) \exp(i n \omega t)
\end{align}
where $J_n$ is the $n^{th}$ Bessel function of the first kind.

If we assume $\delta \phi$ is small, then we can ignore the higher-order sidebands $n \geq 2$,
and write Eq.~\ref{eq:phase_modulation_jacobi_anger} as
\begin{align}
    \label{eq:phase_modulation_jacobi_anger_low_order}
    E_{\delta \phi} &= E_0 e^{i \omega_0 t} \left[ J_0(\delta \phi) + i J_1(\delta \phi) e^{i \omega t} - i J_{-1}(\delta \phi) e^{-i \omega t } \right] 
\end{align}

Finally, using $J_{-1}(\delta \phi) = -J_1(\delta \phi)$, 
$J_0(\delta \phi) \approx 1$, 
and $J_1(\delta \phi) \approx \delta \phi / 2$,
we write the final phase modulation in terms of the carrier $\omega_0$, upper sideband $\omega_0 + \omega$ and lower sideband $\omega_0 - \omega$:
\begin{align}
    \label{eq:phase_modulation_final}
    E_{\delta \phi}   &= E_0 e^{i \omega_0 t} \left( 1 + i \dfrac{\delta \phi}{2} E_0 e^{i \omega t} + i \dfrac{\delta \phi}{2} E_0 e^{-i \omega t} \right)
\end{align}

The key observation of Eq.~\ref{eq:phase_modulation_final} is the relative phase of the sidebands compared with the carrier.
The sidebands are aligned with one another when they are orthogonal to the carrier.
Calculating the power in the field, 
\begin{align}
    \label{eq:phase_modulation_power}
    \nonumber P_{\delta \phi} &= |E_{\delta \phi}|^2 \\
    \nonumber &= |E_0|^2 \left(1 + \dfrac{(\delta \phi)^2}{4} \left( e^{i 2 \omega t} + e^{-i 2 \omega t} \right) \right) \\
    P_{\delta \phi} &\approx |E_0|^2.
\end{align}
The sidebands push and pull the phase of the carrier by $\delta \phi$, 
but to first order do not alter the amplitude.
Figure~\ref{fig:phase_modulation} illustrates the sideband and quadrature picture of phase modulation.



\subsection{Frequency noise}
\label{subsec:frequency_noise}

\textit{Frequency noise} is mathematically equivalent to a phase modulation.
Using the definition of frequency as the time derivative of phase, $d\phi/dt$,
and the phase $\phi$ from Eq.~\ref{eq:phase_modulation},
we calculate the relationship between frequency noise and phase modulation \cite{HeinzelThesis}:
\begin{align}
    \label{eq:phase_derivative}
    \nonumber \dfrac{d\phi}{dt} &= \dfrac{d}{dt}(\omega_0 t + \delta \phi \cos(\omega t)) \\
    \dfrac{d\phi}{dt} &= \omega_0 - \omega \delta \phi \sin(\omega t)
\end{align}
The frequency can be broken down into the carrier term $\omega_0$ and the noise term $\delta \nu$, where
\begin{align}
    \label{eq:frequency_noise_to_phase}
    \dfrac{2 \pi \delta \nu}{\omega} = \delta \phi
\end{align}
where $\delta \nu$ is the amplitude of the frequency swing.

We can substitute Eq.~\ref{eq:frequency_noise_to_phase} into Eq.~\ref{eq:phase_modulation_final} with no change in the final result 
(except an arbitrary phase advance of $\pi/2$ for both sidebands):
\begin{align}
    \label{eq:frequency_modulation}
    E_{\delta \nu} &= E_0 e^{i \omega_0 t} \left( 1 - \dfrac{\pi \delta \nu}{\omega} e^{i \omega t} + \dfrac{\pi \delta \nu}{\omega} e^{-i \omega t} \right)
\end{align}

Here we recall the distinction between $\omega_0$, $\omega$, and $\delta \omega$.
The carrier frequency is $\omega_0$, this is a constant, $\omega_0 = 2 \pi c / \lambda = 1.77 \times 10^{15}~\mathrm{rad/s}$ 
(The Advanced LIGO laser wavelength $\lambda = 1064~\mathrm{nm}$).
The modulation frequency itself is $\omega$, this is how fast the carrier frequency changes.
The frequency modulation amplitude $\delta \nu$ is how much the carrier frequency changes. 



\subsection{Amplitude modulation}
\label{subsec:amplitude_modulation}

An \textit{amplitude modulation} of amplitude $\delta E$ at frequency $\omega$ can be applied to the carrier light:
\begin{align}
    \label{eq:amplitude_modulation}
    \nonumber E_{\delta E} &= E_0 e^{i \omega_0 t} \left( 1 + \dfrac{\delta E}{E_0} \cos(\omega t) \right) \\
    E_{\delta E} &= E_0 e^{i \omega_0 t} \left( 1 + \dfrac{\delta E}{2 E_0} e^{i \omega t} + \dfrac{\delta E}{2 E_0} e^{-i \omega t} \right)
\end{align}

Again, the key observation of Eq.~\ref{eq:amplitude_modulation} is the relative phase of the sidebands compared with the carrier.
This time, the sidebands are aligned with one another when they are also aligned with the carrier.
Calculating the power in the field, 
\begin{align}
    \label{eq:amplitude_modulation_power}
    \nonumber P_{\delta E} &= |E_{\delta E}|^2 \\
    \nonumber &= |E_0|^2 \left(1 + \dfrac{2 \delta E}{E_0} \cos(\omega t) + \dfrac{(\delta E)^2}{E_0^2} \cos(\omega t)^2 \right) \\
    P_{\delta E} &\approx P_0 \left(1 + \dfrac{2 \delta E}{E_0} \cos(\omega t) \right).
\end{align}
The sidebands push and pull the amplitude of the carrier by $\delta E$, 
but do not alter the phase.
Figure~\ref{fig:amplitude_modulation} illustrates the sideband and quadrature picture of amplitude modulation.



\subsection{Intensity noise}
\label{subsec:intensity_and_amplitude_noise}

\textit{Relative intensity noise} (RIN) is mathematically equivalent to amplitude modulation.
From Eq.~\ref{eq:amplitude_modulation_power}, 
we can relate relative intensity noise to relative amplitude noise (RAN).
Dividing Eq.~\ref{eq:amplitude_modulation_power} by $P_0$, we define the relative intensity noise in terms of amplitude modulation:
\begin{align}
    \label{eq:rin_to_ran}
    \dfrac{\delta P}{P_0} &= \dfrac{2 \delta E}{E_0}
\end{align}

Going back to the expression for amplitude modulation $E_{\delta E}$ Eq.~\ref{eq:amplitude_modulation},
we can express the electric field $E_{\delta P}$ and power $P_{\delta E}$ in terms of relative intensity noise $\delta P / P$:
\begin{align}
    \label{eq:intensity_modulation}
    E_{\delta P} &= E_0 e^{i \omega_0 t} \left( 1 + \dfrac{\delta P}{4 P_0} e^{i \omega t} + \dfrac{\delta P}{4 P_0} e^{-i \omega t} \right)\\
    \label{eq:intensity_modulation_power}
    P_{\delta E} &= P_0 \left(1 + \dfrac{\delta P}{P_0} \cos(\omega t) \right)
\end{align}



\begin{figure}[hbt!]
    \centering
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./chapters/modulations/figures/amplitude_modulation.pdf}
        \caption[Amplitude modulation]{
            Amplitude modulation 
            % For amplitude modulation, the sidebands align when they point along the carrier axis. 
            % This modulates the amplitude of the total electric field (Eq.~\ref{eq:amplitude_modulation_power}).
        }
        \label{fig:amplitude_modulation} 
    \end{subfigure}
    \hfill
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./chapters/modulations/figures/frequency_modulation.pdf}
        \caption[Phase modulation]{
            Phase modulation
        }
        \label{fig:phase_modulation} 
    \end{subfigure}
    \caption[Amplitude and phase modulation diagrams]{
        Diagram (1) illustrates the sideband picture, with the x-axis representing frequency and the y- and z-axes representing phase.
        Diagram (2) is a phasor diagram, looking along the frequency axis.
        The static carrier $\vec{E}_0$ oscillates at the carrier frequency $\omega_0$,
        the upper sideband $\vec{E}_{usb}$ at the frequency $\omega_0 + \omega$, and
        the lower sideband $\vec{E}_{lsb}$ at the frequency $\omega_0 - \omega$.
        $\vec{E}_{usb}$ and $\vec{E}_{lsb}$ rotate relative to the carrier $\vec{E}_0$ in opposite directions.
    }
    \label{fig:modulation}
\end{figure}



\section{General sideband power}
\label{sec:general_sideband}

The above sections focused on special cases of modulation.
Here we derive a general expression for power for some arbitrary modulation at signal frequency $\omega$.

We write the modulated carrier as
In the sideband picture, the total electric field can be written
\begin{align}
    \label{eq:electric_field_sidebands}
    \vec{E}_{tot}   &= E_0 e^{i \omega_0 t} \left[ 1 + \delta_+ e^{i \omega t} + \delta_- e^{-i \omega t} \right] \\
                    &= \vec{E}_0 + \vec{E}_+ e^{i \omega t} + \vec{E}_- e^{-i \omega t}
\end{align}

Looking for the power components at DC, $\omega$, and $2\omega$:
\begin{align}
    \label{eq:total_power_sidebands}
    P_\mathrm{tot}       &= |\vec{E}_{tot}|^2 \\
                &= |\vec{E}_0 + \vec{E}_+ e^{i \omega t} + \vec{E}_- e^{-i \omega t}|^2 \\
    \label{eq:dc_power_sidebands}
    P_0         &= |\vec{E}_0|^2 + |\vec{E}_+|^2 + |\vec{E}_-|^2 \\
    \label{eq:omega_power_sidebands}
    P_\omega    &= (\vec{E}_0^* \vec{E}_+ + \vec{E}_0 \vec{E}_-^*) e^{i \omega t} + (\vec{E}_0 \vec{E}_+^* + \vec{E}_0^* \vec{E}_-) e^{-i \omega t} \\
    \label{eq:two_omega_power_sidebands}
    P_{2\omega} &= \vec{E}_+ \vec{E}_-^* e^{i 2 \omega t} + \vec{E}_+^* \vec{E}_- e^{-i 2 \omega t}
\end{align}

$P_\omega$ carries the linear audio signal we care about.
The two components of the sum making up $P_\omega$ are complex conjugates of one another, so we can write
\begin{align}
    \label{eq:omega_power_sidebands_part2}
    P_\omega    &= 2 \Re( (\vec{E}_0^* \vec{E}_+ + \vec{E}_0 \vec{E}_-^*) e^{i \omega t} )
\end{align}
or, leaving $P_\omega$ as a complex quantity,
\begin{align}
    \label{eq:omega_power_sidebands_final}
    \tilde{P_\omega}    &= 2 (\vec{E}_0^* \vec{E}_+ + \vec{E}_0 \vec{E}_-^*)
\end{align}



\section{Quadratures}
\label{sec:quadratures}

Amplitude and phase quadratures offer a convenient way of representing light.
Instead of upper and lower sidebands, modulations are broken down into how they affect the carrier.

Using the two-photon formalism of Caves and Schumaker \cite{Caves1985, Schumaker1985}, 
the quantum annihilation and creation operators for photons of frequency $\omega$, $a_\omega$ and $a_\omega^\dagger$,
are abused to describe the modulation process as annihilating a photon at one frequency and creating another photon at a different, nearby frequency.

% Recall that the creation and annihilation operators $a_\omega$ and $a_\omega^\dagger$ do not commute with one another \cite{GerryKnight2004}:
% \begin{align}
%     \label{eq:creation_annihilation_commutator}
%     [a_\omega, a_\omega^\dagger] = 1
% \end{align}

Following \cite{Buonanno2001, Somiya2006}, the upper sideband annihilation operator $a_+$ and the lower sideband annihilation operator $a_-$ are defined as
\begin{align}
    \label{eq:sideband_operators}
    a_+ = a_{\omega_0 + \omega} \sqrt{\dfrac{\omega_0 + \omega}{\omega_0}} \qquad a_- = a_{\omega_0 - \omega} \sqrt{\dfrac{\omega_0 - \omega}{\omega_0}}.
\end{align} 
Because the carrier frequency $\omega_0$ is much larger than the signal frequencies we care about $\omega$, we can safely ignore the term in the root.

% Using Eq.~\ref{eq:creation_annihilation_commutator}, 
% these annihilation operators and their associated creation operators $a_+^\dagger$ and $a_-^\dagger$ satisfy the following commutation relations:
% \begin{align}
%     \label{eq:sideband_commutators}
%     [a_+, a_+^\dagger] = 1 \qquad [a_-, a_-^\dagger] = 1
% \end{align}

The amplitude and phase quadrature operators $a_1$ and $a_2$ are now defined as 
\begin{align}
    \label{eq:quadrature_operators}
    a_1 = a_+ + a_-^\dagger \qquad a_2 = i (a_+ - a_-^\dagger).
\end{align}
$a_1$ and $a_2$ do not commute with each other, but do commute with themselves, unlike $a_+$ and $a_-$.
We use the definition in Eq.~\ref{eq:quadrature_operators} to make shot noise units equal to one (Subsection \ref{subsec:quadrature_units_to_electric_field_units}),
and make taking the dot product between static and modulated sidebands straightforward (Subsection \ref{subsec:modulations_in_quadrature_representation}).

% \begin{alignat}{2}
%     \label{eq:sideband_commutators_part1}
%     [a_1, a_2^\dagger] &= i \qquad [a_2, a_1^\dagger] &&= -i \\
%     \label{eq:sideband_commutators_part2}
%     [a_1, a_1^\dagger] &= 0 \qquad [a_2, a_2^\dagger] &&= 0
% \end{alignat}

% This means the amplitude and phase quadrature $a_1$ and $a_2$ are not independent, but have quantum correlations, 
% including a the minimum Heisenberg uncertainty that must be present in the product of the two quadratures:
% \begin{align}
%     \label{eq:heisenberg_uncertainty}
%     \Delta a_1 \Delta a_2 \geq \dfrac{1}{4} \left| [a_1, a_2^\dagger] \right| = \dfrac{1}{4}.
% \end{align}
% The variances in each quadrature do not have to be equal, meaning squeezed quantum states are possible \cite{Caves1985, Schumaker1985}.

The quadrature operators can be written as a vector $\vec{a}$ representing the modulations of the total electric field:
\begin{align}
    \label{eq:quadrature_vector}
    \vec{a} = \begin{pmatrix}
        a_1 \\
        a_2
    \end{pmatrix}.
\end{align}

Figure~\ref{fig:modulation} illustrates the amplitude and phase quadratures on the phasor diagrams.


\subsection{Quadrature units to electric field units}
\label{subsec:quadrature_units_to_electric_field_units}

The quadrature operators $a_1$ and $a_2$ are general quantum operators representing field amplitudes oscillating $90^\circ$ out of phase we each other.
$a_1$ and $a_2$ do not commute \cite{GerryKnight2004}:
\begin{align}
    \label{eq:quadrature_operators_commutator}
    [a_1, a_2] = 2 i
\end{align}
as long as $a_1$ and $a_2$ represent the same signal frequency $\omega_0 \pm \omega$.

The Heisenberg uncertainty for these non-commuting operators is
\begin{align}
    \label{eq:quadrature_heisenberg_uncertainty}
    \sigma^2_{a_1} \sigma^2_{a_2} &\geq \dfrac{1}{4} \left| \langle [a_1, a_2] \rangle \right|^2 \\
    \sigma^2_{a_1} \sigma^2_{a_2} &= 1
\end{align}
For unsqueezed vacuum, we have
\begin{align} 
    \langle a_1 \rangle = \langle a_2 \rangle = 0 \\ 
    \sigma^2_{a_1} = \sigma^2_{a_2} = 1.
\end{align}
We say that $a_1$ and $a_2$ have ``units of shot noise'' such that their variance is equal to one.
This is used in Eq.~\ref{eq:shot_noise_null_psd}.

To return to units of electric field, we first note that LIGO literature scales electric fields to have units of $\sqrt{\mathrm{W}}$
so the power in watts can be calculated like 
\begin{align}
    \label{eq:power_ligo_units}
    P = |E|^2.
\end{align}

In electricity and magnetism, power in an electric field is expressed like 
\begin{align}
    \label{eq:power_real_units}
    P = A I = \dfrac{A c \epsilon_0 |E|^2}{2}
\end{align}
where $A$ is the surface area of the beam, $I$ is the beam intensity in $\mathrm{W/m^2}$, and $\epsilon_0$ is the vacuum permeability.
Here, $E$ has units $\mathrm{V/m}$, $\epsilon_0$ has units $\mathrm{N/V^2}$, $A$ has $\mathrm{m^2}$, and $c$ has $\mathrm{m/s}$
Because the entire beam is assumed to be captured by the photodetector, 
we incorporate the prefactor $A c \epsilon_0 / 2$ into our electric field for simplicity.

Finally, we relate the quadrature operators $a_1$ and $a_2$ to their equivalent electric fields $E_1$ and $E_2$ by
\begin{align}
    \label{eq:quadrature_operators_to_quadrature_electric_fields}
    a_1 = \sqrt{\hbar \omega_0} E_1  \qquad  a_2 = \sqrt{\hbar \omega_0} E_2.
\end{align}
This converts units of shot noise to the LIGO units of $\sqrt{W}$.
(The real units of $\hbar \omega_0$ are joules, but the time average has been incorporated into our LIGO electric field units).



\subsection{Modulations in quadrature representation}
\label{subsec:modulations_in_quadrature_representation}

We can express the sideband amplitude and phase modulations from Eqs.\ref{eq:intensity_modulation} and ~\ref{eq:frequency_modulation} 
in quadrature representation using \ref{eq:quadrature_operators}.

We break up the total electric field $\vec{E}_{tot} = \vec{E} + \vec{e}(\omega)$ into a ``static'' component $\vec{E}$ centered around the carrier frequency $\omega_0$ 
and a modulation, or Fourier, component $\vec{e}(\omega)$ at the relative signal frequencies $\omega_0 \pm \omega$.
Expressing an electric field modulated in both intensity and frequency $\vec{E}_{tot}$ from Eqs.~\ref{eq:intensity_modulation} and \ref{eq:frequency_modulation} in quadrature representation yields
\begin{align}
    \label{eq:modulations_in_quadrature}
    \vec{E}_{tot} &= \vec{E} + \vec{e}(\omega) \\
    \label{eq:static_electric_field_quadrature}
    \vec{E} &= E_0 e^{i \omega_0 t} \begin{pmatrix}
        1 \\
        0
    \end{pmatrix} \\
    \label{eq:modulated_electric_field_quadrature}
    \vec{e}(\omega) &= E_0 e^{i \omega_0 t} e^{i \omega t} \Bigg[ \dfrac{2 \pi \delta \nu}{\omega} 
    \begin{pmatrix}
        0 \\
        1
    \end{pmatrix} 
    + \dfrac{\delta P}{2 P} 
    \begin{pmatrix}
        1 \\
        0
    \end{pmatrix}
    \Bigg]
\end{align}
Here, we add our upper and lower sidebands from Eqs.~\ref{eq:intensity_modulation} and \ref{eq:frequency_modulation} together according to Eqs.~\ref{eq:quadrature_operators} to produce the quadrature scalers.
This is used in Eq.~\ref{eq:reflected_wave_quadrature} to represent modulated reflection from a mirror.

Eq.~\ref{eq:modulated_electric_field_quadrature} is equivalent to Eq.~10 in \cite{Somiya2006}, 
except for a factor of $\sqrt{2}$ from different quadrature definitions (Eq.~\ref{eq:quadrature_operators} vs Eq.~6 in \cite{Somiya2006}).

For example, we recover intensity noise from Eq.~\ref{eq:intensity_modulation_power}.
Let $\delta \nu \rightarrow 0$ for $\vec{e}(\omega)$ in Eq.~\ref{eq:modulated_electric_field_quadrature}, 
and take the dot product of $\vec{E}_{tot}$ with itself to get power $P_{\delta P}$:
\begin{align}
    \nonumber P_{\delta P} &= \left| \vec{E} + \vec{e}(\omega) \right|^2 \\
    \nonumber &= \left| \vec{E} \right|^2 + \vec{E}^* \cdot \vec{e}(\omega) + \vec{E} \cdot \vec{e}^*(\omega) + \left|\vec{e}(\omega)\right|^2 \\
    \nonumber &\approx E_0^2 \left( 1 + \dfrac{\delta P}{2 P} e^{i \omega t} + \dfrac{\delta P}{2 P} e^{-i \omega t} \right) 
    \begin{pmatrix}
        1 & 0
    \end{pmatrix} 
    \cdot 
    \begin{pmatrix}
        1 \\
        0
    \end{pmatrix}
    \\
    \label{eq:intensity_noise_quadrature_calculation}
    P_{\delta P} &= P_0 \left( 1 + \dfrac{\delta P}{P} \cos(\omega t) \right)
\end{align}
The result from Eq.~\ref{eq:intensity_noise_quadrature_calculation} matches Eq.~\ref{eq:intensity_modulation_power}.

The same is not true for definitions like Eq.~6 in \cite{Somiya2006} or Eq.~2.7 in \cite{Buonanno2001} or Eqs.~2.53 and 2.54 in \cite{GerryKnight2004}:
attention must be paid to the quadrature definition to ensure theory agrees with instrumental results.



\section{Homodyne angle}
\label{eq:homodyne_angle}

The \textit{homodyne angle} $\zeta$ is the static angle between the carrier phase and the signal sideband basis.
The homodyne angle controls what mixed quadrature $a_\zeta$ is measured on the photodetector \cite{KLMTV, Harms2003, GerryKnight2004, Enomoto2016}:
\begin{align}
    \label{eq:homodyne_quadrature}
    a_\zeta = a_1 \cos \zeta + a_2 \sin \zeta
\end{align}

In the sideband picture, the total electric field can be written
\begin{align}
    \label{eq:electric_field_homodyne}
    E_{tot} = E_0 e^{i \omega_0 t} \left[ e^{i \zeta} + \dfrac{\delta_+}{2} e^{i \omega t} + \dfrac{\delta_-}{2} e^{-i \omega t} \right]
\end{align}
where $\delta_+, \delta_-$ is some arbitrary complex modulation.

\begin{figure}[hbt!]
    \centering
    \includegraphics[width=\textwidth]{./chapters/modulations/figures/homodyne_angle_diagram.pdf}
    \caption[Homodyne angle diagram]{
        Diagram (a) illustrates a local oscillator $\vec{L}$ beating with a signal sideband $\vec{a}$ with homodyne angle $\zeta$.
        Diagram (b) is a phasor diagram illustrating the homodyne angle.
        In this diagram, $\vec{a}$ is entirely in $a_2$, so the homodyne angle must be adjusted to $\zeta = \pi/2$ or $\zeta = 3\pi/2$.
    }
    \label{fig:homodyne_angle}
\end{figure}



Figure~\ref{fig:homodyne_angle} shows a basic example of homodyne detection, 
with local oscillator carrier $\vec{L}$ and sidebands $\vec{a}$.
The local oscillator phase can be adjusted by the homodyne angle
\begin{align}
    \label{eq:local_oscillator_phase}
    \vec{L} = E_0 e^{i \omega_0 t} e^{i \zeta}
\end{align}

In terms of the total electric field and total power oscillations, using the quadrature representation,
\begin{align}
    \label{eq:homodyne_quadrature_electric_field}
    \vec{E}_{tot} &= \dfrac{1}{\sqrt{2}} \left( \vec{L} + \vec{a} \right) \\
    \nonumber P_{tot} &= \dfrac{1}{2} \left| \vec{L} + \vec{a} \right|^2 \\
    \nonumber P_{tot} &= \dfrac{1}{2} \left| \vec{L} \right|^2 + \vec{L} \cdot \vec{a} \\
    \nonumber P_{tot} &= \dfrac{1}{2} \left| \vec{L} \right|^2 + E_0
    \begin{pmatrix}
        \cos \zeta & \sin \zeta
    \end{pmatrix} 
    \begin{pmatrix}
        a_1 \\
        a_2
    \end{pmatrix} \\
    \nonumber P_{tot} &= \dfrac{1}{2} E_0^2 + E_0 a_\zeta \\
    \label{eq:homodyne_quadrature_power}
    P_{tot} &= \dfrac{1}{2} P_0 + P_\zeta
\end{align}

Suppose that $\vec{a}$ is in $a_2$, as in Figure~\ref{fig:homodyne_angle}. 
Then the detected power oscillations at $P_\zeta$ is:
\begin{align}
    \label{eq:homodyne_quadrature_power_trig}
    P_\zeta = E_0 a_2 \sin \zeta
\end{align}
and we can detect the phase modulation when the homodyne angle $\zeta = \pi/2$.



\subsection{Conflicting homodyne angle definition}
\label{subsec:conflicting_homodyne_angle_definition}

In Buonanno and Chen's series of landmark Advanced LIGO quantum noise papers \cite{Buonanno2001, BuonannoChen2002, Buonanno2003},
the new homodyne angle $\zeta_{bnc}$ is defined differently than in Eq.~\ref{eq:homodyne_quadrature}.
For these results, Buonanno and Chen used the homodyne angle definition
\begin{align}
    \label{eq:bnc_homodyne_quadrature}
    a_{\zeta} = a_1 \sin \zeta_{bnc} + a_2 \cos \zeta_{bnc}.
\end{align}
In this case, $\zeta_{bnc} = 0$ yields $a_2$, while $\zeta_{bnc} = \pi/2$ yields $a_1$.
The relationship between $\zeta_{bnc}$ and $\zeta$ is
\begin{align}
    \label{eq:bnc_homodyne_to_regular_homodyne}
    \zeta_{bnc} = \dfrac{\pi}{2} - \zeta
\end{align}

Because the results of \cite{Buonanno2001} were so influential, Advanced LIGO literature uses this definition \cite{AdvLIGOFinalDesign, WardThesis, Somiya2006, Hall2019, HallThesis, Izumi2017}.
Advanced LIGO is said to run with a homodyne angle of $\zeta_{bnc} = \pi/2$, 
as the interferometer is configured such that the signal comes out in the amplitude quadrature $b_1$ of the input electric field \cite{Buonanno2001, AdvLIGOFinalDesign}.

For consistency with Advanced LIGO literature, this thesis also uses the Buonanno and Chen homodyne angle $\zeta_{bnc}$, (see Chapters \ref{chap:calibration}, \ref{chap:correlated_noise}).
