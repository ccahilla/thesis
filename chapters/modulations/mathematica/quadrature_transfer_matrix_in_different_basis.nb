(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     11952,        361]
NotebookOptionsPosition[     10134,        321]
NotebookOutlinePosition[     10550,        338]
CellTagsIndexPosition[     10507,        335]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Quadrature transfer matrix", "Section",
 CellChangeTimes->{{3.872788036411063*^9, 
  3.872788051652396*^9}},ExpressionUUID->"f14922c6-bcca-4365-a43a-\
3cc7a8dcad79"],

Cell[CellGroupData[{

Cell["\<\
From Eq A9 of Buonanno and Chen 2003 (Scaling law in signal recycled \
laser-interferometer gravitational wave-detectors)\
\>", "Subsection",
 CellChangeTimes->{{3.872788058664761*^9, 
  3.872788116432603*^9}},ExpressionUUID->"8771e741-3d3b-4801-ba9f-\
0f967b1887a6"],

Cell[CellGroupData[{

Cell[TextData[{
 "I used some non-standard two photon definition:\n",
 Cell[BoxData[{
  FormBox[
   RowBox[{
    SubscriptBox["a", "1"], "=", 
    RowBox[{
     SubscriptBox["a", "+"], "+", 
     SubscriptBox["a", "-"]}]}], TraditionalForm], "\[IndentingNewLine]", 
  FormBox[
   RowBox[{
    SubscriptBox["a", "2"], "=", 
    RowBox[{"i", "(", 
     RowBox[{
      SubscriptBox["a", "+"], "-", 
      SubscriptBox["a", "-"]}], ")"}]}], TraditionalForm]}],
  FormatType->TraditionalForm,ExpressionUUID->
  "ce43aac6-64e8-4632-8cf5-13d0029154bb"],
 "\nas opposed to the usual\n",
 Cell[BoxData[{
  FormBox[
   RowBox[{
    SubscriptBox["a", "1"], "=", 
    FractionBox[
     RowBox[{
      SubscriptBox["a", "+"], "+", 
      SubscriptBox["a", "-"]}], 
     SqrtBox["2"]]}], TraditionalForm], "\[IndentingNewLine]", 
  FormBox[
   RowBox[{
    SubscriptBox["a", "2"], "=", 
    FractionBox[
     RowBox[{
      SubscriptBox["a", "+"], "-", 
      SubscriptBox["a", "-"]}], 
     RowBox[{"i", 
      SqrtBox["2"]}]]}], TraditionalForm]}],
  FormatType->TraditionalForm,ExpressionUUID->
  "aa73f144-a894-4496-8816-60965d02838d"],
 "\nThis notebook is a check of the quadrature transfer matrix T(\
\[CapitalOmega]) in this slightly modified basis.  As it turns out, it doesn\
\[CloseCurlyQuote]t quite work out the same."
}], "Subsubsection",
 CellChangeTimes->{{3.87278812664837*^9, 
  3.872788277797804*^9}},ExpressionUUID->"61bbddac-a74b-4a3f-bc93-\
e9ad628be548"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"Clear", "[", "b1", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"prms1", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"a1", "->", 
      RowBox[{"ap", "+", "am"}]}], ",", 
     RowBox[{"a2", "->", 
      RowBox[{"I", 
       RowBox[{"(", 
        RowBox[{"ap", "-", "am"}], ")"}]}]}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"prms2", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"bp", "\[Rule]", 
      RowBox[{
       FractionBox["1", "2"], " ", 
       RowBox[{"(", 
        RowBox[{"b1", "-", 
         RowBox[{"\[ImaginaryI]", " ", "b2"}]}], ")"}]}]}], ",", 
     RowBox[{"bm", "\[Rule]", 
      RowBox[{
       FractionBox["1", "2"], " ", 
       RowBox[{"(", 
        RowBox[{"b1", "+", 
         RowBox[{"\[ImaginaryI]", " ", "b2"}]}], ")"}]}]}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"b1a", "=", 
  RowBox[{"Simplify", "[", 
   RowBox[{"kk", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"fp", "+", "fm"}], ")"}], "a1"}], "+", 
       RowBox[{
        FractionBox["1", "I"], 
        RowBox[{"(", 
         RowBox[{"fp", "-", "fm"}], ")"}], "a2"}]}], "/.", "prms1"}], ")"}]}],
    "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"b1b", "=", 
  RowBox[{"b1a", "/.", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{"fp", " ", "ap"}], "->", "bp"}], ",", 
     RowBox[{
      RowBox[{"fm", " ", "am"}], "->", "bm"}]}], 
    "}"}]}]}], "\[IndentingNewLine]", 
 RowBox[{"b1c", "=", 
  RowBox[{"Simplify", "[", 
   RowBox[{"b1b", "/.", "prms2"}], "]"}]}]}], "Input",
 CellChangeTimes->{{3.872787520504229*^9, 3.872787599868194*^9}, {
  3.872787653204296*^9, 3.872787840189321*^9}, {3.872787885908214*^9, 
  3.872787920451157*^9}, {3.872788029454406*^9, 3.872788034440425*^9}, {
  3.872788448853767*^9, 3.872788451039343*^9}, {3.872788609053235*^9, 
  3.872788622861931*^9}, {3.8727887033519573`*^9, 3.872788723989189*^9}},
 CellLabel->"In[68]:=",ExpressionUUID->"d250cc4f-b545-4e92-b6f0-f22a6ff245e7"],

Cell[BoxData[
 RowBox[{"2", " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"am", " ", "fm"}], "+", 
    RowBox[{"ap", " ", "fp"}]}], ")"}], " ", "kk"}]], "Output",
 CellChangeTimes->{{3.87278769324914*^9, 3.8727877468467417`*^9}, {
   3.8727877945429363`*^9, 3.87278784059347*^9}, {3.872787893407382*^9, 
   3.8727879207522593`*^9}, {3.872788617253168*^9, 3.872788626336281*^9}, 
   3.872788724484858*^9},
 CellLabel->"Out[71]=",ExpressionUUID->"d5ea9fbc-a44c-4818-83ad-aa10888ea75e"],

Cell[BoxData[
 RowBox[{"2", " ", 
  RowBox[{"(", 
   RowBox[{"bm", "+", "bp"}], ")"}], " ", "kk"}]], "Output",
 CellChangeTimes->{{3.87278769324914*^9, 3.8727877468467417`*^9}, {
   3.8727877945429363`*^9, 3.87278784059347*^9}, {3.872787893407382*^9, 
   3.8727879207522593`*^9}, {3.872788617253168*^9, 3.872788626336281*^9}, 
   3.8727887244887247`*^9},
 CellLabel->"Out[72]=",ExpressionUUID->"f0b9312b-15c6-4dbf-975b-7ef1561c43c6"],

Cell[BoxData[
 RowBox[{"2", " ", "b1", " ", "kk"}]], "Output",
 CellChangeTimes->{{3.87278769324914*^9, 3.8727877468467417`*^9}, {
   3.8727877945429363`*^9, 3.87278784059347*^9}, {3.872787893407382*^9, 
   3.8727879207522593`*^9}, {3.872788617253168*^9, 3.872788626336281*^9}, 
   3.872788724492977*^9},
 CellLabel->"Out[73]=",ExpressionUUID->"15db78d9-2559-4814-a771-c55ffc66cd92"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"b2a", "=", 
  RowBox[{"Simplify", "[", 
   RowBox[{"kk", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"-", 
         FractionBox["1", "I"]}], 
        RowBox[{"(", 
         RowBox[{"fp", "-", "fm"}], ")"}], "a1"}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"fp", "+", "fm"}], ")"}], "a2"}]}], "/.", "prms1"}], ")"}]}],
    "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"b2b", "=", 
  RowBox[{"b2a", "/.", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{"fp", " ", "ap"}], "->", "bp"}], ",", 
     RowBox[{
      RowBox[{"fm", " ", "am"}], "->", "bm"}]}], 
    "}"}]}]}], "\[IndentingNewLine]", 
 RowBox[{"b2c", "=", 
  RowBox[{"Simplify", "[", 
   RowBox[{"b2b", "/.", "prms2"}], "]"}]}]}], "Input",
 CellChangeTimes->{{3.872788452205311*^9, 3.8727884968579683`*^9}, {
  3.8727886298647003`*^9, 3.872788631265658*^9}, {3.872788714452354*^9, 
  3.8727887196541777`*^9}},
 CellLabel->"In[74]:=",ExpressionUUID->"c9f7a437-ea9c-4415-8407-8c472c1d0680"],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "2"}], " ", "\[ImaginaryI]", " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"am", " ", "fm"}], "-", 
    RowBox[{"ap", " ", "fp"}]}], ")"}], " ", "kk"}]], "Output",
 CellChangeTimes->{3.872788497303877*^9, 3.872788631632247*^9, 
  3.8727887256531343`*^9},
 CellLabel->"Out[74]=",ExpressionUUID->"45129fbd-993e-4f41-a1bc-f36b5694833b"],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "2"}], " ", "\[ImaginaryI]", " ", 
  RowBox[{"(", 
   RowBox[{"bm", "-", "bp"}], ")"}], " ", "kk"}]], "Output",
 CellChangeTimes->{3.872788497303877*^9, 3.872788631632247*^9, 
  3.872788725656889*^9},
 CellLabel->"Out[75]=",ExpressionUUID->"e660c3b2-fc1c-48e8-83ac-fd40de8311e1"],

Cell[BoxData[
 RowBox[{"2", " ", "b2", " ", "kk"}]], "Output",
 CellChangeTimes->{3.872788497303877*^9, 3.872788631632247*^9, 
  3.87278872566096*^9},
 CellLabel->"Out[76]=",ExpressionUUID->"4722c6d4-0529-4c29-b30b-25777156168e"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
The above shows that the Eq A9 for the above quadrature definitions should be\
\
\>", "Subsubsection",
 CellChangeTimes->{{3.872788320358542*^9, 3.872788351342478*^9}, {
  3.872788517453624*^9, 
  3.872788523618273*^9}},ExpressionUUID->"38180e35-fbfd-42dc-a90a-\
10efae22f79d"],

Cell[BoxData[
 RowBox[{
  RowBox[{"T", "=", 
   RowBox[{
    FractionBox["1", "2"], 
    RowBox[{"(", "\[NoBreak]", GridBox[{
       {
        RowBox[{
         SubscriptBox["f", "+"], "+", 
         SubscriptBox["f", "-"]}], 
        RowBox[{
         FractionBox["1", "I"], 
         RowBox[{"(", 
          RowBox[{
           SubscriptBox["f", "+"], "-", 
           SubscriptBox["f", "-"]}], ")"}]}]},
       {
        RowBox[{
         RowBox[{"-", 
          FractionBox["1", "I"]}], 
         RowBox[{"(", 
          RowBox[{
           SubscriptBox["f", "+"], "+", 
           SubscriptBox["f", "-"]}], ")"}]}], 
        RowBox[{
         SubscriptBox["f", "+"], "+", 
         SubscriptBox["f", "-"]}]}
      }], "\[NoBreak]", ")"}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.872788363535075*^9, 3.872788442513095*^9}, {
  3.872788662273354*^9, 3.872788678047853*^9}},
 CellLabel->"In[67]:=",ExpressionUUID->"1482823b-4632-4510-b5f5-cc9cb119c88c"]
}, Open  ]],

Cell[TextData[{
 "This can easily be fixed by changing\n",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["a", "2"], "=", 
    RowBox[{"i", "(", 
     RowBox[{
      SubscriptBox["a", "+"], "-", 
      SubscriptBox["a", "-"]}], ")"}]}], TraditionalForm]],ExpressionUUID->
  "a76b659f-76c1-42de-9566-35fcb744e87f"],
 "\nto\n",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["a", "2"], "=", 
    RowBox[{
     FractionBox["1", "i"], 
     RowBox[{"(", 
      RowBox[{
       SubscriptBox["a", "+"], "-", 
       SubscriptBox["a", "-"]}], ")"}]}]}], TraditionalForm]],ExpressionUUID->
  "d38c6046-5c28-4955-9857-bd413ca37d46"],
 "\nThen the old Eq A9 works fine."
}], "Subsubsection",
 CellChangeTimes->{{3.872788555549205*^9, 3.872788580859316*^9}, {
  3.8727886410701036`*^9, 
  3.8727886486677847`*^9}},ExpressionUUID->"fcaf4d1e-6e86-4311-9016-\
d4aad2fdb8ea"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{808, 755},
WindowMargins->{{92, Automatic}, {Automatic, 36}},
CellContext->Notebook,
FrontEndVersion->"12.3 for Mac OS X x86 (64-bit) (June 19, 2021)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"79a9c966-cbcf-40f2-bf15-615620e78880"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 171, 3, 67, "Section",ExpressionUUID->"f14922c6-bcca-4365-a43a-3cc7a8dcad79"],
Cell[CellGroupData[{
Cell[776, 29, 277, 6, 81, "Subsection",ExpressionUUID->"8771e741-3d3b-4801-ba9f-0f967b1887a6"],
Cell[CellGroupData[{
Cell[1078, 39, 1463, 45, 254, "Subsubsection",ExpressionUUID->"61bbddac-a74b-4a3f-bc93-e9ad628be548"],
Cell[CellGroupData[{
Cell[2566, 88, 2044, 61, 171, "Input",ExpressionUUID->"d250cc4f-b545-4e92-b6f0-f22a6ff245e7"],
Cell[4613, 151, 483, 10, 34, "Output",ExpressionUUID->"d5ea9fbc-a44c-4818-83ad-aa10888ea75e"],
Cell[5099, 163, 433, 8, 34, "Output",ExpressionUUID->"f0b9312b-15c6-4dbf-975b-7ef1561c43c6"],
Cell[5535, 173, 383, 6, 34, "Output",ExpressionUUID->"15db78d9-2559-4814-a771-c55ffc66cd92"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5955, 184, 1035, 31, 90, "Input",ExpressionUUID->"c9f7a437-ea9c-4415-8407-8c472c1d0680"],
Cell[6993, 217, 372, 9, 34, "Output",ExpressionUUID->"45129fbd-993e-4f41-a1bc-f36b5694833b"],
Cell[7368, 228, 318, 7, 34, "Output",ExpressionUUID->"e660c3b2-fc1c-48e8-83ac-fd40de8311e1"],
Cell[7689, 237, 229, 4, 57, "Output",ExpressionUUID->"4722c6d4-0529-4c29-b30b-25777156168e"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[7967, 247, 287, 7, 45, "Subsubsection",ExpressionUUID->"38180e35-fbfd-42dc-a90a-10efae22f79d"],
Cell[8257, 256, 958, 30, 63, "Input",ExpressionUUID->"1482823b-4632-4510-b5f5-cc9cb119c88c"]
}, Open  ]],
Cell[9230, 289, 876, 28, 149, "Subsubsection",ExpressionUUID->"fcaf4d1e-6e86-4311-9016-d4aad2fdb8ea"]
}, Open  ]]
}, Open  ]]
}
]
*)

