%%%%%%%%%%%%
%% Please rename this main.tex file and the output PDF to
%% [lastname_firstname_graduationyear]
%% before submission.
%%
%% This .tex file is for use with BibLaTeX. Please use
%% main-bibtex.tex instead if you prefer BibTeX.
%%%%%%%%%%%%

\documentclass[12pt]{caltech_thesis}
% \setcounter{tocdepth}{2}
\setsecnumdepth{subsubsection}
\usepackage[hyphens]{url}
\usepackage{graphicx}
\usepackage{commands}

\usepackage{libertine}
\usepackage[T1]{fontenc}
% \usepackage{newtxtext,newtxmath}
% \usepackage{gfsartemisia-euler}
% \usepackage[T1]{fontenc}
% \usepackage{kpfonts}
% \usepackage[T1]{fontenc}

\usepackage{enumitem}

\usepackage{todonotes}

\usepackage{listings}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstset{ %
  backgroundcolor=\color{white},   % choose the background color
  basicstyle=\footnotesize,        % size of fonts used for the code
  breaklines=true,                 % automatic line breaking only at whitespace
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  keywordstyle=\color{blue},       % keyword style
  stringstyle=\color{mymauve},     % string literal style
}

%% Tentative: newtx for better-looking Times
% \usepackage[utf8]{inputenc}
% \usepackage[T1]{fontenc}
%\usepackage{newtxtext,newtxmath}

% Must use biblatex to produce the Published Contents and Contributions, per-chapter bibliography (if desired), etc.
\usepackage{hyperref}
\hypersetup{
  colorlinks = true,
  citecolor=[rgb]{0.58039216, 0.        , 0.82745098},
  linkcolor=[rgb]{0.13333333, 0.50509804, 0.13333333},
  urlcolor=[rgb]{0.2745098 , 0.50980392, 0.70588235},
}
\usepackage[backend=biber,hyperref=true,maxnames=2,minnames=2,giveninits=true,natbib=true,sortlocale=en_US,sorting=none,style=numeric-comp,useprefix=true,url=false,doi=false,date=year,eprint=false]{biblatex}
\renewbibmacro{in:}{}

\usepackage{amsmath} % I want dfrac

\usepackage[section]{placeins}

\usepackage{caption}
\usepackage{subcaption}

% Name of your .bib file(s)

\addbibresource{./ownpubs.bib}
\addbibresource{./Cahillane_Craig_Thesis.bib}

\begin{document}

% Do remember to remove the square bracket!
\title{Controlling and Calibrating Interferometric Gravitational Wave Detectors}
\author{Craig Cahillane}

\degreeaward{Doctor of Philosophy}                              % Degree to be awarded
\university{California Institute of Technology}    % Institution name
\address{Pasadena, California}                     % Institution address
\unilogo{./figures/caltech.png}                              % Institution logo

\orcid{0000-0002-3888-314X}

%% IMPORTANT: Select ONE of the rights statement below.
\rightsstatement{All rights reserved}
% \rightsstatement{All rights reserved except where otherwise noted}
% \rightsstatement{Some rights reserved. This thesis is distributed under a [name license, e.g., ``Creative Commons Attribution-NonCommercial-ShareAlike License'']}

%%  If you'd like to remove the Caltech logo from your title page, simply remove the "[logo]" text from the maketitle command
\maketitle[logo]
%\maketitle

\begin{abstract}

In September 2015, the Advanced LIGO detectors made the first direct detection of gravitational waves from a binary black hole merger \cite{GW150914}.
Since then, around fifty total gravitational wave detections have been reported by Advanced LIGO and Advanced Virgo over three dedicated gravitational wave observation times, known as observing runs.

Observing run three (O3) ran from April 2019 to March 2020, with higher sensitivity and more stable operation of the Advanced LIGO detectors \cite{Buikema2020}.
In the first half of O3, thirty-nine gravitational wave events were detected \cite{SecondCatalogPaper}, as opposed to eleven in all of observing runs one (O1) and two (O2) \cite{FirstCatalogPaper}.
The higher rate of detections is due primarily to the increased detector sensitivity to gravitational waves.

Although the Advanced LIGO detectors are more sensitive to gravitational waves than any detector in history, they have not yet achieved design sensitivity.
Work continues to push the detectors to their fundamental limit of sensitivity.
The work in this thesis partially covers the effort to improve the sensitivity of the LIGO Hanford detector prior to O3.

Calibration of the Advanced LIGO interferometer is the conversion of raw detector data into gravitational wave strain data.
This process is crucial to an accurate and precise understanding of astrophysical sources of gravitational waves.
The calibration uncertainty pipeline for characterizing the strain uncertainty during O1 and O2 is discussed in detail \cite{CalUncPaper}.

This thesis covers topics in long-baseline interferometric gravitational wave detector technology,
including an overview of the performance of the detector in O3,
commissioning tasks done to increase the sensitivity of the detector for O3,
overall calibration uncertainty in the gravitational wave data,
and methods for robust estimation of spectral quantities from LIGO data.

\end{abstract}

\begin{acknowledgements}

My path through grad school was anything but straightforward.  
I thank everyone who helped me along the way.

Thank you Alan for giving me the opportunity to work in LIGO from O1 to O3,
for your hands-on mentorship in the the early years,
and your patience and counsel in the later years.

Thank you Rana for the opportunity to join your group halfway through grad school,
for showing us how to control ASC,
and for the stories about the old days.

Thank you Max, Tom, and Surabhi for your companionship throughout grad school and for making the third floor of West Bridge fun.
Thank you Darkhan and Sudarshan for inviting me into the calibration group at Hanford my first time there.
Thank you Evan and Kiwamu for mentoring a young grad student and showing me the path I eventually took for myself.

Thank you Andrew for making the coffee lobster, buying all those boxes, and for teaching me how to take a transfer function.
Thank you Johannes, Aaron, and Brittany for bringing light to the basement.
Thank you Kevin for holding me to a higher academic standard.

Thank you Daniel for reading my baffling alogs, creating a positive commissioning environment, and steering me on the right course.
Thank you Keita for letting me fix frequency noise during run time.
Thank you Jeff for talking about calibration with me.
Thank you Sheila and Jenne for your incredible mentorship, patience, and friendship.
Thank you to the visiting scientists who took interest in my work and invited me into theirs, 
including Peter, Stefan, Robert, Rich, and Calum.

Thank you Aidan, Anchal, and Jaime for getting coffee in the park.
Thank you Gabriele for sharing an office, your data, and your knowledge with me.
Thank you Koji for for all the black glass beam dumps, late night OMC measurements, and for teaching me about intensity noise and HOMs.

Thank you Dan for the late night commissioning times and inviting me into the Finesse group.
Thank you Anna, Paul, and Andreas for providing valuable feedback on the interferometer simulation inner workings.

Thank you TJ and Mary for letting me sleep in your house for a month.
Thank you Gautam for being my go-to friend for all interferometer ideas, co-captaining the Strong Fielders softball team, and for living with me through a pandemic.

Thank you to my mom and dad, Susan and Mark, for the many packages of socks and years of support.
Thank you to my brother Dennis who has mentored me and always been my friend.
Thank you to Georgia who I met along the way.
I love you.

%
% Gautam, TJ, Johannes, Jaime, Eric, Aaron, Brittany, Anchal, Kevin
%
% Max, Tom, Surabhi, Sarah, TJ, Alex, Jess
%
% The 2017 Caltech GSC Champion Strong Fielders
%
% Gabriele, Aidan, Koji
%
% Hanford Crew 1
% Evan, Kiwamu, Dave, Ross
%
% Hanford Crew 2
% Nutsinee, Sheila, Jenne, Keita, Daniel
%
% Finesse Crew
% Dan, Andreas, Anna
%
% Wadey and the Caltech Basement
%
% Rana
%
% Alan
%
% Mom and Dad and Dennis
%
% Georgia
    
% \href{https://gitlab.com/ccahilla/thesis}{Craig's thesis GitLab}
    
\end{acknowledgements}

%% Uncomment the `i know what to do' option to dismiss the instruction in the PDF.
\begin{publishedcontent}[iknowwhattodo]
% List your publications and contributions here.
\nocite{PhysRevD.96.102001}
\nocite{3gfn_paper}
\nocite{Buikema2020}
\nocite{Hall2019}
\nocite{PhysRevD.95.062003}
\nocite{median_csds}
\nocite{nds2utils}
\nocite{beamtrace}

\end{publishedcontent}

\tableofcontents
\listoffigures
\listoftables
\printnomenclature

\mainmatter

\include{chapters/introduction/introduction}

\include{chapters/detector_design/detector_design}

\include{chapters/o3_sensitivity/o3_sensitivity}

\include{chapters/calibration/calibration}

\include{chapters/correlated_noise/correlated_noise}

\include{chapters/probability_distributions/probability_distributions}

\include{chapters/future_work/future_work}

\appendix

\include{chapters/modulations/modulations}

\include{chapters/simple_michelson_laser_noise/simple_michelson_laser_noise}

\include{chapters/topics/topics}

\include{chapters/transfer_function_estimates/transfer_function_estimates}

\include{chapters/injection_uncertainty/injection_uncertainty}

\include{chapters/psd_rejection/psd_rejection}




% Bibliography
\printbibliography[heading=bibintoc]

% \chapter{Consent Form}

% \printindex

% \theendnotes

% %% Pocket materials at the VERY END of thesis
% \pocketmaterial
% \extrachapter{Pocket Material: Map of Case Study Solar Systems}


\end{document}
