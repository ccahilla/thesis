'''
histogram_animation.py

Animates the building of a histogram of PSDs and CSDs.

Craig Cahillane
July 12, 2020
'''

import os
import sys
import time
import re

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

from matplotlib.animation import FuncAnimation
import matplotlib.patches as patches
import matplotlib.path as path

import scipy.signal

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})


#####   Functions   #####
def get_all_psds(x, fs=1.0, window='hann', nperseg=None, noverlap=None,
                nfft=None, detrend='constant', return_onesided=True,
                scaling='density', axis=-1):
    '''Abuses the function scipy.signal.spectrogram() 
    to return all power spectral densities which make up a PSD estimate.
    Should be equivalent exactly to scipy.signal.spectrogram(mode='psd'),
    but we want to use this for cross spectral density estimation as well.

    Parameters
    ----------
    x : array_like
        Time series of measurement values
    fs : float, optional
        Sampling frequency of the `x` time series. Defaults to 1.0.
    window : str or tuple or array_like, optional
        Desired window to use. If `window` is a string or tuple, it is
        passed to `get_window` to generate the window values, which are
        DFT-even by default. See `get_window` for a list of windows and
        required parameters. If `window` is array_like it will be used
        directly as the window and its length must be nperseg.
        Defaults to a Tukey window with shape parameter of 0.25.
    nperseg : int, optional
        Length of each segment. Defaults to None, but if window is str or
        tuple, is set to 256, and if window is array_like, is set to the
        length of the window.
    noverlap : int, optional
        Number of points to overlap between segments. If `None`,
        ``noverlap = nperseg // 8``. Defaults to `None`.
    nfft : int, optional
        Length of the FFT used, if a zero padded FFT is desired. If
        `None`, the FFT length is `nperseg`. Defaults to `None`.
    detrend : str or function or `False`, optional
        Specifies how to detrend each segment. If `detrend` is a
        string, it is passed as the `type` argument to the `detrend`
        function. If it is a function, it takes a segment and returns a
        detrended segment. If `detrend` is `False`, no detrending is
        done. Defaults to 'constant'.
    return_onesided : bool, optional
        If `True`, return a one-sided spectrum for real data. If
        `False` return a two-sided spectrum. Defaults to `True`, but for
        complex data, a two-sided spectrum is always returned.
    scaling : { 'density', 'spectrum' }, optional
        Selects between computing the power spectral density ('density')
        where `Sxx` has units of V**2/Hz and computing the power
        spectrum ('spectrum') where `Sxx` has units of V**2, if `x`
        is measured in V and `fs` is measured in Hz. Defaults to
        'density'.
    axis : int, optional
        Axis along which the spectrogram is computed; the default is over
        the last axis (i.e. ``axis=-1``).
    mode : str, optional
        Defines what kind of return values are expected. Options are
        ['psd', 'complex', 'magnitude', 'angle', 'phase']. 'complex' is
        equivalent to the output of `stft` with no padding or boundary
        extension. 'magnitude' returns the absolute magnitude of the
        STFT. 'angle' and 'phase' return the complex angle of the STFT,
        with and without unwrapping, respectively.

    Returns
    -------
    f : ndarray
        Array of sample frequencies.
    t : ndarray
        Array of segment times.
    Sxxs : ndarray
        Power spectral densities of x. By default, the last axis of Sxx corresponds
        to the segment times.

    '''
    freqs, time, Fxx = scipy.signal.spectrogram(x, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap,
                                    nfft=nfft, detrend=detrend, return_onesided=return_onesided,
                                    scaling=scaling, axis=axis, mode='complex')
    Sxxs = np.abs( 2 * np.conjugate(Fxx) * Fxx )

    return freqs, time, Sxxs

def get_all_csds(x, y, fs=1.0, window='hann', nperseg=None, noverlap=None,
                nfft=None, detrend='constant', return_onesided=True,
                scaling='density', axis=-1):
    '''Abuses the function scipy.signal.spectrogram() 
    to return all cross spectral densities which make up a CSD estimate.


    Parameters
    ----------
    x : array_like
        Time series of measurement values
    y : array_like
        Time series of measurement values
    fs : float, optional
        Sampling frequency of the `x` time series. Defaults to 1.0.
    window : str or tuple or array_like, optional
        Desired window to use. If `window` is a string or tuple, it is
        passed to `get_window` to generate the window values, which are
        DFT-even by default. See `get_window` for a list of windows and
        required parameters. If `window` is array_like it will be used
        directly as the window and its length must be nperseg.
        Defaults to a Tukey window with shape parameter of 0.25.
    nperseg : int, optional
        Length of each segment. Defaults to None, but if window is str or
        tuple, is set to 256, and if window is array_like, is set to the
        length of the window.
    noverlap : int, optional
        Number of points to overlap between segments. If `None`,
        ``noverlap = nperseg // 8``. Defaults to `None`.
    nfft : int, optional
        Length of the FFT used, if a zero padded FFT is desired. If
        `None`, the FFT length is `nperseg`. Defaults to `None`.
    detrend : str or function or `False`, optional
        Specifies how to detrend each segment. If `detrend` is a
        string, it is passed as the `type` argument to the `detrend`
        function. If it is a function, it takes a segment and returns a
        detrended segment. If `detrend` is `False`, no detrending is
        done. Defaults to 'constant'.
    return_onesided : bool, optional
        If `True`, return a one-sided spectrum for real data. If
        `False` return a two-sided spectrum. Defaults to `True`, but for
        complex data, a two-sided spectrum is always returned.
    scaling : { 'density', 'spectrum' }, optional
        Selects between computing the power spectral density ('density')
        where `Sxx` has units of V**2/Hz and computing the power
        spectrum ('spectrum') where `Sxx` has units of V**2, if `x`
        is measured in V and `fs` is measured in Hz. Defaults to
        'density'.
    axis : int, optional
        Axis along which the spectrogram is computed; the default is over
        the last axis (i.e. ``axis=-1``).
    mode : str, optional
        Defines what kind of return values are expected. Options are
        ['psd', 'complex', 'magnitude', 'angle', 'phase']. 'complex' is
        equivalent to the output of `stft` with no padding or boundary
        extension. 'magnitude' returns the absolute magnitude of the
        STFT. 'angle' and 'phase' return the complex angle of the STFT,
        with and without unwrapping, respectively.

    Returns
    -------
    f : ndarray
        Array of sample frequencies.
    t : ndarray
        Array of segment times.
    Sxys : ndarray
        Cross spectral densities of x and y. By default, the last axis of Sxys corresponds
        to the segment times.

    '''
    freqs, time, Fxx = scipy.signal.spectrogram(x, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap,
                                    nfft=nfft, detrend=detrend, return_onesided=return_onesided,
                                    scaling=scaling, axis=axis, mode='complex')
    freqs, time, Fyy = scipy.signal.spectrogram(y, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap,
                                    nfft=nfft, detrend=detrend, return_onesided=return_onesided,
                                    scaling=scaling, axis=axis, mode='complex')
    Sxys = 2 * np.conjugate(Fxx) * Fyy 

    return freqs, time, Sxys

#####   Time Series Parameters   #####

avg = 10000

N = (avg + 1) * 1000            # number of samples
fs = 1e4                        # Hz, sampling frequency, samples/second
total_time = N / fs             # seconds, total time

nperseg = 1000                  # number of samples in a single fft segment
noverlap = 0 #nperseg // 2      # 50% overlap

bandwidth = fs / nperseg
overlap = noverlap / nperseg

averages = (total_time * bandwidth - 1)/(1 - overlap) + 1

print()
print(f'total samples N = {N}')
print(f'sampling frequency = {fs} Hz')
print()
print(f'total_time = {total_time} seconds')
print(f'bandwidth = {bandwidth} Hz')
print(f'overlap = {100 * overlap} %')
print(f'averages = {averages}')

noise_power_density = 1e-3      # power density in V**2/Hz.  Should show up in the power spectral density.
corr_noise_power_density = 7e-4 # correlated noise power density in V**2/Hz.  
                                # Should be the limit the CSD hits
total_noise_power_density = noise_power_density + corr_noise_power_density

noise_power = noise_power_density * fs / 2              # total power in the noise spectrum in V**2.  
                                                        # Equal to the variance of the gaussian noise.  
corr_noise_power = corr_noise_power_density * fs / 2

times = np.arange(N) / fs

# Make times domain signals
a = np.random.normal(scale=np.sqrt(noise_power), size=times.shape) # sqrt(noise_power) = sigma on guassian noise
b = np.random.normal(scale=np.sqrt(noise_power), size=times.shape)
c = np.random.normal(scale=np.sqrt(corr_noise_power), size=times.shape)

# Put in small delay
corr_noise = c
shift_index = 0 #2
shift_c = np.hstack(( c[shift_index:], c[:shift_index] )) 

# Make signal b glitchy
# nnn = np.arange(100)
# b[10000:10100] += 1e6 * np.sin(2 * np.pi * 2501 * nnn/fs) * np.exp(-nnn / 8)
# b[40000:40100] += 1e5 * np.sin(2 * np.pi * 4501 * nnn/fs) * np.exp(-nnn / 2)
# b[70000:70100] += 2e5 * np.sin(2 * np.pi * 501 * nnn/fs) * np.exp(-nnn / 1)
# b[80000:80100] += 6e6 * np.sin(2 * np.pi * 1001 * nnn/fs) * np.exp(-nnn / 8)

# Signals
x = a + c
y = b + c # shift_c

# Return all PSDs and CSDs directly
ff, tt, Sxxs = get_all_psds(x, fs, nperseg=nperseg, noverlap=noverlap)
_, _, Syys = get_all_psds(y, fs, nperseg=nperseg, noverlap=noverlap)

_, _, Sxys = get_all_csds(x, y, fs, nperseg=nperseg, noverlap=noverlap)



#####   Figures   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1]
script_name = script_name.split('.')[0]

fig_dir = f'{script_dir}/figures/{script_name}'
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)



### Animate a histogram
fig, s1 = plt.subplots(1) 

ff_index = len(ff)//2 - 200
Sxys_mid = np.real( Sxys[ff_index,:] )

num_bins = 200
low_bin = -1e-2
high_bin = 1e-2
bins = np.linspace(low_bin, high_bin, num_bins+1)

NN, _ = np.histogram(Sxys_mid, bins, density=True)

# NN, bins, patchez = s1.hist(Sxys_mid, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$S_{x}$'+' samples')

left = np.array(bins[:-1])
right = np.array(bins[1:])
bottom = np.zeros(len(left))
top = bottom + NN
nrects = len(left)

nverts = nrects * (1 + 3 + 1)
verts = np.zeros((nverts, 2))
codes = np.ones(nverts, int) * path.Path.LINETO
codes[0::5] = path.Path.MOVETO
codes[4::5] = path.Path.CLOSEPOLY
verts[0::5, 0] = left
verts[0::5, 1] = bottom
verts[1::5, 0] = left
verts[1::5, 1] = top
verts[2::5, 0] = right
verts[2::5, 1] = top
verts[3::5, 0] = right
verts[3::5, 1] = bottom

alpha = 1.0
patch_avg = patches.Rectangle((0, 0), 5e-5, 1.1*np.max(NN), color='C1', alpha=alpha, zorder=3, label='Average $\Re(\mathrm{CSD})$ Estimate')
patch_med = patches.Rectangle((0, 0), 5e-5, 1.1*np.max(NN), color='C6', alpha=alpha, zorder=3, label='Median $\Re(\mathrm{CSD})$ Estimate')

barpath = path.Path(verts, codes)
patch = patches.PathPatch(barpath, edgecolor='C0', label='Sample $\Re(\mathrm{CSD})$')
s1.add_patch(patch)
s1.add_patch(patch_avg)
s1.add_patch(patch_med)

line_list = [patch_avg, patch_med, patch,]

def init_exp(): 
    frame = 0
    s1.set_title('Histogram of CSDs in a single frequency bin' + '\n' + r'Averages M = ' + f'{frame}')
    s1.set_xlabel('$\Re(CSD)$ Value [$\mathrm{V}^2/\mathrm{Hz}$]')
    s1.set_ylabel('Normalized Occurances')

    # s1.set_yticks(10 * np.arange(-6, 6))

    s1.set_xlim(-4e-3, 4e-3) 
    s1.set_ylim(0, 1.1*np.max(NN)) 

    s1.grid()
    # s1.grid(which='minor', ls='--', alpha=0.5)

    s1.legend(loc='upper left')

    return line_list

def update_exp(frame):
    index = 10 * frame + 1
    s1.set_title('Histogram of CSDs in a single frequency bin' + '\n' + r'Averages M = ' + f'{index}')

    temp_Sxys = np.real( Sxys[ff_index,:index] )

    data = temp_Sxys
    temp_mean = np.mean(data)
    temp_median = np.median(data)

    n, _ = np.histogram(data, bins, density=True)
    top = bottom + n
    verts[1::5, 1] = top
    verts[2::5, 1] = top

    cur_xdata, cur_ydata = patch_avg.get_xy()
    cur_xdata0, cur_ydata0 = patch_med.get_xy()

    cur_xdata = temp_mean
    cur_xdata0 = temp_median

    patch_avg.set_xy((cur_xdata, cur_ydata))
    patch_med.set_xy((cur_xdata0, cur_ydata0))

    patch_avg.set_label('Average $\Re(\mathrm{CSD})$ Estimate = ' + f'{temp_mean:.2e}')
    patch_med.set_label('Median $\Re(\mathrm{CSD})$ Estimate = '  + f'{temp_median:.2e}')

    s1.legend(loc='upper left')

    return line_list
 
ani = FuncAnimation(fig, update_exp, frames=avg//10, interval=30, repeat_delay=3000,
                    init_func=init_exp, blit=True) 

ani_name = f'histogram_csd_exponential_rv_animation.mp4'
full_ani_name = f'{fig_dir}/{ani_name}'
print('Writing animation to {}'.format(full_ani_name))
ani.save(full_ani_name)